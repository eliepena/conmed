<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------
  | VERSION SETTINGS
  | -------------------------------------------------------------------
  | Este archivo contiene las configuraciones de version de la aplicacion
  |
 */

$config['version'] = '1.0';
$config['developers'] = array(
    array('name' => 'Nohemi Rojas B.', 'email' => 'nohemir@gmail.com'),
    array('name' => 'Jose Rodriguez E.', 'email' => 'josearodrigueze@gmail.com'),
);
$config['company'] = 'Soluciones AST';
$config['date'] = '12/04/2013';

