<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------
  | VIEW ENGINE SETTINGS
  | -------------------------------------------------------------------
  | Este archivo contiene las configuraciones propias de view engine
  |
  | -------------------------------------------------------------------
  | EXPLICACION DE VARIABLES
  | -------------------------------------------------------------------
  |
  |   ['hide_operation']  Indica si se ocultan operaciones en el actionbar.
  |   ['hide_patter']     Expresion regular que indicara que operaciones seran
  |                       ocultadas segun su URL.
 */

$config['hide_operation'] = TRUE;
$config['hide_pattern'] = array('\/delete$');

/*
 * Variables de configuracion para la subida de archivos.
 * Se usara el prefijo "u" en los congif para identificar que son rutas de
 * uploads.
 *
 *      ['uploads']         Ruta de la carpeta uploads donde se guardaran los
 *                          archivos subidos al servidor.
 *      ['path_image_no_found'] Ruta Imagen no encontrada.
 *      ['path_img_instrumentos']     Ruta de la carpeta materiales en uploads.
 */

$config['uploads'] = 'uploads';
$config['path_image_no_found'] = 'assets/img/image_no_found.jpg';
$config['path_img_instrumentos'] = $config['uploads'] . '/images/instrumentos';
$config['path_img_instrumentos_thumbnail'] = $config['uploads'] . '/images/instrumentos/thumbnail';
$config['app_name'] = 'Sistema de Cotizaciones Conmed';
$config['app_dir'] = 'Centro Empresarial Macaracuay Plaza, Piso 2, Oficina 3B - Telf. 2578555 / 2577169 - Telefax: 2571059 - Caracas - Aptdo. Postal Plaza las Americas 66197';
$config['app_logo'] = 'assets/img/logo.jpg';
$config['valor_iva'] = 12;
$config['pdf_background_image_validate'] = 'assets/img/logo_pdf_nuevo.jpg';
$config['pdf_background_image_no_validate'] = 'assets/img/logo_pdf_nuevo_novalido.jpg';
$config['global_img_validate'] = false;
$config['pdf_al_vuelo'] = false;

$config['emp_etiq'] ='Corporaci\A2n Conmed C.A.';
$config['dir_etiq1'] = 'CZntro EmprZsarial Macaracuay Plaza,';
$config['dir_etiq2'] = 'P-2, Ofic. 3B., Caracas-VZnZzuZla 1070';
$config['telefono_etiq'] = '(58)212-2571059/257 - 8555';
$config['rif_etiq'] = 'J-30545629-1';
$config['dir_impresora'] = '192.168.1.115';

$config['app_from_email'] = 'desarrollo.ast@gmail.com';
$config['protocol'] = 'smtp';
$config['smtp_host'] = 'ssl://smtp.googlemail.com';
$config['smtp_port'] = '465';
$config['smtp_user'] = 'desarrollo.ast@gmail.com';
$config['smtp_pass'] = '3m4i1ast';
//$config['app_from_email'] = 'no-reply@movil.corporacionconmed.com';
//$config['protocol']     = 'SSL/TLS';
//$config['smtp_host']    = 'vps-1135831-17083-manage.myhosting.com';
//$config['smtp_port']    = '465';
//$config['smtp_user']    = 'no-reply@movil.corporacionconmed.com';
//$config['smtp_pass']  = 'm0v1lc0rm41l';
$config['smtp_timeout'] = '';
$config['charset'] = 'utf-8';
$config['newline'] = "\r\n";
$config['mailtype'] = 'html'; // or html
