<?php

if (!defined('BASEPATH'))
    exit('Acceso Denegado');

/*
  | -------------------------------------------------------------------------
  | Email
  | -------------------------------------------------------------------------
  | This file lets you define parameters for sending emails.
  | Please see the user guide for info:
  |
  |	http://codeigniter.com/user_guide/libraries/email.html
  |
 */

$config = array(
  'mailtype' => "html",
  'protocol' => "smtp",
  'smtp_port' => "465",
  'smtp_host' => "ssl://smtp.googlemail.com",
  'smtp_user' => 'desarrollo.ast@gmail.com',
  'smtp_pass' => '3m4i1ast',
// 'smtp_host' => 'vps-1135831-17083.manage.myhosting.com',
// 'smtp_user' => 'no-reply@movil.corporacionconmed.com',
// 'smtp_pass' => 'm0v1lc0r3m41l',  
  'charset' => "utf-8",
  'newline' => "\r\n"
);


/* End of file email.php */
/* Location: ./application/config/email.php */