<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
  |------------------------
  | Calendar Path Mark Time
  |------------------------
  |
  | Path que contendra las marcas de actualizacion para el calendario
 */
$config['calendar_path_mark'] = './assets/system/';

/*
  |------------------------
  | Calendar File Mark Time
  |------------------------
  |
  | Archivo que contendra las marcas de actualizacion para el calendario
 */
$config['calendar_file_mark'] = 'calendar.txt';

/* End of file calendar.php */
/* Location: ./application/config/calendar.php */