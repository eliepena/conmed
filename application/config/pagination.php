<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| PAGINATION SETTINGS
| -------------------------------------------------------------------
| Este archivo contiene las configuraciones para la paginacion.
|
| -------------------------------------------------------------------
| EXPLACACION DE VARIABLES
| -------------------------------------------------------------------
|
|	['limit'] limite de de la paginacion.
*/

$config['limit'] = 15;