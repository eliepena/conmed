<?php

/**
 * Archivo de lenguaje donde estan los mensajes a ser vistos dentro de los grids
 */

$lang['sZeroRecords'] = 'Showing 0 to 0 of 0 records';
$lang['sLengthMenu'] = 'records per page';
$lang['sInfo'] = 'Showing _START_ to _END_ of _TOTAL_ records';
$lang['sZeroRecords'] = 'Nothing found - sorry';
$lang['sInfoFiltered'] = '(filtered from _MAX_ total records)';