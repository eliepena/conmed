<?php

$lang['required'] = 'El campo %s es requerido.';
$lang['isset'] = 'El campo %s debe contener un valor.';
$lang['valid_email'] = 'El %s debe ser una dirección valida.';
$lang['valid_emails'] = 'El campo %s debe contener todas las direcciones de email validas.';
$lang['valid_url'] = 'El campo %s debe contener una URL valida.';
$lang['valid_ip'] = 'El campo %s debe contener una IP valida.';
$lang['min_length'] = 'El %s debe tener al menos %s caracteres.';
$lang['max_length'] = 'El %s no puede exceder los %s caracteres.';
$lang['exact_length'] = 'El campo %s debe tener exactamente %s caracteres.';
$lang['alpha'] = 'El campo %s debe contener solo letras.';
$lang['alpha_numeric'] = 'El campo %s debe contener solo letras/números.';
$lang['alpha_dash'] = 'El campo %s solamente puede contener caracteres alfanuméricos, guiones bajos y saltos.';
$lang['numeric'] = 'El campo %s debe contener solo números.';
$lang['is_numeric'] = 'El campo %s debe contener solo números.';
$lang['integer'] = 'El campo %s debe contener un número entero.';
$lang['matches'] = 'El campo %s no coincide con el campo %s.';
$lang['is_natural'] = 'El campo %s debe contener solo numeros positivos.';
$lang['is_natural_no_zero'] = 'El campo %s debe contener un número mayor que cero.';
$lang['is_unique'] = 'El campo %s ya se encuentra registrado en el sistema.';
$lang['valid_alpha_space'] = 'El campo %s debe contener solo letras/espacios.';
$lang['valid_alpha_numeric_space'] = 'El campo %s debe contener solo letras, espacios, números, comas y puntos.';
$lang['valid_phone'] = 'El campo %s debe contener un telefono valido Ejm: 04121233443.';
$lang['valid_rif'] = 'El campo %s debe contener un rif valido. Ejm: J123456789.';
$lang['valid_date'] = 'El campo %s debe ser una fecha valida con formato 16-12-1985.';