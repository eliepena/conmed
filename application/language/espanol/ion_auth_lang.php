<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Ion Auth Lang - Spanish
* 
* Author: Wilfrido Garc�a Espinosa
* 		  contacto@wilfridogarcia.com
*         @wilfridogarcia
* 
* Location: http://github.com/benedmunds/ion_auth/
*          
* Created:  05.04.2010 
* 
* Description:  Spanish language file for Ion Auth messages and errors
* 
*/

// Account Creation
$lang['account_creation_successful'] 	  	 = 'Cuenta creada con éxito';
$lang['account_creation_unsuccessful'] 	 	 = 'No se ha podido crear la cuenta';
$lang['account_creation_duplicate_email'] 	 = 'Email en uso o inválido';
$lang['account_creation_duplicate_username'] = 'Nombre de usuario en uso o inválido';


// Password
$lang['password_change_successful'] 	 	 = 'Contraseña renovada éxitosamente';
$lang['password_change_unsuccessful'] 	  	 = 'No se ha podido cambiar la contraseña';
$lang['forgot_password_successful'] 	 	 = 'Te hemos enviado un correo con las instrucciones a seguir';
$lang['forgot_password_unsuccessful'] 	 	 = 'No se ha podido crear una nueva contraseña';

// Activation
$lang['activate_successful'] 		  	     = 'Cuenta activada';
$lang['activate_unsuccessful'] 		 	     = 'No se ha podido activar la cuenta';
$lang['deactivate_successful'] 		  	     = 'Cuenta desactivada';
$lang['deactivate_unsuccessful'] 	  	     = 'No se ha podido desactivar la cuenta';
$lang['activation_email_successful'] 	  	 = 'Email de activación enviado';
$lang['activation_email_unsuccessful']   	 = 'No se ha podido enviar el email de activación';
$lang['activate_email_header']                   ='Bievenido a la Corporaci&oacute;n Conmed';
$lang['activate_email_welcome']                   ='Gracias por registrarse';
$lang['activate_email_title']                   ='Active la cuenta para ';
$lang['activate_email_message']                   ='Haciendo click ';
$lang['activate_email_link']                   ='Aqu&iacute;';

// Login / Logout
$lang['login_successful'] 		      	     = 'Sesión iniciada con éxito';
$lang['login_unsuccessful'] 		  	     = 'No se ha podido iniciar sesión';
$lang['login_unsuccessful_not_active'] 		 = 'La cuenta se encuentra inactiva';
$lang['logout_successful'] 		 	         = 'Sesión finalizada con éxito';

// Account Changes
$lang['update_successful'] 		 	         = 'Información de la cuenta actualizada con éxito';
$lang['update_unsuccessful'] 		 	     = 'No se ha podido actualizar la información de la cuenta';
$lang['delete_successful'] 		 	         = 'Usuario eliminado';
$lang['delete_unsuccessful'] 		 	     = 'No se ha podido Eliminar el usuario';

// Email Subjects
$lang['email_forgotten_password_subject']    = 'Verificación de contraseña olvidada';
$lang['email_new_password_subject']          = 'Nueva Contraseña';
$lang['email_activation_subject']            = 'Activaci&oacute;n de la cuenta';

//Email
$lang['email_reset_password']                      =' Cambio de contrase&ntilde;a a ';
$lang['email_reset_password_instruction']          =' Por favor haga click aqu&iacute; para ';
$lang['email_reset_password_action']               =' Cambiar su contrase&ntilde;a ';