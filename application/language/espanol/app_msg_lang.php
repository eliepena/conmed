<?php

/**
 * Archivo de lenguaje donde estan los mensajes generales de la aplicacion.
 */
$lang['user_no_role'] = 'El usuario no tiene un rol asociado';
$lang['create_success'] = 'Datos guardados satisfactoriamente.';
$lang['update_success'] = 'Datos modificados satisfactoriamente.';
$lang['error'] = 'No se pudo guardar el registro, si el problema persiste comuniquese con sistemas';