<?php

/**
 * Archivo de lenguaje donde estan los mensajes para los documentos generados
 */
//DOUMENTO : PDF COTIZACION
//
//----------------------------------------------
//$lang['PdfCot_Title']         = 'COTIZACION';
$lang['PdfCot_Cod'] = 'Cod';
$lang['PdfCot_Mod'] = 'Modelo';
$lang['PdfCot_Descrip'] = 'Descripcion';
$lang['PdfCot_Gart'] = 'Gart';
$lang['PdfCot_Cant'] = 'Cant';
$lang['PdfCot_Prec'] = 'Precio Bs.F';
$lang['PdfCot_NetTabla'] = 'Neto Bs.F';

$lang['PdfCot_Client'] = 'Cliente';
$lang['PdfCot_Rif'] = 'R.I.F.';
$lang['PdfCot_Nit'] = 'N.I.T.';
$lang['PdfCot_Seg'] = 'Seguro';
$lang['PdfCot_Tlf'] = 'Tlf. Paciente';


$lang['PdfCot_NroCot'] = 'N° Cotizacion';
$lang['PdfCot_FecEmi'] = 'Fecha Emision';
$lang['PdfCot_ForPag'] = 'Forma de Pago';
$lang['PdfCot_TiempEsp'] = 'Tiempo Entrega';
$lang['PdfCot_Vend'] = 'Vendedor';
$lang['PdfCot_MedTratant'] = 'Medico Tratante';

$lang['PdfCot_MontExent'] = 'Monto Exento';
$lang['PdfCot_MontGrav'] = 'Monto Gravable';
$lang['PdfCot_Desc'] = '% Descuento';
$lang['PdfCot_Recarg'] = '% Recargo';
$lang['PdfCot_Iva'] = 'I.V.A';
$lang['PdfCot_NetTotal'] = 'Neto Bs.F.';



$lang['PdfCot_Valid_Line_1'] = 'Cotizacion valida por 20 dias continuos.';
$lang['PdfCot_Valid_Line_2'] = 'Precio sujeto a cambio sin previo aviso.';
$lang['PdfCot_EmitCheq_Line_1'] = 'Favor emitir cheque a nombre de:';
$lang['PdfCot_EmitCheq_Line_2'] = 'CORPORACION CONMED, C.A';
$lang['PdfCot_Nota_Line_1'] = 'NOTA: EN CASO DE QUE EL SEGURO NO CUBRA EL MONTO REFLEJADO EN ESTA COTIZACION,EL';
$lang['PdfCot_Nota_Line_2'] = 'PACIENTE DEBERA CANCELAR LA DIFERENCIA Y HASTA NO HABER CANCELADO EL MONTO TOTAL';
$lang['PdfCot_Nota_Line_3'] = 'NO SE DESPACHARA EL MATERIAL';
$lang['PdfCot_MsjTabla'] = 'Atencion';
$lang['PdfCot_email'] = '';



//DOUMENTO : PDF COTIZACION_BACKUP
//
//----------------------------------------------
//$lang['PdfCot_Title_backup']         = 'BackUp';
$lang['PdfCot_Cod_backup'] = 'Cod';
$lang['PdfCot_Mod_backup'] = 'Modelo';
$lang['PdfCot_Descrip_backup'] = 'Descripcion';
$lang['PdfCot_Cant_backup'] = 'Cant';
$lang['PdfCot_Precio_backup'] = 'Precio';
$lang['PdfCot_Total_backup'] = 'SubTotales';

$lang['PdfCot_Client_backup'] = 'Cliente';
$lang['PdfCot_Rif_backup'] = 'R.I.F.';
$lang['PdfCot_Nit_backup'] = 'N.I.T.';
$lang['PdfCot_Seg_backup'] = 'Seguro';
$lang['PdfCot_Tlf_backup'] = 'Tlf. Paciente';


$lang['PdfCot_NroCot_backup'] = 'N° Cotizacion';
$lang['PdfCot_FecEmi_backup'] = 'Fecha Emision';
$lang['PdfCot_ForPag_backup'] = 'Forma de Pago';
$lang['PdfCot_TiempEsp_backup'] = 'Tiempo Entrega';
$lang['PdfCot_Vend_backup'] = 'Vendedor';
$lang['PdfCot_MedTratant_backup'] = 'Medico Tratante';

$lang['PdfCot_MsjTabla_backup'] = 'Atencion';

$lang['PdfCot_MontSubTotal_backup'] = 'Monto SubTotal';
$lang['PdfCot_Iva_backup'] = 'I.V.A';
$lang['PdfCot_MontTotal_backup'] = 'Monto Total Bs.F.';


//DOUMENTO : PDF COTIZACION_INSTRUMENTO
//
//----------------------------------------------
//$lang['PdfCot_Title_instru']         = 'INSTRUMENTAL';
$lang['PdfCot_Cod_instru'] = 'Cod';
$lang['PdfCot_Mod_instru'] = 'Modelo';
$lang['PdfCot_Descrip_instru'] = 'Descripcion';

$lang['PdfCot_Cant_instru'] = 'Cant';

$lang['PdfCot_NetTabla_instru'] = 'Imagen';

$lang['PdfCot_Client_instru'] = 'Cliente';
$lang['PdfCot_Rif_instru'] = 'R.I.F.';
$lang['PdfCot_Nit_instru'] = 'N.I.T.';
$lang['PdfCot_Seg_instru'] = 'Seguro';
$lang['PdfCot_Tlf_instru'] = 'Tlf. Paciente';


$lang['PdfCot_NroCot_instru'] = 'N° Cotizacion';
$lang['PdfCot_FecEmi_instru'] = 'Fecha Emision';
$lang['PdfCot_ForPag_instru'] = 'Forma de Pago';
$lang['PdfCot_TiempEsp_instru'] = 'Tiempo Entrega';
$lang['PdfCot_Vend_instru'] = 'Vendedor';
$lang['PdfCot_MedTratant_instru'] = 'Medico Tratante';

$lang['PdfCot_MsjTabla_instru'] = 'Atencion';



//DOUMENTO : PDF ORDEN DE ENTREGA
//
//----------------------------------------------
$lang['PdfOrd_Title_Mat'] = 'ORDEN DE ENTREGA(<font size="11pt">Materiales</font>)';
$lang['PdfOrd_Title_Backup'] = 'ORDEN DE ENTREGA(<font size="11pt">Backup</font>)';
$lang['PdfOrd_Title_Ins'] = 'ORDEN DE ENTREGA(<font size="11pt">Instrumental</font>)';
$lang['PdfOrd_Title_Almacen'] = 'ORDEN DE ENTREGA(<font size="11pt">ALMACEN</font>)';
$lang['PdfDev_Title_Mat'] = 'ORDEN DE DEVOLUCION(<font size="11pt">Materiales</font>)';
$lang['PdfDev_Title_Backup'] = 'ORDEN DE DEVOLUCION(<font size="11pt">Backup</font>)';
$lang['PdfDev_Title_Ins'] = 'ORDEN DE DEVOLUCION(<font size="11pt">Instrumental</font>)';
$lang['PdfDev_Title_Almacen'] = 'ORDEN DE DEVOLUCION(<font size="11pt">ALMACEN</font>)';
$lang['PdfOrd_Lote'] = 'Lote';
$lang['PdfOrd_Cod'] = 'Cod';
$lang['PdfOrd_Mod'] = 'Modelo';
$lang['PdfOrd_Descrip'] = 'Descripcion';
$lang['PdfOrd_Gart'] = 'Gart';
$lang['PdfOrd_Cant'] = 'Cant';
$lang['PdfOrd_Prec'] = 'Precio Bs.F';
$lang['PdfOrd_NetTabla'] = 'Neto Bs.F';

$lang['PdfOrd_Client'] = 'Cliente';
$lang['PdfOrd_Rif'] = 'R.I.F.';
$lang['PdfOrd_Nit'] = 'N.I.T.';
$lang['PdfOrd_Seg'] = 'Seguro';
$lang['PdfOrd_Tlf'] = 'Tlf. Paciente';


$lang['PdfOrd_NroCot'] = 'N° Cotizacion';
$lang['PdfOrd_NroOrd'] = 'N° Orden';
$lang['PdfOrd_NroImport'] = 'Importado de';
$lang['PdfOrd_FecEmi'] = 'Fecha Emision';
$lang['PdfOrd_ForPag'] = 'Forma de Pago';
$lang['PdfOrd_OrdenCompra'] = 'Orden de Compra';
$lang['PdfOrd_Vend'] = 'Vendedor';
$lang['PdfOrd_MedTratant'] = 'Medico Tratante';

$lang['PdfOrd_SubTotal'] = 'Sub-Total Bs';
$lang['PdfOrd_Desc'] = '% Descuento';
$lang['PdfOrd_Recarg'] = '% Recargo';
$lang['PdfOrd_Iva'] = 'I.V.A';
$lang['PdfOrd_NetTotal'] = 'Neto Bs.F.';



$lang['PdfOrd_Valid_Line_1'] = '';
$lang['PdfOrd_Valid_Line_2'] = '';
$lang['PdfOrd_Firma_Line_1'] = '';
$lang['PdfOrd_Firma_Line_2'] = 'Recibi conforme:';
$lang['PdfOrd_Firma_Line_3'] = '';
$lang['PdfOrd_Firma_Line_4'] = '';
$lang['PdfOrd_Firma_Line_5'] = '__________________________';
$lang['PdfOrd_MsjTabla'] = 'Atencion';
$lang['PdfOrd_email'] = '';
$lang['PdfOrd_Derecho'] = 'SIN DERECHO A CREDITO FISCAL';


//DOUMENTO : PDF ORDEN DE ENTREGA LIBRE
//
//----------------------------------------------
$lang['free_PdfOrd_Title_Mat'] = 'ORDEN DE ENTREGA (Almacen)';
$lang['free_PdfOrd_Lote'] = 'Lote';
$lang['free_PdfOrd_Cod'] = 'Cod';
$lang['free_PdfOrd_Mod'] = 'Modelo';
$lang['free_PdfOrd_Descrip'] = 'Descripcion';
$lang['free_PdfOrd_Gart'] = 'Gart';
$lang['free_PdfOrd_Cant'] = 'Cant';
$lang['free_PdfOrd_Prec'] = 'Precio Bs.F';
$lang['free_PdfOrd_NetTabla'] = 'Neto Bs.F';

$lang['free_PdfOrd_Client'] = 'Persona/Empresa';
$lang['free_PdfOrd_ced_Rif'] = 'Cedula/Rif';
$lang['free_PdfOrd_Nit'] = 'N.I.T.';
$lang['free_PdfOrdEmail'] = 'Email';
$lang['free_PdfOrd_Tlf'] = 'Tlf. Cliente';


$lang['free_PdfOrd_NroCot'] = 'N° Cotizacion';
$lang['free_PdfOrd_NroOrd'] = 'N° Orden';
$lang['free_PdfOrd_NroImport'] = 'Importado de';
$lang['free_PdfOrd_FecEmi'] = 'Fecha Emision';
$lang['free_PdfOrd_FecDespacho'] = 'Fecha Entrega';
$lang['free_PdfOrd_OrdenCompra'] = 'Orden de Compra';
$lang['free_PdfOrd_Vend'] = 'Vendedor';
$lang['free_PdfOrd_MedTratant'] = 'Medico Tratante';

$lang['free_PdfOrd_SubTotal'] = 'Sub-Total Bs';
$lang['free_PdfOrd_Desc'] = '% Descuento';
$lang['free_PdfOrd_Recarg'] = '% Recargo';
$lang['free_PdfOrd_Iva'] = 'I.V.A';
$lang['free_PdfOrd_NetTotal'] = 'Neto Bs.F.';



$lang['free_PdfOrd_Valid_Line_1'] = '';
$lang['free_PdfOrd_Valid_Line_2'] = '';
$lang['free_PdfOrd_Firma_Line_1'] = '';
$lang['free_PdfOrd_Firma_Line_2'] = 'Recibi conforme:';
$lang['free_PdfOrd_Firma_Line_3'] = '';
$lang['free_PdfOrd_Firma_Line_4'] = '';
$lang['free_PdfOrd_Firma_Line_5'] = '__________________________';
$lang['free_PdfOrd_MsjTabla'] = 'Atencion';
$lang['free_PdfOrd_email'] = '';
$lang['free_PdfOrd_Derecho'] = 'SIN DERECHO A CREDITO FISCAL';

//DOUMENTO : PDF ORDEN DE ENTREGA LIBRE
//
//----------------------------------------------
$lang['free_Almac_PdfOrd_Title_Mat'] = 'ORDEN DE ENTREGA';
$lang['free_Almac_PdfOrd_Lote'] = 'Lote';
$lang['free_Almac_PdfOrd_Cod'] = 'Cod';
$lang['free_Almac_PdfOrd_Mod'] = 'Modelo';
$lang['free_Almac_PdfOrd_Descrip'] = 'Descripcion';
$lang['free_Almac_PdfOrd_Gart'] = 'Gart';
$lang['free_Almac_PdfOrd_Cant'] = 'Cant';
$lang['free_Almac_PdfOrd_Prec'] = 'Precio Bs.F';
$lang['free_Almac_PdfOrd_NetTabla'] = 'Neto Bs.F';

$lang['free_Almac_PdfOrd_Client'] = 'Persona/Empresa';
$lang['free_Almac_PdfOrd_ced_Rif'] = 'Cedula/Rif';
$lang['free_Almac_PdfOrd_Nit'] = 'N.I.T.';
$lang['free_Almac_PdfOrdEmail'] = 'Email';
$lang['free_Almac_PdfOrd_Tlf'] = 'Tlf. Cliente';


$lang['free_Almac_PdfOrd_NroCot'] = 'N° Cotizacion';
$lang['free_Almac_PdfOrd_NroOrd'] = 'N° Orden';
$lang['free_Almac_PdfOrd_NroImport'] = 'Importado de';
$lang['free_Almac_PdfOrd_FecEmi'] = 'Fecha Emision';
$lang['free_Almac_PdfOrd_ForPag'] = 'Forma de Pago';
$lang['free_Almac_PdfOrd_OrdenCompra'] = 'Orden de Compra';
$lang['free_Almac_PdfOrd_Vend'] = 'Vendedor';
$lang['free_Almac_PdfOrd_MedTratant'] = 'Medico Tratante';

$lang['free_Almac_PdfOrd_SubTotal'] = 'Sub-Total Bs';
$lang['free_Almac_PdfOrd_Desc'] = '% Descuento';
$lang['free_Almac_PdfOrd_Recarg'] = '% Recargo';
$lang['free_Almac_PdfOrd_Iva'] = 'I.V.A';
$lang['free_Almac_PdfOrd_NetTotal'] = 'Neto Bs.F.';



$lang['free_Almac_PdfOrd_Valid_Line_1'] = '';
$lang['free_Almac_PdfOrd_Valid_Line_2'] = '';
$lang['free_Almac_PdfOrd_Firma_Line_1'] = '';
$lang['free_Almac_PdfOrd_Firma_Line_2'] = 'Recibi conforme:';
$lang['free_Almac_PdfOrd_Firma_Line_3'] = '';
$lang['free_Almac_PdfOrd_Firma_Line_4'] = '';
$lang['free_Almac_PdfOrd_Firma_Line_5'] = '__________________________';
$lang['free_Almac_PdfOrd_MsjTabla'] = 'Atencion';
$lang['free_Almac_PdfOrd_email'] = '';
$lang['free_Almac_PdfOrd_Derecho'] = 'SIN DERECHO A CREDITO FISCAL';
