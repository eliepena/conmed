<?php

/**
 * Archivo de lenguaje donde estan los mensajes a ser vistos dentro de los grids
 */

$lang['sLengthMenu'] = 'registros por pagina';
$lang['sInfo'] = 'Resultados desde _START_ al _END_ de un total de _TOTAL_ registros.';
$lang['sZeroRecords'] = 'No se encontraron resultados.';
$lang['sInfoEmpty'] = 'Resultados desde 0 al 0 de un total de 0 registros.';
$lang['sInfoFiltered'] = '(filtrado de un total de _MAX_ registros)';