<?php

/**
 * Archivo de lenguaje donde se encuentran los mensajes de correo
 */
$lang['subject_cot_emitida'] = 'Emitida Cotización';
$lang['body_cot_emitida'] = 'La cotizacion con el N° %s ha sido emitida';

$lang['subject_cot_procesada'] = 'Procesada Cotización';
$lang['body_cot_procesada'] = 'La cotizacion con el N° %s ha sido procesada';

$lang['subject_cot_procesada_medio_pago'] = 'Procesada Cotizacion con medio de pago';
$lang['body_cot_procesada_medio_pago'] = 'La Cotización con el N° %s ha sido procesada con el medio de pago';

$lang['subject_cot_pre_aprobada'] = 'Pre-Aprobada Cirugía';
$lang['body_cot_pre_aprobada'] = 'La Cotización con el N° %s ha sido pre-aprobada para realizar la cirugía';

$lang['subject_cot_confirmada'] = 'Confirmada Cirugía';
$lang['body_cot_confirmada'] = 'La Cotización con el N° %s ha sido confirmada para realizar la cirugía';

$lang['subject_cot_reprogramada'] = 'Reprogramada Cirugia';
$lang['body_cot_reprogramada'] = 'La Cotizacion con el N° %s ha sido reprogramada para realizar la cirugia';

$lang['subject_cot_aprobada'] = 'Reprogramada Cirugia';
$lang['body_cot_aprobada'] = 'La Cotizacion con el N° %s ha sido aprobada para realizar la cirugia';

$lang['subject_cot_reprogramar'] = 'Reprogramar Cirugia';
$lang['body_cot_reprogramar'] = 'La Cotizacion con el N° %s debe ser reprogramada para realizar la cirugia';

$lang['subject_cot_almacen'] = 'Notificacion de almacen';
$lang['body_cot_almacen'] = 'La Cotizacion con el N° %s presenta una notificacion de almacen.';

$lang['subject_cot_sustituir'] = 'Sustituir Articulos';
$lang['body_cot_sustituir'] = 'La Cotizacion con el N° %s posee articulos sin disponibilidad en almacen los cuales deben ser sustituidos.';

$lang['subject_despacho']='Se ha creado la orden de despacho';
$lang['body_des_despachado']='Los articulos de la cotizacion N° %s han sido despachados';