<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Send_notification_model
 *
 * Esta clase provee la implementacion
 * de envio de correos masivos mediante CronJob
 *
 * @package Models
 * @category Model
 * @version V-1.0 17/01/13 03:23 PM
 */
class Send_notification_model extends CI_Model {

    public function __construct() {

        parent::__construct();
    }

    /**
     * Provee los id de las cotizaciones pendientes por envio
     *
     * @access  public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com >
     * @version 1.0 28/11/13 04:51 PM
     */
    function getIdQuotesForSendMail() {
        $t_cotizacion = 'business.cotizacion';
        $this->db->select("$t_cotizacion.id");
        $this->db->from($t_cotizacion);
        $this->db->where(array("$t_cotizacion._delete" => "0"));
        $this->db->where(array("$t_cotizacion.send_mail" => "0"));
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    /**
     * Una vez enviado el correo cambia el valor del campo send_mail a 1 en la cotizacion
     *
     * @access  public
     * @param integer $id_cotizacion Identificador de la cotizacion
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com >
     * @version 1.0 29/11/13 02:33 PM
     */
    function changeValueSendMailByQuote($id_cotizacion = NULL) {
        if ($id_cotizacion):
            $send_mail = array('send_mail' => '1');
            $this->db->where_in('id', $id_cotizacion);
            $this->db->update("business.cotizacion", $send_mail);
        endif;
    }

}
