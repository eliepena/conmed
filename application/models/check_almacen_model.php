<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of check_almacen_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad almacen
 * y sus entidades dependientes o extendidas
 *
 * @package Modules
 * @category Model
 * @version V-1.0 17/01/13 03:23 PM
 */
class Check_almacen_model extends CI_Model {

    public function __construct() {

        parent::__construct();
    }

    /**
     * Provee todas las  cantidades de articulos existentes en almacen
     *
     * @access  public
     * @param integer $cantidad Cantidad con la cual se evaluara la disponibilidad de articulos en almacen
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com >
     * @version 1.0 19/11/13 02:28 PM
     */
    function availabilityOfItems($cantidad = 0) {
        $t_tramo_articulo = 'business.tramo_articulo';
        $this->db->select("$t_tramo_articulo.id_articulo as id");
        $this->db->from($t_tramo_articulo);
        $this->db->where(array("$t_tramo_articulo._delete" => "0"));
        $this->db->group_by("$t_tramo_articulo.id_articulo");
        $this->db->having("sum($t_tramo_articulo.stock)=$cantidad");
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    /**
     * Provee todas las ID de articulos existentes en las cotizaciones
     * cuyo sea estatus sea igual al definido o pasado en el parametro $estatus
     *
     * @access  public
     * @param   string  $estatus Estatus de las cotizaciones a listar
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com >
     * @version 1.0 21/10/13 11:55 AM
     */
    function getQuoteWithFilterStatus($status = NULL) {
        if (!$status)
            return FALSE;
        $this->load->model('engine/engine_model', 'engine_model');
        $t_cotizacion = 'business.cotizacion';
        $cot_aprobadas = array();
        $status_name = mb_convert_case($status, MB_CASE_TITLE, "UTF-8");
        $status_id = $this->engine_model->getValueByCategory($status_name, '_label');

        if (!$status_id)
            return FALSE;

        $this->db->select("$t_cotizacion.*", FALSE);
        $this->db->from("$t_cotizacion");
        $query = $this->db->get();

        $cotizaciones = ($query->num_rows() > 0) ? $query->result_array() : FALSE;

        if ($cotizaciones):
            foreach ($cotizaciones as $key => $value):
                $max_estatus = $this->getStatus($value['id']);
                if ($max_estatus):
                    if ($max_estatus['status_id'] == $status_id):
                        $cot_aprobadas[$value['id']] = $this->getArticlesQuote($value['id']);
                    endif;
                endif;
            endforeach;
        endif;
        return (sizeof($cot_aprobadas) > 0) ? $cot_aprobadas : FALSE;
    }

    /**
     * Obtiene el status actual de una cotizacion
     *
     * @access  public
     * @param   integer  $id_cotizacion Identificador de la cotizacion
     * @return  array Resultado de la operacion.
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com >
     * @version 1.0 21/10/13 11:55 AM
     */
    function getStatus($id_cotizacion = NULL) {
        if (!$id_cotizacion)
            return FALSE;
        $this->db->select('cat_cotizacion_estatus AS status_id, _label AS status_name');
        $this->db->from('business.cotizacion_estatus');
        $this->db->join('dynamic.category', 'cat_cotizacion_estatus=category.id');
        $this->db->where('id_cotizacion', $id_cotizacion);
        $this->db->order_by('created_at', 'DESC');
        $this->db->order_by('status_id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row_array() : FALSE;
    }

    /**
     * Obtiene el arreglo de articulos de una cotizacion
     *
     * @access  public
     * @param   integer  $id_cotizacion Identificador de la cotizacion
     * @return  array Resultado de la operacion.
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com >
     * @version 1.0 21/10/13 11:55 AM
     */
    function getArticlesQuote($id_cotizacion = NULL) {
        if (!$id_cotizacion)
            return FALSE;
        $t_cotizacion_articulo = 'business.cotizacion_articulo';
        $this->db->select("$t_cotizacion_articulo.id_articulo");
        $this->db->from("$t_cotizacion_articulo");
        $this->db->where("$t_cotizacion_articulo.id_cotizacion", $id_cotizacion);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    /**
     * obtiene el correo electronico de un vendedor de una cotizacion dado el codigo de la misma
     *
     * @access public
     * @param integer $id_cotizacion Identificador de la cotizacion
     * @return array $data email vendedor
     * @author  Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version 1.0 06/08/13 10:22 AM
     */
    function getEmailVendedor($id_cotizacion = array()) {
        $this->db->select('per.correo_electronico');
        $this->db->from('business.cotizacion cot');
        $this->db->join('business.persona per', 'per.id=cot.id_vendedor');
        $this->db->where($id_cotizacion);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : NULL;
    }

    /**
     * Obtiene el codigo de una cotizacion dado su identificador
     *
     * @access  public
     * @param   integer  $id_cotizacion identificador de la cotizacion
     * @return  string   codigo de la cotizacion
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com >
     * @version 1.0 21/10/13 11:55 AM
     */
    public function getCode($id_cotizacion) {
        $this->db->select('codigo');
        $this->db->from('business.cotizacion');
        $this->db->where('id', $id_cotizacion);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->row()->codigo) : NULL;
    }

    /**
     * Dado un identificador obtiene los datos asociados al identifiador.
     *
     * @access  public
     * @param   Integer $id Identificador del registro a ser consultado.
     * @param   string $field campo por el cual se realizara la busqueda
     * @return  Array   Con los datos del registro consultado.
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com >
     * @version 1.0 21/10/13 11:55 AM
     */
    public function getById($id, $field = 'id') {

        // Tables
        $table = 'business.cotizacion';
        $t_medico = 'business.medico';
        $t_centro_salud = 'business.centro_salud';
        $t_empleado = 'business.empleado';
        $t_paciente = 'business.paciente';
        $t_cotizacion_pago = 'business.cotizacion_pago';
        $t_cotizacion_cirugia = 'business.cotizacion_cirugia';
        $t_seguro = 'business.seguro';
        $t_cat_tipo_pago = 'dynamic.category';
        $t_cirugia = 'business.cirugia';
        $t_lesion = 'lesion';
        $t_articulacion = 'articulacion';
        $t_tecnica = 'tecnica';

        // Selects
        $this->db->select($table . '.id');
        $this->db->select("(CASE WHEN $table.observacion IS NULL THEN
                                                    'SIN OBSERVACIONES'
                                              ELSE
                                                    $table.observacion
                                              END) as observacion", FALSE);
        $this->db->select($table . '.codigo');
        $this->db->select($table . '.created_at AS fecha_cotizacion');
        $this->db->select($table . '.fecha_cirugia');
        $this->db->select($table . '.hora_cirugia');
        $this->db->select($table . '.descuento');
        $this->db->select($table . '.recargo');
        $this->db->select($table . '.id_validador');
        $this->db->select($table . '.id_paciente');
        $this->db->select($table . '.monto_total');
        $this->db->select($table . '.id_seguro');
        $this->db->select($table . '.enviar');
        $this->db->select($t_cotizacion_pago . '.id_empresa');
        $this->db->select($t_cotizacion_pago . '.id_persona');
        $this->db->select($t_cotizacion_pago . '.monto');
        $this->db->select($t_cotizacion_pago . '.fecha_instrumento');
        $this->db->select($t_cotizacion_pago . '.cat_tipo_pago');
        $this->db->select($t_cotizacion_pago . '.cat_pagador');
        $this->db->select($t_cotizacion_pago . '.numero_instrumento');
        $this->db->select($t_cotizacion_pago . '.cat_banco');
        $this->db->select($t_cotizacion_pago . '.vencimiento');
        $this->db->select($t_cotizacion_pago . '.deducible');
        $this->db->select($t_cat_tipo_pago . '._label AS tipo_pago');
        $this->db->select($t_medico . '.id AS id_medico_id_ec');
        $this->db->select("$t_medico.primer_nombre || ' ' || $t_medico.primer_apellido AS id_medico");
        $this->db->select("$t_empleado.primer_nombre || ' ' || $t_empleado.primer_apellido AS id_vendedor");
        $this->db->select("$t_paciente.primer_nombre || ' ' || $t_paciente.primer_apellido AS id_paciente");
        $this->db->select($t_paciente . '.id AS id_paciente_id_ec');
        $this->db->select($t_paciente . '.validado AS id_paciente_validado');
        $this->db->select($t_centro_salud . '.id AS id_centro_salud_id_ec');
        $this->db->select($t_centro_salud . '.nombre AS id_centro_salud');
        $this->db->select($t_articulacion . '.nombre AS id_articulacion');
        $this->db->select($t_articulacion . '.id AS cod_articulacion');
        $this->db->select($t_tecnica . '.nombre AS id_tecnica');
        $this->db->select($t_lesion . '.nombre AS id_lesion');

        // Froms and inners
        $this->db->from($table);
        $this->db->join($t_medico, "$t_medico.id=$table.id_medico");
        $this->db->join($t_centro_salud, "$t_centro_salud.id=$table.id_centro_salud");
        $this->db->join($t_empleado, "$t_empleado.id=$table.id_vendedor");
        $this->db->join($t_paciente, "$t_paciente.id=$table.id_paciente");
        $this->db->join($t_cotizacion_cirugia, "$t_cotizacion_cirugia.id_cotizacion=$table.id");
        $this->db->join("$t_cirugia AS $t_articulacion", "$t_articulacion.id=$t_cotizacion_cirugia.id_articulacion");
        $this->db->join("$t_cirugia AS $t_lesion", "$t_lesion.id=$t_cotizacion_cirugia.id_lesion");
        $this->db->join("$t_cirugia AS $t_tecnica", "$t_tecnica.id=$t_cotizacion_cirugia.id_tecnica", 'LEFT');
        $this->db->join($t_cotizacion_pago, "$t_cotizacion_pago.id_cotizacion=$table.id");
        $this->db->join($t_cat_tipo_pago, "$t_cat_tipo_pago.id=$t_cotizacion_pago.cat_tipo_pago");
        // $this->db->join($t_seguro, "$t_seguro.id=$table.id_seguro", 'LEFT');
        // Where
        $this->db->where($table . '.' . $field, $id);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row_array() : FALSE;
    }

    /**
     *  Provee los datos de un articulo
     *
     * @access public
     * @param integer $id Identificador del articulo
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com >
     * @version 1.0 21/10/13 11:55 AM
     */
    public function getInfoArticle($id = NULL) {

        $this->db->select('art.id,');
        $this->db->select('art.modelo as modelo,');
        $this->db->select('art.codigo as codigo,');
        $this->db->select('art.descripcion as descripcion,');
        $this->db->select('cat._label AS tipo_articulo');

        $this->db->from('business.articulo as art');
        $this->db->join('dynamic.category cat', 'art.tipo_articulo=cat.id');
        $this->db->where(array("art.id" => $id));
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row_array() : FALSE;
    }

}