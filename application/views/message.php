<?php if ($this->session->flashdata('error')): ?>
<div class="alert alert-error fade in">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <?php echo $this->session->flashdata('error'); ?>
</div>
<?php endif ?>

<?php if ($this->session->flashdata('success')): ?>
<div class="alert alert-success fade in">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <?php echo $this->session->flashdata('success'); ?>
</div>
<script>$(function(){setTimeout(function(){$('#modal_window').modal('hide')},1500);});</script>
<?php endif; ?>