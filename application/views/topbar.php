
<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">

            <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="nav-collapse collapse">
                <ul class="nav">
                    <li class="">
                        <a href="#" onclick="request({url:'home'})">Inicio</a>
                    </li>
                    <li class="">
                        <a href="#" onclick="request({url:'home/home/about'})">Acerca de</a>
                    </li>
                    <li class="">
                        <a href="<?php echo base_url() ?>home?menu=doc" target="_blank">Ayuda</a>
                    </li>
                    <?php if (isset($activeNotification) && $activeNotification): ?>
                        <li class="">
                            <a href="#" id="notification-msg" data-original-title="Notificaciones">
                                <i class="icon-flag"></i>
                                <span id="notification">
                                    <?php if (isset($cant) AND $cant != 0): ?><span id="not" class="badge badge-warning"><?php echo $cant ?></span><?php endif ?>
                                </span>
                            </a>
                        </li>
                    <?php endif; ?>
                </ul>
                <span class="navbar-form pull-right" >
                    <div class="btn-group">
                        <span class="btn btn-info"><i class="icon-user icon-white"></i> <?php echo $this->session->userdata('username') ?></span>

                        <a class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>

                        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">

                            <li>
                                <a tabindex="-1" title="Perfil" href="#"onclick="request({url:'auth/users/user_profile'})"><?php echo $this->session->userdata('user_first_name') . ' ' . $this->session->userdata('user_last_name'); ?>
                                    <br/><strong><?php echo $this->session->userdata('role_name'); ?></strong>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li><a tabindex="-1" title="Cerrar Sesión" href="<?php base_url() ?>auth/logout"> Cerrar Sesión</a></li>
                        </ul>
                    </div>
                </span>

            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        var notificationMouseOver=false;
        $('#notification-msg').popover({
            placement: 'bottom',
            trigger: 'click',
            html: true,
            content: '<?php echo $msg ?>'
        }).hover(
        function(){
            notificationMouseOver = true;
        },
        function(){
            notificationMouseOver = false;
        }
    );


        $('body').click(function() {
            if(!notificationMouseOver)
                $('#notification-msg').popover('hide');
        });

    });

</script>
