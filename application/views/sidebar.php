<div class="accordion" id="accordion2">
    <?php $menus = $this->session->userdata('sidebar') ?>
    <?php if ($menus): ?>

        <?php foreach ($menus as $key => $menu): ?>
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" data-target="#menu<?php echo $key; ?>">
                        <b><?php echo $menu['label'] ?></b>
                    </a>
                </div>
                <?php if (isset($menu['submenu'])): ?>
                    <?php $submenus = $menu['submenu'] ?>
                    <div id="menu<?php echo $key ?>" class="accordion-body collapse">
                        <?php foreach ($submenus as $k => $submenu): ?>
                            <div class="accordion-inner">
                                <li>
                                    <a href="#" onclick="request({url:'<?= $submenu['url'] ?>'})"><?php echo $submenu['label'] ?></a>
                                </li>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
