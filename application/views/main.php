<!DOCTYPE html>
<html>
    <head>
        <title>Conmed</title>
        <?php echo meta(array('name' => 'Content-type', 'content' => 'text/html; charset=utf-8', 'type' => 'equiv')) ?>
        <link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/favicon.ico" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/DT_bootstrap.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/validation.css" >
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/chosen.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/datepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/bootstrap-timepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/bootstrap-formhelpers.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/bootstrap-modal.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/bootstrap-checkbox.css" />
        <link rel='stylesheet' type='text/css' href='<?php echo base_url() ?>assets/css/fullcalendar/fullcalendar.css' />
        <link rel='stylesheet' type='text/css' href='<?php echo base_url() ?>assets/css/fullcalendar/fullcalendar.print.css' media='print' />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/jquery.tree.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/jquery-ui-1.10.1.css"/>
        <link href="<?php echo base_url() ?>assets/css/sm-core-css.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/css/sm-simple/sm-simple.css" rel="stylesheet" type="text/css" />

<!--        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>-->

       
        <script src="<?php echo base_url() ?>assets/js/min/jquery-1.10.2.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/min/jquery-ui-1.10.1.custom.min.js"></script>
        
        <script>var intervals = Array();</script>
    </head>
    <body>
        <?php
        $data_toolbar['activeNotification'] = $activeNotification;
        if (isset($topBarView)):
            echo $this->load->view($topBarView, $data_toolbar);
        endif;
        ?>
        <div class="container content">
            <div class="row">
                <div class="span2">
                    <?php
                    if (isset($menu)):
                        echo $menu;
                    endif;
                    ?>
                </div>
                <div style="margin-left:5%;" id='center-content' class="span9">
                    <?php
                    echo $this->load->view('message');
                    if (isset($content)):
                        echo $content;
                    endif;
                    ?>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div id="modal_window" class="modal hide fade" data-width="760" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="myModalLabel">Modal header</h3>
            </div>
            <div class="m-body"></div>
        </div>
        <!-- Modal -->
        <div id="modal_window2" class="modal hide fade" data-width="760" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="myModalLabel">Modal header</h3>
            </div>
            <div class="m-body"></div>
        </div>
        <!-- Modal de listado de articulos -->
        <div id='container-articles' class="modal hide fade" tabindex="-5" role="dialog" aria-labelledby="modal-articles" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="myModalLabel">Listado de Articulos</h3>
            </div>
            <div class="modal-body row-fluid">
                <div id='list-articles' class="span9"><table class="js-table-articulos table table-striped table-bordered"></table></div>
                <div id='detail-article' class="span3 hide">
                    <div class='row-fluid'>
                        <br class='span9'/>
                        <div id='article-model' class='span9 control-group'>
                            <label class='control-label'><strong>Modelo</strong></label>
                            <div class='controls'></div>
                        </div>
                        <div id='article-description' class='span9 control-group'>
                            <label class='control-label'><strong>Descripción</strong></label>
                            <div class='controls'></div>
                        </div>
                        <div id='article-image' class='span9 control-group'>
                            <label class='control-label'><strong>Imagen</strong></label>
                            <div class='controls'>
                                <img src=""/>
                            </div>
                        </div>
                        <div id='article-prices' class='span9 control-group'>
                            <label class='control-label'><strong>Precios</strong></label>
                            <div class='controls'>
                                <table border='1'><thead><tr><th>Nombre</th><th>Precio</th></tr></thead><tbody></tbody></table>
                            </div>
                        </div>
                        <div id='article-stock' class='span9 control-group'>
                            <label class='control-label'><strong>Existencia</strong></label>
                            <div class='controls'>
                                <table border='1'><thead><tr><th>Almacen</th><th>Cantidad</th></tr></thead><tbody></tbody></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal de instrumentistas -->
        <div id='container-instrumentalist' class="modal hide fade" tabindex="-5" role="dialog" aria-labelledby="modal-instrumentalist" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="myModalLabel">Listado de Instrumentistas</h3>
            </div>
            <div class="modal-body row-fluid" >
                <div style="width: 68%;height: 100%;margin-right:15px;float: right;">
                    <ul class="nav nav-tabs" id="myTab_instrumentalist">
                        <li class="active"><a href="#instrumentalist-1" onclick="$('#myTab_instrumentalist li:eq(0) a').tab('show');">Cirugias Pendientes</a></li>
                        <li><a href="#instrumentalist-2" onclick="$('#myTab_instrumentalist li:eq(1) a').tab('show');">Estadistica de Trabajo</a></li>
                        <button disabled="disabled" id="add-instrumentalist" style="float:right;margin-right:15px;" class="hide btn btn-success">Agregar a Cirugia</button>
                    </ul>
                    <div class="tab-content" style="margin-right:15px;">
                        <div class="tab-pane active" id="instrumentalist-1"><center>Elija un Instrumentista en el menu a su izquierda para ver detalles.</center></div>
                        <div class="tab-pane" id="instrumentalist-2"><center>Elija un Instrumentista en el menu a su izquierda para ver detalles.</center></div>
                    </div>
                </div>
                <div class="well row-fluid" style="padding: 8px 0;width: 28%;height: 100%;margin-left: 0;overflow: auto">
                    <div class="alert alert-info">INSTRUMENTISTAS</div>
                    <ul id="nav_list-instrumentalist"class="nav nav-list"></ul>
                </div>
            </div>
        </div>

        <div id="fileModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="fileModalLabel"></h3>
            </div>
            <div class="m-body">
                <table align="center">
                    <tr>
                        <td><img id='selected-file-image' width="100%" height="100%"/></td>
                    </tr>
                </table>
                <br/>
            </div>
        </div>

        <!-- Modal Imagen de Articulos -->
        <div id="articleModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="articleModalLabel"></h3>
            </div>
            <div class="m-body">
                <table align="center">
                    <tr>
                        <td><img id='selected-article-image' width="100%" height="100%"/></td>
                        <td>
                            <label><strong>Descripción</strong></label>
                            <span id="selected-article-description"></span>
                        </td>
                    </tr>
                </table>
                <br/>
            </div>
        </div>
        <!-- Modal Imagen de Ayuda -->
        <div id="helpModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="helpModalLabel"></h3>
            </div>
            <div class="m-body">
                <table align="center">
                    <tr>
                        <td><img id='selected-help-image' width="100%" height="100%"/></td>
<!--                        <td >
                            <label><strong>Descripción</strong></label>
                            <span id="selected-help-description"></span>
                        </td>-->
                    </tr>
                </table>
                <br/>
            </div>
        </div>
        <div id="modal_" ></div>
        <script src="<?php echo base_url() ?>assets/js/min/accounting.min.js" async></script>
        <script src="<?php echo base_url() ?>assets/js/min/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/min/bootbox.min.js" async></script>
        <script src="<?php echo base_url() ?>assets/js/min/bootstrap-formhelpers-phone.format.min.js" async></script>
        <script src="<?php echo base_url() ?>assets/js/min/bootstrap-formhelpers-phone.min.js" async></script>
        <script src="<?php echo base_url() ?>assets/js/bootstrap-timepicker.js" async></script>
        <script src="<?php echo base_url() ?>assets/js/min/bootstrap-modal.min.js" async></script>
        <script src="<?php echo base_url() ?>assets/js/min/bootstrap-modalmanager.min.js" async></script>
        <script src="<?php echo base_url() ?>assets/js/min/bootstrap-typeahead.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/min/chosen.ajaxaddition.jquery.min.js" async></script>
        <script src="<?php echo base_url() ?>assets/js/min/chosen.jquery.min.js" async></script>
        <script src="<?php echo base_url() ?>assets/js/min/fullcalendar.min.js" async></script>
        <script src="<?php echo base_url() ?>assets/js/min/jquery.chained.min.js" async></script>
        <script src="<?php echo base_url() ?>assets/js/min/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/min/jquery.mask.min.js" async></script>
        <script src="<?php echo base_url() ?>assets/js/min/validate.form.min.js" async></script>
        <script src="<?php echo base_url() ?>assets/js/min/validate.min.js" async></script>
        <!--<script src="<?php echo base_url() ?>assets/js/min/view_engine.min.js"></script>-->
        <script src="<?php echo base_url() ?>assets/js/view_engine.js"></script>
        <script src="<?php echo base_url() ?>assets/js/bootstrap-datepicker.js" async></script>
        <script src="<?php echo base_url() ?>assets/js/bootstrap-datepicker.es.js" async></script>
        <script src="<?php echo base_url() ?>assets/js/min/DT_bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/articulos.js" async></script>
        <script src="<?php echo base_url() ?>assets/js/ajaxfileupload.js" async></script>
        <script src="<?php echo base_url() ?>assets/js/min/highcharts.min.js" async></script>
        <script src="<?php echo base_url() ?>assets/js/bootstrap-checkbox.js" async></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.dataTables.rowGrouping.js" async></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.tree.js" async></script>

        <!--SmartMenus jQuery plugin-->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.smartmenus.js"></script>

        <!--SmartMenus jQuery init-->
        <script type="text/javascript">
            $(function() {
                $('#main-menu-doc').smartmenus({
                    subMenusSubOffsetX: 1,
                    subMenusSubOffsetY: -8
                });
            });
            var BASE_URL = '<?php echo base_url(); ?>';
            var IVA = '<?php echo ($this->config->item('valor_iva')); ?>';
            var PATH_IMG_ARTICLES = '<?php echo $this->config->item('path_img_instrumentos') . "/"; ?>';
            var PATH_IMG_NO_FOUND = '<?php echo $this->config->item('path_image_no_found'); ?>';
            var PATH_FILE = '<?php echo $this->config->item('uploads') . '/'; ?>';
            getUserAgent();
<?php if ($activeNotification): ?>
        setTimeout("notification()", 10000);
<?php endif; ?>
        </script>
    </body>
</html>