<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Rest Conmed
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		cbello
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Conmed extends REST_Controller
{
	/**
	 * resive las cotizaciones procesadas por el consumidor
	 * las procesadas les cambia el status
	 * y las que vengan con fallas se agregan a una tabla para llevar el contro y se envia un email sobre la falla
	 * 
	 */
	public function cotizaciones_agregadas_post()
    {
    	$this->load->model(array('RCotizacion_model'));
    	$this->load->helper('ast_helper');
    	
    	//cotizaciones resividas por el consumidor
    	$cotizaciones = json_decode($this->post('cotizaciones'));
    	
    	//Registro el evento
    	writeLastUpdated(time());
    	$str = var_export($cotizaciones, true);
    	try{
	    	foreach($cotizaciones as $cotizacion)
	    	{
	    		if(is_null($cotizacion->error_code))
	    		{
	    			//Agrego el sattus
	    			$this->RCotizacion_model->addProfitId($cotizacion->id_cotizacion, $cotizacion->id_cotizacion_nueva);
	    		}
	    		else
	    		{
	    			//Se verifica si tiene activa el envio de email
	    			if($this->config->item('debug_email'))
	    			{
	    				 
	    				//Envio de email de error
		    			sendEmailError(
		    				array('cbello@ast.com.ve'),
		    				$this->load->view('email_error_rest',
		    					array(
		    						'id_cotizacion' => $cotizacion->id_cotizacion,
		    						'error_code' => $cotizacion->error_code, 
		    						'error_descripcion' => $cotizacion->error_content
		    					),
		    					TRUE
		    				),
		    				sprintf('Error en cotizacion %d', $cotizacion->id_cotizacion),
		    				''
		    			); 
	    			}
	    			$this->RCotizacion_model->addErrorProfit($cotizacion->id_cotizacion, $cotizacion->error_code, $cotizacion->error_content);
	    		}
	    	}
    	}
    	catch(Exception $e)
    	{
    		throw new Exception($e->getMessage());
    	}
    	$this->response($str, 200);	
    }
    
    
    /**
     * funciona que revisa si el status del del servicio esta detenido
     *
     */
    public function is_stop_get()
    {
    	$this->load->helper('ast_helper');
    	//Registro el evento
    	writeLastUpdated(time());
    	$stop = 1;
    	$this->response($stop, 200);
    }
    
    protected function _validation_login() {
    	if($this->input->get('identity') && $this->input->get('password')) {
    		$identity = trim($this->input->get('identity'));
    		$password = trim($this->input->get('password'));
    		if($identity != '' && $password != '')
    		{
    			return true;
    		}
    	}
    	else
    	{
    		return false;
    	}
    }
    
    function login_get() {
    	$this->load->library('ion_auth');
    	$this->load->library('acl');
    	//URL demo OJO
    	$url_demo = 'http://192.168.1.108/';
    	
    	//-----------------------------
    	//Seteamos los datos de auditoria
    	$operation_data = NULL;
    	if ($remember = (bool) $this->input->get('remember'))
    		$operation_data['remember'] = $remember;
    	if ($identity = $this->input->get('identity'))
    		$operation_data['identity'] = $identity;
    	//-----------------------------
    	// si el usuario esta en session lo redirecciona
    	
    
    	$this->data['title'] = "Login";
    
    	
    
    	if ($this->_validation_login() == true) {
    		
    		
    		//$remember = (bool) $this->input->get('remember');
    		$remember = true;
    		$user = $this->ion_auth->login($this->input->get('identity'), $this->input->get('password'), $remember);
    
    		//-----------------------------------
    		//Registramos la auditoria
    		$this->audit->setOperationData($operation_data);
    		$this->audit->register();
    		//-----------------------------------
    		//if the login is successful
    		if ($user) {
    			
    			if($this->get('jsoncallback'))
    				echo $this->get('jsoncallback')."(".json_encode(
    						array(
    							'code' => 100, 
    							'id_user' => $user['user_id'], 
    							'url_access' => $url_demo
    								
    			 			)).")";
    			else
    				$this->response(json_encode(
    						array(
    							'code' => 100, 
    							'id_user' => $user['user_id'], 
    							'url_access' => $url_demo
    								
    			 			)), 200);
    			return;
    		} else {
    			//if the login was un-successful
    			if($this->get('jsoncallback'))
    				echo $this->get('jsoncallback')."(".json_encode(array('code' => 101)).")";
    			else
    				$this->response(json_encode(array('code' => 101)), 200);
    		}
    	} else {
    		//$this->data['message'] = $operation_data['errors'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
    
    		$this->data['identity'] = array('name' => 'identity',
    				'id' => 'identity',
    				'type' => 'text',
    				'value' => $this->form_validation->set_value('identity'),
    				'required' => true,
    				'class' => 'input-xlarge'
    		);
    		$this->data['password'] = array('name' => 'password',
    				'id' => 'password',
    				'type' => 'password',
    				'required' => true,
    				'class' => 'input-xlarge'
    		);
    		$this->data['content'] = 'login';
    		//Registramos la auditoria
    		if (empty($operation_data['errors']))
    			unset($operation_data['errors']);
    		$this->audit->setOperationData($operation_data);
    		//-----------------------------------
    		
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."(".json_encode($this->data).")";
    		else
    			$this->response($this->data, 200);
    		//-----------------------------------
    		
    	}
    }
    
    /**
     * servicio rest que retorna las cotizaciones
     */
    function create_get()
    {
    	
    	$this->load->library('ion_auth');
    	$this->load->helper('ast');
    	if($this->ion_auth->logged_in())
    	{
    		$this->load->model('cotizacion/Cotizacion_model', 'Cotizacion_model');
	        $this->load->model('cotizacion/validacion_model', 'validacion_model');
	        $this->load->model('persona/empleado_model', 'empleado_model');
	        $this->load->model('empresa/empresa_model', 'empresa_model');
	        $this->load->model('persona/paciente_model', 'paciente_model');
	        $this->load->model('engine/engine_model', 'engine_model');
	        
	        $data_temp = $_GET;
	    	$data_temp = $this->procesarData($data_temp);
	    	$data_temp = $this->_avancedFormat('INSERT', $data_temp);
	    	$data = $data_temp;
	    	$getMaterialesPdf = $this->Cotizacion_model->getMaterialesPdf($data['cotizacion_material']);
	    	$getDatosPdf = $this->Cotizacion_model->getDatosPdf($data['cotizacion']['id_medico'], $data['cotizacion']['id_seguro'], $data['cotizacion']['id_paciente'], $data['cotizacion_pago']['cat_tipo_pago'], $data['cotizacion']['id_vendedor']);
	    	$datos_paciente = $this->paciente_model->getPaciente($data['cotizacion']['id_paciente'], 'tlf_movil');
	    	
	    	foreach ($getMaterialesPdf as $clave => $valor):
	    	
	    	$materiales[] = array(
	    			'codigo_m' => $valor[0]['id'],
	    			'modelo_m' => $valor[0]['modelo'],
	    			'descripcion_m' => $valor[0]['descripcion'],
	    			'gart_m' => '',
	    			'cant_m' => $valor[0]['cantidad'],
	    			'precio_m' => number_format($valor[0]['precio'], 2, ',', '.'),
	    			'neto_m' => number_format($valor[0]['precio'] * $valor[0]['cantidad'], 2, ',', '.')
	    	);
	    	endforeach;
	    	$params_pdf = array(
	    			'nro_cotizacion' => $data['cotizacion']['codigo'],
	    			'fecha_emision' => date('d/m/Y'),
	    			'forma_pago' => $getDatosPdf['tipo_pago'],
	    			'tiempo_entrega' => 'INMEDIATA',
	    			'vendedor' => $getDatosPdf['vendedor'],
	    			'dr_tratante' => $getDatosPdf['medico'],
	    			'cliente' => $getDatosPdf['pnombre'],
	    			'rif' => $getDatosPdf['pcedula'],
	    			'nit' => '',
	    			'seguro' => ($getDatosPdf['nseguro'] == null ? 'No Aplica' : $getDatosPdf['nseguro']),
	    			'tlf' => (empty($datos_paciente[0]['tlf_movil']) || $datos_paciente[0]['tlf_movil'] == null ? 'Sin Informacion' : $datos_paciente[0]['tlf_movil']),
	    			'monto_exento' => number_format($data['cotizacion']['monto_exento'], 2, ',', '.'),
	    			'monto_gravable' => number_format($data['cotizacion']['monto_gravable'], 2, ',', '.'),
	    			'porcentaje_descuento' => $data['cotizacion']['descuento'],
	    			'calculo_descuento' => number_format(($data['cotizacion']['monto_gravable'] * $data['cotizacion']['descuento']) / 100, 2, ',', '.'),
	    			'recargo' => number_format(($data['cotizacion']['monto_gravable'] * $data['cotizacion']['recargo']) / 100, 2, ',', '.'),
	    			'p_recargo' => $data['cotizacion']['recargo'],
	    			'porcentaje_iva' => $this->config->item('valor_iva'),
	    			'calculo_iva' => number_format(($data['cotizacion']['monto_gravable'] * $this->config->item('valor_iva')) / 100, 2, ',', '.'),
	    			'neto' => number_format($data['cotizacion']['monto_total'], 2, ',', '.'),
	    			'materiales' => $materiales
	    	);
	    	$this->EXC_create_pdf_cotizacion($params_pdf, FALSE, TRUE);
	    	$success = $this->Cotizacion_model->insert($data_temp);
	    	if($success)
	    	{
	    		if($this->get('jsoncallback'))
	    			echo $this->get('jsoncallback')."(".json_encode(array('code' => 300)).")";
	    		else
	    			$this->response(json_encode(array('code' => 300)), 200);
	    	}
	    	else
	    	{
	    		$this->response(array('error' => 'Couldn\'t find any users!'), 404);
	    	}
    	}
    	else
    	{
    		$error_data = array('code' => 301);
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."(".json_encode($error_data).")";
    		else
    			$this->response(json_encode($error_data), 200);
    	}
    }
    function create_libre_get()
    {
    	 
    	$this->load->library('ion_auth');
    	$this->load->helper('ast');
    	if($this->ion_auth->logged_in())
    	{
    		
    		$this->load->model('cotizacion/Cotizacion_free_model', 'Cotizacion_free_model');
    		$this->load->model('cotizacion/validacion_model', 'validacion_model');
    		$this->load->model('persona/empleado_model', 'empleado_model');
    		$this->load->model('empresa/empresa_model', 'empresa_model');

    		$this->load->model('engine/engine_model', 'engine_model');
    		 
    		$data_temp = $_GET;
    		$data_temp = $this->procesarDataLibre($data_temp);
    		$data_temp = $this->_avancedFormatLibre('INSERT', $data_temp);
    		$data = $data_temp;
    		
    		$getMaterialesPdf = $this->Cotizacion_free_model->getMaterialesPdf($data['cotizacion_articulo']);
    		
    		$getDatosPdf = $this->Cotizacion_free_model->getDatosPdf(0, $data['cotizacion_art']['id_vendedor'], $data['cotizacion_art']['id_cliente']);
    		
    		foreach ($getMaterialesPdf as $clave => $valor):
	    		$materiales[] = array(
	    				'codigo_m' => $valor[0]['id'],
	    				'modelo_m' => $valor[0]['modelo'],
	    				'descripcion_m' => $valor[0]['descripcion'],
	    				'gart_m' => '',
	    				'cant_m' => $valor[0]['cantidad'],
	    				'precio_m' => number_format($valor[0]['precio'], 2, ',', '.'),
	    				'neto_m' => number_format($valor[0]['precio'] * $valor[0]['cantidad'], 2, ',', '.')
	    		);
    		endforeach;
    		
    		
    		$params_pdf = array(
    				'nro_cotizacion' => $data['cotizacion_art']['codigo'],
    				'fecha_emision' => date('d/m/Y'),
    				'forma_pago' => $getDatosPdf['tipo_pago'],
    				'tiempo_entrega' => 'INMEDIATA',
    				'vendedor' => $getDatosPdf['vendedor'],
    				'cliente' => $getDatosPdf['cliente']['nombre_cliente'],
    				'rif' => $getDatosPdf['cliente']['cedula_rif'],
    				'nit' => '',
    				//'tlf' => (empty($datos_paciente[0]['tlf_movil']) || $datos_paciente[0]['tlf_movil'] == null ? 'Sin Informacion' : $datos_paciente[0]['tlf_movil']),
    				'monto_exento' => number_format($data['cotizacion_art']['monto_exento'], 2, ',', '.'),
    				'monto_gravable' => number_format($data['cotizacion_art']['monto_gravable'], 2, ',', '.'),
    				'porcentaje_descuento' => $data['cotizacion_art']['descuento'],
    				'calculo_descuento' => number_format(($data['cotizacion_art']['monto_gravable'] * $data['cotizacion_art']['descuento']) / 100, 2, ',', '.'),
    				'recargo' => number_format(($data['cotizacion_art']['monto_gravable'] * $data['cotizacion_art']['recargo']) / 100, 2, ',', '.'),
    				'p_recargo' => $data['cotizacion_art']['recargo'],
    				'porcentaje_iva' => $this->config->item('valor_iva'),
    				'calculo_iva' => number_format(($data['cotizacion_art']['monto_gravable'] * $this->config->item('valor_iva')) / 100, 2, ',', '.'),
    				'neto' => number_format($data['cotizacion_art']['monto_total'], 2, ',', '.'),
    				'materiales' => $materiales
    		);
    		
    		$this->EXC_create_pdf_cotizacion($params_pdf, FALSE, TRUE);
    		
    		$success = $this->Cotizacion_free_model->insert($data);
    		if($success)
    		{
    			if($this->get('jsoncallback'))
    				echo $this->get('jsoncallback')."(".json_encode(array('code' => 300)).")";
    			else
    				$this->response(json_encode(array('code' => 300)), 200);
    		}
    		else
    		{
    			$this->response(array('error' => 'Couldn\'t find any users!'), 404);
    		}
    		
    	}
    	else
    	{
    		$error_data = array('code' => 301);
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."(".json_encode($error_data).")";
    		else
    			$this->response(json_encode($error_data), 200);
    	}
    }
    /**
     * servicio rest que retorna las cotizaciones
     */
    function cotizaciones_get()
    {
    	$this->load->model(array('RCotizacion_model'));
    	$this->load->helper('ast_helper');
    	//Registro el evento
    	writeLastUpdated(time());
    	//Busca las cotizaciones
    	$cotizaciones = $this->RCotizacion_model->getCotizaciones();
        	
        if($cotizaciones)
        {
        	$this->response($cotizaciones, 200);
        }
        else
        {
            $this->response(array('error' => 'Couldn\'t find any users!'), 404);
        }
    }	
    function cotizacionesByVendedor_get()
    {
    	$this->load->library('ion_auth');
    	$this->load->model(array('RCotizacion_model', 'persona/empleado_model'));
    	$this->load->helper('ast_helper');
    	//Busca las cotizaciones
    	$id = $this->empleado_model->getIdByUser($this->session->userdata('user_id'));
    	$today_update = $date = date("Y-m-d H:i:s");
    	$fecha_inicial = $this->get("last_update");
    	//echo $fecha_inicial;
    	$fecha_inicial = $fecha_inicial == '' ? null : $fecha_inicial;
    	$cotizaciones = $this->RCotizacion_model->getCotizacionesByVendedor($id, $fecha_inicial);
    
    	if($cotizaciones)
    	{
    		$cotizaciones = array('datetime_update' => $today_update, 'cotizaciones' => $cotizaciones);
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."(".json_encode($cotizaciones).")";
    		else
    			$this->response($cotizaciones, 200);
    
    	}
    	else
    	{
    		$cotizaciones = array('datetime_update' => $today_update, 'cotizaciones' => array());
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."(".json_encode($cotizaciones).")";
    		else
    			$this->response($cotizaciones, 200);
    	}
    }
    function cotizacionesLibreByVendedor_get()
    {
    	$this->load->library('ion_auth');
    	$this->load->model(array('RCotizacion_model', 'persona/empleado_model'));
    	$this->load->helper('ast_helper');
    	//Busca las cotizaciones
    	$id = $this->empleado_model->getIdByUser($this->session->userdata('user_id'));
    	$today_update = $date = date("Y-m-d H:i:s");
    	$fecha_inicial = $this->get("last_update");
    	//echo $fecha_inicial;
    	$fecha_inicial = $fecha_inicial == '' ? null : $fecha_inicial;
    	$cotizaciones = $this->RCotizacion_model->getCotizacionesLibreByVendedor($id, $fecha_inicial);
    	 
    	if($cotizaciones)
    	{
    		$cotizaciones = array('datetime_update' => $today_update, 'cotizaciones' => $cotizaciones);
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."(".json_encode($cotizaciones).")";
    		else
    			$this->response($cotizaciones, 200);
    		
    	}
    	else
    	{
    		$cotizaciones = array('datetime_update' => $today_update, 'cotizaciones' => array());
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."(".json_encode($cotizaciones).")";
    		else
    			$this->response($cotizaciones, 200);
    	}
    }
    
    /**
     * servicio rest que retorna las cotizaciones
     */
    function pacientes_get()
    {
    	$this->load->model(array('RPaciente_model'));
    	$this->load->helper('ast_helper');
    	//Registro el evento
    	writeLastUpdated(time());
    	//Busca las cotizaciones
    	$pacientes = $this->RPaciente_model->getPacientes();
    	 
    	if($pacientes)
    	{
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."(".json_encode($pacientes).")";
    		else
    			$this->response($pacientes, 200);
    	}
    	else
    	{
    		$this->response(array('error' => 'Couldn\'t find any users!'), 404);
    	}
    }
    
    /**
     * servicio rest que retorna las cotizaciones
     */
    function medicos_get()
    {
    	$this->load->model(array('RMedico_model'));
    	$this->load->helper('ast_helper');
    	//Registro el evento
    	writeLastUpdated(time());
    	//Busca las cotizaciones
    	$medicos = $this->RMedico_model->getMedicos();
    
    	if($medicos)
    	{
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."(".json_encode($medicos).")";
    		else
    			$this->response($medicos, 200);
    	}
    	else
    	{
    		$this->response(array('error' => 'Couldn\'t find any users!'), 404);
    	}
    }
    
    function direccion_medicos_get()
    {
    	
    	$this->load->model(array('persona/medico_model'));
    	
    	$direccion_medicos = $this->medico_model->searchDirections();
    	if($direccion_medicos)
    	{
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."(".json_encode($direccion_medicos).")";
    		else
    			$this->response($direccion_medicos, 200);
    	}
    	else
    	{
    		$this->response(array('error' => 'Couldn\'t find any users!'), 404);
    	}
    }
    
    function emisor_pagos_get()
    {
    	$this->load->model(array('settings/categories_model'));
    	    	 
    	$emisor_pagos = $this->categories_model->getCategory(array('_table' => 'pagador'), 'id,_label');
    	if($emisor_pagos)
    	{
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."(".json_encode($emisor_pagos).")";
    		else
    			$this->response($emisor_pagos, 200);
    	}
    	else
    	{
    		$this->response(array('error' => 'Couldn\'t find any users!'), 404);
    	}
    }
    
    function terceros_get()
    {
    	$this->load->model(array('settings/categories_model'));
    	 
    	$terceros = $this->categories_model->getCategory(array('_table' => 'letra'), 'id,_label');
    	if($terceros)
    	{
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."(".json_encode($terceros).")";
    		else
    			$this->response($terceros, 200);
    	}
    	else
    	{
    		$this->response(array('error' => 'Couldn\'t find any users!'), 404);
    	}
    }
    
    function localidades_get()
    {
    	 
    	$this->load->model('RConmed_model');
    	$localidades = $this->RConmed_model->getLocalidades(array(), '*');
    
    	if($localidades)
    	{
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."(".json_encode($localidades).")";
    		else
    			$this->response($localidades, 200);
    	}
    	else
    	{
    		$this->response(array('error' => 'Couldn\'t find any users!'), 404);
    	}
    }
    
    function create_paciente_get()
    {
    	//print_r($_GET);
    	//$this->db->insert('business.paciente', );
    	$get = $_GET;
    	$names = array();
    	$values = array();
    	unset($get['_']);
    	unset($get['jsoncallback']);
    	foreach($get as $name => $value)
    	{
    		//echo $name;
    		$names[] = $name;
    		$values[] = is_numeric($value) ? $value : "'$value'";
    	}
    	
    	$names[] = "created_by";
    	$values[] = 1;
    	$keys_array = array_keys($names);
    	$keys = join(',', $names);
    	//$values_array = array_values($data);
    	$values = join(',', $values);
    	
    	
    	$insert = "INSERT INTO business.paciente ($keys) values ($values) returning *";
    	
    	$query = $this->db->query($insert);
    	$result = $this->db->affected_rows() ? $query->row_array() : FALSE;
    	
    	$this->load->model('RConmed_model');
    	//$localidades = $this->RConmed_model->getLocalidades(array(), '*');
    
    	if($result)
    	{
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."(".json_encode(array('code' => 300, 'id' => $result['id'])).")";
    		else
    			$this->response(array('code' => 301), 200);
    	}
    	else
    	{
    		$this->response(array('error' => 'Couldn\'t find any users!'), 404);
    	}
    }
    
    function articulos_get()
    {
    	
    	$this->load->model('RConmed_model');
    	$articulos = $this->RConmed_model->getArticulos(array(), 'id, codigo,modelo,descripcion, tipo_articulo');
		
		if($articulos)
    	{
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."(".json_encode($articulos).")";
    		else
    			$this->response($articulos, 200);
    	}
    	else
    	{
    		$this->response(array('error' => 'Couldn\'t find any users!'), 404);
    	}
    }
    
    function sincronizar_get()
    {
    	 
    	$this->load->model(
    		array(
    			'RPaciente_model', 
    			'RConmed_model', 
    			'cotizacion/Cotizacion_model',
    			'empresa/seguro_model',
    			'RMedico_model',
    			'engine/engine_model',
    			'cotizacion/articulo_model'
    		)
    	);
    	$today_update = $date = date("Y-m-d H:i:s");
    	$ultima_s = $this->get('last_update');
    	$ultima_t = strtotime($ultima_s);
    	$where = array('modified_at >=' => "'$ultima_s'");    	
    	$articulos_full = $this->articulo_model->articuloRecomendacion(false, false);
    	
    	$empresas = $this->engine_model->getComboByTable('id_empresa', 'nombre', false, $label = 'nombre', $where);
    	if ($empresas) foreach ($empresas as $i => $empresa) {
    		$empresa_create_time = strtotime($empresa['created_at']);
    		if($empresa['created_at'] == $empresa['modified_at'])
    		{
    			$empresa['is_new'] = 1;
    			$empresas[$i] = $empresa;
    		}
    		else if($empresa_create_time > $ultima_t)
    		{
    			$empresa['is_new'] = 1;
    			$empresas[$i] = $empresa;
    		}
    		else {
    			$empresa['is_new'] = 0;
    			$empresas[$i] = $empresa;
    		}
    	}
    	
    	$personas = $this->engine_model->getComboByTable('id_persona', 'cedula, primer_nombre,primer_apellido', false, $label = 'nombre', $where);
    	if ($personas) foreach ($personas as $i => $persona) {
    		$persona_create_time = strtotime($persona['created_at']);
    		if($persona['created_at'] == $persona['modified_at'])
    		{
    			$persona['is_new'] = 1;
    			$personas[$i] = $persona;
    		}
    		else if($persona_create_time > $ultima_t)
    		{
    			$persona['is_new'] = 1;
    			$personas[$i] = $persona;
    		}
    		else {
    			$persona['is_new'] = 0;
    			$personas[$i] = $persona;
    		}
    	}
    	
    	$aseguradoras = $this->seguro_model->getAseguradora($where, '*');
    	
    	if ($aseguradoras) foreach ($aseguradoras as $i => $aseguradora) {
    		$aseguradora_create_time = strtotime($aseguradora['created_at']);
    		if($aseguradora['created_at'] == $aseguradora['modified_at'])
    		{
    			$aseguradora['is_new'] = 1;
    			$aseguradoras[$i] = $aseguradora;
    		}
    		else if($aseguradora_create_time > $ultima_t)
    		{
    			$aseguradora['is_new'] = 1;
    			$aseguradoras[$i] = $aseguradora;
    		}
    		else {
    			$aseguradora['is_new'] = 0;
    			$aseguradoras[$i] = $aseguradora;
    		}
    	}
    	$cirugias = $this->Cotizacion_model->getCirugia($where, '*');
    	
    	if ($cirugias) foreach ($cirugias as $i => $cirugia) {
    		$cirugia_create_time = strtotime($cirugia['created_at']);
    		if($cirugia['created_at'] == $cirugia['modified_at'])
    		{
    			$cirugia['is_new'] = 1;
    			$cirugias[$i] = $cirugia;
    		}
    		else if($cirugia_create_time > $ultima_t)
    		{
    			$cirugia['is_new'] = 1;
    			$cirugias[$i] = $cirugia;
    		}
    		else {
    			$cirugia['is_new'] = 0;
    			$cirugias[$i] = $cirugia;
    		}
    	}
    	$pacientes = $this->RPaciente_model->getPacientes($where);
    	if ($pacientes) foreach ($pacientes as $i => $paciente) {
    		$paciente_create_time = strtotime($paciente['created_at']);
    		if($paciente['created_at'] == $paciente['modified_at'])
    		{
    			$paciente['is_new'] = 1;
    			$pacientes[$i] = $paciente;
    		}
    		else if($paciente_create_time > $ultima_t)
    		{
    			$paciente['is_new'] = 1;
    			$pacientes[$i] = $paciente;
    		}
    		else {
    			$paciente['is_new'] = 0;
    			$pacientes[$i] = $paciente;
    		}
    	}
    	$medicos = $this->RMedico_model->getMedicos($where);
    	if ($medicos) foreach ($medicos as $i => $medico) {
    		$medico_create_time = strtotime($medico['created_at']);
    		if($medico['created_at'] == $medico['modified_at'])
    		{
    			$medico['is_new'] = 1;
    			$medicos[$i] = $medico;
    		}
    		else if($medico_create_time > $ultima_t	)
    		{
    			$medico['is_new'] = 1;
    			$medicos[$i] = $medico;
    		}
    		else {
    			$medico['is_new'] = 0;
    			$medicos[$i] = $medico;
    		}
    	}
    	
    	$articulos = $this->RConmed_model->getArticulos($where, '*');
    	if ($articulos) foreach ($articulos as $i => $articulo) {
    		$articulo_create_time = strtotime($articulo['created_at']);
    		if($articulo['created_at'] == $articulo['modified_at'])
    		{
    			$articulo['is_new'] = 1;
    			$articulos[$i] = $articulo;
    		}
    		else if($articulo_create_time > $ultima_t)
    		{
    			$articulo['is_new'] = 1;
    			$articulos[$i] = $articulo;
    		}
    		else {
    			$articulo['is_new'] = 0;
    			$articulos[$i] = $articulo;
    		}
    	}
    	$data = array('datetime_update' => $today_update, 'articulos_full' => $articulos_full, 'personas' => $personas, 'empresas' => $empresas, 'aseguradoras' => $aseguradoras, 'medicos' => $medicos, 'pacientes' => $pacientes, 'cirugias' => $cirugias, 'articulos' => $articulos);
    	if($data)
    	{
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."(".json_encode($data).")";
    		else
    			$this->response($data, 200);
    	}
    	else
    	{
    		$this->response(array('error' => 'Couldn\'t find any users!'), 404);
    	}
    } 
    function articulos_full_get()
    {
    	 
    	$this->load->model('cotizacion/articulo_model');
    	$articulos = $this->articulo_model->articuloRecomendacion(false, false);
    
    	if($articulos)
    	{
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."(".json_encode($articulos).")";
    		else
    			$this->response($articulos, 200);
    	}
    	else
    	{
    		$this->response(array('error' => 'Couldn\'t find any users!'), 404);
    	}
    }
    
    function terceros2_get()
    {
    	$this->load->model('engine/engine_model');
    	
    	
    	$tipo_tercero = $this->input->get('tipo_tercero');
    	$data = $this->input->get('data');
    	
    	$value = $this->engine_model->getValueByCategory($tipo_tercero);
    	
    	$arr_values['empresas'] = $this->engine_model->getComboByTable('id_empresa', 'nombre', false, $label = 'text');
    	
    	$arr_values['personas'] = $this->engine_model->getComboByTable('id_persona', 'cedula, primer_nombre,primer_apellido', false, $label = 'text');

    	
    	if($arr_values)
    	{
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."(".json_encode($arr_values).")";
    		else
    			$this->response($arr_values, 200);
    	}
    	else
    	{
    		$this->response(array('error' => 'Couldn\'t find any users!'), 404);
    	}
    }
    
    function forma_pago_get()
    {
    	$this->load->model(array('settings/categories_model'));
    
    	$tipo_pago = $this->categories_model->getCategory(array('_table' => 'tipo_pago'), 'id,_label');
    	if($tipo_pago)
    	{
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."(".json_encode($tipo_pago).")";
    		else
    			$this->response($tipo_pago, 200);
    	}
    	else
    	{
    		$this->response(array('error' => 'Couldn\'t find any users!'), 404);
    	}
    }
    
    function seguros_get()
    {
    	$this->load->model('empresa/seguro_model');
    
    	$aseguradoras = $this->seguro_model->getAseguradora(array(), 'id,nombre');
    	if($aseguradoras)
    	{
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."(".json_encode($aseguradoras).")";
    		else
    			$this->response($aseguradoras, 200);
    	}
    	else
    	{
    		$this->response(array('error' => 'Couldn\'t find any users!'), 404);
    	}
    }
    
    function actualizacion_get()
    {
    	
    	$date = date("Y-m-d H:i:s");   
    	if($this->get('jsoncallback'))
    		echo $this->get('jsoncallback')."(".json_encode(array('actualizacion' => $date)).")";
    	else
    		$this->response($aseguradoras, 200);
    	
    	
    }
    
    function tipos_direccion_get()
    {
    	
    	$this->load->model(array('settings/categories_model'));
    	$this->load->helper('ast_helper');
    	//Registro el evento
    	writeLastUpdated(time());
    	//Busca las cotizaciones
    	$tipos_direccion =$this->categories_model->getCategory(array('_table' => 'tipo_direccion_cotizacion'), 'id,_label');
    	
    	if($tipos_direccion)
    	{
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."(".json_encode($tipos_direccion).")";
    		else
    			$this->response($tipos_direccion, 200);
    	}
    	else
    	{
    		$this->response(array('error' => 'Couldn\'t find any users!'), 404);
    	}
    }
    function precios_get()
    {
    	$this->load->model(array('RPrecio_model'));
    	$this->load->helper('ast_helper');
    	//Registro el evento
    	writeLastUpdated(time());
    	//Busca las cotizaciones
    	$precios = $this->RPrecio_model->getTiposPrecio();
    
    	if($precios)
    	{
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."(".json_encode($precios).")";
    		else
    			$this->response($precios, 200);
    	}
    	else
    	{
    		$this->response(array('error' => 'Couldn\'t find any users!'), 404);
    	}
    }
    
    
    function centros_get()
    {
    	$this->load->model(array('empresa/clinica_model'));
    	$this->load->helper('ast_helper');
    	//Registro el evento
    	writeLastUpdated(time());
    	//Busca las cotizaciones
    	$centros = $this->clinica_model->getDirectionsCentroSalud();
    
    	if($centros)
    	{
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."(".json_encode($centros).")";
    		else
    			$this->response($centros, 200);
    	}
    	else
    	{
    		$this->response(array('error' => 'Couldn\'t find any users!'), 404);
    	}
    }
    
    
    function articulaciones_get()
    {
    	$this->load->model(array('RCirugia_model'));
    	$this->load->helper('ast_helper');
    	//Registro el evento
    	writeLastUpdated(time());
    	//Busca las cotizaciones
    	$articulaciones = $this->RCirugia_model->getArticulaciones();
    
    	if($articulaciones)
    	{
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."(".json_encode($articulaciones).")";
    		else
    			$this->response($articulaciones, 200);
    	}
    	else
    	{
    		$this->response(array('error' => 'Couldn\'t find any users!'), 404);
    	}
    }
    function cirugias_get()
    {
    	$this->load->model(array('cotizacion/Cotizacion_model'));
    	
    	$cirugias = $this->Cotizacion_model->getCirugia(array(), 'id,id_parent,nombre, chk_tabla');
    	
    	if($cirugias)
    	{
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."(".json_encode($cirugias).")";
    		else
    			$this->response($cirugias, 200);
    	}
    	else
    	{
    		$this->response(array('error' => 'Couldn\'t find any users!'), 404);
    	}
    }
    
    function test_post()
    {
    	$this->load->model(array('RPrecio_model'));
    	$this->load->helper('ast_helper');
    	//Registro el evento
    	writeLastUpdated(time());
    	//Busca las cotizaciones
    	$precios = $this->RPrecio_model->getTiposPrecio();
    
    	if($precios)
    	{
    		writeLastUpdated("hola mundo");
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."("."<p>holaa</p>".")";
    			//echo "<p>holaa</p>";
    		else
    			//$this->response($precios, 200);
    			echo $this->get('jsoncallback')."("."<p>holaa</p>".")";
    	}
    	else
    	{
    		$this->response(array('error' => 'Couldn\'t find any users!'), 404);
    	}
    }
    function test_get()
    {
    	$this->load->model(array('RPrecio_model'));
    	$this->load->helper('ast_helper');
    	//Registro el evento
    	writeLastUpdated(time());
    	//Busca las cotizaciones
    	$precios = $this->RPrecio_model->getTiposPrecio();
    
    	if($precios)
    	{
    		writeLastUpdated("hola mundo");
    		if($this->get('jsoncallback'))
    			echo $this->get('jsoncallback')."(".json_encode(array('d'=>$this->get('nombre'))).")";
    		//echo "<p>holaa</p>";
    		else
    			//$this->response($precios, 200);
    			echo $this->get('jsoncallback')."(".json_encode($precios).")";
    	}
    	else
    	{
    		$this->response(array('error' => 'Couldn\'t find any users!'), 404);
    	}
    }
    
    function procesarData($data_temp)	
    {	
    	//
    	$cotizacion = array();
    	$cotizacion['fecha_cotizacion'] = $data_temp['fecha_cotizacion'];
    	$cotizacion['id_precios'] = $data_temp['id_precio'];
    	$cotizacion['tipo_direccion'] = $data_temp['id_tipo_direccion'];
    	$cotizacion['cat_tipo_pago'] = $data_temp['id_forma_pago'];
    	$cotizacion['cat_pagador'] = $data_temp['id_emisor_pago'];
    	$cotizacion['enviar'] = $data_temp['id_envio_direccion'];
    	$cotizacion['observacion'] = "";//ojo
    	if(isset($data_temp['id_tercero']))
    		$cotizacion['cat_letra'] = $data_temp['id_tipo_tercero']; 
    	if(!isset($data_temp['id_seguro']))
    		$cotizacion['id_seguro'] = null;
    	if(isset($data_temp['id_seguro']) && $data_temp['id_seguro'] == '')
    		$cotizacion['id_seguro'] = null;
    	//$cotizacion['created_by'] = 0;
    	//$monto_total = str_replace(',', '.', str_replace('.', '', $data_temp['total_price']));
    	//$monto_gravable = str_replace(',', '.', str_replace('.', '', $data_temp['monto_gravable']));
    	$cotizacion['monto_total'] = $data_temp['total_price'];
    	$cotizacion['monto_gravable'] = $data_temp['monto_gravable'];
    	$cotizacion['monto_exento'] = 0;
    	$cotizacion['descuento'] = 0;
    	$cotizacion['recargo'] = 0;
    	
    	$cotizacion['mat'] = $data_temp['mat-id'];
    	$cotizacion['cant_mat'] = $data_temp['cantidad_mat'];
    	$cotizacion['precio_mat'] = $data_temp['precios_mat'];
    	
    	if(isset($data_temp['bac-id']))
    	{
	    	$cotizacion['backup'] = $data_temp['bac-id'];
	    	$cotizacion['precio_backup'] = $data_temp['precios_bac'];
	    	$cotizacion['cant_backup'] = $data_temp['cantidad_bac'];
    	}
    	if(isset($data_temp['ins-id']))
    	{
	    	$cotizacion['instru'] = $data_temp['ins-id'];
	    	$cotizacion['cant_instru'] = $data_temp['cantidad_ins'];
	    	
    	}
    	$cotizacion['created_by'] = $this->session->userdata('user_id');
    	$cotizacion = array_merge($data_temp, $cotizacion);
    	return $cotizacion;
    	//return array('cotizacion' => $cotizacion, 'cotizacion_cirugia' => $cotizacion_cirugia, 'cotizacion_material' => $cotizacion_material, 'cotizacion_material_backup' => $cotizacion_material_backup,
    	//		'cotizacion_instrumento' => $cotizacion_instrumento, 'cotizacion_pago' => $cotizacion_pago, 'cotizacion_validacion' => $cotizacion_validacion
    	//);
    	//$id_precio = $post['id_precios'];
    }
    
    
    function procesarDataLibre	($data_temp)
    {
    	//
    	$cotizacion = array();
    	$cotizacion['fecha_cotizacion'] = $data_temp['fecha_cotizacion'];
    	$cotizacion['descuento'] = "";
    	$cotizacion['recargo'] = "";
    	//$cotizacion['id_vendedor'] = $this->empleado_model->getIdByUser($this->session->userdata('user_id'));
    	$cotizacion['id_cliente'] = $data_temp['id_cliente'];
    	$cotizacion['enviar'] = $data_temp['id_envio_direccion'];
    	//$cotizacion['codigo'] = $this->Cotizacion_free_model->generateCode();
    	$cotizacion['monto_total'] = $data_temp['total_price'];
    	$cotizacion['monto_gravable'] = $data_temp['monto_gravable'];
    	$cotizacion['monto_exento'] = 0;
    	$cotizacion['descuento'] = 0;
    	$cotizacion['recargo'] = 0;
    	$cotizacion['cat_tipo_pago'] =$data_temp['id_forma_pago'];
    	//if(isset($data_temp['id_cliente']))
    	$cotizacion['cat_letra'] = $data_temp['tipo_cliente'];
    	
    	$cotizacion['art'] = $data_temp['mat-id'];
    	$cotizacion['cant_art'] = $data_temp['cantidad_mat'];
    	$cotizacion['precio_art'] = $data_temp['precios_mat'];
    	
    	
    	
    	$cotizacion['observacion'] = "";//ojo
		$cotizacion['created_by'] = $this->empleado_model->getIdByUser($this->session->userdata('user_id'));
        $cotizacion = array_merge($data_temp, $cotizacion);
        return $cotizacion;
        
    }
    
    private function _extendsFormat($data) {
    	$data_ext = array();
    	$data_ext['created_by'] = $data['created_by'];
    	return $data_ext;
    }
    function _avancedFormat($format_type, $data) {
    
    	$format_type = strtoupper($format_type);
    	//$post = $this->input->post();
    	$post = $data;
    	$validations_cot = $this->validacion_model->getAll();
    	$role = $this->session->userdata('role_name');
    	//$role = 'Recepcionista';
    	$role = "";
    	$data['vendedor'] = $this->session->userdata('user_id');
    	$data['preview'] = false;
    	if ($format_type == 'INSERT') 
    	{	
    		return prepareDataCotizacion($post, $data, $role, $validations_cot, true);
    	}
    }
    
    function _avancedFormatLibre($format_type, $data) {
    
    	$format_type = strtoupper($format_type);
    	$post = $data;
    	$role = "";
    	if ($format_type == 'INSERT')
    	{
    		return prepareDataCotizacionFree($post, $data);
    	}
    }
    
    /**
     * Genera PDF de la Cotizacion
     *
     * @access public
     * @param array $params Parametros necesarios para Generar el PDF     *
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 10/06/13 11:37 AM
     */
    public function EXC_create_pdf_cotizacion($params = array(), $background_no_validate = FALSE, $membrete = FALSE) {
    	/**
    	 * Descripcion detallada de variables recibidas.
    	 *
    	 * Este arreglo se pasa en el arreglo $params
    	 * $materiales[]=array(
    	 *              'codigo_m'      => string,
    	 *              'modelo_m'      => string,
    	 *              'descripcion_m' => string,
    	 *              'gart_m'        => string,
    	 *              'cant_m'        => string,
    	 *              'precio_m'      => string,
    	 *              'neto_m'        => string
    	 *          );
    	 *
    	 * Arreglo principal del metodo
    	 * $params = array(
    	 *              'nro_cotizacion'  => string,
    	 *              'fecha_emision'   => string,
    	 *              'forma_pago'      => string,
    	 *              'tiempo_entrega'  => string,
    	 *              'vendedor'        => string,
    	 *              'dr_tratante'     => string,
    	 *              'cliente'         => string,
    	 *              'rif'             => string,
    	 *              'nit'             => string,
    	 *              'seguro'          => string,
    	 *              'tlf'             => string,
    	 *              'monto_exento'    => string,
    	 *              'monto_gravable'  => string,
    	 *              'descuento'       => string,
    	 *              'recargo'         => string,
    	 *              'iva'             => string,
    	 *              'neto'            => string,
    	 *              'materiales'      => array()
    	 *          );
    	 */
    	$dir_modo_impresion = ($membrete == true ? 'membrete_on/' : 'membrete_off/');
    
    	$this->lang->load('app_doc');
    	$this->lang->load('app_email');
    	$this->load->library('encrypt');
    	$this->load->helper('view_engine_helper');
    	extract($params);
    	//Cadena que contiene el string para generar el pdf
    	$html = $this->load->view('cotizacion/pdf_cotizacion', $params, TRUE);
    	// -----------------------------
    	// -----------------------------
    	//Asignacion de nombre, ruta y generacion del documento
    	$name_file = "cot_$nro_cotizacion";
    	$destination_path = 'uploads/cotizaciones/' . $dir_modo_impresion;
    	$destination_path = ($background_no_validate ? $destination_path . "/preview/" : $destination_path);
    
    	generate_pdf($name_file, $destination_path, 'F', $html, $background_no_validate, $membrete);
    	return utf8_decode("{$destination_path}" . "{$name_file}.pdf");
    	// -----------------------------
    }
    
    /**
    * servicio rest que retorna las cotizaciones que pueden ser depachadas
    */
   function cotizacionesND_get()
   {
   	$this->load->model(array('delivery/delivery_model'));
   	$this->load->helper('ast_helper');
   	//Registro el evento
   	writeLastUpdated(time());
   	//Busca las cotizaciones
   	$cotizaciones = $this->delivery_model->getComboByTable('id_cotizacion', 'business.cotizacion.codigo', false, $label = 'text');
   	
   	if($cotizaciones)
   	{
   	 if($this->get('jsoncallback'))
   	 echo $this->get('jsoncallback')."(".json_encode($cotizaciones).")";
   	 else
   	 $this->response($cotizaciones, 200);
   	}
   	else
   	{
   	 $this->response(array('error' => 'Couldn\'t find any users!'), 404);
   	}
   }
   
   /**
    * servicio rest que retorna los datos de la cotizaci��n a despachar
    */
   function datos_cotizacion_get()
   {
   	$this->load->model(array('cotizacion/Cotizacion_model'));
       $this->load->helper('ast_helper');
   	//Registro el evento
   	writeLastUpdated(time());
   	//Busca las cotizaciones
   	$data['cotizacion'] = $this->Cotizacion_model->getById($_GET['cot']);
       $data['materiales'] = $this->separateItemForQuantity($this->Cotizacion_model->getArticulosByCot($_GET['cot'], 'MAT'));
       $data['backup'] = $this->separateItemForQuantity($this->Cotizacion_model->getArticulosByCot($_GET['cot'], 'MAT', '1'));
       $data['instrumentos'] = $this->separateItemForQuantity($this->Cotizacion_model->getArticulosByCot($_GET['cot'], 'INS'));
        
   	if($data)
   	{
   	 if($this->get('jsoncallback'))
   	 echo $this->get('jsoncallback')."(".json_encode($data).")";
   	 else
   	 $this->response($data, 200);
   	}
   	else
   	{
   	 $this->response(array('error' => 'Couldn\'t find any users!'), 404);
   	}
   }
}