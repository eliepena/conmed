<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of RCotizacion_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad cotizacion
 * y sus entidades dependientes o extendidas sobre el servicio REST
 *
 * @package Modules
 * @category Model
 * @author Carlos Bello. <calink124@gmail.com>
 * @version V-1.0 29/11/12 01:26 PM
 */
class RCotizacion_model extends CI_Model 
{

    public function __construct() {
    	parent::__construct();
    	$this->_db = $this->load->database('test', TRUE);
    }
    
    function addProfitId($id, $id_profit)
    {
    	$table_cotizaciones = 'business' . '.cotizacion';
    	$data = array(
    			'profit' => $id_profit,
    	);
    	
    	$this->_db->where('id', $id);
    	$this->_db->update($table_cotizaciones, $data);
    }
    
    
    function addErrorProfit($id, $error_code, $error_descricpcion)
    {
    	$table_cotizacion_error = 'business' . '.cotizacion_error_profit';
    	
    	$data = array(
    			'id_cotizacion' => $id ,
    			'error_code' => $error_code ,
    			'error_descripcion' => $error_descricpcion ,
    			'estatus' => '1',
    			'created_at' => date("Y-m-d H:i:s"),
    			'updated_at' => date("Y-m-d H:i:s")
    	);
    	
    	$this->_db->insert($table_cotizacion_error, $data);
    }
    
    function getCotizacionesByVendedor($id, $fecha_init) {
    	 
    	//Estatus permitido para procesar cotizaciones
    	$status_allowed = 60;
    	 
    	 
    	//Tables
    	$table_cotizaciones = 'business' . '.view_cotizacion_movil';
    	$table_category = 'dynamic' . '.category';
    	$table_materiales = 'business' . '.view_cotizacion_articulo';
    	$table_cotizacion_status = 'business' . '.cotizacion_estatus';//Vista de Materiales
    
    	//Query
    	$this->_db->select('c.*');
    	//$this->_db->where('c.profit IS NULL');
    	//$this->_db->where('cep.id_cotizacion IS NULL');
    	$this->_db->where('c.id_vendedor', $id);
    	//echo $fecha_init;
    	if(!is_null($fecha_init))
    		$this->_db->where('c.fecha_creacion >', "'$fecha_init'");
    	//$this->_db->order_by("created_at", "desc");
    	
    	$this->_db->from($table_cotizaciones." c");
    	//$this->_db->join($table_cotizaciones_error." cep", 'c.id_cotizacion = cep.id_cotizacion', 'left');
    	$query = $this->_db->get();
    	$cotizaciones = $query->result_array();
    	$cotizaciones_result = array();
    	 
    	foreach($cotizaciones as $cotizacion)
    	{
    
    		//Reviso el ultimo status de la cotizacion en la bd
    		$this->_db->select("cat._label");
    		$this->_db->from($table_cotizacion_status." s");
    		$this->_db->where('s.id_cotizacion', $cotizacion['id_cotizacion']);
    		$this->_db->join($table_category." cat", 's.cat_cotizacion_estatus = cat.id');
    		$this->_db->order_by("created_at", "desc");
    		$this->_db->limit(1);
    		$query = $this->_db->get();
    		$status = $query->result_array();
    		$status_cot = $status[0]['_label'];
    		//JOSE STATUS
    		//Si la cotizacion tiene el status permitido
    		if(true)
    		{
    			/*
    			$this->_db->from($table_materiales);
    			//$this->_db->join($table_cotizaciones_error." cep", 'c.id_cotizacion = cep.id_cotizacion', 'left');
    			$this->_db->where('id_cotizacion', $cotizacion['id_cotizacion']);
    			$this->_db->where("backup = '0'");
    			$this->_db->where("_label = 'MAT'");
    	   		*/
    			//$query = $this->_db->get();
    			//$materiales = $query->result_array();
    			$materiales_cotizacion = array();
    			$precio_subtotal = 0;
    			/*
    			foreach($materiales as $material)
    			{
    				$iva_porcentaje = 12;
    				$materiales_cotizacion[] = array(
    						'cantidad' => $material['cantidad'],
    						'precio' =>  $material['precio'],
    						'modelo' =>  $material['modelo'],
    						'id_material' =>  $material['id_articulo'],
    						'descripcion' =>  $material['descripcion'],
    						'id_cotizacion' =>  $material['id_cotizacion']
    				);
    				$precio_subtotal += $material['precio'];
    
    			}
    			
    			$iva = ($precio_subtotal * $iva_porcentaje) / 100;
    			$precio_total = $precio_subtotal + $iva;
    	   
    	   		*/
    	   
    			$cotizaciones_result[]  = array(
    					//'materiales' => $materiales_cotizacion,
    					'statu_label' => $status_cot,
    					'codigo' => $cotizacion['codigo'],
    					'tecnica' => $cotizacion['tecnica'],
    					'lesion' => $cotizacion['lesion'],
    					'articulacion' => $cotizacion['articulacion'],
    					'monto_subtotal' => is_null($cotizacion['monto_gravable']) ? 0 : $cotizacion['monto_gravable'],
    					'monto_total' => $cotizacion['monto_total'],
    					//'monto_iva' => $iva,
    					'monto_exento' => $cotizacion['monto_exento'],
    					'descuento' => $cotizacion['descuento'],
    					'fecha_creacion' => $cotizacion['fecha_creacion'],
    					'empresa_direccion' => $cotizacion['empresa_direccion'],
    					'empresa_nombre' => $cotizacion['empresa_nombre'],
    					'empresa_rif' => $cotizacion['empresa_rif'],
    					'seguro_direccion' => $cotizacion['seguro_direccion'],
    					'seguro_nombre' => $cotizacion['seguro_nombre'],
    					'seguro_rif' => $cotizacion['seguro_rif'],
    					'id_medico' => $cotizacion['id_medico'],
    					'id_paciente' => $cotizacion['id_paciente'],
    					'id_vendedor' => $cotizacion['id_vendedor'],
    					'id_cotizacion' => $cotizacion['id_cotizacion'],
    					'vendedor_cedula' => $cotizacion['vendedor_cedula'],
    					'paciente_cedula' => $cotizacion['paciente_cedula'],
    					'paciente_primer_nombre' => $cotizacion['paciente_primer_nombre'],
    					'paciente_primer_apellido' => $cotizacion['paciente_primer_apellido'],
    					'medico_cedula' => $cotizacion['medico_cedula'],
    					'medico_primer_nombre' => $cotizacion['medico_primer_nombre'],
    					'medico_primer_apellido' => $cotizacion['medico_primer_apellido'],
    					'vendedor_correo_electronico' => $cotizacion['vendedor_correo_electronico'],
    					'vendedor_primer_nombre' => $cotizacion['vendedor_primer_nombre'],
    					'vendedor_segundo_nombre' => $cotizacion['vendedor_segundo_nombre'],
    					'vendedor_primer_apellido' => $cotizacion['vendedor_primer_apellido'],
    					'vendedor_segundo_apellido' => $cotizacion['vendedor_segundo_apellido'],
    					'vendedor_tlf_casa' => $cotizacion['vendedor_tlf_casa'],
    					'vendedor_tlf_movil' => $cotizacion['vendedor_tlf_movil'],
    					'vendedor_tlf_oficina' => $cotizacion['vendedor_tlf_oficina'],
    					'vendedor_fecha_creacion' => $cotizacion['vendedor_fecha_creacion'],
    					'vendedor_fecha_ingreso' => $cotizacion['vendedor_fecha_ingreso'],
    					'vendedor_fecha_modificacion' => $cotizacion['vendedor_fecha_modificacion'],
    					'vendedor_direccion' => $cotizacion['vendedor_direccion'],
    					'cat_pagador' => $cotizacion['cat_pagador'],
    					'id_empresa' => $cotizacion['id_empresa'],
    					'centro_salud_nombre' => $cotizacion['centro_salud_nombre'],
    					'centro_salud_rif' => $cotizacion['centro_salud_rif'],
    					'centro_salud_direccion' => $cotizacion['centro_salud_direccion'],
    					'fecha_cotizacion' => $cotizacion['fecha_cotizacion'],
    					'fecha_cirugia' => $cotizacion['fecha_cirugia'],
    					'seguro_telefono' => $cotizacion['seguro_telefono'],
    					'seguro_telefono_opcional' => $cotizacion['seguro_telefono_opcional'],
    					'seguro_localidad' => $cotizacion['seguro_localidad'],
    					'centro_salud_telefono' => $cotizacion['centro_salud_telefono'],
    					'centro_salud_telefono_opcional' => $cotizacion['centro_salud_telefono_opcional'],
    					'centro_salud_localidad' => $cotizacion['centro_salud_localidad'],
    					'medico_telefono' => $cotizacion['medico_telefono'],
    					'medico_telefono_opcional' => $cotizacion['medico_telefono_opcional'],
    					'medico_localidad' => $cotizacion['medico_localidad'],
    					'empresa_telefono' => $cotizacion['empresa_telefono'],
    					'empresa_telefono_opcional' => $cotizacion['empresa_telefono_opcional'],
    					'empresa_localidad' => $cotizacion['empresa_localidad'],
    					'persona_telefono' => $cotizacion['persona_telefono'],
    					'persona_telefono_opcional' => $cotizacion['persona_telefono_opcional'],
    					'persona_localidad' => $cotizacion['persona_localidad'],
    					'paciente_telefono' => $cotizacion['paciente_telefono'],
    					'paciente_telefono_opcional' => $cotizacion['paciente_telefono_opcional'],
    					'paciente_localidad' => $cotizacion['paciente_localidad'],
    					'paciente_direccion' => $cotizacion['paciente_direccion'],
    					'medico_direccion' => $cotizacion['medico_direccion']
    			);
    		}
    	}
    	//print_r($cotizaciones_result);echo " ".$precio_total;die;
    	return ($query->num_rows() > 0) ? ($cotizaciones_result) : FALSE;
    }

    function getCotizacionesLibreByVendedor($id, $fecha_init) {
    
    	//Estatus permitido para procesar cotizaciones
    	$status_allowed = 60;
    
    
    	//Tables
    	$table_cotizaciones = 'business' . '.view_libre_cotizacion_movil';
    	$table_category = 'dynamic' . '.category';
    	$table_materiales = 'business' . '.view_libre_cotizacion_articulo';
    	$table_cotizacion_status = 'business' . '.libre_cotizacion_estatus';//Vista de Materiales
    
    	//Query
    	$this->_db->select('c.*');
    	//$this->_db->where('c.profit IS NULL');
    	//$this->_db->where('cep.id_cotizacion IS NULL');
    	$this->_db->where('c.id_vendedor', $id);
    	if(!is_null($fecha_init))
    		$this->_db->where('c.fecha_creacion >', "'$fecha_init'");
    	//$this->_db->order_by("created_at", "desc");
    	 
    	$this->_db->from($table_cotizaciones." c");
    	//$this->_db->join($table_cotizaciones_error." cep", 'c.id_cotizacion = cep.id_cotizacion', 'left');
    	$query = $this->_db->get();
    	$cotizaciones = $query->result_array();
    	$cotizaciones_result = array();
    
    	foreach($cotizaciones as $cotizacion)
    	{
    
    		//Reviso el ultimo status de la cotizacion en la bd
    		$this->_db->select("cat._label");
    		$this->_db->from($table_cotizacion_status." s");
    		$this->_db->where('s.id_cotizacion', $cotizacion['id_cotizacion']);
    		$this->_db->join($table_category." cat", 's.cat_libre_cotizacion_estatus = cat.id');
    		$this->_db->order_by("created_at", "desc");
    		$this->_db->limit(1);
    		$query = $this->_db->get();
    		$status = $query->result_array();
    		$status_cot = $status[0]['_label'];
    		//JOSE STATUS
    		//Si la cotizacion tiene el status permitido
    		if(true)
    		{
    			/*
    			 $this->_db->from($table_materiales);
    			//$this->_db->join($table_cotizaciones_error." cep", 'c.id_cotizacion = cep.id_cotizacion', 'left');
    			$this->_db->where('id_cotizacion', $cotizacion['id_cotizacion']);
    			$this->_db->where("backup = '0'");
    			$this->_db->where("_label = 'MAT'");
    			*/
    			//$query = $this->_db->get();
    			//$materiales = $query->result_array();
    			$materiales_cotizacion = array();
    			$precio_subtotal = 0;
    			/*
    			 foreach($materiales as $material)
    			 {
    			$iva_porcentaje = 12;
    			$materiales_cotizacion[] = array(
    					'cantidad' => $material['cantidad'],
    					'precio' =>  $material['precio'],
    					'modelo' =>  $material['modelo'],
    					'id_material' =>  $material['id_articulo'],
    					'descripcion' =>  $material['descripcion'],
    					'id_cotizacion' =>  $material['id_cotizacion']
    			);
    			$precio_subtotal += $material['precio'];
    
    			}
    			 
    			$iva = ($precio_subtotal * $iva_porcentaje) / 100;
    			$precio_total = $precio_subtotal + $iva;
    
    			*/
    
    			$cotizaciones_result[]  = array(
    					//'materiales' => $materiales_cotizacion,
    					'statu_label' => $status_cot,
    					'codigo' => $cotizacion['codigo'],
    					
    					'monto_subtotal' => is_null($cotizacion['monto_gravable']) ? 0 : $cotizacion['monto_gravable'],
    					'monto_total' => $cotizacion['monto_total'],
    					//'monto_iva' => $iva,
    					'monto_exento' => $cotizacion['monto_exento'],
    					'descuento' => $cotizacion['descuento'],
    					'fecha_creacion' => $cotizacion['fecha_creacion'],
    					'empresa_direccion' => $cotizacion['empresa_direccion'],
    					'empresa_nombre' => $cotizacion['empresa_nombre'],
    					'empresa_rif' => $cotizacion['empresa_rif'],
    					'persona_nombre' => $cotizacion['persona_nombre'],
    					'persona_apellido' => $cotizacion['persona_apellido'],
    					'persona_cedula' => $cotizacion['persona_cedula'],
    					'id_vendedor' => $cotizacion['id_vendedor'],
    					'id_cotizacion' => $cotizacion['id_cotizacion'],
    					
    					//'cat_pagador' => $cotizacion['cat_pagador'],
    					'direccion' => $cotizacion['direccion'],
    					'fecha_cotizacion' => $cotizacion['fecha_cotizacion']
    					
    			);
    		}
    	}
    	//print_r($cotizaciones_result);echo " ".$precio_total;die;
    	return ($query->num_rows() > 0) ? ($cotizaciones_result) : FALSE;
    }
    /**
     * Obtiene las cotizaciones
     *
     * @access  public
     * @param integer $estatus  Identificador del estatus
     * @return  Array   Con los datos de las cotizaciones consultadas.
     * @author  Carlos Bello
     * @version 1
     */
    function getCotizaciones($estatus = NULL) {
    	
    	//Estatus permitido para procesar cotizaciones
    	$status_allowed = 60;
    	
    	
    	//Tables
    	$table_cotizaciones = 'business' . '.view_cotizacion';
    	$table_cotizaciones_error = 'business' . '.cotizacion_error_profit';
    	$table_materiales = 'business' . '.view_cotizacion_articulo';
    	$table_cotizacion_status = 'business' . '.cotizacion_estatus';//Vista de Materiales
    
    	//Query
    	$this->_db->select('c.*');
    	$this->_db->where('c.profit IS NULL');
    	$this->_db->where('cep.id_cotizacion IS NULL');
    	$this->_db->from($table_cotizaciones." c");
    	$this->_db->join($table_cotizaciones_error." cep", 'c.id_cotizacion = cep.id_cotizacion', 'left');
    	$query = $this->_db->get();
    	$cotizaciones = $query->result_array();
    	$cotizaciones_result = array();
    	
    	foreach($cotizaciones as $cotizacion)
    	{
    		
    		//Reviso el ultimo status de la cotizacion en la bd 
    		$this->_db->from($table_cotizacion_status);
    		$this->_db->where('id_cotizacion', $cotizacion['id_cotizacion']);
    		$this->_db->order_by("created_at", "desc");
    		$query = $this->_db->get();
    		$status = $query->result_array();
    		
    		//JOSE STATUS
    		//Si la cotizacion tiene el status permitido
    		if(count($status) > 0 && $status[0]['cat_cotizacion_estatus'] == $status_allowed)
    		{
	    		$this->_db->from($table_materiales);
	    		$this->_db->where('id_cotizacion', $cotizacion['id_cotizacion']);
	    		$this->_db->where("backup = '0'");
	    		$this->_db->where("_label = 'MAT'");
	    		
				$query = $this->_db->get();
	    		$materiales = $query->result_array();
	    		$materiales_cotizacion = array();
	    		$precio_subtotal = 0;
	    		foreach($materiales as $material)
	    		{
	    			$iva_porcentaje = 12;
	    			$materiales_cotizacion[] = array(
	    				'cantidad' => $material['cantidad'],
	    				'precio' =>  $material['precio'],
	    				'modelo' =>  $material['modelo'],
	    				'id_material' =>  $material['id_articulo'],
	    				'descripcion' =>  $material['descripcion'],
	    				'id_cotizacion' =>  $material['id_cotizacion']
	    			);
	    			$precio_subtotal += $material['precio'];
	    			
	    		}
	    		$iva = ($precio_subtotal * $iva_porcentaje) / 100;
	    		$precio_total = $precio_subtotal + $iva;
	    		
	    		
	    		
	    		$cotizaciones_result[]  = array(
	    			'materiales' => $materiales_cotizacion,
	    			'monto_subtotal' => is_null($cotizacion['monto_gravable']) ? 0 : $cotizacion['monto_gravable'],
	    			'monto_total' => $cotizacion['monto_total'],
	    			'monto_iva' => $iva,
	    			'monto_exento' => $cotizacion['monto_exento'],
	    			'descuento' => $cotizacion['descuento'],
	    			'fecha_creacion' => $cotizacion['fecha_creacion'],	
	    			'fecha_creacion' => $cotizacion['fecha_creacion'],
	    			'empresa_direccion' => $cotizacion['empresa_direccion'],
	    			'empresa_nombre' => $cotizacion['empresa_nombre'],
	    			'empresa_rif' => $cotizacion['empresa_rif'],
	    			'seguro_direccion' => $cotizacion['seguro_direccion'],
	    			'seguro_nombre' => $cotizacion['seguro_nombre'],
	    			'seguro_rif' => $cotizacion['seguro_rif'],
	    			'id_medico' => $cotizacion['id_medico'],
	    			'id_paciente' => $cotizacion['id_paciente'],
	    			'id_vendedor' => $cotizacion['id_vendedor'],
	    			'id_cotizacion' => $cotizacion['id_cotizacion'],
	    			'vendedor_cedula' => $cotizacion['vendedor_cedula'],
	    			'paciente_cedula' => $cotizacion['paciente_cedula'],
	    			'paciente_primer_nombre' => $cotizacion['paciente_primer_nombre'],
	    			'paciente_primer_apellido' => $cotizacion['paciente_primer_apellido'],
	    			'medico_cedula' => $cotizacion['medico_cedula'],
	    			'medico_primer_nombre' => $cotizacion['medico_primer_nombre'],
	    			'medico_primer_apellido' => $cotizacion['medico_primer_apellido'],
	    			'vendedor_correo_electronico' => $cotizacion['vendedor_correo_electronico'],
	    			'vendedor_primer_nombre' => $cotizacion['vendedor_primer_nombre'],
	    			'vendedor_segundo_nombre' => $cotizacion['vendedor_segundo_nombre'],
	    			'vendedor_primer_apellido' => $cotizacion['vendedor_primer_apellido'],
	    			'vendedor_segundo_apellido' => $cotizacion['vendedor_segundo_apellido'],
	    			'vendedor_tlf_casa' => $cotizacion['vendedor_tlf_casa'],
	    			'vendedor_tlf_movil' => $cotizacion['vendedor_tlf_movil'],
	    			'vendedor_tlf_oficina' => $cotizacion['vendedor_tlf_oficina'],
	    			'vendedor_fecha_creacion' => $cotizacion['vendedor_fecha_creacion'],
	    			'vendedor_fecha_ingreso' => $cotizacion['vendedor_fecha_ingreso'],
	    			'vendedor_fecha_modificacion' => $cotizacion['vendedor_fecha_modificacion'],
	    			'vendedor_direccion' => $cotizacion['vendedor_direccion'],
	    			'cat_pagador' => $cotizacion['cat_pagador'],
	    			'id_empresa' => $cotizacion['id_empresa'],
	    			'centro_salud_nombre' => $cotizacion['centro_salud_nombre'],
	    			'centro_salud_rif' => $cotizacion['centro_salud_rif'],
	    			'centro_salud_direccion' => $cotizacion['centro_salud_direccion'],
	
	    				'seguro_telefono' => $cotizacion['seguro_telefono'],
	    				'seguro_telefono_opcional' => $cotizacion['seguro_telefono_opcional'],
	    				'seguro_localidad' => $cotizacion['seguro_localidad'],
	    				'centro_salud_telefono' => $cotizacion['centro_salud_telefono'],
	    				'centro_salud_telefono_opcional' => $cotizacion['centro_salud_telefono_opcional'],
	    				'centro_salud_localidad' => $cotizacion['centro_salud_localidad'],
	    				'medico_telefono' => $cotizacion['medico_telefono'],
	    				'medico_telefono_opcional' => $cotizacion['medico_telefono_opcional'],
	    				'medico_localidad' => $cotizacion['medico_localidad'],
	    				'empresa_telefono' => $cotizacion['empresa_telefono'],
	    				'empresa_telefono_opcional' => $cotizacion['empresa_telefono_opcional'],
	    				'empresa_localidad' => $cotizacion['empresa_localidad'],
	    				'persona_telefono' => $cotizacion['persona_telefono'],
	    				'persona_telefono_opcional' => $cotizacion['persona_telefono_opcional'],			
	    				'persona_localidad' => $cotizacion['persona_localidad'],
	    				'paciente_telefono' => $cotizacion['paciente_telefono'],
	    				'paciente_telefono_opcional' => $cotizacion['paciente_telefono_opcional'],
	    				'paciente_localidad' => $cotizacion['paciente_localidad'],
	    				'paciente_direccion' => $cotizacion['paciente_direccion'],
	    				'medico_direccion' => $cotizacion['medico_direccion']
	    		);
    		}
    	}
    	//print_r($cotizaciones_result);echo " ".$precio_total;die;
    	return ($query->num_rows() > 0) ? ($cotizaciones_result) : FALSE;
    }

    
}
