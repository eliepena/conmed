<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of RCotizacion_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad cotizacion
 * y sus entidades dependientes o extendidas sobre el servicio REST
 *
 * @package Modules
 * @category Model
 * @author Carlos Bello. <calink124@gmail.com>
 * @version V-1.0 29/11/12 01:26 PM
 */
class RConmed_model extends CI_Model 
{

    public function __construct() {
    	parent::__construct();
    	$this->_db = $this->load->database('test', TRUE);
    }
    
    
    function getArticulos($where = array(), $campo = '')
    {
    	$this->db->select((empty($campo) ? ' * ' : ' ' . $campo . ' '), false);
    	$this->db->from('business.articulo');
    	if (sizeof($where) > 0):
    		$this->db->where($where);
    	endif;
    	$this->db->where(array('_delete' => '0'));
    	$this->db->order_by('modelo');
    
    	$query = $this->db->get();
    	return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }
    
    
    function getLocalidades($where = array(), $campo = '') 
    {
        $this->db->select((empty($campo) ? ' * ' : ' ' . $campo . ' '), false);
        $this->db->from('business.localidad');
        if (sizeof($where) > 0):
            $this->db->where($where);
        endif;
        //$this->db->where(array('_delete' => '0'));
        $this->db->order_by('nombre');

        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }
    
}
