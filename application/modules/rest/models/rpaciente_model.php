<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of RCotizacion_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad cotizacion
 * y sus entidades dependientes o extendidas sobre el servicio REST
 *
 * @package Modules
 * @category Model
 * @author Carlos Bello. <calink124@gmail.com>
 * @version V-1.0 29/11/12 01:26 PM
 */
class RPaciente_model extends CI_Model 
{

    public function __construct() {
    	parent::__construct();
    	$this->_db = $this->load->database('test', TRUE);
    }
    
    
    
    
    /**
     * Obtiene las cotizaciones
     *
     * @access  public
     * @param integer $estatus  Identificador del estatus
     * @return  Array   Con los datos de las cotizaciones consultadas.
     * @author  Carlos Bello
     * @version 1
     */
    function getPacientes( $where = null) {
    	
    	//Tables
    	$table_paciente = 'business' . '.paciente';
    	
    	//Query
    	$this->_db->select('*');
    	$this->_db->where('validado = \'1\'');
    	$this->_db->where(array('_delete' => '0'));
    	$this->_db->from($table_paciente);
    	if(!is_null($where))
    	{
    		$this->_db->where($where);
    	}
    	$query = $this->_db->get();
    	$pacientes = $query->result_array();
    	$pacientes_result = array();
    	
    	//print_r($cotizaciones_result);echo " ".$precio_total;die;
    	return ($query->num_rows() > 0) ? ($pacientes) : FALSE;
    }

    
}
