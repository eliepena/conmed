<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of RCotizacion_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad cotizacion
 * y sus entidades dependientes o extendidas sobre el servicio REST
 *
 * @package Modules
 * @category Model
 * @author Carlos Bello. <calink124@gmail.com>
 * @version V-1.0 29/11/12 01:26 PM
 */
class RPrecio_model extends CI_Model 
{

    public function __construct() {
    	parent::__construct();
    	$this->_db = $this->load->database('test', TRUE);
    }
    
    
    
    
    /**
     * Obtiene las cotizaciones
     *
     * @access  public
     * @param integer $estatus  Identificador del estatus
     * @return  Array   Con los datos de las cotizaciones consultadas.
     * @author  Carlos Bello
     * @version 1
     */
    function getTiposPrecio($estatus = NULL) {
    	
    	//Tables
    	$table_tipos_precio = 'business' . '.precios';
    	
    	//Query
    	$this->_db->select('tp.*');
    	//$this->_db->where('m.validado = \'1\'');
    	$this->_db->from($table_tipos_precio." tp");
    	$query = $this->_db->get();
    	$tipos_precio = $query->result_array();
    	$tipos_precio_result = array();
    	/*
    	foreach($pacientes as $paciente)
    	{
    		$pacientes_result[]  = array(
    			'monto_exento' => $paciente['monto_exento'],
    			'descuento' => $paciente['descuento'],
    			'fecha_creacion' => $paciente['fecha_creacion'],	
    			'fecha_creacion' => $paciente['fecha_creacion'],
    			'empresa_direccion' => $paciente['empresa_direccion'],
    			'empresa_nombre' => $paciente['empresa_nombre'],
    			'empresa_rif' => $cotizacion['empresa_rif'],
    			'seguro_direccion' => $cotizacion['seguro_direccion'],
    			'seguro_nombre' => $cotizacion['seguro_nombre'],
    			'seguro_rif' => $cotizacion['seguro_rif'],
    			'id_medico' => $cotizacion['id_medico'],
    			'id_paciente' => $cotizacion['id_paciente'],
    			'id_vendedor' => $cotizacion['id_vendedor'],
    			'id_cotizacion' => $cotizacion['id_cotizacion'],
    			'vendedor_cedula' => $cotizacion['vendedor_cedula'],
    			'paciente_cedula' => $cotizacion['paciente_cedula'],
    			'paciente_primer_nombre' => $cotizacion['paciente_primer_nombre'],
    			'paciente_primer_apellido' => $cotizacion['paciente_primer_apellido'],
    			'medico_cedula' => $cotizacion['medico_cedula'],
    			'medico_primer_nombre' => $cotizacion['medico_primer_nombre'],
    			'medico_primer_apellido' => $cotizacion['medico_primer_apellido'],
    			'vendedor_correo_electronico' => $cotizacion['vendedor_correo_electronico'],
    			'vendedor_primer_nombre' => $cotizacion['vendedor_primer_nombre'],
    			'vendedor_segundo_nombre' => $cotizacion['vendedor_segundo_nombre'],
    			'vendedor_primer_apellido' => $cotizacion['vendedor_primer_apellido'],
    			'vendedor_segundo_apellido' => $cotizacion['vendedor_segundo_apellido'],
    			'vendedor_tlf_casa' => $cotizacion['vendedor_tlf_casa'],
    			'vendedor_tlf_movil' => $cotizacion['vendedor_tlf_movil'],
    			'vendedor_tlf_oficina' => $cotizacion['vendedor_tlf_oficina'],
    			'vendedor_fecha_creacion' => $cotizacion['vendedor_fecha_creacion'],
    			'vendedor_fecha_ingreso' => $cotizacion['vendedor_fecha_ingreso'],
    			'vendedor_fecha_modificacion' => $cotizacion['vendedor_fecha_modificacion'],
    			'vendedor_direccion' => $cotizacion['vendedor_direccion'],
    			'cat_pagador' => $cotizacion['cat_pagador'],
    			'id_empresa' => $cotizacion['id_empresa'],
    			'centro_salud_nombre' => $cotizacion['centro_salud_nombre'],
    			'centro_salud_rif' => $cotizacion['centro_salud_rif'],
    			'centro_salud_direccion' => $cotizacion['centro_salud_direccion'],


	    	);
    	}
    	*/
    	//print_r($cotizaciones_result);echo " ".$precio_total;die;
    	return ($query->num_rows() > 0) ? ($tipos_precio) : FALSE;
    }

    
}
