<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Clase de acceso a datos del modulo de auditoria
 *
 * @package conmed.application.modules
 * @subpackage audit
 * @category Modelo
 * @author Jose A. Rodriguez E. <josearodrigueze@gmail.com>
 * @version V - 1.0 11-04-2013 17:26:24
 */
class Sales_check_model extends AST_Model {

    function __construct() {
        parent::__construct();
        $this->setSchema('acl');
    }
}