<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of articulos
 *
 * Esta clase provee la implementacion para la capa de control de
 * acciones de la entidad articulos y sus entidades dependientes o extendidas
 *
 * @package Module
 * @category Controller
 * @author Eliezer Pe#a. <pe.eliezer@gmail.com>
 * @version V-1.0 29/11/12 01:26 PM
 */
class Articulo extends AST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('articulo_model', 'articulo_model');
        $this->load->model('cotizacion/cotizacion_model', 'Cotizacion_model');
        $this->load->model('cotizacion/cotizacion_free_model', 'Cotizacion_free_model');
    }

    /**
     * Provee el acceso al formulario de ingreso de nuevos datos, asi mismo,
     * provee el acceso a la insercion de datos nuevos dentro en base de datos.
     *
     * @access public
     * @param array $params Parametros adicionales enviados al controlador.
     * @author Eliezer A. Pe#a
     * @version V 1.0 13/09/2013 1:45 pm
     */
    function create($params = NULL) {
        if (!$this->input->post()) {
            $datos = $this->articulo_model->getCatPrecios();
            $this->setTargetOn('content');
            $this->setTitle('Agregar Articulo');
            $view = array('view' => $this->view_engine->form());
            $view = $this->load->view('article_view.php', $view, TRUE);
            $this->setContent($view);
        } elseif ($params[0] == 'process') {
            $process = $this->view_engine->process();
            $success = FALSE;
            if ($process['result']) {
                $params = array('format_type' => 'insert', 'data' => $process['data']);
                $precio1 = $this->articulo_model->getIdByName('precio_1');
                $precio2 = $this->articulo_model->getIdByName('precio_2');
                $precio3 = $this->articulo_model->getIdByName('precio_3');
                $precio4 = $this->articulo_model->getIdByName('precio_4');
                $precio5 = $this->articulo_model->getIdByName('precio_5');
                $params['data']['precio_1'] = (empty($params['data']['precio_1'])) ? 0.00 : $params['data']['precio_1'];
                $params['data']['precio_2'] = (empty($params['data']['precio_2'])) ? 0.00 : $params['data']['precio_2'];
                $params['data']['precio_3'] = (empty($params['data']['precio_3'])) ? 0.00 : $params['data']['precio_3'];
                $params['data']['precio_4'] = (empty($params['data']['precio_4'])) ? 0.00 : $params['data']['precio_4'];
                $params['data']['precio_5'] = (empty($params['data']['precio_5'])) ? 0.00 : $params['data']['precio_5'];
                $params['precios'] = array(
                    array('precio' => $params['data']['precio_1'], 'id_precio' => $precio1, 'created_by' => $this->session->userdata('user_id')),
                    array('precio' => $params['data']['precio_2'], 'id_precio' => $precio2, 'created_by' => $this->session->userdata('user_id')),
                    array('precio' => $params['data']['precio_3'], 'id_precio' => $precio3, 'created_by' => $this->session->userdata('user_id')),
                    array('precio' => $params['data']['precio_4'], 'id_precio' => $precio4, 'created_by' => $this->session->userdata('user_id')),
                    array('precio' => $params['data']['precio_5'], 'id_precio' => $precio5, 'created_by' => $this->session->userdata('user_id'))
                );
                unset($params['data']['precio_1']);
                unset($params['data']['precio_2']);
                unset($params['data']['precio_3']);
                unset($params['data']['precio_4']);
                unset($params['data']['precio_5']);
                unset($params['data']['precio']);
                unset($params['data']['id_precio']);
                unset($params['data']['id_articulo']);
                $params['data']['created_by'] = $this->session->userdata('user_id');
//                print_r($params);
//                die;
                $data = $params;
                $success = $this->articulo_model->insert($data);
                if ($success) {
                    $this->updateArticles();
                }
                $this->setContent($success);
            } else {
                $this->setContent($process['validation_error']);
            }

            //seteamos los datos de auditoria
            $this->audit->setOperationData($process);
            $this->audit->setChkResult($success);
        }
    }

    /**
     * Edit de Articulo.
     *
     * @access public
     * @param array $data contiene los datos del articulo.
     * @param integer $id contiene el idnetificador del articulo
     * @author Eliezer Pe#a
     * @version V 1.0 17/09/2013
     */
    function edit($params = NULL) {
        $array_post = $this->input->post();
        $id = NULL;

        if (empty($params) AND isset($array_post['id'])) {
            $id = $this->encrypt->decode($array_post['id']);
            $pre = $this->articulo_model->getPrices($id);
            $data = $this->articulo_model->getById($id);
            $this->session->set_userdata('articulo.id', $id);
            foreach ($pre as $key => $value) {
                $precio = $this->articulo_model->getNameById($value['id_precio']);
                $pre[$precio] = $value['precio'];
                unset($pre[$key]);
            }
            $data = array_merge($data, $pre);
            $view['data'] = $data;
            $this->setContent($this->view_engine->form(array('data' => $data)), $view, TRUE);
        } elseif ($params[0] == 'process') {
            $process = $this->view_engine->process();
            $success = FALSE;
            $id = $this->session->userdata('articulo.id');
            if ($process['result']) {

                $params = array('format_type' => 'update', 'data' => $process['data']);
                $precio1 = $this->articulo_model->getIdByName('precio_1');
                $precio2 = $this->articulo_model->getIdByName('precio_2');
                $precio3 = $this->articulo_model->getIdByName('precio_3');
                $precio4 = $this->articulo_model->getIdByName('precio_4');
                $precio5 = $this->articulo_model->getIdByName('precio_5');

                $data['data'] = $this->_format($params);
                $data['data']['precio_1'] = (empty($data['data']['precio_1'])) ? 0.00 : $data['data']['precio_1'];
                $data['data']['precio_2'] = (empty($data['data']['precio_2'])) ? 0.00 : $data['data']['precio_2'];
                $data['data']['precio_3'] = (empty($data['data']['precio_3'])) ? 0.00 : $data['data']['precio_3'];
                $data['data']['precio_4'] = (empty($data['data']['precio_4'])) ? 0.00 : $data['data']['precio_4'];
                $data['data']['precio_5'] = (empty($data['data']['precio_5'])) ? 0.00 : $data['data']['precio_5'];

                $data['precios'] = array(
                    array('precio' => $data['data']['precio_1'], 'id_precio' => $precio1, 'created_by' => $this->session->userdata('user_id')),
                    array('precio' => $data['data']['precio_2'], 'id_precio' => $precio2, 'created_by' => $this->session->userdata('user_id')),
                    array('precio' => $data['data']['precio_3'], 'id_precio' => $precio3, 'created_by' => $this->session->userdata('user_id')),
                    array('precio' => $data['data']['precio_4'], 'id_precio' => $precio4, 'created_by' => $this->session->userdata('user_id')),
                    array('precio' => $data['data']['precio_5'], 'id_precio' => $precio5, 'created_by' => $this->session->userdata('user_id'))
                );
                unset($data['data']['precio_1']);
                unset($data['data']['precio_2']);
                unset($data['data']['precio_3']);
                unset($data['data']['precio_4']);
                unset($data['data']['precio_5']);
                unset($data['data']['id_precio']);
                unset($data['data']['id_articulo']);

                $success = $this->Articulo_model->edit($id, $data);

                $this->setContent($success);

                if ($success) {
                    $this->session->unset_userdata('articulo.id');
                    $this->updateArticles();
                }
            } else {
                $this->setContent($process['validation_error']);
            }
            //seteamos los datos de auditoria
            $operation_data = $process;
            $this->audit->setChkResult($success);
        }
        //seteamos los datos de auditoria
        $operation_data['id'] = $id;
        $this->audit->setOperationData($operation_data);
    }

    /**
     * list cantidad lote de Articulo.
     *
     * @access public
     * @author Eliezer Pe#a
     * @version V 1.0 17/09/2013
     */
    function listCantidad($params = NULL) {
        $array_post = $this->input->post();
        $id = $this->encrypt->decode($array_post['id']);
        $this->session->set_userdata('articulo.id', $id);
        $param['data'] = $this->articulo_model->listCantidad($id);
        $this->setTargetOn('window');
        $this->setContent($this->view_engine->grouping_grid(array('data' => $param['data'])), TRUE);
    }

    /**
     * create lote de Articulo.
     *
     * @access public
     * @author Eliezer Pe#a
     * @version V 1.0 17/09/2013
     */
    function createCantidad($params = NULL) {
        $this->setTargetOn('window2');

        if (!$this->input->post()) {
            $id = $this->session->userdata('articulo.id');
            $data = $this->articulo_model->getById($id);
            $this->setContent($this->view_engine->form(array('data' => $data)));
        } elseif ($params[0] == 'process') {
            $process = $this->view_engine->process();
            $success = FALSE;
            $id = $this->session->userdata('articulo.id');
            if ($process['result']) {
                $params = array('format_type' => 'insert', 'data' => $process['data']);

                $data = $this->_format($params);
                // unset($data['imagen']);
                // $data['id_almacen'] = $data['descripcion'];
                $data['stock'] = $data['cantidad'];
                $data['id_articulo'] = $id;
                unset($data['descripcion']);
                $success = $this->articulo_model->insertCantidad($data);
                $this->setContent($success);
                if ($success) {
                    $this->session->unset_userdata('articulo.id');
                    $this->updateArticles();
                }
            } else {
                $this->setContent($process['validation_error']);
            }
        }
    }

    /**
     * edit lote de Articulo.
     *
     * @access public
     * @author Eliezer Pe#a
     * @version V 1.0 17/09/2013
     */
    function editCantidad($params = NULL) {
        $this->setTargetOn('window2');
        $array_post = $this->input->post();
        $id = NULL;
        if (empty($params) AND isset($array_post['id'])) {
            $id = ($array_post['id']);
            $id_articulo = $this->articulo_model->getArticuloID($id);
            $tramo = $this->articulo_model->getTramoArticulo($id);
            $data = $this->articulo_model->getById($id_articulo);
            $data = array_merge($data, $tramo[0]);
            $this->session->set_userdata('tramo_articulo.id', $id);
            $this->setContent($this->view_engine->form(array('data' => $data)));
        } elseif ($params[0] == 'process') {
            $process = $this->view_engine->process();
            $success = FALSE;
            $id = $this->session->userdata('tramo_articulo.id');
            if ($process['result']) {
                $params = array('format_type' => 'update', 'data' => $process['data']);
                $data['data'] = $this->_format($params);
                unset($data['data']['imagen']);
                //print_r($data);

                $success = $this->Articulo_model->editCantidad($id, $data);

                $this->setContent($success);

                if ($success) {
                    $this->session->unset_userdata('tramo_articulo.id');
                    $this->updateArticles();
                }
            } else {
                $this->setContent($process['validation_error']);
            }
            //seteamos los datos de auditoria
            $operation_data = $process;
            $this->audit->setChkResult($success);
        }
        //seteamos los datos de auditoria
        $operation_data['id'] = $id;
        $this->audit->setOperationData($operation_data);
    }

    /**
     * delete lote de Articulo.
     *
     * @access public
     * @author Eliezer Pe#a
     * @version V 1.0 17/09/2013
     */
    function delete() {
        //$id_request = $this->input->post();

        if ($this->input->is_ajax_request() && $this->input->post()) {

            $content = 'La pagina solicitada';
            $title = 'Error!!!';

            // Verificamos si el borrado es masivo o de uno solo
            $id_request = $this->input->post('id');
            if (is_array($id_request))
                foreach ($id_request as &$id_ec) {
                    $id_ec = $this->encrypt->decode($id_ec);
                } else
                $id_request = ($id_request);

            //Arreglo de parametros a ser pasados al format.
            $params = array(
                'format_type' => 'delete',
                'data' => array()
            );
            $data = $this->_format($params);
            $success = $this->articulo_model->deleteCantidad($id_request);

            if ($success) {
                $content = '<div class="modal-body">Registro eliminado saisfactoriamente.</div>';
                $title = 'Exito';
                $this->updateArticles();
            } else
                $content = '<div class="modal-body">No se pudo eliminar el registro. Por favor intente de nuevo.</div>';

            $this->setTitle($title);
            $this->setContent($content);

            // -----------------------------
            //seteamos los datos de auditoria
            $this->audit->setOperationData(array('id' => $id_request));
            $this->audit->setChkResult($success);
            // -----------------------------
        }else {
            show_404();
        }
    }

    /**
     * actualiza el archivo json de los articulos
     *
     * @return array headers
     * @version 1.0 15/08/2013 16:55
     * @author Jose Rodriguez <josearodrigueze@gmail.com>
     */
    function updateArticles() {

        //Procesando las cabeceras
        $headers = $this->articulo_model->get_headers_articles();
        // echo '<pre>',print_r($headers, TRUE),'</pre>';die;
        foreach ($headers as &$header) {
            switch ($header['sTitle']) {
                case 'id':
                case 'imagen':
                case 'arr_precio':
                case 'arr_id_precio':
                case 'arr_precio_nombre':
                case 'almacen':
                    $header['bVisible'] = FALSE;
                    break;
                case 'modelo':
                    $header['sWidth'] = '25%';
                    break;
            }
            $header['sTitle'] = mb_convert_case($header['sTitle'], MB_CASE_TITLE);
        }
        //Procesando los datos.
        $data = $this->articulo_model->get_articles();
//         echo '<pre>',print_r($data, TRUE),'</pre>';die;
        foreach ($data as &$row)
            $row = array_values($row);
        $file = './assets/system/articles.json';
        $this->load->helper('file');
        $write = write_file($file, json_encode(array('headers' => $headers, 'data' => $data)));
    }

    /**
     * Lee un archivo csv separado por ";" y realiza las inserciones y actualizaciones correspondientes
     * la estructura de campos es:
     * LINEA|MODELO|CODIGO ARTICULO|DESCRIPCION ARTICULO|PRECIO 1|PRECIO 2|PRECIO 3|PRECIO 4|PRECIO 5
     *
     * @param file $Articulo Archivo csv de articulos a
     * @version 1.0 23/10/2013
     * @author Eliezer Pe#a <pe.eliezer@gmail.com>
     */
    function uploadArticulos() {
        if (!$this->input->post()) {
            $view['view'] = $this->view_engine->form();
            $view['title'] = "Carga masiva de Articulos";
            $view['file'] = 'uploads/articulos/carga/modelodearchivo.txt';
            $view = $this->load->view('upload', $view, TRUE);
            $this->setContent($view);
        } elseif ($this->input->post()) {
            set_time_limit(0);

            $process = $this->view_engine->process();

            if ($process['result']) {
                $params = array('format_type' => 'insert', 'data' => $process['data']);

                $data = $this->_format($params);
                $nn = 0;
                if (($gestor = fopen('uploads/articulos/carga/' . $data['Articulos'], 'r')) !== FALSE) {
                    while (($datos = fgetcsv($gestor, 1000, ";")) !== FALSE) {
                        $c = count($datos);
                        $array_data['data']['tipo_articulo'] = $this->articulo_model->getTipoArticulo($datos[0]);
                        $array_data['data']['modelo'] = $datos[1];
                        $array_data['data']['codigo'] = $datos[2];
                        $array_data['data']['descripcion'] = $datos[3];
                        $array_data['data']['created_by'] = $this->session->userdata('user_id');
                        $array_data['precios'] = array(array('precio' => $datos[4], 'id_precio' => '87'),
                            array('precio' => $datos[5], 'id_precio' => '88'),
                            array('precio' => $datos[6], 'id_precio' => '89'),
                            array('precio' => $datos[7], 'id_precio' => '90'),
                            array('precio' => $datos[8], 'id_precio' => '91'));
                        for ($x = 0; $x < $c; $x++) {
                            $param[$nn] = $array_data; //arreglo de datos de archivo
                        }
                        $nn++;
                    }

                    fclose($gestor);
                }
                unset($param[0]); //removiendo las cabeceras
                foreach ($param as $key => $value) {
                    $modelo[] = $value['data']['modelo'];
                }//extrayendo la columna modelo para consultar
                $article = array();
                $articulo = $this->articulo_model->getArticuloCsv($modelo); //consulta de datos de bd
                //print_r($modelo);

                if (empty($articulo)) {
                    $to_insert = $modelo;
                } else {
                    foreach ($articulo as $art => $value)
                        $article[$art] = $value['modelo'];
                    $to_insert = array_diff($modelo, $article); //modelos no presentes en bd para insertarlos
                    $to_update = array_intersect($modelo, $article); //modelos para actualizar
                }
                //definir los datos a insertar
                if (!empty($to_insert)) {
                    foreach ($to_insert as $value) {
                        foreach ($param as $key => $val) {
                            if ($val['data']['modelo'] == $value)
                                $data_insert[] = $param[$key];
                        }
                    }
                    $nuevos = count($data_insert);
                } else
                    $data_insert = NULL;

                //definir los datos a actualizar
                if (!empty($to_update)) {
                    foreach ($to_update as $value) {
                        foreach ($param as $key => $val) {
                            if ($val['data']['modelo'] == $value)
                                $data_update[] = $param[$key];
                        }
                    }
                    $actualizados = count($data_update);
                }else {
                    $data_update = NULL;
                }
                $success = $this->articulo_model->transactArticulos($data_insert, $data_update);
                if ($success) {
                    $nuevo = ((!isset($nuevos)) ? '0' : $nuevos);
                    $actualizado = (!isset($actualizados)) ? '0' : $actualizados;
                    $content = '<div class="modal-body">Se insertaron ' . $nuevo . ' nuevos articulos y ' . $actualizado . ' articulos fueron actualizados.</div>';
                    $title = 'Exito';
                    $this->updateArticles();
                } else
                    $content = '<div class="modal-body">Ocurrio un error al leer el archivo</div>';

                $this->setTitle($title);
                $this->setContent($content);
            } else {
                $this->setContent($process['validation_error']);
            }

            //seteamos los datos de auditoria
            $this->audit->setOperationData($process);
            $this->audit->setChkResult($success);
        }
    }

    function price_adjustment($params = NULL) {
        if (empty($params)) {
            $this->load->model('precios_model');
            $tipos_precio = $this->precios_model->getAll(0, array(), array('identificacion' => 'ASC'));

            $view_params = array(
                'title' => $this->getTitle(),
                'tipos_precio' => $tipos_precio,
                'operation_id' => $this->operation_id,
                'button_submit' => 'Ejecutar',
                'url' => 'inventario/articulo/price_adjustment',
                'articulacion' => $this->Cotizacion_model->getCirugia(array('chk_tabla' => 'articulacion'), 'id,nombre'),
                'lesion' => $this->Cotizacion_model->getCirugia(array('chk_tabla' => 'lesion'), 'id,id_parent,nombre'),
                'tecnica' => $this->Cotizacion_model->getCirugia(array('chk_tabla' => 'tecnica'), 'id,id_parent,nombre')
            );
            $content = $this->load->view('articulos/price_adjustment', $view_params, TRUE);
        } elseif ($params[0] == 'process') {
            $array_post = $this->input->post();
            $array_prices = NULL;
            $success = FALSE;
            $cont_empty = 0;

            $msg_avanced[1] = '<div class="modal-body">Agregue al menos un articulo al ajuste.</div>';
            $msg_avanced[2][1] = '<div class="modal-body">La tecnica no posee articulos asociados.</div>';
            $msg_avanced[2][2] = '<div class="modal-body">La lesion no posee articulos asociados.</div>';
            $msg_avanced[2][3] = '<div class="modal-body">La articulacion no posee articulos asociados.</div>';
            $msg_avanced[2][4] = '<div class="modal-body">Seleccione al menos una articulacion para aplicar el ajuste.</div>';
            $msg_avanced[3] = '<div class="modal-body">No existen articulos asociados para el tipo de ajuste.</div>';
            if ($array_post['filtro'] == 1) {
                if (isset($array_post['art']) && $array_post['art'] != NULL) {

                    $array_prices = $this->articulo_model->getPrices(array_values($array_post['art']));
                } else {
                    $content = $msg_avanced[1];
                }
            } elseif ($array_post['filtro'] == 2) {
                $title = 'Error';
                if (!empty($array_post['id_tecnica'])) {
                    if ($articles_recomendados = $this->getValueArticlesRecomendacion($array_post['id_tecnica'])) {
                        $array_prices = $this->articulo_model->getPrices(array_values($articles_recomendados));
                    } else {
                        $content = $msg_avanced[2][1];
                    }
                } elseif (!empty($array_post['id_lesion'])) {
                    if ($articles_recomendados = $this->getValueArticlesRecomendacion($array_post['id_lesion'])) {
                        $array_prices = $this->articulo_model->getPrices(array_values($articles_recomendados));
                    } else {
                        $content = $msg_avanced[2][2];
                    }
                } elseif (!empty($array_post['id_articulacion'])) {
                    if ($articles_recomendados = $this->getValueArticlesRecomendacion($array_post['id_articulacion'])) {
                        $array_prices = $this->articulo_model->getPrices(array_values($articles_recomendados));
                    } else {
                        $content = $msg_avanced[2][3];
                    }
                } else {
                    $content = $msg_avanced[2][4];
                }
            } elseif ($array_post['filtro'] == 3) {
                if ($id_articles = $this->getValueAllIdArticles()) {
                    $array_prices = $this->articulo_model->getPrices(array_values($id_articles));
                } else {
                    $content = $msg_avanced[3];
                }
            }
            if (isset($array_post['check_price']) && sizeof($array_post['check_price']) > 0) {
                foreach ($array_post['check_price'] as $key => $value) {

                    if (!empty($array_post['porcentaje'][$key]) && $this->is_numeric($array_post['porcentaje'][$key]) || $this->decimal($array_post['porcentaje'][$key])) {
                        
                    } else {
                        $cont_empty++;
                    }
                }
                if ($cont_empty <= 0) {
                    if ($array_prices != NULL && sizeof($array_prices) > 0) {
                        //LLAMAR A FORM VALIDATION

                        $formula = (empty($array_post['input_formula']) ? '(([POR]/100)+1)*[ART]' : $this->limpiar_formula_malisiosa($array_post['input_formula']));

                        //Valido hay Tipo de Precios chequeados
                        if ($this->formula_valida($formula)) {
                            $porcentajes = $data = array();

                            //Obtengo la formula a ajecutar. Le anexo round para redondear a 2 decimales y hacia arriba.
                            $formula = 'round(' . strtoupper($formula) . ', 2, PHP_ROUND_HALF_UP)';

                            //Consulto los precios de los articulos
                            //$array_prices = $this->articulo_model->getPrices(array_values($array_post['art']));

                            foreach ($array_prices as $price) {
                                $replace = $price['precio'];

                                //Sustituyo la posicion de Articulo en la formula.
                                $formula_art = str_replace('[ART]', $replace, $formula);
                                // $formula_art = str_replace('.', ',', $formula);
                                //Evaluo porcentaje si esta vacio lo cargo y mas nunca
                                if (empty($porcentajes)) {
                                    //  Recorro el arreglo para identificar los tipos de precios y los porcejas d aumento.
                                    foreach ($array_post['check_price'] as $key => &$value)
                                        $porcentajes[$value] = $array_post['porcentaje'][$key];
                                }

                                //Sustityo la posicion de Porcentaje en la Formula
                                if (array_key_exists($price['id_precio'], $porcentajes)) {
                                    $row = array('id_precio' => $price['id_precio'], 'id_articulo' => $price['id_articulo'], 'precio' => 0, 'id' => $price['id']);
                                    $formula_art = str_replace('[POR]', $porcentajes[$price['id_precio']], $formula_art);
                                    eval('$row["precio"] = ' . $formula_art . ';');

                                    array_push($data, $row);
                                }
                            }

                            $success = $this->articulo_model->masive_price_adjustment($data);

                            if ($success) {
                                $content = '<div class="modal-body">Precios Actualizados Satisfactoriamente.</div>';
                                $title = 'Exito';
                                $this->updateArticles();
                            } else {
                                $content = '<div class="modal-body">No se pudieron actualizar los precios. Por favor intente de nuevo.</div>';
                            }
                        } else { //Caso contrario mensaje de error.
                            $title = 'Error';
                            $content = '<div class="modal-body">Formula invalidad, revise la misma e intente de nuevo.</div>';
                        }
                    }
                } else {
                    $title = 'Error';
                    $content = '<div class="modal-body">
                                    Verifique en los porcentajes las siguientes validaciones. 
                                    <ul>
                                        <li>No pueden estar vacio</li>
                                        <li>Deben ser valores numericos enteros(Ejemp: 1) o Decimales(Ejemp:2.2)</li>
                                    </ul>
                                </div>';
                }
            } else {
                $title = 'Error';
                $content = '<div class="modal-body">Seleccione el tipo de precio que desea ajustar</div>';
            }
            $this->setTargetOn('json');
            $view['view'] = $success;
            $view['title'] = $title;
            $view['result'] = $content;
            $view['msg'] = $content;
            $content = $view;

            //seteamos los datos de auditoria
            $this->audit->setChkResult($success);
            $this->audit->setOperationData($array_post);
        }

        $this->setContent($content);
    }

    /**
     * Evalua si la formula dada es correcta.
     *
     *  @param String $formula
     *  @return Boolean Codigo de Error si la Formula
     */
    private function formula_valida($formula) {
        if (empty($formula))
            return FALSE;
        $rs = NULL;
        $formula = str_replace('[ART]', '1', $formula);
        $formula = str_replace('[POR]', '1', $formula);
        eval('$rs = ' . $formula . ';');
        return (empty($rs)) ? FALSE : TRUE;
    }

    private function limpiar_formula_malisiosa($formula) {
        return str_replace(array(';', '$'), '', $formula);
    }

    /**
     * Muestra el detalle un registro de un articulo.
     *
     * @access public
     * @author Eliezer Pe#a
     * @version V 1.0 27/01/2014
     */
    function detail() {
        $array_post = $this->input->post();
        $id = $this->encrypt->decode($array_post['id']);

        if (!empty($id)) {
            $pre = $this->articulo_model->getPrices($id);
            $data = $this->articulo_model->getById($id);
            //$this->session->set_userdata('articulo.id', $id);
            foreach ($pre as $key => $value) {
                $precio = $this->articulo_model->getNameById($value['id_precio']);
                $pre[$precio] = $value['precio'];
                unset($pre[$key]);
            }
            $data = array_merge($data, $pre);
            $view['data'] = $data;
            $this->setContent($this->view_engine->form(array('data' => $data)), $view, TRUE);
        }
        //seteamos los datos de auditoria
        $this->audit->setOperationData(array('id' => $id));
    }

    /**
     * delete Articulo.
     *
     * @access public
     * @author  Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version 1.0
     */
    function articulo_delete() {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $success = FALSE;
            $content = 'La pagina solicitada';
            $title = 'Detalle de Articulo a Eliminar';

            // Verificamos si el borrado es masivo o de uno solo
            $id_request = $this->input->post('id');
            $id = $this->encrypt->decode($id_request);
            $used = $this->articulo_model->getUsedArticles($id);

            if (is_array($id_request))
                foreach ($id_request as &$id_ec) {
                    $id_ec = $this->encrypt->decode($id_ec);
                } else
                $id_request = ($id_request);

            //Arreglo de parametros a ser pasados al format.
            $params = array(
                'format_type' => 'delete',
                'data' => array()
            );
            $data = $this->_format($params);
            if (empty($used)) {
                $success = $this->articulo_model->deleteArticle($id);
            }
            //$success = $this->articulo_model->deleteCantidad($id_request);
            if ($success) {
                $content = '<div class="modal-body">Registro eliminado satisfactoriamente.</div>';
                $title = 'Exito';
                $this->updateArticles();
            } elseif (!empty($used)) {

                foreach ($used as $key => $value):
                    if ($value['tipo_cotizacion'] == 'normal'):

                        $cotiz['data'][$key] = $this->Cotizacion_model->getById($value['id_cotizacion']);
                        $cotiz['data'][$key]['tipo_cot'] = 'normal';

                    elseif ($value['tipo_cotizacion'] == 'libre'):


                        $cotiz['data'][$key] = $this->Cotizacion_free_model->getById($value['id_cotizacion']);
                        $cotiz['data'][$key]['tipo_cot'] = 'libre';
                    endif;
                endforeach;

                $this->session->set_userdata('id_articulo', $id_request);
                $content = $this->load->view('inventario/articulos/delete_articles', array('data' => $cotiz), TRUE);
                // $content = '<div class="modal-body">No se pudo eliminar el registro. El articulo esta siendo usado en una cotizacion.</div>';
            }
            $this->setTitle($title);
            $this->setContent($content);

            // -----------------------------
            //seteamos los datos de auditoria
            $this->audit->setOperationData(array('id' => $id_request));
            $this->audit->setChkResult($success);
            // -----------------------------
        } else {
            show_404();
        }
    }

    /**
     * deshabilita los articulos en caso de que no puedan ser eliminados.
     *
     * @access public
     * @author  Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version 1.0
     */
    function EXC_disableArticles() {

        $id_request = $this->session->userdata('id_articulo');
        $id = $this->encrypt->decode($id_request);

        $disabled = $this->articulo_model->disableArticle($id);
        if ($disabled) {
            $this->updateArticles();
        }
        $this->offOutput();
        $resp = array('success' => true);
        echo json_encode($resp);
    }

    /**
     * deshabilita los articulos en caso de que no puedan ser eliminados.
     *
     * @access public
     * @author  Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version 1.0
     */
    function EXC_disableArticlesMassive($articles) {
        $array_post = $this->input->post();
        if (!empty($array_post)) {
            $articulos = $array_post['articulo'];
            foreach ($articulos as $key => $value) {
                unset($articulos[$key]['name']);
                $disabled = $this->articulo_model->disableArticle($articulos[$key]['value']);
            }
        }

        if ($disabled) {
            $this->updateArticles();
        }
        $this->offOutput();
        $resp = array('success' => true);
        echo json_encode($resp);
    }

    /**
     * deshabilita los articulos en caso de que no puedan ser eliminados.
     *
     * @access public
     * @author  Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version 1.0
     */
    function EXC_deleteArticlesMassive() {
        $array_post = $this->input->post();
        if (!empty($array_post)) {
            $articulos = $array_post['articulo'];
            foreach ($articulos as $key => $value) {
                unset($articulos[$key]['name']);
                $disabled = $this->articulo_model->deleteArticle($articulos[$key]['value']);
            }
        }

        if ($disabled) {
            $this->updateArticles();
        }
        $this->offOutput();
        $resp = array('success' => true);
        echo json_encode($resp);
    }

    function listDisabled($param = NULL) {
        $this->listAll();
    }

    /**
     * metodo para paginacion de articulos desactivados
     *
     * @access public
     * @author  Eliezer Pe#a
     * @version V 1.0 
     */
    public function getPaginationData($params) {
        $uri = $this->uri->uri_string();

        $array_post = $this->input->post();

        if (strpos($uri, 'listDisabled')) {
            if (isset($array_post['id'])) {
                $id = $this->encrypt->decode($array_post['id']);
                $this->session->set_userdata('articulo.id', $id);
            }
            $params['id'] = $this->session->userdata('articulo.id');
            $data['data'] = $this->articulo_model->getDisabled($params);
            $data['count'] = $this->articulo_model->countDisabled($params);

            return $data;
        } else {
            return parent::getPaginationData($params);
        }
    }

    /**
     * Muestra el detalle un registro de un articulo.
     *
     * @access public
     * @author Eliezer Pe#a
     * @version V 1.0 27/01/2014
     */
    function restoreArticles($params = NULL) {
        $array_post = $this->input->post();
        $id = NULL;

        if (empty($params) AND isset($array_post['id'])) {
            $id = $this->encrypt->decode($array_post['id']);
            $data = $this->articulo_model->getDisabledById($id);
            $this->session->set_userdata('articulo.id', $id);
            $this->setContent($this->view_engine->form(array('data' => $data, 'button_submit' => 'Restaurar')));
        } elseif ($params[0] == 'process') {
            $process = $this->view_engine->process();
            $success = FALSE;
            $id = $this->session->userdata('articulo.id');

            if ($process['result']) {
                $disabled = '0';
                unset($process['data']);

                $success = $this->articulo_model->disableArticle($id, $disabled);
                $this->setContent($success);
                if ($success) {
                    $this->session->unset_userdata('articulo.id');
                    $this->updateArticles();
                }
            } else {

                $this->setContent($process['validation_error']);
            }
            //seteamos los datos de auditoria
            $operation_data = $process;
            $this->audit->setChkResult($success);
        }
        //seteamos los datos de auditoria
        $operation_data['id'] = $id;
        $this->audit->setOperationData($operation_data);
    }

    /**
     * print etiqueta.
     *
     * @access public
     * @author shirley perez
     * @version V 1.0 14/01/2014
     */
    public function ImprimirEtiqueta($params = NULL) {
        $this->setTargetOn('window2');
        $array_post = $this->input->post();
        $id = NULL;
        if (empty($params) AND isset($array_post['id'])) {
            $id = ($array_post['id']);
            $id_articulo = $this->articulo_model->getArticuloID($id);
            $tramo = $this->articulo_model->getTramoArticulo($id);
            $data = $this->articulo_model->getById($id_articulo);
            $data = array_merge($data, $tramo[0]);
            $this->session->set_userdata('tramo_articulo.id', $id);
            $this->setContent($this->view_engine->form(array('data' => $data, 'button_submit' => 'Imprimir')));
        } elseif ($params[0] == 'process') {
            $process = $this->view_engine->process();
            $success = FALSE;
            $id = $this->session->userdata('tramo_articulo.id');
            if ($process['result']) {
                $params = array('format_type' => 'update', 'data' => $process['data']);

                $id_articulo = $this->articulo_model->getArticuloID($id);
                $data_art = $this->articulo_model->getById($id_articulo);
                $data['data'] = $this->_format($params);

                unset($data['data']['imagen']);

                $this->session->unset_userdata('tramo_articulo.id');

                //Llenamos el arreglo a imprimir con el arreglo value	
                for ($i = 1; $i <= 1; $i++) {//$data['data']['cantidad']; $i++) {
                    $codigo_barra = $this->CodigoBarra($data_art['modelo'] . '-' . $data['data']['lote']);
                    if (strlen($data_art['descripcion']) > 23) { //verificamos que la longitud del nombre no sea mayor a n caracteres, si es mayor se divide
                        $nombre_art1 = substr($data_art['descripcion'], 0, 23);
                        $nombre_art2 = substr($data_art['descripcion'], 24, strlen($data_art['descripcion']));
                    } else {
                        $nombre_art1 = $data_art['descripcion'];
                        $nombre_art2 = "";
                    }
                    //Verificamos los campos disponibles para imprimir

                    if ($data_art['id_proveedor'] != null && $data_art['descripcion_etiqueta'] != null) {
                        //Obs.:Dividir la descripcion si es mayor a 23 caracteres
                        $print_data = '^XA
                            ^MMT
                            ^PW413
                            ^LL0591
                            ^LS0
                            ^FT149,39^A0N,25,24^FH\^FD' . $data_art['modelo'] . '^FS
                            ^FT73,103^A0N,21,19^FH\^FD' . $nombre_art1 . '^FS
                            ^FT74,71^A0N,21,19^FH\^FD' . $nombre_art2 . '^FS
                            ^FT75,139^A0N,25,24^FH\^FDLOT ' . $data['data']['lote'] . '^FS
                            ^BY1,3,34^FT116,185^BCN,,Y,N
                            ^FD>:' . $codigo_barra . '^FS
                            ^FT83,554^A0N,17,16^FH\^FDDra. Representante Legar^FS
                            ^FT85,529^A0N,17,16^FH\^FD' . $llenar_nombre_representante_legal . '^FS
                            ^FT77,456^A0N,17,16^FH\^FD' . $llenar_nombre_proovedor . ',^FS
                            ^FT77,477^A0N,17,16^FH\^FD' . $llenar_dir_proveedor1 . '^FS
                            ^FT77,498^A0N,17,16^FH\^FD' . $llenar_dir_proveedor2 . '^FS
                            ^FT77,419^A0N,17,16^FH\^FDRZgistrado Zn Zl M.P.P.S bajo Zl No.:' . $llenar_nro_registro . '^FS
                            ^FT77,381^A0N,17,16^FH\^FDTZl\82fono:^FS
                            ^FT77,358^A0N,17,16^FH\^FDRIF:^FS
                            ^FT76,313^A0N,17,16^FH\^FD' . $this->config->item('dir_etiq1') . '^FS
                            ^FT76,334^A0N,17,16^FH\^FD' . $this->config->item('dir_etiq2') . '^FS
                            ^FT77,275^A0N,17,16^FH\^FDImportado y distribuido por: ^FS
                            ^FT77,228^A0N,17,16^FH\^FD' . $descripcion1 . '^FS
                            ^FT77,249^A0N,17,16^FH\^FD' . $descripcion2 . '^FS
                            ^FT77,400^A0N,17,16^FH\^FD' . $this->config->item('telefono_etiq') . '^FS
                            ^FT77,294^A0N,17,16^FH\^FD' . $this->config->item('emp_etiq') . '^FS
                            ^FT107,358^A0N,17,16^FH\^FD' . $this->config->item('rif_etiq') . '^FS
                            ^FT77,438^A0N,17,16^FH\^FDFabricado por:^FS
                            ^PQ1,0,1,Y^XZ';
                    } else if ($data_art['id_proveedor'] == null && $data_art['descripcion_etiqueta'] != null) {
                        //Obs.:Dividir la descripcion si es mayor a 23 caracteres
                        $print_data = '^XA
                                        ^MMT
                                        ^PW413
                                        ^LL0591
                                        ^LS0
                                        ^FT149,39^A0N,25,24^FH\^FD' . $data_art['modelo'] . '^FS
                                        ^FT73,103^A0N,21,19^FH\^FD' . $nombre_art1 . '^FS
                                        ^FT74,71^A0N,21,19^FH\^FD' . $nombre_art2 . '^FS
                                        ^FT75,139^A0N,25,24^FH\^FDLOT ' . $data['data']['lote'] . '^FS
                                        ^BY1,3,34^FT116,185^BCN,,Y,N
                                        ^FD>:' . $codigo_barra . '^FS
                                        ^FT78,393^A0N,17,16^FH\^FDTZl\82fono:^FS
                                        ^FT78,367^A0N,17,16^FH\^FDRIF:^FS
                                        ^FT78,320^A0N,17,16^FH\^FD' . $this->config->item('dir_etiq1') . '^FS
                                        ^FT78,341^A0N,17,16^FH\^FD' . $this->config->item('dir_etiq2') . '^FS
                                        ^FT77,278^A0N,17,16^FH\^FDImportado y distribuido por: ^FS
                                        ^FT77,231^A0N,17,16^FH\^FD' . $llenar_descripcion1 . '^FS
                                        ^FT77,252^A0N,17,16^FH\^FD' . $llenar_descripcion2 . '^FS
                                        ^FT78,412^A0N,17,16^FH\^FD' . $this->config->item('telefono_etiq') . '^FS
                                        ^FT77,302^A0N,17,16^FH\^FD' . $this->config->item('emp_etiq') . '^FS
                                        ^FT108,367^A0N,17,16^FH\^FD' . $this->config->item('rif_etiq') . '^FS
                                        ^PQ1,0,1,Y^XZ';
                    } else if ($data_art['id_proveedor'] != null && $data_art['descripcion_etiqueta'] == null) {
                        //Obs.:Dividir la direccion del proveedor si es mayor a 23 caracteres
                        $print_data = '^XA
                                        ^MMT
                                        ^PW413
                                        ^LL0591
                                        ^LS0
                                        ^FT149,39^A0N,25,24^FH\^FD' . $data_art['modelo'] . '^FS
                                        ^FT73,103^A0N,21,19^FH\^FD' . $nombre_art1 . '^FS
                                        ^FT74,71^A0N,21,19^FH\^FD' . $nombre_art2 . '^FS
                                        ^FT75,139^A0N,25,24^FH\^FDLOT ' . $data['data']['lote'] . '^FS
                                        ^BY1,3,34^FT116,185^BCN,,Y,N
                                        ^FD>:' . $codigo_barra . '^FS
                                        ^FT83,522^A0N,17,16^FH\^FD' . $llenar_nombre_representante_legal . '^FS
                                        ^FT85,497^A0N,17,16^FH\^FDNombrZ dZl RZprZsZntantZ LZgal:^FS
                                        ^FT77,429^A0N,17,16^FH\^' . $llenar_nombre_proovedor . ',^FS
                                        ^FT77,450^A0N,17,16^FH\^FD' . $llenar_dir_proveedor1 . '^FS
                                        ^FT77,471^A0N,17,16^FH\^FD' . $llenar_dir_proveedor2 . '^FS
                                        ^FT77,383^A0N,17,16^FH\^FDRZgistrado Zn Zl M.P.P.S bajo Zl No.:' . $llenar_nro_registro . '^FS
                                        ^FT77,345^A0N,17,16^FH\^FDTZl\82fono:^FS
                                        ^FT77,321^A0N,17,16^FH\^FDRIF:^FS
                                        ^FT76,277^A0N,17,16^FH\^FD' . $this->config->item('dir_etiq1') . '^FS
                                        ^FT76,298^A0N,17,16^FH\^FD' . $this->config->item('dir_etiq2') . '^FS
                                        ^FT77,238^A0N,17,16^FH\^FDImportado y distribuido por: ^FS
                                        ^FT77,364^A0N,17,16^FH\^FD' . $this->config->item('telefono_etiq') . '^FS
                                        ^FT77,258^A0N,17,16^FH\^FD' . $this->config->item('emp_etiq') . '^FS
                                        ^FT107,322^A0N,17,16^FH\^FD' . $this->config->item('rif_etiq') . '^FS
                                        ^FT77,406^A0N,17,16^FH\^FDFabricado por:^FS
                                        ^PQ1,0,1,Y^XZ';
                    } else {
                        $print_data = '^XA
                                        ^MMT
                                        ^PW413
                                        ^LL0591
                                        ^LS0
                                        ^FT149,39^A0N,25,24^FH\^FD' . $data_art['modelo'] . '^FS
                                        ^FT73,106^A0N,21,19^FH\^FD' . $nombre_art1 . '^FS
                                        ^FT75,140^A0N,21,21^FH\^FDLOT ' . $data['data']['lote'] . '^FS
                                        ^FT74,74^A0N,21,19^FH\^FD' . $nombre_art2 . '^FS
                                        ^BY1,3,34^FT112,214^BCN,,Y,N
                                        ^FD>:' . $codigo_barra . '^FS
                                        ^FT77,404^A0N,17,16^FH\^FDTZl\82fono:^FS
                                        ^FT78,377^A0N,17,16^FH\^FDRIF:^FS
                                        ^FT78,328^A0N,17,16^FH\^FD' . $this->config->item('dir_etiq1') . '^FS
                                        ^FT78,349^A0N,17,16^FH\^FD' . $this->config->item('dir_etiq2') . '^FS
                                        ^FT77,275^A0N,17,16^FH\^FDImportado y distribuido por: ^FS
                                        ^FT77,432^A0N,17,16^FH\^FD' . $this->config->item('telefono_etiq') . '^FS
                                        ^FT77,301^A0N,17,16^FH\^FD' . $this->config->item('emp_etiq') . '^FS
                                        ^FT108,378^A0N,17,16^FH\^FD' . $this->config->item('rif_etiq') . '^FS
                                        ^PQ1,0,1,Y^XZ';
                    }


                    // Open a telnet connection to the printer, then push all the data into it.
                    try {
                        $fp = pfsockopen("" . $this->config->item('dir_impresora') . "", 9100);
                        fputs($fp, $print_data);
                        fclose($fp);

                        $content = '<div class="modal-body">Impresión realizada saisfactoriamente.</div>';
                        $title = 'Exito';
                        $this->setTitle($title);
                        $this->setContent($content);
                    } catch (Exception $e) {
                        $content = '<div class="modal-body">Error: ' . $e->getMessage() . '</div>';
                        $title = 'Error';
                        $this->setTitle($title);
                        $this->setContent($content);
                    }
                }
            } else {
                $this->setContent($process['validation_error']);
            }
            //seteamos los datos de auditoria
            $operation_data = $process;
            $this->audit->setChkResult($success);
        }
        //seteamos los datos de auditoria
        $operation_data['id'] = $id;
        $this->audit->setOperationData($operation_data);
    }

    /**
     * Genera el código de barra.
     *
     * @access public
     * @author shirley perez
     * @version V 1.0 14/01/2014
     */
    public function CodigoBarra($values = '') {
        $array_codigo = str_split($values);
        $cadena = "";
        $i = 0;
        $var_ant = "";

        foreach ($array_codigo as $valor) {
            if (ctype_digit($valor) && $i != 0 && $valor != '-' && !ctype_digit($var_ant)) {
                $cadena .= '>5' . $valor;
                $var_ant = $valor;
            } else if ($valor == '-') {
                $cadena .= '>63' . $valor;
                $var_ant = $valor;
            } else {
                $cadena .= $valor;
                $var_ant = $valor;
            }

            $i++;
        }

        return $cadena;
    }

    function advanced_delete($params = NULL) {
        if (empty($params)) {

            $view_params = array(
                'title' => $this->getTitle(),
                'articulacion' => $this->Cotizacion_model->getCirugia(array('chk_tabla' => 'articulacion'), 'id,nombre'),
                'lesion' => $this->Cotizacion_model->getCirugia(array('chk_tabla' => 'lesion'), 'id,id_parent,nombre'),
                'tecnica' => $this->Cotizacion_model->getCirugia(array('chk_tabla' => 'tecnica'), 'id,id_parent,nombre'),
                'tipo_articulo' => $this->Articulo_model->getSelectTipo(),
                'fabricante' => $this->Articulo_model->getFabricante(),
                'button_submit' => 'Ejecutar',
                'url' => 'inventario/articulo/advanced_delete'
            );
            $content = $this->load->view('advanced_delete', $view_params, TRUE);
        } elseif ($params[0] == 'process') {
            $array_post = $this->input->post();
            $success = FALSE;
//            print_r($array_post);
//            die;
            $articles = $this->Articulo_model->getArticulo($array_post);

            $i = 0;
            $j = 0;
            $no_borrable = array();
            $borrable = array();
            foreach ($articles as $key => $value) {
                $used = $this->articulo_model->getUsedArticles($articles[$key]['id']);
                if ($used) {
                    $no_borrable[$i] = $articles[$key]['id'];
                    $i++;
                } else {
                    $borrable[$i] = $articles[$key]['id'];
                    $j++;
                }
            }
            if (!empty($borrable)) {
                $art_borr = $this->Articulo_model->getById($borrable);
                $data['borrable'] = $art_borr;
            }
            if (!empty($no_borrable)) {
                $art_no_borr = $this->Articulo_model->getById($no_borrable);
                $data['no_borrable'] = $art_no_borr;
            }


            $view['data'] = $data;
            $success = TRUE;
            $this->setTargetOn('window');
            // $view['view'] = $success;
            $view['title'] = $title;
            // $view['result'] = $content;
            $content = $this->load->view('inventario/articulos/delete_articles_general', $data, TRUE);
//            echo '<pre>';
//            var_dump($content);
//            echo '</pre>';
//            exit;
            // -----------------------------
            //$this->setTitle($title);
            //seteamos los datos de auditoria
            $this->audit->setChkResult($success);
            $this->audit->setOperationData($array_post);
        }

        $this->setContent($content);
    }

    function EXC_valida_articulos($params = NULL) {
        $array_post = $this->input->post();
        $success = FALSE;
//            print_r($array_post);
//            die;
        $articles = $this->Articulo_model->getArticulo($array_post);
        $i = 0;
        $j = 0;
        $no_borrable = array();
        $borrable = array();
        if (!empty($articles)) {
            foreach ($articles as $key => $value) {
                $used = $this->articulo_model->getUsedArticles($articles[$key]['id']);
                if ($used) {
                    if ($articles[$key]['id']):
                        $no_borrable[$i] = $articles[$key]['id'];
                    endif;

                    $i++;
                }
            }
            $borrable = array_diff($articles, $no_borrable);
            if (!empty($borrable)) {
                foreach ($borrable as $key => $value) {
                    $art_borr[$key] = $this->Articulo_model->getByIdBorrable($borrable[$key]['id']);
                    if ($art_borr[$key]):
                        foreach ($art_borr as $k => $v) {
                            $art_borr[$key] = $v[0];
                        }
                    endif;
                    if (!$art_borr[$key]):
                        unset($art_borr[$key]);
                    endif;
                }
            } else {
                $borrable = FALSE;
            }
            $data['borrable'] = $art_borr;
//            print_r($art_borr);
//            die;
            if (!empty($no_borrable)) {
                $art_no_borr = $this->Articulo_model->getByIdNoBorrable($no_borrable);
                $data['no_borrable'] = $art_no_borr;
            } else {
                $data['no_borrable'] = FALSE;
            }
            $success = TRUE;
            $msg = 's';
        } else {
            $success = FALSE;
            $msg = 'Disculpe no se encuentran articulos con su criterio de busqueda';
        }
        $data['valid_no_borrable'] = (sizeof($data['no_borrable']) > 0 ? TRUE : FALSE);
        $data['valid_borrable'] = (sizeof($data['borrable']) > 0 ? TRUE : FALSE);
        $this->offOutput();
        $rsp = array('data' => $data, 'success' => $success, 'msg' => $msg);
//        $view['data'] = $data;
//        $success = TRUE;
//        $this->setTargetOn('window');
//        // $view['view'] = $success;
//        $view['title'] = $title;
        // $view['result'] = $content;
        // $content = $this->load->view('inventario/articulos/delete_articles_general', $view, TRUE);
        echo json_encode($rsp);
    }

    function getValueArticlesRecomendacion($id_cirugia = NULL) {
        $articles_response = array();
        $articles = $this->articulo_model->getArticulosRecomendados($id_cirugia);
        foreach ($articles as $key => $value) {

            $articles_response[] = $value['id_articulo'];
        }
        return $articles_response;
    }

    function getValueAllIdArticles() {
        $articles_response = array();
        $articles = $this->Articulo_model->getAllId();
        foreach ($articles as $key => $value) {

            $articles_response[] = $value['id'];
        }
        return $articles_response;
    }

    /**
     * Numeric
     *
     * @access	public
     * @param	string
     * @return	bool
     */
    public function numeric($str) {
        return (bool) preg_match('/^[\-+]?[0-9]*\.?[0-9]+$/', $str);
    }

    // --------------------------------------------------------------------

    /**
     * Is Numeric
     *
     * @access	public
     * @param	string
     * @return	bool
     */
    public function is_numeric($str) {
        return (!is_numeric($str)) ? FALSE : TRUE;
    }

    // --------------------------------------------------------------------

    /**
     * Integer
     *
     * @access	public
     * @param	string
     * @return	bool
     */
    public function integer($str) {
        return (bool) preg_match('/^[\-+]?[0-9]+$/', $str);
    }

    /**
     * Decimal number
     *
     * @access	public
     * @param	string
     * @return	bool
     */
    public function decimal($str) {
        return (bool) preg_match('/^[\-+]?[0-9]+\.[0-9]+$/', $str);
    }

}
