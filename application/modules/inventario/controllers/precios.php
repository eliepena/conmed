<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of precios
 *
 * Esta clase provee la implementacion para la capa de control de
 * acciones de la entidad precios y sus entidades dependientes o extendidas
 *
 * @package Module
 * @category Controller
 * @author Eliezer Pe#a. <pe.eliezer@gmail.com>, Jose Rodriguez <josearodrigueze@gmail.com>
 * @version V-1.1 23/09/13 05:00 PM
 */
class Precios extends AST_Controller {

    public function __construct() {
        parent::__construct();
    }
}

/** End Class precios*/
/** End File precios.php*/