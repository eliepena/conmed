<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of almacen
 *
 * Esta clase provee la implementacion para la capa de control de
 * acciones de la entidad almacen y sus entidades dependientes o extendidas
 *
 * @package Module
 * @category Controller
 * @author Eliezer Pe#a. <pe.eliezer@gmail.com>
 * @version V-1.0 02/10/13
 */
class Almacen extends AST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('almacen_model');
    }

    /**
     * Obtiene los datos a se paginados y mostrados en el grid. La idea de esta funcion
     * es para cuando deseamos paginar datos que no son asociados directamente con el
     * modelo del controlador. Es necesario que retone siempre un array con las posiciones
     * data y count
     *
     * @access  public
     * @param   string  array[offset]
     * @param   string  array[search_fields]
     * @param   string  array[sort]
     * @return  array   con las posiciones [data] y [count]
     */
    public function getPaginationData($params) {
        $uri = $this->uri->uri_string();
        $array_post = $this->input->post();
        if (strpos($uri, 'listEstante')) {
            if (isset($array_post['id'])) {
                $id = $this->encrypt->decode($array_post['id']);
                $this->session->set_userdata('almacen.id', $id);
            }
            $params['id'] = $this->session->userdata('almacen.id');
            $data['data'] = $this->Almacen_model->getEstante($params);
            $data['count'] = $this->Almacen_model->countEstante($params);

            return $data;
        } elseif (strpos($uri, 'listTramo')) {
            if (isset($array_post['id'])) {
                $id = $this->encrypt->decode($array_post['id']);
                $this->session->set_userdata('almacen.id', $id);
            }
            if (isset($array_post['id'])) {
                $id = $this->encrypt->decode($array_post['id']);
                $this->session->set_userdata('almacen.id', $id);
            }
            $params['id'] = $this->session->userdata('almacen.id');
            $data['data'] = $this->Almacen_model->getTramo($params);

            $data['count'] = $this->Almacen_model->countTramo($params);
            //          print_r($data);die;
            return $data;
        } else {
            return parent::getPaginationData($params);
        }
    }

    /**
     * Muestra los estantes
     *
     * @access  public
     * @param     array    $params    parametros a ser procesados en el metodo
     * @return  array    dato a ser retornado
     */
    public function listEstante($params) {
        $this->listAll();
    }

    /**
     * Provee un formulario para la carga de nuevos estantes asociados a los almacenes
     *
     * @access  public
     * @param array $data datos obtenidos del formulario de creacion de estantes
     * @author Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version V 1.0 03/10/2013
     */
    function createEstante() {
        $this->setTargetOn('window2');
        $id = $this->session->userdata('almacen.id');

        if (!$this->input->post()) {

            $data['id_almacen'] = $id;
            $this->setContent($this->view_engine->form());
        } elseif ($this->input->post()) {
            $process = $this->view_engine->process();
            $success = FALSE;
            if ($process['result']) {
                $params = array('format_type' => 'insert', 'data' => $process['data']);

                $data = $this->_format($params);
                $data['id_almacen'] = $id;

                $success = $this->Almacen_model->insertEstante($data);

                if ($success) {
                    $this->session->unset_userdata('almacen.id');
                }
                $this->setContent($success);
            } else {
                $this->setContent($process['validation_error']);
            }

            //seteamos los datos de auditoria
            $this->audit->setOperationData($process);
            $this->audit->setChkResult($success);
        }
    }

    /**
     * Provee un formulario para la edicion estantes asociados a los almacenes
     *
     * @access  public
     * @param array $data datos a ser modificados
     * @author Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version V 1.0 04/10/2013
     */
    function editEstante($params = NULL) {
        $array_post = $this->input->post();
        $id = NULL;
        if (empty($params) AND isset($array_post['id'])) {
            $id = $this->encrypt->decode($array_post['id']);
            $this->session->set_userdata('estante.id', $id);

            $data = $this->Almacen_model->getAllEstante($id);
            $this->setTargetOn('window2');
            $this->setContent($this->view_engine->form(array('data' => $data)));
        } elseif ($params[0] == 'process') {

            $process = $this->view_engine->process();
            $success = FALSE;
            $id = $this->session->userdata('estante.id');
            if ($process['result']) {
                $params = array(
                    'format_type' => 'update',
                    'data' => $process['data']
                );

                $data = $this->_format($params);

                $success = $this->almacen_model->updateEstante($id, $data);

                if ($success)
                    $this->session->unset_userdata('estante.id');

                $this->setContent($success);
            } else {
                $this->setContent($process['validation_error']);
            }

            // -----------------------------
            //seteamos los datos de auditoria
            $operation_data = $process;
            $this->audit->setChkResult($success);
            // -----------------------------
        }

        // -----------------------------
        //seteamos los datos de auditoria
        $operation_data['id'] = $id;
        $this->audit->setOperationData($operation_data);
        // -----------------------------
    }

    /**
     * Provee una vista de los detalles del estante seleccionado
     *
     * @access  public
     * @author Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version V 1.0 03/10/2013
     */
    function detailEstante($params = NULL) {
        $array_post = $this->input->post();
        $id = NULL;
        if (empty($params) AND isset($array_post['id'])) {
            $id = $this->encrypt->decode($array_post['id']);
            $this->session->set_userdata('estante.id', $id);
            $data = $this->Almacen_model->getAllEstante($id);

            $this->setTargetOn('window2');


            $this->setContent($this->view_engine->form(array('data' => $data)));
        }
    }

    /**
     * Muestra los tramos
     *
     * @access  public
     * @param     array    $params    parametros a ser procesados en el metodo
     * @return  array    dato a ser retornado
     */
    public function listTramo($params) {
        $this->listAll();
    }

    /**
     * Provee un formulario para la carga de nuevos tramos asociados a los estantes
     *
     * @access  public
     * @param array $data datos obtenidos del formulario de creacion de estantes
     * @author Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version V 1.0 03/10/2013
     */
    function createTramo() {
        $this->setTargetOn('window2');
        $id = $this->session->userdata('almacen.id');
        if (!$this->input->post()) {
            $this->setContent($this->view_engine->form());
        } elseif ($this->input->post()) {
            $process = $this->view_engine->process();
            $success = FALSE;
            if ($process['result']) {
                $params = array('format_type' => 'insert', 'data' => $process['data']);

                $data = $this->_format($params);
                $data['id_estante'] = $data['descripcion'];
                $data['descripcion'] = $data['tramo'];
                unset($data['tramo']);

                $success = $this->Almacen_model->insertTramo($data);

                if ($success) {
                    $this->session->unset_userdata('almacen.id');
                }
                $this->setContent($success);
            } else {
                $this->setContent($process['validation_error']);
            }

            //seteamos los datos de auditoria
            $this->audit->setOperationData($process);
            $this->audit->setChkResult($success);
        }
    }

     /**
     * Provee un formulario para la edicion tramos asociados a los estantes
     *
     * @access  public
     * @param array $data datos a ser modificados
     * @author Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version V 1.0 03/10/2013
     */
    function editTramo($params = NULL) {
        $array_post = $this->input->post();
        $id = NULL;
        if (empty($params) AND isset($array_post['id'])) {
            $id = $this->encrypt->decode($array_post['id']);
            $this->session->set_userdata('tramo.id', $id);
            $data = $this->Almacen_model->getAllTramo($id);

            $this->setTargetOn('window2');


            $this->setContent($this->view_engine->form(array('data' => $data)));
        } elseif ($params[0] == 'process') {

            $process = $this->view_engine->process();
            $success = FALSE;
            $id = $this->session->userdata('tramo.id');
            if ($process['result']) {
                $params = array(
                    'format_type' => 'update',
                    'data' => $process['data']
                );

                $data = $this->_format($params);
                $data['id_estante'] = $data['descripcion'];
                $data['descripcion'] = $data['tramo'];
                unset($data['tramo']);

                $success = $this->almacen_model->updateTramo($id, $data);

                if ($success)
                    $this->session->unset_userdata('almacen.id');

                $this->setContent($success);
            } else {
                $this->setContent($process['validation_error']);
            }

            // -----------------------------
            //seteamos los datos de auditoria
            $operation_data = $process;
            $this->audit->setChkResult($success);
            // -----------------------------
        }

        // -----------------------------
        //seteamos los datos de auditoria
        $operation_data['id'] = $id;
        $this->audit->setOperationData($operation_data);
        // -----------------------------
    }

    /**
     * Provee una vista de los detalles del estante seleccionado
     *
     * @access  public
     * @author Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version V 1.0 03/10/2013
     */
    function detailTramo($params = NULL) {
        $array_post = $this->input->post();
        $id = NULL;
        if (empty($params) AND isset($array_post['id'])) {
            $id = $this->encrypt->decode($array_post['id']);
            $this->session->set_userdata('tramo.id', $id);
            $data = $this->Almacen_model->getAllTramo($id);

            $this->setTargetOn('window2');


            $this->setContent($this->view_engine->form(array('data' => $data)));
        }
    }

}

