<script>
    var toggle_check = false;
    $(function() {
        customFields();
        $("select[name='filtro']").change(function(e) {
            var _this = $("select[name='filtro']");
            //            console.log(_this.val() );
            if (_this.val() == 2) {
                $(".fabricante").css('display', 'block');
                $(".categoria").css('display', 'none');
                $(".cirugia").css('display', 'none');
                $("select[name='id_articulacion']").val('').trigger('liszt:updated');
                $("select[name='id_proveedor']").val('').trigger('liszt:updated');
                $("select[name='id_tipo_articulo']").val('').trigger('liszt:updated');
                $("select[name='id_lesion']").val('').trigger('liszt:updated');
                $("select[name='id_tecnica']").val('').trigger('liszt:updated');
                $('#635_chzn').addClass('chzn-disabled');
                $('#634_chzn').addClass('chzn-disabled');
                //                $("#msg-sistema").css('display', 'block');
                //                $("#msg-sistema").html('El ajuste se aplicara de acuerdo al filtro seleccionado...');

            }
            else if (_this.val() == 1) {
                $(".fabricante").css('display', 'none');
                $(".categoria").css('display', 'none');
                $(".cirugia").css('display', 'block');
                $("select[name='id_articulacion']").val('').trigger('liszt:updated');
                $("select[name='id_proveedor']").val('').trigger('liszt:updated');
                $("select[name='id_tipo_articulo']").val('').trigger('liszt:updated');
                $("select[name='id_lesion']").val('').trigger('liszt:updated');
                $("select[name='id_tecnica']").val('').trigger('liszt:updated');
                $('#635_chzn').addClass('chzn-disabled');
                $('#634_chzn').addClass('chzn-disabled');
                //                $("#msg-sistema").css('display', 'block');
                //                $("#msg-sistema").html('A continuacion seleccione el/los articulo(s)  para aplicar el ajuste...');

            } else if (_this.val() == 3) {
                $(".fabricante").css('display', 'none');
                $(".cirugia").css('display', 'none');
                $(".categoria").css('display', 'block');
                $("select[name='id_articulacion']").val('').trigger('liszt:updated');
                $("select[name='id_proveedor']").val('').trigger('liszt:updated');
                $("select[name='id_tipo_articulo']").val('').trigger('liszt:updated');
                $("select[name='id_lesion']").val('').trigger('liszt:updated');
                $("select[name='id_tecnica']").val('').trigger('liszt:updated');
                $('#635_chzn').addClass('chzn-disabled');
                $('#634_chzn').addClass('chzn-disabled');
                //                $("#msg-sistema").html('El ajuste se aplicara forma general...');
            } else if (_this.val() == '') {
                $(".avanzada").css('display', 'none');
                $(".especifica").css('display', 'none');
                $("select[name='id_articulacion']").val('').trigger('liszt:updated');
                $("select[name='id_proveedor']").val('').trigger('liszt:updated');
                $("select[name='id_tipo_articulo']").val('').trigger('liszt:updated');
                $("select[name='id_lesion']").val('').trigger('liszt:updated');
                $("select[name='id_tecnica']").val('').trigger('liszt:updated');
                $('#635_chzn').addClass('chzn-disabled');
                $('#634_chzn').addClass('chzn-disabled');
                //                $("#msg-sistema").empty();
                //                $("#msg-sistema").css('display', 'none');
            }
        });
    });
    $(".fabricante").css('display', 'none');
    $(".cirugia").css('display', 'none');
    $(".categoria").css('display', 'none');

    var $modal = $('#disabled_article');
    //    var articulos='';
    function validaArticulo() {
        var filtros = $('#form_<?php echo $operation_id ?>').serialize();
        $('#table-no_borrable').empty();
        $('#table-borrable').empty();
        $('#err-msg').addClass('hide');

        $modal.modalmanager('loading');
        $('#disabled_article .modal-backdrop').css('z-index', '1500');
        $('#disabled_article .modal-scrollable').css('z-index', '1510');
        $('#disabled_article .loading-spinner').css('z-index', '1510');
        $.ajax({
            url: '<?= base_url() ?>inventario/articulo/EXC_valida_articulos',
            data: filtros,
            success: function(response) {
                $modal.modalmanager('loading');
                if (response.success && response.data) {
                    $('#table-no_borrable').append('<tr><th>Codigo</th><th>Modelo</th></tr>');
                    if(response.data.valid_no_borrable){
                        $.each(response.data.no_borrable, function(k, v) {
                            $('#table-no_borrable').append('<tr><td>' + v.codigo + '</td><td>' + v.modelo + '</td></tr>');
                            $('#form_articulos_no_borrable').append('<input type="hidden" name="articulos_no_borrable"  value="' + v.id + '">');
                        });
                    }
                    if(response.data.valid_borrable){
                        $.each(response.data.borrable, function(k, v) {
                            $('#table-borrable').append('<tr><td>' + v.codigo + '</td><td>' + v.modelo + '</td></tr>');
                            $('#form_articulos_borrable').append('<input type="hidden" name="articulos_borrable"  value="' + v.id + '">');
                        });
                    }
                    $('#modal_result').modal()

                } else if (!response.success) {
                    $('#err-msg').text('');
                    $('#err-msg').append(response.msg);
                    $('#err-msg').removeClass('hide');
                    $("select[name='id_articulacion']").val('').trigger('liszt:updated');
                    $("select[name='filtro']").val('').trigger('liszt:updated');
                    $("select[name='id_proveedor']").val('').trigger('liszt:updated');
                    $("select[name='id_tipo_articulo']").val('').trigger('liszt:updated');
                    $("select[name='id_lesion']").val('').trigger('liszt:updated');
                    $("select[name='id_tecnica']").val('').trigger('liszt:updated');
                    $('#635_chzn').addClass('chzn-disabled');
                    $('#634_chzn').addClass('chzn-disabled');
                                        $(".fabricante").css('display', 'none');
                    $(".categoria").css('display', 'none');
                    $(".cirugia").css('display', 'none');
                } else {
                    $('#err-msg').text('');
                    $('#err-msg').append('Disculpe no se encuentran articulos con su criterio de busqueda');
                    $(".fabricante").css('display', 'none');
                    $(".categoria").css('display', 'none');
                    $(".cirugia").css('display', 'none');
                }
            }, complete: function() {
              
                if ($('#table-no_borrable >tbody >tr').length == 1) {
                    $('#block_no_borrable').css('display', 'none');
                }
                if ($('#table-borrable >tbody >tr').length == 1) {
                    $('#block_borrable').css('display', 'none');
                }
            }
        });
    }

    $("#borrable").toggle();

    $("#detail_content_borrable").click(function() {
        $("#borrable").toggle("slide");
        if (toggle_check) {
            $("#detail_content_borrable").html("Ver articulos a eliminar");
            toggle_check = false;
        } else {
            $("#detail_content_borrable").html("Ocultar Detalle del Articulo");
            toggle_check = true;
        }
    });
    $("#no_borrable").toggle();
    $("#detail_content_no_borrable").click(function() {
        $("#no_borrable").toggle("slide");
        if (toggle_check) {
            $("#detail_content_no_borrable").html("Ver Articulos a deshabilitar");
            toggle_check = false;
        } else {
            $("#detail_content_no_borrable").html("Ocultar Detalle del Articulo");
            toggle_check = true;
        }
    });
    function updateArticles() {
        var $modal = $('#disabled_article');

        var articulos = $('#form_articulos_no_borrable').serializeArray();
        $modal.modalmanager('loading');
        $('#disabled_article .modal-backdrop').css('z-index', '1500');
        $('#disabled_article .modal-scrollable').css('z-index', '1510');
        $('#disabled_article .loading-spinner').css('z-index', '1510');
        $.ajax({
            url: '<?= base_url() ?>inventario/articulo/EXC_disableArticlesMassive',
            data: {articulo: articulos},
            type: 'post',
            success: function(response) {
                $modal.modalmanager('loading');
                if (response.success) {
                    $('#succ').removeClass('hide');
                    $('#block_no_borrable').css('display', 'none');
                    $('input[name=articulos_no_borrable]').val('');


                }
            }, complete: $('#content_disabled').addClass('hide')
        });
    }



    function deleteArticles() {
        var $modal = $('#disabled_article');

        var articulos = $('#form_articulos_borrable').serializeArray();
        $modal.modalmanager('loading');
        $('#disabled_article .modal-backdrop').css('z-index', '1500');
        $('#disabled_article .modal-scrollable').css('z-index', '1510');
        $('#disabled_article .loading-spinner').css('z-index', '1510');
        $.ajax({
            url: '<?= base_url() ?>inventario/articulo/EXC_deleteArticlesMassive',
            data: {articulo: articulos},
            type: 'post',
            success: function(response) {
                $modal.modalmanager('loading');
                if (response.success) {
                    $('#del').removeClass('hide');
                    $('#block_borrable').css('display', 'none');
                    $('input[name=articulos_borrable]').val('');


                }
            }, complete: $('#content_disabled').addClass('hide')
        });
    }





</script>
<div id="alert_<?php echo $operation_id ?>" ></div>
<div id="modal_<?php echo $operation_id ?>" ></div>
<form id="form_articulos_borrable"></form>
<form id="form_articulos_no_borrable"></form>
<div id="err-msg" class="alert alert-error hide">
    <button type="button" class="close" data-dismiss="alert">×</button>
</div>
<h3><?php echo $title ?></h3>
<div id="disabled_article" ></div>
<form id="form_<?php echo $operation_id ?>" name="form_<?php echo $operation_id ?>" action="#" method="POST">
    <ul>
        <li id="li-filtro-<?php echo $operation_id ?>">
            <div class="control-group">
                <label class="control-label" for="filtro">Tipo de eliminacion<span style='color:red;font-size: 16px'> *</span></label>
                <div class="controls">
                    <select name="filtro"
                            id="filtro<?php echo $operation_id ?>"
                            data-placeholder="Seleccione el filtro de eliminacion"
                            class="chzn-select"
                            data-rules="required|integer"
                            data-display="Filtro de Ajuste"
                            data-content="Seleccione el tipo de eliminacion"
                            title="Filtro de Ajuste"
                            >
                        <option value=""></option>
                        <option value="3">Por Tipo de Articulo</option>
                        <option value="2">Por Fabricante</option>
                        <option value="1">Por Cirugia</option>
                    </select>
                    <script type="text/javascript">
                    </script>
                </div>
            </div>
        </li>
        <li class="cirugia" id="li-id_articulacion-12106">
            <div class="control-group">
                <label class="control-label" for="633">Articulación<span style='color:red;font-size: 16px'> *</span></label>
                <div class="controls">
                    <select name="id_articulacion"
                            id="633"
                            data-placeholder="Seleccione la Articulación"
                            class="chzn-select"
                            data-rules="integer"
                            data-display="Articulación"
                            data-content="Indique la articulación lesionada"
                            title="Articulación"
                            >
                        <option value=""></option>
                        <?php
                        if (isset($articulacion) && sizeof($articulacion) > 0):
                            foreach ($articulacion as $key => $value):
                                ?>
                                <option value="<?php echo $value['id']; ?>"  ><?php echo $value['nombre']; ?></option>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </select>
                    <script type="text/javascript">
                    </script>
                    <span id="button-id_articulacion"></span>
                </div>
            </div>
        </li>
        <li class=" cirugia " id="li-id_lesion-12106">
            <div class="control-group">
                <label class="control-label" for="634">Lesión<span style='color:red;font-size: 16px'> *</span></label>
                <div class="controls">
                    <select name="id_lesion"
                            id="634"
                            data-placeholder="Seleccione la Lesión"
                            class="chzn-select"
                            data-rules="integer"
                            data-display="Lesión"
                            data-content="Indique el tipo de lesión"
                            title="Lesión"
                            >

                        <option value=""></option>

                        <?php
                        if (isset($lesion) && sizeof($lesion) > 0):
                            foreach ($lesion as $key => $value):
                                ?>
                                <option value="<?php echo $value['id']; ?>" class='<?php echo $value['id_parent']; ?>' ><?php echo $value['nombre']; ?></option>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </select>
                    <script type="text/javascript">

                        $(function() {
                            $('#634').chained('#633');
                            $('#634').trigger('liszt:updated');
                            $('#633').bind('change', function() {
                                $('#634').trigger('liszt:updated')
                            });
                        });
                    </script>
                    <span id="button-id_lesion"></span>
                </div>
            </div>
        </li>
        <li class=" cirugia " id="li-id_tecnica-12106">
            <div class="control-group">
                <label class="control-label" for="635">Técnica</label>
                <div class="controls">
                    <select name="id_tecnica"
                            id="635"
                            data-placeholder="Seleccione la Técnica Utilizada"
                            class="chzn-select"
                            data-rules="integer"
                            data-display="Técnica"
                            data-content="Indique la técnica utilizada en la cirugía"
                            title="Técnica"
                            >
                        <option value=""></option>
                        <?php
                        if (isset($tecnica) && sizeof($tecnica) > 0):
                            foreach ($tecnica as $key => $value):
                                ?>
                                <option value="<?php echo $value['id']; ?>" class='<?php echo $value['id_parent']; ?>' ><?php echo $value['nombre']; ?></option>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </select>
                    <script type="text/javascript">
                        $(function() {
                            $('#635').chained('#634');
                            $('#635').trigger('liszt:updated');
                            $('#634').bind('change', function() {
                                $('#635').trigger('liszt:updated')
                            });
                        });
                    </script>
                    <span id="button-id_tecnica"></span>
                </div>
            </div>
        </li>
        <li class="categoria" id=" li-id_tipo-12106">
            <div class="control-group">
                <label class="control-label" for="633">Tipo de Articulo<span style='color:red;font-size: 16px'> </span></label>
                <div class="controls">
                    <select name="id_tipo_articulo"
                            id=""
                            data-placeholder="Seleccione el tipo articulo"
                            class="chzn-select"
                            data-rules="integer"
                            data-display="Tipo de articulo"
                            data-content="Indique el tipo de articulo"
                            title="Tipo de articulo"
                            >
                        <option value=""></option>
                        <?php
                        if (isset($tipo_articulo) && sizeof($tipo_articulo) > 0):
                            foreach ($tipo_articulo as $key => $value):
                                ?>
                                <option value="<?php echo $value['id']; ?>"  ><?php echo $value['_label']; ?></option>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </select>
                    <script type="text/javascript">
                    </script>
                    <span id="button-id_proveedor"></span>
                </div>
            </div>
        </li>
        <li class="fabricante" id=" li-id_proveedor-12106">
            <div class="control-group">
                <label class="control-label" for="633">Fabricante<span style='color:red;font-size: 16px'> </span></label>
                <div class="controls">
                    <select name="id_proveedor"
                            id="1780"
                            data-placeholder="Seleccione el fabricante"
                            class="chzn-select"
                            data-rules="integer"
                            data-display="Fabricante"
                            data-content="Indique el Fabricante"
                            title="Fabricante"
                            >
                        <option value=""></option>
                        <?php
                        if (isset($fabricante) && sizeof($fabricante) > 0):
                            foreach ($fabricante as $key => $value):
                                ?>
                                <option value="<?php echo $value['id']; ?>"  ><?php echo $value['nombre']; ?></option>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </select>
                    <script type="text/javascript">
                    </script>
                    <span id="button-id_proveedor"></span>
                </div>
            </div>
        </li>


        <div id="modal_result" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Articulos encontrados</h3>
            </div>
            <div id="succ" class="alert alert-success hide" >
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>El(Los) Articulo(s) ha sido deshabilitado(s)</strong> ya no podra(n) ser incluido(s) en nuevas cotizaciones.
            </div>

            <div class="modal-body">
                <div id="block_no_borrable">
                    Algunos articulos no pueden ser eliminados, solo deshabilitados <br/> 
                    &nbsp;&nbsp;&nbsp;
                    <div class="form-actions ">
                        <button id="detail_content_no_borrable" class="btn btn-info update">Ver Articulos por deshabilitar</button>
                        <input type="button" class="btn btn-info" onclick="updateArticles()" value="Deshabilitar" />
                    </div>
                    <div id="no_borrable">
                        <table id="table-no_borrable" class="table table-bordered table-striped">
                            <tr><td>Codigo</td><td>Modelo</td></tr>
                        </table>

                    </div>

                </div>  
                <div id="del" class="alert alert-block hide" >
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>Los Articulos ha sido eliminados</strong> definitivamente
                </div>
                <div id="block_borrable">
                    &nbsp;         
                    &nbsp;         
                    &nbsp; 
                    &nbsp;&nbsp;&nbsp;
                    <div class="form-actions ">
                        <button id="detail_content_borrable" class="btn btn-danger update">Ver articulos a eliminar</button>
                        <input type="button" class="btn btn-danger" onclick="deleteArticles()" value="Eliminar" />
                    </div>
                    <div id="borrable">
                        <table id="table-borrable" class="table table-bordered table-striped">
                            <tr><td>Codigo</td><td>Modelo</td></tr>
                        </table>

                    </div>

                </div>

                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
                    <!--<button class="btn btn-primary">Save changes</button>-->
                </div>
            </div>
        </div>
        <li><br/>
            <div class="form-actions ">

                <input type="button" class="btn btn-primary" onclick="validaArticulo()" value="<?php echo $button_submit ?>" />
            </div>
        </li>
    </ul>
</form>