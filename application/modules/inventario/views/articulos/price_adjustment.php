<script type="text/javascript">
    
    build_table_articles();
    $(function() {


        var form = 'form_<?php echo $operation_id ?>';
        var url_process = '<?php echo base_url($url . '/process'); ?>';
        var modal = '#modal_<?php echo $operation_id ?>';
        var reload_content = '<?php echo base_url($url); ?>';
        var _alert = '#alert_<?php echo $operation_id ?>';
        formWindow(form, url_process, modal, _alert, reload_content, true);

        // $('#table_<?php echo $operation_id ?>').dataTable();
        customFields();
        $("select[name='filtro']").change(function(e) {
            var _this = $("select[name='filtro']");
            if (_this.val() == 2) {
                $(".avanzada").css('display', 'block');
                $(".especifica").css('display', 'none');
                $("#msg-sistema").css('display', 'block');
                $("#msg-sistema").html('El ajuste se aplicara de acuerdo al filtro seleccionado...');

            }
            else if (_this.val() == 1) {
                $(".avanzada").css('display', 'none');
                $(".especifica").css('display', 'block');
                $("#msg-sistema").css('display', 'block');
                $("#msg-sistema").html('A continuacion seleccione el/los articulo(s)  para aplicar el ajuste...');

            } else if (_this.val() == 3) {
                $(".avanzada").css('display', 'none');
                $(".especifica").css('display', 'none');
                $("#msg-sistema").css('display', 'block');
                $("#msg-sistema").html('El ajuste se aplicara de forma general a todos los articulos...');
            } else if (_this.val() == '') {
                $(".avanzada").css('display', 'none');
                $(".especifica").css('display', 'none');
                $("#msg-sistema").empty();
                $("#msg-sistema").css('display', 'none');
            }


        });
    });
    $(".avanzada").css('display', 'none');
    $(".especifica").css('display', 'none');
    $("#msg-sistema").empty();
    $("#msg-sistema").css('display', 'none');
    function sure(){
        var form = '#form_<?php echo $operation_id ?>';
        var msg='Se aplicara el ajuste de precios de acuerdo a su eleccion. Desea continuar? ';
        bootbox.confirm(msg, function(rs) {
            if (rs === true) {
                $(form).submit();
            }
        });
    }
</script>
<div id="alert_<?php echo $operation_id ?>" ></div>
<div id="modal_<?php echo $operation_id ?>" ></div>
<h3><?php echo $title ?></h3>


<form id="form_<?php echo $operation_id ?>" name="form_<?php echo $operation_id ?>" action="#" method="POST" >
    <table><tr><td>
                <ul class="row" style="margin-top: 0">

                    <li id="li-filtro-<?php echo $operation_id ?>">
                        <div class="control-group">
                            <label class="control-label" for="filtro">Filtro de Ajuste<span style='color:red;font-size: 16px'> *</span></label>
                            <div class="controls">
                                <select name="filtro"
                                        id="filtro<?php echo $operation_id ?>"
                                        data-placeholder="Seleccione el filtro de Ajuste"
                                        class="chzn-select"
                                        data-rules="required|integer"
                                        data-display="Filtro de Ajuste"
                                        data-content="Seleccione el filtro de Ajuste"
                                        title="Filtro de Ajuste"
                                        >
                                    <option value=""></option>
                                    <option value="3">General</option>
                                    <option value="1">Especifico</option>
                                    <option value="2">Avanzado</option>
                                </select>
                                <script type="text/javascript">
                                </script>
                            </div>
                        </div>
                    </li>
                    <li class='avanzada' id="li-id_articulacion-<?php echo $operation_id ?>">
                        <div class="control-group">
                            <label class="control-label" for="633">Articulación</label>
                            <div class="controls">
                                <select name="id_articulacion"
                                        id="633"
                                        data-placeholder="Seleccione la Articulación"
                                        class="chzn-select"
                                        data-display="Articulación"
                                        data-content="Indique la articulación lesionada"
                                        title="Articulación"
                                        >
                                    <option value=""></option>
                                    <option value="todas" class='2' >TODAS</option>
                                    <?php
                                    if (isset($articulacion) && sizeof($articulacion) > 0):
                                        foreach ($articulacion as $key => $value):
                                            ?>
                                            <option value="<?php echo $value['id']; ?>"  class='2'><?php echo $value['nombre']; ?></option>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </select>
                                <script type="text/javascript">
                                </script>
                                <span id="button-id_articulacion"></span>
                            </div>
                        </div>
                    </li>
                    <script type="text/javascript">

                        $(function() {
                            $('#633').chained('#filtro<?php echo $operation_id ?>');
                            $('#633').trigger('liszt:updated');
                            $('#filtro<?php echo $operation_id ?>').bind('change', function() {
                                $('#633').trigger('liszt:updated')
                            });
                        });
                    </script>
                    <li class='avanzada' id="li-id_lesion-<?php echo $operation_id ?>">
                        <div class="control-group">
                            <label class="control-label" for="634">Lesión</label>
                            <div class="controls">
                                <select name="id_lesion"
                                        id="634"
                                        data-placeholder="Seleccione la Lesión"
                                        class="chzn-select"
                                        data-display="Lesión"
                                        data-content="Indique el tipo de lesión"
                                        title="Lesión"
                                        >

                                    <option value=""></option>

                                    <?php
                                    if (isset($lesion) && sizeof($lesion) > 0):
                                        foreach ($lesion as $key => $value):
                                            $class = $value['id_parent'];
                                            ?>
                                            <option value="<?php echo $value['id']; ?>" class='<?php echo $value['id_parent']; ?>' ><?php echo $value['nombre']; ?></option>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>

                                </select>
                                <script type="text/javascript">

                                    $(function() {
                                        $('#634').chained('#633');
                                        $('#634').trigger('liszt:updated');
                                        $('#633').bind('change', function() {
                                            $('#634').trigger('liszt:updated')
                                        });
                                    });
                                </script>
                                <span id="button-id_lesion"></span>
                            </div>
                        </div>
                    </li>
                    <li class='avanzada' id="li-id_tecnica-<?php echo $operation_id ?>">
                        <div class="control-group">
                            <label class="control-label" for="635">Técnica</label>
                            <div class="controls">
                                <select name="id_tecnica"
                                        id="635"
                                        data-placeholder="Seleccione la Técnica Utilizada"
                                        class="chzn-select"
                                        data-rules="integer"
                                        data-display="Técnica"
                                        data-content="Indique la técnica utilizada en la cirugía"
                                        title="Técnica"
                                        >
                                    <option value=""></option>
                                    <?php
                                    if (isset($tecnica) && sizeof($tecnica) > 0):
                                        foreach ($tecnica as $key => $value):
                                            ?>
                                            <option value="<?php echo $value['id']; ?>" class='<?php echo $value['id_parent']; ?>' ><?php echo $value['nombre']; ?></option>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </select>
                                <script type="text/javascript">
                                    $(function() {
                                        $('#635').chained('#634');
                                        $('#635').trigger('liszt:updated');
                                        $('#634').bind('change', function() {
                                            $('#635').trigger('liszt:updated')
                                        });
                                    });
                                </script>
                                <span id="button-id_tecnica"></span>
                            </div>
                        </div>
                    </li>
                </ul>
            </td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <div id="price_adjustment" >
                    <!--        <div class="span5" id="articles_selections">
                                <p>
                                    <label for="select_all"><b>Seleccionar todos los articulos :</b></label>
                                    <input type="checkbox" id="select_all" name="select_all" value="true"/>
                                </p>
                                <table id="table_articles_selections" class="js-table table table-striped table-bordered dataTable"><thead><tr><th></th><th></th><th>Modelo</th><th>Descripción</th></tr></thead><tbody></tbody></table>
                            </div>-->
                    <div class="span4 row" id="price_selections">
                        <div class="span4">
                            <h5>Seleccione los Tipo de Precio que desea ajustar:</h5>
                            <ul>
                                <?php foreach ($tipos_precio as $k => $tipo): ?>
                                    <li> 
                                        <label for="<?php echo $tipo['id'] ?>"><b><?php echo (isset($tipo['nombre']) ? $tipo['nombre'] : ''); ?> :</b></label>
                                        <input type="checkbox" id="<?php echo $tipo['id'] ?>" name="check_price[<?php echo $k ?>]" value="<?php echo $tipo['id'] ?>"/>

                                        <label for="porcentaje_<?php echo $tipo['id'] ?>"><b> </b></label>
                                        <input type="text" id="porcentaje_<?php echo $tipo['id'] ?>" name="porcentaje[<?php echo $k ?>]" maxlength="3" value="" data-rules="required integer"/> %
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <div class="span4 hide" id="formula">
                            <label for="input_formula"><b>Ingrese la Formula que desea aplicar: </b></label>
                            <input type="text" id="input_formula" name="input_formula" placeholder="(([POR]/100))*[ART]"/>
                        </div>
                        <!--                        <div class="span4" id="placeholders">
                                                    Placeholders
                                                </div>-->
                    </div>
                </div>
            </td></tr></table>
    <div  id='msg-sistema' class=" form-actions span9 row">

    </div>
    <div class="especifica row">

    </div>
    <div  class="especifica">
        <div  id="articles_selections">
            <a href="#" role="button" class="btn btn-primary" data-toggle="modal" onclick="show_articles(event, 'art', 'table_articles_selections');">Seleccionar Articulo</a><br/><br/>
            <table id="table_articles_selections" class="js-table table table-striped table-bordered dataTable"><thead><tr><th></th><th></th><th>Modelo</th><th>Descripción</th></tr></thead><tbody></tbody></table>
        </div>
    </div>
    <div class="row">
        <div class="span9 form-actions">
            <input type="button" class="btn btn-primary" onclick="sure()"value="<?php echo $button_submit ?>" />
        </div>
    </div>
</form>