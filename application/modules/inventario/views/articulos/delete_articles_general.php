<script>
    function updateArticles() {
        var $modal = $('#modal_disabled_article');
        $modal.modalmanager('loading');
        $.ajax({
            url: '<?= base_url() ?>inventario/articulo/EXC_disableArticles',
          
            success: function(response) {
                $modal.modalmanager('loading');
                if (response.success) {
                    $('#succ').removeClass('hide');
                    $('#foot').addClass('hide');

                }
            }, complete: reloadGrid
        });
    }

</script>
<?php //print_r($borrable);die('hola'); ?>
<div id="disabled_article" ></div>
<div id="succ" class="alert alert-success hide" >
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>El Articulo ha sido deshabilitado</strong> ya no podra ser incluido en nuevas cotizaciones.
</div>
<div class="control-label">
    <p>
        <b>Lo sentimos, el Articulo NO se puede eliminar porque esta siendo usado por la (s) cotizacion(es)</b>
    </p>
</div>
<div style="margin-top: 10px; margin-left: 20px">
   <th>Codigo</th>
    <?php foreach ($borrable as $k => $val): ?>
       
            <table class=" table table-bordered table-striped">
                <!--<th>Paciente</th><th>Medico</th><th>Fecha de Cotizacion</th>-->
                <tr>
                    <td><?php echo $val['codigo'] ?></td>
<!--                    <td><?php echo $val['id_paciente'] ?> </td>
                    <td><?php echo $val['id_medico'] ?></td>
                    <td><?php echo $val['fecha_cotizacion'] ?></td>-->
                </tr>
            </table>
        
    <?php endforeach; ?>

</div>
<div id="foot" class="modal-footer">
    <button class="btn" data-dismiss="modal">Cancelar</button>
    <input type="button" class="btn btn-primary update" onclick="updateArticles()" value="Deshabilitar">
</div>