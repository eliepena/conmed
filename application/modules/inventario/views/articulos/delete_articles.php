<script>
    var toggle_check=false;
    function updateArticles() {
        var $modal = $('#disabled_article');
        $modal.modalmanager('loading');
        $.ajax({
            url: '<?= base_url() ?>inventario/articulo/EXC_disableArticles',
            success: function(response) {
                $modal.modalmanager('loading');
                if (response.success) {
                    $('#succ').removeClass('hide');
                    $('#foot').addClass('hide');
                    $('#content_disabled').addClass('hide');

                }
            }, complete: reloadGrid
        });
    }
    $( "#detail_content_disabled" ).toggle();
    $( "#detail_content" ).click(function() {
        $( "#detail_content_disabled" ).toggle( "slide" );
        if(toggle_check){
            $( "#detail_content" ).html( "Ver Detalle del Articulo" );
            toggle_check=false;
        }else{
            $( "#detail_content" ).html( "Ocultar Detalle del Articulo" );
            toggle_check=true;
        }
    });

</script>
<div id="disabled_article" ></div>
<div id="succ" class="alert alert-success hide" >
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>El Articulo ha sido deshabilitado</strong> ya no podra ser incluido en nuevas cotizaciones.
</div>

<div id="content_disabled">
    <div class="control-label ">
        <p>
            &nbsp; <b>Lo sentimos, el Articulo NO se puede eliminar porque esta siendo usado por la (s) cotizacion(es)</b><br/>

            &nbsp; <b>Solo podra deshabilitarlo de forma parcial.</b>

        </p>
    </div>
    &nbsp;&nbsp;&nbsp;<button id="detail_content" class="btn btn-primary update">Ver Detalle del Articulo</button>
    <div id="detail_content_disabled"style="margin-top: 10px; margin-left: 20px">

        <?php foreach ($data['data'] as $k => $val): ?>
            <?php if ($val['tipo_cot'] == 'normal'): ?>
                <table class=" table table-bordered table-striped">
                    <th>Codigo</th><th>Paciente</th><th>Medico</th><th>Fecha de Cotizacion</th>
                    <tr>
                        <td><?php echo $val['codigo'] ?></td>
                        <td><?php echo $val['id_paciente'] ?> </td>
                        <td><?php echo $val['id_medico'] ?></td>
                        <td><?php echo $val['fecha_cotizacion'] ?></td>
                    </tr>
                </table>
            <?php elseif ($val['tipo_cot'] == 'libre'): ?>
                <table class=" table table-bordered table-striped">
                    <th>Codigo</th><th>Cliente</th><th>Cedula/Rif</th><th>Fecha de Cotizacion</th>
                    <tr>
                        <td><?php echo $val['codigo'] ?></td>
                        <td><?php echo $val['id_cliente'] ?> </td>
                        <td><?php echo $val['cedula_rif'] ?></td>
                        <td><?php echo $val['fecha_cotizacion'] ?></td>
                    </tr>
                </table>
            <?php endif; ?>
        <?php endforeach; ?>

    </div> 
    <div id="foot" class="footer form-actions modal-footer">
        <button class="btn" data-dismiss="modal">Cancelar</button>
        <input type="button" class="btn btn-primary update" onclick="updateArticles()" value="Deshabilitar">
    </div>

</div>