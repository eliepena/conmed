<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of almacen_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad almacen
 * y sus entidades dependientes o extendidas
 *
 * @package Modules
 * @category Model
 * @version V-1.0 17/01/13 03:23 PM
 */
class Almacen_model extends AST_Model {

    public function __construct() {

        parent::__construct();
    }

    /**
     * Obtiene los registros de las estantes asociados a un almacen
     *
     * @access  public
     * @param     array    $params    filtros para la consulta
     * @return  array    direcciones del medico

     */
    public function getEstante($params) {
        extract($params);
        $this->setEstante($id);

        $search = $this->filterSearchFields($search_fields);

        return parent::getAll($offset, $search, $sort);
    }

    /**
     * Cuenta el numero total de estantes para un almacen
     *
     * @access  public
     * @param   array    $params    filtros de la consulta
     * @return  integer    total de registros

     */
    public function countEstante($params) {
        extract($params);
        $search = $this->filterSearchFields($search_fields);

        $this->setEstante($id);
        return parent::getCount($search);
    }

    /**
     * obtiene los registros de estante para paginarlos
     *
     * @access  public
     * @param   integer    $id    filtros de la consulta
     *

     */
    private function setEstante(&$id) {
        $this->db->select('estante.id');
        $this->db->select('estante.descripcion');
        $this->db->from('business.estante');
        $this->db->where('id_almacen', $id);
    }

    /**
     * Filtra los datos para el listado de estante
     *
     * @access  private
     * @param     array    $search    Search Fields
     * @return  array    Search Fields Filtrados

     */
    private function filterSearchFields($search) {
        $uri = NULL;
        $uri = $this->uri->uri_string();
        //print_r($uri);
        if (strpos($uri, 'listEstante')) {
            if (empty($search))
                return NULL;
            $searching = $search['descripcion'];
            $search = array(
                'estante.descripcion' => $searching,
            );
            return $search;
        }elseif (strpos($uri, 'listTramo')) {
            if (empty($search))
                return NULL;
            $searching = $search['descripcion'];
            $search = array(
                'estante.descripcion' => $searching,
                'tr.descripcion' => $searching,
            );

            return $search;
        }
    }

    /**
     * Inserta los nuevos valores a la tabla estante
     *
     * @access  publico
     * @param     array    $data   datos a ser insertados
     * @return  resultado de la transaccion

     */
    function insertEstante($data) {
        $this->db->trans_start();
        $this->db->insert('business.estante', $data);
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    /**
     * obtiene los datos de un estante dado su id
     *
     * @access  publico
     * @param     integer    $id   identificador de estante
     * @return  array datos correspondientes al estante

     */
    function getAllEstante($id) {
        $this->db->select('estante.id');
        $this->db->select('estante.descripcion as descripcion');
        $this->db->from('business.estante');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $data = $query->result_array();
        return ($query->num_rows() > 0) ? ($data[0]) : false;
    }

    /**
     * actualiza los nuevos valores a la tabla estante
     *
     * @access  publico
     * @param     array    $data   datos a ser actualizados
     * @return  resultado de la transaccion

     */
    function updateEstante($id, $data) {
        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->update('business.estante', $data);
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    /**
     * Obtiene los registros de los estantes y tramos asociados a un almacen
     *
     * @access  public
     * @param     array    $params    filtros para la consulta
     * @return  array    direcciones del medico

     */
    public function getTramo($params) {
        extract($params);
        $this->setTramo($id);

        $search = $this->filterSearchFields($search_fields);

        return parent::getAll($offset, $search, $sort);
    }

    /**
     * Cuenta el numero total de tramos para un almacen
     *
     * @access  public
     * @param   array    $params    filtros de la consulta
     * @return  integer    total de registros

     */
    public function countTramo($params) {
        extract($params);
        $search = $this->filterSearchFields($search_fields);

        $this->setTramo($id);
        return parent::getCount($search);
    }

    /**
     * Cuenta el numero total de tramos para un almacen
     *
     * @access  public
     * @param   array    $params    filtros de la consulta
     * @return  integer    total de registros

     */
    private function setTramo(&$id) {
        $this->table_name = 'estante';
        $this->db->select('tr.id,');
        $this->db->select('estante.descripcion ,');
        $this->db->select('tr.descripcion as tramo');
        $this->db->join('business.tramo tr', 'estante.id=tr.id_estante');
        $this->db->where('estante.id_almacen', $id);
    }

    /**
     * Inserta los nuevos valores a la tabla estante
     *
     * @access  publico
     * @param     array    $data   datos a ser insertados
     * @return  resultado de la transaccion

     */
    function insertTramo($data) {
        $this->db->trans_start();
        $this->db->insert('business.tramo', $data);
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    /**
     * obtiene los datos de un tramo dado su id
     *
     * @access  publico
     * @param     integer    $id   identificador de tramo
     * @return  array datos correspondientes al tramo

     */
    function getAllTramo($id) {
        $this->db->select('tramo.id');
        $this->db->select('tramo.descripcion as tramo');
        $this->db->select('tramo.id_estante as descripcion');
        $this->db->from('business.tramo');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $data = $query->result_array();
        return ($query->num_rows() > 0) ? ($data[0]) : false;
    }

    /**
     * actualiza los nuevos valores a la tabla tramo
     *
     * @access  publico
     * @param     array    $data   datos a ser actualizados
     * @return  resultado de la transaccion

     */
    function updateTramo($id, $data) {
        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->update('business.tramo', $data);
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    /**
     * obtiene los datos de un tramo articulo dado su id
     *
     * @access  publico
     * @param     integer    $id   identificador de tramo_articulos
     * @return  array datos correspondientes al tramo_articulo
     */
    function getTramoArticulo($id) {
        $this->db->select('*');
        $this->db->from('business.tramo_articulo');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $data = $query->result_array();
        return ($query->num_rows() > 0) ? ($data[0]) : false;
    }

}
