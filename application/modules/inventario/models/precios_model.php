<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Precios_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad Precios
 * y sus entidades dependientes o extendidas
 *
 * @package Modules
 * @category Model
 * @version V-1.0 17/01/13 03:23 PM
 */
class Precios_model extends AST_Model {

    public function __construct() {

        parent::__construct();
    }

    public function getByFields($params = array()) {
        //  $this->db->table_name=;
        $table = $this->concatSchemaTable();
        $this->db->select('id, ');
        $this->db->select('nombre, ');
        $this->db->select('identificacion, ');
        $this->db->select('descripcion as descripcion, ');
        $this->db->select('creation_date ');
        $this->db->from($table);
        if (!empty($params)) {
            $value = $params['creation_date'];
            unset($params['creation_date']);
            //se aplica para que la poder realizar busqueda por fecha en el grid
            //------------------------------------------------------
            $params["to_char(creation_date,'DD-MM-YYYY HH:MM:SS')"] = $value;
            //-------------------------------------------------------------


            $this->_ilike($params, 'OR', TRUE);
        }
    }

    /**
     * Devuelve los datos basicos del tipo de precio
     *
     * @access public
     * @param integer $id Identificador del tipo de precio
     * @param string  $campo Campos a mostrar en el select (default:'*')
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 31/07/13 12:35 PM
     */
    function getPrecios($id = NULL, $campo = '') {

        $this->db->select((empty($campo) ? ' * ' : ' ' . $campo . ' '), false);
        $this->db->from('business.precios');
        if ($id):
            $this->db->where(array('id' => $id));
        endif;
        $this->db->where(array('_delete' => '0'));

        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

}