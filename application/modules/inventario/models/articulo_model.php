<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of articulos_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad articulos
 * y sus entidades dependientes o extendidas
 *
 * @package Modules
 * @category Model
 * @version V-1.0 17/01/13 03:23 PM
 */
class Articulo_model extends AST_Model {

    public function __construct() {

        parent::__construct();
    }

    /**
     * obtiene el id del tipo de precio dado su nombre
     *
     * @access public
     * @param strind $name nombre del tipo de precio
     * @author Eliezer Pe#a
     * @version V 1.0 18/09/2013
     */
    function getIdByName($name) {
        $this->db->select('id');
        $this->db->from('business.precios');
        $this->db->where('nombre', $name);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row()->id : FALSE;
    }

    /**
     * obtiene el nombre del tipo de precio dado su id
     *
     * @access public
     * @param integer $id contiene el idnetificador del tipo de precio
     * @author Eliezer Pe#a
     * @version V 1.0 18/09/2013
     */
    function getNameById($id) {
        $this->db->select('nombre');
        $this->db->from('business.precios');
        $this->db->where('id', $id);
        $this->orderBy('nombre');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row()->nombre : FALSE;
    }

    /**
     * create de Articulo.
     *
     * @access public
     * @param array $data contiene los datos del articulo.
     * @author Eliezer Pe#a
     * @version V 1.0 17/09/2013
     */
    function insert($data) {
        $this->db->trans_start();
        //ANTERIOR
//        $this->db->insert('business.articulo', $data['data']);
//        @$id_articulo = $this->db->affected_rows() ? $this->db->insert_id() : FALSE;

        $get_data_articulo = $this->insertWithReturning($data['data'], 'business.articulo');
        $precios = $data['precios'];


        if ($precios) {
            foreach ($precios as $mat => $material)
                $precios[$mat]['id_articulo'] = $get_data_articulo['id'];
            $this->db->insert_batch('business.articulo_precio', $precios);
        }
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    /**
     *  Dado un arreglo se obtiene todas las coincidencias del arreglo por
     * clave/valor, donde clave es el campo la base de datos y valor el valor a
     * ser filtrado.
     *
     * @access public
     * @param array $params contiene los datos del articulo.
     * @author Eliezer Pe#a
     * @version V 1.0 17/09/2013
     */
    public function getByFields($params = array()) {

        $this->db->select('art.id,');
        $this->db->select('art.modelo as modelo,');
        $this->db->select('art.codigo as codigo,');
        $this->db->select('art.descripcion as descripcion,');
        $this->db->select('cat._label AS tipo_articulo');

        $this->db->from('business.articulo as art');
        $this->db->join('dynamic.category cat', 'art.tipo_articulo=cat.id');

        if (!empty($params)) {
            $value = $params['codigo'];
            unset($params['tipo_articulo']);
            $params['art.modelo'] = $value;
            $params['art.codigo'] = $value;
            $params['art.descripcion'] = $value;
            $params['cat._label'] = $value;

            $this->_ilike($params, 'OR', TRUE);
            $this->db->where('art._delete', '0');
        }
    }

    /**
     * Devuelve los precios de un(os) articulo dado su(s) id(s)
     *
     * @access public
     * @param mixed $id contiene el idnetificador del articulo
     * @author Eliezer Pe#a, Jose Rodriguez
     * @version V 2.0 22/01/2014
     */
    function getPrices($id = NULL) {
        $this->db->select('id_precio');
        $this->db->select('precio');
        $this->db->from('business.articulo_precio');
        if (is_array($id)) {
            $this->db->select('id');
            $this->db->select('id_articulo');
            $this->db->where_in('id_articulo', $id);
            $this->db->order_by('id_articulo');
        } else
            $this->db->where('id_articulo', $id);

        $query = $this->db->get();
        // echo $this->db->last_query();die;
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    /**
     * Edit de Articulo.
     *
     * @access public
     * @param array $data contiene los datos del articulo.
     * @param integer $id contiene el idnetificador del articulo
     * @author Eliezer Pe#a
     * @version V 1.0 18/09/2013
     */
    function edit($id, $data) {

        $this->db->trans_start();
        $this->db->where('articulo.id', $id);
        $this->db->update('business.articulo', $data['data']);
        $this->db->delete('business.articulo_precio', array('id_articulo' => $id));
        $precios = $data['precios'];
        if ($precios) {
            foreach ($precios as $mat => $material)
                $precios[$mat]['id_articulo'] = $id;
            $this->db->insert_batch('business.articulo_precio', $precios);
        }
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    /**
     * list de lote.
     *
     * @access public
     * @param integer $id contiene el idnetificador del articulo
     * @author Eliezer Pe#a, Jose Rodriguez
     * @version V 1.1 23/10/2013
     */
    function listCantidad($id = NULL) {
        $this->db->select('ta.lote');
        $this->db->select('\'Almacen:\'||\'<b> \'||"al"."descripcion" || \' </b>- Total:<b> \' || (
            SELECT SUM(ta.cantidad)
            FROM business.tramo_articulo ta
            WHERE ta.id_almacen = "al"."id" AND ta.id_articulo = "ar"."id"
        ) AS "descripcion",', FALSE);
        $this->db->select('ta.id');
        $this->db->select('ta.cantidad');
        $this->db->select('ta.fecha_vencimiento');
        $this->db->select('ta.fecha_ingreso');
        $this->db->where('ar.id', $id);
        $this->db->from('business.almacen al ');
        $this->db->join('business.tramo_articulo ta ', 'al.id=id_almacen');
        $this->db->join('business.articulo ar ', 'ta.id_articulo=ar.id');
        $this->db->group_by('ta.id,al.descripcion,ar.descripcion,ta.cantidad,ta.lote,ta.fecha_vencimiento,ta.fecha_ingreso,al.id, ar.id');
        $query = $this->db->get();
        // echo $this->db->last_query();die;
        return $query->num_rows() > 0 ? $query->result_array() : FALSE;
    }

    /**
     * insert de lote.
     *
     * @access public
     * @param integer $id contiene el identificador del articulo
     * @author Eliezer Pe#a
     * @version V 1.0 18/09/2013
     */
    function insertCantidad($data) {
        $this->db->trans_start();
        $this->db->insert('business.tramo_articulo', $data);
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    /**
     * obtiene un id del articulo, dado el id
     *
     * @access public
     * @param integer $id contiene de la tabla tramo_articulo
     * @author Eliezer Pe#a
     * @version V 1.0 18/09/2013
     */
    function getArticuloID($id) {
        $this->db->select('id_articulo');
        $this->db->from('business.tramo_articulo');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row()->id_articulo : FALSE;
    }

    /**
     * obtiene los datos del lote dado su id
     *
     * @access public
     * @param integer $id contiene de la tabla tramo_articulo
     * @author Eliezer Pe#a
     * @version V 1.0 18/09/2013
     */
    function getTramoArticulo($id = null, $generica = FALSE, $busquedaPorIdArticulo = FALSE) {
        if (!$generica):
            if (!$id):
                return FALSE;
            endif;
        endif;
        $this->db->select('id');
        $this->db->select('id_almacen');
        $this->db->select('lote');
        $this->db->select('cantidad');
        $this->db->select('fecha_ingreso');
        $this->db->select('fecha_vencimiento');
        $this->db->select('id_articulo');
        if (!$generica):
            if ($busquedaPorIdArticulo):
                $this->db->where('id_articulo', $id);
            else:
                $this->db->where('id', $id);
            endif;

        endif;

        $this->db->from('business.tramo_articulo');
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : FALSE;
    }

    /**
     * edit de lote
     *
     * @access public
     * @param integer $id contiene de la tabla tramo_articulo
     * @author Eliezer Pe#a
     * @version V 1.0 18/09/2013
     */
    function editCantidad($id, $data) {
        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->update('business.tramo_articulo', $data['data']);
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    /**
     * delete de lote
     *
     * @access public
     * @param integer $id contiene de la tabla tramo_articulo
     * @author Eliezer Pe#a
     * @version V 1.0 18/09/2013
     */
    function deleteCantidad($id) {
        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->delete('business.tramo_articulo');
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    /**
     * Obtiene todos los articulos provistos por la vista view_articles
     * @return array articulos
     * @version 1.0 15/08/2013 16:55
     * @author Jose Rodriguez <josearodrigueze@gmail.com>
     */
    function get_articles() {
        $this->db->from('business.view_articles');
        $this->db->order_by('id', 'asc');
        // $this->db->limit(50);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    /**
     * Obtiene las cabeceras de los articulos.
     * @return array headers
     * @version 1.0 15/08/2013 16:55
     * @author Jose Rodriguez <josearodrigueze@gmail.com>
     */
    function get_headers_articles() {
        $query = $this->db->query('SELECT column_name AS "sTitle" FROM information_schema.columns WHERE table_name =\'view_articles\' ORDER BY ordinal_position');
        return $query->result_array();
    }

    /**
     * obtiene los datos de los articulos dado un arreglo
     *
     * @access public
     * @param array $param contiene los datos de articulos del archivo
     * @return array datos de articulos coincidentes con los presentes en el archivo csv
     * @author Eliezer Pe#a
     * @version V 1.0 24/10/2013
     */
    function getArticuloCsv($param = array()) {
        $this->db->select('id');
        $this->db->select('modelo');
        $this->db->select('codigo');
        $this->db->where_in('modelo', $param);
        $this->db->from('business.articulo');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    /**
     * retorna el id del tipo de articulo dado su nombre
     *
     * @access public
     * @param String $name contiene el nombre del tipo de articulo
     * @return integer id identificador del tipo de articulo
     * @author Eliezer Pe#a
     * @version V 1.0 24/10/2013
     */
    function getTipoArticulo($name) {
        $this->db->select('id');
        $this->db->from('dynamic.category');
        $this->db->where('_label', $name);
        $this->db->where('_table', 'tipo_articulo');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row()->id : FALSE;
    }

    /**
     * unifica los procesos de actualizacion e insercion de articulos en una transaccion
     *
     * @access public
     * @param array $insert contiene los datos a insertar en bd
     * @param array $update contiene los datos a actualizar en bd
     * @author Eliezer Pe#a
     * @version V 1.0 24/10/2013
     */
    function transactArticulos($insert = NULL, $update = NULL) {
        $this->db->trans_start();
        $this->insertArticuloCsv($insert);
        $this->updateArticuloCsv($update);
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    /**
     * inserta los datos de articulos leidos del archivo csv
     *
     * @access public
     * @param array $param contiene los datos a insertar en bd
     * @author Eliezer Pe#a
     * @version V 1.0 24/10/2013
     */
    function insertArticuloCsv($param = array()) {
        if (!empty($param)) {
            foreach ($param as $data) {

                $get_data_articulo = $this->insertWithReturning($data['data'], 'business.articulo');
                $precios = $data['precios'];


                if ($precios) {
                    foreach ($precios as $mat => $material) {
                        $precios[$mat]['id_articulo'] = $get_data_articulo['id'];
                        $precios[$mat]['created_by'] = $this->session->userdata('user_id');
                    }
                    $this->db->insert_batch('business.articulo_precio', $precios);
                }
            }
        }
    }

    /**
     * actualiza los datos de articulos leidos del archivo csv
     *
     * @access public
     * @param array $data contiene los datos a actualizar en bd
     * @author Eliezer Pe#a
     * @version V 1.0 24/10/2013
     */
    function updateArticuloCsv($data) {
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $id = $this->getArticuloCsv($value['data']['modelo']);
                foreach ($id as $k => $val) {
                    $data[$key]['id_articulo'] = $val['id'];
                    $data[$key]['data']['modified_by'] = $this->session->userdata('user_id');
                    $data[$key]['data']['modified_at'] = 'now';
                    unset($data[$key]['data']['created_by']);
                }
            }

            foreach ($data as $key => $value) {
                $this->db->where('modelo', $value['data']['modelo']);
                $this->db->update('business.articulo', $value['data']);
                $precios = $value['precios'];
                if ($precios) {
                    foreach ($precios as $mat => $material) {
                        $precios[$mat]['id_articulo'] = $value['id_articulo'];
                        $precios[$mat]['modified_by'] = $this->session->userdata('user_id');
                        $precios[$mat]['modified_at'] = 'now()';
                        $this->db->update('business.articulo_precio', $precios[$mat], array('id_articulo' => $precios[$mat]['id_articulo'], 'id_precio' => $precios[$mat]['id_precio']));
                    }
                }
            }
        }
    }

    /**
     * obtiene el precio de un articulo
     *
     * @access public
     * @param integer $id Identificador del articulo
     * @author Eliezer Pe#a
     * @version V 1.0 18/09/2013
     */
    function getPriceArticulo($id = NULL) {
        if (!$id)
            return FALSE;

        $this->db->select('id_precio');
        $this->db->select('precio');
        $this->db->from('business.articulo_precio');
        $this->db->where('id_articulo', $id);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : FALSE;
    }

    public function masive_price_adjustment($data) {
        // $this->db->trans_start(TRUE);
        $this->db->trans_start();
        $this->db->update_batch('business.articulo_precio', $data, 'id');
        // echo $this->db->last_query();
        // die;
        return $this->db->trans_complete();
    }

    function getCatPrecios($param = NULL) {
        $this->db->select('id');
        $this->db->select('identificacion');
        $this->db->from('business.precios');
        $this->db->where("_delete", '0');
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : FALSE;
    }

    /**
     * Verifica si un articulo ya esta siendo utilizado en una cotizacion, backup o esta recomendado
     *
     * @access public 
     * @param
     * @author  Frederick D. Bustamante G.
     * @version 
     */
    function getUsedArticles($id) {

        if (!$id)
            return FALSE;
        $query = $this->db->query("
                SELECT 
                    distinct
                    id_cotizacion, id_articulo, 'normal' as tipo_cotizacion
                FROM 
                    business.cotizacion_articulo
                WHERE id_articulo in ($id)
                    
            UNION
                SELECT
                  distinct
                   id_cotizacion, id_articulo, 'libre' as tipo_cotizacion
                FROM 
                    business.libre_cotizacion_articulo
                    WHERE id_articulo in ($id)");

        return $query->num_rows() > 0 ? $query->result_array() : FALSE;
    }

    /**
     * delete de articulo
     *
     * @access public
     * @param integer $id contiene de la tabla articulo
     * @author Eliezer Pe#a
     * 
     */
    function deleteArticle($id) {
        $this->db->trans_start();
        // $this->db->where('id', $id);
        $this->db->delete('business.tramo_articulo', array('id_articulo' => $id));
        $this->db->delete('business.articulo_recomendacion', array('id_articulo' => $id));
        $this->db->delete('business.cotizacion_articulo', array('id_articulo' => $id));
        $this->db->delete('business.libre_cotizacion_articulo', array('id_articulo' => $id));
        $this->db->delete('business.tramo_articulo', array('id_articulo' => $id));
        $this->db->delete('business.articulo_precio', array('id_articulo' => $id));
        $this->db->delete('business.articulo', array('id' => $id));
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    /**
     * desactiva o activa el articulo seleccionado
     *
     * @access public
     * @param integer $id contiene de la tabla articulo
     * @author Eliezer Pe#a
     * 
     */
    function disableArticle($id, $_delete = '1') {
        $data = array('_delete' => $_delete);
        $this->db->trans_start();
        if (is_array($id)) {
            $this->db->where_in('id', $id);
        } else {
            $this->db->where('id', $id);
        }
        $this->db->update('business.articulo', $data);
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    /**
     * Obtiene los articulos desactivados
     *
     * @access  public
     * @param     array    $params    filtros para la consulta
     * @return  array    orden de devolucion

     */
    public function getDisabled($params) {
        extract($params);
        $this->setDisabled($id);
        $search = $this->filterSearchFields($search_fields);
        return parent::getAll($offset, $search, $sort);
    }

    /**
     * Cuenta el numero total de estantes para un almacen
     *
     * @access  public
     * @param   array    $params    filtros de la consulta
     * @return  integer    total de registros

     */
    public function countDisabled($params) {
        extract($params);
        $search = $this->filterSearchFields($search_fields);
        $this->setDisabled($id);
        return parent::getCount($search);
    }

    /**
     * obtiene los registros de estante para paginarlos
     *
     * @access  public
     * @param   integer    $id    filtros de la consulta
     *

     */
    private function setDisabled(&$id) {
        $this->db->select('art.id,');
        $this->db->select('art.modelo as modelo,');
        $this->db->select('art.codigo as codigo,');
        $this->db->select('art.descripcion as descripcion,');
        $this->db->select('cat._label AS tipo_articulo');
        // $this->db->from('business.articulo as art');
        // $this->db->join('dynamic.category cat', 'art.tipo_articulo=cat.id');
        $this->db->where('art._delete', '1');
    }

    /**
     * Filtra los datos para el listado de articulos
     *
     * @access  private
     * @param     array    $search    Search Fields
     * @return  array    Search Fields Filtrados

     */
    private function filterSearchFields($search) {
        $uri = NULL;
        $uri = $this->uri->uri_string();
    }

    function getDisabledById($id, $field = 'id') {

        $this->selects();
        $this->db->where($field, $id);
        $this->whereDelete('1');
        $rs = $this->db->get($this->concatSchemaTable());
        return ($rs->num_rows() > 0) ? $rs->row_array() : FALSE;
    }

    function getFabricante($params = NULL) {
        $this->db->select('id');
        $this->db->select('nombre');
        $this->db->from('business.fabricante');
        $this->db->where('_delete', '0');
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : FALSE;
    }

    function getArticulo($params = array()) {
        $this->db->distinct();
        $this->db->select('a.id');
        $this->db->from('business.articulo a');
        
        if (!empty($params['id_articulacion'])){
            $this->db->where('ar.id_cirugia', $params['id_articulacion']);
        $this->db->join('business.articulo_recomendacion ar', 'a.id=ar.id_articulo');
        }
        if (!empty($params['id_lesion']))
            $this->db->or_where('ar.id_cirugia', $params['id_lesion']);
        if (!empty($params['id_tecnica']))
            $this->db->or_where('ar.id_cirugia', $params['id_tecnica']);
        if (!empty($params['id_proveedor']))
            $this->db->where('a.id_proveedor', $params['id_proveedor']);
        if (!empty($params['id_tipo_articulo']))
            $this->db->where('a.tipo_articulo', $params['id_tipo_articulo']);
        $this->db->where('_delete','0');
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : FALSE;
    }

    /**
     * Dado un identificador obtiene los datos asociados al identifiador.
     *
     * @access  public
     * @param   Integer $id Identificador del registro a ser consultado.
     * @param   string $field campo por el cual se realizara la busqueda
     * @return  Array   Con los datos del registro consultado.
     * @author  Jose A. Rodriguez E.
     * @version V-1.0 20/11/2012 09:42
     */
    function getByIdBorrable($id = array(), $field = 'id') {
        $this->selects();
        $this->db->where_in($field, $id);
        $this->db->where('_delete','0');
        $this->whereDelete();
        $rs = $this->db->get($this->concatSchemaTable());
        return ($rs->num_rows() > 0) ? $rs->result_array() : FALSE;
    }
    /**
     * Dado un identificador obtiene los datos asociados al identifiador.
     *
     * @access  public
     * @param   Integer $id Identificador del registro a ser consultado.
     * @param   string $field campo por el cual se realizara la busqueda
     * @return  Array   Con los datos del registro consultado.
     * @author  Jose A. Rodriguez E.
     * @version V-1.0 20/11/2012 09:42
     */
    function getByIdNoBorrable($id = array(), $field = 'id') {
        $this->selects();
        $this->db->where_in($field, $id);
        $this->db->where('_delete','0');
        $this->whereDelete();
        $rs = $this->db->get($this->concatSchemaTable());
        return ($rs->num_rows() > 0) ? $rs->result_array() : FALSE;
    }

    /**
     * retorna el id del tipo de articulo dado su nombre
     *
     * @access public
     * @param String $name contiene el nombre del tipo de articulo
     * @return integer id identificador del tipo de articulo
     * @author Eliezer Pe#a
     * @version V 1.0 24/10/2013
     */
    function getSelectTipo() {
        $this->db->select('id');
        $this->db->select('_label');
        $this->db->from('dynamic.category');
//        $this->db->where('_label', $name);
        $this->db->where('_table', 'tipo_articulo');
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : FALSE;
    }

    /**
     * Retorna los articulos recomendados segun el identificador de cirugia
     *
     * @access public
     * @param String $id_cirugia contiene el nombre del tipo de articulo
     * @return integer id identificador del tipo de articulo
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 16/02/2014
     */
    function getArticulosRecomendados($id_cirugia = NULL) {
        if (!$id_cirugia)
            return FALSE;
        $this->db->select('business.articulo_recomendacion.id_articulo');
        $this->db->from('business.cirugia');
        $this->db->join('business.articulo_recomendacion', 'business.cirugia.id=business.articulo_recomendacion.id_cirugia');
        $this->db->where('business.cirugia.id', $id_cirugia);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result_array() : FALSE;
    }

    /**
     * Obtiene todos los identificadores de una tabla
     *
     * @access public
     * @param string $id Campo del select
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 16/02/2014
     */
    function getAllId($id = 'id') {
        $this->db->select($this->concatSchemaTable() . ".$id",FALSE);
        $this->db->from($this->concatSchemaTable());
        $this->whereDelete();
        $rs = $this->db->get();
        return ($rs->num_rows() > 0) ? $rs->result_array() : FALSE;
    }

}
