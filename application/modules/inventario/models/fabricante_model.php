<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Fabricante_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad fabricante
 * y sus entidades dependientes o extendidas
 *
 * @package Modules
 * @category Model
 * @version V-1.0 13/02/14 03:23 PM
 */
class Fabricante_model extends AST_Model {

    public function __construct() {

        parent::__construct();
    }

}