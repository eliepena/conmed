<div style="margin-top: 10px; margin-left: 20px">
    <table id="instrumentoTabledev" class=" table table-striped table-bordered">
        <thead>
            <tr>
                <th width="10px"></th>

                <th>Modelo</th>
                <th>Descripción</th>
                <th>Lote</th>
                <th width="10px">Cantidad</th>
<!--                <th>Disponible?</th>-->
            </tr>
        </thead>
        <tbody>
            <?php if (isset($instrumentos) and !empty($instrumentos)): ?>
                <?php foreach ($instrumentos as $key => $instrumento): ?>
                    <tr id="tr-instru-<?php echo $instrumento['id']; ?>dev" >
                        <td>
                        </td>
                        <td id="model-instru-<?= $instrumento['id']; ?>dev" ><?php echo $instrumento['modelo']; ?></td>
                        <td id="name-instru-<?= $instrumento['id']; ?>dev" ><?php echo $instrumento['descripcion']; ?></td>
                        <?php
                        if ($operation != 'detail'):
                            $value = "value='{$instrumento['cantidad']}'";
                            $readonly = ' ';
                        else:
                            $readonly = ' readonly="readonly" ';
                            $value = "value='{$instrumento['cantidad']}'";
                        endif;
                        ?>
                        <td><label><?php echo $instrumento['lote'] ?></label></td> 
                <input type="hidden" name="ins_lote[<?= $instrumento['id'] ?>][<?php echo $instrumento['id_lote'] ?>]" value="<?php echo $instrumento['id_lote'] ?>" />

                <td><input min="0" max="<?= $instrumento['cantidad'] ?>" id="cant-instru-<?php echo $instrumento['id']; ?>dev" type="number" class="input-mini" name="cant_instru[<?php echo $instrumento['id']; ?>][<?php echo $instrumento['id_lote'] ?>]" placeholder="0" key="<?php echo $instrumento['id']; ?>" <?= $readonly ?> value="<?php echo ($operation != 'create' ?  $instrumento['cantidad'] : '0'); ?>" <?php echo ($operation != 'create' ? 'readonly="readonly"': ''); ?>/></td>

                </tr>
                <tr>
                    <td style="background-color:#C2DDB6"   colspan="6" class=" ins-<?= $instrumento['id']; ?>-dev hide sustituto">
                        <table id="ins-<?php echo $instrumento['id']; ?>" class=" table table-striped table-bordered">

                            <thead>
                                <tr>
                                    <th width="10px"></th>
                                    <th width="10px"></th>
                                    <th>Modelo</th>
                                    <th>Descripción</th>
        <!--                                    <th width="10px">Cantidad</th>-->
                                </tr>
                            </thead>
                        </table>

                    </td></tr>
            <?php endforeach; ?>
        <?php endif; ?>
        </tbody>
    </table>
    <?php if ($operation == 'create'): ?>
        <div class="form-actions">
            <input type="button" onclick="navigate_tab('prev',12);" value="Anterior" class="btn btn-primary">
            <input type="button" onclick="navigate_tab('next',12);" value="Siguiente" class="btn btn-primary">
        </div>
    <?php endif ?>
</div>
<script type="text/javascript">

    $('input[type="checkbox"].style3').checkbox({
        buttonStyle: 'btn-danger',
        buttonStyleChecked: 'btn-success',
        checkedClass: 'icon-check',
        uncheckedClass: 'icon-check-empty'
    });

</script>