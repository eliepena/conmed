<html>
    <head>
        <title></title>
        <style>
            body {
                font-size:9.5pt;
            }
            .head{
                background-color: #A5A5A5;
                font-weight:bold;
            }
            .right{
                text-align:right;
            }
            .left{
                text-align:left;
            }
            .center{
                text-align: center;
            }
            .borde_ext{

                border: 1px black solid;
            }
            .borde{

                border-top: 1px black solid;
                border-right: 1px black solid;
            }
            .borde_bottom{
                border-bottom:  1px black solid;
            }
            .borde_right{
                border-right:   1px black solid;
            }
            .borde_top{
                border-top:   1px black solid;
            }
            .borde_left{
                border-left: 1px black solid;
            }
        </style>
    </head>
    <body>
        <table border="0" >
            <thead>
                <tr>
                    <th colspan ="7" style="font-size:14pt;font-weight: bold;width:104%;text-align: right;height: 20px;padding-left:  30%;" >
                        <?php echo $this->lang->line('PdfDev_Title_Mat'); ?>
                        &nbsp;&nbsp;&nbsp;
                    </th>
                </tr>
                <tr>
                    <th style="width:30%" rowspan="4" colspan ="1" class="borde_top borde_bottom borde_left left ">
                        <br/>
                        <b><?php echo $this->lang->line('PdfOrd_Client'); ?>:</b>
                        <br>
                        <font size="10"><?php echo $cliente; ?></font>
                    </th>
                    <th style="width:15%" rowspan="4" colspan ="1" class="borde_top borde_bottom borde_left left ">
                        <br/>
                        <b><?php echo $this->lang->line('PdfOrd_Rif'); ?>:</b>
                        <font size="10"><?php echo $rif; ?></font>
                    </th>
                    <th style="width:14%" rowspan="4" colspan ="1" class="borde_top borde_bottom borde_left borde_right left">
                        <br/>
                        <b><?php echo $this->lang->line('PdfOrd_Nit'); ?>:</b>
                        <br/>
                        <font size="10"><?php echo $nit; ?></font>
                    </th>
                    <th style="width:22%" colspan ="2" class="  right">
                        <?php echo $this->lang->line('PdfOrd_NroOrd'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php echo $codigo_orden; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:22%" colspan ="2" class="  right">
                        <?php echo $this->lang->line('PdfOrd_FecEmi'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php echo $fecha_emision; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:22%" colspan ="2" class="  right">
                        <?php //echo $this->lang->line('PdfOrd_ForPag'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php //echo $forma_pago; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:22%" colspan ="2" class="  right">
                        <?php //echo $this->lang->line('PdfOrd_OrdenCompra'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php //echo $tiempo_entrega; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:59%"  colspan ="3" class="borde_top borde_bottom borde_left borde_right left ">
                        <br/>
                        <b><?php echo $this->lang->line('PdfOrd_Seg'); ?>:</b>
                        <font size="10"><?php echo $seguro; ?></font>
                    </th>
                    <th style="width:22%" colspan ="2" class="  right">
                        <?php //echo $this->lang->line('PdfOrd_Vend'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php //echo $vendedor; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:59%"  colspan ="3" class="borde_top borde_bottom borde_left borde_right left ">
                        <br/>
                        <b><?php echo $this->lang->line('PdfOrd_Tlf'); ?>:</b>
                        <font size="10"><?php echo $tlf; ?></font>
                    </th>
                    <th style="width:22%" colspan ="2" class="  right">
                        <?php //echo $this->lang->line('PdfOrd_MedTratant'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php //echo $dr_tratante; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:59%" colspan ="3" class="borde_top ">
                        <b><?php echo $this->lang->line('PdfOrd_MsjTabla'); ?>:</b>
                    </th>
                    <th style="width:22%" colspan ="2" class="  right">
                        <?php //echo $this->lang->line('PdfOrd_NroImport'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class="right" ><?php //echo $nro_cotizacion;  ?></th>
                </tr>
                <tr class="center">
                    <th style="width:6%" class="borde_top borde_bottom borde_left head">
                        <?php echo $this->lang->line('PdfOrd_Cod'); ?>
                    </th>
                    <th style="width:15%" class="borde_top borde_bottom borde_left head">
                        <?php echo $this->lang->line('PdfOrd_Mod'); ?>
                    </th>
                    <th style="width:64%" class="borde_top borde_bottom borde_left head">
                        <?php echo $this->lang->line('PdfOrd_Descrip'); ?>
                    </th>
                    <th style="width:7%" class="borde_top borde_bottom borde_left head">
                        <?php echo $this->lang->line('PdfOrd_Cant'); ?>
                    </th>
                    <th style="width:7%" class="borde_top borde_bottom borde_left borde_right head">
                        <?php echo $this->lang->line('PdfOrd_Lote'); ?>
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                $cant_materiales = sizeof($materiales);
                $count = 1;
                foreach ($materiales AS $clave => $valor):
                    $style_row = ($count % 2 == 0 ? '' : '');
                    echo '<tr >
                                <td style="width:6%;' . $style_row . '" class="borde_left border_right">' . $valor['codigo_m'] . '</td>
                                <td style="width:15%;' . $style_row . '" class="borde_left border_right">' . $valor['modelo_m'] . '</td>
                                <td style="width:64%;' . $style_row . '" class="borde_left">' . $valor['descripcion_m'] . '</td>
                                <td style="width:7%;' . $style_row . '"  class="borde_left right">' . $valor['lote_m'] . '</td>
                                <td style="width:7%;' . $style_row . '" class="borde_left borde_right right">' . $valor['cant_m'] . '</td>
                               
                              </tr>';
                    $count = $count + 1;
                endforeach;
                $espacio_fijo = 26 - $cant_materiales;
                $count_space = 1;
                if ($espacio_fijo > 0):
                    for ($j = 1; $j <= $espacio_fijo; $j++) :
                        echo '<tr>
                                    <td class="borde_left border_right">&nbsp;</td>
                                    <td class="borde_left">&nbsp;</td>
                                    <td class="borde_left">&nbsp;</td>
                                    <td class="borde_left">&nbsp;</td>
                                    <td class="borde_left borde_right">&nbsp;</td>
                                   
                                  </tr>';
                        $count_space = $count_space + 1;
                    endfor;
                endif;
                ?>

            </tbody>
            <tfoot>
                <tr>
                    <td style="width:99%;font-weight: bold;text-decoration: underline;font-size:7pt;"   colspan = "8" class="borde_top"></td>
                </tr>
            </tfoot>
        </table>
    </body>
</html>