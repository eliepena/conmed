<?php
if ($operation == 'detail') {
    ?>
    <script type="text/javascript">
        $(function(){
            //        var form = 'form_15200';
            //        var url = 'http://conmed/delivery/delivery/create/process';
            //        var modal = '#modal_';
            //        var reload_content = false;
            //        var _alert = '#alert_15200';
            //        formWindow(form, url, modal, _alert, reload_content);
            //Ejecuta las Validaciones del Cliente
            new FormValidator('form_15200', buildRules('form_15200'),
            function(errors, event){
                var flag = validateForm(errors, event);

                if (event && event.preventDefault) {
                    event.preventDefault();
                } else if (event) {
                    event.returnValue = false;
                }

                //validacion submit
                if(flag){
                    //este archivo es particular,
                    //Valida TAB DATOS luego de ser validada en el cliente.
                    validate_orden_tab();
                }
            });
            customFields();
            build_table_articles();
        });
    </script>
    <?php
}
?>



<div class="modal-body">
    <ul>
        <?php
        if ($operation == 'detail') {
            ?>
            <li id="li-label_datos_cotizacion-15200">
                <br>
                <label for="label_datos_cotizacion-15200">
                    <strong>DATOS DE COTIZACION</strong></label>
                <div id="label-datos_cotizacion-15200"></div>
            </li>
            <li id="li-paciente-15102">
                <div class="control-group">
                    <label class="control-label" for="paciente-15200">Paciente/Cliente</label>
                    <div class="controls">
                        <select name="paciente"
                                id="paciente-15200"
                                data-placeholder="Seleccione un Paciente"
                                class="chzn-select"
                                data-rules=""
                                data-display="Paciente"
                                data-content="Indique el Paciente"
                                title="Nombre de Paciente"
                                >
                            <option value="0">Todos</option>
                            <?php
                            if (isset($data_paciente) && $data_paciente):
                                $option_paciente = '';
                                foreach ($data_paciente as $key => $value):
                                    $option_paciente = $value['text'];
                                    ?>
                                    <option value="<?php echo $value['id']; ?>" ><?php echo $option_paciente; ?></option>
                                    <?php
                                endforeach;
                                foreach ($data_paciente as $key => $value):
                                    ?>
                                    <input type="hidden" id="p-<?php echo $value['id']; ?>" value="<?php echo $value['tipo']; ?>"/>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </select>
                        <span id="button-paciente"></span>
                    </div></div>
            </li>
            <li id="li-codigo-15200">




                <div class="control-group">
                    <label class="control-label" for="297">Nro Orden de Despacho<span style='color:red;font-size: 16px'> *</span></label>
                    <div class="controls">
                        <select name="codigo"
                                id="297"
                                data-placeholder="Seleccione orden de despacho"
                                class="chzn-select"
                                data-rules="required"
                                data-display="Nro orden de despacho"
                                data-content="Indique la orden de despacho"
                                title="Nro despacho"
                                >

                            <option value=""></option>
                            <?php
                            if (isset($data_cotizacion) && $data_cotizacion):
                                $option_cotizacion = '';
                                foreach ($data_cotizacion as $key => $value):
                                    $option_cotizacion = $value['text'];
                                    ?>
                                    <option value="<?php echo $value['id']; ?>"  ><?php echo $option_cotizacion; ?></option>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </select>

                        <span id="button-codigo"></span>
                    </div></div>


            </li>

            <li id="li-cliente-15200">




                <div class="control-group">
                    <label class="control-label"  for="cliente-15200">Cliente</label>
                    <div class="controls">
                        <input type="text"
                               class="input-xlarge"
                               name="cliente-15200"
                               id="cliente-15200"
                               placeholder=""
                               data-rules=""
                               data-display="Cliente"
                               value=""
                               data-content=""
                               title="Cliente"
                               readonly="readonly"
                               />



                    </div>
                </div>


            </li>
            <li id="li-cedula-15200">



                <div class="control-group">
                    <label class="control-label"  for="cedula-15200">Cedula/RIF</label>
                    <div class="controls">
                        <input type="text"
                               class="input-xlarge"
                               name="cedula-15200"
                               id="cedula-15200"
                               placeholder=""
                               data-rules=""
                               data-display="Cedula-RIF"
                               data-content=""
                               title="Cedula-RIF"
                               readonly="readonly"
                               />



                    </div>
                </div>


            </li>  <li id="li-email-15200">



                <div class="control-group">
                    <label class="control-label"  for="email-15200">Correo Electronico</label>
                    <div class="controls">
                        <input type="text"
                               class="input-xlarge"
                               name="email-15200"
                               id="email-15200"
                               placeholder=""
                               data-rules=""
                               data-display="Correo Electronico"
                               data-content=""
                               title="Correo Electronico"
                               readonly="readonly"
                               />



                    </div>
                </div>


            </li>



            <li id="li-medico-15200">



                <div class="control-group">
                    <label class="control-label"  for="medico-15200">Medico Tratante</label>
                    <div class="controls">
                        <input type="text"
                               class="input-xlarge"
                               name="medico-15200"
                               id="medico-15200"
                               placeholder=""
                               data-rules=""
                               data-display="Medico Tratante"
                               data-content=""
                               title="Medico Tratante"
                               readonly="readonly"
                               />



                    </div>
                </div>


            </li>

            <li id="li-vendedor-15200">


                <div class="control-group">
                    <label class="control-label"  for="vendedor-15200">Vendedor</label>
                    <div class="controls">
                        <input type="text"
                               class="input-xlarge"
                               name="vendedor-15200"
                               id="vendedor-15200"
                               placeholder=""
                               data-rules=""
                               data-display="Vendedor"
                               data-content=""
                               title="Vendedor"
                               readonly="readonly"
                               />



                    </div>
                </div>


            </li>
            <li id="li-tipo_pago-15200">




                <div class="control-group">
                    <label class="control-label"  for="tipo_pago-15200">Tipo de Pago</label>
                    <div class="controls">
                        <input type="text"
                               class="input-xlarge"
                               name="tipo_pago-15200"
                               id="tipo_pago-15200"
                               placeholder=""
                               data-rules=""
                               data-display="Tipo de Pago"
                               data-content=""
                               title="Tipo de pago"
                               readonly="readonly"
                               />



                    </div>
                </div>


            </li>
            <!--            <li id="li-direccion-15200">
                            <div class="control-group">
                                <label class="control-label" for="direccion-15200">Dirección de Envio</label>
                                <div class="controls">
                                    <textarea class="span3" name="direccion-15200" id="direccion-15200"  data-rules="" data-display="Dirección de Envio" data-content="Indique la dirección de envio" title="Dirección de Envio" rows="5" readonly="readonly"></textarea>
                                </div>
                            </div>
                        </li>-->


            <?php
        }
        ?>

        <?php
        if ($operation != 'detail') {
            ?>


            <li>
                <div class="control-group">
                    <label class="control-label" >Nro Orden</label>
                    <div class="controls">

                        <input
                            class="input-xlarge"
                            type="text"
                            data-display="Orden de despacho"
                            data-content="Numero de Orden de Despacho"
                            title="Orden de despacho"
                            value="<?php echo (isset($data[0]['codigo']) ? $data[0]['codigo'] : ''); ?>"
                            readonly="readonly" />



                    </div>
                </div>


            </li>

            <li>
                <div class="control-group">
                    <label class="control-label" >Nro Cotizacion</label>
                    <div class="controls">

                        <input
                            class="input-xlarge"
                            type="text"
                            data-display="Nro Cotizacion"
                            data-content="Nro de Cotizacion de la Orden de Despacho"
                            title="Nro Cotizacion"
                            value="<?php echo (isset($data[0]['id_cotizacion']) ? $data[0]['id_cotizacion'] : ''); ?>"
                            readonly="readonly" />



                    </div>
                </div>


            </li>
            <li>
                <div class="control-group">
                    <label class="control-label" >Fecha de solicitud</label>
                    <div class="controls">

                        <input
                            class="input-xlarge"
                            type="text"
                            data-display="Fecha de solicitud"
                            data-content="Fecha de solicitud de la Orden de Despacho"
                            title="Fecha de solicitud"
                            value="<?php echo (isset($data[0]['fecha_solicitud']) ? $data[0]['fecha_solicitud'] : ''); ?>"
                            readonly="readonly"  />



                    </div>
                </div>


            </li>

            <li>
                <div class="control-group">
                    <label class="control-label" >Fecha de Entrega</label>
                    <div class="controls">

                        <input
                            class="input-xlarge"
                            type="text"
                            data-display="Fecha de Entrega"
                            data-content="Fecha de Entrega de la Orden de Despacho"
                            title="Fecha de Entrega"
                            value="<?php echo (isset($data[0]['fecha_despacho']) ? $data[0]['fecha_despacho'] : ''); ?>"
                            readonly="readonly" />



                    </div>
                </div>


            </li>
            <li>
                <div class="control-group">
                    <label class="control-label" >Estatus de la Orden</label>
                    <div class="controls">

                        <input
                            class="input-xlarge"
                            type="text"
                            data-display="Estatus de la Orden"
                            data-content="Estatus de la Orden de Despacho"
                            title="Estatus de la Orden"
                            value="<?php echo (isset($data[0]['cat_estatus_orden_despacho']) ? $data[0]['cat_estatus_orden_despacho'] : ''); ?>"
                            readonly="readonly" />



                    </div>
                </div>


            </li>

            <li>
                <div class="control-group">
                    <label class="control-label" >Despachada Por</label>
                    <div class="controls">

                        <input
                            class="input-xlarge"
                            type="text"
                            data-display="Despachada Por"
                            data-content="Empleado que despacho la Orden de Entrega"
                            title="Despachada Por"
                            value="<?php echo (isset($data[0]['id_despachador']) ? $data[0]['id_despachador'] : ''); ?>"
                            readonly="readonly"  />



                    </div>
                </div>


            </li>
            <?php
        }
        ?>


        <li id="li-id_cirugia-15200">
            <input type="hidden"
                   class="input-xlarge"
                   name="id_cirugia"
                   id="id_cirugia-15200"
                   placeholder=""
                   data-rules=""
                   data-display="Identificador de la Cirugia"
                   value=""
                   data-content=""
                   title="Identificador de la Cirugia"
                   />
        </li>







        <li id="li-id_cotizacion-15200">
            <input type="hidden"
                   class="input-xlarge"
                   name="id_cotizacion"
                   id="id_cotizacion-15200"
                   placeholder=""
                   data-rules=""
                   data-display="Nro Cotizacion"
                   value=""
                   data-content=""
                   title="Nro Cotizacion"
                   />

        </li>
    </ul>
</div>

<?php
if ($operation == 'detail') {
    ?>
    <div class="form-actions">
        <!--<button class="btn" data-dismiss="modal">Cancelar</button>-->
        <input type="button" onclick="navigate_tab('next',10);" value="Siguiente" class="btn btn-primary">
    </div>

    <?php
}
?>

