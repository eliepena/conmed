<div style="margin-top: 10px; margin-left: 20px">
    <table id="materialBackupTableOrden" class="table table-striped table-bordered">
        <thead>
            <tr>

                <th width="10px"></th>
                <th>Modelo</th>
                <th>Descripción</th>
                <th width="4px">Lote</th>
                <th width="4px">Cant</th>
                <!--<th width="16px">Disponible?</th>-->
            </tr>
        </thead>
        <tbody>
            <?php
            $monto = 0;

            if (isset($backup) and !empty($backup)):
                ?>
                <?php foreach ($backup as $key => $material): ?>
                    <tr>
                        <td>
                        </td>
                        <td><?php echo $material['modelo']; ?></td>
                        <td><?php echo $material['descripcion']; ?></td>
                        <td><label><?php echo $material['lote'] ?></label></td>
                <input type="hidden" name="back_lote[<?= $material['id'] ?>][<?php echo $material['id_lote'] ?>]" value="<?php echo $material['id_lote'] ?>" />
                <td><input name="cantidad-backup[<?php echo $material['id']; ?>][<?php echo $material['id_lote'] ?>]" min="0" type="number" class="input-mini" value="<?php echo $material['cantidad']; ?>" <?php echo ($operation != 'create' ? 'readonly="readonly"': ''); ?>/></td>

                </tr>
                <tr>
                    <td style="background-color:#C2DDB6"   colspan="6" class="backup-<?= $material['id']; ?>-Orden hide sustituto">
                        <table id="backup-<?php echo $material['id']; ?>" class=" table table-striped table-bordered">

                            <thead>
                                <tr>
                                    <th width="10px"></th>
                                    <th width="10px"></th>
                                    <th>Modelo</th>
                                    <th>Descripción</th>
                                    <th width="10px">Cantidad</th>
                                </tr>
                            </thead>
                        </table>

                    </td></tr>
                <?php
            endforeach;

        endif;
        ?>
        </tbody>
        <tfoot>

        </tfoot>
    </table>
    <?php if ($operation == 'create'): ?>
        <div class="form-actions">
            <input type="button" onclick="navigate_tab('prev',13);" value="Anterior" class="btn btn-primary">
            <input type="button" onclick="navigate_tab('next',13);" value="Siguiente" class="btn btn-primary">
        </div>
    <?php endif ?>
</div>

<script type="text/javascript">

    $('input[type="checkbox"].style3').checkbox({
        buttonStyle: 'btn-danger',
        buttonStyleChecked: 'btn-success',
        checkedClass: 'icon-check',
        uncheckedClass: 'icon-check-empty'
    });


    function show_add(id){

        if ($("."+id).is (':visible')){

            $("."+id).hide();
        }else{
            $(".sustituto").hide();
            $("."+id).show();
        }


    }
</script>