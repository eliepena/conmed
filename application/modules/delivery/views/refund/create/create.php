<div id="alert_content"></div>
<div id="link"></div>
<form id="form_15200" name="form_15200" action="#" method="POST" class="form-horizontal">
    <?php echo $view ?>
</form>

<script>
    $(function(){
//        $("li:has(a[href=#11]),#11").addClass('hide_element');
//        $("li:has(a[href=#12]),#12").addClass('hide_element');
//        $("li:has(a[href=#13]),#13").addClass('hide_element');
//        $("li:has(a[href=#14]),#14").addClass('hide_element');


    });
    //Busqueda de los registros de cotizacion
//    $("select[name='codigo']").ajaxChosen({
//        dataType: 'json',
//        type: 'GET',
//        url:"<?= base_url() ?>delivery/delivery/EXC_searchSelect"
//    },{
//        generateUrl: function(q){ return "<?= base_url() ?>delivery/delivery/EXC_searchSelect" },
//        loadingImg: '<?= base_url() ?>assets/img/loading.gif'
//    });
//
//    //Cambio de Tecnica
//    $("select[name='codigo']").change(function(e){
//        id_cot = $(this).val();
//        tipo_precio = $("select[name='id_precios']").val();
//        if (id_cot != "" ){
//            load_cot({
//                container:{
//                    medico:'#medico-15200',
//                    vendedor:'#vendedor-15200',
//                    cliente:'#cliente-15200',
//                    tipo_pago:'#tipo_pago-15200',
//                    id_cotizacion:'#id_cotizacion-15200',
//                    id_articulacion:'#id_cirugia-15200',
//                    mat:'#2-container',
//                    backup:'#4-container',
//                    instru:'#3-container',
//                    resumen:'#5-container'
//                },
//                url:"<?= base_url() ?>delivery/EXC_getDataCotizacion/",
//                data:{
//                    id:id_cot
//                }})
//
//
//        }
//    });
    
  var selectTodos =$("select[name='codigo']  option") ;
    $("select[name='paciente']").change(function(e){
        id_paciente = $(this).val();
        tipo=$('#p-'+id_paciente).val();
        if(id_paciente==0){
            $("select[name='codigo']  option").remove();
            $("select[name='codigo'] ").append(selectTodos)
            $("select[name='codigo']").val('').trigger('liszt:updated');
            $("input[name='cliente-15200']").val('');
            $("input[name='medico-15200']").val('');
            $("input[name='vendedor-15200']").val('');
            $("input[name='tipo_pago-15200']").val('');
        }else{
        $.ajax({
            type:"post",
            url:"<?= base_url() ?>delivery/refund/EXC_selectCotizacion",
            data:{id_paciente:id_paciente, tipo:tipo},
            success:function(resp) {
                $("select[name='codigo']  option").remove();
                $("select[name='codigo'] ").append(resp.data)
                $("select[name='codigo']").val('').trigger('liszt:updated');
            }
        });
        }
        
        
      
        //$("select[name='codigo'] ").append('<option id=""></option>');
        $("select[name='codigo']").val('').trigger('liszt:updated');
       
    });
    $("select[name='codigo']").change(function(e){
        id_cot = $(this).val();
        id_paciente = $("select[name='paciente']").val();
        tipo_cliente=$('#p-'+id_paciente).val();
        if(tipo_cliente=='per' || tipo_cliente=='emp'){
             $("#li-medico-15200").hide();
             $("#li-cedula-15200").show();
             $("#li-email-15200").show();
             $("#li-tipo_pago-15200").hide();
             $("#li-vendedor-15200").hide();
        }else{
            $("#li-medico-15200").show();
            $("#li-vendedor-15200").show();
             $("#li-cedula-15200").hide();
             $("#li-email-15200").hide();
             $("#li-tipo_pago-15200").hide();
        }
        
        
        
        
        
        if (id_cot != "" ){
            load_cot({
                container:{
                    medico:'#medico-15200',
                    vendedor:'#vendedor-15200',
                    cliente:'#cliente-15200',
                    cedula:'#cedula-15200',
                    email:'#email-15200',
                    tipo_pago:'#tipo_pago-15200',
                    id_cotizacion:'#id_cotizacion-15200',
                    id_articulacion:'#id_cirugia-15200',
                    mat:'#11-container',
                    backup:'#13-container',
                    instru:'#12-container',
                    resumen:'#14-container'
                },
                url:"<?= base_url() ?>delivery/refund/EXC_getDataCotizacion/",
                data:{
                    id:id_cot,
                    tipo:tipo_cliente
                }})


        }
    });
    function load_cot(params){
        $('body').modalmanager('loading');
        //Dispara el evento

        $.ajax({
            type : "POST",
            url: params.url,
            data: params.data,
            dataType:'json',
            success: function(response){
                $('body').modalmanager('loading');
                if(response.success){
                    $(params.container.medico).val(response.data.id_medico);
                    $(params.container.vendedor).val(response.data.id_vendedor);
                    $(params.container.cliente).val(response.data.id_paciente);
                    $(params.container.cedula).val(response.data.cedula);
                    $(params.container.email).val(response.data.correo_electronico);
                    $(params.container.id_cotizacion).val(response.data.id);
                    $(params.container.id_articulacion).val(response.data.cod_articulacion);
                    $(params.container.mat).html(response.materiales);
                    $(params.container.backup).html(response.backup);
                    $(params.container.instru).html(response.instrumentos);
                    $(params.container.resumen).html(response.resumen);
                    
                }
            }
        });
    }

    /**
     * Valida el tab de DATOS de la cotizacion para poder continuar con la carga de informacion
     *
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V- 1.0  26/08/13 03:55 PM
     */
    function validate_orden_tab(){
        $('body').modalmanager('loading');
        //Dispara el evento
        $.ajax({
            url:BASE_URL + 'delivery/delivery/create/process/validate_data',
            data: $('#form_15200').serialize(),

            success: function(response){
                $('body').modalmanager('loading');

                //Verifica si el TAB DATOS se valida exitosamente y muestra los demas TABS
                if(response.validate == 1){
                    $("li:has(a[href=#1-dev]),#1-dev").removeClass("active");
                    $("li:has(a[href=#2-dev]),#2-dev").removeClass('hide_element');
                    $("li:has(a[href=#3-dev]),#3-dev").removeClass('hide_element');
                    $("li:has(a[href=#4-dev]),#4-dev").removeClass('hide_element');
                    $("li:has(a[href=#5-dev]),#5-dev").removeClass('hide_element');
                    $("li:has(a[href=#2-dev]),#2-dev").addClass("active");

                    //Quita el mensaje de Alerta Error
                    $("#alert_content").empty();
                }

                //Se procesa cuando no se pueden validar los datos
                else{
                    //Se mantiene o muestra nuevamente el TAB inicial y oculta los demas TABS
                    $("li:has(a[href=#2-dev]),#2-dev").removeClass("active");
                    $("li:has(a[href=#1-dev]),#1-dev").addClass("active");
                    $("li:has(a[href=#2-dev]),#2-dev").addClass('hide_element');
                    $("li:has(a[href=#3-dev]),#3-dev").addClass('hide_element');
                    $("li:has(a[href=#4-dev]),#4-dev").addClass('hide_element');
                    $("li:has(a[href=#5-dev]),#5-dev").addClass('hide_element');

                    var textAlert = null;

                    //Ocurre cuando hay un error en Base de Datos
                    if(response.view == false)
                        textAlert = 'No se pudo validar los datos, si el problema persiste comuniquese con sistemas';

                    //Ocurre cuando hay errores de validacion en el Servidor
                    else
                        textAlert = response.view;

                    //Mensaje de Alerta Error
                    var alertHtml = [
                        '<div class="alert alert-error fade in">',
                        '<button type="button" class="close" data-dismiss="alert">×</button>',
                        textAlert,
                        '</div>'
                    ].join('');
                    //Elemento que muestra el Alert
                    $("#alert_content").html(alertHtml);
                }
            }
        });
    }
</script>