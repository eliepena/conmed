<script>
  
</script>
<div style="margin-top: 10px; margin-left: 20px">
    <table id="materialTabledev" class=" <?php echo ($operation == 'detail' ? '' : ' js-table '); ?>  table table-striped table-bordered">
        <thead>
            <tr>
                <th width="10px"></th>
<!--                <th width="10px"></th>-->
                <th>Modelo</th>
                <th>Descripción</th>
                <th width="10px">Lote</th>
                <th width="10px">Cantidad</th>
<!--                <th>Disponible?</th>-->
            </tr>
        </thead>
        <tbody>
            <?php
            if (isset($materiales) and !empty($materiales)):
                //Posicion en array que indica cual es tipo de precio seleccionado.
//              echo '<pre>',  print_r($materiales),'</pre>';
                foreach ($materiales as $key => $material):
                    ?>
                    <tr id="tr-mat-<?php echo $material['id']; ?>-Orden">

                        <td>
                            <!--<input class="indicador-mat" id="mat-<?= $material['id'] ?>Orden" type="hidden" value="<?php echo $material['id'] ?>" key="<?= $material['id'] ?>" name="mat[<?= $material['id'] ?>]" />-->
                            <!--<i class="icon-plus" onclick="show_add('mat-<?= $material['id'] ?>-Orden')" />-->
                        </td>



                        <td id="model-mat-<?= $material['id'] ?>Orden"><?php echo $material['modelo'] ?></td>
                        <td id="name-mat-<?= $material['id'] ?>Orden"><?php echo $material['descripcion'] ?></td>
                        <td><label><?php echo $material['lote'] ?></label></td>
                <input type="hidden" name="id_lote[<?= $material['id'] ?>][<?php echo $material['id_lote'] ?>]" value="<?php echo $material['id_lote'] ?>"/>
                <td>
                    <input min="0" max="<?= $material['cantidad'] ?>"  type="number" id="cant-mat-<?= $material['id'] ?>dev" class="input-mini" name="cant_mat[<?= $material['id'] ?>][<?= $material['id_lote'] ?>]" placeholder="0" key="<?= $material['id'] ?>" value="<?php echo ($operation != 'create' ?  $material['cantidad'] : '0'); ?>" <?php echo ($operation != 'create' ? 'readonly="readonly"': ''); ?> />
                </td>
                </tr>
                <tr>
                    <td style="background-color:#C2DDB6"   colspan="6" class="mat-<?= $material['id'] ?>-Orden hide sustituto">
                        <table id="mat-<?php echo $material['id'] ?>" class=" table table-striped table-bordered">

                            <thead>
                                <tr>
                                    <th width="10px"></th>
                                    <th width="10px"></th>
                                    <th>Modelo</th>
                                    <th>Descripción</th>
                                    <th width="10px">Cantidad</th>
                                </tr>
                            </thead>
                        </table>
                    </td>
                </tr>

                
            <?php endforeach; ?>
        <?php endif; ?>
        </tbody>
        <tfoot>

        </tfoot>
    </table>
    <?php
    if ($operation == 'create'):
        ?>
        <div class="form-actions">
            <input type="button" onclick="navigate_tab('prev',11);" value="Anterior" class="btn btn-primary">
            <input type="button" onclick="navigate_tab('next',11);" value="Siguiente" class="btn btn-primary">
        </div>
    <?php endif ?>

</div>

