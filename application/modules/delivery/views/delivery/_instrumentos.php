<div style="margin-top: 10px; margin-left: 20px">
    <table id="instrumentoTableOrden" class=" table table-striped table-bordered">
        <thead>
            <tr>

                <th width="10px"></th>
                <th>Modelo</th>
                <th>Descripción</th>
                <th>Lote</th>
                <th width="10px">Cantidad</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (isset($instrumentos) and !empty($instrumentos)):
                foreach ($instrumentos as $clave=>$valor):
                    if ($valor):
                        foreach ($valor as $key => $instrumento):
                            ?>
                            <tr id="tr-instru-<?php echo $instrumento['id']; ?>Orden" >


                                <td><a data-toggle="modal" href="#" onclick="show_img_article('<?php echo $instrumento['modelo'] ?>',  '<?php echo $instrumento['descripcion'] ?>', '<?php echo $instrumento['imagen'] ?>');"><i class="icon-picture"></i></a></td>
                                <td id="model-instru-<?= $instrumento['id']; ?>Orden" ><?php echo $instrumento['modelo']; ?></td>
                                <td id="name-instru-<?= $instrumento['id']; ?>Orden" ><?php echo $instrumento['descripcion']; ?></td>
                                <td>




                                    <?php if (isset($lotes) && sizeof($lotes) > 0): ?>
                                        <select name="lote_ins[<?php echo $instrumento['id']; ?>][<?php echo $key; ?>]"
                                                data-placeholder="Seleccione el Lote"
                                                class="chzn-select"
                                                data-rules="required"
                                                data-display="Lote"
                                                data-content="Indique el Lote"
                                                title="Lote"
                                                data-rules="required"
                                                >

                                            <option value=""></option>
                                            <?php
                                            $option_lote = '';
                                            foreach ($lotes as $key => $value):
                                                $option_lote = $value['lote'];
                                                if ($value['id_articulo'] == $instrumento['id']):
                                                    ?>
                                                    <option value="<?php echo $value['id']; ?>"  ><?php echo $option_lote; ?></option>
                                                    <?php
                                                endif;
                                            endforeach;
                                            ?>
                                        </select>
                                        <?php
                                    else:
                                        echo $instrumento['lote'];
                                    endif;
                                    ?>

                                </td>
                                <?php
                                $readonly = ' readonly="readonly" ';
                                $value = "value='{$instrumento['cantidad']}'";
                                ?>
                                <td><input min="1" id="cant-instru-<?php echo $instrumento['id']; ?>Orden" type="number" class="input-mini" name="cant_instru[<?php echo $instrumento['id']; ?>]" placeholder="0" key="<?php echo $instrumento['id']; ?>" <?= $readonly ?> <?= $value ?>/></td>
                            </tr>
                            <?php
                        endforeach;
                    endif;
                endforeach;
                ?>
            <?php endif; ?>
        </tbody>
    </table>
    <?php if ($operation == 'detail'): ?>
        <div class="form-actions">
            <input type="button" onclick="navigate_tab('prev',3);" value="Anterior" class="btn btn-primary">
            <input type="button" onclick="navigate_tab('next',3);" value="Siguiente" class="btn btn-primary">
        </div>
    <?php endif ?>
</div>
<script type="text/javascript">

</script>
<input type="hidden" name="ins_valid" id="mat_valid" value="" />