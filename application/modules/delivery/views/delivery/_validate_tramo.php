<?php if ($data): ?>
    <div style="margin-top: 10px; ">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th style="background-color: #F9F9F9;" width="25%"><center>ARTICULO</center></th>
            <th style="background-color: #F9F9F9;">
            <center>

                <table  class="table table-striped table-bordered">
                    <tr>
                        <th colspan="5"><center>LOTES</center></th>
                    </tr>
                    <tr>
                        <th width="10%"></th>
                        <th width="70%">Nombre del Lote</th>
                        <th width="30%">Stock</th>
                    </tr>
                </table>
            </center>
            </th>
            </tr>
            <?php if (isset($data['articles']) and sizeof($data['articles']) > 0): ?>
                <div id="alert_content">
                    <div class="alert alert-error fade in">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <?php
                        foreach ($data['mensajes_error'] as $clave => $valor):
                            foreach ($valor as $key => $value):
                                echo $value . "</br>";
                            endforeach;

                        endforeach;
                        ?>
                    </div>
                </div>


                <?php
                foreach ($data['articles'] as $key => $value):
                    $modelo = current($value);
                    ?>
                    <tr width="25%" ><td ><center><b><?php echo $modelo['modelo'] . "($key)"; ?></b></center></td>
                    <td>
                        <table  class="table table-striped table-bordered">
                            <tbody>
                                <?php foreach ($value as $clave => $valor): ?>
                                    <tr>


                                        <td width="10%" ></td>
                                        <td width="70%"><?php echo $valor['lote']; ?></td>
                                        <td width="30px"><?php echo $valor['stock']; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </td>
                    </tr>
                    <?php
                endforeach;
            else:
                ?>
                <tr><td colspan="2"><center>No Existen </center></td></tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
<?php endif; ?>