<?php
if ($operation == 'detail') {
    ?>
    <script type="text/javascript">
        $(function() {
            new FormValidator('form_15102', buildRules('form_15102'),
                    function(errors, event) {
                        var flag = validateForm(errors, event);

                        if (event && event.preventDefault) {
                            event.preventDefault();
                        } else if (event) {
                            event.returnValue = false;
                        }

                        //validacion submit
                        if (flag) {
                            //este archivo es particular,
                            //Valida TAB DATOS luego de ser validada en el cliente.
                            validate_orden_tab();
                        }
                    });
            customFields();
            build_table_articles();
        });
    </script>
    <?php
}
?>
<div class="modal-body">
    <ul>
        <?php
        if ($operation == 'detail') {
            ?>
            <li id="li-label_datos_cotizacion-15102">
                <br>
                <label for="label_datos_cotizacion-15102">
                    <strong>DATOS DE COTIZACION</strong></label>
                <div id="label-datos_cotizacion-15102"></div>
            </li>

            <input name="tipo_cotizacion"
                   id="tipo_cotizacion-15102"
                   type="hidden"

                   >

            <li  id="li-codigo-15102">
                <div class="control-group">
                    <label class="control-label" for="297">Nro Cotizacion<span style='color:red;font-size: 16px'> *</span></label>
                    <div class="controls">
                        <select name="codigo"
                                id="297"
                                data-placeholder="Seleccione una Cotizacion"
                                class="chzn-select"
                                data-rules="required"
                                data-display="Nro Cotizacion"
                                data-content="Indique la Cotizacion"
                                title="Nro Cotizacion"
                                >
                            <option value=""></option>
                            <?php
                            if (isset($data_cotizacion) && $data_cotizacion):
                                $option_cotizacion = '';
                                foreach ($data_cotizacion as $key => $value):
                                    $option_cotizacion = $value['text'];
                                    ?>
                                    <option value="<?php echo $value['id']; ?>|<?php echo $value['tipo_cotizacion']; ?>"  ><?php echo $option_cotizacion; ?></option>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </select>
                        <span id="button-codigo"></span>
                    </div>
                </div>
            </li>
            <li  id="li-muestra_tipo-15102">
                <div class="control-group">
                    <label class="control-label"  for="muestra_tipo-15102">Tipo de Cotizacion</label>
                    <div class="controls">
                        <input type="text"
                               class="input-xlarge"
                               name="muestra_tipo"
                               id="muestra_tipo-15102"
                               placeholder=""
                               data-rules=""
                               data-display="Cedula/Rif"
                               data-content=""
                               title="Tipo de Cotizacion"
                               readonly="readonly"
                               />
                    </div>
                </div>
            </li>
            <li class='libre data_muestra' id="li-ci_rif-15102">
                <div class="control-group">
                    <label class="control-label"  for="ci_rif-15102">Cedula/Rif</label>
                    <div class="controls">
                        <input type="text"
                               class="input-xlarge"
                               name="ci_rif-15102"
                               id="ci_rif-15102"
                               placeholder=""
                               data-rules=""
                               data-display="Cedula/Rif"
                               data-content=""
                               title="Cedula/Rif"
                               readonly="readonly"
                               />
                    </div>
                </div>
            </li>
            <li class='libre data_muestra' id="li-email-15102">
                <div class="control-group">
                    <label class="control-label"  for="email-15102">Email</label>
                    <div class="controls">
                        <input type="text"
                               class="input-xlarge"
                               name="email-15102"
                               id="email-15102"
                               placeholder=""
                               data-rules=""
                               data-display="email"
                               data-content=""
                               title="email"
                               readonly="readonly"
                               />
                    </div>
                </div>
            </li>
            <li class='data_muestra' id="li-cliente-15102">
                <div class="control-group">
                    <label class="control-label"  for="cliente-15102">Cliente</label>
                    <div class="controls">
                        <input type="text"
                               class="input-xlarge"
                               name="cliente-15102"
                               id="cliente-15102"
                               placeholder=""
                               data-rules=""
                               data-display="Cliente"
                               value=""
                               data-content=""
                               title="Cliente"
                               readonly="readonly"
                               />
                    </div>
                </div>
            </li>
            <li class='normal data_muestra'  id="li-medico-15102">
                <div class="control-group">
                    <label class="control-label"  for="medico-15102">Medico Tratante</label>
                    <div class="controls">
                        <input type="text"
                               class="input-xlarge"
                               name="medico-15102"
                               id="medico-15102"
                               placeholder=""
                               data-rules=""
                               data-display="Medico Tratante"
                               data-content=""
                               title="Medico Tratante"
                               readonly="readonly"
                               />
                    </div>
                </div>
            </li>
            <li class='data_muestra'id="li-vendedor-15102">
                <div class="control-group">
                    <label class="control-label"  for="vendedor-15102">Vendedor</label>
                    <div class="controls">
                        <input type="text"
                               class="input-xlarge"
                               name="vendedor-15102"
                               id="vendedor-15102"
                               placeholder=""
                               data-rules=""
                               data-display="Vendedor"
                               data-content=""
                               title="Vendedor"
                               readonly="readonly"
                               />
                    </div>
                </div>
            </li>
            <li class='normal data_muestra'  id="li-tipo_pago-15102">
                <div class="control-group">
                    <label class="control-label"  for="tipo_pago-15102">Tipo de Pago</label>
                    <div class="controls">
                        <input type="text"
                               class="input-xlarge"
                               name="tipo_pago-15102"
                               id="tipo_pago-15102"
                               placeholder=""
                               data-rules=""
                               data-display="Tipo de Pago"
                               data-content=""
                               title="Tipo de pago"
                               readonly="readonly"
                               />
                    </div>
                </div>
            </li>
            <?php
        }
        ?>
        <li id="li-label_datos_orden-15102">
            <br>
            <label for="label_datos_orden-15102">
                <strong>DATOS DE LA ORDEN</strong></label>
            <div id="label-datos_orden-15102"></div>
        </li>
        <?php
        if ($operation == 'detail') {
            ?>
            <li id="li-fecha_despacho-15102">
                <div class="control-group">
                    <label class="control-label" for="fecha_despacho-15102">Fecha de Despacho<span style='color:red;font-size: 16px'> *</span></label>
                    <div class="controls">
                        <div
                            class="input-append date"
                            data-date=""
                            data-date-format="dd-mm-yyyy"
                            data-date-start-date='<?php echo date("d-m-Y"); ?>'>
                            <input
                                class="date input-small"
                                type="text"
                                id="fecha_despacho-15102"
                                name="fecha_despacho"
                                data-rules="required|valid_date"
                                data-display="Fecha de Despacho"
                                data-content="Indique la Fecha de Despacho"
                                title="Fecha de Despacho"
                                placeholder="Ej: dd-mm-aaaa"
                                readonly="readonly"
                                value="<?php echo date("d-m-Y"); ?>" >

                            <span class="add-on"><i class="icon-calendar"></i></span>
                        </div>
                    </div>
                </div>
            </li>
            <?php
        }
        if ($operation != 'detail') {
            ?>
            <li>
                <div class="control-group">
                    <label class="control-label" >Nro Orden</label>
                    <div class="controls">
                        <input
                            class="input-xlarge"
                            type="text"
                            data-display="Orden de despacho"
                            data-content="Numero de Orden de Despacho"
                            title="Orden de despacho"
                            value="<?php echo (isset($data[0]['codigo']) ? $data[0]['codigo'] : ''); ?>"
                            readonly="readonly" />
                    </div>
                </div>
            </li>
            <li>
                <div class="control-group">
                    <label class="control-label" >Nro Cotizacion</label>
                    <div class="controls">

                        <input
                            class="input-xlarge"
                            type="text"
                            data-display="Nro Cotizacion"
                            data-content="Nro de Cotizacion de la Orden de Despacho"
                            title="Nro Cotizacion"
                            value="<?php echo (isset($data[0]['id_cotizacion']) ? $data[0]['id_cotizacion'] : ''); ?>"
                            readonly="readonly" />
                    </div>
                </div>
            </li>
            <li>
                <div class="control-group">
                    <label class="control-label" >Fecha de solicitud</label>
                    <div class="controls">

                        <input
                            class="input-xlarge"
                            type="text"
                            data-display="Fecha de solicitud"
                            data-content="Fecha de solicitud de la Orden de Despacho"
                            title="Fecha de solicitud"
                            value="<?php echo (isset($data[0]['fecha_solicitud']) ? $data[0]['fecha_solicitud'] : ''); ?>"
                            readonly="readonly"  />
                    </div>
                </div>
            </li>
            <li>
                <div class="control-group">
                    <label class="control-label" >Fecha de Entrega</label>
                    <div class="controls">

                        <input
                            class="input-xlarge"
                            type="text"
                            data-display="Fecha de Entrega"
                            data-content="Fecha de Entrega de la Orden de Despacho"
                            title="Fecha de Entrega"
                            value="<?php echo (isset($data[0]['fecha_despacho']) ? $data[0]['fecha_despacho'] : ''); ?>"
                            readonly="readonly" />
                    </div>
                </div>
            </li>
            <li>
                <div class="control-group">
                    <label class="control-label" >Estatus de la Orden</label>
                    <div class="controls">

                        <input
                            class="input-xlarge"
                            type="text"
                            data-display="Estatus de la Orden"
                            data-content="Estatus de la Orden de Despacho"
                            title="Estatus de la Orden"
                            value="<?php echo (isset($data[0]['cat_estatus_orden_despacho']) ? $data[0]['cat_estatus_orden_despacho'] : ''); ?>"
                            readonly="readonly" />
                    </div>
                </div>
            </li>
            <li>
                <div class="control-group">
                    <label class="control-label" >Despachada Por</label>
                    <div class="controls">

                        <input
                            class="input-xlarge"
                            type="text"
                            data-display="Despachada Por"
                            data-content="Empleado que despacho la Orden de Entrega"
                            title="Despachada Por"
                            value="<?php echo (isset($data[0]['id_despachador']) ? $data[0]['id_despachador'] : ''); ?>"
                            readonly="readonly"  />
                    </div>
                </div>
            </li>
            <?php
        }
        ?>
        <li id="li-id_cirugia-15102">
            <input type="hidden"
                   class="input-xlarge"
                   name="id_cirugia"
                   id="id_cirugia-15102"
                   placeholder=""
                   data-rules=""
                   data-display="Identificador de la Cirugia"
                   value=""
                   data-content=""
                   title="Identificador de la Cirugia"
                   />
        </li>
        <li id="li-id_cotizacion-15102">
            <input type="hidden"
                   class="input-xlarge"
                   name="id_cotizacion"
                   id="id_cotizacion-15102"
                   placeholder=""
                   data-rules=""
                   data-display="Nro Cotizacion"
                   value=""
                   data-content=""
                   title="Nro Cotizacion"
                   />
        </li>
    </ul>
</div>
<?php
if ($operation == 'detail') {
    ?>
    <div class="modal-footer">
        <!--<button class="btn" data-dismiss="modal">Cancelar</button>-->
        <input type="submit" class="btn btn-primary update" value="Siguiente" />
    </div>
    <?php
}
?>