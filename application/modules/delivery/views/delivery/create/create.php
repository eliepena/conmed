<div id="alert_content"></div>
<div id="link"></div>
<form id="form_15102" name="form_15102" action="#" method="POST" class="form-horizontal">
    <?php echo $view ?>
</form>

<script>
    $(function() {
        $("li:has(a[href=#2]),#2").addClass('hide_element');
        $("li:has(a[href=#3]),#3").addClass('hide_element');
        $("li:has(a[href=#4]),#4").addClass('hide_element');
        $("li:has(a[href=#5]),#5").addClass('hide_element');
        $(".normal").css('display', 'none');
        $(".libre").css('display', 'none');
        //
        //        $(".generica").css('display','none');


    });
    $("select[name='id_cliente']").change(function(e) {
        $("select[name='codigo']  option").remove();
        //        $("select[name='codigo']").append('<option value=""></option>');
        $("select[name='codigo']").val('').trigger('liszt:updated');
        var id_client = $(this).val();
        if (id_client != '') {
            //            tipo_per = $(this).val();
            //            load_data_client({
            //                container:{
            //                    cliente:'#cliente-15102',
            //                    ci_rif:'#ci_rif-15102',
            //                    email:'#email-15102'
            //                },
            //                url:"<?= base_url() ?>delivery/delivery/EXC_getDataClient/",
            //                data:{
            //                    id:id_client,
            //                    tipo_persona:tipo_per
            //                }})

            $.ajax({
                type: "post",
                url: "<?= base_url() ?>delivery/EXC_selectCotizacionForCliente",
                data: {id_cliente: id_client},
                success: function(resp) {
                    $("select[name='codigo']  option").remove();
                    $("select[name='codigo'] ").append(resp.data)
                    $("select[name='codigo']").val('').trigger('liszt:updated');
                }
            });
        }

        //        $("select[name='id_cliente']").val('').trigger('liszt:updated');



    });


    var selectTodos = $("select[name='codigo']  option");
    $("select[name='paciente']").change(function(e) {
        $("select[name='codigo']  option").remove();
        $("select[name='codigo']").val('').trigger('liszt:updated');
        id_paciente = $(this).val();
        if (id_paciente == 0) {
            $("select[name='codigo']  option").remove();
            $("select[name='codigo'] ").append(selectTodos)
            $("select[name='codigo']").val('').trigger('liszt:updated');
            $("input[name='cliente-15102']").val('');
            $("input[name='medico-15102']").val('');
            $("input[name='vendedor-15102']").val('');
            $("input[name='tipo_pago-15102']").val('');
        } else {

            $.ajax({
                type: "post",
                url: "<?= base_url() ?>delivery/EXC_selectCotizacion",
                data: {id_paciente: id_paciente},
                success: function(resp) {
                    $("select[name='codigo']  option").remove();
                    $("select[name='codigo'] ").append(resp.data)
                    $("select[name='codigo']").val('').trigger('liszt:updated');
                }
            });
        }



        //$("select[name='codigo'] ").append('<option id=""></option>');
        $("select[name='codigo']").val('').trigger('liszt:updated');

    });

    $("select[name='cat_letra']").change(function(e) {
        $("select[name='id_cliente']  option").remove();
        $("select[name='id_cliente']").val('').trigger('liszt:updated');
        $("select[name='codigo']  option").remove();
        //        $("select[name='codigo']").append('<option value=""></option>');
        $("select[name='codigo']").val('').trigger('liszt:updated');
        cat_letra_value = $(this).val();
        if (cat_letra_value != '') {
            $.ajax({
                type: "post",
                url: "<?= base_url() ?>delivery/EXC_selectClienteForDelivery",
                data: {cat_letra: cat_letra_value},
                success: function(resp) {
                    $("select[name='id_cliente']  option").remove();
                    $("select[name='id_cliente'] ").append(resp.data)
                    $("select[name='id_cliente']").val('').trigger('liszt:updated');
                }
            });
        }

        $("select[name='id_cliente']").val('').trigger('liszt:updated');

    });

    $("select[name='codigo']").change(function(e) {


        if ($(this).val() != "") {

            valor_cot = $(this).val().split('|');
            id_cot = valor_cot[0];
            tipo_cotizacion = valor_cot[1];
            //            alert(tipo_cotizacion)
            //tipo_cotizacion = $("select[name='tipo_cotizacion']").val();
            if (tipo_cotizacion == 'normal') {
                $("li:has(a[href=#2])").html('<a data-toggle="tab" href="#2">Materiales</a>');
                //                $("li:has(a[href=#3])").html('<a style="color:#0069B5" data-toggle="tab" href="#3">Instrumental</a>');
                $("li:has(a[href=#3])").html('<a style="color:red" data-toggle="tab" href="#3">Instrumental</a>');
                $("li:has(a[href=#4])").html('<a style="color:#0069B5" data-toggle="tab" href="#4">Backup</a>');
                $(".data_muestra").css('display', 'block');
                $(".normal").css('display', 'block');
                $(".libre").css('display', 'none');
                load_cot({
                    container: {
                        medico: '#medico-15102',
                        vendedor: '#vendedor-15102',
                        cliente: '#cliente-15102',
                        tipo_pago: '#tipo_pago-15102',
                        id_cotizacion: '#id_cotizacion-15102',
                        id_articulacion: '#id_cirugia-15102',
                        mat: '#2-container',
                        backup: '#4-container',
                        instru: '#3-container',
                        resumen: '#5-container',
                        tipo_c:'#tipo_cotizacion-15102',
                        muestra_tipo:'#muestra_tipo-15102'
                    },
                    url: "<?= base_url() ?>delivery/EXC_getDataCotizacion/",
                    data: {
                        id: id_cot, tipo_cotizacion: tipo_cotizacion
                    }});
            } else if (tipo_cotizacion == 'libre') {
                $("li:has(a[href=#2])").html('<a data-toggle="tab" href="#2">Articulos</a>');
                $("li:has(a[href=#3])").html('<a style="color:red" data-toggle="tab" href="#3">Instrumental</a>');
                $("li:has(a[href=#4])").html('<a style="color:red" data-toggle="tab" href="#4">Backup</a>');
                $(".data_muestra").css('display', 'block');
                $(".libre").css('display', 'block');
                $(".normal").css('display', 'none');
                cat_letra_value = $("select[name='cat_letra']").val();
                load_cot_for_client({
                    container: {
                        //                        medico:'#medico-15102',
                        //                        vendedor:'#vendedor-15102',
                        //                        cliente:'#cliente-15102',
                        //                        tipo_pago:'#tipo_pago-15102',
                        id_cotizacion: '#id_cotizacion-15102',
                        //                        id_articulacion:'#id_cirugia-15102',
                        //                        mat:'#2-container',
                        //                        backup:'#4-container',
                        //                        instru:'#3-container',
                        //                        resumen:'#5-container'
                        cliente: '#cliente-15102',
                        ci_rif: '#ci_rif-15102',
                        email: '#email-15102',
                        mat: '#2-container',
                        backup: '#4-container',
                        instru: '#3-container',
                        resumen: '#5-container',
                        vendedor: '#vendedor-15102',
                        tipo_c:'#tipo_cotizacion-15102',
                        muestra_tipo:'#muestra_tipo-15102'
                    },
                    url: "<?= base_url() ?>delivery/EXC_getDataClient/",
                    data: {
                        id: id_cot, tipo_persona: cat_letra_value, tipo_cotizacion: tipo_cotizacion
                    }});
            }


        } else {
            //            Reset a los datos la cotizacion
            $("#ci_rif-15102").val('');
            $("#email-15102").val('');
            $("#cliente-15102").val('');
            $("#vendedor-15102").val('');
            $("#medico-15102").val('');
            $("#tipo_pago-15102").val('');
            $(".data_muestra").css('display', 'none');
        }
        //Oculta las pesta#as de articulos
        $("li:has(a[href=#2]),#2").addClass('hide_element');
        $("li:has(a[href=#3]),#3").addClass('hide_element');
        $("li:has(a[href=#4]),#4").addClass('hide_element');
        $("li:has(a[href=#5]),#5").addClass('hide_element');
    });

    function load_cot(params) {
        $('body').modalmanager('loading');
        //Dispara el evento

        $.ajax({
            type: "POST",
            url: params.url,
            data: params.data,
            dataType: 'json',
            success: function(response) {
                $('body').modalmanager('loading');
                if (response.success) {
                    $(params.container.medico).val(response.data.id_medico);
                    $(params.container.vendedor).val(response.data.id_vendedor);
                    $(params.container.cliente).val(response.data.id_paciente);
                    $(params.container.tipo_pago).val(response.data.tipo_pago);
                    $(params.container.id_cotizacion).val(response.data.id);
                    $(params.container.id_articulacion).val(response.data.cod_articulacion);
                    $(params.container.mat).html(response.materiales);
                    $(params.container.backup).html(response.backup);
                    //$(params.container.instru).html(response.instrumentos);
                    $(params.container.resumen).html(response.resumen);
                    $(params.container.tipo_c).val(response.data.tipo_cot);
                    $(params.container.muestra_tipo).val(response.data.tipo_cotizacion);

                    checkboxs_disponibilidad('materialTableOrden', 'mat', 1);
                    checkboxs_disponibilidad('materialTableOrden', 'mat', 0);

                    checkboxs_disponibilidad('instrumentoTableOrden', 'ins', 1);
                    checkboxs_disponibilidad('instrumentoTableOrden', 'ins', 0);

                    checkboxs_disponibilidad('materialBackupTableOrden', 'backup', 1);
                    checkboxs_disponibilidad('materialBackupTableOrden', 'backup', 0);
                }
            }
        });
    }
    function load_cot_for_client(params) {
        $('body').modalmanager('loading');
        //Dispara el evento

        $.ajax({
            type: "POST",
            url: params.url,
            data: params.data,
            dataType: 'json',
            success: function(response) {
                $('body').modalmanager('loading');
                if (response.success) {
                    $(params.container.cliente).val(response.data.cliente);
                    $(params.container.ci_rif).val(response.data.cedula_rif);
                    $(params.container.email).val(response.data.email);
                    $(params.container.id_cotizacion).val(response.data.id);
                    $(params.container.vendedor).val(response.data.vendedor);
                    //                    $(params.container.id_articulacion).val(response.data.cod_articulacion);
                    $(params.container.mat).html(response.materiales);
                    $(params.container.backup).html(response.backup);
                    $(params.container.instru).html(response.instrumentos);
                    $(params.container.resumen).html(response.resumen);
                    $(params.container.tipo_c).val(response.data.tipo_cot);
                    $(params.container.muestra_tipo).val(response.data.tipo_cotizacion);


                }
            }
        });
    }
    /**
     * Valida el tab de DATOS de la cotizacion para poder continuar con la carga de informacion
     *
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V- 1.0  26/08/13 03:55 PM
     */
    function validate_orden_tab() {
        $('body').modalmanager('loading');
        //Dispara el evento
        $.ajax({
            url: BASE_URL + 'delivery/delivery/create/process/validate_data',
            data: $('#form_15102').serialize(),
            success: function(response) {
                $('body').modalmanager('loading');

                //Verifica si el TAB DATOS se valida exitosamente y muestra los demas TABS
                if (response.validate == 1) {
                    $("li:has(a[href=#1]),#1").removeClass("active");
                    $("li:has(a[href=#2]),#2").removeClass('hide_element');
                    $("li:has(a[href=#3]),#3").removeClass('hide_element');
                    $("li:has(a[href=#4]),#4").removeClass('hide_element');
                    $("li:has(a[href=#5]),#5").removeClass('hide_element');
                    $("li:has(a[href=#2]),#2").addClass("active");

                    //Quita el mensaje de Alerta Error
                    $("#alert_content").empty();
                }

                //Se procesa cuando no se pueden validar los datos
                else {
                    //Se mantiene o muestra nuevamente el TAB inicial y oculta los demas TABS
                    $("li:has(a[href=#2]),#2").removeClass("active");
                    $("li:has(a[href=#1]),#1").addClass("active");
                    $("li:has(a[href=#2]),#2").addClass('hide_element');
                    $("li:has(a[href=#3]),#3").addClass('hide_element');
                    $("li:has(a[href=#4]),#4").addClass('hide_element');
                    $("li:has(a[href=#5]),#5").addClass('hide_element');

                    var textAlert = null;

                    //Ocurre cuando hay un error en Base de Datos
                    if (response.view == false)
                        textAlert = 'No se pudo validar los datos, si el problema persiste comuniquese con sistemas';

                    //Ocurre cuando hay errores de validacion en el Servidor
                    else
                        textAlert = response.view;

                    //Mensaje de Alerta Error
                    var alertHtml = [
                        '<div class="alert alert-error fade in">',
                        '<button type="button" class="close" data-dismiss="alert">×</button>',
                        textAlert,
                        '</div>'
                    ].join('');
                    //Elemento que muestra el Alert
                    $("#alert_content").html(alertHtml);
                }
            }
        });
    }


</script>