<div style="margin-top: 10px; margin-left: 20px">
    <table id="materialBackupTableOrden" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th width="10px"></th>
                <th>Modelo</th>
                <th>Descripción</th>
                <th>Lote</th>
                <th width="4px">Cant</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (isset($backup) and !empty($backup)):
                foreach ($backup as $value):
                    if ($value):

                        foreach ($value as $key => $material):
                            ?>
                            <tr>
                                <td><a data-toggle="modal" href="#" onclick="show_img_article('<?php echo $material['modelo']; ?>',  '<?php echo $material['descripcion']; ?>', '<?php echo $material['imagen']; ?>');"><i class="icon-picture"></i></a></td>
                                <td><?php echo $material['modelo']; ?></td>
                                <td><?php echo $material['descripcion']; ?></td>
                                <td>




                                    <?php if (isset($lotes) && sizeof($lotes) > 0): ?>
                                        <select name="lote_backup[<?php echo $material['id']; ?>][<?php echo $key; ?>]"
                                                data-placeholder="Seleccione el Lote"
                                                class="chzn-select"
                                                data-rules="required"
                                                data-display="Lote"
                                                data-content="Indique el Lote"
                                                title="Lote"
                                                data-rules="required"
                                                >

                                            <option value=""></option>
                                            <?php
                                            $option_lote = '';
                                            foreach ($lotes as $key => $value):
                                                $option_lote = $value['lote'];
                                                if ($value['id_articulo'] == $material['id']):
                                                    ?>
                                                    <option value="<?php echo $value['id']; ?>"  ><?php echo $option_lote; ?></option>
                                                    <?php
                                                endif;
                                            endforeach;
                                            ?>
                                        </select>
                                        <?php
                                    else:
                                        echo $material['lote'];
                                    endif;
                                    ?>





                                </td>
                                <td><?php echo $material['cantidad']; ?></td>

                            </tr>
                            <?php
                        endforeach;
                    endif;
                endforeach;

            endif;
            ?>
        </tbody>
        <tfoot>

        </tfoot>
    </table>
    <?php if ($operation == 'detail'): ?>
        <div class="form-actions">
            <input type="button" onclick="navigate_tab('prev',4);" value="Anterior" class="btn btn-primary">
            <input type="button" onclick="navigate_tab('next',4);" value="Siguiente" class="btn btn-primary">
        </div>
    <?php endif ?>
</div>

<script type="text/javascript">

    $('input[type="checkbox"].style3').checkbox({
        buttonStyle: 'btn-danger',
        buttonStyleChecked: 'btn-success',
        checkedClass: 'icon-check',
        uncheckedClass: 'icon-check-empty'
    });


    function show_add(id){

        if ($("."+id).is (':visible')){

            $("."+id).hide();
        }else{
            $(".sustituto").hide();
            $("."+id).show();
        }


    }

</script>