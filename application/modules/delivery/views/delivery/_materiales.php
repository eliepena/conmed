<div style="margin-top: 10px; margin-left: 20px">
    <table id="materialTableOrden" class=" <?php echo ($operation == 'detail' ? '' : ' js-table '); ?>  table table-striped table-bordered">
        <thead>
            <tr>
                <th width="10px"></th>
                <th>Modelo</th>
                <th>Descripción</th>
                <th width="10px">Lote</th>
                <th width="10px">Cantidad</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (isset($materiales) and !empty($materiales)):
                foreach ($materiales as $clave => $valor):
                    if ($valor):
                        foreach ($valor as $key => $material):
                            ?>
                        <td><a data-toggle="modal" href="#" onclick="show_img_article('<?= $material['modelo'] ?>',  '<?= $material['descripcion'] ?>', '<?= $material['imagen'] ?>');"><i class="icon-picture"></i></a></td>
                        <td id="model-mat-<?= $material['id'] ?>Orden"><?php echo $material['modelo'] ?></td>
                        <td id="name-mat-<?= $material['id'] ?>Orden"><?php echo $material['descripcion'] ?></td>
                        <td>
                            <?php if (isset($lotes) && sizeof($lotes) > 0): ?>
                                <select name="lote_mat[<?php echo $material['id']; ?>][<?php echo $key; ?>]"
                                        data-placeholder="Seleccione el Lote"
                                        class="chzn-select"
                                        data-rules="required"
                                        data-display="Lote"
                                        data-content="Indique el Lote"
                                        title="Lote"
                                        data-rules="required"
                                        >

                                    <option value=""></option>
                                    <?php
                                    $option_lote = '';
                                    foreach ($lotes as $key => $value):
                                        $option_lote = $value['lote'];
                                        if ($value['id_articulo'] == $material['id']):
                                            ?>
                                            <option value="<?php echo $value['id']; ?>"  ><?php echo $option_lote; ?></option>
                                            <?php
                                        endif;
                                    endforeach;
                                    ?>
                                </select>
                                <?php
                            else:
                                echo $material['lote'];
                            endif;
                            ?>


                        </td>
                        <?php
                        $readonly = 'readonly="readonly"';
                        $value = "value='{$material['cantidad']}'";
                        ?>
                        <td>
                            <input min="1" type="number" id="cant-mat-<?= $material['id'] ?>Orden" class="input-mini" name="cant_mat[<?= $material['id'] ?>]" placeholder="0" key="<?= $material['id'] ?>" <?= $readonly ?> <?= $value ?> />
                        </td>

                        </tr>
                        <?php
                    endforeach;
                endif;
            endforeach;
            ?>
        <?php endif; ?>
        </tbody>
        <tfoot>

        </tfoot>
    </table>
    <?php
    if ($operation == 'detail'):
        ?>
        <div class="form-actions">
            <input type="button" onclick="navigate_tab('prev',2);" value="Anterior" class="btn btn-primary">
            <input type="button" onclick="navigate_tab('next',2);" value="Siguiente" class="btn btn-primary">
        </div>
    <?php endif ?>

</div>
<input type="hidden" name="mat_valid" id="mat_valid" value="" />

