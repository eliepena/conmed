<?php if (isset($data)): ?>
    <table style="" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th colspan="3" style="width: 100%;">
        <center>DATOS DE COTIZACION</center>
    </th>
    </tr>
    <tr>
        <th >Nro.</th>
        <th >Fecha Cirugia</th>
        <th >Hora Cirugia</th>


    </tr>
    </thead>
    <tbody>
        <tr>
            <td style="width: 33%;"><?php echo (isset($data['codigo']) ? $data['codigo'] : 'NO APLICA'); ?></td>
            <td style="width: 33%;"><?php echo (isset($data['fecha_cirugia']) ? $data['fecha_cirugia'] : 'NO APLICA'); ?></td>
            <td style="width: 33%;"><?php echo (isset($data['hora_cirugia']) ? $data['hora_cirugia'] : 'NO APLICA'); ?></td>
        </tr>
        <?php if ($data['tipo_cotizacion'] == 'normal'): ?>
            <tr>
                <td style="width: 10%;"><b>Paciente</b></td>
                <td style="width: 75%;" colspan="2"><?php echo (isset($data['id_paciente']) ? $data['id_paciente'] : 'NO APLICA'); ?></td>
            </tr>
            <tr>
                <td style="width: 25%;"><b>Medico</b></td>
                <td style="width: 75%;" colspan="2"><?php echo (isset($data['id_medico']) ? $data['id_medico'] : 'NO APLICA'); ?></td>
            </tr>
            <tr>
                <td style="width: 25%;"><b>Direccion de Envio</b></td>
                <td style="width: 75%;" colspan="2"><?php echo (isset($data['enviar']) ? $data['enviar'] : 'NO APLICA'); ?></td>
            </tr>
        <?php endif; ?>
        <tr>
            <td style="width: 25%;"><b>Vendedor</b></td>
            <td style="width: 75%;" colspan="2"><?php echo (isset($data['id_vendedor']) ? $data['id_vendedor'] : 'NO APLICA'); ?></td>
        </tr>

        <tr>
            <td colspan="3" style="width: 100%;">
    <center><b>
            <?php echo($operation == 'detail' ? 'VISTA PREVIA' : 'IMPRESION DE ORDEN DE DESPACHO'); ?>
        </b>
    </center>
    </td>
    </tr>
    <tr>
        <!--<td style="width: 25%;"><b>Materiales</b></td>-->
        <td  style="width: 100%;" colspan="3">
    <center>
    <!--        <a id="mat_resumen_" style="visibility: hidden;color:green" href="# " >Disponibles: <span></span></a> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;
            <a id="mat_resumen_not" style="visibility: hidden;color:red" href="# " >No Disponibles: <span></span></a>-->

        <div id="validate_tramo" class="<?php if ($operation != 'detail'): ?> form-actions <?php endif; ?>">

            <?php if ($operation == 'detail'): ?>
                <?php if ($data['tipo_cotizacion'] == 'libre'): ?>
                    <button  type="button" onclick = "print_mode_orden('<?php echo $this->encrypt->encode($data['id']); ?>','materiales_free',true,'delivery/delivery/create/process/preview/materiales_free')" class="btn btn-primary btn-mini"  data-toggle="modal" role="button" > Orden Articulos </button>
                <?php elseif ($data['tipo_cotizacion'] == 'normal'): ?>
                    <button  type="button" onclick = "print_mode_orden('<?php echo $this->encrypt->encode($data['id']); ?>','materiales',true,'delivery/delivery/create/process/preview/materiales')" class="btn btn-primary btn-mini"  data-toggle="modal" role="button" > Orden Materiales </button>
<!--                    <button  type="button" onclick = "print_mode_orden('<?php echo $this->encrypt->encode($data['id']); ?>','instrumento',true,'delivery/delivery/create/process/preview/instrumento')" class="btn btn-primary btn-mini"  data-toggle="modal" role="button" > Orden Instrumento </button>-->
                    <button  type="button" onclick = "print_mode_orden('<?php echo $this->encrypt->encode($data['id']); ?>','backup',true,'delivery/delivery/create/process/preview/backup')" class="btn btn-primary btn-mini"  data-toggle="modal" role="button" > Orden BackUp </button>
                <?php endif; ?>
            <?php else: ?>
                <?php if ($data['tipo_cotizacion'] == 'libre'): ?>
                    <button  type="button" onclick = "print_mode_orden('<?php echo $this->encrypt->encode($data['id']); ?>','materiales_free',false)" class="btn btn-primary btn-mini"  data-toggle="modal" role="button" > Orden Articulos </button>
                <?php elseif ($data['tipo_cotizacion'] == 'normal'): ?>
                    <button  type="button" onclick = "print_mode_orden('<?php echo $this->encrypt->encode($data['id']); ?>','materiales',false)" class="btn btn-primary btn-mini"  data-toggle="modal" role="button" > Orden Materiales </button>
<!--                    <button  type="button" onclick = "print_mode_orden('<?php echo $this->encrypt->encode($data['id']); ?>','instrumento',false)" class="btn btn-primary btn-mini"  data-toggle="modal" role="button" > Orden Instrumento </button>-->
                    <button  type="button" onclick = "print_mode_orden('<?php echo $this->encrypt->encode($data['id']); ?>','backup',false)" class="btn btn-primary btn-mini"  data-toggle="modal" role="button" > Orden BackUp </button>
                    <button  type="button" onclick = "print_mode_orden('<?php echo $this->encrypt->encode($data['id']); ?>','almacen',false)" class="btn btn-success btn-mini"  data-toggle="modal" role="button" > Orden Almacen </button>
                <?php endif; ?>
            <?php endif; ?>

            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </div>
    </center>
    <!--        <span style="float: right;">

        </span>-->
    </td>
    </tr>
    <!--<tr>
        <td style="width: 25%;"><b>Instrumentos</b></td>
        <td style="width: 75%;" colspan="2">

            <a id="ins_resumen_" style="visibility: hidden;color:green" href="#" >Disponibles: <span></span></a> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;
            <a id="ins_resumen_not" style="visibility: hidden;color:red" href="#" >No Disponibles: <span></span></a>
            <span style="float: right;">
                <button  type="button" onclick = "print_mode_orden('<?php //echo $this->encrypt->encode($data['id']);                               ?>','instrumento',false)" class="btn btn-primary btn-mini"  data-toggle="modal" role="button" > Orden Instrumento </button>
            </span>
        </td>
    </tr>
    <tr>
        <td style="width: 25%;"><b>Backup</b></td>
        <td style="width: 75%;" colspan="2">
            <a id="backup_resumen_" style="visibility: hidden;color:green" href="# " >Disponibles: <span></span></a> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;
            <a id="backup_resumen_not" style="visibility: hidden;color:red" href="# " >No Disponibles: <span></span></a>
            <span style="float: right;">
                <button  type="button" onclick = "print_mode_orden('<?php //echo $this->encrypt->encode($data['id']);                               ?>','backup',false)" class="btn btn-primary btn-mini"  data-toggle="modal" role="button" > BackUp </button>
            </span>
        </td>
    </tr>-->
    </tbody>
    </table>
    <?php
    if ($operation == 'detail') {
        ?>
        <div class="form-actions">
            <input type="button" onclick="navigate_tab('prev',5);" value="Anterior" class="btn btn-primary">
            <!--<input type="button" onclick="print_mode(null,'Vista Previa',true)" value="PreVisualizar" class="btn btn-primary">-->
            <input type="button" onclick="save_delivery()" value="Guardar" class="btn btn-primary">
        </div>
        <?php
    }
    ?>

    <script>
        function save_delivery(){
            var formData = $('#form_15102').serializeArray();
            $('body').modalmanager('loading');
            //Dispara el evento
            $.ajax({
                url: BASE_URL +'delivery/delivery/create/process/',
                data: formData,

                success: function(response){
                    $('body').modalmanager('loading');
                    //alert(4);
                    var textAlert = null, alertHtml = null;

                    //Se procesa cuando el registro se guarda exitosamente
                    if(response.view == 1){
                        $('#validate_tramo').empty();
                        //alert(3);
                        $("#form_15102").hide();
                        textAlert = 'Datos guardados satisfactoriamente';
                        alertHtml = [
                            '<div class="alert alert-success fade in">',
                            '<button type="button" class="close" data-dismiss="alert">×</button>',
                            textAlert,
                            '</div>'
                        ].join('');
                        var link ='<a id="link" href="#" class="btn btn-success" onclick="request({url:\''+BASE_URL+'delivery/delivery/listAll\'})">Regresar al listado</a>' ;
                        $("#link").html(link);
                        $('#link').on('click', function() {
                            request({
                                url:BASE_URL +'delivery/delivery/listAll'
                            });
                        });
                        setTimeout(function(){
                            $('#link').trigger('click');
                        },2000);
                        setTimeout(function(){
                            $('#modal_window').modal('hide');
                        },1500);

                    }

                    //Se procesa cuando no se puede guardar el registro
                    else{
                        $('#validate_tramo').empty();
                        //alert(2);
                        //Ocurre cuando hay un error en Base de Datos
                        if(response.view == false){
                            $('#validate_tramo').empty();
                            textAlert = 'No se pudo guardar el registro, si el problema persiste comuniquese con sistemas';

                            //Ocurre cuando hay errores de validacion en el Servidor
                        }else if(response.view == 2){
                            textAlert = response.msg;
                            $('#validate_tramo').empty();
                            $('#validate_tramo').html(response.data);
                            //Mensaje de Alerta Error
                            alertHtml = [
                                '<div class="alert alert-error fade in">',
                                '<button type="button" class="close" data-dismiss="alert">×</button>',
                                textAlert,
                                '</div>'
                            ].join('');
                        }else{
                            $('#validate_tramo').empty();
                            //alert(0);
                            textAlert = response.view;

                            //Mensaje de Alerta Error
                            alertHtml = [
                                '<div class="alert alert-error fade in">',
                                '<button type="button" class="close" data-dismiss="alert">×</button>',
                                textAlert,
                                '</div>'
                            ].join('');
                        }
                    }
                    //Elemento que muestra el Alert
                    $("#alert_content").html(alertHtml);

                }
            });
        }




        function checkboxs_disponibilidad(id_table,tipo_articulo,disponible) {
            var articulos = new Array();
            var iBase = '#' + id_table;
            var iTBody = iBase + ' tbody ';
            var count=0;
            var add_conten='';

            $(iTBody +' input[type=checkbox]').each(function(){

                var _this = $(this);
                if(disponible==1){
                    if(_this.is(':checked')){
                        articulos.push(_this.val());
                        count=count+1;

                    }
                }else{
                    if(!_this.is(':checked')){
                        articulos.push(_this.val());
                        count=count+1;
                        add_conten='not';

                    }
                }

            });

            var sub_array="{'articulos':["+articulos.toString()+"]}";
            $('#'+tipo_articulo+'_resumen_'+add_conten).attr('onclick','articulos_resumen('+sub_array+')');
            $('#'+tipo_articulo+'_resumen_'+add_conten+' span').html(count);
        }
        function articulos_resumen(params){
            alert(params.articulos[0])
        }

        /**
         * Define el modo de impresion del documento.
         *
         * @param  {string} id_cotizacion   Identificador de la Cotizacion
         * @param  {string} tipo_documento  Datos para la busqueda de datos
         * @param  {boolean} mode_preview   Identifica si la impresion en preview o no.
         * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
         * @version V- 1.0  05/09/13 02:52 PM
         */
        function print_mode_orden(id_cotizacion,tipo_documento,mode_preview,url){
            var membrete = false;
            bootbox.dialog('Impresion del documento '+tipo_documento,[{
                    'label': "Sin Membrete!",
                    'class': "btn-danger",
                    'callback': function() {
                        membrete = false;
                        if(mode_preview){
                            preview_pdf_orden(membrete,url,tipo_documento,mode_preview,id_cotizacion);
                        }else{
                            cotizacionDocumento_orden(id_cotizacion,tipo_documento,membrete);
                        }
                    }
                },{

                    'label': "Con Membrete",
                    'class': "btn-primary",
                    'callback': function() {
                        membrete = true;
                        if(mode_preview){
                            preview_pdf_orden(membrete,url,tipo_documento,mode_preview,id_cotizacion);
                        }else{
                            cotizacionDocumento_orden(id_cotizacion,tipo_documento,membrete);
                        }
                    }
                }]);
        }

        /**
         * Obtiene el reporte de la orden de despacho (PDF)  de acuerdo al tipo de documento (backup, instrumental, cotizacion)
         *
         * @param  {string} id_cotizacion  Identificador de la Cotizacion
         * @param  {string} tipo_documento  Datos para la busqueda de datos
         * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
         * @version V- 1.0  26/08/13 03:55 PM
         */
        function cotizacionDocumento_orden(id_cotizacion,tipo_documento,membrete){

            $('body').modalmanager('loading');
            //Dispara el evento
            $.ajax({
                url: BASE_URL +'delivery/delivery/EXC_quoteReport/',
                data: {
                    id:id_cotizacion,
                    tipo:tipo_documento,
                    membrete:membrete
                },
                success: function(response){
                    $('body').modalmanager('loading');
                    if(response.sucess){
                        pop_pup = window.open(BASE_URL +"delivery/delivery/EXC_force_download?url="+response.file+"&name="+tipo_documento,'Pdf'+tipo_documento, 'width=1,height=1,scrollbars=NO');
                        if(!pop_pup){
                            bootbox.alert('Disculpe! Verifique que las ventanas emergentes de su navegador no esten bloqueadas.');
                        }else{
                            setInterval(function() {
                                pop_pup.close();
                            }, 9000);
                        }
                    }else{
                        var textAlert = 'No se pudo generar el documento, si el problema persiste comuniquese con sistemas';
                        bootbox.alert(textAlert);
                    }
                }
            });
        }

        /**
         * Obtiene el pdf Preview de la cotizacion
         *
         * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
         * @version V- 1.0  29/08/13 04:51 PM
         */
        function preview_pdf_orden(membrete,url,tipo_documento,mode_preview,id_cotizacion){
            var formData = $('#form_15102').serializeArray();
            formData.push(
            {
                name:"membrete" ,
                value:membrete
            },
            {
                name:'tipo',
                value:tipo_documento
            },
            {
                name:'mode_preview',
                value:mode_preview
            },
            {
                name:'id',
                value:id_cotizacion
            });
            $('body').modalmanager('loading');
            //Dispara el evento
            $.ajax({
                url: BASE_URL +url,
                data: formData,

                success: function(response){
                    $('body').modalmanager('loading');

                    //Se procesa cuando se ejecuta exitosamente el controlador
                    if(response.view == 1){
                        pop_pup = window.open(BASE_URL +"delivery/delivery/EXC_force_download?url="+response.file+"&name=Pdf_Preview",'PdfPreview', 'width=1,height=1,scrollbars=NO');
                        if(!pop_pup){
                            bootbox.alert('Disculpe! Verifique que las ventanas emergentes de su navegador no esten bloqueadas.');
                        }else{
                            setInterval(function() {
                                pop_pup.close();
                            }, 9000);
                        }

                    }
                    //Se procesa cuando no se ejecuta exitosamente el controlador
                    else{
                        //Muestra nuevamente el TAB inicial
                        //                        $("li:has(a[href=#2]),#2").addClass("active");
                        //                        $("li:has(a[href=#5]),#5").removeClass("active");

                        var textAlert = null;

                        //Ocurre cuando hay un error en Base de Datos
                        if(response.view == false)
                            textAlert = 'No se pudo generar la vista previa, si el problema persiste comuniquese con sistemas';

                        //Ocurre cuando hay errores de validacion en el Servidor
                        else
                            textAlert = response.view;

                        //Mensaje de Alerta Error
                        var alertHtml = [
                            '<div class="alert alert-error fade in">',
                            '<button type="button" class="close" data-dismiss="alert">×</button>',
                            textAlert,
                            '</div>'
                        ].join('');

                        //Elemento que muestra el Alert
                        $("#alert_content").html(alertHtml);
                    }
                }
            });
        }
        $(".chzn-select").chosen({
            allow_single_deselect:true
        });
    </script>
<?php endif; ?>