<html>
    <head>
        <title></title>
        <style>
            body {
                font-size:9.5pt;
            }
            .head{
                background-color: #A5A5A5;
                font-weight:bold;
            }
            .right{
                text-align:right;
            }
            .left{
                text-align:left;
            }
            .center{
                text-align: center;
            }
            .borde_ext{

                border: 1px black solid;
            }
            .borde{

                border-top: 1px black solid;
                border-right: 1px black solid;
            }
            .borde_bottom{
                border-bottom:  1px black solid;
            }
            .borde_right{
                border-right:   1px black solid;
            }
            .borde_top{
                border-top:   1px black solid;
            }
            .borde_left{
                border-left: 1px black solid;
            }
        </style>
    </head>
    <body>
        <table border="0" >
            <thead>
                <tr>
                    <th colspan ="7" style="font-size:14pt;font-weight: bold;width:104%;text-align: right;height: 20px;padding-left:  30%;" >
                        <?php echo $this->lang->line('free_PdfOrd_Title_Mat'); ?>
                        &nbsp;&nbsp;&nbsp;
                    </th>
                </tr>
                <tr>
                    <th style="width:30%" rowspan="4" colspan ="1" class="borde_top borde_bottom borde_left left ">
                        <br/>
                        <b><?php echo $this->lang->line('free_PdfOrd_Client'); ?>:</b>
                        <br>
                        <font size="10"><?php echo $cliente;             ?></font>
                    </th>
                    <th style="width:15%" rowspan="4" colspan ="1" class="borde_top borde_bottom borde_left left ">
                        <br/>
                        <b><?php echo $this->lang->line('free_PdfOrd_ced_Rif'); ?>:</b>
                        <font size="10"><?php echo $ced_rif;             ?></font>
                    </th>
                    <th style="width:14%" rowspan="4" colspan ="1" class="borde_top borde_bottom borde_left borde_right left">
                        <br/>
                        <b><?php echo $this->lang->line('free_PdfOrd_Nit'); ?>:</b>
                        <br/>
                        <font size="10"><?php //echo $nit;             ?></font>
                    </th>
                    <th style="width:22%" colspan ="2" class="  right">
                        <?php echo $this->lang->line('free_PdfOrd_NroOrd'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php echo $codigo_orden; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:22%" colspan ="2" class="  right">
                        <?php echo $this->lang->line('free_PdfOrd_FecEmi'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php echo $fecha_emision; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:22%" colspan ="2" class="  right">
                        <?php // echo $this->lang->line('free_PdfOrd_ForPag'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php //echo $forma_pago; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:22%" colspan ="2" class="  right">
                        <?php // echo $this->lang->line('free_PdfOrd_OrdenCompra'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php //echo $tiempo_entrega; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:59%"  colspan ="3" class="borde_top borde_bottom borde_left borde_right left ">
                        <br/>
                        <b><?php echo $this->lang->line('free_PdfOrdEmail'); ?>:</b>
                        <font size="10"><?php echo $email;?></font>
                    </th>
                    <th style="width:22%" colspan ="2" class="  right">
                        <?php // echo $this->lang->line('free_PdfOrd_Vend'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php //echo $vendedor; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:59%"  colspan ="3" class="borde_top borde_bottom borde_left borde_right left ">
                        <br/>
                        <b><?php echo $this->lang->line('free_PdfOrd_Tlf'); ?>:</b>
                        <font size="10"><?php echo $tlf;             ?></font>
                    </th>
                    <th style="width:22%" colspan ="2" class="  right">
                        <?php // echo $this->lang->line('free_PdfOrd_MedTratant'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php //echo $dr_tratante; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:59%" colspan ="3" class="borde_top ">
                        <b><?php echo $this->lang->line('free_PdfOrd_MsjTabla'); ?>:</b>
                    </th>
                    <th style="width:22%" colspan ="2" class="  right">
                        <?php // echo $this->lang->line('free_PdfOrd_NroImport'); ?>:
                    </th>
                    <th style="width:23%" colspan ="2" class="right" ><?php //echo $nro_cotizacion;             ?></th>
                </tr>
                <tr class="center">

                    <th style="width:6%" class="borde_top borde_bottom borde_left head">
                        <?php echo $this->lang->line('free_PdfOrd_Cod'); ?>
                    </th>

                    <th style="width:16%" class="borde_top borde_bottom borde_left head">
                        <?php echo $this->lang->line('free_PdfOrd_Mod'); ?>
                    </th>
                    <th style="width:36%" class="borde_top borde_bottom borde_left head">
                        <?php echo $this->lang->line('free_PdfOrd_Descrip'); ?>
                    </th>
                    <th style="width:11%" class="borde_top borde_bottom borde_left head">
                        Lot
                    </th>
                    <th style="width:7%" class="borde_top borde_bottom borde_left head">
                        <?php echo $this->lang->line('free_PdfOrd_Cant'); ?>
                    </th>
                    <th style="width:14%" class="borde_top borde_bottom borde_left head">
                        <?php echo $this->lang->line('free_PdfOrd_Prec'); ?>
                    </th>
                    <th style="width:14%" class="borde_top borde_bottom borde_left borde_right head">
                        <?php echo $this->lang->line('free_PdfOrd_NetTabla'); ?>
                    </th>

                </tr>
            </thead>
            <tbody>
                <?php
                $monto_sub_total = 0;
                $calculo_iva = 0;
                $monto_total = 0;
                $cant_articulos = 0;
                $count = 1;
                if (isset($articulos) && sizeof($articulos) > 0):
                    $cant_articulos = sizeof($articulos);

                    foreach ($articulos AS $clave => $valor):
                        $style_row = ($count % 2 == 0 ? '' : '');
                        echo '<tr >

                                <td style="width:6%;' . $style_row . '" class="borde_left border_right">' . $valor['id'] . '</td>
                                <td style="width:16%;' . $style_row . '" class="borde_left border_right">' . $valor['modelo'] . '</td>
                                <td style="width:36%;' . $style_row . '" class="borde_left">' . $valor['descripcion'] . '</td>
                                <td style="width:11%;' . $style_row . '" class="borde_left border_right">' . $valor['lote'] . '</td>
                                <td style="width:7%;' . $style_row . '"  class="borde_left borde_right">' . $valor['cantidad'] . '</td>
                                <td style="width:14%;' . $style_row . '"  class="borde_left borde_right">' . number_format($valor['precio'], 2, ',', '.') . '</td>
                                <td style="width:14%;' . $style_row . '"  class="borde_left borde_right">' . number_format($valor['precio'] * $valor['cantidad'], 2, ',', '.') . '</td>
                              </tr>';
                        $count = $count + 1;
                        $monto_sub_total = $monto_sub_total + ($valor['precio'] * $valor['cantidad']);
                    endforeach;
                    $porcentaje_iva=$this->config->item('valor_iva');
                    $calculo_iva = number_format(($monto_sub_total * $this->config->item('valor_iva')) / 100, 2, ',', '.');
                    $monto_total = number_format($monto_sub_total + $calculo_iva, 2, ',', '.');
                endif;
                $espacio_fijo = 26 - $cant_articulos;
                $count_space = 1;
                if ($espacio_fijo > 0):
                    for ($j = 1; $j <= $espacio_fijo; $j++) :
                        echo '<tr>
                                    <td class="borde_left border_right">&nbsp;</td>
                                    <td class="borde_left">&nbsp;</td>
                                    <td class="borde_left">&nbsp;</td>
                                    <td class="borde_left">&nbsp;</td>
                                    <td class="borde_left">&nbsp;</td>
                                    <td class="borde_left">&nbsp;</td>
                                    <td class="borde_left borde_right">&nbsp;</td>
                                  </tr>';
                        $count_space = $count_space + 1;
                    endfor;
                endif;
                ?>

            </tbody>
            <tfoot>
                <tr>
                    <td style="width:41%;font-weight: bold;text-decoration: underline;font-size:7pt;"   colspan = "2" class="borde_top">

                        <br/>
                        <?php echo $this->lang->line('free_PdfOrd_Valid_Line_1'); ?>
                        <?php echo $this->lang->line('free_PdfOrd_Valid_Line_2'); ?>
                    </td>
                    <td style="width:29%;font-weight: bold;font-size:7pt;"   colspan = "1" class="borde_top center">
                        <br/>
                        <br/>
                        <?php echo $this->lang->line('free_PdfOrd_Firma_Line_1'); ?>
                        <?php echo $this->lang->line('free_PdfOrd_Firma_Line_2'); ?>
                    </td>
                    <td style="width:20%" colspan = "3"  class="borde_top borde_left borde_right right">
                        <?php // echo $this->lang->line('free_PdfOrd_MontExent'); ?>
                    </td>
                    <td style="width:14%" colspan = "1" class="borde_top borde_right right"><?php // echo $monto_exento; ?></td>
                </tr>
                <tr>
                    <td style="text-align:right;width:70%;font-size:6.5pt;margin-right: 50px;"  rowspan="3" colspan = "3"  class="">
                        <br/><br/>
                        &nbsp;<?php echo $this->lang->line('free_PdfOrd_Firma_Line_3'); ?>
                        &nbsp;<?php echo $this->lang->line('free_PdfOrd_Firma_Line_4'); ?><br/>
                        &nbsp;<?php echo $this->lang->line('free_PdfOrd_Firma_Line_5'); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>

                    <td style="width:20%" colspan = "3" class="borde_left borde_right right">
                        <?php echo $this->lang->line('free_PdfOrd_SubTotal'); ?>:
                    </td>
                    <td style="width:14%" colspan = "1" class="borde_right right">
                        <?php echo number_format($monto_sub_total, 2, ',', '.'); ?>
                    </td>
                </tr>
                <tr>
                    <td colspan = "3" class="borde_left borde_right right">
                        <?php //echo $porcentaje_descuento . $this->lang->line('free_PdfOrd_Desc'); ?>:
                    </td>
                    <td colspan = "1" class="borde_right right"><?php //echo $calculo_descuento;             ?></td>
                </tr>
                <tr>
                    <td colspan = "3" class="borde_left borde_right right">
                        <?php //echo $p_recargo . ' ' . $this->lang->line('free_PdfOrd_Recarg'); ?>:
                    </td>
                    <td colspan = "1" class="borde_right right"><?php //echo $recargo;             ?></td>
                </tr>
                <tr>
                    <td colspan="3" >&nbsp;</td>
                    <td colspan = "3" class="borde_top borde_left borde_right right">
                        <?php echo $this->lang->line('free_PdfOrd_Iva') . ' ' . $porcentaje_iva . ' % '; ?>:
                    </td>
                    <td colspan = "1" class="borde_top borde_right right"><?php echo $calculo_iva;             ?></td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align:right;font-weight: bold;font-size:6.5pt;">
                        <?php echo $this->lang->line('free_PdfOrd_Derecho'); ?>&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                    <td colspan = "3" class="borde_bottom borde_top borde_left right">
                        <b><?php echo $this->lang->line('free_PdfOrd_NetTotal'); ?>:</b>
                    </td>
                    <td colspan = "1" class="borde_bottom borde_top borde_right borde_left right"><?php echo $monto_total; ?></td>
                </tr>
            </tfoot>
        </table>
    </body>
</html>