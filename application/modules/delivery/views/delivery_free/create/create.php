<div id="alert_content"></div>
<div id="link"></div>
<form id="form_15104" name="form_15104" action="#" method="POST" class="form-horizontal">
    <?php echo $view ?>
</form>

<script>
    $(function(){
        name_add_table=' orden de despacho ';
        $("li:has(a[href=#2]),#2").addClass('hide_element');
        $("li:has(a[href=#3]),#3").addClass('hide_element');

        $("#li-id_tercero-15104").hide();


    });
    //Busqueda de los registros de cotizacion
//    $("select[name='codigo']").ajaxChosen({
//        dataType: 'json',
//        type: 'GET',
//        url:"<?= base_url() ?>delivery/delivery/EXC_searchSelect"
//    },{
//        generateUrl: function(q){ return "<?= base_url() ?>delivery/delivery/EXC_searchSelect" },
//        loadingImg: '<?= base_url() ?>assets/img/loading.gif'
//    });

    //Cambio de Tecnica
    $("select[name='id_tercero']").change(function(e){
        id_client = $(this).val();
        if (id_client != "" ){
            tipo_per = $("select[name='cat_letra']").val();
            load_client({
                container:{
                    cliente:'#cliente-15104',
                    ci_rif:'#ci_rif-15104',
                    email:'#email-15104',
                                        resumen_persona_empresa:'#resumen_persona_empresa',
                    resumen_ci_rif:'#resumen_cedula_rif',
                    resumen_email:'#resumen_email'
                },
                url:"<?= base_url() ?>delivery/delivery_free/EXC_getDataClient/",
                data:{
                    id:id_client,
                    tipo_persona:tipo_per
                }})


        }
    });

    function load_client(params){
        $('body').modalmanager('loading');
        //Dispara el evento

        $.ajax({
            type : "POST",
            url: params.url,
            data: params.data,
            dataType:'json',
            success: function(response){
                $('body').modalmanager('loading');
                if(response){
                    $(params.container.cliente).val(response.data.cliente);
                    $(params.container.ci_rif).val(response.data.cedula_rif);
                    $(params.container.email).val(response.data.email);
                    $(params.container.resumen_persona_empresa).text(response.data.cliente);
                    $(params.container.resumen_ci_rif).text(response.data.cedula_rif);
                    $(params.container.resumen_email).text(response.data.email);

                }
            }
        });
    }

    /**
     * Valida el tab de DATOS de la cotizacion para poder continuar con la carga de informacion
     *
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V- 1.0  26/08/13 03:55 PM
     */
    function validate_orden_free_tab(){
        $('body').modalmanager('loading');
        //Dispara el evento
        $.ajax({
            url:BASE_URL + 'delivery/delivery_free/create/process/validate_data',
            data: $('#form_15104').serialize(),

            success: function(response){
                $('body').modalmanager('loading');

                //Verifica si el TAB DATOS se valida exitosamente y muestra los demas TABS
                if(response.validate == 1){
                    $("li:has(a[href=#1]),#1").removeClass("active");
                    $("li:has(a[href=#2]),#2").removeClass('hide_element');
                    $("li:has(a[href=#3]),#3").removeClass('hide_element');
                    $("li:has(a[href=#2]),#2").addClass("active");

                    //Quita el mensaje de Alerta Error
                    $("#alert_content").empty();
                }

                //Se procesa cuando no se pueden validar los datos
                else{
                    //Se mantiene o muestra nuevamente el TAB inicial y oculta los demas TABS
                    $("li:has(a[href=#2]),#2").removeClass("active");
                    $("li:has(a[href=#3]),#3").removeClass("active");
                    $("li:has(a[href=#1]),#1").addClass("active");
                    $("li:has(a[href=#2]),#2").addClass('hide_element');
                    $("li:has(a[href=#3]),#3").addClass('hide_element');


                    var textAlert = null;

                    //Ocurre cuando hay un error en Base de Datos
                    if(response.view == false)
                        textAlert = 'No se pudo validar los datos, si el problema persiste comuniquese con sistemas';

                    //Ocurre cuando hay errores de validacion en el Servidor
                    else
                        textAlert = response.view;

                    //Mensaje de Alerta Error
                    var alertHtml = [
                        '<div class="alert alert-error fade in">',
                        '<button type="button" class="close" data-dismiss="alert">×</button>',
                        textAlert,
                        '</div>'
                    ].join('');
                    //Elemento que muestra el Alert
                    $("#alert_content").html(alertHtml);
                }
            }
        });
    }
    //Cambio de Tipo de Persona N/J. Muestra un boton de creacion de Persona o Empresa en caso de ser N o J
    $("select[name='cat_letra']").change(function(e){

        $("select[name='id_tercero'] option").remove();
        $("select[name='id_tercero']").append('<option value=""></option>');
        $("select[name='id_tercero']").val('').trigger('liszt:updated');

        cat_letra = $("select[name='cat_letra'] option:selected").text();

        //Muestra el seleccionador de la persona o empresa
        if(cat_letra!='')
            $("#li-id_tercero-15104").show();

        if (cat_letra == "Natural"){
               var button_tercero = [
                '<a href="#" role="button" class="btn btn-primary" data-toggle="modal" onclick="request({url:\'<?php echo base_url("persona/persona/create") ?>\'})">',
                'Crear Persona</a>'
            ].join('');
        }

        if (cat_letra == "Jurídico"){
               var button_tercero = [
                '<a href="#" role="button" class="btn btn-primary" data-toggle="modal" onclick="request({url:\'<?php echo base_url("empresa/empresa/create") ?>\'})">',
                'Crear Empresa</a>'
            ].join('');
        }

        var target = $("#button-id_tercero");
        target.html(button_tercero);

    });

    //Busqueda de los registros del elemento tercero segun la letra N-J seleccionada
    $("select[name='id_tercero']").ajaxChosen({
        dataType: 'json',
        type: 'GET',
        url:"<?= base_url() ?>cotizacion/cotizacion/EXC_searchSelect?letra="+$("select[name='cat_letra']").val()
    },{
        generateUrl: function(q){ return "<?= base_url() ?>cotizacion/cotizacion/EXC_searchSelect?letra="+$("select[name='cat_letra']").val(); },
        loadingImg: '<?= base_url() ?>assets/img/loading.gif'
    });

</script>