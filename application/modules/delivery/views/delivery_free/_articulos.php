<?php if ($operation != 'detail'): ?>
    <br/>
    <button type ="button" id ="btn-cargaMat"class="btn btn-primary " role="button" data-toggle="modal" onclick="show_articles(event,'art', 'articuloTableOrden');">
        Agregar Articulos
    </button>

    <br/><br/>
<?php endif; ?>
<div style="margin-top: 10px; margin-left: 20px">
    <table id="articuloTableOrden" class=" <?php echo ($operation == 'detail' ? '' : ' js-table '); ?>  table table-striped table-bordered">
        <thead>
            <tr>

                <th width="10px"></th>
                <th width="10px"></th>
                <th>Modelo</th>
                <th>Descripción</th>
                <th>Lote</th>
                <th>Precio</th>
                <th>Cant.</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (isset($articulos) and !empty($articulos)):
                foreach ($articulos as $value):
                    if ($value):
                        foreach ($value as $key => $articulo):
                            ?>
                            <tr >
                                <?php if ($operation == 'detail'): ?>
                                    <td></td>
                                <?php endif; ?>
                                <td><a data-toggle="modal" href="#" onclick="show_img_article('<?= $articulo['modelo'] ?>',  '<?= $articulo['descripcion'] ?>', '<?= $articulo['imagen'] ?>');"><i class="icon-picture"></i></a></td>
                                <td id="model-art-<?= $articulo['id'] ?>Orden"><?php echo $articulo['modelo'] ?></td>
                                <td id="name-art-<?= $articulo['id'] ?>Orden"><?php echo $articulo['descripcion'] ?></td>
                                <td>
                                    <?php if (isset($lotes) && sizeof($lotes) > 0): ?>
                                        <select name="lote_art[<?php echo $articulo['id']; ?>][<?php echo $key; ?>]"
                                                data-placeholder="Seleccione el Lote"
                                                class="chzn-select"
                                                data-rules="required"
                                                data-display="Lote"
                                                data-content="Indique el Lote"
                                                title="Lote"
                                                data-rules="required"
                                                id="lote-art-<?= $articulo['id'] ?>Orden"
                                                >

                                            <option value=""></option>
                                            <?php
                                            $option_lote = '';
                                            foreach ($lotes as $key => $value):
                                                $option_lote = $value['lote'];
                                                if ($value['id_articulo'] == $articulo['id']):
                                                    ?>
                                                    <option value="<?php echo $value['id']; ?>"  ><?php echo $option_lote; ?></option>
                                                    <?php
                                                endif;
                                            endforeach;
                                            ?>
                                        </select>
                                        <?php
                                    else:
                                        echo $articulo['lote'];
                                    endif;
                                    ?>


                                </td>
                                <td>
                                    <?php echo $articulo['precio']; ?>
                                </td>
                                <?php
                                $readonly = 'readonly="readonly"';
                                $value = "value='{$articulo['cantidad']}'";
                                ?>
                                <td>
                                    <input min="1" type="number" id="cant-art-<?= $articulo['id'] ?>Orden" class="input-mini" name="cant_art[<?= $articulo['id'] ?>]" placeholder="0" key="<?= $articulo['id'] ?>" <?= $readonly ?> <?= $value ?> />
                                </td>

                            </tr>
                            <?php
                        endforeach;
                    endif;
                endforeach;
                ?>
            <?php endif; ?>
        </tbody>
        <tfoot>

        </tfoot>
    </table>
    <?php
    if ($operation == 'detail'):
        ?>
        <div class="form-actions">
            <input type="button" onclick="navigate_tab('prev',2);" value="Anterior" class="btn btn-primary">
            <input type="button" onclick="navigate_tab('next',2);" value="Siguiente" class="btn btn-primary">
        </div>
    <?php endif ?>

</div>
<input type="hidden" name="art_valid" id="art_valid" value="" />

