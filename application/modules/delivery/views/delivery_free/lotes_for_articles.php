<select name="lote_art[<?php echo $id_article; ?>]"
        data-placeholder="Seleccione el Lote"
        class="chzn-select"
        data-rules="required"
        data-display="Lote"
        data-content="Indique el Lote"
        title="Lote"
        data-rules="required"
        >

    <option value=""></option>

    <?php
    if ($lotes):
        $option_lote = '';
        foreach ($lotes as $key => $value):
            $option_lote = $value['lote'];
            ?>
            <option value="<?php echo $value['id']; ?>"  ><?php echo $option_lote; ?></option>
            <?php
        endforeach;

    else:
        ?>
        <option value = ""></option>
    <?php endif; ?>

</select>
