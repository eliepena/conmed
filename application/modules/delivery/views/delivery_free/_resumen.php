
<table style="" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th colspan="3" style="width: 100%;">
    <center>DATOS DEL CLIENTE</center>
</th>
</tr>

</thead>
<tbody>

    <tr>
        <td style="width: 10%;"><b>Persona/Empresa</b></td>
        <td style="width: 75%;" id="resumen_persona_empresa" colspan="2"><?php echo (isset($data[0]['cliente']) ? $data[0]['cliente'] : 'Sin Informacion'); ?></td>
    </tr>
    <tr>
        <td style="width: 25%;"><b>Cedula/Rif</b></td>
        <td style="width: 75%;" id="resumen_cedula_rif" colspan="2"><?php echo (isset($data[0]['ced_rif']) ? $data[0]['ced_rif'] : 'Sin Informacion'); ?></td>
    </tr>
    <tr>
        <td style="width: 25%;"><b>Email</b></td>
        <td style="width: 75%;" id="resumen_email" colspan="2"><?php echo (isset($data[0]['email']) ? $data[0]['email'] : 'Sin Informacion'); ?></td>
    </tr>

    <tr>
        <td colspan="3" style="width: 100%;">
<center><b>
        <?php echo($operation != 'detail' ? 'PREVIEW' : 'IMPRESION DE ORDEN DE DESPACHO'); ?>
    </b>
</center>
</td>
</tr>
<tr>
    <!--<td style="width: 25%;"><b>Materiales</b></td>-->
    <td  style="width: 100%;" colspan="3">
<center>
<!--        <a id="mat_resumen_" style="visibility: hidden;color:green" href="# " >Disponibles: <span></span></a> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;
        <a id="mat_resumen_not" style="visibility: hidden;color:red" href="# " >No Disponibles: <span></span></a>-->

    <div class="<?php if ($operation != 'detail'): ?> form-actions <?php endif; ?>">

        <?php if ($operation != 'detail'): ?>
            <button  type="button" onclick = "print_mode_orden('delivery/delivery_free/create/process/preview/articulos','Vista Previa',true)" class="btn btn-primary btn-mini"  data-toggle="modal" role="button" > Orden Articulos </button>

        <?php else: ?>
            <button  type="button" onclick = "print_mode_orden('<?php echo $this->encrypt->encode($data[0]['id']); ?>','articulos',false)" class="btn btn-primary btn-mini"  data-toggle="modal" role="button" > Orden Articulos </button>
            <button  type="button" onclick = "print_mode_orden('<?php echo $this->encrypt->encode($data[0]['id']); ?>','almacen',false)" class="btn btn-success btn-mini"  data-toggle="modal" role="button" > Orden Almacen </button>
        <?php endif; ?>

        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div id="validate_tramo" class="<?php if ($operation == 'detail'): ?> form-actions <?php endif; ?>">

    </div>
</center>
<!--        <span style="float: right;">

    </span>-->
</td>
</tr>
<!--<tr>
    <td style="width: 25%;"><b>Instrumentos</b></td>
    <td style="width: 75%;" colspan="2">

        <a id="ins_resumen_" style="visibility: hidden;color:green" href="#" >Disponibles: <span></span></a> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;
        <a id="ins_resumen_not" style="visibility: hidden;color:red" href="#" >No Disponibles: <span></span></a>
        <span style="float: right;">
            <button  type="button" onclick = "print_mode_orden('<?php //echo $this->encrypt->encode($data['id']);                     ?>','instrumento',false)" class="btn btn-primary btn-mini"  data-toggle="modal" role="button" > Orden Instrumento </button>
        </span>
    </td>
</tr>
<tr>
    <td style="width: 25%;"><b>Backup</b></td>
    <td style="width: 75%;" colspan="2">
        <a id="backup_resumen_" style="visibility: hidden;color:green" href="# " >Disponibles: <span></span></a> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;
        <a id="backup_resumen_not" style="visibility: hidden;color:red" href="# " >No Disponibles: <span></span></a>
        <span style="float: right;">
            <button  type="button" onclick = "print_mode_orden('<?php //echo $this->encrypt->encode($data['id']);                     ?>','backup',false)" class="btn btn-primary btn-mini"  data-toggle="modal" role="button" > BackUp </button>
        </span>
    </td>
</tr>-->
</tbody>
</table>
<?php
if ($operation != 'detail') {
    ?>
    <div class="form-actions">
        <input type="button" onclick="navigate_tab('prev',5);" value="Anterior" class="btn btn-primary">
        <!--<input type="button" onclick="print_mode(null,'Vista Previa',true)" value="PreVisualizar" class="btn btn-primary">-->
        <input type="button" onclick="save_delivery_free()" value="Guardar" class="btn btn-primary">
    </div>
    <?php
}
?>

<script>
    function save_delivery_free(){
        var formData = $('#form_15104').serializeArray();
        $('body').modalmanager('loading');
        //Dispara el evento
        $.ajax({
            url: BASE_URL +'delivery/delivery_free/create/process/',
            data: formData,

            success: function(response){
                $('body').modalmanager('loading');
                //alert(4);
                var textAlert = null, alertHtml = null;

                //Se procesa cuando el registro se guarda exitosamente
                if(response.view == 1){
                    $('#validate_tramo').empty();
                    //alert(3);
                    $("#form_15104").hide();
                    textAlert = 'Datos guardados satisfactoriamente';
                    alertHtml = [
                        '<div class="alert alert-success fade in">',
                        '<button type="button" class="close" data-dismiss="alert">×</button>',
                        textAlert,
                        '</div>'
                    ].join('');
                    var link ='<a id="link" href="#" class="btn btn-success" onclick="request({url:\''+BASE_URL+'delivery/delivery_free/listAll\'})">Regresar al listado</a>' ;
                    $("#link").html(link);
                    $('#link').on('click', function() {
                        request({
                            url:BASE_URL +'delivery/delivery_free/listAll'
                        });
                    });
                    setTimeout(function(){
                        $('#link').trigger('click');
                    },2000);
                    setTimeout(function(){
                        $('#modal_window').modal('hide');
                    },1500);

                }

                //Se procesa cuando no se puede guardar el registro
                else{
                    $('#validate_tramo').empty();
                    //alert(2);
                    //Ocurre cuando hay un error en Base de Datos
                    if(response.view == false){
                        $('#validate_tramo').empty();
                        textAlert = 'No se pudo guardar el registro, si el problema persiste comuniquese con sistemas';

                        //Ocurre cuando hay errores de validacion en el Servidor
                    }else if(response.view == 2){
                        textAlert = response.msg;
                        $('#validate_tramo').empty();
                        //alert(1);
                        $('#validate_tramo').html(response.data);
                        //Mensaje de Alerta Error
                        alertHtml = [
                            '<div class="alert alert-error fade in">',
                            '<button type="button" class="close" data-dismiss="alert">×</button>',
                            textAlert,
                            '</div>'
                        ].join('');
                    }else{
                        $('#validate_tramo').empty();
                        //alert(0);
                        textAlert = response.view;

                        //Mensaje de Alerta Error
                        alertHtml = [
                            '<div class="alert alert-error fade in">',
                            '<button type="button" class="close" data-dismiss="alert">×</button>',
                            textAlert,
                            '</div>'
                        ].join('');
                    }
                }
                //Elemento que muestra el Alert
                $("#alert_content").html(alertHtml);

            }
        });
    }




    function checkboxs_disponibilidad(id_table,tipo_articulo,disponible) {
        var articulos = new Array();
        var iBase = '#' + id_table;
        var iTBody = iBase + ' tbody ';
        var count=0;
        var add_conten='';

        $(iTBody +' input[type=checkbox]').each(function(){

            var _this = $(this);
            if(disponible==1){
                if(_this.is(':checked')){
                    articulos.push(_this.val());
                    count=count+1;

                }
            }else{
                if(!_this.is(':checked')){
                    articulos.push(_this.val());
                    count=count+1;
                    add_conten='not';

                }
            }

        });

        var sub_array="{'articulos':["+articulos.toString()+"]}";
        $('#'+tipo_articulo+'_resumen_'+add_conten).attr('onclick','articulos_resumen('+sub_array+')');
        $('#'+tipo_articulo+'_resumen_'+add_conten+' span').html(count);
    }
    function articulos_resumen(params){
        alert(params.articulos[0])
    }

    /**
     * Define el modo de impresion del documento.
     *
     * @param  {string} id_cotizacion   Identificador de la Cotizacion
     * @param  {string} tipo_documento  Datos para la busqueda de datos
     * @param  {boolean} mode_preview   Identifica si la impresion en preview o no.
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V- 1.0  05/09/13 02:52 PM
     */
    function print_mode_orden(id_orden,tipo_documento,mode_preview){
        var membrete = false;
        var url=id_orden;
        bootbox.dialog('Impresion del documento '+tipo_documento,[{
                'label': "Sin Membrete!",
                'class': "btn-danger",
                'callback': function() {
                    membrete = false;
                    if(mode_preview){
                        preview_pdf_orden(membrete,url);

                    }else{
                        cotizacionDocumento_orden(id_orden,tipo_documento,membrete);
                    }
                }
            },{

                'label': "Con Membrete",
                'class': "btn-primary",
                'callback': function() {
                    membrete = true;
                    if(mode_preview){
                        preview_pdf_orden(membrete,url);
                    }else{
                        cotizacionDocumento_orden(id_orden,tipo_documento,membrete);
                    }
                }
            }]);
    }

    /**
     * Obtiene el reporte de la orden de despacho (PDF)  de acuerdo al tipo de documento (backup, instrumental, cotizacion)
     *
     * @param  {string} id_cotizacion  Identificador de la Cotizacion
     * @param  {string} tipo_documento  Datos para la busqueda de datos
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V- 1.0  26/08/13 03:55 PM
     */
    function cotizacionDocumento_orden(id_cotizacion,tipo_documento,membrete){

        $('body').modalmanager('loading');
        //Dispara el evento
        $.ajax({
            url: BASE_URL +'delivery/delivery_free/EXC_quoteReport/',
            data: {
                id:id_cotizacion,
                tipo:tipo_documento,
                membrete:membrete
            },
            success: function(response){
                $('body').modalmanager('loading');
                if(response.sucess){
                    pop_pup = window.open(BASE_URL +"delivery/delivery_free/EXC_force_download?url="+response.file+"&name="+tipo_documento,'Pdf'+tipo_documento, 'width=1,height=1,scrollbars=NO');
                    if(!pop_pup){
                        bootbox.alert('Disculpe! Verifique que las ventanas emergentes de su navegador no esten bloqueadas.');
                    }else{
                        setInterval(function() {
                            pop_pup.close();
                        }, 9000);
                    }
                }else{
                    var textAlert = 'No se pudo generar el documento, si el problema persiste comuniquese con sistemas';
                    bootbox.alert(textAlert);
                }
            }
        });
    }

    /**
     * Obtiene el pdf Preview de la cotizacion
     *
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V- 1.0  29/08/13 04:51 PM
     */
    function preview_pdf_orden(membrete,url){
        var formData = $('#form_15104').serializeArray();
        formData.push({
            name:"membrete" ,
            value:membrete
        });
        $('body').modalmanager('loading');
        //Dispara el evento
        $.ajax({
            url: BASE_URL +url,
            data: formData,

            success: function(response){
                $('body').modalmanager('loading');
                //Se procesa cuando se ejecuta exitosamente el controlador
                if(response.view == 1){
                    pop_pup = window.open(BASE_URL +"delivery/delivery_free/EXC_force_download?url="+response.file+"&name=Pdf_Preview",'PdfPreview', 'width=1,height=1,scrollbars=NO');
                    if(!pop_pup){
                        bootbox.alert('Disculpe! Verifique que las ventanas emergentes de su navegador no esten bloqueadas.');
                    }else{
                        setInterval(function() {
                            pop_pup.close();
                        }, 9000);
                    }

                }
                //Se procesa cuando no se ejecuta exitosamente el controlador
                else{
                    //Muestra nuevamente el TAB inicial
                    //                        $("li:has(a[href=#2]),#2").addClass("active");
                    //                        $("li:has(a[href=#5]),#5").removeClass("active");

                    var textAlert = null;

                    //Ocurre cuando hay un error en Base de Datos
                    if(response.view == false)
                        textAlert = 'No se pudo generar la vista previa, si el problema persiste comuniquese con sistemas';

                    //Ocurre cuando hay errores de validacion en el Servidor
                    else
                        textAlert = response.view;

                    //Mensaje de Alerta Error
                    var alertHtml = [
                        '<div class="alert alert-error fade in">',
                        '<button type="button" class="close" data-dismiss="alert">×</button>',
                        textAlert,
                        '</div>'
                    ].join('');

                    //Elemento que muestra el Alert
                    $("#alert_content").html(alertHtml);
                }
            }
        });
    }
    $(".chzn-select").chosen({
        allow_single_deselect:true
    });
</script>
