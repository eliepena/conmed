<?php
if ($operation != 'detail') {
    ?>
    <script type="text/javascript">
        $(function(){

            new FormValidator('form_15104', buildRules('form_15104'),
            function(errors, event){
                var flag = validateForm(errors, event);

                if (event && event.preventDefault) {
                    event.preventDefault();
                } else if (event) {
                    event.returnValue = false;
                }

                //validacion submit
                if(flag){
                    //este archivo es particular,
                    //Valida TAB DATOS luego de ser validada en el cliente.
                    validate_orden_free_tab();
                }
            });
            customFields();
            build_table_articles();
        });
    </script>
    <?php
}
?>
<div class="modal-body">
    <ul>
        <?php
        if ($operation != 'detail') {
            ?>
            <li id="li-label_datos_cotizacion-15104">
                <br>
                <label for="label_datos_cotizacion-15104">
                    <strong>DATOS DEL CLIENTE</strong></label>
                <div id="label-datos_cotizacion-15104"></div>
            </li>

            <li id="li-cat_letra-15104">
                <div class="control-group">
                    <label class="control-label">Tercero (Persona/Empresa)</label>
                    <div class="controls">
                        <select name="cat_letra"
                                data-placeholder="Seleccione el Tipo de Persona"
                                class="chzn-select"
                                data-rules="required|integer|rewrite"
                                data-display="Tercero (Persona/Empresa)"
                                data-content="Indique el tipo de persona"
                                title="Tercero (Persona/Empresa)"
                                >
                            <option value=""></option>
                            <?php if (isset($tipo_persona) && sizeof($tipo_persona) > 0): ?>
                                <?php foreach ($tipo_persona as $key => $value): ?>
                                    <option value="<?php echo $value['id']; ?>" ><?php echo $value['_label']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                        <span id="button-cat_letra"></span>
                    </div>
                </div>
            </li>
            <li id="li-id_tercero-15104">
                <div class="control-group">
                    <label class="control-label" for="628">Persona/Empresa</label>
                    <div class="controls">
                        <select name="id_tercero"
                                data-placeholder="Seleccione el Tercero"
                                class="chzn-select"
                                data-rules="integer|rewrite"
                                data-display="Persona/Empresa"
                                data-content="Indique el tercero (Persona o Empresa) que emitirá el pago"
                                title="Persona/Empresa"
                                >
                            <option value=""></option>
                        </select>
                        <span id="button-id_tercero">
                            <a href="#" role="button" class="btn btn-primary" data-toggle="modal"
                               onclick="request({url:BASE_URL +'persona/persona/create', data:{id:'',target_on:'window2'}})">
                                Crear Persona
                            </a>
                        </span>
                    </div>
                </div>
            </li>
            <li id="li-cliente-15104">
                <div class="control-group">
                    <label class="control-label"  for="cliente-15104">Cliente</label>
                    <div class="controls">
                        <input type="text"
                               class="input-xlarge"
                               name="cliente-15104"
                               id="cliente-15104"
                               placeholder=""
                               data-rules=""
                               data-display="Cliente"
                               value=""
                               data-content=""
                               title="Cliente"
                               readonly="readonly"
                               />
                    </div>
                </div>
            </li>
            <li id="li-medico-15104">
                <div class="control-group">
                    <label class="control-label"  for="ci_rif-15104">Cedula/Rif</label>
                    <div class="controls">
                        <input type="text"
                               class="input-xlarge"
                               name="ci_rif-15104"
                               id="ci_rif-15104"
                               placeholder=""
                               data-rules=""
                               data-display="Cedula/Rif"
                               data-content=""
                               title="Cedula/Rif"
                               readonly="readonly"
                               />
                    </div>
                </div>
            </li>
            <li id="li-vendedor-15104">
                <div class="control-group">
                    <label class="control-label"  for="email-15104">Email</label>
                    <div class="controls">
                        <input type="text"
                               class="input-xlarge"
                               name="email-15104"
                               id="email-15104"
                               placeholder=""
                               data-rules=""
                               data-display="email"
                               data-content=""
                               title="email"
                               readonly="readonly"
                               />
                    </div>
                </div>
                <?php
            }
            ?>
        <li id="li-label_datos_orden-15104">
            <br>
            <label for="label_datos_orden-15104">
                <strong>DATOS DE LA ORDEN</strong></label>
            <div id="label-datos_orden-15104"></div>
        </li>
        <?php
        if ($operation != 'detail') {
            ?>
            <li id="li-fecha_despacho-15104">
                <div class="control-group">
                    <label class="control-label" for="fecha_despacho-15104">Fecha de Despacho</label>
                    <div class="controls">
                        <div
                            class="input-append date"
                            data-date=""
                            data-date-format="dd-mm-yyyy">
                            <input
                                class="date input-small"
                                type="text"
                                id="fecha_despacho-15104"
                                name="fecha_despacho"
                                data-rules="required|valid_date"
                                data-display="Fecha de Despacho"
                                data-content="Indique la Fecha de Despacho"
                                title="Fecha de Despacho"
                                placeholder="Ej: dd-mm-aaaa"
                                readonly="readonly" >

                            <span class="add-on"><i class="icon-calendar"></i></span>
                        </div>
                    </div>
                </div>
            </li>
            <?php
        }
        if ($operation == 'detail') {
            ?>
            <li>
                <div class="control-group">
                    <label class="control-label" >Nro Orden</label>
                    <div class="controls">

                        <input
                            class="input-xlarge"
                            type="text"
                            data-display="Orden de despacho"
                            data-content="Numero de Orden de Despacho"
                            title="Orden de despacho"
                            value="<?php echo (isset($data[0]['codigo']) ? $data[0]['codigo'] : ''); ?>"
                            readonly="readonly" />



                    </div>
                </div>
            </li>
            <li>
                <div class="control-group">
                    <label class="control-label" >Fecha de solicitud</label>
                    <div class="controls">
                        <input
                            class="input-xlarge"
                            type="text"
                            data-display="Fecha de solicitud"
                            data-content="Fecha de solicitud de la Orden de Despacho"
                            title="Fecha de solicitud"
                            value="<?php echo (isset($data[0]['fecha_inicio']) ? $data[0]['fecha_inicio'] : ''); ?>"
                            readonly="readonly"  />
                    </div>
                </div>
            </li>
            <li>
                <div class="control-group">
                    <label class="control-label" >Fecha de Entrega</label>
                    <div class="controls">

                        <input
                            class="input-xlarge"
                            type="text"
                            data-display="Fecha de Entrega"
                            data-content="Fecha de Entrega de la Orden de Despacho"
                            title="Fecha de Entrega"
                            value="<?php echo (isset($data[0]['fecha_entrega']) ? $data[0]['fecha_entrega'] : ''); ?>"
                            readonly="readonly" />
                    </div>
                </div>
            </li>
            <li>
                <div class="control-group">
                    <label class="control-label" >Estatus de la Orden</label>
                    <div class="controls">

                        <input
                            class="input-xlarge"
                            type="text"
                            data-display="Estatus de la Orden"
                            data-content="Estatus de la Orden de Despacho"
                            title="Estatus de la Orden"
                            value="<?php echo (isset($data[0]['cat_estatus_orden_despacho']) ? $data[0]['cat_estatus_orden_despacho'] : ''); ?>"
                            readonly="readonly" />
                    </div>
                </div>
            </li>
            <li>
                <div class="control-group">
                    <label class="control-label" >Despachada Por</label>
                    <div class="controls">

                        <input
                            class="input-xlarge"
                            type="text"
                            data-display="Despachada Por"
                            data-content="Empleado que despacho la Orden de Entrega"
                            title="Despachada Por"
                            value="<?php echo (isset($data[0]['id_despachador']) ? $data[0]['id_despachador'] : ''); ?>"
                            readonly="readonly"  />
                    </div>
                </div>
            </li>
            <?php
        }
        ?>
        <li id="li-id_cirugia-15104">
            <input type="hidden"
                   class="input-xlarge"
                   name="id_cirugia"
                   id="id_cirugia-15104"
                   placeholder=""
                   data-rules=""
                   data-display="Identificador de la Cirugia"
                   value=""
                   data-content=""
                   title="Identificador de la Cirugia"
                   />
        </li>
    </ul>
</div>
<?php
if ($operation != 'detail') {
    ?>
    <div class="modal-footer">
        <!--<button class="btn" data-dismiss="modal">Cancelar</button>-->
        <input type="submit" class="btn btn-primary update" value="Siguiente" />
    </div>

    <?php
}
?>
