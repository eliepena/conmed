<select name="precios_art[<?php echo $id_article; ?>]"
        data-placeholder="Seleccione el Precio"
        class="chzn-select"
        data-display="Precios"
        data-content="Indique el Precio"
        title="Precios"
        >
    <option value = ""></option>    <option value="0.00" selected>NO APLICA</option>

    <?php
    if ($lotes):
        $option_precio = '';
        foreach ($precios as $key => $value):
            $option_precio = $value['precio'];
            ?>
            <option value="<?php echo $value['precio']; ?>"  ><?php echo $option_precio; ?></option>
            <?php
        endforeach;

    else:
        ?>

        <option value="0.00" selected>NO APLICA</option>
    <?php endif; ?>

</select>
