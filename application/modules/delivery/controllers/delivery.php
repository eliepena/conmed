<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Descripcion de  Despacho
 *
 * @package conmed.application
 * @subpackage modules
 * @category controller
 * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V - 1.0 09/10/13 11:51 AM
 */
class Delivery extends AST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('cotizacion/cotizacion_model', 'Cotizacion_model');
        $this->load->model('cotizacion/cotizacion_free_model', 'Cotizacion_free_model');
        $this->load->model('delivery/delivery_model', 'delivery_model');
        $this->load->model('persona/empleado_model', 'empleado_model');
        $this->load->model('persona/paciente_model', 'paciente_model');
        $this->load->model('inventario/articulo_model', 'articulo_model');
        $this->load->model('inventario/almacen_model', 'almacen_model');
        $this->load->model('settings/categories_model', 'categories_model');
        $this->load->model('persona/persona_model', 'persona_model');
        $this->load->model('empresa/empresa_model', 'empresa_model');
    }

    /**
     * Provee el acceso al formulario de ingreso de nuevos datos, asi mismo,
     * provee el acceso a la insercion de datos nuevos dentro en base de datos.
     *
     * @access public
     * @param array $params Parametros adicionales enviados al controlador.
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.2 18/10/13 09:59 AM
     */
    function create($params = NULL) {
        $modo = $params;
        $this->session->unset_userdata("monto_material");
        if (!$this->input->post()) {
//            //Tipo cotizacion normal
//            $tipo_cotizacion = $this->categories_model->getCategory(array('_table' => 'tipo_cotizacion', '_label' => 'Normal'), ' id ');
//            $tipo_cotizacion = $tipo_cotizacion[0]['id'];
//
//            //Tipo cotizacion normal
//            $tipo_cotizacion_libre = $this->categories_model->getCategory(array('_table' => 'tipo_cotizacion', '_label' => 'Libre'), ' id ');
//            $tipo_cotizacion_libre = $tipo_cotizacion_libre[0]['id'];
            //Estatus para la cotizacion normal
            $cotizacion_aprobada = $this->categories_model->getCategory(array('_table' => 'cotizacion_estatus', '_label' => 'Aprobada'), ' id ');
            $cotizacion_aprobada = $cotizacion_aprobada[0]['id'];
            $estatus_cotizacion = array($cotizacion_aprobada);

            //Estatus para la cotizacion libre
            $cotizacion_emitida = $this->categories_model->getCategory(array('_table' => 'cotizacion_estatus', '_label' => 'Emitida'), ' id ');
            $cotizacion_emitida = $cotizacion_emitida[0]['id'];
            $estatus_cotizacion_libre = array($cotizacion_emitida);

            //Unificacion de arreglos de datos de cotizacion libre y normal
            $cotizacion_normal_array = $this->delivery_model->getComboByTable('id_cotizacion', 'business.cotizacion.codigo', FALSE, $label = 'text', $estatus_cotizacion, 'Normal');
            $cotizacion_libre_array = $this->delivery_model->getComboByTable('id_libre_cotizacion', 'business.libre_cotizacion.codigo', FALSE, $label = 'text', $estatus_cotizacion_libre, 'Libre');
            $merge_data_cot = array_merge($cotizacion_libre_array, $cotizacion_normal_array);
            //Arreglo de datos para la pestana datos
            $tab_datos = array(
                'operation' => 'detail',
                'data_cotizacion' => $merge_data_cot
            );

            $tabs = array(
                '1' => array('view' => 'particular', 'label' => 'Datos', 'params' => array('file' => 'delivery/delivery/_datos', 'data' => $tab_datos)),
                '2' => array('view' => 'particular', 'label' => 'Materiales', 'params' => array('file' => 'delivery/delivery/_materiales', 'data' => array('operation' => 'detail'))),
                '3' => array('view' => 'particular', 'label' => 'Instrumental', 'params' => array('file' => 'delivery/delivery/_instrumentos', 'data' => array('operation' => 'detail'))),
                '4' => array('view' => 'particular', 'label' => 'Backup', 'params' => array('file' => 'delivery/delivery/_backup_material', 'data' => array('operation' => 'detail'))),
                '5' => array('view' => 'particular', 'label' => 'Resumen Nota de Entrega', 'params' => array('file' => 'delivery/delivery/_resumen', 'data' => array('operation' => 'detail')))
            );
            $view['view'] = $this->view_engine->tab($tabs);

            $this->setContent($this->load->view('delivery/delivery/create/create', $view, TRUE));
        } elseif ($params[0] == 'process') {
            if (isset($modo[1]) && $modo[1] == 'validate_data'):
                $this->_avancedFormValidation('insert', true);
            elseif (isset($modo[1]) && $modo[1] == 'preview'):
                $this->_avancedFormValidation($modo[2], false);
            else:
                $this->_avancedFormValidation('insert');
            endif;
            $process = $this->view_engine->process();
            $success = FALSE;
            if ($process['result']) {

                if (isset($modo[1]) && $modo[1] == 'validate_data'):
                    $this->offOutput();
                    echo json_encode(array('validate' => true));
                elseif (isset($modo[1]) && $modo[1] == 'preview'):
                    $this->offOutput();
                    $this->EXC_quoteReport(TRUE);
                else:

                    $params = array('format_type' => 'insert', 'data' => $process['data']);
                    $data = $this->_format($params);
                    $validate_tramos = $this->prioritySectionsArticles($data);
                    if (!$validate_tramos['success']):
                        $this->offOutput();
                        $data = $this->load->view('delivery/delivery/_validate_tramo', array('data' => $validate_tramos), TRUE);
                        $response = array('view' => 2, 'msg' => 'Verifique los lotes para cada articulo', 'data' => $data);
                        echo json_encode($response);
                    else:


                        $success = $this->delivery_model->insert($data);
                        $this->setContent($success);
                    endif;
                endif;
            } else {

                $this->setContent($process['validation_error']);
            }
            if (!isset($modo[1])):
                // -----------------------------
                //seteamos los datos de auditoria
                $this->audit->setOperationData($process);
                $this->audit->setChkResult($success);
            // -----------------------------
            endif;
        }
    }

    /**
     * Provee el acceso a la busqueda de los combos tipo select y sus valores en BD
     *
     * @access public
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.2 18/10/13 09:59 AM
     */
    function EXC_searchSelect() {
        $data = $this->input->get('data');
        $arr_values = $this->delivery_model->getComboByTable('id_cotizacion', 'business.cotizacion.codigo', $data['q'], $label = 'text');
        $this->offOutput();
        echo json_encode(array('q' => $data['q'], 'results' => $arr_values));
    }

    /**
     * Provee el los datos para las tab del create con respecto a una cotizacion
     *
     * @access public
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.2 18/10/13 10:57 AM
     */
    function EXC_getDataCotizacion() {

        $array_post = $this->input->post();

        $response = array('success' => FALSE, 'data' => array());
        $this->offOutput();
        if (!$array_post['id'])
            echo json_encode($response);
        $data['cotizacion'] = $this->Cotizacion_model->getById($array_post['id']);
        $data['materiales'] = $this->separateItemForQuantity($this->Cotizacion_model->getArticulosByCot($array_post['id'], 'MAT'));
        $data['backup'] = $this->separateItemForQuantity($this->Cotizacion_model->getArticulosByCot($array_post['id'], 'MAT', '1'));
        $data['instrumentos'] = $this->separateItemForQuantity($this->Cotizacion_model->getArticulosByCot($array_post['id'], 'INS'));
        $data['cotizacion']['tipo_cotizacion'] = $array_post['tipo_cotizacion'];


        $tipo_cotizacion = $this->categories_model->getCategory(array('_table' => 'tipo_cotizacion', '_label' => 'Normal'), ' id ');
        $tipo_cotizacion = $tipo_cotizacion[0]['id'];
        $data['cotizacion']['tipo_cot'] = $tipo_cotizacion;


        $lote = $this->articulo_model->getTramoArticulo(null, true);
        if ($data):
            $response = array(
                'success' => TRUE,
                'data' => $data['cotizacion'],
                'instrumentos' => $this->load->view('delivery/delivery/_instrumentos', array('operation' => 'detail', 'instrumentos' => $data['instrumentos'], 'lotes' => $lote), true),
                'materiales' => $this->load->view('delivery/delivery/_materiales', array('operation' => 'detail', 'materiales' => $data['materiales'], 'lotes' => $lote), true),
                'backup' => $this->load->view('delivery/delivery/_backup_material', array('operation' => 'detail', 'backup' => $data['backup'], 'lotes' => $lote), true),
                'resumen' => $this->load->view('delivery/delivery/_resumen', array('operation' => 'detail', 'data' => $data['cotizacion']), true)
            );
        endif;
        echo json_encode($response);
    }

    /**
     * Validaciones extras realizadas a la entidad, que no las soporta el View_Engine
     *
     * @access public
     * @param array $data Datos para aplicar el formateo.
     * @return Mixed , string  Validaciones o FALSE en caso de que no tenga errores
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.2 18/10/13 09:59 AM
     */
    private function _avancedFormValidation($format_type, $validate_data_tab = false) {
        $this->load->library('form_validation');
        $format_type = strtoupper($format_type);

        //Insercion
        if ($format_type == 'INSERT') {
            $post = $this->input->post();
            //Valida los elementos que se utilizan en esta parte de la validacion
            if (!isset($post['codigo']) OR !isset($post['id_cirugia']))
                return FALSE;
            if ($validate_data_tab) {
                //valida la existencia del codigo de la cotizacion
                if ($post['codigo'] == '')
                    $this->form_validation->set_rules('codigo', 'Cotizacion', 'required');
                $this->form_validation->set_message('required', 'Debe seleccionar una Cotizacion');
                //Se verifica que exista la fecha de despacho
                if (!isset($post['fecha_despacho'])) {
                    if ($post['fecha_despacho'] == ''):
                        $this->form_validation->set_rules('fecha_despacho', 'Fecha de Despacho', 'required|valid_date');
                    endif;
                }
            }
            else {

                if (isset($post['lote_mat'])):


                    $lot_mat = $post['lote_mat'];
                    $count_emtpy_mat = 0;
                    foreach ($lot_mat as $key => $value):
                        foreach ($value as $clave => $valor):
                            if (empty($valor)):
                                $count_emtpy_mat = $count_emtpy_mat + 1;
                            endif;

                        endforeach;
                    endforeach;

                    if ($count_emtpy_mat > 0):
                        $this->form_validation->set_rules('mat_valid', 'Lote de Material', 'required');
                        $this->form_validation->set_message('required', 'Debe Asignar un Lote a cada Material');
                        return FALSE;

                    endif;
                endif;
                if (isset($post['lote_ins'])):
                    $lot_ins = $post['lote_ins'];
                    $count_emtpy_ins = 0;
                    foreach ($lot_ins as $key => $value):
                        foreach ($value as $clave => $valor):
                            if (empty($valor)):
                                $count_emtpy_ins = $count_emtpy_ins + 1;
                            endif;

                        endforeach;
                    endforeach;

                    if ($count_emtpy_ins > 0):
                        $this->form_validation->set_rules('ins_valid', 'Lote de Instrumento', 'required');
                        $this->form_validation->set_message('required', 'Debe Asignar un Lote a cada Instrumento');
                        return FALSE;
                    endif;
                endif;
                if (isset($post['lote_backup'])):
                    $lot_backup = $post['lote_backup'];
                    $count_emtpy_backup = 0;
                    foreach ($lot_backup as $key => $value):
                        foreach ($value as $clave => $valor):
                            if (empty($valor)):
                                $count_emtpy_backup = $count_emtpy_backup + 1;
                            endif;
                        endforeach;
                    endforeach;

                    if ($count_emtpy_backup > 0):

                        $this->form_validation->set_rules('backup_valid', 'Lote de Backup', 'required');
                        $this->form_validation->set_message('required', 'Debe Asignar un Lote a cada Backup');
                        return FALSE;

                    endif;
                endif;
                //FIN FORMAT INSERT
            }
        }

        if ($format_type == 'MATERIALES') {
            $post = $this->input->post();

            //Valida los elementos que se utilizan en esta parte de la validacion
            if (!isset($post['codigo']) OR !isset($post['id_cirugia']))
                return FALSE;
            if (isset($post['lote_mat'])):
                $lot_mat = $post['lote_mat'];
                $count_emtpy_mat = 0;
                foreach ($lot_mat as $key => $value):
                    foreach ($value as $clave => $valor):
                        if (empty($valor)):
                            $count_emtpy_mat = $count_emtpy_mat + 1;
                        endif;

                    endforeach;
                endforeach;

                if ($count_emtpy_mat > 0):
                    $this->form_validation->set_rules('mat_valid', 'Lote de Material', 'required');
                    $this->form_validation->set_message('required', 'Debe Asignar un Lote a cada Material');
                    return FALSE;

                endif;
            endif;

            //FIN FORMAT MATERIALES
        }

        if ($format_type == 'MATERIALES_FREE') {
            $post = $this->input->post();

            //Valida los elementos que se utilizan en esta parte de la validacion
            if (!isset($post['codigo']) OR !isset($post['id_cirugia']))
                return FALSE;
            if (isset($post['lote_mat'])):
                $lot_mat = $post['lote_mat'];
                $count_emtpy_mat = 0;
                foreach ($lot_mat as $key => $value):
                    foreach ($value as $clave => $valor):
                        if (empty($valor)):
                            $count_emtpy_mat = $count_emtpy_mat + 1;
                        endif;

                    endforeach;
                endforeach;

                if ($count_emtpy_mat > 0):
                    $this->form_validation->set_rules('mat_valid', 'Lote de Material', 'required');
                    $this->form_validation->set_message('required', 'Debe Asignar un Lote a cada Articulo');
                    return FALSE;

                endif;
            endif;

            //FIN FORMAT MATERIALES LIBRE
        }
        if ($format_type == 'INSTRUMENTO') {
            $post = $this->input->post();

            //Valida los elementos que se utilizan en esta parte de la validacion
            if (!isset($post['codigo']) OR !isset($post['id_cirugia']))
                return FALSE;


            if (isset($post['lote_ins'])):
                $lot_ins = $post['lote_ins'];
                $count_emtpy_ins = 0;
                foreach ($lot_ins as $key => $value):
                    foreach ($value as $clave => $valor):
                        if (empty($valor)):
                            $count_emtpy_ins = $count_emtpy_ins + 1;
                        endif;

                    endforeach;
                endforeach;

                if ($count_emtpy_ins > 0):
                    $this->form_validation->set_rules('ins_valid', 'Lote de Instrumento', 'required');
                    $this->form_validation->set_message('required', 'Debe Asignar un Lote a cada Instrumento');
                    return FALSE;
                endif;

            endif;
            //FIN FORMAT INSTRUMENTO
        }

        if ($format_type == 'BACKUP') {
            $post = $this->input->post();

            //Valida los elementos que se utilizan en esta parte de la validacion
            if (!isset($post['codigo']) OR !isset($post['id_cirugia']))
                return FALSE;
            if (isset($post['lote_backup'])):
                $lot_backup = $post['lote_backup'];
                $count_emtpy_backup = 0;
                foreach ($lot_backup as $key => $value):
                    foreach ($value as $clave => $valor):
                        if (empty($valor)):
                            $count_emtpy_backup = $count_emtpy_backup + 1;
                        endif;
                    endforeach;
                endforeach;

                if ($count_emtpy_backup > 0):
                    $this->form_validation->set_rules('backup_valid', 'Lote de Backup', 'required');
                    $this->form_validation->set_message('required', 'Debe Asignar un Lote a cada Backup');
                    return FALSE;
                endif;
            endif;
            //FIN FORMAT BACKUP
        }
    }

    /**
     * Formateo avanzado de los datos a insertar en la orden de despacho
     *
     * @access public
     * @param array $format_type Tipo de Formateo [INSERT o UPDATE].
     * @param array $data Datos para aplicar el formateo.
     * @return array Arreglo con los datos formateados segun la regla de negocio.
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.2 18/10/13 09:59 AM
     */
    function _avancedFormat($format_type, $data) {

        $format_type = strtoupper($format_type);
        $role = $this->session->userdata('role_name');

        if ($format_type == 'INSERT') {
            $tipo_articulo_mat = $this->categories_model->getCategory(array('_table' => 'tipo_articulo', '_label' => 'MAT'), ' id ');
            $tipo_articulo_ins = $this->categories_model->getCategory(array('_table' => 'tipo_articulo', '_label' => 'INS'), ' id ');

            $tipo_articulo_mat = $tipo_articulo_mat[0]['id'];
            $tipo_articulo_ins = $tipo_articulo_ins[0]['id'];

            $orden = $this->_extendsFormat($data);
            $orden_material = array();
            $orden_material_backup = array();
            $orden_instrumento = array();

            //------------- INICIO FORMAT orden de despacho
            $orden['id_cirugia'] = $data['id_cirugia'];
            $orden['id_cotizacion'] = $data['id_cotizacion'];
            $orden['fecha_despacho'] = $data['fecha_despacho'];
            $orden['tipo_cotizacion'] = $data['tipo_cotizacion'];
            if (isset($data['id_cliente'])):
                $orden['id_cliente'] = $data['id_cliente'];
            elseif (isset($data['paciente'])):
                $orden['id_cliente'] = $data['paciente'];
            endif;

            $orden['fecha_solicitud'] = 'now()';
            //$orden['codigo'] = $this->delivery_model->generateCode();
            $orden['id_despachador'] = $orden['created_by'] = $this->empleado_model->getIdByUser($this->session->userdata('user_id'));


            //------------- INICIO FORMAT orden de despacho articulos
            $orden_mat_articulo = array();
            $orden_ins_articulo = array();
            $orden_backup_articulo = array();
            if (isset($data['lote_mat'])):
                $lot_mat = $data['lote_mat'];
                foreach ($lot_mat as $key => $value):
                    foreach ($value as $clave => $valor):
                        $orden_mat_articulo[$key . $clave]['tipo_articulo'] = $tipo_articulo_mat;
                        $orden_mat_articulo[$key . $clave]['id_articulo'] = $key;
                        $orden_mat_articulo[$key . $clave]['id_lote'] = $valor;
                        $orden_mat_articulo[$key . $clave]['cantidad'] = 1;
                        $orden_mat_articulo[$key . $clave]['created_by'] = $this->empleado_model->getIdByUser($this->session->userdata('user_id'));

                    endforeach;
                endforeach;
            endif;
            if (isset($data['lote_ins'])):
                $lot_ins = $data['lote_ins'];
                foreach ($lot_ins as $key => $value):
                    foreach ($value as $clave => $valor):
                        $orden_ins_articulo[$key . $clave]['tipo_articulo'] = $tipo_articulo_ins;
                        $orden_ins_articulo[$key . $clave]['id_articulo'] = $key;
                        $orden_ins_articulo[$key . $clave]['id_lote'] = $valor;
                        $orden_ins_articulo[$key . $clave]['cantidad'] = 1;
                        $orden_ins_articulo[$key . $clave]['created_by'] = $this->empleado_model->getIdByUser($this->session->userdata('user_id'));

                    endforeach;
                endforeach;
            endif;
            if (isset($data['lote_backup'])):
                $lot_backup = $data['lote_backup'];
                foreach ($lot_backup as $key => $value):
                    foreach ($value as $clave => $valor):
                        $orden_backup_articulo[$key . $clave]['tipo_articulo'] = $tipo_articulo_mat;
                        $orden_backup_articulo[$key . $clave]['id_articulo'] = $key;
                        $orden_backup_articulo[$key . $clave]['id_lote'] = $valor;
                        $orden_backup_articulo[$key . $clave]['cantidad'] = 1;
                        $orden_backup_articulo[$key . $clave]['created_by'] = $this->empleado_model->getIdByUser($this->session->userdata('user_id'));
                    endforeach;
                endforeach;
            endif;
            //-------------FIN FORMAT  orden de despacho
            //--------------Retorno de datos
            return array(
                'orden' => $orden,
                'orden_mat_articulo' => $orden_mat_articulo,
                'orden_ins_articulo' => $orden_ins_articulo,
                'orden_backup_articulo' => $orden_backup_articulo
            );
        }
    }

    /**
     * Formateo extendido de datos, se usa para aplicar el mismo formato a la operacion que guarda
     * en varias entidades
     *
     * @access public
     * @param array $data Datos para aplicar el formateo.
     * @return array Arreglo con los datos formateados segun la regla de negocio.
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.2 18/10/13 09:59 AM
     */
    private function _extendsFormat() {
        $data_ext = array();
        $data_ext['created_by'] = $this->empleado_model->getIdByUser($this->session->userdata('user_id'));
        return $data_ext;
    }

    /**
     * Genera o ubica documentos de la cotizacion de acuerdo al tipo de documento.
     *
     * @access public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 09/08/13 09:46 AM
     */
    public function EXC_quoteReport($preview = FALSE) {
        $this->offOutput();
        $id = $this->input->post('id');
        $array_post = $this->input->post();
        $type_report = $this->input->post('tipo');
        $type_quote = $this->input->post('tipo_cotizacion');
        $articles_of_quote = array();

        $membrete = $this->input->post('membrete');
        $dir_modo_impresion = ($membrete == 'true' ? 'membrete_on' : 'membrete_off');
        $alvuelo = true;
        if ($preview):

            $id = $this->encrypt->decode($id);
            $id_orden_despacho = $this->delivery_model->getIdOrdenDespacho($id);
            $data_articles = $this->Cotizacion_model->get_articulos_quote($id);
            if ($data_articles):
                foreach ($data_articles as $key => $value):
                    $articles_of_quote[$value['id_articulo']] = $value;
                endforeach;
            endif;
            if ($this->engine_model->getValueByCategory($type_quote) == 'Libre'):
                $data = $this->Cotizacion_model->get_data_cotizacion_free($id);
            elseif ($this->engine_model->getValueByCategory($type_quote) == 'Normal'):
                $data = $this->Cotizacion_model->get_data_cotizacion($id);
            endif;

            $data[0]['codigo_orden'] = $this->delivery_model->generateCode();
            $data = $data[0];

            switch ($type_report) {
                case 'materiales_free':
                    $getDatosPdf = $this->Cotizacion_model->getDatosPdf_free($data['id_vendedor']);
                    $params_pdf = array(
                        'codigo_orden' => $data['codigo_orden'],
                        'fecha_emision' => date('d/m/Y'),
                        'vendedor' => $getDatosPdf['vendedor'],
                        'cliente' => $array_post['cliente-15102'],
                        'ced_rif' => $array_post['ci_rif-15102'],
                        'nit' => '',
                        'email' => (!empty($array_post['email-15102']) ? $array_post['email-15102'] : 'S/I'),
                        'tlf' => (!empty($array_post['tlf_movil-15102']) ? $array_post['tlf_movil-15102'] : 'S/I')
                    );
                    $monto_subtotal = 0;
                    $precio = 0.00;
                    $lote = array();
                    $articles = array();
                    $articles = $this->articlesForPdf($array_post['lote_mat']);

                    if ($articles):
                        foreach ($articles as $key => $value):
                            foreach ($value as $clave => $valor):
                                $lote = $this->almacen_model->getTramoArticulo($valor['lote']);
                                if ($articles_of_quote[$valor['id']]['precio']):
                                    $precio = $articles_of_quote[$valor['id']]['precio'];
                                endif;
                                $materiales[] = array(
                                    'lote' => $lote['lote'],
                                    'id' => $valor['id'],
                                    'modelo' => $valor['modelo'],
                                    'descripcion' => $valor['descripcion'],
                                    'gart' => '',
                                    'cantidad' => $valor['cantidad'],
                                    'precio' => $precio
                                );
                            endforeach;
                            $monto_subtotal = $monto_subtotal + ($precio * $valor['cantidad']);
                        endforeach;
                        $iva_calculo = ($monto_subtotal * $this->config->item('valor_iva')) / 100;
                        $params_pdf['monto_exento'] = number_format($data['monto_exento'], 2, ',', '.');
                        $params_pdf['monto_gravable'] = number_format($monto_subtotal, 2, ',', '.');
                        $params_pdf['porcentaje_descuento'] = $data['descuento'];
                        $params_pdf['calculo_descuento'] = number_format(($monto_subtotal * $data['descuento']) / 100, 2, ',', '.');
                        $params_pdf['recargo'] = number_format(($monto_subtotal * $data['recargo']) / 100, 2, ',', '.');
                        $params_pdf['p_recargo'] = $data['recargo'];
                        $params_pdf['porcentaje_iva'] = $this->config->item('valor_iva');
                        $params_pdf['calculo_iva'] = number_format($iva_calculo, 2, ',', '.');
                        $params_pdf['neto'] = number_format($monto_subtotal + $iva_calculo, 2, ',', '.');
                        $params_pdf['articulos'] = $materiales;

                        $response = array(
                            'view' => true,
                            'file' => $this->_create_document($params_pdf, false, 'materiales_free_preview', $membrete)
                        );
                    else:
                        $response = array(
                            'view' => false
                        );
                    endif;

                    break;
                case 'instrumento':
                    $getDatosPdf = $this->Cotizacion_model->getDatosPdf($data['id_medico'], $data['id_seguro'], $data['id_paciente'], $data['cat_tipo_pago'], $data['id_vendedor']);
                    $datos_paciente = $this->paciente_model->getPaciente($data['id_paciente'], 'tlf_movil');
                    $cuerpo_instrumentos = $articles = $this->articlesForPdf($array_post['lote_ins']);
                    $lote = array();
                    if ($cuerpo_instrumentos):
                        foreach ($cuerpo_instrumentos as $key => $value):
                            foreach ($value as $clave => $valor):
                                $lote = $this->almacen_model->getTramoArticulo($valor['lote']);

                                $cuerpo_inst[] = array(
                                    'lote' => $lote['lote'],
                                    'id' => $valor['id'],
                                    'modelo' => $valor['modelo'],
                                    'descripcion' => $valor['descripcion'],
                                    'cantidad' => $valor['cantidad']
                                );
                            endforeach;
                        endforeach;
                        $params_pdf = array(
                            'codigo_orden' => $data['codigo_orden'],
                            'nro_cotizacion' => $data['codigo'],
                            'fecha_emision' => date('d/m/Y'),
                            'forma_pago' => $getDatosPdf['tipo_pago'],
                            'tiempo_entrega' => 'INMEDIATA',
                            'vendedor' => $getDatosPdf['vendedor'],
                            'dr_tratante' => $getDatosPdf['medico'],
                            'cliente' => $getDatosPdf['pnombre'],
                            'rif' => $getDatosPdf['pcedula'],
                            'nit' => '',
                            'seguro' => ($getDatosPdf['nseguro'] == null ? 'No Aplica' : $getDatosPdf['nseguro']),
                            'tlf' => (empty($datos_paciente[0]['tlf_movil']) || $datos_paciente[0]['tlf_movil'] == null ? 'Sin Informacion' : $datos_paciente[0]['tlf_movil']),
                        );
                        $params_pdf['cuerpo_tabla'] = $cuerpo_inst;
                        $response = array(
                            'view' => true,
                            'file' => $this->_create_document($params_pdf, false, 'instrumento_preview', $membrete)
                        );

                    else:
                        $response = array(
                            'view' => false
                        );

                    endif;
                    break;
                case 'backup':
                    $getDatosPdf = $this->Cotizacion_model->getDatosPdf($data['id_medico'], $data['id_seguro'], $data['id_paciente'], $data['cat_tipo_pago'], $data['id_vendedor']);
                    $datos_paciente = $this->paciente_model->getPaciente($data['id_paciente'], 'tlf_movil');
                    $articles = $this->articlesForPdf($array_post['lote_backup']);
                    $precio = 0.00;
                    $lote = array();
                    if ($articles):
                        foreach ($articles as $key => $value):
                            foreach ($value as $clave => $valor):
                                $lote = $this->almacen_model->getTramoArticulo($valor['lote']);
                                if ($articles_of_quote[$valor['id']]['precio']):
                                    $precio = $articles_of_quote[$valor['id']]['precio'];
                                endif;
                                $cuerpo_backup[] = array(
                                    'lote' => $lote['lote'],
                                    'id' => $valor['id'],
                                    'modelo' => $valor['modelo'],
                                    'descripcion' => $valor['descripcion'],
                                    'cantidad' => $valor['cantidad'],
                                    'precio' => number_format($precio, 2, ',', '.')
                                );
                            endforeach;
                        endforeach;
                    endif;
                    if ($cuerpo_backup):
                        $params_pdf = array(
                            'codigo_orden' => $data['codigo_orden'],
                            'nro_cotizacion' => $data['codigo'],
                            'fecha_emision' => date('d/m/Y'),
                            'forma_pago' => $getDatosPdf['tipo_pago'],
                            'tiempo_entrega' => 'INMEDIATA',
                            'vendedor' => $getDatosPdf['vendedor'],
                            'dr_tratante' => $getDatosPdf['medico'],
                            'cliente' => $getDatosPdf['pnombre'],
                            'rif' => $getDatosPdf['pcedula'],
                            'nit' => '',
                            'seguro' => ($getDatosPdf['nseguro'] == null ? 'No Aplica' : $getDatosPdf['nseguro']),
                            'tlf' => (empty($datos_paciente[0]['tlf_movil']) || $datos_paciente[0]['tlf_movil'] == null ? 'Sin Informacion' : $datos_paciente[0]['tlf_movil']),
                        );


                        $params_pdf['cuerpo_tabla'] = $cuerpo_backup;
                        $response = array(
                            'view' => true,
                            'file' => $this->_create_document($params_pdf, false, 'backup_preview', $membrete)
                        );
                    else:
                        $response = array(
                            'view' => false
                        );
                    endif;
                    break;
                case 'materiales':
                    $getDatosPdf = $this->Cotizacion_model->getDatosPdf($data['id_medico'], $data['id_seguro'], $data['id_paciente'], $data['cat_tipo_pago'], $data['id_vendedor']);
                    $datos_paciente = $this->paciente_model->getPaciente($data['id_paciente'], 'tlf_movil');
                    $params_pdf = array(
                        'codigo_orden' => $data['codigo_orden'],
                        'nro_cotizacion' => $data['codigo'],
                        'fecha_emision' => date('d/m/Y'),
                        'forma_pago' => $getDatosPdf['tipo_pago'],
                        'tiempo_entrega' => 'INMEDIATA',
                        'vendedor' => $getDatosPdf['vendedor'],
                        'dr_tratante' => $getDatosPdf['medico'],
                        'cliente' => $getDatosPdf['pnombre'],
                        'rif' => $getDatosPdf['pcedula'],
                        'nit' => '',
                        'seguro' => ($getDatosPdf['nseguro'] == null ? 'No Aplica' : $getDatosPdf['nseguro']),
                        'tlf' => (empty($datos_paciente[0]['tlf_movil']) || $datos_paciente[0]['tlf_movil'] == null ? 'Sin Informacion' : $datos_paciente[0]['tlf_movil']),
                    );
                    $getMaterialesPdf = $this->articlesForPdf($array_post['lote_mat']);
                    $materiales = array();
                    $monto_subtotal = 0;
                    $precio = 0.00;
                    $lote = array();
                    if ($getMaterialesPdf):
                        foreach ($getMaterialesPdf as $key => $value):
                            foreach ($value as $clave => $valor):
                                $lote = $this->almacen_model->getTramoArticulo($valor['lote']);
                                if ($articles_of_quote[$valor['id']]['precio']):
                                    $precio = $articles_of_quote[$valor['id']]['precio'];
                                endif;
                                $materiales[] = array(
                                    'lote_m' => $lote['lote'],
                                    'codigo_m' => $valor['id'],
                                    'modelo_m' => $valor['modelo'],
                                    'descripcion_m' => $valor['descripcion'],
                                    'gart_m' => '',
                                    'cant_m' => $valor['cantidad'],
                                    'precio_m' => number_format($precio, 2, ',', '.'),
                                    'neto_m' => number_format($precio * $valor['cantidad'], 2, ',', '.')
                                );
                                $monto_subtotal = $monto_subtotal + ($precio * $valor['cantidad']);
                            endforeach;
                        endforeach;
                        $iva_calculo = ($monto_subtotal * $this->config->item('valor_iva')) / 100;
                        $params_pdf['monto_exento'] = number_format($data['monto_exento'], 2, ',', '.');
                        $params_pdf['monto_gravable'] = number_format($monto_subtotal, 2, ',', '.');
                        $params_pdf['porcentaje_descuento'] = $data['descuento'];
                        $params_pdf['calculo_descuento'] = number_format(($monto_subtotal * $data['descuento']) / 100, 2, ',', '.');
                        $params_pdf['recargo'] = number_format(($monto_subtotal * $data['recargo']) / 100, 2, ',', '.');
                        $params_pdf['p_recargo'] = $data['recargo'];
                        $params_pdf['porcentaje_iva'] = $this->config->item('valor_iva');
                        $params_pdf['calculo_iva'] = number_format($iva_calculo, 2, ',', '.');
                        $params_pdf['neto'] = number_format($monto_subtotal + $iva_calculo, 2, ',', '.');
                        $params_pdf['materiales'] = $materiales;

                        $response = array(
                            'view' => true,
                            'file' => $this->_create_document($params_pdf, false, 'materiales_preview', $membrete)
                        );
                    else:
                        $response = array(
                            'view' => false
                        );
                    endif;
                    break;
                    echo json_encode($response);
            } elseif ($id):
            $id = $this->encrypt->decode($this->session->userdata('id_cot'));

            $data_orden_despacho = $this->delivery_model->getIdOrdenDespacho($id, ' * ', TRUE);
            $id_orden_despacho = $data_orden_despacho[0]['id'];
            $type_quote = $data_orden_despacho[0]['tipo_cotizacion'];

            if ($this->engine_model->getValueByCategory($type_quote) == 'Libre'):
                $data = $this->Cotizacion_model->get_data_cotizacion_free($id);
            elseif ($this->engine_model->getValueByCategory($type_quote) == 'Normal'):
                $data = $this->Cotizacion_model->get_data_cotizacion($id);
            endif;

            $data[0]['codigo_orden'] = $this->delivery_model->generateCode();
            $data = $data[0];

            switch ($type_report) {
                case 'backup':
                    $filename = "uploads/despacho/{$dir_modo_impresion}/backup/backup_$data[codigo].pdf";
                    if (file_exists($filename) && !$alvuelo):
                        $response = array(
                            'sucess' => true,
                            'file' => $filename
                        );
                    else:
                        $getDatosPdf = $this->Cotizacion_model->getDatosPdf($data['id_medico'], $data['id_seguro'], $data['id_paciente'], $data['cat_tipo_pago'], $data['id_vendedor']);
                        $datos_paciente = $this->paciente_model->getPaciente($data['id_paciente'], 'tlf_movil');
                        $cuerpo_backup = $this->delivery_model->getArticulosByPdf($id_orden_despacho, 'MAT', '1');
                        if ($cuerpo_backup):
                            $params_pdf = array(
                                'codigo_orden' => $data['codigo_orden'],
                                'nro_cotizacion' => $data['codigo'],
                                'fecha_emision' => date('d/m/Y'),
                                'forma_pago' => $getDatosPdf['tipo_pago'],
                                'tiempo_entrega' => 'INMEDIATA',
                                'vendedor' => $getDatosPdf['vendedor'],
                                'dr_tratante' => $getDatosPdf['medico'],
                                'cliente' => $getDatosPdf['pnombre'],
                                'rif' => $getDatosPdf['pcedula'],
                                'nit' => '',
                                'seguro' => ($getDatosPdf['nseguro'] == null ? 'No Aplica' : $getDatosPdf['nseguro']),
                                'tlf' => (empty($datos_paciente[0]['tlf_movil']) || $datos_paciente[0]['tlf_movil'] == null ? 'Sin Informacion' : $datos_paciente[0]['tlf_movil']),
                            );
                            $params_pdf['cuerpo_tabla'] = $cuerpo_backup;
                            $response = array(
                                'sucess' => true,
                                'file' => $this->_create_document($params_pdf, false, 'backup', $membrete)
                            );
                        else:
                            $response = array(
                                'sucess' => false
                            );
                        endif;
                    endif;

                    break;
                case 'instrumento':
                    $filename = "uploads/despacho/{$dir_modo_impresion}/instrumentos/instru_$data[codigo].pdf";
                    if (file_exists($filename) && !$alvuelo):
                        $response = array(
                            'sucess' => true,
                            'file' => $filename
                        );
                    else:

                        $getDatosPdf = $this->Cotizacion_model->getDatosPdf($data['id_medico'], $data['id_seguro'], $data['id_paciente'], $data['cat_tipo_pago'], $data['id_vendedor']);
                        $datos_paciente = $this->paciente_model->getPaciente($data['id_paciente'], 'tlf_movil');
                        $cuerpo_instrumentos = $this->delivery_model->getArticulosByPdf($id_orden_despacho, 'INS');
                        if ($cuerpo_instrumentos):
                            $params_pdf = array(
                                'codigo_orden' => $data['codigo_orden'],
                                'nro_cotizacion' => $data['codigo'],
                                'fecha_emision' => date('d/m/Y'),
                                'forma_pago' => $getDatosPdf['tipo_pago'],
                                'tiempo_entrega' => 'INMEDIATA',
                                'vendedor' => $getDatosPdf['vendedor'],
                                'dr_tratante' => $getDatosPdf['medico'],
                                'cliente' => $getDatosPdf['pnombre'],
                                'rif' => $getDatosPdf['pcedula'],
                                'nit' => '',
                                'seguro' => ($getDatosPdf['nseguro'] == null ? 'No Aplica' : $getDatosPdf['nseguro']),
                                'tlf' => (empty($datos_paciente[0]['tlf_movil']) || $datos_paciente[0]['tlf_movil'] == null ? 'Sin Informacion' : $datos_paciente[0]['tlf_movil']),
                            );
                            $params_pdf['cuerpo_tabla'] = $cuerpo_instrumentos;
                            $response = array(
                                'sucess' => true,
                                'file' => $this->_create_document($params_pdf, false, 'instrumento', $membrete)
                            );

                        else:
                            $response = array(
                                'sucess' => false
                            );

                        endif;
                    endif;
                    break;
                case 'materiales':
                    $filename = "uploads/despacho/{$dir_modo_impresion}/cot_$data[codigo].pdf";
                    if (file_exists($filename) && !$alvuelo):
                        $response = array(
                            'sucess' => true,
                            'file' => $filename
                        );
                    else:
                        $getDatosPdf = $this->Cotizacion_model->getDatosPdf($data['id_medico'], $data['id_seguro'], $data['id_paciente'], $data['cat_tipo_pago'], $data['id_vendedor']);
                        $datos_paciente = $this->paciente_model->getPaciente($data['id_paciente'], 'tlf_movil');
                        $params_pdf = array(
                            'codigo_orden' => $data['codigo_orden'],
                            'nro_cotizacion' => $data['codigo'],
                            'fecha_emision' => date('d/m/Y'),
                            'forma_pago' => $getDatosPdf['tipo_pago'],
                            'tiempo_entrega' => 'INMEDIATA',
                            'vendedor' => $getDatosPdf['vendedor'],
                            'dr_tratante' => $getDatosPdf['medico'],
                            'cliente' => $getDatosPdf['pnombre'],
                            'rif' => $getDatosPdf['pcedula'],
                            'nit' => '',
                            'seguro' => ($getDatosPdf['nseguro'] == null ? 'No Aplica' : $getDatosPdf['nseguro']),
                            'tlf' => (empty($datos_paciente[0]['tlf_movil']) || $datos_paciente[0]['tlf_movil'] == null ? 'Sin Informacion' : $datos_paciente[0]['tlf_movil']),
                        );
                        $getMaterialesPdf = $this->delivery_model->getArticulosByPdf($id_orden_despacho, 'MAT');

                        $materiales = array();
                        $monto_subtotal = 0;
                        if ($getMaterialesPdf):
                            foreach ($getMaterialesPdf as $clave => $valor):

                                $materiales[] = array(
                                    'lote_m' => $valor['lote'],
                                    'codigo_m' => $valor['id'],
                                    'modelo_m' => $valor['modelo'],
                                    'descripcion_m' => $valor['descripcion'],
                                    'gart_m' => '',
                                    'cant_m' => $valor['cantidad'],
                                    'precio_m' => $valor['precio'],
                                    'neto_m' => number_format($valor['precio'] * $valor['cantidad'], 2, ',', '.')
                                );
                                $monto_subtotal = $monto_subtotal + ($valor['precio'] * $valor['cantidad']);
                            endforeach;

                            $iva_calculo = ($monto_subtotal * $this->config->item('valor_iva')) / 100;
                            $params_pdf['monto_exento'] = number_format($data['monto_exento'], 2, ',', '.');
                            $params_pdf['monto_gravable'] = number_format($monto_subtotal, 2, ',', '.');
                            $params_pdf['porcentaje_descuento'] = $data['descuento'];
                            $params_pdf['calculo_descuento'] = number_format(($monto_subtotal * $data['descuento']) / 100, 2, ',', '.');
                            $params_pdf['recargo'] = number_format(($monto_subtotal * $data['recargo']) / 100, 2, ',', '.');
                            $params_pdf['p_recargo'] = $data['recargo'];
                            $params_pdf['porcentaje_iva'] = $this->config->item('valor_iva');
                            $params_pdf['calculo_iva'] = number_format($iva_calculo, 2, ',', '.');
                            $params_pdf['neto'] = number_format($monto_subtotal + $iva_calculo, 2, ',', '.');
                            $params_pdf['materiales'] = $materiales;

                            $response = array(
                                'sucess' => true,
                                'file' => $this->_create_document($params_pdf, false, 'materiales', $membrete)
                            );
                        else:
                            $response = array(
                                'sucess' => false
                            );
                        endif;
                    endif;
                    break;
                case 'almacen':
                    $filename = "uploads/despacho/{$dir_modo_impresion}/almacen/almacen_$data[codigo].pdf";
                    if (file_exists($filename) && !$alvuelo):
                        $response = array(
                            'sucess' => true,
                            'file' => $filename
                        );
                    else:

                        $getDatosPdf = $this->Cotizacion_model->getDatosPdf($data['id_medico'], $data['id_seguro'], $data['id_paciente'], $data['cat_tipo_pago'], $data['id_vendedor']);
                        $datos_paciente = $this->paciente_model->getPaciente($data['id_paciente'], 'tlf_movil');
                        $cuerpo_instrumentos = $this->delivery_model->getArticulosByPdf($id_orden_despacho, 'ALMACEN');

                        if ($cuerpo_instrumentos):
                            $params_pdf = array(
                                'codigo_orden' => $data['codigo_orden'],
                                'nro_cotizacion' => $data['codigo'],
                                'fecha_emision' => date('d/m/Y'),
                                'forma_pago' => $getDatosPdf['tipo_pago'],
                                'tiempo_entrega' => 'INMEDIATA',
                                'vendedor' => $getDatosPdf['vendedor'],
                                'dr_tratante' => $getDatosPdf['medico'],
                                'cliente' => $getDatosPdf['pnombre'],
                                'rif' => $getDatosPdf['pcedula'],
                                'nit' => '',
                                'seguro' => ($getDatosPdf['nseguro'] == null ? 'No Aplica' : $getDatosPdf['nseguro']),
                                'tlf' => (empty($datos_paciente[0]['tlf_movil']) || $datos_paciente[0]['tlf_movil'] == null ? 'Sin Informacion' : $datos_paciente[0]['tlf_movil']),
                            );
                            $params_pdf['cuerpo_tabla'] = $cuerpo_instrumentos;
                            $response = array(
                                'sucess' => true,
                                'file' => $this->_create_document($params_pdf, true, 'almacen', $membrete)
                            );

                        else:
                            $response = array(
                                'sucess' => false
                            );

                        endif;
                    endif;
                    break;
                default:
                    $params_pdf['cuerpo_tabla'] = array();
                    break;
                case 'materiales_free':
                    $filename = "uploads/despacho/{$dir_modo_impresion}/cot_$data[codigo].pdf";
                    if (file_exists($filename) && !$alvuelo):
                        $response = array(
                            'sucess' => true,
                            'file' => $filename
                        );
                    else:
                        $data_cliente = $this->empresa_model->getEmpresa($data['id_cliente']);
                        if (!$data_cliente)
                            $data_cliente = $this->persona_model->getPersona($data['id_cliente']);

                        if (isset($data_cliente[0]['cedula'])):
                            $nombre = $data_cliente[0]['primer_nombre'] . " " . $data_cliente[0]['primer_apellido'];
                            $cedula_rif = $data_cliente[0]['cedula'];
                            $email = $data_cliente[0]['correo_electronico'];
                        elseif (isset($data_cliente[0]['rif'])):
                            $nombre = $data_cliente[0]['nombre'];
                            $cedula_rif = $data_cliente[0]['rif'];
                            $email = $data_cliente[0]['email'];
                        endif;
                        $getDatosPdf = $this->Cotizacion_model->getDatosPdf_free($data['id_vendedor']);

                        $params_pdf = array(
                            'codigo_orden' => $data['codigo_orden'],
                            'fecha_emision' => date('d/m/Y'),
                            'vendedor' => $getDatosPdf['vendedor'],
                            'cliente' => $nombre,
                            'ced_rif' => $cedula_rif,
                            'nit' => '',
                            'email' => $email,
                            'tlf' => (!empty($array_post['tlf_movil-15102']) ? $array_post['tlf_movil-15102'] : 'S/I')
                        );

                        $getMaterialesPdf = $this->delivery_model->getArticulosByPdf($id_orden_despacho, 'MAT', 0, 'libre_cotizacion');
                        $materiales = array();
                        $monto_subtotal = 0;
                        if ($getMaterialesPdf):
                            foreach ($getMaterialesPdf as $clave => $valor):
                                $materiales[] = array(
                                    'lote' => $valor['lote'],
                                    'id' => $valor['id'],
                                    'modelo' => $valor['modelo'],
                                    'descripcion' => $valor['descripcion'],
                                    'gart' => '',
                                    'cantidad' => $valor['cantidad'],
                                    'precio' => $valor['precio'],
                                    'neto' => $valor['precio'] * $valor['cantidad']
                                );
                                $monto_subtotal = $monto_subtotal + ($valor['precio'] * $valor['cantidad']);
                            endforeach;
                            $iva_calculo = ($monto_subtotal * $this->config->item('valor_iva')) / 100;
                            $params_pdf['monto_exento'] = number_format($data['monto_exento'], 2, ',', '.');
                            $params_pdf['monto_gravable'] = number_format($monto_subtotal, 2, ',', '.');
                            $params_pdf['porcentaje_descuento'] = $data['descuento'];
                            $params_pdf['calculo_descuento'] = number_format(($monto_subtotal * $data['descuento']) / 100, 2, ',', '.');
                            $params_pdf['recargo'] = number_format(($monto_subtotal * $data['recargo']) / 100, 2, ',', '.');
                            $params_pdf['p_recargo'] = $data['recargo'];
                            $params_pdf['porcentaje_iva'] = $this->config->item('valor_iva');
                            $params_pdf['calculo_iva'] = number_format($iva_calculo, 2, ',', '.');
                            $params_pdf['neto'] = number_format($monto_subtotal + $iva_calculo, 2, ',', '.');
                            $params_pdf['articulos'] = $materiales;

                            $response = array(
                                'sucess' => true,
                                'file' => $this->_create_document($params_pdf, false, 'materiales_free', $membrete)
                            );
                        else:
                            $response = array(
                                'sucess' => false
                            );
                        endif;
                    endif;
                    break;
                case 'almacen_free':
                    $filename = "uploads/despacho/{$dir_modo_impresion}/almacen/almacen_$data[codigo].pdf";
                    if (file_exists($filename) && !$alvuelo):
                        $response = array(
                            'sucess' => true,
                            'file' => $filename
                        );
                    else:

                        $getDatosPdf = $this->Cotizacion_model->getDatosPdf($data['id_medico'], $data['id_seguro'], $data['id_paciente'], $data['cat_tipo_pago'], $data['id_vendedor']);
                        $datos_paciente = $this->paciente_model->getPaciente($data['id_paciente'], 'tlf_movil');
                        $cuerpo_instrumentos = $this->delivery_model->getArticulosByPdf($id_orden_despacho, 'ALMACEN');

                        if ($cuerpo_instrumentos):
                            $params_pdf = array(
                                'codigo_orden' => $data['codigo_orden'],
                                'nro_cotizacion' => $data['codigo'],
                                'fecha_emision' => date('d/m/Y'),
                                'forma_pago' => $getDatosPdf['tipo_pago'],
                                'tiempo_entrega' => 'INMEDIATA',
                                'vendedor' => $getDatosPdf['vendedor'],
                                'dr_tratante' => $getDatosPdf['medico'],
                                'cliente' => $getDatosPdf['pnombre'],
                                'rif' => $getDatosPdf['pcedula'],
                                'nit' => '',
                                'seguro' => ($getDatosPdf['nseguro'] == null ? 'No Aplica' : $getDatosPdf['nseguro']),
                                'tlf' => (empty($datos_paciente[0]['tlf_movil']) || $datos_paciente[0]['tlf_movil'] == null ? 'Sin Informacion' : $datos_paciente[0]['tlf_movil']),
                            );
                            $params_pdf['cuerpo_tabla'] = $cuerpo_instrumentos;
                            $response = array(
                                'sucess' => true,
                                'file' => $this->_create_document($params_pdf, true, 'almacen', $membrete)
                            );

                        else:
                            $response = array(
                                'sucess' => false
                            );

                        endif;
                    endif;
                    break;
                default:
                    $params_pdf['cuerpo_tabla'] = array();
                    break;
            }
        else:
            $response = array(
                'sucess' => false
            );
        endif;
        echo json_encode($response);
    }

    /**
     * Recibe los datos preparados para crear el pdf en su ruta correspondiente
     *
     * @access private
     * @param array $params Arreglo de variables pasados a la vista que armara el html
     * @param array $background_no_validate Determina si fondo(NO VALIDO) estara visible o no
     * @param string $document  Tipo de documento que se va a crear (Se evalua el tipo de documento en el SWITCH CASE correspondiente)
     * @param boolean $membrete Determina si el estara visible o no.
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 16/01/14 10:52 AM
     */
    private function _create_document($params = array(), $background_no_validate = FALSE, $document = '', $membrete = FALSE) {
        $this->lang->load('app_doc');
        $this->lang->load('app_email');
        $this->load->library('encrypt');
        $this->load->helper('view_engine_helper');
        extract($params);
        $dir_modo_impresion = ($membrete == 'true' ? 'membrete_on' : 'membrete_off');
        $dir_modo_impresion = '';
        switch ($document) {
            //IMPRESIONES NORMALES
            case 'materiales':
                $view = 'delivery/delivery/pdf_materiales_orden';
                $name = 'mat_orden';
                $dir = 'despacho/materiales';
                break;
            case 'materiales_free':
                $view = 'delivery/delivery/pdf_materiales_orden_free';
                $name = 'mat_orden_free';
                $dir = 'despacho/materiales';
                break;
            case 'backup':
                $view = 'delivery/delivery/pdf_backup_orden';
                $name = 'backup_orden';
                $dir = 'despacho/backup';
                break;
            case 'instrumento':
                $view = 'delivery/delivery/pdf_instrumentos_orden';
                $name = 'instru_orden';
                $dir = 'despacho/instrumentos';
                break;
            case 'almacen':
                $view = 'delivery/delivery/pdf_almacen_orden';
                $name = 'almacen_orden';
                $dir = 'despacho/almacen';
                break;

            //PREVIEWS
            case 'materiales_preview':
                $view = 'delivery/delivery/pdf_materiales_orden_preview';
                $name = 'mat_orden_preview';
                $dir = 'despacho/previews';
                break;
            case 'materiales_free_preview':
                $view = 'delivery/delivery/pdf_materiales_orden_free_preview';
                $name = 'art_orden_free_preview';
                $dir = 'despacho/previews';
                break;
            case 'instrumento_preview':
                $view = 'delivery/delivery/pdf_instrumentos_orden_preview';
                $name = 'instru_orden_preview';
                $dir = 'despacho/previews';
                break;

            case 'backup_preview':
                $view = 'delivery/delivery/pdf_backup_orden_preview';
                $name = 'backup_orden_preview';
                $dir = 'despacho/previews';
                break;
            default:
                $view = 'pdf_materiales_orden';
                $name = 'ord';
                break;
        }
        //Cadena que contiene el string para generar el pdf
        $html = $this->load->view($view, $params, TRUE);
        // -----------------------------
        // -----------------------------
        //Asignacion de nombre, ruta y generacion del documento
        $name_file = "{$name}_{$codigo_orden}";
        $destination_path = "uploads/{$dir}/";
        generate_pdf($name_file, $destination_path, 'F', $html, $background_no_validate, $membrete);
        return utf8_decode("{$destination_path}" . "{$name_file}.pdf");
        // -----------------------------
    }

    /**
     * Forza la descarga de un archivo pdf
     *
     * @access public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 15/11/13 09:53 AM
     */
    function EXC_force_download() {
        $array_get = $this->input->get();
        $this->offOutput();
        $array_url = explode('_', $array_get['url']);
        $array_url_invertido = array_reverse($array_url);


        $len = filesize($array_get['url']);
        header('Content-type: application/pdf');
        header('Content-Disposition: attachment; filename="' . $array_get['name'] . '_' . $array_url_invertido[0] . '"');
        header('Content-Length: ' . $len);
        readfile($array_get['url']);
    }

    /**
     * Se encarga de mostrar los detalles de una cotizacion
     *
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 25/10/13 10:19 AM
     */
    function detail() {
        $name_material = 'Materiales';
        $id_ec = $this->input->post('id');
        $target = $this->input->post('target_on');
        $this->session->set_userdata('id_cot', $id_ec);
        $id = $this->encrypt->decode($id_ec);
        if (!empty($id)) {
            $id_orden_despacho = $this->delivery_model->getIdOrdenDespacho($id);
            $data['orden'] = $this->delivery_model->getDataDelivery($id);

            if ($this->engine_model->getValueByCategory($data['orden'][0]['tipo_cotizacion']) == 'Libre'):
                $type_quote = 'libre_cotizacion';
                $name_material = 'Articulos';
                $data['cotizacion'] = $this->Cotizacion_free_model->getById($id);
            elseif ($this->engine_model->getValueByCategory($data['orden'][0]['tipo_cotizacion']) == 'Normal'):
                $type_quote = 'cotizacion';
                $data['cotizacion'] = $this->Cotizacion_model->getById($id);

            endif;
            $data['cotizacion']['tipo_cotizacion'] = $data['orden'][0]['tipo_cotizacion'];
            $tipo_articulo_mat = $this->categories_model->getCategory(array('_table' => 'tipo_articulo', '_label' => 'MAT'), ' id ');
            $tipo_articulo_ins = $this->categories_model->getCategory(array('_table' => 'tipo_articulo', '_label' => 'INS'), ' id ');
            $array_materiales[] = $data['materiales'] = $this->delivery_model->getArticulosByOrden($id_orden_despacho, '0', $tipo_articulo_mat[0]['id'], $type_quote);
            $array_backup[] = $data['backup'] = $this->delivery_model->getArticulosByOrden($id_orden_despacho, '1', $tipo_articulo_mat[0]['id'], $type_quote);
            $array_instrumentos[] = $data['instrumentos']=array();// = $this->delivery_model->getArticulosByOrden($id_orden_despacho, '0', $tipo_articulo_ins[0]['id'], $type_quote);

            //Arreglo de datos para la pestana datos
            $tabs = array(
                '1' => array('view' => 'particular', 'label' => 'Datos', 'params' => array('file' => 'delivery/delivery/_datos', 'data' => array('operation' => '', 'data' => $data['orden']))),
                '2' => array('view' => 'particular', 'label' => "{$name_material}", 'params' => array('file' => 'delivery/delivery/_materiales', 'data' => array('operation' => '', 'materiales' => $array_materiales))),
                '3' => array('view' => 'particular', 'label' => 'Instrumental', 'params' => array('file' => 'delivery/delivery/_instrumentos', 'data' => array('operation' => '', 'instrumentos' => $array_instrumentos))),
                '4' => array('view' => 'particular', 'label' => 'Backup', 'params' => array('file' => 'delivery/delivery/_backup_material', 'data' => array('operation' => '', 'backup' => $array_backup))),
                '5' => array('view' => 'particular', 'label' => 'Resumen Nota de Entrega', 'params' => array('file' => 'delivery/delivery/_resumen', 'data' => array('operation' => '', 'data' => $data['cotizacion'])))
            );
            if ($data['orden'][0]['tipo_cotizacion'] == 126):
                unset($tabs[3]);
                unset($tabs[4]);
            endif;
            $view['view'] = $this->view_engine->tab($tabs);
            $this->setContent($this->load->view('delivery/delivery/detail', $view, TRUE));
        }
    }

    /**
     * Separa el arreglo de cotizacion articulo
     * repitiendo las filas del articulo por la
     * cantidad que posea.
     *
     * @access  public
     * @param   array $articles Arreglo de articulos para separar por cantidad
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 11/11/13 11:13 AM
     */
    public function separateItemForQuantity($articles = array()) {
        if (!$articles)
            return FALSE;

        $separateItem = array();
        foreach ($articles as $key => $value):
            for ($i = 1; $i <= $value['cantidad']; $i++):
                $articles[$key]['cantidad'] = 1;
                $separateItem[$value['id']][] = $articles[$key];
            endfor;
        endforeach;
        return $separateItem;
    }

    /**
     * Retorna los articulos sustituidos para listar
     *
     * @access  public
     * @param   array   $data Parametros de entrada
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 08/11/13 10:29 AM
     */
    private function _listReplacements($data = array()) {
        if ($data['materiales']):
            foreach ($data['materiales'] as $key => $value):
                if ($this->Cotizacion_model->checkReplaceArticle($value['id'])):
                    $data['materiales'] [$key]['sustituidos'] = $this->Cotizacion_model->checkReplaceArticle($value['id']);
                else:
                    unset($data['materiales'] [$key]);
                endif;

            endforeach;
        endif;

        if ($data['instrumentos']):
            foreach ($data['instrumentos'] as $key => $value):
                if ($this->Cotizacion_model->checkReplaceArticle($value['id'])):
                    $data['instrumentos'] [$key]['sustituidos'] = $this->Cotizacion_model->checkReplaceArticle($value['id']);
                else:
                    unset($data['instrumentos'] [$key]);
                endif;
            endforeach;
        endif;

        if ($data['backup']):
            foreach ($data['backup'] as $key => $value):
                if ($this->Cotizacion_model->checkReplaceArticle($value['id'])):
                    $data['backup'] [$key]['sustituidos'] = $this->Cotizacion_model->checkReplaceArticle($value['id']);
                else:
                    unset($data['backup'] [$key]);
                endif;
            endforeach;
        endif;

        return $data;
    }

    /**
     *  Provee los tramos asociados a cada articulo asi como
     *  su disponibilidad de acuerdo a la fecha de (vencimiento,creacion) y stock
     *
     * @access  public
     * @param   array $params Arreglo de articulos a evaluar
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 19/11/13 03:43 PM
     */
    function SectionsArticles($params = array()) {
        $id_articulos_con_tramos = array();
        $tramo = array();
        $tramo = $this->delivery_model->availabilityTramoOfItems($params);
        if ($tramo):
            foreach ($params as $key => $value) :
                foreach ($tramo as $clave => $valor):
                    if ($valor['id_articulo'] == $value):
                        $id_articulos_con_tramos[$value][$valor['id']] = $tramo[$clave];
                    endif;
                endforeach;
            endforeach;
        endif;
        return $id_articulos_con_tramos;
    }

    /**
     *  Agrupa los los datos del insert por articulos y cuenta los lotes
     * (Este metodo se utiliza para evaluar cantidad de articulos a insertar en cada lote)
     *
     * @access  private
     * @param   array $id_articulos Arreglo de articulos sin repeticion
     * @param   array $merge_articulos Unificacion de articulos provenientes del form (materiales, instrumental, backup)
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 22/11/13 03:09 PM
     */
    private function countRepeatSectionsForArticles($id_articulos = array(), $merge_articulos = array()) {
        $tramo_articulos = $this->SectionsArticles($id_articulos);
        $count = array();
        foreach ($id_articulos as $key => $value):

            foreach ($tramo_articulos[$value] as $k => $v):
                $count[$value][$v['id']] = 0;
                foreach ($merge_articulos as $a => $b):
                    if ($b['id_articulo'] == $value && $b['id_lote'] == $v['id']):
                        $count[$value][$v['id']] = $count[$value][$v['id']] + 1;
                    endif;
                endforeach;
            endforeach;
        endforeach;
        return $count;
    }

    /**
     *  Evalua la prioridad de seleccion de articulos con
     * respecto al stock y la fecha de creacion del lote
     *
     * @access  public
     * @param   array $params Arreglo de articulos a evaluar
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 22/11/13 02:51 PM
     */
    function prioritySectionsArticles($params = array()) {
        //---------Declaracion e Inicializacion de variables--------------------------------------------------//
        $id_articulos = array();
        $mensajes_error = array();
        $response = array('success' => FALSE);
        $array_materiales = (isset($params['orden_mat_articulo']) ? $params['orden_mat_articulo'] : array());
        $array_instrumental = (isset($params['orden_ins_articulo']) ? $params['orden_ins_articulo'] : array());
        $array_backup = (isset($params['orden_backup_articulo']) ? $params['orden_backup_articulo'] : array());
        $merge_articulos = array_merge($array_materiales, $array_instrumental, $array_backup);
        //----------------------------------------------------------------------------------------------------//
        //----------Extracion y unificacion de id_articulo---------------------------------------//
        if ($merge_articulos):
            foreach ($merge_articulos as $key => $value):
                $id_articulos[$key] = $value['id_articulo'];
            endforeach;
        endif;
        //--------------------------------------------------------------------------------------//

        $tramo_articulos = $this->SectionsArticles($id_articulos);
        $count_tramos_select = $this->countRepeatSectionsForArticles($id_articulos, $merge_articulos);

        if (sizeof($tramo_articulos)):
            foreach ($tramo_articulos as $clave => $valor):
                $first_tramo = current($tramo_articulos[$clave]);
                $count = 0;
                $resto = array();

                foreach ($valor as $a => $b):
                    $stock = $b['stock'];
                    $lote = $b['lote'];
                    $cant_lote_form = (isset($count_tramos_select[$b['id_articulo']][$b['id']]) ? $count_tramos_select[$b['id_articulo']][$b['id']] : NULL);
                    if (($count < 1) && ($cant_lote_form == NULL)):
                        $resto[$count] = $stock;
                    elseif ((isset($resto[$count - 1])) && ($count > 0) && (isset($resto[$count - 1]) > 0) && $cant_lote_form != NULL && $first_tramo['stock'] > 0):
                        $mensajes_error[$clave][] = "El articulo $b[modelo]($b[id]) tiene como prioridad el lote $first_tramo[lote] antes que el lote $lote";
                    elseif ($cant_lote_form != NULL):
                        if (( $cant_lote_form <= $stock) && ($stock > 0)):
                            $resto[$count] = $stock - $cant_lote_form;
                        else:
                            $mensajes_error[$clave][] = "El articulo $b[modelo]($b[id] con lote $lote supera el stock existente que es de $stock";
                        endif;
                    endif;
                    ++$count;
                endforeach;

            endforeach;

            if (sizeof($mensajes_error) > 0):
                $response['articles'] = $this->SectionsArticles(array_keys($mensajes_error));
                $response['mensajes_error'] = $mensajes_error;
            else:
                $response['success'] = TRUE;
                $response['articles'] = $mensajes_error;
            endif;
            return $response;
        else:
            return $response;
        endif;
    }

    function EXC_selectCotizacion() {
        $id = $this->input->post('id_paciente');
        $cotizacion_aprobada = $this->categories_model->getCategory(array('_table' => 'cotizacion_estatus', '_label' => 'Aprobada'), ' id ');
        $cotizacion_aprobada = $cotizacion_aprobada[0]['id'];

        $data = $this->delivery_model->getCotizacionByPaciente($id, $cotizacion_aprobada);
        $resp = array(
            'success' => TRUE,
            'data' => $this->load->view('delivery/delivery/_option_paciente', array('data' => $data), TRUE));
        $this->offOutput();
        echo json_encode($resp);
    }

    function EXC_selectCotizacionForCliente() {
        $id = $this->input->post('id_cliente');
//        $cotizacion_aprobada = $this->categories_model->getCategory(array('_table' => 'cotizacion_estatus', '_label' => 'Aprobada'), ' id ');
//        $cotizacion_aprobada = $cotizacion_aprobada[0]['id'];

        $data = $this->delivery_model->getCotizacionByCliente($id, 1);

        $resp = array(
            'success' => TRUE,
            'data' => $this->load->view('delivery/delivery/_option_cotizacion_cliente', array('data' => $data), TRUE));
        $this->offOutput();
        echo json_encode($resp);
    }

    function EXC_selectClienteForDelivery() {
        $cat_letra = $this->input->post('cat_letra');
//        $cotizacion_aprobada = $this->categories_model->getCategory(array('_table' => 'cotizacion_estatus', '_label' => 'Aprobada'), ' id ');
//        $cotizacion_aprobada = $cotizacion_aprobada[0]['id'];

        $data = $this->delivery_model->getClienteForDelivery($cat_letra);

        $resp = array(
            'success' => TRUE,
            'data' => $this->load->view('delivery/delivery/_option_cliente', array('data' => $data), TRUE));
        $this->offOutput();
        echo json_encode($resp);
    }

    /**
     * Provee el los datos para las tab del create con respecto a un cliente
     *
     * @access public
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 09/01/14 02:16 PM
     */
    function EXC_getDataClient($id = NULL) {
        $array_post = $this->input->post();

        $response = array('success' => FALSE, 'data' => array());
        $id_cot = $array_post['id'];

        $lote = $this->articulo_model->getTramoArticulo(null, true);
        $data_cot = $this->Cotizacion_free_model->getById($array_post['id']);

        $array_post['id'] = $data_cot['id_persona_empresa'];
        $salida['response']['cliente'] = '';
        $salida['response']['cedula_rif'] = '';
        $salida['response']['email'] = '';
        $this->offOutput();
        if (!$array_post['id'] && $array_post['tipo_persona'])
            echo json_encode($response);
        if ($data_cot['tipo_persona'] == 'natural'):
            $data['cliente'] = $this->persona_model->getPersona($array_post['id']);
            $salida['response']['cliente'] = $data['cliente'] [0]['primer_nombre'] . ' ' . $data['cliente'] [0]['primer_apellido'];
            $salida['response']['cedula_rif'] = $data['cliente'] [0]['cedula'];
            $salida['response']['email'] = $data['cliente'] [0]['correo_electronico'];
        elseif ($data_cot['tipo_persona'] == 'juridica'):
            $data['cliente'] = $this->empresa_model->getEmpresa($array_post['id']);
            $salida['response']['cliente'] = $data['cliente'] [0]['nombre'];
            $salida['response']['cedula_rif'] = (empty($data['cliente'] [0]['rif']) ? 'Sin Informacion' : $data['cliente'] [0]['rif']);
            $salida['response']['email'] = (empty($data['cliente'] [0]['email']) ? 'Sin Informacion' : $data['cliente'] [0]['email']);
        endif;
        $salida['response'] ['id'] = $id_cot;
        $salida['response'] ['vendedor'] = $data_cot['vendedor'];
        $salida['materiales'] = $this->separateItemForQuantity($this->Cotizacion_free_model->getArticulosByCotFree($id_cot));
        $salida['response']['tipo_cotizacion'] = $array_post['tipo_cotizacion'];

        $tipo_cotizacion = $this->categories_model->getCategory(array('_table' => 'tipo_cotizacion', '_label' => 'Libre'), ' id ');
        $tipo_cotizacion = $tipo_cotizacion[0]['id'];
        $salida['response']['tipo_cot'] = $tipo_cotizacion;


        if ($salida):
            $response = array(
                'success' => TRUE,
                'data' => $salida['response'],
                'materiales' => $this->load->view('delivery/delivery/_materiales', array('operation' => 'detail', 'materiales' => $salida['materiales'], 'lotes' => $lote), true),
                'resumen' => $this->load->view('delivery/delivery/_resumen', array('operation' => 'detail', 'data' => $salida['response']), true)
            );
        endif;
        echo json_encode($response);
    }

    /**
     * Provee los datos de articulos partiendo de un arreglo de id
     *
     * @access public
     * @param array $params Identificadores de los articulos
     * @author  Frederick D. Bustamante G.
     * @version 1.0 14/01/14 12:34 PM
     */
    function articlesForPdf($params) {
        if (!$params)
            return FALSE;
        $articulos = array();
        $articles_for_lotes = array();
        $articles_n = array();
        $response = array();

        $articles = $this->delivery_model->getArticulosByPdf_preview(array_keys($params));
        if ($articles):
            foreach ($articles as $key => $value):
                $articles_n[$value['id']] = $value;
            endforeach;
            foreach ($params as $key => $value):
                foreach ($value as $clave => $valor):
                    if (isset($params[$key][$clave])):
                        if (!isset($articles_for_lotes[$key][$valor])) :
                            $articles_for_lotes[$key][$valor] = 1;
                        else:
                            $articles_for_lotes[$key][$valor]+=1;
                        endif;
                    endif;
                endforeach;
            endforeach;
            foreach ($articles_for_lotes as $key => $value):
                foreach ($value as $clave => $valor):
                    $response[$key] [$clave] = $articles_n[$key];
                    $response[$key] [$clave]['lote'] = $clave;
                    $response[$key] [$clave]['cantidad'] = $valor;
                endforeach;
            endforeach;
            return $response;
        else:
            return FALSE;
        endif;
    }

}
