<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Descripcion de  Despacho
 *
 * @package conmed.application
 * @subpackage modules
 * @category controller
 * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V - 1.0 09/10/13 11:51 AM
 */
class Delivery_free extends AST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('cotizacion/cotizacion_model', 'Cotizacion_model');
        $this->load->model('delivery/delivery_free_model', 'delivery_free_model');
        $this->load->model('persona/empleado_model', 'empleado_model');
        $this->load->model('persona/paciente_model', 'paciente_model');
        $this->load->model('inventario/articulo_model', 'articulo_model');
        $this->load->model('settings/categories_model', 'categories_model');
        $this->load->model('persona/persona_model', 'persona_model');
        $this->load->model('empresa/empresa_model', 'empresa_model');
    }

    /**
     * Provee el acceso al formulario de ingreso de nuevos datos, asi mismo,
     * provee el acceso a la insercion de datos nuevos dentro en base de datos.
     *
     * @access public
     * @param array $params Parametros adicionales enviados al controlador.
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.2 18/10/13 09:59 AM
     */
    function create($params = NULL) {
        $modo = $params;
        if (!$this->input->post()) {
//Arreglo de datos para la pestana datos
            $tab_datos = array(
                'operation' => 'create',
                'tipo_persona' => $this->categories_model->getCategory(array('_table' => 'letra'), 'id,_label')
            );

            $tabs = array(
                '1' => array('view' => 'particular', 'label' => 'Datos', 'params' => array('file' => 'delivery/delivery_free/_datos', 'data' => $tab_datos)),
                '2' => array('view' => 'particular', 'label' => 'Articulos', 'params' => array('file' => 'delivery/delivery_free/_articulos', 'data' => array('operation' => 'create'))),
                '3' => array('view' => 'particular', 'label' => 'Resumen Nota de Entrega', 'params' => array('file' => 'delivery/delivery_free/_resumen', 'data' => array('operation' => 'create')))
            );
            $view['view'] = $this->view_engine->tab($tabs);

            $this->setContent($this->load->view('delivery/delivery_free/create/create', $view, TRUE));
        } elseif ($params[0] == 'process') {

            if (isset($modo[1]) && $modo[1] == 'validate_data'):
                $this->_avancedFormValidation('insert', TRUE);
            elseif (isset($modo[1]) && $modo[1] == 'preview'):
                $this->_avancedFormValidation('ARTICULOS');
            else:
                $this->_avancedFormValidation('insert');
            endif;
            $process = $this->view_engine->process();

            $success = FALSE;
            if ($process['result']) {
//                $params = array('format_type' => 'insert', 'data' => $process['data']);
//                $data = $this->_format($params);
                if (isset($modo[1]) && $modo[1] == 'validate_data'):
                    $this->offOutput();
                    echo json_encode(array('validate' => true));
                elseif (isset($modo[1]) && $modo[1] == 'preview'):
                    $this->offOutput();
                    $params = array('format_type' => 'insert', 'data' => $process['data']);
                    $data = $this->_format($params);
                    $this->EXC_quoteReport(TRUE, $data);
                else:

                    $params = array('format_type' => 'insert', 'data' => $process['data']);
                    $data = $this->_format($params);

                    $validate_tramos = $this->prioritySectionsArticles($data);
                    if (!$validate_tramos['success']):
                        $this->offOutput();
                        $data = $this->load->view('delivery/delivery_free/_validate_tramo', array('data' => $validate_tramos), TRUE);
                        $response = array('view' => 2, 'msg' => 'Verifique los lotes para cada articulo', 'data' => $data);
                        echo json_encode($response);
                    else:


                        $success = $this->delivery_free_model->insert($data);
                        $this->setContent($success);
                    endif;
                endif;
            } else {

                $this->setContent($process['validation_error']);
            }
            if (!isset($modo[1])):
// -----------------------------
//seteamos los datos de auditoria
                $this->audit->setOperationData($process);
                $this->audit->setChkResult($success);
// -----------------------------
            endif;
        }
    }

    /**
     * Provee el acceso a la busqueda de los combos tipo select y sus valores en BD
     *
     * @access public
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.2 18/10/13 09:59 AM
     */
    function EXC_searchSelect() {
        $data = $this->input->get('data');
        $arr_values = $this->delivery_free_model->getComboByTable('id_cotizacion', 'business.cotizacion.codigo', $data['q'], $label = 'text');
        $this->offOutput();
        echo json_encode(array('q' => $data['q'], 'results' => $arr_values));
    }

    /**
     * Provee el los datos para las tab del create con respecto a un cliente
     *
     * @access public
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.1 03/01/14 10:52 AM
     */
    function EXC_getDataClient() {

        $array_post = $this->input->post();
        $response = array('success' => FALSE, 'data' => array());
        $data['cliente'] = $this->persona_model->getPersona($array_post['id']);
        $salida['response']['cliente'] = '';
        $salida['response']['cedula_rif'] = '';
        $salida['response']['email'] = '';
        $this->offOutput();
        if (!$array_post['id'] && $array_post['tipo_persona'])
            echo json_encode($response);
        if ($array_post['tipo_persona'] == 46):
            $data['cliente'] = $this->persona_model->getPersona($array_post['id']);
            $salida['response']['cliente'] = $data['cliente'] [0]['primer_nombre'] . ' ' . $data['cliente'] [0]['primer_apellido'];
            $salida['response']['cedula_rif'] = $data['cliente'] [0]['cedula'];
            $salida['response']['email'] = $data['cliente'] [0]['correo_electronico'];
        else:
            $data['cliente'] = $this->empresa_model->getEmpresa($array_post['id']);
            $salida['response']['cliente'] = $data['cliente'] [0]['nombre'];
            $salida['response']['cedula_rif'] = (empty($data['cliente'] [0]['rif']) ? 'Sin Informacion' : $data['cliente'] [0]['rif']);
            $salida['response']['email'] = (empty($data['cliente'] [0]['email']) ? 'Sin Informacion' : $data['cliente'] [0]['email']);
        endif;


        if ($data):
            $response = array(
                'success' => TRUE,
                'data' => $salida['response']
            );
        endif;
        echo json_encode($response);
    }

    /**
     * Validaciones extras realizadas a la entidad, que no las soporta el View_Engine
     *
     * @access public
     * @param array $data Datos para aplicar el formateo.
     * @return Mixed , string  Validaciones o FALSE en caso de que no tenga errores
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.2 18/10/13 09:59 AM
     */
    private function _avancedFormValidation($format_type, $validate_data_free_tab = false) {
        $this->load->library('form_validation');
        $format_type = strtoupper($format_type);

        //Insercion
        if ($format_type == 'INSERT') {
            $post = $this->input->post();

            //Valida los elementos que se utilizan en esta parte de la validacion

            if ($validate_data_free_tab) {
                //valida la existencia del id persona empresa
                if ($post['id_tercero'] == ''):
                    $this->form_validation->set_rules('id_tercero', 'Persona/Empresa', 'required');
                endif;
                //Se verifica que exista la fecha de despacho
                if (empty($post['fecha_despacho'])):
                    $this->form_validation->set_rules('fecha_despacho', 'Fecha de Despacho', 'required|valid_date');
                endif;
            }
            else {
                $count_emtpy_art = 0;
                //valida la existencia del id persona empresa
                if ($post['id_tercero'] == ''):
                    $this->form_validation->set_rules('id_tercero', 'Persona/Empresa', 'required');



                elseif (isset($post['art'])):
                    $art = $post['art'];
                    $lot_art = $post['lote_art'];

                    foreach ($art as $key => $value):
                        if (empty($lot_art[$value])):
                            $count_emtpy_art = $count_emtpy_art + 1;
                        endif;
                    endforeach;
                    if ($count_emtpy_art > 0):
                        $this->form_validation->set_rules('art_valid', 'Lote de articulo', 'required');
                        $this->form_validation->set_message('required', 'Debe Asignar un Lote a cada Articulo');
                        return NULL;
                    endif;
                else:
                    $this->form_validation->set_rules('art_valid', 'Lote de articulo', 'required');
                    $this->form_validation->set_message('required', 'Debe agregar al menos un articulo a la orden de despacho');
                    return NULL;
                endif;


                //FIN FORMAT INSERT
            }
        }

        if ($format_type == 'ARTICULOS') {
            $post = $this->input->post();

            //valida la existencia del id persona empresa
            if ($post['id_tercero'] == ''):
                $this->form_validation->set_rules('id_tercero', 'Persona/Empresa', 'required');
                return FALSE;
            endif;
            //Se verifica que exista la fecha de despacho

            if ($post['fecha_despacho'] == ''):
                $this->form_validation->set_rules('fecha_despacho', 'Fecha de Despacho', 'required | valid_date');
                return FALSE;
            endif;



            $count_emtpy_art = 0;
            if (isset($post['art'])):
                $art = $post['art'];
                $lot_art = $post['lote_art'];

                foreach ($art as $key => $value):
                    if (empty($lot_art[$value])):
                        $count_emtpy_art = $count_emtpy_art + 1;
                    endif;
                endforeach;
                if ($count_emtpy_art > 0):
                    $this->form_validation->set_rules('art_valid', 'Lote de articulo', 'required');
                    $this->form_validation->set_message('required', 'Debe Asignar un Lote a cada Articulo');
                    return FALSE;
                endif;
            else:
                $this->form_validation->set_rules('art_valid', 'Lote de articulo', 'required');
                $this->form_validation->set_message('required', 'Debe agregar al menos un articulo a la orden de despacho');
                return FALSE;
            endif;
            //FIN FORMAT ARTICULOS
        }
    }

    /**
     * Formateo avanzado de los datos a insertar en cotizacion
     *
     * @access public
     * @param array $format_type Tipo de Formateo [INSERT o UPDATE].
     * @param array $data Datos para aplicar el formateo.
     * @return array Arreglo con los datos formateados segun la regla de negocio.
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.2 18/10/13 09:59 AM
     */
    function _avancedFormat($format_type, $data) {

        $format_type = strtoupper($format_type);
        $created_by = $this->empleado_model->getIdByUser($this->session->userdata('user_id'));
        if ($format_type == 'INSERT') {

            $orden = $this->_extendsFormat($data);

            //------------- INICIO FORMAT orden de despacho libre
            $tipo_orden = $this->categories_model->getCategory(array('_table' => 'tipo_despacho', '_label' => 'Libre'), ' id ');
            $orden['id_cirugia'] = $data['id_cirugia'];
            $orden['id_cotizacion'] = NULL;
            $orden['fecha_despacho'] = $data['fecha_despacho'];
            $orden['fecha_solicitud'] = 'now()';
            $orden['id_cliente'] = $data['id_tercero'];
            $orden['cat_tipo_despacho'] = $tipo_orden[0]['id'];
            $orden['id_despachador'] = $orden['created_by'] = $created_by;

            $orden_free_articulo = array();
            if (isset($data['art'])):
                $art = $data['art'];
                $lote_art = $data['lote_art'];
                $type_art = $data['type_art'];
                $cant_art = $data['cant_art'];
                $precios_art = $data['precios_art'];
                $cant = 0;
                foreach ($art as $key => $value):
                    $type = strtoupper($type_art[$value]);
                    $tipo_articulo = $this->categories_model->getCategory(array('_table' => 'tipo_articulo', '_label' => $type), ' id ');

                    $lote = $lote_art[$value];
                    $cant = (int) $cant_art[$value];
                    $precio = $precios_art[$value];
                    if ($cant > 1):
                        for ($i = 1; $i <= $cant; $i++):
                            $orden_fre_articulo[$key . $i]['tipo_articulo'] = $tipo_articulo[0]['id'];
                            $orden_fre_articulo[$key . $i]['id_articulo'] = $key;
                            $orden_fre_articulo[$key . $i]['id_lote'] = $lote;
                            $orden_fre_articulo[$key . $i]['cantidad'] = 1;
                            $orden_fre_articulo[$key . $i]['precio'] = $precio;
                            $orden_fre_articulo[$key . $i]['created_by'] = $created_by;
                        endfor;
                    else:

                        $orden_fre_articulo[$key]['tipo_articulo'] = $tipo_articulo[0]['id'];
                        $orden_fre_articulo[$key]['id_articulo'] = $key;
                        $orden_fre_articulo[$key]['id_lote'] = $lote;
                        $orden_fre_articulo[$key]['cantidad'] = 1;
                        $orden_fre_articulo[$key]['precio'] = $precio;
                        $orden_fre_articulo[$key]['created_by'] = $created_by;
                    endif;
                endforeach;
            endif;
            //-------------FIN FORMAT  orden de despacho libre
            //--------------Retorno de datos
            return array(
                'orden' => $orden,
                'orden_free_articulo' => $orden_fre_articulo
            );
        }
    }

    /**
     * Formateo extendido de datos, se usa para aplicar el mismo formato a la operacion que guarda
     * en varias entidades
     *
     * @access public
     * @param array $data Datos para aplicar el formateo.
     * @return array Arreglo con los datos formateados segun la regla de negocio.
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.2 18/10/13 09:59 AM
     */
    private function _extendsFormat() {
        $data_ext = array();
        $data_ext['created_by'] = $this->empleado_model->getIdByUser($this->session->userdata('user_id'));
        return $data_ext;
    }

    /**
     * Genera o ubica documentos de acuerdo al tipo de documento.
     *
     * @access public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 03/01/14 10:17 AM
     */
    public function EXC_quoteReport($preview = FALSE, $data = array()) {
        $this->offOutput();
        $id = $this->input->post('id');
        $type_report = $this->input->post('tipo');
        $membrete = $this->input->post('membrete');
        $dir_modo_impresion = ($membrete == 'true' ? 'membrete_on' : 'membrete_off');
        $alvuelo = true;
        if ($preview):
            $array_post = $this->input->post();
            $data['codigo_orden'] = $this->delivery_free_model->generateCode();
            $cuerpo_articulos = $this->delivery_free_model->getArticulosByPdf_preview($array_post['art']);
            if ($cuerpo_articulos):
                foreach ($cuerpo_articulos as $key => $value):
                    $cuerpo_articulos[$key]['cantidad'] = $array_post['cant_art'][$value['id']];
                    $cuerpo_articulos[$key]['lote'] = $array_post['lote_art'][$value['id']];
                    $cuerpo_articulos[$key]['precio'] = $array_post['precios_art'][$value['id']];
                endforeach;
                $params_pdf = array(
                    'codigo_orden' => $data['codigo_orden'],
                    'fecha_emision' => date('d/m/Y'),
                    'vendedor' => '',
                    'cliente' => $array_post['cliente-15104'],
                    'ced_rif' => $array_post['ci_rif-15104'],
                    'nit' => '',
                    'email' => (!empty($array_post['email-15104']) ? $array_post['email-15104'] : 'S/I'),
                    'tlf' => (!empty($array_post['tlf_movil-15104']) ? $array_post['tlf_movil-15104'] : 'S/I')
                );
                $params_pdf['articulos'] = $cuerpo_articulos;
                $response = array(
                    'view' => true,
                    'file' => $this->_create_document($params_pdf, false, 'preview', $membrete)
                );
            else:
                $response = array(
                    'view' => false
                );
            endif;
        elseif ($id):
            $id_orden_despacho = $this->encrypt->decode($id);
            $data = $this->delivery_free_model->dataForPdfOfOrden($id_orden_despacho);
            $cuerpo_articulos = $this->delivery_free_model->getArticulosByPdf($id_orden_despacho);
            switch ($type_report):
                case 'articulos':
                    $params_pdf = array(
                        'codigo_orden' => (!empty($data[0]['codigo']) ? $data[0]['codigo'] : 'S/I'),
                        'fecha_emision' => $data[0]['fecha_inicio'],
                        'fecha_despacho' => (!empty($data[0]['fecha_entrega']) ? $data[0]['fecha_entrega'] : 'S/I'),
                        'email' => (!empty($data[0]['email']) ? $data[0]['email'] : 'S/I'),
                        'vendedor' => (!empty($data[0]['tlf']) ? $data[0]['tlf'] : 'S/I'),
                        'cliente' => (!empty($data[0]['cliente']) ? $data[0]['cliente'] : 'S/I'),
                        'ced_rif' => (!empty($data[0]['ced_rif']) ? $data[0]['ced_rif'] : 'S/I'),
                        'nit' => '',
                        'tlf' => (!empty($data[0]['tlf']) ? $data[0]['tlf'] : 'S/I')
                    );

                    $params_pdf['articulos'] = $cuerpo_articulos;

                    $response = array(
                        'sucess' => true,
                        'file' => $this->_create_document($params_pdf, false, 'articulos', $membrete)
                    );
                    break;
                case 'almacen':
                    $params_pdf = array(
                        'codigo_orden' => (!empty($data[0]['codigo']) ? $data[0]['codigo'] : 'S/I'),
                        'fecha_emision' => $data[0]['fecha_inicio'],
                        'fecha_despacho' => (!empty($data[0]['fecha_entrega']) ? $data[0]['fecha_entrega'] : 'S/I'),
                        'email' => (!empty($data[0]['email']) ? $data[0]['email'] : 'S/I'),
                        'vendedor' => (!empty($data[0]['tlf']) ? $data[0]['tlf'] : 'S/I'),
                        'cliente' => (!empty($data[0]['cliente']) ? $data[0]['cliente'] : 'S/I'),
                        'ced_rif' => (!empty($data[0]['ced_rif']) ? $data[0]['ced_rif'] : 'S/I'),
                        'nit' => '',
                        'tlf' => (!empty($data[0]['tlf']) ? $data[0]['tlf'] : 'S/I')
                    );

                    $params_pdf['articulos'] = $cuerpo_articulos;

                    $response = array(
                        'sucess' => true,
                        'file' => $this->_create_document($params_pdf, false, 'almacen', $membrete)
                    );
                    break;
            endswitch;
        else:
            $response = array(
                'sucess' => false
            );
        endif;
        echo json_encode($response);
    }

    public function _create_document($params = array(), $background_no_validate = FALSE, $document = '', $membrete = FALSE) {
        $this->lang->load('app_doc');
        $this->lang->load('app_email');
        $this->load->library('encrypt');
        $this->load->helper('view_engine_helper');
        extract($params);
        $dir_modo_impresion = ($membrete == 'true' ? 'membrete_on' : 'membrete_off');
        $dir_modo_impresion = '';
        switch ($document) {
            case 'preview':
                $view = 'delivery/delivery_free/pdf_articulos_orden';
                $name = 'orden_despacho_preview';
                $dir = 'despacho_libre/preview';
                break;

            case 'articulos':
                $view = 'delivery/delivery_free/pdf_articulos_orden';
                $name = 'orden_despacho';
                $dir = 'despacho_libre/articulos';
                break;

            case 'almacen':
                $view = 'delivery/delivery_free/pdf_almacen_orden';
                $name = 'orden_despacho_almacen';
                $dir = 'despacho_libre/almacen';
                break;
            default:
                $view = 'pdf_materiales_orden';
                $name = 'ord';
                break;
        }
//Cadena que contiene el string para generar el pdf
        $html = $this->load->view($view, $params, true);

// -----------------------------
// -----------------------------
//Asignacion de nombre, ruta y generacion del documento
        $name_file = "{$name}_{$codigo_orden}";
        $destination_path = "uploads/{$dir}/";
//$destination_path = ($background_no_validate ? $destination_path . "preview/" : $destination_path);

        generate_pdf($name_file, $destination_path, 'F', $html, $background_no_validate, $membrete);
        return utf8_decode("{$destination_path}" . "{$name_file}.pdf");
// -----------------------------
    }

    /**
     * Forza la descarga de un archivo pdf
     *
     * @access public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 15/11/13 09:53 AM
     */
    function EXC_force_download() {
        $array_get = $this->input->get();
        $this->offOutput();
        $array_url = explode('_', $array_get['url']);
        $array_url_invertido = array_reverse($array_url);


        $len = filesize($array_get['url']);
        header('Content-type: application/pdf');
        header('Content-Disposition: attachment; filename="' . $array_get['name'] . '_' . $array_url_invertido[0] . '"');
        header('Content-Length: ' . $len);
        readfile($array_get['url']);
    }

    /**
     * Se encarga de mostrar los detalles de una cotizacion
     *
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 25/10/13 10:19 AM
     */
    function detail() {

        $id_ec = $this->input->post('id');
        $target = $this->input->post('target_on');
        $id_orden_despacho = $this->encrypt->decode($id_ec);
        if (!empty($id_orden_despacho)) {

            $data['orden'] = $this->delivery_free_model->getDataDelivery($id_orden_despacho);

            $array_articulos[] = $data['articulos'] = $this->delivery_free_model->getArticulosByOrden($id_orden_despacho);



            //Arreglo de datos para la pestana datos
            $tabs = array(
                '1' => array('view' => 'particular', 'label' => 'Datos', 'params' => array('file' => 'delivery/delivery_free/_datos', 'data' => array('operation' => 'detail', 'data' => $data['orden']))),
                '3' => array('view' => 'particular', 'label' => 'Articulos', 'params' => array('file' => 'delivery/delivery_free/_articulos', 'data' => array('operation' => 'detail', 'articulos' => $array_articulos))),
                '4' => array('view' => 'particular', 'label' => 'Resumen Nota de Entrega', 'params' => array('file' => 'delivery/delivery_free/_resumen', 'data' => array('operation' => 'detail', 'data' => $data['orden'])))
            );

            $view['view'] = $this->view_engine->tab($tabs);
            $this->setContent($this->load->view('delivery/delivery_free/detail', $view, TRUE));
        }
    }

    /**
     * Separa el arreglo de cotizacion articulo
     * repitiendo las filas del articulo por la
     * cantidad que posea.
     *
     * @access  public
     * @param   array $articles Arreglo de articulos para separar por cantidad
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 11/11/13 11:13 AM
     */
    public function separateItemForQuantity($articles = array()) {
        if (!$articles)
            return FALSE;

        $separateItem = array();
        foreach ($articles as $key => $value):
            for ($i = 1; $i <= $value['cantidad']; $i++):
                $articles[$key]['cantidad'] = 1;
                $separateItem[$value['id']][] = $articles[$key];
            endfor;
        endforeach;
        return $separateItem;
    }

    /**
     * Retorna los articulos sustituidos para listar
     *
     * @access  public
     * @param   array   $data Parametros de entrada
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 08/11/13 10:29 AM
     */
    private function _listReplacements($data = array()) {


        if ($data['materiales']):
            foreach ($data['materiales'] as $key => $value):
                if ($this->Cotizacion_model->checkReplaceArticle($value['id'])):
                    $data['materiales'] [$key]['sustituidos'] = $this->Cotizacion_model->checkReplaceArticle($value['id']);
                else:
                    unset($data['materiales'] [$key]);
                endif;

            endforeach;
        endif;

        if ($data['instrumentos']):
            foreach ($data['instrumentos'] as $key => $value):
                if ($this->Cotizacion_model->checkReplaceArticle($value['id'])):
                    $data['instrumentos'] [$key]['sustituidos'] = $this->Cotizacion_model->checkReplaceArticle($value['id']);
                else:
                    unset($data['instrumentos'] [$key]);
                endif;
            endforeach;
        endif;

        if ($data['backup']):
            foreach ($data['backup'] as $key => $value):
                if ($this->Cotizacion_model->checkReplaceArticle($value['id'])):
                    $data['backup'] [$key]['sustituidos'] = $this->Cotizacion_model->checkReplaceArticle($value['id']);
                else:
                    unset($data['backup'] [$key]);
                endif;
            endforeach;
        endif;

        return $data;
    }

    /**
     *  Provee los tramos asociados a cada articulo asi como
     *  su disponibilidad de acuerdo a la fecha de (vencimiento,creacion) y stock
     *
     * @access  public
     * @param   array $params Arreglo de articulos a evaluar
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 19/11/13 03:43 PM
     */
    function SectionsArticles($params = array()) {
        $id_articulos_con_tramos = array();
        $tramo = array();
        $tramo = $this->delivery_free_model->availabilityTramoOfItems($params);
        if ($tramo):
            foreach ($params as $key => $value) :
                foreach ($tramo as $clave => $valor):
                    if ($valor['id_articulo'] == $value):
                        $id_articulos_con_tramos[$value][$valor['id']] = $tramo[$clave];
                    endif;
                endforeach;
            endforeach;
        endif;
        return $id_articulos_con_tramos;
    }

    /**
     *  Agrupa los los datos del insert por articulos y cuenta los lotes
     * (Este metodo se utiliza para evaluar cantidad de articulos a insertar en cada lote)
     *
     * @access  private
     * @param   array $id_articulos Arreglo de articulos sin repeticion
     * @param   array $merge_articulos Unificacion de articulos provenientes del form (materiales, instrumental, backup)
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 22/11/13 03:09 PM
     */
    private function countRepeatSectionsForArticles($id_articulos = array(), $merge_articulos = array()) {
        $tramo_articulos = $this->SectionsArticles($id_articulos);
        $count = array();
        foreach ($id_articulos as $key => $value):

            foreach ($tramo_articulos[$value] as $k => $v):
                $count[$value][$v['id']] = 0;
                foreach ($merge_articulos as $a => $b):
                    if ($b['id_articulo'] == $value && $b['id_lote'] == $v['id']):
                        $count[$value][$v['id']] = $count[$value][$v['id']] + 1;
                    endif;
                endforeach;
            endforeach;
        endforeach;
        return $count;
    }

    /**
     *  Evalua la prioridad de seleccion de articulos con
     * respecto al stock y la fecha de creacion del lote
     *
     * @access  public
     * @param   array $params Arreglo de articulos a evaluar
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 22/11/13 02:51 PM
     */
    function prioritySectionsArticles($params = array()) {
//---------Declaracion e Inicializacion de variables--------------------------------------------------//
        $id_articulos = array();
        $mensajes_error = array();
        $response = array('success' => FALSE);
        $array_articulos = (isset($params['orden_free_articulo']) ? $params['orden_free_articulo'] : array());

        $merge_articulos = array_merge($array_articulos);

//----------------------------------------------------------------------------------------------------//
//----------Extracion y unificacion de id_articulo---------------------------------------//
        if ($merge_articulos):
            foreach ($merge_articulos as $key => $value):
                $id_articulos[$key] = $value['id_articulo'];
            endforeach;
        endif;
//--------------------------------------------------------------------------------------//

        $tramo_articulos = $this->SectionsArticles($id_articulos);
        $count_tramos_select = $this->countRepeatSectionsForArticles($id_articulos, $merge_articulos);

        if (sizeof($tramo_articulos)):
            foreach ($tramo_articulos as $clave => $valor):
                $first_tramo = current($tramo_articulos[$clave]);
                $count = 0;
                $resto = array();

                foreach ($valor as $a => $b):
                    $stock = $b['stock'];
                    $lote = $b['lote'];
                    $cant_lote_form = (isset($count_tramos_select[$b['id_articulo']][$b['id']]) ? $count_tramos_select[$b['id_articulo']][$b['id']] : NULL);
                    if (($count < 1) && ($cant_lote_form == NULL)):
                        $resto[$count] = $stock;
                    elseif ((isset($resto[$count - 1])) && ($count > 0) && (isset($resto[$count - 1]) > 0) && $cant_lote_form != NULL && $first_tramo['stock'] > 0):
                        $mensajes_error[$clave][] = "El articulo $b[modelo]($b[id]) tiene como prioridad el lote $first_tramo[lote] antes que el lote $lote";
                    elseif ($cant_lote_form != NULL):
                        if (( $cant_lote_form <= $stock) && ($stock > 0)):
                            $resto[$count] = $stock - $cant_lote_form;
                        else:
                            $mensajes_error[$clave][] = "El articulo $b[modelo]($b[id] con lote $lote supera el stock existente que es de $stock";
                        endif;
                    endif;
                    ++$count;
                endforeach;

            endforeach;

            if (sizeof($mensajes_error) > 0):
                $response['articles'] = $this->SectionsArticles(array_keys($mensajes_error));
                $response['mensajes_error'] = $mensajes_error;
            else:
                $response['success'] = TRUE;
                $response['articles'] = $mensajes_error;
            endif;
            return $response;
        else:
            return $response;
        endif;
    }

    /**
     * Provee los selct  para el listado de articulos
     *
     * @access  pulic
     * @return array Datos para select de articulos
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 03/01/14 11:28 AM
     */
    function EXC_selectForArticles() {
        $this->offOutput();
        $array_post = $this->input->post();
        $option = '';
        $response = array(
            'success' => FALSE,
            'option' => $option
        );
        if (!isset($array_post['id']) && empty($array_post['id']))
            return $response;
        $data['lotes'] = $this->articulo_model->getTramoArticulo($array_post['id'], FALSE, TRUE);
        $data['precios'] = $this->articulo_model->getPriceArticulo($array_post['id']);

        $data['id_article'] = $array_post['id'];
        $option = $this->load->view('delivery/delivery_free/lotes_for_articles', $data, TRUE);
        $option_precio = $this->load->view('delivery/delivery_free/precios_for_articles', $data, TRUE);

        $response = array(
            'success' => TRUE,
            'option' => $option,
            'option_precio' => $option_precio
        );


        echo json_encode($response);
    }

    function getLabelCategory($where = array(), $select = '', $return = '') {
//        $return.=(empty($return) ? ' id ' : ' ,id ');
//        $select.=(empty($select) ? ' * ' : ' ,id , _label ');
//        $category = $this->categories_model->getCategory(array(), $select);
//        if (sizeof($where) > 0)
//            foreach ($category as $key => $value):
//
//            endforeach;
//        endif;
//        echo '<pre>';
//        var_dump($category);
//        echo '</pre>';
//        exit;
    }

}
