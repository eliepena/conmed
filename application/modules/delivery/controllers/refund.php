<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of refund
 *
 * Esta clase provee la implementacion para la capa de control de
 * acciones de la entidad persona y sus entidades dependientes o extendidas
 *
 * @package Module
 * @category Controller
 * @author Eliezer Pe#a. <pe.eliezer@gmail.com>
 * @version V-1.0 28/12/13 
 */
class Refund extends AST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('cotizacion/cotizacion_model', 'Cotizacion_model');
        $this->load->model('delivery/delivery_model', 'delivery_model');
        $this->load->model('delivery/refund_model', 'refund_model');
        $this->load->model('persona/empleado_model', 'empleado_model');
        $this->load->model('persona/paciente_model', 'paciente_model');
        $this->load->model('delivery/delivery_free_model', 'delivery_free_model');
    }

    /**
     * Provee el acceso al formulario de ingreso de nuevos datos, asi mismo,
     * provee el acceso a la insercion de datos nuevos dentro en base de datos.
     *
     * @access public
     * @param array $params Parametros adicionales enviados al controlador.
     * @author Eliezer Pe#a <pe.eliezer@gmail.com>
     * 
     */
    function create($params = NULL) {
        $id = $this->session->userdata('orden_despacho.id');


        if (!$this->input->post()) {

            $data['orden'] = $this->delivery_model->getDataDelivery($id);
            $data['cotizacion'] = $this->Cotizacion_model->getById($id);
            $data['materiales'] = $this->refund_model->getArticles($id, 'MAT', '0');
            $data['backup'] = $this->refund_model->getArticles($id, 'MAT', '1');
            $data['instrumentos'] = $this->refund_model->getArticles($id, 'INS', '0');
            //   print_r($data['backup']);die;
            //Arreglo de datos para la pestana datos
            $tab_datos = array(
                'operation' => 'detail',
                'data_cotizacion' => $this->refund_model->getComboByTable('id_orden_despacho', 'business.orden_despacho.codigo', FALSE, $label = 'text', 93)
                , 'data_paciente' => $this->refund_model->searchCostumer());
            //   print_r($tab_datos);die;
            $tabs = array(
                '10' => array('view' => 'particular', 'label' => 'Datos', 'params' => array('file' => 'delivery/refund/_datos', 'data' => $tab_datos)),
                '11' => array('view' => 'particular', 'label' => 'Materiales', 'params' => array('file' => 'delivery/refund/_materiales', 'data' => array('operation' => 'create', 'materiales' => $data['materiales']))),
                '12' => array('view' => 'particular', 'label' => 'Instrumental', 'params' => array('file' => 'delivery/refund/_instrumentos', 'data' => array('operation' => 'create', 'instrumentos' => $data['instrumentos']))),
                '13' => array('view' => 'particular', 'label' => 'Backup', 'params' => array('file' => 'delivery/refund/_backup_material', 'data' => array('operation' => 'create', 'backup' => $data['backup']))),
                '14' => array('view' => 'particular', 'label' => 'Resumen Nota de Entrega', 'params' => array('file' => 'delivery/refund/_resumen', 'data' => array('operation' => 'create', 'data' => $data['cotizacion'])))
            );
            //$this->setTargetOn('window');
            $view['view'] = $this->view_engine->tab($tabs);
            $this->setContent($this->load->view('delivery/refund/create/create', $view, TRUE));
        } elseif ($params[0] == 'process') {
            $array_post = $this->input->post();
            $id = $array_post['codigo'];
            //  print_r($array_post); die;
            if (isset($modo[1]) && $modo[1] == 'validate_data'):
                $this->_avancedFormValidation('insert', true);
            else:
                $this->_avancedFormValidation('insert');
            endif;
            $process = $this->view_engine->process();
            if ($process['result']) {
                if (isset($modo[1]) && $modo[1] == 'validate_data'):
                    $this->offOutput();
                    echo json_encode(array('validate' => true));
                else:
                    $params = array('format_type' => 'insert', 'data' => $process['data']);
                    $data = $this->_format($params);
                    $data['id_orden_despacho'] = $id;
                    $success = $this->refund_model->insert($data);
                    $this->setContent($success);
                endif;
                $this->setContent($success);
            } else {

                $this->setContent($process['validation_error']);
            }
            if (!isset($modo[1])):
                // -----------------------------
                //seteamos los datos de auditoria
                $this->audit->setOperationData($process);
                $this->audit->setChkResult($success);
            // -----------------------------
            endif;
        }
    }

    /**
     * Validaciones extras realizadas a la entidad, que no las soporta el View_Engine
     *
     * @access public
     * @param array $data Datos para aplicar el formateo.
     * @return Mixed , string  Validaciones o FALSE en caso de que no tenga errores
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.2 18/10/13 09:59 AM
     */
    private function _avancedFormValidation($format_type, $validate_data_tab = false) {
        $this->load->library('form_validation');
        $format_type = strtoupper($format_type);

        //Insercion
        if ($format_type == 'INSERT') {
            $post = $this->input->post();

            //Valida los elementos que se utilizan en esta parte de la validacion
            if (!isset($post['codigo']) OR !isset($post['id_cirugia']))
                return FALSE;
            if ($validate_data_tab) {
                //valida la existencia del codigo de la cotizacion
                if ($post['codigo'] == '')
                    $this->form_validation->set_rules('codigo', 'Cotizacion', 'required');
                $this->form_validation->set_message('required', 'Debe seleccionar una orden de despacho');
                //Se verifica que exista la fecha de despacho
                if (!isset($post['fecha_despacho'])) {
                    if ($post['fecha_despacho'] == ''):
                        $this->form_validation->set_rules('fecha_despacho', 'Fecha de Despacho', 'required|valid_date');
                    endif;
                }
            }
            else {
                
            }
        }
    }

    /**
     * Provee el los datos para las tab del create con respecto a una cotizacion
     *
     * @access public
     * @author
     * @version V 1.2 18/10/13 10:57 AM
     */
    function EXC_getDataCotizacion() {
        $this->load->model('cotizacion/cotizacion_free_model', 'cotizacion_free_model');
        $this->load->model('cotizacion/cotizacion_model', 'cotizacion_model');
        $this->load->library('View_engine');
        $array_post = $this->input->post();

        $response = array('success' => FALSE, 'data' => array());
        $this->offOutput();
        if (!$array_post['id'])
            echo json_encode($response);
        if (!isset($array_post['tipo'])) {

            $tipo = $this->refund_model->getTipoCliente($array_post['id']);
            $array_post['tipo'] = $tipo[0]['tipo'];
         
        }
        if ($array_post['tipo'] == 'per' || $array_post['tipo'] == 'emp') {

            $data['cotizacion'] = $this->refund_model->getDatosGenerales($array_post['id'], $array_post['tipo']);
        } elseif ($array_post['tipo'] == 'pac') {
            $id_cotizacion = $this->refund_model->getCotId($array_post['id']);
            $data_cot = $this->cotizacion_model->getById($id_cotizacion);
            $data['cotizacion'] = $this->delivery_model->getDataDelivery($id_cotizacion);
           $data['cotizacion'][0]=$data_cot;
//            print_r($data);
//            die;
        }
        $data['materiales'] = $this->refund_model->getArticles($array_post['id'], 'MAT', '0');
        $data['backup'] = $this->refund_model->getArticles($array_post['id'], 'MAT', '1');
        $data['instrumentos'] = $this->refund_model->getArticles($array_post['id'], 'INS', '0');
        //$lote = $this->articulo_model->getTramoArticulo(null, true);

        if ($data):
            $response = array(
                'success' => TRUE,
                'data' => $data['cotizacion'][0],
                'instrumentos' => $this->load->view('delivery/refund/_instrumentos', array('operation' => 'create', 'instrumentos' => $data['instrumentos']), true),
                'materiales' => $this->load->view('delivery/refund/_materiales', array('operation' => 'create', 'materiales' => $data['materiales']), true),
                'backup' => $this->load->view('delivery/refund/_backup_material', array('operation' => 'create', 'backup' => $data['backup']), true),
                'resumen' => $this->load->view('delivery/refund/_resumen', array('operation' => 'create', 'data' => $data['cotizacion']), true)
            );
        //print_r($response);die;
        endif;
        echo json_encode($response);
    }

    /**
     * sirve para hacer el list de la orden de devolucion
     *
     * @access public
     * @author  Eliezer Pe#a
     * @version V 1.0 
     */
    function listOrden_devolucion($params) {

        $this->listAll();
    }

    /**
     * metodo para paginacion de ordenes de devolucion
     *
     * @access public
     * @author  Eliezer Pe#a
     * @version V 1.0 
     */
    public function getPaginationData($params) {
        $uri = $this->uri->uri_string();

        $array_post = $this->input->post();

        if (strpos($uri, 'listAll')) {
            if (isset($array_post['id'])) {
                $id = $this->encrypt->decode($array_post['id']);
                $this->session->set_userdata('orden_despacho.id', $id);
            }
            $params['id'] = $this->session->userdata('orden_despacho.id');
            $data['data'] = $this->refund_model->getOrden_devolucion($params);
            $data['count'] = $this->refund_model->countOrden_devolucion($params);

            return $data;
        } else {
            return parent::getPaginationData($params);
        }
    }

    /**
     * Genera o ubica documentos de la cotizacion de acuerdo al tipo de documento.
     *
     * @access public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 09/08/13 09:46 AM
     */
    public function EXC_quoteReport() {
        $this->offOutput();
        $id = $this->input->post('id');
        $type_report = $this->input->post('tipo');
        $membrete = $this->input->post('membrete');
        $dir_modo_impresion = ($membrete == 'true' ? 'membrete_on' : 'membrete_off');
        $alvuelo = true;
        if ($id):
            $id = $this->encrypt->decode($id);
            $id_orden_devolucion = $id;
            $id_orden_despacho = $this->refund_model->getDev($id_orden_devolucion);
            $tipo = $this->refund_model->getTipoCliente($id_orden_despacho);
            $tipo_cliente = $tipo[0]['tipo'];
            $id_cotizacion=  $this->refund_model->getCotId($id_orden_despacho);
            
            $data = $this->Cotizacion_model->get_data_cotizacion( $id_cotizacion);
         
            $data[0]['codigo_orden'] = $this->refund_model->getCode($id_orden_devolucion);
            $data = $data[0];
            switch ($type_report) {
                case 'backup':
                    $filename = "uploads/devolucion/{$dir_modo_impresion}/backup/backup_$data[codigo_orden].pdf";
                    if (file_exists($filename) && !$alvuelo):
                        $response = array(
                            'sucess' => true,
                            'file' => $filename
                        );
                    else:
                        if ($tipo_cliente == 'pac') {
                            $getDatosPdf = $this->Cotizacion_model->getDatosPdf($data['id_medico'], $data['id_seguro'], $data['id_paciente'], $data['cat_tipo_pago'], $data['id_vendedor']);
                            $datos_paciente = $this->paciente_model->getPaciente($data['id_paciente'], 'tlf_movil');
                        } elseif ($tipo_cliente == 'emp' || $tipo_cliente == 'per') {
                            $DatosPdf = $this->refund_model->getDatosPDF($id_orden_despacho, $tipo_cliente);
                            $getDatosPdf = $DatosPdf[0];
                            //print_r($getDatosPdf);die;
                        }
                        $params_pdf = array(
                            'codigo_orden' => $data['codigo_orden'],
                            'nro_cotizacion' => (!isset($data['codigo']) ? 'No Aplica' : $data['codigo']),
                            'fecha_emision' => date('d/m/Y'),
                            'forma_pago' => (!isset($getDatosPdf['tipo_pago']) ? 'No Aplica' : $getDatosPdf['tipo_pago']),
                            'tiempo_entrega' => 'INMEDIATA',
                            'vendedor' => (!isset($getDatosPdf['vendedor']) ? 'No Aplica' : $getDatosPdf['vendedor']),
                            'dr_tratante' => (!isset($getDatosPdf['medico']) ? 'No Aplica' : $getDatosPdf['medico']),
                            'cliente' => (!isset($getDatosPdf['pnombre']) ? 'No Aplica' : $getDatosPdf['pnombre']),
                            'rif' => (!isset($getDatosPdf['pcedula']) ? 'No Aplica' : $getDatosPdf['pcedula']),
                            'nit' => '',
                            'seguro' => (!isset($getDatosPdf['nseguro']) ? 'No Aplica' : $getDatosPdf['nseguro']),
                            'tlf' => (empty($datos_paciente[0]['tlf_movil']) || $datos_paciente[0]['tlf_movil'] == null ? 'Sin Informacion' : $datos_paciente[0]['tlf_movil']),
                        );

                        $params_pdf['cuerpo_tabla'] = $this->refund_model->getArticulosByPdf($id_orden_devolucion, 'MAT', '1');
                        $response = array(
                            'sucess' => true,
                            'file' => $this->_create_document($params_pdf, false, 'backup', $membrete)
                        );
                    endif;

                    break;
                case 'instrumento':
                    $filename = "uploads/devolucion/{$dir_modo_impresion}/instrumentos/instru_$data[codigo_orden].pdf";
                    if (file_exists($filename) && !$alvuelo):
                        $response = array(
                            'sucess' => true,
                            'file' => $filename
                        );
                    else:

                        if ($tipo_cliente == 'pac') {
                            $getDatosPdf = $this->Cotizacion_model->getDatosPdf($data['id_medico'], $data['id_seguro'], $data['id_paciente'], $data['cat_tipo_pago'], $data['id_vendedor']);
                            $datos_paciente = $this->paciente_model->getPaciente($data['id_paciente'], 'tlf_movil');
                        } elseif ($tipo_cliente == 'emp' || $tipo_cliente == 'per') {
                            $DatosPdf = $this->refund_model->getDatosPDF($id_orden_despacho, $tipo_cliente);
                            $getDatosPdf = $DatosPdf[0];
                            //print_r($getDatosPdf);die;
                        }
                        $params_pdf = array(
                            'codigo_orden' => $data['codigo_orden'],
                            'nro_cotizacion' => (!isset($data['codigo']) ? 'No Aplica' : $data['codigo']),
                            'fecha_emision' => date('d/m/Y'),
                            'forma_pago' => (!isset($getDatosPdf['tipo_pago']) ? 'No Aplica' : $getDatosPdf['tipo_pago']),
                            'tiempo_entrega' => 'INMEDIATA',
                            'vendedor' => (!isset($getDatosPdf['vendedor']) ? 'No Aplica' : $getDatosPdf['vendedor']),
                            'dr_tratante' => (!isset($getDatosPdf['medico']) ? 'No Aplica' : $getDatosPdf['medico']),
                            'cliente' => (!isset($getDatosPdf['pnombre']) ? 'No Aplica' : $getDatosPdf['pnombre']),
                            'rif' => (!isset($getDatosPdf['pcedula']) ? 'No Aplica' : $getDatosPdf['pcedula']),
                            'nit' => '',
                            'seguro' => (!isset($getDatosPdf['nseguro']) ? 'No Aplica' : $getDatosPdf['nseguro']),
                            'tlf' => (empty($datos_paciente[0]['tlf_movil']) || $datos_paciente[0]['tlf_movil'] == null ? 'Sin Informacion' : $datos_paciente[0]['tlf_movil']),
                        );

                        $params_pdf['cuerpo_tabla'] = $this->refund_model->getArticulosByPdf($id_orden_devolucion, 'INS');
                        $response = array(
                            'sucess' => true,
                            'file' => $this->_create_document($params_pdf, false, 'instrumento', $membrete)
                        );
                    endif;
                    break;
                case 'materiales':
                    $filename = "uploads/devolucion/{$dir_modo_impresion}/cot_$data[codigo_orden].pdf";
                    if (file_exists($filename) && !$alvuelo):
                        $response = array(
                            'sucess' => true,
                            'file' => $filename
                        );
                    else:
                        if ($tipo_cliente == 'pac') {
                            $getDatosPdf = $this->Cotizacion_model->getDatosPdf($data['id_medico'], $data['id_seguro'], $data['id_paciente'], $data['cat_tipo_pago'], $data['id_vendedor']);
                            $datos_paciente = $this->paciente_model->getPaciente($data['id_paciente'], 'tlf_movil');
                        } elseif ($tipo_cliente == 'emp' || $tipo_cliente == 'per') {
                            $DatosPdf = $this->refund_model->getDatosPDF($id_orden_despacho, $tipo_cliente);
                            $getDatosPdf = $DatosPdf[0];
                            //print_r($getDatosPdf);die;
                        }
                        $params_pdf = array(
                            'codigo_orden' => $data['codigo_orden'],
                            'nro_cotizacion' => (!isset($data['codigo']) ? 'No Aplica' : $data['codigo']),
                            'fecha_emision' => date('d/m/Y'),
                            'forma_pago' => (!isset($getDatosPdf['tipo_pago']) ? 'No Aplica' : $getDatosPdf['tipo_pago']),
                            'tiempo_entrega' => 'INMEDIATA',
                            'vendedor' => (!isset($getDatosPdf['vendedor']) ? 'No Aplica' : $getDatosPdf['vendedor']),
                            'dr_tratante' => (!isset($getDatosPdf['medico']) ? 'No Aplica' : $getDatosPdf['medico']),
                            'cliente' => (!isset($getDatosPdf['pnombre']) ? 'No Aplica' : $getDatosPdf['pnombre']),
                            'rif' => (!isset($getDatosPdf['pcedula']) ? 'No Aplica' : $getDatosPdf['pcedula']),
                            'nit' => '',
                            'seguro' => (!isset($getDatosPdf['nseguro']) ? 'No Aplica' : $getDatosPdf['nseguro']),
                            'tlf' => (empty($datos_paciente[0]['tlf_movil']) || $datos_paciente[0]['tlf_movil'] == null ? 'Sin Informacion' : $datos_paciente[0]['tlf_movil']),
                        );
                        $getMaterialesPdf = $this->refund_model->getArticulosByPdf($id_orden_devolucion, 'MAT');
                        $materiales = array();
                        if ($getMaterialesPdf):
                            foreach ($getMaterialesPdf as $clave => $valor):

                                $materiales[] = array(
                                    'lote_m' => $valor['lote'],
                                    'codigo_m' => $valor['id'],
                                    'modelo_m' => $valor['modelo'],
                                    'descripcion_m' => $valor['descripcion'],
                                    'gart_m' => '',
                                    'cant_m' => $valor['cantidad'],
                                        /* 'precio_m' => number_format($valor['precio'], 2, ',', '.'),
                                          'neto_m' => number_format($valor['precio'] * $valor['cantidad'], 2, ',', '.') */
                                );
                            //  $monto_subtotal = $monto_subtotal + ($valor['precio'] * $valor['cantidad']);
                            endforeach;
                        endif;

                        $params_pdf['materiales'] = $materiales;
                        $response = array(
                            'sucess' => true,
                            'file' => $this->_create_document($params_pdf, false, 'materiales', $membrete)
                        );
                    endif;
                    break;
                default:
                    $params_pdf['cuerpo_tabla'] = array();
                    break;
            }
        else:
            $response = array(
                'sucess' => false
            );
        endif;
        echo json_encode($response);
    }

    /**
     * Se encarga de definir los parametros de creacion de pdf
     *
     * @author  Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version 1.0 14/11/13 10:19 AM
     */
    public function _create_document($params = array(), $background_no_validate = FALSE, $document = '', $membrete = FALSE) {
        $this->lang->load('app_doc');
        $this->lang->load('app_email');
        $this->load->library('encrypt');
        $this->load->helper('view_engine_helper');
        extract($params);
        $dir_modo_impresion = ($membrete == 'true' ? 'membrete_on' : 'membrete_off');
        $dir_modo_impresion = '';
        switch ($document) {
            case 'materiales':
                $view = 'delivery/refund/pdf_materiales_devol';
                $name = 'mat_devol';
                $dir = 'devolucion/materiales';
                break;
            case 'backup':
                $view = 'delivery/refund/pdf_backup_devol';
                $name = 'backup_devol';
                $dir = 'devolucion/backup';
                break;
            case 'instrumento':
                $view = 'delivery/refund/pdf_instrumentos_devol';
                $name = 'instru_devol';
                $dir = 'devolucion/instrumentos';
                break;

            default:
                $view = 'pdf_materiales_devol';
                $name = 'ord';
                break;
        }
        //Cadena que contiene el string para generar el pdf
        $html = $this->load->view($view, $params, TRUE);
        // -----------------------------
        // -----------------------------
        //Asignacion de nombre, ruta y generacion del documento
        $name_file = "{$name}_{$codigo_orden}";
        $destination_path = "uploads/{$dir}/";
        //$destination_path = ($background_no_validate ? $destination_path . "preview/" : $destination_path);

        generate_pdf($name_file, $destination_path, 'F', $html, $background_no_validate, $membrete);
        return utf8_decode("{$destination_path}" . "{$name_file}.pdf");
        // -----------------------------
    }

    /**
     * Se encarga de mostrar los detalles de orden de devolucion
     *
     * @author  Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version 1.0 14/11/13 10:19 AM
     */
    function detail() {

        $id_ec = $this->input->post('id');

        //  $this->setTargetOn('window2');
        $id = $this->encrypt->decode($id_ec);
        //$id=  $this->refund_model->getDev($id_dev);
        // print_r($id);die;
        if (!empty($id)) {
            $id_orden_despacho = $this->refund_model->getDev($id);
            $data['orden'] = $this->delivery_model->getDataDelivery($id_orden_despacho);
            if (empty($data['orden'])) {
                $data['orden'] = $this->delivery_free_model->getDataDelivery($id_orden_despacho);
            }
            $data['cotizacion'] = $this->Cotizacion_model->getById($id);
//            $data['materiales'] = $this->Cotizacion_model->getArticulosByCot($id, 'MAT');
            $data['materiales'] = $this->refund_model->getArticlesDet($id, 'MAT', '0');
            $data['backup'] = $this->refund_model->getArticlesDet($id, 'MAT', '1');
            $data['instrumentos'] = $this->refund_model->getArticlesDet($id, 'INS', '0');
            $data['cotizacion']['id'] = $id;

            //Arreglo de datos para la pestana datos
            $tabs = array(
                '1-dev' => array('view' => 'particular', 'label' => 'Datos', 'params' => array('file' => 'delivery/refund/_datos', 'data' => array('operation' => '', 'data' => $data['orden']))),
                '2-dev' => array('view' => 'particular', 'label' => 'Materiales', 'params' => array('file' => 'delivery/refund/_materiales', 'data' => array('operation' => '', 'materiales' => $data['materiales']))),
                '3-dev' => array('view' => 'particular', 'label' => 'Instrumental', 'params' => array('file' => 'delivery/refund/_instrumentos', 'data' => array('operation' => '', 'instrumentos' => $data['instrumentos']))),
                '4-dev' => array('view' => 'particular', 'label' => 'Backup', 'params' => array('file' => 'delivery/refund/_backup_material', 'data' => array('operation' => '', 'backup' => $data['backup']))),
                '5-dev' => array('view' => 'particular', 'label' => 'Resumen Nota de Entrega', 'params' => array('file' => 'delivery/refund/_resumen', 'data' => array('operation' => '', 'data' => $data['cotizacion'])))
            );

            $view['view'] = $this->view_engine->tab($tabs);
            $this->setContent($this->load->view('delivery/delivery/detail', $view, TRUE));
        }
    }

    function EXC_selectCotizacion() {
        $id = $this->input->post('id_paciente');
        $tipo = $this->input->post('tipo');

         if (!isset($tipo)) {
            $tipo_cliente = $this->refund_model->getTipoCliente($id);
            $tipo = $tipo_cliente[0]['tipo'];
        }
        if ($tipo == 'pac') {
            $cotizacion_aprobada = $this->categories_model->getCategory(array('_table' => 'cotizacion_estatus', '_label' => 'Aprobada'), ' id ');
            $cotizacion_aprobada = $cotizacion_aprobada[0]['id'];
            $data = $this->refund_model->getCotizacionByPaciente($id, $cotizacion_aprobada);
        } else if ($tipo == 'emp') {

            $data = $this->refund_model->getLibreByCliente($id);
        } elseif ($tipo == 'per') {
            $data = $this->refund_model->getLibreByCliente($id);
        }
        $resp = array(
            'success' => TRUE,
            'data' => $this->load->view('delivery/delivery/_option_paciente', array('data' => $data), TRUE));
        $this->offOutput();
        echo json_encode($resp);
    }

}