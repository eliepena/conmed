<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Refund_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad Refund
 * y sus entidades dependientes o extendidas
 *
 * @package Modules
 * @category Model
 * @version V-1.0 29/11/12 01:26 PM
 */
class Refund_model extends AST_Model {

    public function __construct() {

        parent::__construct();
        $this->table_name = 'orden_devolucion';
    }

    /**
     * Obtiene los registros de las estantes asociados a una orden de devolucion
     *
     * @access  public
     * @param     array    $params    filtros para la consulta
     * @return  array    orden de devolucion

     */
    public function getOrden_devolucion($params) {
        extract($params);
        $this->setOrden_devolucion($id);

        $search = $this->filterSearchFields($search_fields);

        return parent::getAll($offset, $search, $sort);
    }

    /**
     * Cuenta el numero total de estantes para un almacen
     *
     * @access  public
     * @param   array    $params    filtros de la consulta
     * @return  integer    total de registros

     */
    public function countOrden_devolucion($params) {
        extract($params);
        $search = $this->filterSearchFields($search_fields);

        $this->setOrden_devolucion($id);
        return parent::getCount($search);
    }

    /**
     * obtiene los registros de estante para paginarlos
     *
     * @access  public
     * @param   integer    $id    filtros de la consulta
     *

     */
    private function setOrden_devolucion(&$id) {
         $tipo_orden = $this->categories_model->getCategory(array('_table' => 'tipo_despacho', '_label' => 'Libre'), ' id ');
        $t_orden_despacho = 'business.orden_despacho';
        $t_orden_despacho_estatus = $this->getSchema() . '.orden_despacho_estatus';
        $t_persona = $this->getSchema() . '.persona';
        $t_cirugia = $this->getSchema() . '.cirugia';
        $t_empleado = $this->getSchema() . '.empleado';
        $t_empresa = $this->getSchema() . '.empresa';
        $t_category = 'dynamic.category';

        //Select
        $this->db->select("$this->table_name.id");
        $this->db->select("$this->table_name.created_at as fecha_devolucion");
        $this->db->select("COALESCE($t_persona.primer_nombre || ' ' || $t_persona.primer_apellido , $t_empresa.nombre) as primer_nombre", FALSE);
        $this->db->select("$t_orden_despacho.codigo");
        $this->db->select("$t_orden_despacho.fecha_solicitud");
        $this->db->select("$t_orden_despacho.fecha_despacho");
        $this->db->select("$t_category._label as cat_estatus_orden_despacho");
        $this->db->select("$t_empleado.primer_nombre || ' ' || $t_empleado.primer_apellido AS id_despachador");

        //From and join
        //$this->db->from("$t_orden_despacho");
        $this->db->join("$t_orden_despacho","$t_orden_despacho.id= $this->table_name.id_orden_despacho");
        $this->db->join("$t_orden_despacho_estatus", "$t_orden_despacho.id=$t_orden_despacho_estatus.id_orden_despacho");
        $this->db->join("$t_category", "$t_orden_despacho_estatus.cat_orden_despacho_estatus=$t_category.id");
        $this->db->join("$t_empleado", "$t_orden_despacho.id_despachador=$t_empleado.id left ");
        $this->db->join("$t_persona", "$t_orden_despacho.id_cliente=$t_persona.id left ");
        $this->db->join("$t_empresa", "$t_orden_despacho.id_cliente=$t_empresa.id");
        if (!empty($params)) {
            $value = $params['codigo'];

            unset($params['id_despachador']);
            unset($params['cat_estatus_orden_despacho']);
            unset($params['codigo']);

            $params[" CAST($t_empleado.primer_nombre || ' ' || $t_empleado.primer_apellido AS TEXT) "] = $value;
            $params[" CAST($t_category._label AS TEXT) "] = $value;
            $params[" CAST($t_orden_despacho.codigo AS TEXT) "] = $value;
            $params[" to_char($t_orden_despacho.fecha_solicitud,'DD-MM-YYYY HH:MI PM') "] = $value;
            $params[" to_char($t_orden_despacho.fecha_despacho,'DD-MM-YYYY HH:MI PM') "] = $value;
            $params[" CAST($t_orden_despacho.id AS TEXT) "] = $value;

            $this->_ilike($params, 'OR', TRUE);
        }

        $this->db->where("$t_orden_despacho._delete", '0');
//        if ($tipo_orden):
//            $this->db->where("$t_orden_despacho.cat_tipo_despacho", $tipo_orden[0]['id']);
//        endif;
    }

    /**
     * Filtra los datos para el listado de estante
     *
     * @access  private
     * @param     array    $search    Search Fields
     * @return  array    Search Fields Filtrados

     */
    private function filterSearchFields($search) {
        $uri = NULL;
        $uri = $this->uri->uri_string();
        //print_r($uri);
//        if (strpos($uri, 'listOrden_devolucion')) {
//
//            return $search;
//        }
    }

    /**
     * obtiene el identificador de la cotizacion a la que corresponde la orden de despacho dado 
     * su id
     *
     * @access  public
     * @param   integer    $id    identificador de orden de despacho 
     * @return  row  id_cotizacion  identificador de la cotizacion

     */
    function getCotId($id) {
        $this->db->select('id_cotizacion');
        $this->db->from('business.orden_despacho');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $data = $query->result_array();
        return ($query->num_rows() > 0) ? ($query->row()->id_cotizacion) : false;
    }

    /**
     * obtiene el identificador de la  orden de despacho a la que corresponde la cotizacion dado 
     * su id
     *
     * @access  public
     * @param   integer    $id  identificador de la cotizacion  
     * @return  row  id identificador de orden de despacho 

     */
    function getDelId($id) {
        $this->db->select('id');
        $this->db->from('business.orden_despacho');
        $this->db->where('id_cotizacion', $id);
        $query = $this->db->get();
        $data = $query->result_array();
        return ($query->num_rows() > 0) ? ($query->row()->id) : false;
    }

    /**
     * obtiene el identificador de la  orden de despacho a la que corresponde la devolucion dado 
     * su id
     *
     * @access  public
     * @param   integer    $id  identificador de la cotizacion  
     * @return  row  id identificador de orden de despacho 

     */
    function getDev($id) {
        $this->db->select('id_orden_despacho as id');
        $this->db->from('business.orden_devolucion');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $data = $query->result_array();
        return ($query->num_rows() > 0) ? ($query->row()->id) : false;
    }

    /**
     * obtiene el identificador de la  orden de devolucion a la que corresponde la orden de despacho  dado 
     * su id
     *
     * @access  public
     * @param   integer    $id  identificador de orden de despacho 
     * @return  row  id identificador de orden de devolucion 

     */
    function getDevId($id) {
        $this->db->select('id');
        $this->db->from('business.orden_devolucion');
        $this->db->where('id_orden_despacho', $id);
        $query = $this->db->get();
        $data = $query->result_array();
        return ($query->num_rows() > 0) ? ($query->row()->id) : false;
    }

    /**
     * Provee las implementacion para realizar inserciones dentro de orden_devolucion
     * y las entidades dependientes de ella
     *
     * @access  public
     * @param   Array  $params datos a ser guardados dentro de la base de datos.
     * @return  Boolean Indicando el resultado de la operacion.

     */
    function insert($params) {
        $this->db->trans_start();
        $data = array('id_orden_despacho' => $params['id_orden_despacho'], 'created_by' => $this->session->userdata('user_id'));
        $get_data_order = $this->insertWithReturning($data, 'business.orden_devolucion');

        if (!empty($params['id_lote'])) {
            foreach ($params['id_lote'] as $key => $value) {

                foreach ($value as $k => $val) {
                    $lote[$k]['id_articulo'] = $key;
                    $lote[$k]['id_lote'] = $k;
                    $lote[$k]['backup'] = '0';
                    $lote[$k]['created_by'] = $this->session->userdata('user_id');
                    $lote[$k]['id_orden_devolucion'] = $get_data_order['id'];
                }
            }
            foreach ($params['cant_mat'] as $key => $value) {

                foreach ($value as $k => $val) {
                    $lote[$k]['cantidad'] = $val;
                }
            }
            foreach ($lote as $key => $value) {
                if ($key == 0)
                    unset($lote[$key]);
                if ($lote[$key]['cantidad'] == 0) {
                    unset($lote[$key]);
                }
            }
            if ($lote) {
                $this->db->insert_batch('business.orden_devolucion_articulos', $lote);
            }
        }
        if (!empty($params['ins_lote'])) {
            foreach ($params['ins_lote'] as $key => $value) {

                foreach ($value as $k => $val) {
                    $instru[$k]['id_articulo'] = $key;
                    $instru[$k]['id_lote'] = $k;
                    $instru[$k]['backup'] = '0';
                    $instru[$k]['created_by'] = $this->session->userdata('user_id');
                    $instru[$k]['id_orden_devolucion'] = $get_data_order['id'];
                }
            }
            foreach ($params['cant_instru'] as $key => $value) {

                foreach ($value as $k => $val) {
                    $instru[$k]['cantidad'] = $val;
                }
            }
            foreach ($instru as $key => $value) {
                if ($key == 0)
                    unset($instru[$key]);
                if ($instru[$key]['cantidad'] == 0) {
                    unset($instru[$key]);
                }
            }

            if ($instru) {
                $this->db->insert_batch('business.orden_devolucion_articulos', $instru);
            }
        }
        if (!empty($params['back_lote'])) {
            foreach ($params['back_lote'] as $key => $value) {

                foreach ($value as $k => $val) {
                    $backup[$k]['id_articulo'] = $key;
                    $backup[$k]['id_lote'] = $k;
                    $backup[$k]['backup'] = '1';
                    $backup[$k]['created_by'] = $this->session->userdata('user_id');
                    $backup[$k]['id_orden_devolucion'] = $get_data_order['id'];
                }
            }
            foreach ($params['cantidad-backup'] as $key => $value) {

                foreach ($value as $k => $val) {
                    $backup[$k]['cantidad'] = $val;
                }
            }
            foreach ($backup as $key => $value) {
                if ($key == 0)
                    unset($backup[$key]);
                if ($backup[$key]['cantidad'] == 0) {
                    unset($backup[$key]);
                }
            }
            if ($backup) {
                $this->db->insert_batch('business.orden_devolucion_articulos', $backup);
            }
        }

        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    /**
     * Obtiene datos de los articulos dado el identificador de la orden de devolucion
     *
     * @access  public
     * @param   integer  $id identificador de la orden 
     * @param   String  $tipo categoria de articulo 
     * @param   bit     $back indicador de tipo backup 
     * @return  array    datos del vendedor
     * @author  Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version V 1.0 
     */
    function getArticles($id, $tipo, $back = NULL) {
        $this->db->distinct();
        $this->db->select('
            "da"."id_articulo" as id,
            count("da"."id_articulo" ) as cantidad,
            "ta"."id" as id_lote, "a"."modelo",
            "a"."descripcion", 
            "ta"."lote", 
            "cat"."_label" as tipo_articulo ');
        $this->db->from('business.orden_despacho od');
        $this->db->join('business.orden_despacho_articulos da', 'da.id_orden_despacho=od.id');
        $this->db->join('business.articulo a', "da.id_articulo=a.id");
        $this->db->join('dynamic.category cat', 'a.tipo_articulo=cat.id');
        $this->db->join('business.tramo_articulo ta', "ta.id_articulo=a.id", 'left');
        $this->db->where('od.id', $id);
        $this->db->where('cat._label', $tipo);
        $this->db->where('da.backup', $back);
        $this->db->group_by('"da"."id_articulo" , 
            "ta"."id", "a"."modelo",
            "a"."descripcion", 
            "ta"."lote", 
            "cat"."_label"');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

    /**
     * Obtiene datos de los articulos dado el identificador de la orden de devolucion
     *
     * @access  public
     * @param   integer  $id identificador de la orden 
     * @param   String  $tipo categoria de articulo 
     * @param   bit     $back indicador de tipo backup 
     * @return  array    datos de los articulos para detalle
     * @author  Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version V 1.0 
     */
    function getArticlesDet($id, $tipo, $back = NULL) {
        $this->db->distinct();
        $this->db->select('
            "da"."id_articulo" as id,
            "da".cantidad,
            "ta"."id" as id_lote, "a"."modelo",
            "a"."descripcion", 
            "ta"."lote", 
            "cat"."_label" as tipo_articulo ');
        $this->db->from('business.orden_devolucion od');

        $this->db->join('business.orden_devolucion_articulos da', 'da.id_orden_devolucion=od.id');
        $this->db->join('business.articulo a', "da.id_articulo=a.id");
        $this->db->join('business.orden_despacho d', 'od.id_orden_despacho=d.id');
        $this->db->join('dynamic.category cat', 'a.tipo_articulo=cat.id');
        $this->db->join('business.tramo_articulo ta', "ta.id=da.id_lote", 'left');
        $this->db->where('od.id', $id);
        $this->db->where('cat._label', $tipo);
        $this->db->where('da.backup', $back);
        $this->db->group_by('"da"."id_articulo" , 
            "ta"."id", "a"."modelo","da".cantidad,
            "a"."descripcion", 
            "ta"."lote", 
            "cat"."_label"');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

    /**
     * obtiene el identificador del lote al que corresponde un articulo dado 
     * su id
     *
     * @access  public
     * @param   integer    $id_articulo  identificador de articulo 
     * @return  array  data  datos del lote 
     * @author  Eliezer Pe#a <pe.eliezer@gmail.com>
     */
    function getLote($id_articulo) {
        $this->db->select('id');
        $this->db->select('lote');
        $this->db->from('business.articulo_tramo');
        $this->db->where('id_articulo', $id_articulo);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

    /**
     * Obtiene los articulos asociados a una orden de despacho
     *
     * @access  public
     * @param   integer $id_cotizacion  Identificador de la cotizacion
     * @param   string  $category  Tipo de categoria
     * @param   string  $backup  Determina si el articulo es un backup
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 14/11/13 02:29 PMM
     */
    function getArticulosByPdf($id_orden, $category, $backup = '0') {

        if ($id_orden && $category):
            //Tables
            $cotizacion_articulo = $this->getSchema() . '.cotizacion_articulo';
            $articulo = $this->getSchema() . '.articulo';
            $cotizacion = 'business.cotizacion';
            $t_tramo_articulo = 'business.tramo_articulo';
            $orden_devolucion_articulos = $this->getSchema() . '.orden_devolucion_articulos';
            $orden_devolucion = $this->getSchema() . '.orden_devolucion';
            $table_category = 'dynamic.category';

            //Query
            $this->db->select("$orden_devolucion_articulos.id_lote", false);
            $this->db->select("$t_tramo_articulo.lote");
            $this->db->select($articulo . '.id');
            $this->db->select("$orden_devolucion_articulos .cantidad as cantidad", FALSE);
            $this->db->select($articulo . '.modelo');
            $this->db->select($articulo . '.descripcion');
            $this->db->select($articulo . '.imagen');

            $this->db->from($orden_devolucion_articulos);
            $this->db->join($orden_devolucion, "$orden_devolucion_articulos.id_orden_devolucion=$orden_devolucion.id");
            $this->db->join($articulo, "$articulo.id=$orden_devolucion_articulos.id_articulo");
            $this->db->join($table_category, "$articulo.tipo_articulo=$table_category.id  ");
            $this->db->join($t_tramo_articulo, "$orden_devolucion_articulos.id_lote=$t_tramo_articulo.id  ");
            if ($category == 'ALMACEN'):
                $this->db->where(
                        array(
                            "$orden_devolucion_articulos.id_orden_devolucion" => $id_orden,
                            "$table_category._table" => 'tipo_articulo'));
            else:
                $this->db->where(
                        array(
                            "$orden_devolucion_articulos.id_orden_devolucion" => $id_orden,
                            "$orden_devolucion_articulos.backup" => $backup,
                            "$table_category._label" => $category,
                            "$table_category._table" => 'tipo_articulo'));
            endif;
            $this->db->group_by("$orden_devolucion_articulos.id_lote");
            $this->db->group_by("$t_tramo_articulo.lote");
            $this->db->group_by($articulo . '.id'); // $this->db->group_by($cotizacion_articulo . '.precio');
            $this->db->group_by($orden_devolucion_articulos . '.cantidad');
            $this->db->group_by($articulo . '.modelo');
            $this->db->group_by($articulo . '.descripcion');
            $this->db->group_by($articulo . '.imagen');
            $query = $this->db->get();
            return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;

        else:
            return FALSE;
        endif;
    }

    /**
     * Obtiene el codigo en la tabla orden_devolucion
     *
     * @access  public
     * @return  string  codigo maximo
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V - 1.0 09/10/13 11:54 AM
     */
    function getCode($id) {
        $this->db->select('codigo');
        $this->db->from('business.orden_devolucion');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->row()->codigo) : false;
    }

    /**
     * Retorna los valores de una consulta a una entidad filtrado por determinados campos
     *
     * @access  public
     * @param   string $table nombre del campo identificador de la tabla en donde se consultara
     * @param   string $fields cadena que contiene los campos separados por coma
     * @param   string $filter cadena que contiene el filtro que se desea para los campos buscados
     * @param   string $label cadena que define como se llamara el campo label en el select, por defecto text
     * @return  array Arreglo con los valores de la consulta
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V - 1.0 09/10/13 11:54 AM
     */
    public function getComboByTable($table, $fields, $filter = FALSE, $label = 'text', $status_cotizacion = array()) {
        if (!$fields)
            return array();

        if ($filter) {
            $arr_fields = explode(',', $fields);
            foreach ($arr_fields as $key => $arr) {
                unset($arr_fields[$key]);
                $arr_fields[$arr] = $filter;
            }
        }

        //limpia los campos table y field para realizar la consulta
        $table = preg_replace('/^id_/i', '', $table);

        //Determina si la cadena de campos contiene el esquema de la tabla, de ser asi, extrae el esquema y los campos
        //si no, deja por defecto business, este esquema debe estar definido al principio y separado por |
        $schema = (strstr($fields, '|')) ? substr($fields, 0, strrpos($fields, '|')) : 'business';
        $fields = (strstr($fields, '|')) ? substr($fields, strrpos($fields, '|') + 1, strlen($fields)) : $fields;

        //Concatena los campos que se fijaron en la BD por el caracter (,)
        $fields = preg_replace('/,/i', " || ' ' || ", $fields);

        $this->db->select("$fields  AS $label");
        $this->db->select("$schema.$table.id");


        $this->db->from("$schema.$table");

        $this->db->group_by("$schema.$table.codigo");
        $this->db->group_by("$schema.$table.id");
        $this->db->where("id not in(select id_orden_despacho from business.orden_devolucion where _delete='0')");
        if ($filter)
            $this->_ilike($arr_fields, 'OR', TRUE);

        $query = $this->db->get();

        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    /**
     * busca los pacientes para cargar el select
     *
     * @access  public
     * @return arreglo de registros a cargar en el select
     * @author  Eliezer Pe#a
     * @version 1.0 20/12/2013
     */
    function searchCostumer() {

        $query = $this->db->query("select distinct
	pac.id,  pac.cedula || ' ' || pac.primer_nombre || ' ' ||pac.primer_apellido as text, 'pac' as tipo

	from business.paciente pac
	 join business.cotizacion as cot on cot.id_paciente=pac.id
	join business.cotizacion_estatus cot_es on cot.id=cot_es.id_cotizacion and cat_cotizacion_estatus=93
	where cot.id in (SELECT od.id_cotizacion FROM business.orden_despacho od WHERE _delete='0' 
        and od.id not in(select id_orden_despacho from business.orden_devolucion where _delete='0'))

        union 

        select distinct
	emp.id,  emp.rif || ' ' || emp.nombre as text, 'emp' as tipo

	from business.empresa emp
	join business.orden_despacho as or_des on emp.id=or_des.id_cliente
	join business.cotizacion as cot on cot.id=or_des.id_cotizacion
	join business.cotizacion_estatus cot_es on cot.id=cot_es.id_cotizacion and cat_cotizacion_estatus=93
	where cot.id  in (SELECT id_cotizacion FROM business.orden_despacho od WHERE _delete='0' 
        and od.id not in(select id_orden_despacho from business.orden_devolucion where _delete='0'))

        union 

        select distinct
	per.id,  per.cedula || ' ' || per.primer_nombre || ' ' ||per.primer_apellido as text, 'per' as tipo

	from business.persona per
	join business.orden_despacho as or_des on per.id=or_des.id_cliente
	join business.libre_cotizacion as cot on or_des.id_cotizacion=cot.id
        where cot.id in (SELECT id_cotizacion FROM business.orden_despacho od WHERE _delete='0'
        and od.id not in(select id_orden_despacho from business.orden_devolucion where _delete='0'))");
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    /**
     * returna el arreglo de cotizaciones asociadas a un paciente dado
     *
     * @access  public
     * @param integer $id_paciente contiene el identificador del paciente
     * @param integer $estatus contiene el codigo del estatus de la cotizacion
     * @return arreglo de registros a cargar en el select
     * @author  Eliezer Pe#a
     * @version 1.0 20/12/2013
     */
    function getCotizacionByPaciente($id_paciente, $estatus) {
        $this->db->distinct();
        $this->db->select('des.id');
        $this->db->select('des.codigo');
        $this->db->select('cot.id_medico');
        $this->db->select('cot.id_vendedor');
        $this->db->from('business.orden_despacho des');
        $this->db->join('business.cotizacion cot', 'des.id_cotizacion=cot.id');
        $this->db->join('business.cotizacion_estatus cot_es', 'cot.id=cot_es.id_cotizacion');
        $this->db->where('cot.id_paciente', $id_paciente);
        $this->db->where('cot_es.cat_cotizacion_estatus', $estatus);
        $this->db->where("cot.id in (SELECT id_cotizacion FROM business.orden_despacho WHERE _delete='0') ");
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    /**
     * returna el arreglo de notas de devolucion asociadas a un cliente dado
     *
     * @access  public
     * @param integer $id_paciente contiene el identificador del cliente
     * @return arreglo de registros a cargar en el select
     * @author  Eliezer Pe#a
     * @version 1.0 20/12/2013
     */
    function getLibreByCliente($id_cliente) {
        $this->db->distinct();
        $this->db->select('des.id');
        $this->db->select('des.codigo');
        $this->db->from('business.orden_despacho des');
        $this->db->where('des.id_cliente', $id_cliente);
        $this->db->where("des.id not in (SELECT id FROM business.orden_despacho WHERE _delete='0') ");
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    function getDatosGenerales($id, $tipo = NULL) {
        if (!isset($tipo)) {
            
        } elseif ($tipo == 'per') {
            $this->db->select("per.primer_nombre || ' ' || per.primer_apellido AS id_paciente");
            $this->db->select("per.cedula");
            $this->db->select("per.correo_electronico");
            $this->db->from("business.persona per");
            $this->db->where('per.id=(select id_cliente from business.orden_despacho where id=' . $id . ')');
            $query = $this->db->get();
            return ($query->num_rows() > 0) ? $query->result_array() : false;
        } elseif ($tipo == 'emp') {
            $this->db->select("emp.nombre AS cliente");
            $this->db->select("emp.rif AS cedula");
            $this->db->select("emp.email AS correo_electronico");
            $this->db->from("business.empresa emp");
            $this->db->where('emp.id=(select id_cliente from business.orden_despacho where id=' . $id . ')');
            $query = $this->db->get();
            return ($query->num_rows() > 0) ? $query->result_array() : false;
        }
    }
    
    
    
    
     function getDatosPDF($id, $tipo = NULL) {
        if (!isset($tipo)) {
            
        } elseif ($tipo == 'per') {
            $this->db->select("per.primer_nombre || ' ' || per.primer_apellido AS pnombre");
            $this->db->select("per.cedula as pcedula");
            $this->db->select("per.correo_electronico");
            $this->db->from("business.persona per");
            $this->db->where('per.id=(select id_cliente from business.orden_despacho where id=' . $id . ')');
            $query = $this->db->get();
            return ($query->num_rows() > 0) ? $query->result_array() : false;
        } elseif ($tipo == 'emp') {
            $this->db->select("emp.nombre AS pnombre ");
            $this->db->select("emp.rif as pcedula");
            $this->db->select("emp.email AS correo_electronico");
            $this->db->from("business.empresa emp");
            $this->db->where('emp.id=(select id_cliente from business.orden_despacho where id=' . $id . ')');
            $query = $this->db->get();
            return ($query->num_rows() > 0) ? $query->result_array() : false;
        }
    }

    function getTipoCliente($id) {
        $query = $this->db->query("select distinct
	 'emp' as tipo
            from business.empresa emp
            join business.orden_despacho as or_des on emp.id=or_des.id_cliente
            join dynamic.category as cat on cat.id=or_des.tipo_cotizacion
            join business.cotizacion as cot on cot.id=or_des.id_cotizacion
            join business.cotizacion_estatus cot_es on cot.id=cot_es.id_cotizacion and cat_cotizacion_estatus=93
            where cot.id not in (SELECT id_cotizacion FROM business.orden_despacho WHERE _delete='0') 
            and emp.id=(select id_cliente from business.orden_despacho where id=$id)and cat.id=126
         union 
          select distinct
          'per' as tipo
            from business.persona per
            join business.orden_despacho as or_des on per.id=or_des.id_cliente
            join dynamic.category as cat on cat.id=or_des.tipo_cotizacion
            left join business.libre_cotizacion as cot on or_des.id_cotizacion=cot.id
            where per.id =(select id_cliente from business.orden_despacho where id=$id) and cat.id=126
         union 
          select distinct
          'pac' as tipo
            from business.persona per
            join business.orden_despacho as or_des on per.id=or_des.id_cliente
            join dynamic.category as cat on cat.id=or_des.tipo_cotizacion
            left join business.libre_cotizacion as cot on or_des.id_cotizacion=cot.id
            where per.id =(select id_cliente from business.orden_despacho where id=$id) and cat.id=127");
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

}