<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Descripcion de  Despacho
 *
 * @package conmed.application
 * @subpackage modules
 * @category controller
 * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V - 1.0 09/10/13 11:54 AM
 */
class Delivery_model extends AST_Model {

    function __construct() {
        parent::__construct();
        $this->setSchema('business');
        $this->init('orden_despacho');
        $this->load->model('settings/categories_model', 'categories_model');
        $this->load->model('cotizacion/validacion_model', 'validacion_model');
        $this->load->model('cotizacion/cotizacion_model', 'cotizacion_model');
    }

    /**
     * Retorna los valores de una consulta a una entidad filtrado por determinados campos
     *
     * @access  public
     * @param   string $table nombre del campo identificador de la tabla en donde se consultara
     * @param   string $fields cadena que contiene los campos separados por coma
     * @param   string $filter cadena que contiene el filtro que se desea para los campos buscados
     * @param   string $label cadena que define como se llamara el campo label en el select, por defecto text
     * @return  array Arreglo con los valores de la consulta
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V - 1.0 09/10/13 11:54 AM
     */
    public function getComboByTable($table, $fields, $filter = FALSE, $label = 'text', $status_cotizacion = array(), $tipo_cotizacion = NULL) {
        if (!$fields)
            return array();

        if ($filter) {
            $arr_fields = explode(',', $fields);
            foreach ($arr_fields as $key => $arr) {
                unset($arr_fields[$key]);
                $arr_fields[$arr] = $filter;
            }
        }

        //limpia los campos table y field para realizar la consulta
        $table = preg_replace('/^id_/i', '', $table);

        //Determina si la cadena de campos contiene el esquema de la tabla, de ser asi, extrae el esquema y los campos
        //si no, deja por defecto business, este esquema debe estar definido al principio y separado por |
        $schema = (strstr($fields, '|')) ? substr($fields, 0, strrpos($fields, '|')) : 'business';
        $fields = (strstr($fields, '|')) ? substr($fields, strrpos($fields, '|') + 1, strlen($fields)) : $fields;

        //Concatena los campos que se fijaron en la BD por el caracter (,)
        $fields = preg_replace('/,/i', " || ' ' || ", $fields);

        $this->db->select("$fields  AS $label");
        $this->db->select("$schema.$table.id");


        $this->db->from("$schema.$table");
        if ($tipo_cotizacion == 'Normal') {
            $this->db->select("'normal' as tipo_cotizacion", FALSE);
            $this->db->join('business.cotizacion_estatus', "$schema.$table.id=cotizacion_estatus.id_cotizacion");
            if (sizeof($status_cotizacion) > 0):
                $this->db->where_in('cotizacion_estatus.cat_cotizacion_estatus', $status_cotizacion);
            endif;

            $this->db->where("$schema.$table.id not in( SELECT id_cotizacion FROM business.orden_despacho WHERE _delete='0' )");
        }else if ($tipo_cotizacion == 'Libre') {
            $this->db->select("'libre' as tipo_cotizacion", FALSE);
            $this->db->join('business.libre_cotizacion_estatus', "$schema.$table.id=libre_cotizacion_estatus.id_cotizacion");
            if (sizeof($status_cotizacion) > 0):
                $this->db->where_in('libre_cotizacion_estatus.cat_libre_cotizacion_estatus', $status_cotizacion);
            endif;

            $this->db->where("$schema.$table.id not in( SELECT id_cotizacion FROM business.orden_despacho WHERE _delete='0' )");
        }

        $this->db->group_by("$schema.$table.codigo");
        $this->db->group_by("$schema.$table.id");

        if ($filter)
            $this->_ilike($arr_fields, 'OR', TRUE);

        $query = $this->db->get();

        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    /**
     * Dado un arreglo se obtiene todas las coincidencias del arreglo por
     * clave/valor, donde clave es el campo la base de datos y valor el valor a
     * ser filtrado.
     *
     * @access  public
     * @param   Array $params <p>Contiene los pares clave valor a ser filtrados.</p>
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V - 1.0 09/10/13 11:54 AM
     */
    function getByFields($params) {
        //tables
        $tipo_orden = $this->categories_model->getCategory(array('_table' => 'tipo_despacho', '_label' => 'Cotizacion'), ' id ');
        $t_orden_despacho = $this->concatSchemaTable();
        $t_orden_despacho_estatus = $this->getSchema() . '.orden_despacho_estatus';
        $t_cotizacion = $this->getSchema() . '.cotizacion';
        $t_libre_cotizacion = $this->getSchema() . '.libre_cotizacion';
        $t_cirugia = $this->getSchema() . '.cirugia';
        $t_empleado = $this->getSchema() . '.empleado';
        $t_category = 'dynamic.category';

        //Select

        $this->db->select("COALESCE($t_cotizacion.id , $t_libre_cotizacion.id) as id", FALSE);
        $this->db->select("COALESCE($t_cotizacion.codigo , $t_libre_cotizacion.codigo) as id_cotizacion", FALSE);
        $this->db->select("$t_orden_despacho.codigo");
        $this->db->select("$t_orden_despacho.fecha_solicitud");
        $this->db->select("$t_orden_despacho.fecha_despacho");
        $this->db->select(" (CASE WHEN $t_cirugia.nombre is null THEN 'NO APLICA' ELSE $t_cirugia.nombre END) as id_cirugia");
        $this->db->select("$t_category._label as cat_estatus_orden_despacho");
        $this->db->select("$t_empleado.primer_nombre || ' ' || $t_empleado.primer_apellido AS id_despachador");

        //From and join
        $this->db->from("$t_orden_despacho left ");
        $this->db->join("$t_cotizacion", "$t_orden_despacho.id_cotizacion=$t_cotizacion.id left ");
        $this->db->join("$t_libre_cotizacion", "$t_orden_despacho.id_cotizacion=$t_libre_cotizacion.id ");
        $this->db->join("$t_orden_despacho_estatus", "$t_orden_despacho.id=$t_orden_despacho_estatus.id_orden_despacho ");
        $this->db->join("$t_category", "$t_orden_despacho_estatus.cat_orden_despacho_estatus=$t_category.id left ");
        $this->db->join("$t_cirugia", "$t_orden_despacho.id_cirugia=$t_cirugia.id ");
        $this->db->join("$t_empleado", "$t_orden_despacho.id_despachador=$t_empleado.id ");
        if (!empty($params)) {
            $value = $params['id_cirugia'];

            unset($params['id_despachador']);
            unset($params['id_cirugia']);
            unset($params['cat_estatus_orden_despacho']);
            unset($params['id_cotizacion']);
            unset($params['codigo']);

            $params[" CAST($t_empleado.primer_nombre || ' ' || $t_empleado.primer_apellido AS TEXT) "] = $value;
            $params[" CAST($t_category._label AS TEXT) "] = $value;
            $params[" CAST($t_orden_despacho.codigo AS TEXT) "] = $value;
            $params[" to_char($t_orden_despacho.fecha_solicitud,'DD-MM-YYYY HH:MI PM') "] = $value;
            $params[" to_char($t_orden_despacho.fecha_despacho,'DD-MM-YYYY HH:MI PM') "] = $value;
            $params[" CAST($t_cirugia.nombre AS TEXT) "] = $value;
            $params[" CAST($t_cotizacion.codigo AS TEXT) "] = $value;
            $params[" CAST($t_libre_cotizacion.codigo AS TEXT) "] = $value;

            $this->_ilike($params, 'OR', TRUE);
        }

        $this->db->where("$t_orden_despacho._delete", '0');
        if ($tipo_orden):
        //$this->db->where("$t_orden_despacho.cat_tipo_despacho", $tipo_orden[0]['id']);
        endif;
    }

    /**
     * Obtiene el maximo codigo a generar en la tabla orden_despacho
     *
     * @access  public
     * @return  string  codigo maximo
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V - 1.0 09/10/13 11:54 AM
     */
    function generateCode() {
        $length = 15;
        $id_entity = $this->getEntityId('orden_despacho');
        $current_value = $this->getCurrentSequentialCounter($id_entity);
        $code = ($current_value > 0) ? $current_value : str_repeat('0', $length);
        return @str_repeat('0', $length - strlen(++$code)) . $code;
    }

    /**
     * Provee las implementacion para realizar inserciones dentro de orden_despacho
     * y las entidades dependientes de ella
     *
     * @access  public
     * @param   Array  $data datos a ser guardados dentro de la base de datos.
     * @return  Boolean Indicando el resultado de la operacion.
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V - 1.0 09/10/13 11:54 AM
     */
    function insert($data) {
        $this->db->trans_start();
        $get_data_orden_despacho = $this->insertWithReturning($data['orden'], $this->concatSchemaTable());
        $id_orden_despacho = $get_data_orden_despacho['id'];
        $orden_mat_articulo = $data['orden_mat_articulo'];
        $orden_ins_articulo = $data['orden_ins_articulo'];
        $orden_backup_articulo = $data['orden_backup_articulo'];
        if ($orden_mat_articulo) {
            foreach ($orden_mat_articulo as $mat => $material)
                $orden_mat_articulo[$mat]['id_orden_despacho'] = $id_orden_despacho;
            $this->db->insert_batch('business.orden_despacho_articulos', $orden_mat_articulo);
        }

        if ($orden_ins_articulo) {
            foreach ($orden_ins_articulo as $ins => $material)
                $orden_ins_articulo[$ins]['id_orden_despacho'] = $id_orden_despacho;
            $this->db->insert_batch('business.orden_despacho_articulos', $orden_ins_articulo);
        }
        if ($orden_backup_articulo) {
            foreach ($orden_backup_articulo as $backup => $material):
                $orden_backup_articulo[$backup]['id_orden_despacho'] = $id_orden_despacho;
                $orden_backup_articulo[$backup]['backup'] = '1';
            endforeach;
            $this->db->insert_batch('business.orden_despacho_articulos', $orden_backup_articulo);
        }



        $this->changeStatus($id_orden_despacho, 'Registrada');

        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    /**
     * Cambia el status de una cotizacion a aprobada, emitida,procesada
     *
     * @access  public
     * @param   integer  $id_cotizacion Identificador de la cotizacion
     * @param   string   $status Nuevo Estatus de la cotizacion, debe ser el valor que esta
     *                  en categoria cotizacion_estatus
     * @param   Boolean $send_email Indica si se enviara email con el cambio de estatus
     * @return  Boolean Resultado de la operacion.
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V - 1.0 09/10/13 11:54 AM
     */
    
    function changeStatus($id, $status, $send_email = FALSE, $destination_path = '') {
        $attachment = '';
        $body = '';
        $status_name = mb_convert_case($status, MB_CASE_TITLE, "UTF-8");
        $status_id = $this->engine_model->getValueByCategory($status_name, '_label');
        $datos = $this->delivery_model->getById($id);
        $id_cotizacion = $datos['id_cotizacion'];
        $id_validacion = $this->validacion_model->getFieldByName('cirugia_aprobada');
        $cat_level_current = $this->engine_model->getValueByCategory('almacenista', '_label');
//        print_r($status_id);die;
        if (!$status_id)
            return FALSE;

        $data = array('id_orden_despacho' => $id, 'cat_orden_despacho_estatus' => $status_id, 'created_by' => $this->session->userdata('user_id'));

        $result_ope = $this->db->insert('business.orden_despacho_estatus', $data);

//        cerrar notificacion a almacenista
        $this->notificationClose($id_cotizacion, $id_validacion, $cat_level_current);

        $this->load->helper('view_engine');
//        if (!$result_ope or !$send_email)


        $code = $this->cotizacion_model->getCode($id_cotizacion);
        $html['datos'] = $this->cotizacion_model->getById($id_cotizacion);
        // print_r($html);die;
        switch ($status_name) {
            case 'Registrada':
             $vendedor_email = $this->cotizacion_model->getEmailVendedor($id_cotizacion);
                $despachador_email = $this->session->userdata('user_email');
                $jefe_despacho = $this->getJefeDespacho();
                $email = array($vendedor_email[0]['correo_electronico'], $despachador_email,$jefe_despacho);
//                $email = 'epena@ast.com.ve'; //cable ;
                $html['html'] = sprintf($this->lang->line('body_des_despachado'), $code);
                $html['title'] = $this->lang->line('subject_despacho');
                $body = $this->load->view('delivery/delivery/email_despacho', $html, TRUE);
                $subject = $this->lang->line('subject_despacho');
                $attachment = $destination_path;
                break;
            default:
                return FALSE;
                break;
        }
        sendEmail($email, $body, $subject, $body, $attachment);
        return $result_ope;
    }

    /**
     * Dado un arreglo se obtiene todas las coincidencias del arreglo por
     * clave/valor, donde clave es el campo la base de datos y valor el valor a
     * ser filtrado.
     *
     * @access  public
     * @param   Array $params <p>Contiene los pares clave valor a ser filtrados.</p>
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V - 1.0 09/10/13 11:54 AM
     */
    function getDataDelivery($id = null) {
        if (!$id)
            return FALSE;
        //tables
        $t_orden_despacho = $this->concatSchemaTable();
        $t_orden_despacho_estatus = $this->getSchema() . '.orden_despacho_estatus';
        $t_cotizacion = $this->getSchema() . '.cotizacion';
        $t_libre_cotizacion = $this->getSchema() . '.libre_cotizacion';
        $t_cirugia = $this->getSchema() . '.cirugia';
        $t_empleado = $this->getSchema() . '.empleado';
        $t_category = 'dynamic.category';

        //Select
        $this->db->select("COALESCE($t_cotizacion.id , $t_libre_cotizacion.id) as id", FALSE);
        $this->db->select("COALESCE($t_cotizacion.codigo , $t_libre_cotizacion.codigo) as id_cotizacion", FALSE);
        $this->db->select("$t_orden_despacho.fecha_solicitud");
        $this->db->select("$t_orden_despacho.fecha_despacho");
        $this->db->select("$t_orden_despacho.codigo");
        $this->db->select("$t_cirugia.nombre as id_cirugia");
        $this->db->select("$t_category._label as cat_estatus_orden_despacho");
        $this->db->select("$t_empleado.primer_nombre || ' ' || $t_empleado.primer_apellido AS id_despachador");
        $this->db->select("$t_orden_despacho.tipo_cotizacion AS tipo_cotizacion");

        //From and join
        $this->db->from("$t_orden_despacho LEFT ");
        $this->db->join("$t_cotizacion", "$t_orden_despacho.id_cotizacion=$t_cotizacion.id LEFT ");
        $this->db->join("$t_libre_cotizacion", "$t_orden_despacho.id_cotizacion=$t_libre_cotizacion.id");
        $this->db->join("$t_orden_despacho_estatus", "$t_orden_despacho.id=$t_orden_despacho_estatus.id_orden_despacho");
        $this->db->join("$t_category", "$t_orden_despacho_estatus.cat_orden_despacho_estatus=$t_category.id LEFT ");
        $this->db->join("$t_cirugia", "$t_orden_despacho.id_cirugia=$t_cirugia.id ");
        $this->db->join("$t_empleado", "$t_orden_despacho.id_despachador=$t_empleado.id");

        $this->db->where("$t_cotizacion.id", $id);
        $this->db->or_where("$t_libre_cotizacion.id", $id);
        $this->db->where("$t_orden_despacho._delete", '0');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    /**
     * Verifica la disponibilidad de articulos de una cotizacion en almacen
     * sin tomar en cuenta los sustituidos e incluyendo los sustitutos
     *
     * @access  public
     * @param   integer $id_cotizacion  Identificador de la cotizacion
     * @param   string  $category  Tipo de categoria
     * @param   string  $backup  Determina si el articulo es un backup
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 29/10/13 03:00 PM
     */
    function availabilityOfItemsByCot($id_cotizacion = NULL) {
        if (!$id_cotizacion)
            return FALSE;
        //Tables
        $t_tramo_articulo = 'business.tramo_articulo';

        //Query
        $this->db->select($t_tramo_articulo . '.id_articulo');
        $this->db->from($t_tramo_articulo);
        $this->db->where("$t_tramo_articulo.id_articulo in
                        (
                        SELECT
                                business.cotizacion_articulo.id_articulo
                        FROM
                                business.cotizacion_articulo
                        WHERE
                                business.cotizacion_articulo.id_parent is  null
                                AND business.cotizacion_articulo.id_cotizacion =  $id_cotizacion
                                AND business.cotizacion_articulo.id_articulo not in (
                                            SELECT
                                                    business.cotizacion_articulo.id_parent
                                            FROM
                                                    business.cotizacion_articulo
                                            WHERE
                                                    business.cotizacion_articulo.id_parent is not null

                                            GROUP BY business.cotizacion_articulo.id_parent
                                                                                    )
		UNION ALL

			SELECT
                                business.cotizacion_articulo.id_articulo
                        FROM
                                business.cotizacion_articulo
                        WHERE
                                business.cotizacion_articulo.id_parent is not null
                                AND business.cotizacion_articulo.id_cotizacion =  $id_cotizacion)
                    ");
        $this->db->group_by($t_tramo_articulo . '.id_articulo');
        $this->db->having("sum($t_tramo_articulo.cantidad) =0", FALSE);


        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

    /**
     * Obtiene los articulos asociados a una orden de despacho
     *
     * @access  public
     * @param   integer $id_orden  Identificador de la orden de despacho
     * @param   string  $category  Tipo de categoria
     * @param   string  $backup  Determina si el articulo es un backup
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 12/09/13 12:22 PM
     */
    function getArticulosByOrden($id_orden, $backup = '0', $tipo = 0, $type_quote = 'cotizacion') {

        if ($id_orden):
            //Tables
            $cotizacion_articulo = $this->getSchema() . ".{$type_quote}_articulo";
            $articulo = $this->getSchema() . '.articulo';
            $cotizacion = "business.{$type_quote}";
            $t_tramo_articulo = 'business.tramo_articulo';
            $orden_despacho_articulos = $this->getSchema() . '.orden_despacho_articulos';
            $orden_despacho = $this->getSchema() . '.orden_despacho';
            $table_category = 'dynamic.category';

            //Query
            $this->db->distinct("$orden_despacho_articulos.id_lote");
            $this->db->select("$orden_despacho_articulos.id_lote", false);
            $this->db->select("$t_tramo_articulo.lote");
            $this->db->select($articulo . '.id');
            //$this->db->select($cotizacion_articulo . '.id as id_cot_articulo');
            $this->db->select($cotizacion_articulo . '.precio');
            $this->db->select("count($cotizacion_articulo .cantidad ) as cantidad", FALSE);
            $this->db->select($cotizacion_articulo . '.precio');
            $this->db->select($articulo . '.modelo');
            $this->db->select($articulo . '.descripcion');
            $this->db->select($articulo . '.imagen');

            $this->db->from($orden_despacho_articulos);
            $this->db->join($cotizacion_articulo, "$cotizacion_articulo.id_articulo=$orden_despacho_articulos.id_articulo");
            $this->db->join($articulo, "$articulo.id=$cotizacion_articulo.id_articulo");
            $this->db->join($cotizacion, "$cotizacion.id=$cotizacion_articulo.id_cotizacion");
            $this->db->join($table_category, "$articulo.tipo_articulo=$table_category.id  ");
            $this->db->join($t_tramo_articulo, "$orden_despacho_articulos.id_lote=$t_tramo_articulo.id  ");


            if ($type_quote == 'cotizacion'):
                $this->db->where(
                        array(
                            "$orden_despacho_articulos.id_orden_despacho" => $id_orden,
                            "$orden_despacho_articulos.backup" => $backup,
                            "$orden_despacho_articulos.tipo_articulo" => $tipo
                ));
            elseif ($type_quote == 'libre_cotizacion'):
                $this->db->where(
                        array(
                            "$orden_despacho_articulos.id_orden_despacho" => $id_orden
                ));
            endif;
            $this->db->group_by("$orden_despacho_articulos.id_lote");
            $this->db->group_by("$t_tramo_articulo.lote");
            $this->db->group_by($articulo . '.id');
            $this->db->group_by($cotizacion_articulo . '.id ');
            $this->db->group_by($cotizacion_articulo . '.precio');
            $this->db->group_by("$cotizacion_articulo .cantidad");
            $this->db->group_by($cotizacion_articulo . '.precio');
            $this->db->group_by($articulo . '.modelo');
            $this->db->group_by($articulo . '.descripcion');
            $this->db->group_by($articulo . '.imagen');
            $query = $this->db->get();
            return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;

        else:
            return FALSE;
        endif;
    }

    /**
     * Retorna el id de una orden de despacho
     *
     * @access
     * @param integer $id Identificador de la cotizacion
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 12/11/13 04:07 PM
     */
    function getIdOrdenDespacho($id = NULL, $select = ' id ', $return_select_full = FALSE) {
        if (!$id)
            return FALSE;
        //Tables
        $orden_despacho = $this->getSchema() . '.orden_despacho';


        //Query
        $this->db->select("$select", FALSE);

        $this->db->from($orden_despacho);
        $this->db->where(array("$orden_despacho.id_cotizacion" => $id));
        $query = $this->db->get();
        if ($return_select_full):
            return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
        else:
            return ($query->num_rows() > 0) ? ($query->row('id')) : FALSE;
        endif;
    }

    /**
     * Obtiene los articulos asociados a una orden de despacho
     *
     * @access  public
     * @param   integer $id_cotizacion  Identificador de la cotizacion
     * @param   string  $category  Tipo de categoria
     * @param   string  $backup  Determina si el articulo es un backup
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 14/11/13 02:29 PMM
     */
    function getArticulosByPdf($id_orden, $category, $backup = '0', $type_quote = 'cotizacion') {

        if ($id_orden && $category):
            //Tables
            $cotizacion_articulo = $this->getSchema() . ".{$type_quote}_articulo";
            $articulo = $this->getSchema() . '.articulo';
            $cotizacion = "business.{$type_quote}";
            $t_tramo_articulo = 'business.tramo_articulo';
            $orden_despacho_articulos = $this->getSchema() . '.orden_despacho_articulos';
            $orden_despacho = $this->getSchema() . '.orden_despacho';
            $table_category = 'dynamic.category';

            //Query
            $this->db->distinct("$orden_despacho_articulos.id_lote");
            $this->db->select("$orden_despacho_articulos.id_lote", false);
            $this->db->select("$t_tramo_articulo.lote");
            $this->db->select($articulo . '.id');
            //$this->db->select($cotizacion_articulo . '.id as id_cot_articulo');
            $this->db->select($cotizacion_articulo . '.precio');
            $this->db->select("count($cotizacion_articulo .cantidad ) as cantidad", FALSE);
            $this->db->select($cotizacion_articulo . '.precio');
            $this->db->select($articulo . '.modelo');
            $this->db->select($articulo . '.descripcion');
            $this->db->select($articulo . '.imagen');

            $this->db->from($orden_despacho_articulos);
            $this->db->join($cotizacion_articulo, "$cotizacion_articulo.id_articulo=$orden_despacho_articulos.id_articulo");
            $this->db->join($articulo, "$articulo.id=$cotizacion_articulo.id_articulo");
            $this->db->join($cotizacion, "$cotizacion.id=$cotizacion_articulo.id_cotizacion");
            $this->db->join($table_category, "$articulo.tipo_articulo=$table_category.id  ");
            $this->db->join($t_tramo_articulo, "$orden_despacho_articulos.id_lote=$t_tramo_articulo.id  ");
            if ($category == 'ALMACEN'):
                $this->db->where(
                        array(
                            "$orden_despacho_articulos.id_orden_despacho" => $id_orden,
                            "$table_category._table" => 'tipo_articulo'));
            else:
                if ($type_quote == 'cotizacion'):
                    $this->db->where(
                            array(
                                "$orden_despacho_articulos.id_orden_despacho" => $id_orden,
                                "$orden_despacho_articulos.backup" => $backup,
                                "$table_category._label" => $category,
                                "$table_category._table" => 'tipo_articulo'));
                elseif ($type_quote == 'libre_cotizacion'):
                    $this->db->where(
                            array(
                                "$orden_despacho_articulos.id_orden_despacho" => $id_orden));
                endif;
            endif;
            $this->db->group_by("$orden_despacho_articulos.id_lote");
            $this->db->group_by("$t_tramo_articulo.lote");
            $this->db->group_by($articulo . '.id');
            $this->db->group_by($cotizacion_articulo . '.id ');
            $this->db->group_by($cotizacion_articulo . '.precio');
            $this->db->group_by("$cotizacion_articulo .cantidad");
            $this->db->group_by($cotizacion_articulo . '.precio');
            $this->db->group_by($articulo . '.modelo');
            $this->db->group_by($articulo . '.descripcion');
            $this->db->group_by($articulo . '.imagen');
            $query = $this->db->get();
            return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;

        else:
            return FALSE;
        endif;
    }

    /**
     * Provee los tramos disponibles para cada articulo
     *
     * @access  public
     * @param integer $cantidad Cantidad con la cual se evaluara la disponibilidad de articulos en almacen
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com >
     * @version 1.0 19/11/13 02:28 PM
     */
    function availabilityTramoOfItems($id = array()) {

        $t_tramo_articulo = 'business.tramo_articulo';
        $t_articulo = 'business.articulo';
        $this->db->select("$t_tramo_articulo.* ", FALSE);
        $this->db->select("$t_articulo.modelo", FALSE);
        $this->db->from($t_tramo_articulo);
        $this->db->join("$t_articulo", "$t_articulo.id=$t_tramo_articulo.id_articulo");
        $this->db->where(array("$t_tramo_articulo._delete" => "0"));
        $this->db->where_in('id_articulo', $id);
        $this->db->order_by('created_at', 'ASC');
//        $this->db->group_by("$t_tramo_articulo.id_articulo");
//        $this->db->having("sum($t_tramo_articulo.cantidad)=$cantidad");
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    /**
     * busca los pacientes para cargar el select
     *
     * @access  public
     * @return arreglo de registros a cargar en el select
     * @author  Eliezer Pe#a
     * @version 1.0 20/12/2013
     */
    function searchCostumer() {
        $this->db->distinct();
        $this->db->select('pac.id');
        $this->db->select(" pac.cedula || ' ' || pac.primer_nombre || ' ' ||pac.primer_apellido as text");
        $this->db->from('business.paciente as pac');
        $this->db->join('business.cotizacion as cot', 'cot.id_paciente=pac.id');
        $this->db->join('business.cotizacion_estatus cot_es', 'cot.id=cot_es.id_cotizacion and cat_cotizacion_estatus=93');
        $this->db->where("cot.id not in (SELECT id_cotizacion FROM business.orden_despacho WHERE _delete='0') ");
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    /**
     * returna el arreglo de cotizaciones asociadas a un paciente dado
     *
     * @access  public
     * @param integer $id_paciente contiene el identificador del paciente
     * @param integer $estatus contiene el codigo del estatus de la cotizacion
     * @return arreglo de registros a cargar en el select
     * @author  Eliezer Pe#a
     * @version 1.0 20/12/2013
     */
    function getCotizacionByPaciente($id_paciente, $estatus) {
        $this->db->distinct();
        $this->db->select('cot.id');
        $this->db->select('cot.codigo');
        $this->db->from('business.cotizacion cot');
        $this->db->join('business.cotizacion_estatus cot_es', 'cot.id=cot_es.id_cotizacion');
        $this->db->where('cot.id_paciente', $id_paciente);
        $this->db->where('cot_es.cat_cotizacion_estatus', $estatus);
        $this->db->where("cot.id not in (SELECT id_cotizacion FROM business.orden_despacho WHERE _delete='0') ");
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    /**
     * returna el arreglo clientes para la orden de despacho
     *
     * @access  public
     * @param integer $cat_letra Define el tipo de cliente a retornar para la orden de despacho
     * @return arreglo de registros a cargar en el select
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 09/01/14 12:15 PM
     */
    function getClienteForDelivery($cat_letra) {

        $t_cliente = '';
        if (!$cat_letra)
            return FALSE;
        if ($cat_letra == 46):
            $t_cliente = $this->getSchema() . '.persona';
            $this->db->select("$t_cliente.id", FALSE);
            $this->db->select("COALESCE($t_cliente.primer_nombre || ' ' || $t_cliente.primer_apellido ) as cliente", FALSE);
        elseif ($cat_letra == 47):
            $t_cliente = $this->getSchema() . '.empresa';
            $this->db->select("$t_cliente.id", FALSE);
            $this->db->select("COALESCE( $t_cliente.nombre) as cliente", FALSE);
        endif;

        $t_libre_cotizacion = $this->getSchema() . '.libre_cotizacion';
        $this->db->distinct("$t_cliente.id", FALSE);
        $this->db->from($t_cliente);
        $this->db->join("$t_libre_cotizacion", "$t_cliente.id=$t_libre_cotizacion.id_cliente");
        $this->db->where(array("$t_libre_cotizacion._delete" => '0'));
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    /**
     * returna el arreglo de cotizaciones asociadas a un cliente (persona/empresa)
     *
     * @access  public
     * @param integer $id_paciente contiene el identificador del paciente
     * @param integer $estatus contiene el codigo del estatus de la cotizacion
     * @return arreglo de registros a cargar en el select
     * @author  Eliezer Pe#a
     * @version 1.0 20/12/2013
     */
    function getCotizacionByCliente($id_cliente, $estatus) {
        $this->db->distinct();
        $this->db->select('cot.id');
        $this->db->select('cot.codigo');
        $this->db->from('business.libre_cotizacion cot');
//        $this->db->join('business.libre_cotizacion_estatus cot_es', 'cot.id=cot_es.id_cotizacion');
        $this->db->where('cot.id_cliente', $id_cliente);
//        $this->db->where('cot_es.cat_cotizacion_estatus', $estatus);
        $this->db->where("cot.id not in (SELECT id_cotizacion FROM business.orden_despacho WHERE _delete='0') ");
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    /**
     * Obtiene los articulos asociados al preview de  una orden de despacho
     *
     * @access  public
     * @param   array $articles  Arreglo de identificadores de articulos
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 02/01/14 04:01 PM
     */
    function getArticulosByPdf_preview($articles) {

        if ($articles):
            //Tables

            $articulo = $this->getSchema() . '.articulo';

            //Query

            $this->db->select($articulo . '.id');
            $this->db->select($articulo . '.modelo');
            $this->db->select($articulo . '.descripcion');
            $this->db->select($articulo . '.imagen');

            $this->db->from($articulo);


            $this->db->where_in("$articulo.id", $articles);

            $query = $this->db->get();
            return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;

        else:
            return FALSE;
        endif;
    }

    /**
     * Obtiene el correo electronico de la persona que tenga el rol de gerente de despacho
     *
     * @access  public
     * 
     * @author  Eliezer pe#a <pe.eliezer@gmail.com>
     * @version 1.0 13/02/14 
     */
    function getJefeDespacho() {
        $this->db->select('u.email');
        $this->db->from('acl.users u');
        $this->db->join('acl.users_groups ug', 'u.id=ug.user_id');
        $this->db->join('acl.groups g', 'g.id=ug.group_id');
        $this->db->like('g.description', 'Gerente De Despacho');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->row->email) : FALSE;
    }

}
