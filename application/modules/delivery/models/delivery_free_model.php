<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Descripcion de  Despacho
 *
 * @package conmed.application
 * @subpackage modules
 * @category controller
 * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V - 1.0 09/10/13 11:54 AM
 */
class Delivery_free_model extends AST_Model {

    function __construct() {
        parent::__construct();
        $this->setSchema('business');
        $this->init('orden_despacho');
        $this->load->model('settings/categories_model', 'categories_model');
    }

    /**
     * Retorna los valores de una consulta a una entidad filtrado por determinados campos
     *
     * @access  public
     * @param   string $table nombre del campo identificador de la tabla en donde se consultara
     * @param   string $fields cadena que contiene los campos separados por coma
     * @param   string $filter cadena que contiene el filtro que se desea para los campos buscados
     * @param   string $label cadena que define como se llamara el campo label en el select, por defecto text
     * @return  array Arreglo con los valores de la consulta
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V - 1.0 09/10/13 11:54 AM
     */
    public function getComboByTable($table, $fields, $filter = FALSE, $label = 'text', $status_cotizacion = array()) {
        if (!$fields)
            return array();

        if ($filter) {
            $arr_fields = explode(',', $fields);
            foreach ($arr_fields as $key => $arr) {
                unset($arr_fields[$key]);
                $arr_fields[$arr] = $filter;
            }
        }

        //limpia los campos table y field para realizar la consulta
        $table = preg_replace('/^id_/i', '', $table);

        //Determina si la cadena de campos contiene el esquema de la tabla, de ser asi, extrae el esquema y los campos
        //si no, deja por defecto business, este esquema debe estar definido al principio y separado por |
        $schema = (strstr($fields, '|')) ? substr($fields, 0, strrpos($fields, '|')) : 'business';
        $fields = (strstr($fields, '|')) ? substr($fields, strrpos($fields, '|') + 1, strlen($fields)) : $fields;

        //Concatena los campos que se fijaron en la BD por el caracter (,)
        $fields = preg_replace('/,/i', " || ' ' || ", $fields);

        $this->db->select("$fields  AS $label");
        $this->db->select("$schema.$table.id");


        $this->db->from("$schema.$table");
        $this->db->join('business.cotizacion_estatus', "$schema.$table.id=cotizacion_estatus.id_cotizacion");
        if (sizeof($status_cotizacion) > 0):
            $this->db->where_in('cotizacion_estatus.cat_cotizacion_estatus', $status_cotizacion);
        endif;

        $this->db->where("$schema.$table.id not in( SELECT id_cotizacion FROM business.orden_despacho WHERE _delete='0' )");


        $this->db->group_by("$schema.$table.codigo");
        $this->db->group_by("$schema.$table.id");

        if ($filter)
            $this->_ilike($arr_fields, 'OR', TRUE);

        $query = $this->db->get();

        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    /**
     * Dado un arreglo se obtiene todas las coincidencias del arreglo por
     * clave/valor, donde clave es el campo la base de datos y valor el valor a
     * ser filtrado.
     *
     * @access  public
     * @param   Array $params <p>Contiene los pares clave valor a ser filtrados.</p>
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V - 1.0 09/10/13 11:54 AM
     */
    function getByFields($params) {
        //tables
        $tipo_orden = $this->categories_model->getCategory(array('_table' => 'tipo_despacho', '_label' => 'Libre'), ' id ');
        $t_orden_despacho = $this->concatSchemaTable();
        $t_orden_despacho_estatus = $this->getSchema() . '.orden_despacho_estatus';
        $t_persona = $this->getSchema() . '.persona';
        $t_cirugia = $this->getSchema() . '.cirugia';
        $t_empleado = $this->getSchema() . '.empleado';
        $t_empresa = $this->getSchema() . '.empresa';
        $t_category = 'dynamic.category';

        //Select
        $this->db->select("$t_orden_despacho.id");
        $this->db->select("COALESCE($t_persona.primer_nombre || ' ' || $t_persona.primer_apellido , $t_empresa.nombre) as primer_nombre", FALSE);
        $this->db->select("$t_orden_despacho.codigo");
        $this->db->select("$t_orden_despacho.fecha_solicitud");
        $this->db->select("$t_orden_despacho.fecha_despacho");
        $this->db->select("$t_category._label as cat_estatus_orden_despacho");
        $this->db->select("$t_empleado.primer_nombre || ' ' || $t_empleado.primer_apellido AS id_despachador");

        //From and join
        $this->db->from("$t_orden_despacho");
        $this->db->join("$t_orden_despacho_estatus", "$t_orden_despacho.id=$t_orden_despacho_estatus.id_orden_despacho");
        $this->db->join("$t_category", "$t_orden_despacho_estatus.cat_orden_despacho_estatus=$t_category.id");
        $this->db->join("$t_empleado", "$t_orden_despacho.id_despachador=$t_empleado.id left ");
        $this->db->join("$t_persona", "$t_orden_despacho.id_cliente=$t_persona.id left ");
        $this->db->join("$t_empresa", "$t_orden_despacho.id_cliente=$t_empresa.id");
        if (!empty($params)) {
            $value = $params['codigo'];

            unset($params['id_despachador']);
            unset($params['cat_estatus_orden_despacho']);
            unset($params['codigo']);

            $params[" CAST($t_empleado.primer_nombre || ' ' || $t_empleado.primer_apellido AS TEXT) "] = $value;
            $params[" CAST($t_category._label AS TEXT) "] = $value;
            $params[" CAST($t_orden_despacho.codigo AS TEXT) "] = $value;
            $params[" to_char($t_orden_despacho.fecha_solicitud,'DD-MM-YYYY HH:MI PM') "] = $value;
            $params[" to_char($t_orden_despacho.fecha_despacho,'DD-MM-YYYY HH:MI PM') "] = $value;
            $params[" CAST($t_orden_despacho.id AS TEXT) "] = $value;

            $this->_ilike($params, 'OR', TRUE);
        }

        $this->db->where("$t_orden_despacho._delete", '0');
        if ($tipo_orden):
            $this->db->where("$t_orden_despacho.cat_tipo_despacho", $tipo_orden[0]['id']);
        endif;
    }

    /**
     * Obtiene el maximo codigo a generar en la tabla orden_despacho
     *
     * @access  public
     * @return  string  codigo maximo
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V - 1.0 09/10/13 11:54 AM
     */
    function generateCode() {
        $length = 15;
        $query = $this->db->query("SELECT MAX(codigo) AS max FROM  " . $this->concatSchemaTable());
        $code = ($query->num_rows() > 0) ? ($query->row()->max) : str_repeat('0', $length);
        return str_repeat('0', $length - strlen( ++$code)) . $code;
    }

    /**
     * Provee las implementacion para realizar inserciones dentro de orden_despacho
     * y las entidades dependientes de ella
     *
     * @access  public
     * @param   Array  $data datos a ser guardados dentro de la base de datos.
     * @return  Boolean Indicando el resultado de la operacion.
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V - 1.0 09/10/13 11:54 AM
     */
    function insert($data) {
        $this->db->trans_start();
        $get_data_orden_despacho = $this->insertWithReturning($data['orden'], $this->concatSchemaTable());
        $id_orden_despacho = $get_data_orden_despacho['id'];
        $orden_free_articulo = $data['orden_free_articulo'];
        if ($orden_free_articulo) {
            foreach ($orden_free_articulo as $art => $articulo)
                $orden_free_articulo[$art]['id_orden_despacho'] = $id_orden_despacho;
            $this->db->insert_batch('business.orden_despacho_articulos', $orden_free_articulo);
        }
        $this->changeStatus($id_orden_despacho, 'registrada');
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    /**
     * Cambia el status de una cotizacion a aprobada, emitida,procesada
     *
     * @access  public
     * @param   integer  $id_cotizacion Identificador de la cotizacion
     * @param   string   $status Nuevo Estatus de la cotizacion, debe ser el valor que esta
     *                  en categoria cotizacion_estatus
     * @param   Boolean $send_email Indica si se enviara email con el cambio de estatus
     * @return  Boolean Resultado de la operacion.
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V - 1.0 09/10/13 11:54 AM
     */
    function changeStatus($id, $status, $send_email = FALSE, $destination_path = '') {
        $attachment = '';
        $body = '';
        $status_name = mb_convert_case($status, MB_CASE_TITLE, "UTF-8");
        $status_id = $this->engine_model->getValueByCategory($status_name, '_label');

        if (!$status_id)
            return FALSE;

        $data = array('id_orden_despacho' => $id, 'cat_orden_despacho_estatus' => $status_id, 'created_by' => $this->session->userdata('user_id'));

        $result_ope = $this->db->insert('business.orden_despacho_estatus', $data);

//        $this->load->helper('view_engine');
//        if (!$result_ope or !$send_email)
        return $result_ope;

//        $code = $this->getCode($id_cotizacion);
//        $html['datos'] = $this->getById($id_cotizacion);
//        switch ($status_name) {
//            case 'Emitida':
//                $email = array($this->session->userdata('user_email'));
//                //$email = 'epena@ast.com.ve' //cable ;
//                $html['html'] = sprintf($this->lang->line('body_cot_emitida'), $code);
//                $html['title'] = $this->lang->line('subject_cot_emitida');
//                $body = $this->load->view('email_cotizacion', $html, TRUE);
//                $subject = $this->lang->line('subject_cot_emitida');
//                $attachment = $destination_path;
//
//                break;
//            default:
//                return FALSE;
//                break;
//        }
//
//        return sendEmail($email, $body, $subject, $body, $attachment);
    }

    /**
     * Dado un arreglo se obtiene todas las coincidencias del arreglo por
     * clave/valor, donde clave es el campo la base de datos y valor el valor a
     * ser filtrado.
     *
     * @access  public
     * @param   Array $params <p>Contiene los pares clave valor a ser filtrados.</p>
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V - 1.0 09/10/13 11:54 AM
     */
    function getDataDelivery($id = null) {
        if (!$id)
            return FALSE;
        //tables
        $t_orden_despacho = $this->concatSchemaTable();
        $t_orden_despacho_estatus = $this->getSchema() . '.orden_despacho_estatus';
        $t_persona = $this->getSchema() . '.persona';
        $t_empresa = $this->getSchema() . '.empresa';
        $t_cirugia = $this->getSchema() . '.cirugia';
        $t_empleado = $this->getSchema() . '.empleado';
        $t_category = 'dynamic.category';

        //Select
        $this->db->select("$t_orden_despacho.id");
        $this->db->select("COALESCE($t_persona.id, $t_empresa.id) as id_cliente", FALSE);
        $this->db->select("COALESCE($t_persona.primer_nombre || ' ' || $t_persona.primer_apellido , $t_empresa.nombre) as cliente", FALSE);
        $this->db->select("COALESCE($t_persona.correo_electronico, $t_empresa.email) as email", FALSE);
        $this->db->select("COALESCE($t_persona.letra || ' - ' || $t_persona.cedula , $t_empresa.rif) as ced_rif", FALSE);
        $this->db->select("$t_orden_despacho.fecha_solicitud");
        $this->db->select("$t_orden_despacho.fecha_despacho");
        $this->db->select("to_char($t_orden_despacho.fecha_despacho,'dd-mm-YYYY') as fecha_entrega", FALSE);
        $this->db->select("to_char($t_orden_despacho.fecha_solicitud,'dd-mm-YYYY') as fecha_inicio", FALSE);
        $this->db->select("$t_orden_despacho.codigo");
        $this->db->select("$t_category._label as cat_estatus_orden_despacho");
        $this->db->select("$t_empleado.primer_nombre || ' ' || $t_empleado.primer_apellido AS id_despachador");

        //From and join
        $this->db->from("$t_orden_despacho left ");
        $this->db->join("$t_persona", "$t_orden_despacho.id_cliente=$t_persona.id left ");
        $this->db->join("$t_empresa", "$t_orden_despacho.id_cliente=$t_empresa.id");
        $this->db->join("$t_orden_despacho_estatus", "$t_orden_despacho.id=$t_orden_despacho_estatus.id_orden_despacho");
        $this->db->join("$t_category", "$t_orden_despacho_estatus.cat_orden_despacho_estatus=$t_category.id");
        $this->db->join("$t_empleado", "$t_orden_despacho.id_despachador=$t_empleado.id");

        $this->db->where("$t_orden_despacho.id", $id);
        $this->db->where("$t_orden_despacho._delete", '0');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    /**
     * Verifica la disponibilidad de articulos de una cotizacion en almacen
     * sin tomar en cuenta los sustituidos e incluyendo los sustitutos
     *
     * @access  public
     * @param   integer $id_cotizacion  Identificador de la cotizacion
     * @param   string  $category  Tipo de categoria
     * @param   string  $backup  Determina si el articulo es un backup
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 29/10/13 03:00 PM
     */
    function availabilityOfItemsByCot($id_cotizacion = NULL) {
        if (!$id_cotizacion)
            return FALSE;
        //Tables
        $t_tramo_articulo = 'business.tramo_articulo';

        //Query
        $this->db->select($t_tramo_articulo . '.id_articulo');
        $this->db->from($t_tramo_articulo);
        $this->db->where("$t_tramo_articulo.id_articulo in
                        (
                        SELECT
                                business.cotizacion_articulo.id_articulo
                        FROM
                                business.cotizacion_articulo
                        WHERE
                                business.cotizacion_articulo.id_parent is  null
                                AND business.cotizacion_articulo.id_cotizacion =  $id_cotizacion
                                AND business.cotizacion_articulo.id_articulo not in (
                                            SELECT
                                                    business.cotizacion_articulo.id_parent
                                            FROM
                                                    business.cotizacion_articulo
                                            WHERE
                                                    business.cotizacion_articulo.id_parent is not null

                                            GROUP BY business.cotizacion_articulo.id_parent
                                                                                    )
		UNION ALL

			SELECT
                                business.cotizacion_articulo.id_articulo
                        FROM
                                business.cotizacion_articulo
                        WHERE
                                business.cotizacion_articulo.id_parent is not null
                                AND business.cotizacion_articulo.id_cotizacion =  $id_cotizacion)
                    ");
        $this->db->group_by($t_tramo_articulo . '.id_articulo');
        $this->db->having("sum($t_tramo_articulo.cantidad) =0", FALSE);


        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

    /**
     * Obtiene los articulos asociados a una orden de despacho
     *
     * @access  public
     * @param   integer $id_orden  Identificador de la orden de despacho
     * @param   string  $category  Tipo de categoria
     * @param   string  $backup  Determina si el articulo es un backup
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 12/09/13 12:22 PM
     */
    function getArticulosByOrden($id_orden, $backup = '0', $tipo = 0) {

        if ($id_orden):
            //Tables
            $orden_despacho_articulos = $this->getSchema() . '.orden_despacho_articulos';
            $orden_despacho = $this->getSchema() . '.orden_despacho';
            $articulo = $this->getSchema() . '.articulo';
            $t_tramo_articulo = 'business.tramo_articulo';

            //Query
            $this->db->select($orden_despacho_articulos . '.id');
            $this->db->select($orden_despacho_articulos . '.precio');
            $this->db->select($orden_despacho_articulos . '.cantidad');
            $this->db->select($articulo . '.modelo');
            $this->db->select($articulo . '.descripcion');
            $this->db->select($articulo . '.imagen');
            $this->db->select($t_tramo_articulo . '.lote');
            $this->db->from($orden_despacho_articulos);
            $this->db->join($articulo, "$articulo.id=$orden_despacho_articulos.id_articulo");
            $this->db->join($orden_despacho, "$orden_despacho.id=$orden_despacho_articulos.id_orden_despacho");
            $this->db->join($t_tramo_articulo, "$t_tramo_articulo.id=$orden_despacho_articulos.id_lote");
            $this->db->where(
                    array(
                        "$orden_despacho_articulos.id_orden_despacho" => $id_orden
            ));
            $query = $this->db->get();
            return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;

        else:
            return FALSE;
        endif;
    }

    /**
     * Retorna el id de una orden de despacho
     *
     * @access
     * @param integer $id Identificador de la cotizacion
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 12/11/13 04:07 PM
     */
    function getIdOrdenDespacho($id = NULL) {
        if (!$id)
            return FALSE;
        //Tables
        $orden_despacho = $this->getSchema() . '.orden_despacho';


        //Query
        $this->db->select($orden_despacho . '.id');

        $this->db->from($orden_despacho);
        $this->db->where(array("$orden_despacho.id_cotizacion" => $id));
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->row('id')) : FALSE;
    }

    /**
     * Obtiene los articulos asociados a una orden de despacho
     *
     * @access  public
     * @param   integer $id_cotizacion  Identificador de la cotizacion
     * @param   string  $category  Tipo de categoria
     * @param   string  $backup  Determina si el articulo es un backup
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 14/11/13 02:29 PMM
     */
    function getArticulosByPdf($id_orden, $category = '') {

        if ($id_orden):
            //Tables
            $cotizacion_articulo = $this->getSchema() . '.cotizacion_articulo';
            $articulo = $this->getSchema() . '.articulo';
            $t_tramo_articulo = 'business.tramo_articulo';
            $orden_despacho_articulos = $this->getSchema() . '.orden_despacho_articulos';
            $table_category = 'dynamic.category';

            //Query
            $this->db->select("$orden_despacho_articulos.id_lote", false);
            $this->db->select("$t_tramo_articulo.lote");
            $this->db->select($articulo . '.id');
            $this->db->select($orden_despacho_articulos . '.precio');
            $this->db->select("count($orden_despacho_articulos .cantidad ) as cantidad", FALSE);
            $this->db->select($articulo . '.modelo');
            $this->db->select($articulo . '.descripcion');
            $this->db->select($articulo . '.imagen');

            $this->db->from($orden_despacho_articulos);
            $this->db->join($articulo, "$articulo.id=$orden_despacho_articulos.id_articulo");
            $this->db->join($t_tramo_articulo, "$orden_despacho_articulos.id_lote=$t_tramo_articulo.id  ");

            $this->db->where(
                    array(
                        "$orden_despacho_articulos.id_orden_despacho" => $id_orden));

            $this->db->group_by("$orden_despacho_articulos.id_lote");
            $this->db->group_by($orden_despacho_articulos . '.precio');
            $this->db->group_by("$orden_despacho_articulos .cantidad");
            $this->db->group_by("$t_tramo_articulo.lote");
            $this->db->group_by($articulo . '.id');
            $this->db->group_by($articulo . '.modelo');
            $this->db->group_by($articulo . '.descripcion');
            $this->db->group_by($articulo . '.imagen');
            $query = $this->db->get();
            return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;

        else:
            return FALSE;
        endif;
    }

    /**
     * Provee los tramos disponibles para cada articulo
     *
     * @access  public
     * @param integer $cantidad Cantidad con la cual se evaluara la disponibilidad de articulos en almacen
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com >
     * @version 1.0 19/11/13 02:28 PM
     */
    function availabilityTramoOfItems($id = array()) {

        $t_tramo_articulo = 'business.tramo_articulo';
        $t_articulo = 'business.articulo';
        $this->db->select("$t_tramo_articulo.* ", FALSE);
        $this->db->select("$t_articulo.modelo", FALSE);
        $this->db->from($t_tramo_articulo);
        $this->db->join("$t_articulo", "$t_articulo.id=$t_tramo_articulo.id_articulo");
        $this->db->where(array("$t_tramo_articulo._delete" => "0"));
        $this->db->where_in('id_articulo', $id);
        $this->db->order_by('created_at', 'ASC');
//        $this->db->group_by("$t_tramo_articulo.id_articulo");
//        $this->db->having("sum($t_tramo_articulo.cantidad)=$cantidad");
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    /**
     * Obtiene los articulos asociados al preview de  una orden de despacho libre
     *
     * @access  public
     * @param   array $articles  Arreglo de identificadores de articulos
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 02/01/14 04:01 PM
     */
    function getArticulosByPdf_preview($articles = array()) {

        if ($articles):
            //Tables

            $articulo = $this->getSchema() . '.articulo';

            //Query

            $this->db->select($articulo . '.id');
            $this->db->select($articulo . '.modelo');
            $this->db->select($articulo . '.descripcion');
            $this->db->select($articulo . '.imagen');

            $this->db->from($articulo);


            $this->db->where_in("$articulo.id", $articles);

            $query = $this->db->get();
            return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;

        else:
            return FALSE;
        endif;
    }

    /**
     * Descripcion
     *
     * @access public
     * @param integer $id Identificador de la orden de despacho
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 06/01/14 10:43 AM
     */
    function dataForPdfOfOrden($id = NULL) {
        if (!$id)
            return FALSE;



        //tables
        $t_orden_despacho = $this->concatSchemaTable();
        $t_orden_despacho_estatus = $this->getSchema() . '.orden_despacho_estatus';
        $t_orden_despacho_articulos = $this->getSchema() . '.orden_despacho_articulos';
        $t_persona = $this->getSchema() . '.persona';
        $t_empresa = $this->getSchema() . '.empresa';
        $t_cirugia = $this->getSchema() . '.cirugia';
        $t_empleado = $this->getSchema() . '.empleado';
        $t_category = 'dynamic.category';


        $this->db->select($t_orden_despacho . '.*', FALSE);
        $this->db->select("to_char($t_orden_despacho.fecha_despacho,'dd-mm-YYYY') as fecha_entrega", FALSE);
        $this->db->select("to_char($t_orden_despacho.fecha_solicitud,'dd-mm-YYYY') as fecha_inicio", FALSE);
        $this->db->select("COALESCE($t_persona.primer_nombre || ' ' || $t_persona.primer_apellido , $t_empresa.nombre) as cliente", FALSE);
        $this->db->select("COALESCE($t_persona.correo_electronico, $t_empresa.email) as email", FALSE);
        $this->db->select("COALESCE($t_persona.letra || ' - ' || $t_persona.cedula , $t_empresa.rif) as ced_rif", FALSE);
        $this->db->select("COALESCE($t_persona.tlf_casa, $t_persona.tlf_oficina , $t_persona.tlf_movil ,$t_empresa.tlf) as tlf", FALSE);


        $this->db->from("$t_orden_despacho left ");
        $this->db->join("$t_persona", "$t_orden_despacho.id_cliente=$t_persona.id left ");
        $this->db->join("$t_empresa", "$t_orden_despacho.id_cliente=$t_empresa.id");
        $this->db->where("$t_orden_despacho.id", $id);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

}
