<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * Esta clase provee la implementacion de las operaciones de notificaciones
 * y sus entidades dependientes o extendidas
 * 
 * @package		CodeIgniter
 * @subpackage	        Modules
 * @category		Model
 * @author		Nohemi Rojas<nohemir@gmail.com>
 * @since               V 1.0 23/11/12 02:40 PM
 */
class Notification_model extends AST_Model {

    /**
     * Constructor
     *
     * @access	public
     * @author  Nohemi Rojas
     * @version V-1.0 28/01/13 02:51 PM
     */
    public function __construct() {
        parent::__construct();
    }

     /**
     * Obtiene las validaciones de las cotizaciones abiertas
     *
     * @param string  $level Nivel de la validacion
     * @param integer $limit Limite del Numero de registros 
     * @return array Registros obtenidos
     * @access public
     * @author  Nohemi Rojas
     * @version V 1.0 29/01/13 02:59 PM
     */
    function getValidationOpen($level, $limit = NULL) {
        $this->db->select("cot.codigo, val.descripcion,val.url_operation");
        // $this->db->select('cot_val.*');
        $this->db->select("cot.id");
        $this->db->from("business.cotizacion_validacion AS cot_val");
        $this->db->join('business.cotizacion AS cot', 'cot.id = cot_val.id_cotizacion');
        $this->db->join('business.validacion AS val', 'val.id = cot_val.id_validacion');
        $this->db->join('dynamic.category AS cat', 'cat.id = cot_val.cat_val_nivel');
        if ($level == 'vendedor')
            $this->db->where('cot.created_by', $this->session->userdata('user_id'));
        $this->db->where('abierto', '1');
        $this->db->where('cat._label', $level);
        if ($limit)
            $this->db->limit($limit);
       $this->db->order_by('cot_val.created_at', 'DESC');
       $query1 = $this->db->get()->result_array();

        $this->db->select("cot.modelo as codigo, val.descripcion,val.url_operation");
        // $this->db->select('cot_val.*');
        $this->db->select("cot.id");
        $this->db->from("business.cotizacion_validacion AS cot_val");
        $this->db->join('business.articulo AS cot', 'cot.id = cot_val.id_cotizacion');
        $this->db->join('business.validacion AS val', 'val.id = cot_val.id_validacion');
        $this->db->join('dynamic.category AS cat', 'cat.id = cot_val.cat_val_nivel');
        $this->db->where('abierto', '1');
        $this->db->where('cat._label', $level);
        if ($limit)
            $this->db->limit($limit);
        $this->db->order_by('cot_val.created_at', 'DESC');
        $query2 = $this->db->get()->result_array();



        $query = array_merge($query1, $query2);
        //print_r($query);die;
        return (count($query)> 0) ? $query : FALSE;;
    }

}

// END Notification_model class
/* End of file notification_model.php */
/* Location: application/modules/engine/models/notification_model.php */