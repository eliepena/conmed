<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**

 *
 * @package		CodeIgniter
 * @subpackage	        Modules
 * @category		Controllers
 * @author		Nohemi Rojas<nohemir@gmail.com>
 * @since               V 1.0 30/01/13 01:11 PM
 */
class Notification extends AST_Controller {

    /**
     * Constructor
     * 
     * @access	public
     * @author  Nohemi Rojas
     * @version V-1.0 21/11/12 02:34 PM
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     *  lista todas las notificaciones pendientes 
     * @access public
     * @author Eliezer Pe#a <pe.eliezer@gmail.com>
     * @return array vista de las notificaciones
     */
    
    public function listAll() {
         $role_name = $this->session->userdata('role_name');

        //$role_name='validador';
        // $role_name='vendedor';
         switch ($role_name) {
            case 'validador':
                $level = 'validador';
                $data['url'] = 'cotizacion/cotizacion/validate';
                break;
            case 'Admin':
                $level = 'validador';
                $data['url'] = 'cotizacion/cotizacion/validate';
                break;
            case 'vendedor':
                $level = 'vendedor';
                $data['url'] = 'cotizacion/cotizacion/correct';
                break;
            case 'Logistica':
                $level = 'logistica';
                $data['url'] = 'cotizacion/cotizacion/detail';
                break;
            default:
                return array('msg' => '', 'cant' => 0);
                break;
        }
        
        $data['data']= $this->notification_model->getValidationOpen($level);
   
        if(!empty($data['data']))
            foreach ($data['data'] as &$value)
                $value['id'] = $this->encrypt->encode($value['id']);
    
        $view = $this->load->view('listAll', $data, TRUE);
     
        $this->setContent($view);
    }
}

// END home class
/* End of file home.php */
/* Location: application/modules/home/home.php */
?>