<?php if ($data > 0): ?>
    <table class="table-striped">
        <?php  $i = 0 ?>
        <?php foreach ($data as $value) : ?>
            <?php $i++ ?>
            <tr><td>
                    <?php $op = (!empty($value['url_operation']) || $value['url_operation'] != '') ? $value['url_operation'] : $url; ?>
                    <?php if ($param): ?>
                        <a href="#" onclick="request({url:\'<?= base_url("$op") ?>\', data:{id:\'<?= $value['id'] ?>\'}});">
                            <?php echo $value['codigo'] ?>
                        </a>
                    <?php else: ?>
                        <a href="#" onclick="request({url:'<?= base_url("$op") ?>', data:{id:'<?= $value['id'] ?>'}});">
                            <?php echo $value['codigo'] ?>
                        </a>
                    <?php endif; ?>

                    <?php echo $value['descripcion'] ?>
                </td>
            </tr>
            <?php if ($i == 3): ?>
                <?php break ?>
            <?php endif; ?>
        <?php endforeach; ?>
    </table>

    <?php if (count($data) >= 3): ?>
        <?php if ($param): ?>
            <a href="#" class="link" onclick="request({url:\'<?= base_url("notification/notification/listAll") ?>\'});">
                Ver Todas
            </a>
        <?php else: ?>
            <a href="#" class="link" onclick="request({url:'<?= base_url("notification/notification/listAll") ?>'});">
                Ver todas
            </a>
        <?php endif; ?>
    <?php endif; ?>

<?php else: ?>
    No se encontraron notificaciones
<?php endif; ?>