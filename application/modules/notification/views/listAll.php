<h3>Notificaciones</h3>
<?php if ($data>0): ?>
    <table class="table-striped table table-bordered">
        <tr>
            <th>Cotizacion</th>
            <th>Accion</th>
        </tr>
        <?php $i = 0 ?>
        <?php foreach ($data as $value) : ?>
            <?php $i++ ?>
            <tr><td>
                    <?php $op = (!empty($value['url_operation']) || $value['url_operation'] != '') ? $value['url_operation'] : $url; ?>
                        <a href="#" onclick="request({url:'<?= base_url("".$op) ?>', data:{id:'<?= $value['id'] ?>'}});">
                            <?php echo $value['codigo'] ?>
                        </a>
                    </td>
                     <td>
                    <?php echo $value['descripcion'] ?>
                </td>
            </tr>
          <?php endforeach; ?>
    </table>

<?php else: ?>
    No se encontraron notificaciones
<?php endif; ?>