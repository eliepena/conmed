<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Seguro
 *
 * Esta clase provee la implementacion para la capa de control de
 * acciones de la entidad Empresa y sus entidades dependientes o extendidas
 *
 * @package Module
 * @category Controller
 * @version 17/01/13 03:20 PM
 */
class Seguro extends AST_Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Muestra la interefaz de creacion de seguro y guardo los datos enviado
     * por la vista de creacion de seguro.
     *
     * @access  public
     * @param array $params Parametros adicionales enviados al controlador.
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 21/01/13 12:59
     */
    public function create($params = NULL) {
        if (!empty($params) and $this->input->post())
            $_POST['validado'] = '1';
        parent::create($params);
    }

    /**
     * Format avanazado para la clinica
     *
     * @access  public
     * @param array $format_type Tipo de Formateo [INSERT, UPDATE].
     * @param array $data Datos a ser retornados.
     * @return  array    datos formateados.
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 23/01/13 10:23
     */
    public function _avancedFormat($format_type, $data) {
        $data['validado'] = (empty($data['validado'])) ? '0' : $data['validado'];
        return $data;
    }

    /**
     * Ver historial de pacientes de los seguros asociados
     *
     * @access  public
     * @param array $format_type Tipo de Formateo [INSERT, UPDATE].
     * @param array $data Datos a ser retornados.
     * @author  Frederick D. Bustamante G.
     * @version V 1.0 14/05/13 11:46 AM
     */
    public function patient_history() {


        if ($this->input->post('id')) {
            $id = $this->get_post_id();
            $this->session->set_userdata('seguro.id', $id);
            unset($_POST);

            //seteamos los datos de auditoria
            $this->audit->setOperationData(array('id' => $id));
        }

        $view_params = $this->pagination();

        //Evaluamos si view paran tiene datos, si los tiene lazamos la vista
        // caso contrario mandamos a imprimer los datos
        if ($view_params){
            $view_params['show_title'] = FALSE;
            $this->setContent($this->view_engine->grid($view_params));
        }

    }

    /**
     * Ver historial de Medicos de los seguros asociados
     *
     * @access  public
     * @param array $format_type Tipo de Formateo [INSERT, UPDATE].
     * @param array $data Datos a ser retornados.
     * @author  Eliezer A. Pe#a
     * @version V 1.0 14/05/13 11:46 AM
     */
    public function medic_history(){
         if ($this->input->post('id')) {
            $id = $this->get_post_id();
            $this->session->set_userdata('seguro.id', $id);
            unset($_POST);

            //seteamos los datos de auditoria
            $this->audit->setOperationData(array('id' => $id));
        }

        $view_params = $this->pagination();

        //Evaluamos si view paran tiene datos, si los tiene lazamos la vista
        // caso contrario mandamos a imprimer los datos
        if ($view_params){
            $view_params['show_title'] = FALSE;
            $this->setContent($this->view_engine->grid($view_params));
        }
    }


    /**
     * Ver historial de cirugias de los seguros asociados
     *
     * @access  public
     * @param alfanum $id Identificador del Seguro.
     * @param array $view_params Parametros para crear window en la vista.
     * @author  Frederick D. Bustamante G.
     * @version V 1.0 15/05/13 04:22 PM
     */
    public function surgeries_history() {
        if ($this->input->post('id')) {
            $id = $this->get_post_id();
            $this->session->set_userdata('seguro.id', $id);
            unset($_POST);

            //seteamos los datos de auditoria
            $this->audit->setOperationData(array('id' => $id));
        }

        $view_params = $this->pagination();

        //Evaluamos si view paran tiene datos, si los tiene lazamos la vista
        // caso contrario mandamos a imprimer los datos
        if ($view_params){
            $view_params['show_title'] = FALSE;
            $this->setContent($this->view_engine->grid($view_params));
        }
    }

   /**
     * Se define para el uso de la funcion pagination() de acuerdo al metodo de la uri
     *
     * @access  public
     * @param array $format_type Tipo de Formateo [INSERT, UPDATE].
     * @param array $data Datos a ser retornados.
     * @return  array    datos formateados.
     * @author  Frederick D. Bustamante G.
     * @version V 1.0 14/05/13 11:46 AM
     */
    function getPaginationData($param) {
        $uri = $this->uri->uri_string();
        //Se evalua de que uri viene
		if (strpos($uri, 'medic_history')) {
            $param['id'] = $this->session->userdata('seguro.id');
            $data['data'] = $this->Seguro_model->medic_history($param, false);
            $data['count'] = $this->Seguro_model->medic_history($param, true);
            return $data;
        } elseif (strpos($uri, 'patient_history')) {
            $param['id'] = $this->session->userdata('seguro.id');
            $data['data'] = $this->Seguro_model->patient_history($param, false);
            $data['count'] = $this->Seguro_model->patient_history($param, true);
            return $data;
        } elseif (strpos($uri, 'surgeries_history')) {
            $param['id'] = $this->session->userdata('seguro.id');
            $data['data'] = $this->Seguro_model->surgeries_history($param, false);
            $data['count'] = $this->Seguro_model->surgeries_history($param, true);
            return $data;
        }else {
            return parent::getPaginationData($param);
        }
    }

}