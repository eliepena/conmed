<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Descripcion de clinica
 *
 * @package conmed.application
 * @subpackage modules
 * @category controller
 * @author Jose A. Rodriguez E. <josearodrigueze@gmail.com>
 * @version V - 1.0 24/04/2013 11:30:15 am
 */
class Clinica extends AST_Controller {

    function __construct() {
        parent::__construct();
    }

    /**
     * Muestra la interefaz de creacion de seguro y guardo los datos enviado
     * por la vista de creacion de seguro.
     *
     * @access  public
     * @param array $params Parametros adicionales enviados al controlador.
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 21/01/13 12:59
     */
    public function create($params = NULL) {
        if (!empty($params) and $this->input->post())
            $_POST['validado'] = '1';
        parent::create($params);
    }

    /**
     * Format avanazado para la clinica
     *
     * @access  public
     * @param array $format_type Tipo de Formateo [INSERT o UPDATE].
     * @param array $data Datos a ser retornados.
     * @return  array    datos formateados.
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 23/01/13 10:23
     */
    public function _avancedFormat($format_type, $data) {
        $data['validado'] = (empty($data['validado'])) ? '0' : $data['validado'];
        return $data;
    }

}