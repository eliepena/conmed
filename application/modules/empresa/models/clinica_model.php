<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Descripcion de clinica_model
 *
 * DESCRIPCION
 *
 * @package PAQUETE
 * @subpackage SUBPAQUETE
 * @category CATEGORIA
 * @author Jose A. Rodriguez E. <josearodrigueze@gmail.com>
 * @version V - 1.0 FECHA
 */
class Clinica_model extends AST_Model {

    function __construct() {
        parent::__construct();
        $this->setSchema('business');
        $this->init('centro_salud');
    }

    /**
     * Dado un arreglo se obtiene todas las coincidencias del arreglo por
     * clave/valor, donde clave es el campo la base de datos y valor el valor a
     * ser filtrado.
     *
     * @access  public
     * @param   Array   $params <p>Contiene los pares clave valor a ser filtrados.</p>
     * @param   boolean $returned [optional] <p>
     *      <b>TRUE(default)</b> retorna el resultado de la consulta a BD.
     *      <b>FALSE</b> realiza la ejecucion del metodo where de CI.</p>
     * @return  mixed   <p>
     *  <b>Array<b/> con el resultado de la consulta.
     *  <b>Void<b/> setea el where del metodo de CI.</p>
     * @author  Jose A. Rodriguez E.
     * @version V-1.0 22/01/13 15:50
     */
    function getByFields($params = array()) {
        $t_centro = $this->concatSchemaTable();

        //Select
        $this->db->select($t_centro . '.id');
        $this->db->select('nombre');
        $this->db->select('rif');
        $this->db->select('tlf');
        $this->db->select('website');
        $this->db->select('nombre_contacto');
        $this->db->select('tlf_contacto');
        $this->db->select('CASE WHEN validado = \'1\' THEN \'SI\' ELSE \'NO\' END AS "validado" ', FALSE);

        //From e inners
        $this->db->from($t_centro);

        //Where
        $this->whereDelete();
        if (!empty($params)) {
//            $params['_label'] = $params['nombre'];
            unset($params['validado']);
            $this->_ilike($params, 'OR', TRUE);
        }

        $debug = debug_backtrace();
        if ($debug[1]['function'] == 'getAll')
            $this->db->order_by('validado');
    }

    /**
     * Devuelve los datos basicos del Centro de Salud
     *
     * @access public
     * @param integer $id Identificador del Centro de Salud
     * @param string  $campo Campos a mostrar en el select (default:'*')
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 31/07/13 12:35 PM
     */
    function getCentroSalud($where = array(), $campo = '') {

        $this->db->select((empty($campo) ? ' * ' : ' ' . $campo . ' '), false);
        $this->db->from($this->concatSchemaTable());
        if (sizeof($where) > 0):
            $this->db->where($where);
        endif;
        $this->db->where(array('_delete' => '0'));
        $this->db->order_by('nombre');

        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

    /**
     * Devuelve la Direccion de un centro de salud
     *
     * @access public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 31/07/13 12:35 PM
     */
    function getDirectionsCentroSalud() {
        $table = $this->concatSchemaTable();
        $t_clinica = $this->getSchema() . '.centro_salud';
        $this->db->select("$table.id", false);
        $this->db->select("$table.nombre", false);
        $this->db->select("$table.direccion", false);
        $this->db->select("cli_loc.nombre as localidad", false);
        $this->db->from($table, 'LEFT');
        $this->db->join("{$this->getSchema()}.localidad AS cli_loc", "cli_loc.id=$t_clinica.id_localidad");
        $this->db->where(array("$table._delete" => '0'));
        $this->db->order_by("$table.nombre");
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

}