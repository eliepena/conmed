<?php

/**
 * Description of Seguro_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad Seguro
 * y sus entidades dependientes o extendidas
 *
 * @package Modules
 * @category Model
 * @version V-1.0 17/01/13 03:21 PM
 */
class Seguro_model extends AST_Model {

    public function __construct() {
        $this->setSchema('business');
        parent::__construct();
    }

    /**
     * Dado un arreglo se obtiene todas las coincidencias del arreglo por
     * clave/valor, donde clave es el campo la base de datos y valor el valor a
     * ser filtrado.
     *
     * @access  public
     * @param   Array   $params <p>Contiene los pares clave valor a ser filtrados.</p>
     * @param   boolean $returned [optional] <p>
     *      <b>TRUE(default)</b> retorna el resultado de la consulta a BD.
     *      <b>FALSE</b> realiza la ejecucion del metodo where de CI.</p>
     * @return  mixed   <p>
     *  <b>Array<b/> con el resultado de la consulta.
     *  <b>Void<b/> setea el where del metodo de CI.</p>
     * @author  Jose A. Rodriguez E.
     * @version V-1.0 22/01/13 15:50
     */
    function getByFields($params = array()) {
        $t_seguro = $this->concatSchemaTable();
        $t_category = 'dynamic.category';

        //Select
        $this->db->select($t_seguro . '.id');
        // $this->db->select('id_ec');
        $this->db->select('nombre');
        $this->db->select('rif');
        $this->db->select('tlf');
        $this->db->select('website');
        $this->db->select('nombre_contacto');
        $this->db->select('tlf_contacto');
        $this->db->select('_label AS cat_tipo_seguro');
        $this->db->select('CASE WHEN validado = \'1\' THEN \'SI\' ELSE \'NO\' END AS "validado" ', FALSE);

        //From e inners
        $this->db->from($t_seguro);
        $this->db->join($t_category, "$t_category.id=$t_seguro.cat_tipo_seguro");

        //Where
//        $this->db->where('validado', '1');
        $this->whereDelete();
        if (!empty($params)) {
            $params['_label'] = $params['nombre'];
            unset($params['cat_tipo_seguro']);
            unset($params['validado']);
            $this->_ilike($params, 'OR', TRUE);
        }

        $debug = debug_backtrace();
        if ($debug[1]['function'] == 'getAll')
            $this->db->order_by('nombre ASC, validado');
    }

    /**
     * Se obtienen los registros pertinentes a la paginacion de Historial de Pacientes
     *
     * @access  public
     * @param array $params Parametros recibidos para el where de la consulta.
     * @param boolean $count Determina el tipo de retorno del metodo (Datos o Cantidad de datos).
     * @return mixed
     * @author  Frederick D. Bustamante G.
     * @version V 1.0 14/05/13 05:40 PM <frederickdanielb@gmail.com>
     */
    public function patient_history($params = array(), $count) {
        extract($params);
        $schema = 'business';
        $table_seguro = 'seguro';
        $table_cotizacion = 'cotizacion';
        $table_paciente = 'paciente';

        $this->db->select($table_paciente . '.id', FALSE);
        $this->db->select($table_paciente . '.cedula', FALSE);
        $this->db->select("$table_paciente.primer_nombre|| ' ' ||$table_paciente.segundo_nombre AS primer_nombre", FALSE);
        $this->db->select("$table_paciente.primer_apellido|| ' ' ||$table_paciente.segundo_apellido AS primer_apellido", FALSE);
        $this->db->select($table_paciente . '.tlf_casa', FALSE);
        $this->db->select($table_paciente . '.tlf_oficina', FALSE);
        $this->db->select($table_paciente . '.tlf_movil', FALSE);
        $this->db->select($table_paciente . '.correo_electronico', FALSE);
        $this->db->select($table_paciente . '.direccion', FALSE);
        $this->db->select($table_paciente . '.sexo', FALSE);
        $this->db->select($table_paciente . '.fecha_nacimiento', FALSE);
        $this->db->from($schema . '.' . $table_seguro, " inner ");
        $this->db->join($schema . '.' . $table_cotizacion, "$table_seguro.id = $table_cotizacion.id_seguro inner");
        $this->db->join($schema . '.' . $table_paciente, "$table_cotizacion.id_paciente = $table_paciente.id");
        $this->db->where(array("$table_seguro.id" => $id));

        //Define el orden con que se mostraran los datos.
        //el sort es obtenido de param pasado desde el controller
        foreach ($sort as $field_bd => $order)
            $this->db->order_by($field_bd, $order);

        //Define el el intervalo de paginaciones a mostrar
        $this->_limit($offset);


        //Define la busqueda dinamica de la paginacion
        if (!empty($search_fields)):
            $search = NULL;

            //se concatena el nombre de la tabla del select
            //para evitar error de ambiguedad en el sql
            foreach ($search_fields as $key => $value):
                $search["{$table_paciente}.{$key}"] = $value;
            endforeach;
            $this->_ilike($search, 'OR', TRUE);
        endif;

        $query = $this->db->get();
        return ($query->num_rows() > 0 ? ($count ? $query->num_rows() : $query->result_array()) : 0);
    }

    /**
     * Se obtienen los registros pertinentes a la paginacion de Historial de cirugias
     *
     * @access  public
     * @param array $params Parametros recibidos para el where de la consulta.
     * @param boolean $count Determina el tipo de retorno del metodo (Datos o Cantidad de datos).
     * @return mixed
     * @author  Frederick D. Bustamante G.
     * @version V 1.0 14/05/13 05:40 PM <frederickdanielb@gmail.com>
     */
    public function surgeries_history($params = array(), $count) {
        extract($params);
        $schema = 'business';
        $table_seguro = 'seguro';
        $table_cotizacion = 'cotizacion';
        $table_paciente = 'paciente';
        $table_medico = 'medico';
        $table_empleado = 'empleado';

        //$sql_paciente = "SELECT primer_nombre|| ' ' || segundo_nombre AS pacient FROM business.paciente;";
        $this->db->select($table_cotizacion . '.*', FALSE);
        $this->db->select("$table_paciente.primer_nombre|| ' ' || $table_paciente.segundo_apellido AS id_paciente", FALSE);
        $this->db->select("$table_medico.primer_nombre|| ' ' || $table_medico.segundo_apellido AS id_medico", FALSE);
        $this->db->select("$table_empleado.primer_nombre|| ' ' || $table_empleado.segundo_apellido AS id_vendedor", FALSE);
        $this->db->from($schema . '.' . $table_seguro, " inner ");
        $this->db->join($schema . '.' . $table_cotizacion, "$table_seguro.id = $table_cotizacion.id_seguro LEFT");
        $this->db->join($schema . '.' . $table_paciente, "$table_cotizacion.id_paciente = $table_paciente.id LEFT");
        $this->db->join($schema . '.' . $table_medico, "$table_cotizacion.id_medico = $table_medico.id LEFT");
        $this->db->join($schema . '.' . $table_empleado, "$table_cotizacion.id_vendedor = $table_empleado.id");
        $this->db->where(array("$table_seguro.id" => $id));

        //Define el orden con que se mostraran los datos.
        //el sort es obtenido de param pasado desde el controller
        foreach ($sort as $field_bd => $order)
            $this->db->order_by($field_bd, $order);

        //Define el el intervalo de paginaciones a mostrar
        $this->_limit($offset);


        //Define la busqueda dinamica de la paginacion
        if (!empty($search_fields)):
            $search = NULL;

            //se concatena el nombre de la tabla del select
            //para evitar error de ambiguedad en el sql
            foreach ($search_fields as $key => $value):
                if ($key == 'id_paciente'):
                    $search["CAST({$table_paciente}.primer_nombre as TEXT)"] = $value;
                    $search["CAST({$table_paciente}.primer_apellido as TEXT)"] = $value;
                elseif ($key == 'id_medico'):
                    $search["CAST({$table_medico}.primer_nombre as TEXT)"] = $value;
                    $search["CAST({$table_medico}.primer_apellido as TEXT)"] = $value;
                elseif ($key == 'id_vendedor'):
                    $search["CAST({$table_empleado}.primer_nombre as TEXT)"] = $value;
                    $search["CAST({$table_empleado}.primer_apellido as TEXT)"] = $value;
                else:
                    $search["CAST({$table_cotizacion}.{$key} as TEXT)"] = $value;
                endif;

            endforeach;
            $this->_ilike($search, 'OR', TRUE);
        endif;

        $query = $this->db->get();
        if ($query->num_rows() > 0):
            if ($count):
                return $query->num_rows();
            else:
                return $query->result_array();
            endif;
        else:
            return 0;
        endif;
    }

    /**
     * Se obtienen los registros pertinentes a la paginacion de Historial de Medicos
     *
     * @access  public
     * @param array $format_type Tipo de Formateo [INSERT, UPDATE].
     * @param array $data Datos a ser retornados.
     * @return  mixed
     * @author  Eliezer A. Pe#a
     * @version V 1.0 15/05/13 05:40 PM
     */
    public function medic_history($params = array(), $count) {
        extract($params);
        $schema = 'business';
        $table_seguro = 'seguro';
        $table_cotizacion = 'cotizacion';
        $table_medico = 'medico';
        $medico_localidad = 'medico_localidad';
        $centro_salud = 'centro_salud';

        $this->db->select($table_medico . '.id', FALSE);
        $this->db->select("$table_medico.primer_nombre|| ' ' ||$table_medico.segundo_nombre AS primer_nombre", FALSE);
        $this->db->select("$table_medico.primer_apellido|| ' ' ||$table_medico.segundo_apellido AS primer_apellido", FALSE);
        $this->db->select($table_medico . '.cedula', FALSE);
        $this->db->select($table_medico . '.tlf_casa', FALSE);
        $this->db->select($table_medico . '.tlf_movil', FALSE);
        $this->db->select($table_medico . '.correo_electronico', FALSE);
        $this->db->select($centro_salud . '.nombre', FALSE);
        $this->db->from($schema . '.' . $table_seguro, 'inner');
        $this->db->join($schema . '.' . $table_cotizacion, "$table_seguro.id = $table_cotizacion.id_seguro");
        $this->db->join($schema . '.' . $table_medico, "$table_cotizacion.id_medico = $table_medico.id");
        $this->db->join($schema . '.' . $medico_localidad, "$table_medico.id=$medico_localidad.id_medico");
        $this->db->join($schema . '.' . $centro_salud, "$medico_localidad.id_centro_salud=$centro_salud.id");
        $this->db->where(array("$table_seguro.id" => $id));

        //Define el orden con que se mostraran los datos.
        //el sort es obtenido de param pasado desde el controller
        foreach ($sort as $field_bd => $order)
            $this->db->order_by($field_bd, $order);

        //Define el el intervalo de paginaciones a mostrar
        $this->_limit($offset);


        //Define la busqueda dinamica de la paginacion
        if (!empty($search_fields)):
            $search = NULL;

            //se concatena el nombre de la tabla del select
            //para evitar error de ambiguedad en el sql
            foreach ($search_fields as $key => $value):
                if ("{$key}" != "nombre")
                    $search["{$table_medico}.{$key}"] = $value;
                else
                    $search["{$centro_salud}.{$key}"] = $value;
            endforeach;
            $this->_ilike($search, 'OR', TRUE);
        endif;

        $query = $this->db->get();
        if ($query->num_rows() > 0):
            if ($count):
                return $query->num_rows();
            else:
                return $query->result_array();
            endif;
        else:
            return 0;
        endif;
    }

    /**
     * Devuelve los datos basicos de la Aseguradora
     *
     * @access public
     * @param integer $id Identificador la Aseguradora
     * @param string  $campo Campos a mostrar en el select (default:'*')
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 31/07/13 12:35 PM
     */
    function getAseguradora($where = array(), $campo = '') {

        $this->db->select((empty($campo) ? ' * ' : ' ' . $campo . ' '), false);
        $this->db->from($this->concatSchemaTable());
        if (sizeof($where) > 0):
            $this->db->where($where);
        endif;
        $this->db->where(array('_delete' => '0'));
        $this->db->order_by('nombre');

        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

}