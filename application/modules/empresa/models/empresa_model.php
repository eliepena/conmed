<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Empresa_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad empresa
 * y sus entidades dependientes o extendidas
 *
 * @package Modules
 * @category Model
 * @author Nohemi Rojas. <nohemir@gmail.com>
 * @version V-1.0 17/01/13 03:21 PM
 */
class Empresa_model extends AST_Model {

    public function __construct() {
        $this->setSchema('business');
        parent::__construct();
    }

    /**
     * Obtiene datos de la empresa
     *
     * @access  public
     * @param   integer  $id_empresa identificador de la empresa
     * @return  array    datos de la empresa
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 19/12/13 04:49 PM
     */
    public function getEmpresa($id_empresa) {
        $this->db->select("*");
        $this->db->from('business.empresa');
        $this->db->where('id', $id_empresa);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : NULL;
    }

}

?>
