<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Cotizacion
 *
 * Esta clase provee la implementacion para la capa de control de
 * acciones de la entidad cotizacion y sus entidades dependientes o extendidas
 *
 * @package Module
 * @category Controller
 * @author Nohemi Rojas. <nohemir@gmail.com>
 * @version V-1.0 29/11/12 01:26 PM
 */
class Cotizacion extends AST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('persona/paciente_model', 'paciente_model');
        $this->load->model('persona/Instrumentista_model', 'instrumentista_model');
        $this->load->model('engine/engine_model', 'engine_model');
        $this->load->model('validacion_model', 'validacion_model');
        $this->load->model('persona/persona_model', 'persona_model');
        $this->load->model('empresa/empresa_model', 'empresa_model');
        $this->load->model('persona/empleado_model', 'empleado_model');
        $this->load->model('empresa/seguro_model', 'seguro_model');
        $this->load->model('inventario/precios_model', 'precios_model');
        $this->load->model('persona/medico_model', 'medico_model');
        $this->load->model('empresa/clinica_model', 'clinica_model');
        $this->load->model('settings/categories_model', 'categories_model');
        $this->load->model('auth/users_model', 'users_model');
        $this->load->model('delivery/delivery_model', 'delivery_model');
        $this->load->helper('ast');
    }

    /**
     * Provee el acceso al formulario de ingreso de nuevos datos, asi mismo,
     * provee el acceso a la insercion de datos nuevos dentro en base de datos.
     *
     * @access public
     * @param array $params Parametros adicionales enviados al controlador.
     * @author  Nohemi Rojas
     * @version V 1.0 30/11/12 01:17 PM
     */
    function create($params = NULL) {
        $modo = $params;
        $this->session->unset_userdata("monto_material");
        $this->session->userdata('permissions');
        if (!$this->input->post()) {

            //Arreglo de datos para la pestana datos
            $tab_datos = array(
                'operation' => 'create',
                'paciente' => $this->paciente_model->getPaciente(NULL, 'id,letra,cedula,primer_nombre,primer_apellido'),
                'precio' => $this->precios_model->getPrecios(NULL, 'id,nombre'),
                'articulacion' => $this->Cotizacion_model->getCirugia(array('chk_tabla' => 'articulacion'), 'id,nombre'),
                'lesion' => $this->Cotizacion_model->getCirugia(array('chk_tabla' => 'lesion'), 'id,id_parent,nombre'),
                'tecnica' => $this->Cotizacion_model->getCirugia(array('chk_tabla' => 'tecnica'), 'id,id_parent,nombre'),
                'medico' => $this->medico_model->getMedico(array(), 'id,letra,cedula,primer_nombre,primer_apellido'),
                'centro_salud' => $this->clinica_model->getDirectionsCentroSalud(),
                'tipo_direccion' => $this->categories_model->getCategory(array('_table' => 'tipo_direccion_cotizacion'), 'id,_label'),
                'emisor_pago' => $this->categories_model->getCategory(array('_table' => 'pagador'), 'id,_label'),
                'aseguradora' => $this->seguro_model->getAseguradora(array(), 'id,nombre'),
                'tipo_persona' => $this->categories_model->getCategory(array('_table' => 'letra'), 'id,_label'),
                'tipo_pago' => $this->categories_model->getCategory(array('_table' => 'tipo_pago'), 'id,_label'),
                'direccion_medico' => $this->medico_model->searchDirections(),
                'direccion_centro_salud' => $this->medico_model->searchDirections(),
                'vendedor' => $this->users_model->getVendedor()
            );

            $tabs = array(
                '1' => array('view' => 'particular', 'label' => 'Datos', 'params' => array('file' => 'cotizacion/_datos', 'after_validation' => 'cotizacion/create/after_validation.js.php', 'button_submit' => 'Siguiente', 'data' => $tab_datos)),
                '2' => array('view' => 'particular', 'label' => 'Materiales', 'params' => array('file' => 'cotizacion/_materiales', 'data' => array('operation' => 'create'))),
                '3' => array('view' => 'particular', 'label' => 'Backup', 'params' => array('file' => 'cotizacion/_create_backup', 'data' => array('operation' => 'create'))),
                '4' => array('view' => 'particular', 'label' => 'Instrumental', 'params' => array('file' => 'cotizacion/_instrumentos', 'data' => array('operation' => 'create'))),
                '5' => array('view' => 'particular', 'label' => 'Totales', 'params' => array('file' => 'cotizacion/_totales', 'data' => array('operation' => 'create')))
            );
            $view['view'] = $this->view_engine->tab($tabs);

            $this->setContent($this->load->view('create/create', $view, TRUE));
        } elseif ($params[0] == 'process') {
            $success = FALSE;
            if (isset($modo[1]) && $modo[1] == 'validate_data'):
                $this->_avancedFormValidation('insert', true);
            else:
                $this->_avancedFormValidation('insert');
            endif;
            $process = $this->view_engine->process();
            if ($process['result']) {
                if (isset($modo[1]) && $modo[1] == 'validate_data'):
                    $this->offOutput();
                    echo json_encode(array('validate' => true));
                else:

                    if (isset($modo[1]) && $modo[1] == 'preview'):
                        $process['data']['preview'] = true;
                    else:
                        $process['data']['preview'] = false;
                    endif;

                    $params = array('format_type' => 'insert', 'data' => $process['data']);
                    $data = $this->_format($params);
                    $getMaterialesPdf = $this->Cotizacion_model->getMaterialesPdf($data['cotizacion_material']);
                    $getDatosPdf = $this->Cotizacion_model->getDatosPdf($data['cotizacion']['id_medico'], $data['cotizacion']['id_seguro'], $data['cotizacion']['id_paciente'], $data['cotizacion_pago']['cat_tipo_pago'], $data['cotizacion']['id_vendedor']);
                    $datos_paciente = $this->paciente_model->getPaciente($data['cotizacion']['id_paciente'], 'tlf_movil');

                    foreach ($getMaterialesPdf as $clave => $valor):
                        $materiales[] = array(
                            'codigo_m' => $valor[0]['id'],
                            'modelo_m' => $valor[0]['modelo'],
                            'descripcion_m' => $valor[0]['descripcion'],
                            'gart_m' => '',
                            'cant_m' => $valor[0]['cantidad'],
                            'precio_m' => number_format($valor[0]['precio'], 2, ',', '.'),
                            'neto_m' => number_format($valor[0]['precio'] * $valor[0]['cantidad'], 2, ',', '.')
                        );
                    endforeach;
                    $params_pdf = array(
                        'nro_cotizacion' => $data['cotizacion']['codigo'],
                        'fecha_emision' => date('d/m/Y'),
                        'forma_pago' => $getDatosPdf['tipo_pago'],
                        'tiempo_entrega' => 'INMEDIATA',
                        'vendedor' => $getDatosPdf['vendedor'],
                        'dr_tratante' => $getDatosPdf['medico'],
                        'cliente' => $getDatosPdf['pnombre'],
                        'rif' => $getDatosPdf['pcedula'],
                        'nit' => '',
                        'seguro' => ($getDatosPdf['nseguro'] == null ? 'No Aplica' : $getDatosPdf['nseguro']),
                        'tlf' => (empty($datos_paciente[0]['tlf_movil']) || $datos_paciente[0]['tlf_movil'] == null ? 'Sin Informacion' : $datos_paciente[0]['tlf_movil']),
                        'monto_exento' => number_format($data['cotizacion']['monto_exento'], 2, ',', '.'),
                        'monto_gravable' => number_format($data['cotizacion']['monto_gravable'], 2, ',', '.'),
                        'porcentaje_descuento' => $data['cotizacion']['descuento'],
                        'calculo_descuento' => number_format(($data['cotizacion']['monto_gravable'] * $data['cotizacion']['descuento']) / 100, 2, ',', '.'),
                        'recargo' => number_format(($data['cotizacion']['monto_gravable'] * $data['cotizacion']['recargo']) / 100, 2, ',', '.'),
                        'p_recargo' => $data['cotizacion']['recargo'],
                        'porcentaje_iva' => $this->config->item('valor_iva'),
                        'calculo_iva' => number_format(($data['cotizacion']['monto_gravable'] * $this->config->item('valor_iva')) / 100, 2, ',', '.'),
                        'neto' => number_format($data['cotizacion']['monto_total'], 2, ',', '.'),
                        'materiales' => $materiales
                    );

                    if (isset($modo[1]) && $modo[1] == 'preview'):
                        $file = $this->EXC_create_pdf_cotizacion($params_pdf, true, $this->input->post('membrete'));
                        $this->offOutput();
                        echo json_encode(array('view' => 1, 'file' => $file));
                    else:
                        $this->EXC_create_pdf_cotizacion($params_pdf, FALSE, TRUE);
                        $success = $this->Cotizacion_model->insert($data);
                        $this->setContent($success);
                    endif;
                endif;
            } else {
                $this->setContent($process['validation_error']);
            }
            if (!isset($modo[1])):
                // -----------------------------
                //seteamos los datos de auditoria
                $this->audit->setOperationData($process);
                $this->audit->setChkResult($success);
            // -----------------------------
            endif;
        }
    }

    /**
     * Se encarga de corregir una cotizacion
     *
     * @param array $params Parametros adicionales enviados al controlador.
     * @author Nohemi Rojas
     * @version 1.0 22/01/13 03:34 PM
     */
    function correct($params = NULL) {
        $this->validate_correct($params, 'vendedor', 'validador');
    }

    /**
     * Se encarga de validar una cotizacion
     *
     * @param array $params Parametros adicionales enviados al controlador.
     * @author Nohemi Rojas
     * @version 1.0 22/01/13 03:34 PM
     */
    function validate($params = NULL) {
        $this->validate_correct($params, 'validador', 'vendedor');
    }

    /**
     * Se encarga de validar o corregir una cotizacion,
     *
     * @param array $params Parametros adicionales enviados al controlador.
     * @param string $params Indicador del nivel que se maneja
     * @param string $params Indicador del nivel al que se cambiara el estatus
     * @author Nohemi Rojas
     * @version 1.0 25/01/13 04:24 PM
     */
    private function validate_correct($params, $level_current, $level_last) {
        $array_post = $this->input->post();
        if ($params == NULL) {
            $id = $this->encrypt->decode($array_post['id']);
            $data = $this->Cotizacion_model->getById($id);

            //Necesario para busqueda
            $data['id_paciente'] = $data['id_paciente_id_ec'];
            //Obtiene campos particulas relacionados a la validacion de cotizacion
            $fields = $this->validacion_model->getFields($data['id'], $level_current);


            //Busca los datos de persona en caso de pago Tercero
            if ($data['id_persona']) {
                $persona = $this->persona_model->getById($data['id_persona'], 'id');
                $data['id_persona_id_ec'] = $persona['id_ec'];
                $data['id_persona_validado'] = $persona['validado'];
            }

            //Busca los datos de Empresa en caso de pago Tercero
            if ($data['id_empresa']) {
                $empresa = $this->empresa_model->getById($data['id_empresa'], 'id');
                $data['id_empresa_id_ec'] = $empresa['id_ec'];
                $data['id_empresa_validado'] = $empresa['validado'];
            }

            $this->session->set_userdata("Cotizacion_model.id", $data['id']);
            $this->session->set_userdata("fields_val_cotizacion", $fields);
            $this->session->set_userdata("values_val_cotizacion", $data);


            $reload_content = 'cotizacion/reload_content.js.php';
            $view['view'] = $this->view_engine->form(array('fields' => $fields, 'reload_content' => $reload_content));

            $view['data'] = $data;
            $view['fields'] = ($fields) ? count($fields) : 0;
            $view['level'] = $level_current;
            $view['id_operation'] = $this->operation_id;


            $this->setContent($this->load->view('validate', $view, TRUE));
        } elseif ($params[0] == 'process') {
            $success = FALSE;
            $process = $this->view_engine->process(array('fields' => $this->session->userdata("fields_val_cotizacion")));

            if ($process['result']) {
                $id = $this->session->userdata("Cotizacion_model.id");

                $process['data']['level_current'] = $level_current;
                $params = array('format_type' => 'validate', 'data' => $process['data']);

                $data = $this->_format($params);

                $success = $this->Cotizacion_model->validate($id, $data, $this->session->userdata("values_val_cotizacion"), $level_current, $level_last);

                if ($success) {
                    $this->session->unset_userdata("Cotizacion_model.id");
                    $this->session->unset_userdata("fields_val_cotizacion");
                    $this->session->unset_userdata("values_val_cotizacion");
                }

                $this->setContent($success);
            } else {
                $this->setContent($process['validation_error']);
            }

            // -----------------------------
            //seteamos los datos de auditoria
            $operation_data = $process;
            $this->audit->setChkResult($success);
            // -----------------------------
        }
        // -----------------------------
        //seteamos los datos de auditoria
        $operation_data['id'] = $id;
        $this->audit->setOperationData($operation_data);
        // -----------------------------
    }

    /**
     * Se encarga de mostrar los detalles de una cotizacion
     *
     * @author Nohemi Rojas
     * @version 1.0 03/12/12 09:05 AM
     */
    function detail() {
        $id_ec = $this->input->post('id');
        $target = $this->input->post('target_on');
        $id = $this->encrypt->decode($id_ec);
        $this->session->set_userdata('id_cot', $id_ec);
        if (!empty($id)) {

            $data = $this->Cotizacion_model->getById($id);
            $data['current_status'] = $this->Cotizacion_model->getStatus($id);
            $data['all_status'] = $this->Cotizacion_model->getAllStatus($id);

            $data['validate'] = $this->validacion_model->getFields($id, 'validador');
            $data['correct'] = $this->validacion_model->getFields($id, 'vendedor');
            $data['id_ec'] = $id_ec;

            //Busca los datos de persona en caso de pago Tercero
            if (isset($data['id_persona'])) {
                $persona = $this->persona_model->getById($data['id_persona'], 'id');
                $data['id_persona'] = $persona['primer_nombre'] . ' ' . $persona['primer_apellido'];
                $data['id_persona_id_ec'] = $this->encrypt->encode($persona['id']);
            }

            //Busca los datos de Empresa en caso de pago Tercero
            elseif (isset($data['id_empresa'])) {
                $empresa = $this->empresa_model->getById($data['id_empresa'], 'id');
                $data['id_empresa'] = $empresa['nombre'];
                $data['id_empresa_id_ec'] = $this->encrypt->encode($empresa['id']);
            }

            //Busca los datos del Seguro en caso de pago Aseguradora
            elseif (isset($data['id_seguro'])) {
                $seguro = $this->seguro_model->getById($data['id_seguro'], 'id');
                $data['id_seguro'] = $seguro['nombre'];
                $data['id_seguro_id_ec'] = $this->encrypt->encode($seguro['id']);
            }

            //Encoding datos
            $data['id_paciente_id_ec'] = $this->encrypt->encode($data['id_paciente_id_ec']);
            $data['id_medico_id_ec'] = $this->encrypt->encode($data['id_medico_id_ec']);
            $data['id_centro_salud_id_ec'] = $this->encrypt->encode($data['id_centro_salud_id_ec']);
            $array_for_replace = array();
            $array_for_replace['materiales'] = $data['materiales'] = $this->Cotizacion_model->getArticulosByCot($data['id'], 'MAT');
            $array_for_replace['backup'] = $data['backup'] = $this->Cotizacion_model->getArticulosByCot($data['id'], 'MAT', '1');
            $array_for_replace['instrumentos'] = $data['instrumentos'] = $this->Cotizacion_model->getArticulosByCot($data['id'], 'INS');

            //Se obtienen los materiales sustituidos y sus sustitutos
            $data['sustituidos'][] = $this->_obtainReplacements($array_for_replace);

            $dataInstrumentalists['detail_quote'] = $this->view_engine->form(array('data' => $data, 'operation_url' => trim($this->uri->uri_string(), '/')));
            $dataInstrumentalists ['instrumentalists'] = $this->instrumentista_model->instrumentalistsByCot($id);
            $dataInstrumentalists ['operation'] = 'detail';
            $data['target_on'] = $target;
            $tabs = array(
                array('view' => 'particular', 'label' => 'Notificaciones', 'params' => array('file' => 'cotizacion/notification', 'data' => array('data' => $data, 'target_on' => $target))),
                //array('view' => 'form', 'url' => trim($this->uri->uri_string(), '/'), 'params' => array('data' => $data)),
                array('view' => 'particular', 'label' => 'Detalles de Cotizacion', 'params' => array('file' => 'cotizacion/_detailQuote', 'data' => $dataInstrumentalists)),
                array('view' => 'particular', 'label' => 'Materiales', 'params' => array('file' => 'cotizacion/_materiales', 'data' => array('operation' => 'detail', 'materiales' => $data['materiales'], 'articulos_sustituidos' => $data['sustituidos'][0]['materiales']))),
                array('view' => 'particular', 'label' => 'Instrumental', 'params' => array('file' => 'cotizacion/_instrumentos', 'data' => array('operation' => 'detail', 'instrumentos' => $data['instrumentos'], 'articulos_sustituidos' => $data['sustituidos'][0]['instrumentos']))),
                array('view' => 'form', 'url' => 'cotizacion/cotizacion/detailPayment', 'params' => array('data' => $data)),
//               array('view' => 'grid', 'url' => 'cotizacion/method_of_payment/listAll', 'params' => array('data' => $data)),
                array('view' => 'particular', 'label' => 'Backup', 'params' => array('file' => 'cotizacion/_backup_material', 'data' => array('backup' => $data['backup'], 'articulos_sustituidos' => $data['sustituidos'][0]['backup']))),
            );

            $view['view'] = $this->view_engine->tab($tabs);
            $this->setContent($this->load->view('detail', $view, TRUE));
        }
    }

    /**
     * Provee el acceso a la busqueda de los combos tipo select y sus valores en BD
     *
     * @access public
     * @author  Nohemi Rojas
     * @version V 1.0 8/10/12 01:17 PM
     */
    function EXC_searchSelect() {
        $id_letra = $this->input->get('letra');
        $data = $this->input->get('data');

        $value = $this->engine_model->getValueByCategory($id_letra);

        if ($value == 'Jurídico')
            $arr_values = $this->engine_model->getComboByTable('id_empresa', 'nombre', $data['q'], $label = 'text');

        if ($value == 'Natural')
            $arr_values = $this->engine_model->getComboByTable('id_persona', 'cedula,primer_nombre,primer_apellido', $data['q'], $label = 'text');

        $this->offOutput();

        echo json_encode(array('q' => $data['q'], 'results' => $arr_values));
    }

    /**
     * Formateo avanzado de los datos a insertar en cotizacion
     *
     * @access public
     * @param array $format_type Tipo de Formateo [INSERT o UPDATE].
     * @param array $data Datos para aplicar el formateo.
     * @return array Arreglo con los datos formateados segun la regla de negocio.
     * @author Nohemi Rojas
     * @version V 1.0 03/12/12 01:20 PM
     */
    function _avancedFormat($format_type, $data) {

        $format_type = strtoupper($format_type);
        $post = $this->input->post();
        $validations_cot = $this->validacion_model->getAll();
        $role = $this->session->userdata('role_name');

        if ($format_type == 'INSERT') {
            return prepareDataCotizacion($post, $data, $role, $validations_cot);
        }

        if ($format_type == 'VALIDATE') {
            $arr_data = array();
            foreach ($data as $key => $value) {
                //_cat corresponden con la  observacion de cada campo a validar dentro de cotizacion
                $temp = strstr($key, '_cat') ? '_cat' : NULL;
                if (!$temp)
                    continue;
                //limpia el campo para que quede con el nombre del campo, sin el sufijo _cat
                $field = preg_replace("/$temp/i", '', $key);

                //queda de la siguiente forma [persona_id]=>(_checked=> 'valor', _cat=>'Valor')
                $arr_data[$field][$temp] = ($value) ? $value : '0';
                $chk_value = $this->engine_model->getParentByCategory($value);
                if ($data['level_current'] == 'validador')
                    $arr_data[$field]['_checked'] = ($chk_value['table'] == 'validado') ? '1' : '0';
                if ($data['level_current'] == 'vendedor')
                    $arr_data[$field]['_checked'] = '0';
            }

            //Verifica si los siguientes datos fueron cambiados para actualizarlos
            $cotizacion_pago = array();
            if (isset($post['monto']))
                $cotizacion_pago['monto'] = $post['monto'];
            if (isset($post['deducible']) && !empty($post['deducible']))
                $cotizacion_pago['deducible'] = $post['deducible'];
            if (isset($post['fecha_instrumento']))
                $cotizacion_pago['fecha_instrumento'] = $post['fecha_instrumento'];

            return array('cotizacion_validacion' => $arr_data, 'cotizacion_pago' => $cotizacion_pago);
        }

        if ($format_type == 'ADDPAYMENT') {
            $cotizacion_validacion = array();
            foreach ($validations_cot as $value) {
                //Agrega los campos definidos en la entidad validacion
                if (in_array($value['campo'], array_keys($data))) {
                    $cotizacion_validacion[] = array(
                        'id_validacion' => $value['id'],
                        'cat_val_nivel' => $this->engine_model->getValueByCategory('validador', '_label'),
                        'id_cotizacion' => $this->session->userdata('Cotizacion_model.id'),
                    );
                }
            }

            //Viene vacio xq el la etiqueta html select no posee readonly
            //para hacer readonly si implemento disabled, para no actualizar
            //a vacio borramos ese campo.
            unset($data['cat_tipo_pago']);
            //Este registro no pertenece a la tabla cotizacion_pago
            unset($data['fecha_cirugia']);
            return array('cotizacion_pago' => $data, 'cotizacion_validacion' => $cotizacion_validacion);
        }
        if ($format_type == 'UPDATE') {
            unset($data['created_at']);
            unset($data['created_by']);
            $data['modified_by'] = $this->session->userdata('user_id');
            $data['modified_at'] = date("Y-m-d H:i:s");
            return $data;
        }

        //FORMATEO DE DATOS PARA SUSTITUCION DE ARTICULOS
        if ($format_type == 'REPLACEARTICLE') {
            //Formateo de datos para material sustituto.
            if ($data['mat_replace']):


                foreach ($data['mat_replace'] as $key => $value):
                    $cotizacion_articulo_replace_mat[$key]['id_cotizacion'] = $this->session->userdata("cot_replace.id");
                    $cotizacion_articulo_replace_mat[$key]['id_articulo'] = $key;
                    $cotizacion_articulo_replace_mat[$key]['precio'] = $data['precio_matReplace'][$key];
                    $cotizacion_articulo_replace_mat[$key]['cantidad'] = (empty($data['cant_mat_replace'][$key]) ? 0 : $data['cant_mat_replace'][$key]);
                    $cotizacion_articulo_replace_mat[$key]['id_parent'] = $value;
                endforeach;
            endif;

            //Formateo de datos para instrumental sustituto.
            if ($data['ins_replace']):


                foreach ($data['ins_replace'] as $key => $value):
                    $cotizacion_articulo_replace_ins[$key]['id_cotizacion'] = $this->session->userdata("cot_replace.id");
                    $cotizacion_articulo_replace_ins[$key]['id_articulo'] = $key;
                    $cotizacion_articulo_replace_ins[$key]['cantidad'] = (empty($data['cant_ins_replace'][$key]) ? 0 : $data['cant_ins_replace'][$key]);
                    $cotizacion_articulo_replace_ins[$key]['id_parent'] = $value;
                endforeach;
            endif;

            //Formateo de datos para backup sustituto.
            if ($data['backup_replace']):
                foreach ($data['backup_replace'] as $key => $value):
                    $cotizacion_articulo_replace_backup[$key]['id_cotizacion'] = $this->session->userdata("cot_replace.id");
                    $cotizacion_articulo_replace_backup[$key]['id_articulo'] = $key;
                    $cotizacion_articulo_replace_backup[$key]['precio'] = $data['precio_backupReplace'][$key];
                    $cotizacion_articulo_replace_backup[$key]['cantidad'] = (empty($data['cant_backup_replace'][$key]) ? 0 : $data['cant_backup_replace'][$key]);
                    $cotizacion_articulo_replace_backup[$key]['id_parent'] = $value;
                    $cotizacion_articulo_replace_backup[$key]['backup'] = '1';
                endforeach;
            endif;


            //Retornos de datos  ya formateados para sustitucion de articulos.
            return array(
                'cotizacion_articulo_replace_mat' => $cotizacion_articulo_replace_mat,
                'cotizacion_articulo_replace_ins' => $cotizacion_articulo_replace_ins,
                'cotizacion_articulo_replace_backup' => $cotizacion_articulo_replace_backup
            );
        }
    }

    /**
     * Validaciones extras realizadas a la entidad, que no las soporta el View_Engine
     *
     * @access public
     * @param array $data Datos para aplicar el formateo.
     * @return Mixed , string  Validaciones o FALSE en caso de que no tenga errores
     * @author Nohemi Rojas, Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.2 21/01/13 02:25 PM
     */
    private function _avancedFormValidation($format_type, $validate_data_tab = false) {

        $this->load->library('form_validation');
        $format_type = strtoupper($format_type);

        //Insercion
        if ($format_type == 'INSERT') {

            $post = $this->input->post();

            //Valida los elementos que se utilizan en esta parte de la validacion
            if (($post['cat_pagador'] == '') OR ($post['cat_tipo_pago'] == ''))
                return FALSE;
            if (!$validate_data_tab):
                //valida la existencia de material
                if (!isset($post['mat']))
                    $this->form_validation->set_rules('mat', 'Material', 'required');
                $this->form_validation->set_message('required', 'Debe seleccionar al menos un Material');
            endif;
            $pagador = $this->engine_model->getValueByCategory($post['cat_pagador']);

            //valida si el valor seleccionado del emisor de pagao es tercero
            if ($pagador == 'Tercero') {
                //Se verifica que se selecciono el Persona/empresa (N/J)
                if ($post['cat_letra'] == '')
                    $this->form_validation->set_rules('cat_letra', 'Tercero (Persona/Empresa)', 'required|integer|rewrite');

                //Se verifica que exista el id_tercero
                if (!isset($post['id_tercero']) or (isset($post['id_tercero']) and $post['id_tercero'] == '')) {
                    $this->form_validation->set_rules('id_tercero', 'Persona/Empresa', 'required|integer|rewrite');
                }
            }

            //valida si el valor seleccionado del emisor de pago es aseguradora
            if ($pagador == 'Aseguradora') {
                //Se verifica que exista el id_seguro
                if (!isset($post['id_seguro']) or (isset($post['id_seguro']) and $post['id_seguro'] == ''))
                    $this->form_validation->set_rules('id_seguro', 'Aseguradora', 'required|integer|rewrite');

                else {
                    $this->load->model('empresa/seguro_model', 'seguro_model');
                    $seguro = $this->seguro_model->getById($post['id_seguro'], 'id');

                    $tipo_seguro = $this->engine_model->getValueByCategory($seguro['cat_tipo_seguro']);

                    switch ($tipo_seguro) {

                        case 'Malo':
                            $this->form_validation->set_rules('id_seguro', 'Aseguradora', 'required|integer|rewrite');
                            $this->form_validation->set_message('rewrite', 'No se puede seleccionar la aseguradora, ya que no es confiable');
                            break;

                        case 'Regular':
                            //validacion especial que no permite carta aval
                            $cat_tipo_pago = $this->engine_model->getValueByCategory($post['cat_tipo_pago']);

                            if ($cat_tipo_pago == 'Carta Aval') {

                                $this->form_validation->set_rules('id_seguro', 'Aseguradora', 'required|integer|rewrite');
                                $this->form_validation->set_message('rewrite', 'La Aseguradora seleccionada no permite Carta Aval como medio de pago');
                            }else
                                $this->form_validation->set_rules('id_seguro', 'Aseguradora', 'required|integer');
                            break;

                        default:
                            $this->form_validation->set_rules('id_seguro', 'Aseguradora', 'required|integer');
                            break;
                    }
                }
            }
        }
        //FIN FORMAT INSERT
        //Medio de Pago
        if ($format_type == 'ADDPAYMENT') {
            $payment_type = $this->session->userdata("Cotizacion_model.tipo_pago");
            switch (strtolower($payment_type)) {
                case 'carta aval':
                    $this->form_validation->set_rules('vencimiento', 'Fecha de Vencimiento del Instrumento', 'required|valid_date');
                    break;

                case 'depósito':
                case 'cheque personal':
                case 'cheque de gerencia':
                    $this->form_validation->set_rules('cat_banco', 'Banco', 'required|integer');
                    break;
            }
        }
        if ($format_type == 'RESCHEDULESURGERY') {
            $id = $this->session->userdata("Cotizacion_model.id");
            $post = $this->input->post();
            if ($post['hora_cirugia'] == ''):
                $this->form_validation->set_rules('hora_cirugia', 'Hora de Cirugia', 'required');
            endif;
        }
        if ($format_type == 'APPROVEQUOTE') {
            $post = $this->input->post();
            if (!isset($post['id_instrumentista']))
                $this->form_validation->set_rules('id_instrumentista', 'Instrumentista', 'required');
            $this->form_validation->set_message('required', 'Debe agregar al menos un intrumentista');
        }
        if ($format_type == 'REPLACEARTICLE') {
            $post = $this->input->post();
            if (isset($post['mat'])):
                if (!isset($post['mat_replace']) && !isset($post['cant_mat_replace'])):
                    $this->form_validation->set_rules('mat_replace', 'Material Sustituto', 'required');
                    $this->form_validation->set_message('required', 'Debe agregar al menos un Material Sustituto');
                    return TRUE;
                endif;

            endif;
            if (isset($post['instru'])):

                if (!isset($post['ins_replace']) && !isset($post['cant_ins_replace'])):
                    $this->form_validation->set_rules('instru_replace', 'Instrumento Sustituto', 'required');

                    return TRUE;
                endif;

            endif;
            if (isset($post['backup'])):

                if (!isset($post['backup_replace']) && !isset($post['cant_backup_replace'])):
                    $this->form_validation->set_rules('backup_replace', 'Backup Sustituto', 'required');
                    $this->form_validation->set_message('required', 'Debe agregar al menos un Backup Sustituto');
                    return TRUE;
                endif;

            endif;
            return FALSE;
        }
    }

    /**
     * Formateo extendido de datos, se usa para aplicar el mismo formato a la operacion que guarda
     * en varias entidades
     *
     * @access public
     * @param array $data Datos para aplicar el formateo.
     * @return array Arreglo con los datos formateados segun la regla de negocio.
     * @author Nohemi Rojas
     * @version V 1.0 03/12/12 01:20 PM
     */
    private function _extendsFormat($data) {
        $data_ext = array();
        $data_ext['created_by'] = $data['created_by'];
        return $data_ext;
    }

    /**
     * Permite la adicion del medio de pago a ala cotizacion
     *
     * @access  public
     * @param   array    $params[OPTIONAL]  parametros adicionales.
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 30/01/13 11:04
     */
    public function addPayment($params = NULL) {
        $array_post = $this->input->post();
        $id = (isset($array_post['id'])) ? $this->encrypt->decode($array_post['id']) : NULL;
        if (count($array_post) == 1) {
            $data = $this->Cotizacion_model->getById($id);

            //almacenamos el real id de la cotizacion .
            $this->session->set_userdata("Cotizacion_model.id", $data['id']);
            $this->session->set_userdata("Cotizacion_model.tipo_pago", $data['tipo_pago']);

            $reload_content = 'cotizacion/reload_content.js.php';
            $view_params['form'] = $this->view_engine->form(array('data' => $data, 'reload_content' => $reload_content));
            $view = $this->load->view('addPayment', $view_params, TRUE);
            $this->setTargetOn('window2');
            $this->setContent($view);
        } elseif ($params[0] == 'process') {

            //Agregamos las validaciones particulares segun el tipo de pago.
            $this->_avancedFormValidation('addPayment');

            $process = $this->view_engine->process();

            $success = FALSE;
            if ($process['result']) {
                $id = $this->session->userdata("Cotizacion_model.id");

                $params = array('format_type' => 'addPayment', 'data' => $process['data']);

                //EJECUTA formateo avanzado
                $data = $this->_format($params);
                $success = $this->Cotizacion_model->addPayment($id, $data);

                if ($success)
                    $this->session->unset_userdata("Cotizacion_model.id");

                $this->setContent($success);
            } else {
                $this->setContent($process['validation_error']);
            }

            // -----------------------------
            //seteamos los datos de auditoria
            $operation_data = $process;
            $this->audit->setChkResult($success);
            // -----------------------------
        }

        // -----------------------------
        //seteamos los datos de auditoria
        $operation_data['id'] = $id;
        $this->audit->setOperationData($operation_data);
        // -----------------------------
    }

    /**
     * Genera PDF de la Cotizacion
     *
     * @access public
     * @param array $params Parametros necesarios para Generar el PDF     *
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 10/06/13 11:37 AM
     */
    public function EXC_create_pdf_cotizacion($params = array(), $background_no_validate = FALSE, $membrete = FALSE) {
        /**
         * Descripcion detallada de variables recibidas.
         *
         * Este arreglo se pasa en el arreglo $params
         * $materiales[]=array(
         *              'codigo_m'      => string,
         *              'modelo_m'      => string,
         *              'descripcion_m' => string,
         *              'gart_m'        => string,
         *              'cant_m'        => string,
         *              'precio_m'      => string,
         *              'neto_m'        => string
         *          );
         *
         * Arreglo principal del metodo
         * $params = array(
         *              'nro_cotizacion'  => string,
         *              'fecha_emision'   => string,
         *              'forma_pago'      => string,
         *              'tiempo_entrega'  => string,
         *              'vendedor'        => string,
         *              'dr_tratante'     => string,
         *              'cliente'         => string,
         *              'rif'             => string,
         *              'nit'             => string,
         *              'seguro'          => string,
         *              'tlf'             => string,
         *              'monto_exento'    => string,
         *              'monto_gravable'  => string,
         *              'descuento'       => string,
         *              'recargo'         => string,
         *              'iva'             => string,
         *              'neto'            => string,
         *              'materiales'      => array()
         *          );
         */
        $dir_modo_impresion = ($membrete == 'true' ? 'membrete_on/' : 'membrete_off/');

        $this->lang->load('app_doc');
        $this->lang->load('app_email');
        $this->load->library('encrypt');
        $this->load->helper('view_engine_helper');
        extract($params);
        //Cadena que contiene el string para generar el pdf
        $html = $this->load->view('pdf_cotizacion', $params, TRUE);
        // -----------------------------
        // -----------------------------
        //Asignacion de nombre, ruta y generacion del documento
        $name_file = "cot_$nro_cotizacion";
        $destination_path = 'uploads/cotizaciones/' . $dir_modo_impresion;
        $destination_path = ($background_no_validate ? $destination_path . "preview/" : $destination_path);

        generate_pdf($name_file, $destination_path, 'F', $html, $background_no_validate, $membrete);
        return utf8_decode("{$destination_path}" . "{$name_file}.pdf");
        // -----------------------------
    }

    /**
     * Reprograma la la fecha de la cirugia
     *
     * @access public
     * @param mixed $param Datos de entrada para trabajar la vista.
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 23/07/13 02:37 PM
     */
    public function rescheduleSurgery($params) {

        $array_post = $this->input->post();
        $id = (isset($array_post['id'])) ? $this->encrypt->decode($array_post['id']) : NULL;
        if ($id) {
            $data = $this->Cotizacion_model->getById($id);

            $observacion = $this->Cotizacion_model->getObservacionById($id);
            $data['descripcion'] = $observacion[0]['descripcion'];
            $data['fecha_tentativa'] = $observacion[0]['fecha_tentativa'];
            //almacenamos el  id real de la cotizacion .
            $this->session->set_userdata("Cotizacion_model.id", $data['id']);

            $reload_content = 'cotizacion/reload_content.js.php';
            $view_params['form'] = $this->view_engine->form(array('data' => $data, 'reload_content' => $reload_content));
            $view = $this->load->view('rescheduleSurgery', $view_params, TRUE);
            $this->setTargetOn('window2');
            $this->setContent($view);
        } elseif ($params[0] == 'process') {

            //Agregamos las validaciones particulares.
            $this->_avancedFormValidation('rescheduleSurgery');

            $process = $this->view_engine->process();

            $success = FALSE;
            if ($process['result']) {
                $id = $this->session->userdata("Cotizacion_model.id");

                //------Se aplica formateo particular para la Hora de la cirugia-----
                $tmp_data = array();
                $tmp_data = $process['data'];
                unset($process['data']);
                unset($tmp_data['hora_cirugia']);
                unset($tmp_data['meridian']);
                $tmp_data['hora_cirugia'] = $tmp_data['hour'] . ":" . $tmp_data['minute'] . ":00";
                unset($tmp_data['hour']);
                unset($tmp_data['minute']);
                unset($tmp_data['descripcion']);
                unset($tmp_data['fecha_tentativa']);
                $process['data'] = $tmp_data;
                unset($tmp_data);
                //------------------------------------------------------------------//

                $params = array('format_type' => 'update', 'data' => $process['data']);

                //EJECUTA formateo avanzado
                $data = $this->_format($params);
                $success = $this->Cotizacion_model->rescheduleSurgery($id, $data);

                if ($success)
                    $this->session->unset_userdata("Cotizacion_model.id");

                $this->setContent($success);
            } else {
                $this->setContent($process['validation_error']);
            }

            // -----------------------------
            //seteamos los datos de auditoria
            $operation_data = $process;
            $this->audit->setChkResult($success);
            // -----------------------------
        }

        // -----------------------------
        //seteamos los datos de auditoria
        $operation_data['id'] = $id;
        $this->audit->setOperationData($operation_data);
        // -----------------------------
    }

    /**
     * Aprueba la cotizacion
     *
     * @access public
     * @param mixed Datos de entrada para trabajar la vista.
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 23/07/13 02:42 PM
     */
    public function approveQuote($params) {

        $array_post = $this->input->post();
        $id = (isset($array_post['id'])) ? $this->encrypt->decode($array_post['id']) : NULL;
        if ($id) {
            $data = $this->Cotizacion_model->getById($id);

            //almacenamos el  id real de la cotizacion y de la cirugia.
            $this->session->set_userdata("Cotizacion_model.id", $data['id']);
            $this->session->set_userdata("Cotizacion_model.id_cirugia", $data['cod_articulacion']);
            $view_params['data'] = $data;
            $view_params['no_disponibilidad'] = $this->delivery_model->availabilityOfItemsByCot($data['id']);
            $view_params['id_ec'] = $this->encrypt->encode($data['id']);

            $view = $this->load->view('approveQuote', $view_params, TRUE);
            $this->setTargetOn('window2');
            $this->setContent($view);
        } elseif ($params[0] == 'process') {

            //Agregamos las validaciones particulares.
            $this->_avancedFormValidation('approveQuote');

            $process = $this->view_engine->process();

            $success = FALSE;
            if ($process['result']) {
                $id = $this->session->userdata("Cotizacion_model.id");
                $id_cirugia = $this->session->userdata("Cotizacion_model.id_cirugia");

                $params = array('format_type' => 'insert', 'data' => $process['data']);
                $id_instrumentista = $params['data']['id_instrumentista'];
                //Datos de el/los  instrumentista(s)
                foreach ($id_instrumentista as $key => $value) {
                    $id_instrumentista[$key] = array(
                        'id_cotizacion' => $id,
                        'id_instrumentista' => $value,
                        'created_by' => $this->session->userdata('user_id'));
                }
                $params['data']['created_by'] = $this->session->userdata('user_id');
                unset($params['data']['id_instrumentista']);
                $params['id_instrumentista'] = $id_instrumentista;
                $data_insert['id_instrumentista'] = $params['id_instrumentista'];
                $data_insert['id'] = $id;

                $success = $this->Cotizacion_model->approveQuote($data_insert);

                if ($success)
                    $this->session->unset_userdata("Cotizacion_model.id");
                $this->session->unset_userdata("Cotizacion_model.id_cirugia");

                $this->setContent($success);
            } else {
                $this->setContent($process['validation_error']);
            }

            // -----------------------------
            //seteamos los datos de auditoria
            $operation_data = $process;
            $this->audit->setChkResult($success);
            // -----------------------------
        }

        // -----------------------------
        //seteamos los datos de auditoria
        $operation_data['id'] = $id;
        $this->audit->setOperationData($operation_data);
        // -----------------------------
    }

    /**
     * Crea los documentos pertinentes a la cotizacion
     *
     * @access  private
     * @param   array   $params Arreglos de datos a mostrar en el documento
     * @param boolean $background_no_validate Indica si el documento lleva o no la marca NO VALIDO de fondo
     * @param string $document Indica el tipo de documento a crear
     * @param boolean $membrete Determina si el documento lleva o no el fondo predeterminado
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 02/12/13 11:38 AM
     */
    function _create_document($params = array(), $background_no_validate = FALSE, $document = '', $membrete = FALSE) {
        $this->lang->load('app_doc');
        $this->lang->load('app_email');
        $this->load->library('encrypt');
        $this->load->helper('view_engine_helper');
        extract($params);
        $dir_modo_impresion = ($membrete == 'true' ? 'membrete_on' : 'membrete_off');
        switch ($document) {
            case 'cotizacion':
                $view = 'pdf_cotizacion';
                $name = 'cot';
                $dir = 'cotizaciones/' . $dir_modo_impresion;
                break;
            case 'backup':
                $view = 'pdf_backup';
                $name = 'backup';
                $dir = 'cotizaciones/' . $dir_modo_impresion . '/backup';
                break;
            case 'instrumento':
                $view = 'pdf_instrumentos';
                $name = 'instru';
                $dir = 'cotizaciones/' . $dir_modo_impresion . '/instrumentos';
                break;

            default:
                $view = 'pdf_cotizacion';
                $name = 'cot';
                break;
        }
        //Cadena que contiene el string para generar el pdf
        $html = $this->load->view($view, $params, TRUE);
        // -----------------------------
        // -----------------------------
        //Asignacion de nombre, ruta y generacion del documento
        $name_file = "{$name}_{$nro_cotizacion}";
        $destination_path = "uploads/{$dir}/";
        $destination_path = ($background_no_validate ? $destination_path . "preview/" : $destination_path);

        generate_pdf($name_file, $destination_path, 'F', $html, $background_no_validate, $membrete);
        return utf8_decode("{$destination_path}" . "{$name_file}.pdf");
        // -----------------------------
    }

    /**
     * Genera o ubica documentos de la cotizacion de acuerdo al tipo de documento.
     *
     * @access public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 09/08/13 09:46 AM
     */
    public function EXC_quoteReport() {
        $this->offOutput();
        $id = $this->input->post('id');
        $type_report = $this->input->post('tipo');
        $membrete = $this->input->post('membrete');
        $dir_modo_impresion = ($membrete == 'true' ? 'membrete_on' : 'membrete_off');
        $alvuelo = $this->config->item('pdf_al_vuelo');
        if ($id):
            $id = $this->encrypt->decode($id);

            $data = $this->Cotizacion_model->get_data_cotizacion($id);
            $data = $data[0];

            switch ($type_report) {
                case 'backup':
                    $filename = "uploads/cotizaciones/{$dir_modo_impresion}/backup/backup_$data[codigo].pdf";
                    if (file_exists($filename) && !$alvuelo):
                        $response = array(
                            'sucess' => true,
                            'file' => $filename
                        );
                    else:
                        $getDatosPdf = $this->Cotizacion_model->getDatosPdf($data['id_medico'], $data['id_seguro'], $data['id_paciente'], $data['cat_tipo_pago'], $data['id_vendedor']);
                        $datos_paciente = $this->paciente_model->getPaciente($data['id_paciente'], 'tlf_movil');
                        $params_pdf = array(
                            'nro_cotizacion' => $data['codigo'],
                            'fecha_emision' => date('d/m/Y'),
                            'forma_pago' => $getDatosPdf['tipo_pago'],
                            'tiempo_entrega' => 'INMEDIATA',
                            'vendedor' => $getDatosPdf['vendedor'],
                            'dr_tratante' => $getDatosPdf['medico'],
                            'cliente' => $getDatosPdf['pnombre'],
                            'rif' => $getDatosPdf['pcedula'],
                            'nit' => '',
                            'seguro' => ($getDatosPdf['nseguro'] == null ? 'No Aplica' : $getDatosPdf['nseguro']),
                            'tlf' => (empty($datos_paciente[0]['tlf_movil']) || $datos_paciente[0]['tlf_movil'] == null ? 'Sin Informacion' : $datos_paciente[0]['tlf_movil']),
                        );
                        $params_pdf['cuerpo_tabla'] = $this->Cotizacion_model->getArticulosByCot($data['id'], 'MAT', '1');
                        $response = array(
                            'sucess' => true,
                            'file' => $this->_create_document($params_pdf, false, 'backup', $membrete)
                        );
                    endif;

                    break;
                case 'instrumento':
                    $filename = "uploads/cotizaciones/{$dir_modo_impresion}/instrumentos/instru_$data[codigo].pdf";
                    if (file_exists($filename) && !$alvuelo):
                        $response = array(
                            'sucess' => true,
                            'file' => $filename
                        );
                    else:

                        $getDatosPdf = $this->Cotizacion_model->getDatosPdf($data['id_medico'], $data['id_seguro'], $data['id_paciente'], $data['cat_tipo_pago'], $data['id_vendedor']);
                        $datos_paciente = $this->paciente_model->getPaciente($data['id_paciente'], 'tlf_movil');
                        $params_pdf = array(
                            'nro_cotizacion' => $data['codigo'],
                            'fecha_emision' => date('d/m/Y'),
                            'forma_pago' => $getDatosPdf['tipo_pago'],
                            'tiempo_entrega' => 'INMEDIATA',
                            'vendedor' => $getDatosPdf['vendedor'],
                            'dr_tratante' => $getDatosPdf['medico'],
                            'cliente' => $getDatosPdf['pnombre'],
                            'rif' => $getDatosPdf['pcedula'],
                            'nit' => '',
                            'seguro' => ($getDatosPdf['nseguro'] == null ? 'No Aplica' : $getDatosPdf['nseguro']),
                            'tlf' => (empty($datos_paciente[0]['tlf_movil']) || $datos_paciente[0]['tlf_movil'] == null ? 'Sin Informacion' : $datos_paciente[0]['tlf_movil']),
                        );
                        $params_pdf['cuerpo_tabla'] = $this->Cotizacion_model->getArticulosByCot($data['id'], 'INS');
                        $response = array(
                            'sucess' => true,
                            'file' => $this->_create_document($params_pdf, false, 'instrumento', $membrete)
                        );
                    endif;
                    break;
                case 'cotizacion':
                    $filename = "uploads/cotizaciones/{$dir_modo_impresion}/cot_$data[codigo].pdf";
                    if (file_exists($filename) && !$alvuelo):
                        $response = array(
                            'sucess' => true,
                            'file' => $filename
                        );
                    else:
                        $getDatosPdf = $this->Cotizacion_model->getDatosPdf($data['id_medico'], $data['id_seguro'], $data['id_paciente'], $data['cat_tipo_pago'], $data['id_vendedor']);
                        $datos_paciente = $this->paciente_model->getPaciente($data['id_paciente'], 'tlf_movil');
                        $params_pdf = array(
                            'nro_cotizacion' => $data['codigo'],
                            'fecha_emision' => date('d/m/Y'),
                            'forma_pago' => $getDatosPdf['tipo_pago'],
                            'tiempo_entrega' => 'INMEDIATA',
                            'vendedor' => $getDatosPdf['vendedor'],
                            'dr_tratante' => $getDatosPdf['medico'],
                            'cliente' => $getDatosPdf['pnombre'],
                            'rif' => $getDatosPdf['pcedula'],
                            'nit' => '',
                            'seguro' => ($getDatosPdf['nseguro'] == null ? 'No Aplica' : $getDatosPdf['nseguro']),
                            'tlf' => (empty($datos_paciente[0]['tlf_movil']) || $datos_paciente[0]['tlf_movil'] == null ? 'Sin Informacion' : $datos_paciente[0]['tlf_movil']),
                        );
                        $getMaterialesPdf = $this->Cotizacion_model->getArticulosByCot($data['id'], 'MAT');
                        foreach ($getMaterialesPdf as $clave => $valor):

                            $materiales[] = array(
                                'codigo_m' => $valor['id'],
                                'modelo_m' => $valor['modelo'],
                                'descripcion_m' => $valor['descripcion'],
                                'gart_m' => '',
                                'cant_m' => $valor['cantidad'],
                                'precio_m' => number_format($valor['precio'], 2, ',', '.'),
                                'neto_m' => number_format($valor['precio'] * $valor['cantidad'], 2, ',', '.')
                            );
                        endforeach;
                        $params_pdf['monto_exento'] = number_format($data['monto_exento'], 2, ',', '.');
                        $params_pdf['monto_gravable'] = number_format($data['monto_gravable'], 2, ',', '.');
                        $params_pdf['porcentaje_descuento'] = $data['descuento'];
                        $params_pdf['calculo_descuento'] = number_format(($data['monto_gravable'] * $data['descuento']) / 100, 2, ',', '.');
                        $params_pdf['recargo'] = number_format(($data['monto_gravable'] * $data['recargo']) / 100, 2, ',', '.');
                        $params_pdf['p_recargo'] = $data['recargo'];
                        $params_pdf['porcentaje_iva'] = $this->config->item('valor_iva');
                        $params_pdf['calculo_iva'] = number_format(($data['monto_gravable'] * $this->config->item('valor_iva')) / 100, 2, ',', '.');
                        $params_pdf['neto'] = number_format($data['monto_total'], 2, ',', '.');
                        $params_pdf['materiales'] = $materiales;
                        $response = array(
                            'sucess' => true,
                            'file' => $this->_create_document($params_pdf, false, 'cotizacion', $membrete)
                        );
                    endif;
                    break;
                default:
                    $params_pdf['cuerpo_tabla'] = array();
                    break;
            }
        else:
            $response = array(
                'sucess' => false
            );
        endif;
        echo json_encode($response);
    }

    /**
     * Se encarga de mostrar los materiales, instrumentos y backup que corresponden a una cirugia, recargando un
     * div hmtl, esta funcion apaga el output y muestra la vista.
     *
     * @access public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 12/08/13 02:57 PM
     */
    function EXC_cuerpo_cotizacion() {
        $params = $this->input->get();
        $this->offOutput();
        $this->load->helper('ast');
        $this->load->model('cotizacion/articulo_model', 'articulo_model');
        $data1['materiales'] = $this->articulo_model->articuloRecomendacion($params['cirugia'], 'MAT');
        if ($data1['materiales']):
            foreach ($data1['materiales'] as &$material) {
                $material['arr_precio'] = pg_array_to_php($material['arr_precio']);
                $material['arr_id_precio'] = pg_array_to_php($material['arr_id_precio']);
            }
        endif;

        $data1['operation'] = 'create';
        $data1['tipo_precio'] = $params['tipo_precio'];


        $data2['instrumentos'] = $this->articulo_model->articuloRecomendacion($params['cirugia'], 'INS');
        //parametros necesario para el tab
        $data2['url'] = 'cotizacion/cotizacion/create';
        $data2['redirect'] = 'cotizacion/cotizacion/listAll';
        $data2['operation'] = 'create';

        $data3['backup'] = $this->articulo_model->articuloRecomendacion($params['cirugia'], 'BACKUP');
        if ($data3['backup']):
            foreach ($data3['backup'] as &$material3) {
                $material3['arr_precio'] = pg_array_to_php($material3['arr_precio']);
                $material3['arr_id_precio'] = pg_array_to_php($material3['arr_id_precio']);
            }
            $data3['tipo_precio'] = $params['tipo_precio'];
        endif;




        $data3['operation'] = 'create';

        $response = array(
            'instrumentos' => $this->load->view('_instrumentos', $data2, true),
            'materiales' => $this->load->view('_materiales', $data1, true),
            'backup' => $this->load->view('_create_backup', $data3, true)
        );
        echo json_encode($response);
    }

    /**
     * cambia el estatus de la cotizacion a reprogramar cirugia
     *
     * @access public
     * @param mixed $param Datos de entrada para trabajar la vista.
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 23/07/13 02:37 PM
     */
    public function changeSurgery($params) {
        $array_post = $this->input->post();
        $id = (isset($array_post['id'])) ? $this->encrypt->decode($array_post['id']) : NULL;
        if ($id) {
            $data = $this->Cotizacion_model->getById($id);

            //almacenamos el  id real de la cotizacion .
            $this->session->set_userdata("Cotizacion_model.id", $data['id']);

            $reload_content = 'cotizacion/reload_content.js.php';
            $view_params['form'] = $this->view_engine->form(array('button_submit' => 'Si, Reprogramar Cirugia', 'data' => $data, 'reload_content' => $reload_content));
            $view = $this->load->view('changeSurgery', $view_params, TRUE);
            $this->setTargetOn('window2');
            $this->setContent($view);
        } elseif ($params[0] == 'process') {

            //Agregamos las validaciones particulares.
            // $this->_avancedFormValidation('approveQuote');

            $process = $this->view_engine->process();
//            print_r($process); die;
            $success = FALSE;
            if ($process['result']) {
                $id = $this->session->userdata("Cotizacion_model.id");

                $success = $this->Cotizacion_model->changeSurgery($id, $process['data']);

                if ($success)
                    $this->session->unset_userdata("Cotizacion_model.id");

                $this->setContent($success);
            } else {
                $this->setContent($process['validation_error']);
            }

            // -----------------------------
            //seteamos los datos de auditoria
            $operation_data = $process;

            $this->audit->setChkResult($success);
            // -----------------------------
        }

        // -----------------------------
        //seteamos los datos de auditoria
        $operation_data['id'] = $id;
        $this->audit->setOperationData($operation_data);
        // -----------------------------
    }

    /**
     * Forza la descarga de un archivo pdf
     *
     * @access public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 19/08/13 11:18 AM
     */
    function EXC_force_download() {
        $array_get = $this->input->get();
        $this->offOutput();
        $array_url = explode('_', $array_get['url']);
        $array_url_invertido = array_reverse($array_url);


        $len = filesize($array_get['url']);
        header('Content-type: application/pdf');
        header('Content-Disposition: attachment; filename="' . $array_get['name'] . '_' . $array_url_invertido[0] . '"');
        header('Content-Length: ' . $len);
        readfile($array_get['url']);
    }

    /**
     * Se encarga de mostrar los materiales, instrumentos y backup que corresponden a una cirugia, recargando un
     * div hmtl, esta funcion apaga el output y muestra la vista.
     *
     * @access public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 12/08/13 02:57 PM
     */
    function EXC_get_direction() {
        $params = $this->input->get();
        $succes = false;
        $this->offOutput();
        if ($params['tipo_direccion'] == 'medico'):
            $data = $this->medico_model->get_direction_medico($params['id_medico']);
        endif;
        if ($params['tipo_direccion'] == 'centro'):
            $data = $this->medico_model->get_direction_medico($params['id_centro']);
        endif;

        if ($data):
            $succes = true;
        endif;
        $response = array(
            'data' => $data,
            'succes' => $succes
        );
        echo json_encode($response);
    }

    /**
     * programa la fecha de la cirugia
     *
     * @access public
     * @param mixed $param Datos de entrada para trabajar la vista.
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>, Eliezer Pe#a
     * @version 1.0 23/07/13 02:37 PM
     */
    public function scheduleSurgery($params) {

        $array_post = $this->input->post();
        $id = (isset($array_post['id'])) ? $this->encrypt->decode($array_post['id']) : NULL;
        if ($id) {
            $data = $this->Cotizacion_model->getById($id);

            //almacenamos el  id real de la cotizacion .
            $this->session->set_userdata("Cotizacion_model.id", $data['id']);

            $reload_content = 'cotizacion/reload_content.js.php';
            $view_params['form'] = $this->view_engine->form(array('data' => $data, 'reload_content' => $reload_content));
            $view = $this->load->view('rescheduleSurgery', $view_params, TRUE);
            $this->setTargetOn('window2');
            $this->setContent($view);
        } elseif ($params[0] == 'process') {

            //Agregamos las validaciones particulares.
            $this->_avancedFormValidation('rescheduleSurgery');

            $process = $this->view_engine->process();

            $success = FALSE;
            if ($process['result']) {
                $id = $this->session->userdata("Cotizacion_model.id");

                //------Se aplica formateo particular para la Hora de la cirugia-----
                $tmp_data = array();
                $tmp_data = $process['data'];
                unset($process['data']);
                unset($tmp_data['hora_cirugia']);
                unset($tmp_data['meridian']);
                $tmp_data['hora_cirugia'] = $tmp_data['hour'] . ":" . $tmp_data['minute'] . ":00";
                unset($tmp_data['hour']);
                unset($tmp_data['minute']);
                $process['data'] = $tmp_data;
                unset($tmp_data);
                //------------------------------------------------------------------//

                $params = array('format_type' => 'update', 'data' => $process['data']);

                //EJECUTA formateo avanzado
                $data = $this->_format($params);
                $success = $this->Cotizacion_model->scheduleSurgery($id, $data);

                if ($success)
                    $this->session->unset_userdata("Cotizacion_model.id");

                $this->setContent($success);
            } else {
                $this->setContent($process['validation_error']);
            }

            // -----------------------------
            //seteamos los datos de auditoria
            $operation_data = $process;
            $this->audit->setChkResult($success);
            // -----------------------------
        }

        // -----------------------------
        //seteamos los datos de auditoria
        $operation_data['id'] = $id;
        $this->audit->setOperationData($operation_data);
        // -----------------------------
    }

    /**
     * Cambia el estatus de la cotizacion a sustituir_articulos
     *
     * @access  public
     * @param   integer $id Identificador de la cotizacion
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 31/10/13 10:23 AM
     */
    function EXC_changeToStatusReplaceArticles() {
        $this->offOutput();
        $response = array('view' => TRUE);
        $id_cotizacion = $this->encrypt->decode($this->input->post('id'));
        $change_estatus = $this->Cotizacion_model->replaceArticles($id_cotizacion);
        if ($change_estatus):
            $response = array('view' => TRUE);
        endif;
        echo json_encode($response);
    }

    /**
     * Depliega la interfaz para sustituir articulos en la cotizacion
     *
     * @access  public
     * @param   array   $params Parametros de entrada
     * @author  Frederick D. Bustamante G.
     * @version 1.0 05/11/13 09:51 AM
     */
    function replaceArticle($params = NULL) {

        $array_post = $this->input->post();
        $validations = FALSE;

        if (isset($params[0]) && $params[0] == 'process'):

            $output['result'] = true;
            $success = FALSE;
            $validations = $this->_avancedFormValidation('replacearticle');
            if ($validations && $this->form_validation->run() == FALSE) :

                $output['validation_error'] = validation_errors();
                $output['result'] = false;
                $this->setContent($output['validation_error']);
            endif;
            if ($output['result']):
                $params = array('format_type' => 'replacearticle', 'data' => $array_post);
                $data = $this->_format($params);
                $success = $this->Cotizacion_model->replace_articles($data);
                $this->setContent($success);
            endif;

            // -----------------------------
            //seteamos los datos de auditoria
            $this->audit->setOperationData($output);
            $this->audit->setChkResult($success);
        // -----------------------------
        else:
            $id_ec = $array_post['id'];
            $array_post['id'] = $this->encrypt->decode($array_post['id']);
            $this->session->set_userdata("cot_replace.id", $array_post['id']);
            if ($array_post['id']) {
                $tab_datos = array(
                    'operation' => '',
                    'datos' => $this->Cotizacion_model->getById($array_post['id'])
                );
                $data['cotizacion'] = $this->Cotizacion_model->getById($array_post['id']);
                $data['materiales'] = $this->Cotizacion_model->getArticulosByCot($array_post['id'], 'MAT');
                $data['backup'] = $this->Cotizacion_model->getArticulosByCot($array_post['id'], 'MAT', '1');
                $data['instrumentos'] = $this->Cotizacion_model->getArticulosByCot($array_post['id'], 'INS');


                $tabs = array(
                    '1-0' => array('view' => 'particular', 'label' => 'Datos', 'params' => array('file' => 'cotizacion/replace/_replace_datos', 'data' => $tab_datos)),
                    '1-1' => array('view' => 'particular', 'label' => 'Materiales', 'params' => array('file' => 'cotizacion/replace/_replaceMaterial', 'data' => array('operation' => 'detail', 'materiales' => $data['materiales']))),
                    '2-1' => array('view' => 'particular', 'label' => 'Instrumental', 'params' => array('file' => 'cotizacion/replace/_replaceInstrumento', 'data' => array('operation' => 'detail', 'instrumentos' => $data['instrumentos']))),
                    '3-1' => array('view' => 'particular', 'label' => 'Backup', 'params' => array('file' => 'cotizacion/replace/_replaceBackup', 'data' => array('id_ec' => $id_ec, 'operation' => 'detail', 'uri_content' => (isset($array_post['content']) && $array_post['content'] ? TRUE : FALSE), 'backup' => $data['backup']))),
                        //'4-1' => array('view' => 'particular', 'label' => 'Detalle', 'params' => array('file' => 'cotizacion/replace/_replaceDetalle', 'data' => array('id_ec' => $id_ec, 'operation' => 'detail', 'uri_content' => (isset($array_post['content']) && $array_post['content'] ? TRUE : FALSE))))
                );
                $view['view'] = $this->view_engine->tab($tabs);


                $this->setContent($this->load->view('replace/replaceArticle', $view, TRUE));
            }
        endif;
    }

    /**
     * Retorna los articulos sustituidos con sus respectivos sustitutos
     *
     * @access  public
     * @param   array   $data Parametros de entrada
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 08/11/13 10:29 AM
     */
    private function _obtainReplacements($data = array()) {

        if ($data['materiales']):
            foreach ($data['materiales'] as $key => $value):
                if ($this->Cotizacion_model->checkReplaceArticle($value['id'])):
                    $data['materiales'] [$key]['sustituidos'] = $this->Cotizacion_model->checkReplaceArticle($value['id']);
                else:
                    unset($data['materiales'] [$key]);
                endif;

            endforeach;
        endif;

        if ($data['instrumentos']):
            foreach ($data['instrumentos'] as $key => $value):
                if ($this->Cotizacion_model->checkReplaceArticle($value['id'])):
                    $data['instrumentos'] [$key]['sustituidos'] = $this->Cotizacion_model->checkReplaceArticle($value['id']);
                else:
                    unset($data['instrumentos'] [$key]);
                endif;
            endforeach;
        endif;

        if ($data['backup']):
            foreach ($data['backup'] as $key => $value):
                if ($this->Cotizacion_model->checkReplaceArticle($value['id'])):
                    $data['backup'] [$key]['sustituidos'] = $this->Cotizacion_model->checkReplaceArticle($value['id']);
                else:
                    unset($data['backup'] [$key]);
                endif;
            endforeach;
        endif;

        return $data;
    }

}