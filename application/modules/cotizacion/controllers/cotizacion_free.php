<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Cotizacion Libre
 *
 * Esta clase provee la implementacion para la capa de control de
 * acciones de la entidad libre_cotizacion y sus entidades dependientes o extendidas
 *
 * @package Module
 * @category Controller
 * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V-1.0 07/01/14 10:00 AM
 */
class Cotizacion_free extends AST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('cotizacion/cotizacion_free_model', 'Cotizacion_free_model');
        $this->load->model('cotizacion/cotizacion_model', 'Cotizacion_free_model');
        $this->load->model('delivery/delivery_free_model', 'delivery_free_model');
        $this->load->model('persona/empleado_model', 'empleado_model');
        $this->load->model('persona/paciente_model', 'paciente_model');
        $this->load->model('inventario/articulo_model', 'articulo_model');
        $this->load->model('settings/categories_model', 'categories_model');
        $this->load->model('persona/persona_model', 'persona_model');
        $this->load->model('empresa/empresa_model', 'empresa_model');
        $this->load->helper('ast');
    }

    /**
     * Provee el acceso al formulario de ingreso de nuevos datos, asi mismo,
     * provee el acceso a la insercion de datos nuevos dentro en base de datos.
     *
     * @access public
     * @param array $params Parametros adicionales enviados al controlador.
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 07/01/14 12:26 PM
     */
    function create($params = NULL) {
        $modo = $params;
        if (!$this->input->post()) {

            //Arreglo de datos para la pestana datos
            $tab_datos = array(
                'operation' => 'create',
                'tipo_persona' => $this->categories_model->getCategory(array('_table' => 'letra'), 'id,_label'),
                'tipo_pago' => $this->categories_model->getCategory(array('_table' => 'tipo_pago'), 'id,_label'),
            );

            unset($tab_datos["tipo_pago"][0]);

            $tabs = array(
                '1' => array('view' => 'particular', 'label' => 'Datos', 'params' => array('file' => 'cotizacion/cotizacion_free/_datos', 'data' => $tab_datos)),
                '2' => array('view' => 'particular', 'label' => 'Articulos', 'params' => array('file' => 'cotizacion/cotizacion_free/_articulos', 'data' => array('operation' => 'create'))),
                '3' => array('view' => 'particular', 'label' => 'Totales', 'params' => array('file' => 'cotizacion/cotizacion_free/_totales', 'data' => array('operation' => 'create')))
            );
            $view['view'] = $this->view_engine->tab($tabs);

            $this->setContent($this->load->view('cotizacion_free/create/create', $view, TRUE));
        } elseif ($params[0] == 'process') {
            $success = FALSE;
            if (isset($modo[1]) && $modo[1] == 'validate_data'):
                $this->_avancedFormValidation('insert', TRUE);
            elseif (isset($modo[1]) && $modo[1] == 'preview'):
                $this->_avancedFormValidation('insert');
            else:
                $this->_avancedFormValidation('insert');
            endif;
            $process = $this->view_engine->process();
            if ($process['result']) {
                if (isset($modo[1]) && $modo[1] == 'validate_data'):
                    $this->offOutput();
                    echo json_encode(array('validate' => true));

                else:
                    if (isset($modo[1]) && $modo[1] == 'preview'):
                        $process['data']['preview'] = true;
                    else:
                        $process['data']['preview'] = false;
                    endif;
                    $params = array('format_type' => 'insert', 'data' => $process['data']);
                    $data = $this->_format($params);

                    $getMaterialesPdf = $this->Cotizacion_free_model->getMaterialesPdf($data['cotizacion_articulo']);

                    $getDatosPdf = $this->Cotizacion_free_model->getDatosPdf(0, $data['cotizacion_art']['id_vendedor'], $data['cotizacion_art']['id_cliente']);

                    foreach ($getMaterialesPdf as $clave => $valor):
                        $materiales[] = array(
                            'codigo_m' => $valor[0]['id'],
                            'modelo_m' => $valor[0]['modelo'],
                            'descripcion_m' => $valor[0]['descripcion'],
                            'gart_m' => '',
                            'cant_m' => $valor[0]['cantidad'],
                            'precio_m' => number_format($valor[0]['precio'], 2, ',', '.'),
                            'neto_m' => number_format($valor[0]['precio'] * $valor[0]['cantidad'], 2, ',', '.')
                        );
                    endforeach;


                    $params_pdf = array(
                        'nro_cotizacion' => $data['cotizacion_art']['codigo'],
                        'fecha_emision' => date('d/m/Y'),
                        'forma_pago' => $getDatosPdf['tipo_pago'],
                        'tiempo_entrega' => 'INMEDIATA',
                        'vendedor' => $getDatosPdf['vendedor'],
                        'cliente' => $getDatosPdf['cliente']['nombre_cliente'],
                        'rif' => $getDatosPdf['cliente']['cedula_rif'],
                        'nit' => '',
                        //'tlf' => (empty($datos_paciente[0]['tlf_movil']) || $datos_paciente[0]['tlf_movil'] == null ? 'Sin Informacion' : $datos_paciente[0]['tlf_movil']),
                        'monto_exento' => number_format($data['cotizacion_art']['monto_exento'], 2, ',', '.'),
                        'monto_gravable' => number_format($data['cotizacion_art']['monto_gravable'], 2, ',', '.'),
                        'porcentaje_descuento' => $data['cotizacion_art']['descuento'],
                        'calculo_descuento' => number_format(($data['cotizacion_art']['monto_gravable'] * $data['cotizacion_art']['descuento']) / 100, 2, ',', '.'),
                        'recargo' => number_format(($data['cotizacion_art']['monto_gravable'] * $data['cotizacion_art']['recargo']) / 100, 2, ',', '.'),
                        'p_recargo' => $data['cotizacion_art']['recargo'],
                        'porcentaje_iva' => $this->config->item('valor_iva'),
                        'calculo_iva' => number_format(($data['cotizacion_art']['monto_gravable'] * $this->config->item('valor_iva')) / 100, 2, ',', '.'),
                        'neto' => number_format($data['cotizacion_art']['monto_total'], 2, ',', '.'),
                        'materiales' => $materiales
                    );

                    if (isset($modo[1]) && $modo[1] == 'preview'):
                        $file = $this->EXC_create_pdf_cotizacion($params_pdf, true, $this->input->post('membrete'), TRUE);
                        $this->offOutput();
                        echo json_encode(array('view' => 1, 'file' => $file));
                    else:
                        $success = $this->Cotizacion_free_model->insert($data);
                    endif;
                endif;
                $this->setContent($success);
            } else {

                $this->setContent($process['validation_error']);
            }
        }
    }

    /**
     * Validaciones extras realizadas a la entidad, que no las soporta el View_Engine
     *
     * @access public
     * @param array $data Datos para aplicar el formateo.
     * @return Mixed , string  Validaciones o FALSE en caso de que no tenga errores
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 07/01/14 03:00 PM
     */
    private function _avancedFormValidation($format_type, $validate_data_free_tab = false) {
        $this->load->library('form_validation');
        $format_type = strtoupper($format_type);
        //Insercion
        if ($format_type == 'INSERT') {



            $post = $this->input->post();



            //Se verifica que exista el id_tercero
            if (!isset($post['id_cliente']) or (isset($post['id_cliente']) and $post['id_cliente'] == '')) {
                $this->form_validation->set_rules('id_cliente', 'Cliente (Persona/Empresa)', 'required|');
                $this->form_validation->set_message('required', 'El campo Cliente (Persona/Empresa) es requerido');
                return FALSE;
            }
            if ($post['cat_tipo_pago'] == '')
                $this->form_validation->set_rules('cat_tipo_pago', 'Tipo de Pago', 'required|integer|rewrite');
            if ($post['enviar'] == '')
                $this->form_validation->set_rules('enviar', 'Direccion de Envio', 'required|rewrite');
            if (!$validate_data_free_tab):
                //valida la existencia de material
                if (!isset($post['art']) or (isset($post['art']) and $post['art'] == '')):
                    $this->form_validation->set_rules('art', 'Articulos', 'required');
                    $this->form_validation->set_message('required', 'Debe agregar al menos un articulo a la cotizacion');
                    return FALSE;
                endif;

            endif;
        }
    }

    /**
     * Formateo avanzado de los datos a insertar en cotizacion libre
     *
     * @access public
     * @param array $format_type Tipo de Formateo [INSERT o UPDATE].
     * @param array $data Datos para aplicar el formateo.
     * @return array Arreglo con los datos formateados segun la regla de negocio.
     * @author Nohemi Rojas
     * @version V 1.0 03/12/12 01:20 PM
     */
    function _avancedFormat($format_type, $data) {

        $format_type = strtoupper($format_type);
        $post = $this->input->post();
        $role = $this->session->userdata('role_name');

        if ($format_type == 'INSERT') {
        	$data['created_by'] = $this->empleado_model->getIdByUser($this->session->userdata('user_id'));
        	return prepareDataCotizacionFree($post, $data);           
        }
    }

    /**
     * Descripcion Formato extendido para la insercion de datos
     *
     * @access private
     * @param array $data Arreglo de datos de entrada para el formateo
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 08/01/14 03:23 PM
     */
    function _extendsFormat($data) {
        $data_ext = array();
        $data_ext['created_by'] = $this->empleado_model->getIdByUser($this->session->userdata('user_id'));
        $data_ext['modified_by'] = $this->empleado_model->getIdByUser($this->session->userdata('user_id'));
        return $data_ext;
    }

    /**
     * Genera PDF de la Cotizacion Libre
     *
     * @access public
     * @param array $params Parametros necesarios para Generar el PDF
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 16/01/14 11:17 AM
     */
    public function EXC_create_pdf_cotizacion($params = array(), $background_no_validate = FALSE, $membrete = FALSE, $mode_preview = FALSE) {
        $preview = ($mode_preview ? 'preview/' : 'articulos/');

        $dir_modo_impresion = ($membrete == 'true' ? 'membrete_on/' : 'membrete_off/');
        $this->lang->load('app_doc');
        $this->lang->load('app_email');
        $this->load->library('encrypt');
        $this->load->helper('view_engine_helper');
        extract($params);
        //Cadena que contiene el string para generar el pdf
        $html = $this->load->view('cotizacion/cotizacion_free/pdf_cotizacion_free', $params, TRUE);
        // -----------------------------
        // -----------------------------
        //Asignacion de nombre, ruta y generacion del documento
        $name_file = "cot_$nro_cotizacion";
        $destination_path = 'uploads/cotizaciones_free/' . $dir_modo_impresion;
        $destination_path = ($background_no_validate ? $destination_path . $preview : $destination_path);

        generate_pdf($name_file, $destination_path, 'F', $html, $background_no_validate, $membrete);
        return utf8_decode("{$destination_path}" . "{$name_file}.pdf");
        // -----------------------------
    }

    /**
     * Forza la descarga de un archivo pdf
     *
     * @access public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 16/01/14 12:00 PM
     */
    function EXC_force_download() {
        $array_get = $this->input->get();

        $this->offOutput();
        $array_url = explode('_', $array_get['url']);
        $array_url_invertido = array_reverse($array_url);


        $len = filesize($array_get['url']);
        header('Content-type: application/pdf');
        header('Content-Disposition: attachment; filename="' . $array_get['name'] . '_' . $array_url_invertido[0] . '"');
        header('Content-Length: ' . $len);
        readfile($array_get['url']);
    }

    /**
     * Se encarga de mostrar los detalles de una cotizacion
     *
     * @author Nohemi Rojas
     * @version 1.0 03/12/12 09:05 AM
     */
    function detail() {
        $id_ec = $this->input->post('id');
        $target = $this->input->post('target_on');
        $id = $this->encrypt->decode($id_ec);
        $this->session->set_userdata('id_cot', $id_ec);
        if (!empty($id)) {

            $data = $this->Cotizacion_free_model->getById($id);
            $data['id_cliente_id_ec'] = $this->encrypt->encode($data['id_persona_empresa']);
            $view['id_cliente_id_ec']=$data['id_cliente_id_ec'];
            $data['current_status'] = $this->Cotizacion_free_model->getStatus($id);
            $data['all_status'] = $this->Cotizacion_free_model->getAllStatus($id);
            $data['id_ec'] = $id_ec;

            //Busca los datos de persona en caso de pago Tercero
            if (isset($data['id_persona_empresa'])):
                $persona = $this->persona_model->getById($data['id_persona'], 'id');
                $empresa = $this->empresa_model->getById($data['id_empresa'], 'id');
                if ($persona):
                    $view['tipo_cliente'] = 'persona';

                elseif ($empresa):
                    $view['tipo_cliente'] = 'empresa';
                endif;
            endif;


            //Encoding datos

            $data['articulos'] = $this->Cotizacion_free_model->getArticulosByCotFree($data['id']);
            $data ['operation'] = 'detail';
            $data['target_on'] = $target;
            $tabs = array(
                array('view' => 'particular', 'label' => 'Notificaciones', 'params' => array('file' => 'cotizacion/cotizacion_free/notification', 'data' => array('data' => $data, 'target_on' => $target))),
                array('view' => 'form', 'url' => trim($this->uri->uri_string(), '/'), 'params' => array('data' => $data)),
                array('view' => 'particular', 'label' => 'Articulos', 'params' => array('file' => 'cotizacion/cotizacion_free/_articulos', 'data' => array('operation' => 'detail', 'articulos' => $data['articulos']))),
                array('view' => 'form', 'url' => 'cotizacion/cotizacion_free/detailPayment', 'params' => array('data' => $data))
            );

            $view['view'] = $this->view_engine->tab($tabs);
            $this->setContent($this->load->view('cotizacion/cotizacion_free/detail', $view, TRUE));
        }
    }

    /**
     * Genera o ubica documentos de la cotizacion de acuerdo al tipo de documento.
     *
     * @access public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 17/01/14 01:56 PM
     */
    public function EXC_quoteReport() {
        $this->offOutput();
        $id = $this->input->post('id');
        $type_report = $this->input->post('tipo');
        $membrete = $this->input->post('membrete');
        $dir_modo_impresion = ($membrete == 'true' ? 'membrete_on' : 'membrete_off');
        $alvuelo = TRUE;
        if ($id):
            $id = $this->encrypt->decode($id);

            $data = $this->Cotizacion_free_model->get_data_cotizacion_free($id);
            $data = $data[0];

            switch ($type_report) {
                case 'cotizacion':
                    $filename = "uploads/cotizaciones_free/{$dir_modo_impresion}/cot_$data[codigo].pdf";
                    if (file_exists($filename) && !$alvuelo):
                        $response = array(
                            'sucess' => true,
                            'file' => $filename
                        );
                    else:
                        $getDatosPdf = $this->Cotizacion_free_model->getDatosPdf(0, $data['id_vendedor'], $data['id_cliente']);
                        $getMaterialesPdf = $this->Cotizacion_free_model->getArticulosByCotFree($data['id']);
                        foreach ($getMaterialesPdf as $clave => $valor):
                            $materiales[] = array(
                                'codigo_m' => $valor['id'],
                                'modelo_m' => $valor['modelo'],
                                'descripcion_m' => $valor['descripcion'],
                                'gart_m' => '',
                                'cant_m' => $valor['cantidad'],
                                'precio_m' => number_format($valor['precio'], 2, ',', '.'),
                                'neto_m' => number_format($valor['precio'] * $valor['cantidad'], 2, ',', '.')
                            );
                        endforeach;


                        $params_pdf = array(
                            'nro_cotizacion' => $data['codigo'],
                            'fecha_emision' => date('d/m/Y'),
                            'forma_pago' => $getDatosPdf['tipo_pago'],
                            'tiempo_entrega' => 'INMEDIATA',
                            'vendedor' => $getDatosPdf['vendedor'],
                            'cliente' => $getDatosPdf['cliente']['nombre_cliente'],
                            'rif' => $getDatosPdf['cliente']['cedula_rif'],
                            'nit' => '',
                            //'tlf' => (empty($datos_paciente[0]['tlf_movil']) || $datos_paciente[0]['tlf_movil'] == null ? 'Sin Informacion' : $datos_paciente[0]['tlf_movil']),
                            'monto_exento' => number_format($data['monto_exento'], 2, ',', '.'),
                            'monto_gravable' => number_format($data['monto_gravable'], 2, ',', '.'),
                            'porcentaje_descuento' => $data['descuento'],
                            'calculo_descuento' => number_format(($data['monto_gravable'] * $data['descuento']) / 100, 2, ',', '.'),
                            'recargo' => number_format(($data['monto_gravable'] * $data['recargo']) / 100, 2, ',', '.'),
                            'p_recargo' => $data['recargo'],
                            'porcentaje_iva' => $this->config->item('valor_iva'),
                            'calculo_iva' => number_format(($data['monto_gravable'] * $this->config->item('valor_iva')) / 100, 2, ',', '.'),
                            'neto' => number_format($data['monto_total'], 2, ',', '.'),
                            'materiales' => $materiales
                        );
                        $response = array(
                            'sucess' => true,
                            'file' => $this->_create_document($params_pdf, false, 'cotizacion', $membrete)
                        );
                    endif;
                    break;
                default:
                    $params_pdf['cuerpo_tabla'] = array();
                    break;
            }
        else:
            $response = array(
                'sucess' => false
            );
        endif;
        echo json_encode($response);
    }

    /**
     * Crea los documentos pertinentes a la cotizacion
     *
     * @access  private
     * @param   array   $params Arreglos de datos a mostrar en el documento
     * @param boolean $background_no_validate Indica si el documento lleva o no la marca NO VALIDO de fondo
     * @param string $document Indica el tipo de documento a crear
     * @param boolean $membrete Determina si el documento lleva o no el fondo predeterminado
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 17/01/14 02:17 PM
     */
    function _create_document($params = array(), $background_no_validate = FALSE, $document = '', $membrete = FALSE) {
        $this->lang->load('app_doc');
        $this->lang->load('app_email');
        $this->load->library('encrypt');
        $this->load->helper('view_engine_helper');
        extract($params);
        $dir_modo_impresion = ($membrete == 'true' ? 'membrete_on' : 'membrete_off');
        switch ($document) {
            case 'cotizacion':
                $view = 'cotizacion/cotizacion_free/pdf_cotizacion_free';
                $name = 'cot';
                $dir = 'cotizaciones_free/' . $dir_modo_impresion;
                break;
            default:
                $view = 'pdf_cotizacion';
                $name = 'cot';
                break;
        }
        //Cadena que contiene el string para generar el pdf
        $html = $this->load->view($view, $params, TRUE);
        // -----------------------------
        // -----------------------------
        //Asignacion de nombre, ruta y generacion del documento
        $name_file = "{$name}_{$nro_cotizacion}";
        $destination_path = "uploads/{$dir}/";
        $destination_path = ($background_no_validate ? $destination_path : $destination_path);

        generate_pdf($name_file, $destination_path, 'F', $html, $background_no_validate, $membrete);
        return utf8_decode("{$destination_path}" . "{$name_file}.pdf");
        // -----------------------------
    }

}
