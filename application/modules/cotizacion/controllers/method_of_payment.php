<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Esta clase provee la implementacion para la capa de control de
 * acciones de la entidad cotizacion_pago y sus entidades dependientes o extendidas
 *
 * @package Module
 * @category Controller
 * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V-1.0 26/09/13 05:06 PM
 */
class Method_of_payment extends AST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('method_of_payment_model', 'method_of_payment_model');
        $this->load->model('persona/paciente_model', 'paciente_model');
        $this->load->model('engine/engine_model', 'engine_model');
        $this->load->model('cotizacion/cotizacion_model', 'Cotizacion_model');
        $this->load->model('persona/persona_model', 'persona_model');
        $this->load->model('empresa/empresa_model', 'empresa_model');
        $this->load->model('persona/empleado_model', 'empleado_model');
        $this->load->model('empresa/seguro_model', 'seguro_model');
        $this->load->model('inventario/precios_model', 'precios_model');
        $this->load->model('persona/medico_model', 'medico_model');
        $this->load->model('empresa/clinica_model', 'clinica_model');
        $this->load->model('settings/categories_model', 'categories_model');
        $this->load->model('auth/users_model', 'users_model');
    }

    function create($params = NULL) {
        if (!$this->input->post()) {
            $view ['view'] = $this->view_engine->form();
            //se carga  una nueva vista para aplicar eventos personalizados
            $view = $this->load->view('cotizacion/_createMethod', $view, TRUE);
            $this->setContent($view);
        } elseif ($params[0] == 'process') {

            $insert = FALSE;
            $process = $this->view_engine->process();
            if ($process['result']) {

                $this->_avancedFormValidation('insert', $process['data']);
                $params = array('format_type' => 'insert', 'data' => $process['data']);
                $data = $this->_format($params);

                $insert = $this->method_of_payment_model->insert($data);
                $this->setContent($insert);
            } else {
                $this->setContent($process['validation_error']);
            }

            // -----------------------------
            //seteamos los datos de auditoria
            $this->audit->setOperationData($process);
            $this->audit->setChkResult($insert);
            // -----------------------------
        }
    }

    /**
     * Validaciones extras realizadas a la entidad, que no las soporta el View_Engine
     *
     * @access public
     * @param array $data Datos para aplicar el formateo.
     * @return Mixed , string  Validaciones o FALSE en caso de que no tenga errores
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 26/09/13 05:06 PM
     */
    private function _avancedFormValidation($format_type, $validate_data_tab = false) {

        $this->load->library('form_validation');
        $format_type = strtoupper($format_type);

        //Insercion
        if ($format_type == 'INSERT') {

            $post = $this->input->post();

            //Valida los elementos que se utilizan en esta parte de la validacion
            if (($post['cat_pagador'] == '') OR ($post['cat_tipo_pago'] == ''))
                return FALSE;

            if (!$validate_data_tab):
                //valida la existencia de material
                if (!isset($post['mat']))
                    $this->form_validation->set_rules('mat', 'Material', 'required');
                $this->form_validation->set_message('required', 'Debe seleccionar al menos un Material');
            endif;
            $pagador = $this->engine_model->getValueByCategory($post['cat_pagador']);

            //valida si el valor seleccionado del emisor de pagao es tercero
            if ($pagador == 'Tercero') {
                //Se verifica que se selecciono el Persona/empresa (N/J)
                if ($post['cat_letra'] == '')
                    $this->form_validation->set_rules('cat_letra', 'Tercero (Persona/Empresa)', 'required|integer|rewrite');

                //Se verifica que exista el id_tercero
                if (!isset($post['id_tercero']) or (isset($post['id_tercero']) and $post['id_tercero'] == '')) {
                    $this->form_validation->set_rules('id_tercero', 'Persona/Empresa', 'required|integer|rewrite');
                }
            }

            //valida si el valor seleccionado del emisor de pago es aseguradora
            if ($pagador == 'Aseguradora') {
                //Se verifica que exista el id_seguro
                if (!isset($post['id_seguro']) or (isset($post['id_seguro']) and $post['id_seguro'] == ''))
                    $this->form_validation->set_rules('id_seguro', 'Aseguradora', 'required|integer|rewrite');

                else {
                    $this->load->model('empresa/seguro_model', 'seguro_model');
                    $seguro = $this->seguro_model->getById($post['id_seguro'], 'id');

                    $tipo_seguro = $this->engine_model->getValueByCategory($seguro['cat_tipo_seguro']);

                    switch ($tipo_seguro) {

                        case 'Malo':
                            $this->form_validation->set_rules('id_seguro', 'Aseguradora', 'required|integer|rewrite');
                            $this->form_validation->set_message('rewrite', 'No se puede seleccionar la aseguradora, ya que no es confiable');
                            break;

                        case 'Regular':
                            //validacion especial que no permite carta aval
                            $cat_tipo_pago = $this->engine_model->getValueByCategory($post['cat_tipo_pago']);

                            if ($cat_tipo_pago == 'Carta Aval') {

                                $this->form_validation->set_rules('id_seguro', 'Aseguradora', 'required|integer|rewrite');
                                $this->form_validation->set_message('rewrite', 'La Aseguradora seleccionada no permite Carta Aval como medio de pago');
                            }else
                                $this->form_validation->set_rules('id_seguro', 'Aseguradora', 'required|integer');
                            break;

                        default:
                            $this->form_validation->set_rules('id_seguro', 'Aseguradora', 'required|integer');
                            break;
                    }
                }
            }
        }
    }

    /**
     * Formateo avanzado de los datos a insertar
     *
     * @access public
     * @param array $format_type Tipo de Formateo [INSERT o UPDATE].
     * @param array $data Datos para aplicar el formateo.
     * @return array Arreglo con los datos formateados segun la regla de negocio.
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 26/09/13 05:06 PM
     */
    function _avancedFormat($format_type, $data) {

        $format_type = strtoupper($format_type);
        $post = $this->input->post();
        if($format_type == 'DELETE'){
            return parent::_avancedFormat($format_type, $data);
        }elseif ($format_type == 'INSERT') {


            $cotizacion_pago = $this->_extendsFormat($data);

            $cotizacion['id_seguro'] = $data['id_seguro'];






//------------- INICIO FORMAT cotizacion_pago
            $cotizacion_pago['cat_tipo_pago'] = $data['cat_tipo_pago'];
            $cotizacion_pago['cat_pagador'] = $data['cat_pagador'];
            //Verifica que exista tercero
            if (isset($data['id_tercero']) and isset($data['cat_letra'])) {
                $tercero = $this->engine_model->getValueByCategory($data['cat_letra']);
                if ($tercero == 'Natural') {
                    $cotizacion_pago['id_persona'] = $data['id_tercero'];
                    $data['id_persona'] = $data['id_tercero']; //se crea a fin de hacer Validacion
                }
                if ($tercero == 'Jurídico') {
                    $cotizacion_pago['id_empresa'] = $data['id_tercero'];
                    $data['id_empresa'] = $data['id_tercero']; //se crea a fin de hacer Validacion
                }
            }
//------------- FIN FORMAT cotizacion_pago
//--------------Retorno de datos
            return array('cotizacion_pago' => $cotizacion_pago);
        }
    }

    /**
     * Formateo extendido de datos, se usa para aplicar el mismo formato a la operacion que guarda
     * en varias entidades
     *
     * @access public
     * @param array $data Datos para aplicar el formateo.
     * @return array Arreglo con los datos formateados segun la regla de negocio.
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 26/09/13 05:06 PM
     */
    private function _extendsFormat($data) {
        $data_ext = array();
        $data_ext['created_by'] = $data['created_by'];
        return $data_ext;
    }

    /**
     * Implementacion basica para la edicion de registros, por un lado llama a
     * a la vista de edicion de registros y por otro lado guarda el registro en
     * base de datos.
     *
     * @access public
     * @param array $params Parametros adicionales enviados al controlador.
     * @author Jose  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 27/09/13 09:43 AM
     */
    function delete00() {

        if ($this->input->is_ajax_request() && $this->input->post()) {
            echo '<pre>';
            var_dump($this->encrypt->decode($this->input->post('id')));
            echo '</pre>';
            exit;
            $content = 'La pagina solicitada';
            $title = 'Error!!!';

            // Verificamos si el borrado es masivo o de uno solo
            $id_request = $this->input->post('id');
            if (is_array($id_request))
                foreach ($id_request as &$id_ec) {
                    $id_ec = $this->encrypt->decode($id_ec);
                }
            else
                $id_request = $this->encrypt->decode($id_request);


            //Arreglo de parametros a ser pasados al format.
            $params = array(
                'format_type' => 'delete',
                'data' => array()
            );
            $data = $this->_format($params);
            $success = $this->Categories_model->delete($id_request);

            if ($success) {
                $content = '<div class="modal-body">Registro eliminado saisfactoriamente.</div>';
                $title = 'Exito';
            } else
                $content = '<div class="modal-body">No se pudo eliminar el registro. Por favor intente de nuevo.</div>';

            $this->setTitle($title);
            $this->setContent($content);

            // -----------------------------
            //seteamos los datos de auditoria
            $this->audit->setOperationData(array('id' => $id_request));
            $this->audit->setChkResult($success);
            // -----------------------------
        }else {
            show_404();
        }
    }

}