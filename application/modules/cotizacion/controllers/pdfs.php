<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pdfs extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    /**
     * Genera PDF de la Cotizacion
     *
     * @access public
     * @param array $params Parametros necesarios para Generar el PDF     *
     * @author  Frederick D. Bustamante G.
     * @version
     */
    public function create_pdf_cotizacion($params = array()) {
        /**
         * Descripcion detallada de variables recibidas.
         *
         * Este arreglo se pasa en el arreglo $params
         * $materiales[]=array(
         *              'codigo_m'      => string,
         *              'modelo_m'      => string,
         *              'descripcion_m' => string,
         *              'gart_m'        => string,
         *              'cant_m'        => string,
         *              'precio_m'      => string,
         *              'neto_m'        => string
         *          );
         *
         * Arreglo principal del metodo
         * $params = array(
         *              'nro_cotizacion'  => string,
         *              'fecha_emision'   => string,
         *              'forma_pago'      => string,
         *              'tiempo_entrega'  => string,
         *              'vendedor'        => string,
         *              'dr_tratante'     => string,
         *              'cliente'         => string,
         *              'rif'             => string,
         *              'nit'             => string,
         *              'seguro'          => string,
         *              'tlf'             => string,
         *              'monto_exento'    => string,
         *              'monto_gravable'  => string,
         *              'descuento'       => string,
         *              'recargo'         => string,
         *              'iva'             => string,
         *              'neto'            => string,
         *              'materiales'      => array()
         *          );
         *
         */
        $this->load->library('encrypt');
        $this->load->helper('view_engine_helper');
        extract($params);
        $encrypted_nro_cotizacion = $this->encrypt->encode($nro_cotizacion);
        $html = $this->load->view('pdf_cotizacion', $params, TRUE);
        $name_file = "cot_$nro_cotizacion";
        $destination_path = 'uploads/cotizaciones/';
        //generate_pdf($name_file, $destination_path, 'F', $html);
        //sendEmail_dev(array('frederickdanielb@gmail.com'), '1232133', 'desarrollo.ast@gmail.com', 'hooooola', array($destination_path . $name_file . '.pdf'));
    }

    function envio() {
        $this->load->helper('view_engine_helper');
        //sendEmail_dev(array('frederickdanielb@gmail.com'), '1232133', 'desarrollo.ast@gmail.com', 'hooooola');
    }

}
