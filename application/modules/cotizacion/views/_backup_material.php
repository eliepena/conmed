<?php
$permissions = $this->session->userdata('permissions');
$role = $this->session->userdata('role_name');
?>

<div style="margin-top: 10px; margin-left: 20px">
    <table id="materialBackupTable" class="js-table table table-striped table-bordered">
        <thead>
            <tr>

                <th width="10px"></th>
                <th>Modelo</th>
                <th>Descripción</th>
                <th width="10px">Precio</th>
                <th width="4px">Cant</th>
                <th width="16px">SubTotal</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $monto = 0;
            if (isset($backup) and !empty($backup)):
                ?>
                <?php foreach ($backup as $key => $material): ?>
                    <tr>

                        <td><a data-toggle="modal" href="#" onclick="show_img_article('<?php echo $material['modelo']; ?>',  '<?php echo $material['descripcion']; ?>', '<?php echo $material['imagen']; ?>');"><i class="icon-picture"></i></a></td>
                        <td><?php echo $material['modelo']; ?></td>
                        <td><?php echo $material['descripcion']; ?></td>
                        <td><?php echo $material['precio']; ?></td>
                        <td><?php echo $material['cantidad']; ?></td>
                        <td><?php echo number_format($material['precio'] * $material['cantidad'], 2, ',', '.') ?></td>
                    </tr>
                    <?php
                    $monto+= $material['precio'] * $material['cantidad'];
                endforeach;
                $calculo_iva = ($monto * $this->config->item('valor_iva')) / 100;
                $monto_total = $monto + $calculo_iva;
            endif;
            ?>
        </tbody>
        <tfoot>
            <?php if ($operation != 'detail'): ?>
                <tr>
                    <th colspan="3">Total</th>
                    <th colspan="3"><input type="text" id ="total_view-backup" name="total_view-backup" placeholder="0" readonly="readonly"/></th>
                </tr>
            <?php else: ?>
                <?php if (isset($permissions[2109]) || $role == 'Admin'): ?>
                    <tr>
                        <th colspan="3" rowspan="3" width="70%" style="border-bottom: 0px solid #000;border-left: 0px solid #000;" ></th>
                        <th colspan="2">Monto SubTotal</th>
                        <td colspan="1"><?php echo number_format($monto, 2, ',', '.') ?></td>
                    </tr>
                    <tr>

                        <th colspan="2"><?php echo ' IVA ' . $this->config->item('valor_iva') . ' % '; ?></th>
                        <td colspan="1"><?php echo number_format(isset($calculo_iva) ? $calculo_iva : 0.00, 2, ',', '.'); ?></td>
                    </tr>
                    <tr>

                        <th colspan="2">Monto Total</th>
                        <td colspan="1"><?php echo number_format(isset($monto_total) ? $monto_total : 0.00, 2, ',', '.'); ?></td>
                    </tr>
                    <?php
                endif;
            endif;
            ?>
        </tfoot>
    </table>
</div>


<?php if ($operation == 'detail'): ?>
    <?php if (isset($articulos_sustituidos) and sizeof($articulos_sustituidos) > 0): ?>
        <div style="margin-top: 10px; margin-left: 20px">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th style="background-color: #F9F9F9;" width="25%"><center>MATERIAL SUSTITUIDO</center></th>
                <th style="background-color: #F9F9F9;"><center>

                    <table  class="table table-striped table-bordered">
                        <tr>

                            <th colspan="5"><center>ARTICULO SUSTITUTO</center></th>

                        </tr>
                        <tr>

                            <th width="20px" ></th>
                            <th width="80px" >Modelo</th>
                            <th>Descripción</th>
                            <th width="80px" >Precio</th>
                            <th width="50px">Cant</th>
                        </tr>

                    </table>

                </center></th> </tr>



                <?php foreach ($articulos_sustituidos as $key => $value): ?>


                    <tr width="25%" ><td ><center><b><?php echo $value['modelo']; ?></b></center></td>
                    <td>
                        <table  class="table table-striped table-bordered">
                            <tbody>
                                <?php foreach ($value['sustituidos'] as $clave => $material): ?>
                                    <tr>

                                        <td width="20px" ><a data-toggle="modal" href="#" onclick="show_img_article('<?= $material['modelo'] ?>',  '<?= $material['descripcion'] ?>', '<?= $material['imagen'] ?>');"><i class="icon-picture"></i></a></td>
                                        <td width="80px" ><?php echo $material['modelo'] ?></td>
                                        <td><?php echo $material['descripcion'] ?></td>
                                        <td width="80px"><?php echo number_format($material['precio'], 2, ',', '.') ?></td>
                                        <td width="50px" ><?php echo $material['cantidad']; ?> </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </td>
                    </tr>

                <?php endforeach; ?>
                </tbody>
            </table>


        </div>
    <?php endif; ?>
<?php endif; ?>