<?php if ($fields > 0): ?>
    <?php echo $view ?>

<?php else: ?>
<br />   
<h3> No hay Campos por Validar</h3>
<br />
<?php endif; ?>

<script type="text/javascript">
    $(function(){
        var level = '<?php echo $level ?>';
        var id_operation = <?php echo $id_operation?>;
        //Verifica que exista elemento Paciente
        if($('#li-id_paciente_label-'+id_operation).length > 0) {
            //Si el paciente esta validada mostrara detail, sino mostrara edit
            var pac_val = '<?php echo isset($data['id_paciente_validado']) ? $data['id_paciente_validado'] : 0 ?>';
            var pac_id = '<?php echo isset($data['id_paciente_id_ec']) ? $this->encrypt->encode($data['id_paciente_id_ec']) : 0 ?>';
           
            if(pac_val==0){
                var button_paciente = [
                    '<a href="#" role="button" class="btn btn-primary" data-toggle="modal" onclick="request({url:\'<?php echo base_url("persona/paciente/edit") ?>\', data:{id: \''+pac_id+'\',target_on:\'window2\'}})">',
                    'Editar Paciente</a>'
                ].join('');
            }
            
            else{
                var button_paciente = [
                    '<a href="#" role="button" class="btn btn-primary" data-toggle="modal" onclick="request({url:\'<?php echo base_url("persona/paciente/detail") ?>\', data:{id: \''+pac_id+'\',target_on:\'window2\'}})">',
                    'Detalles de Paciente</a>'
                ].join('');
            }
            var target = $("#button-id_paciente_cat");
            target.html(button_paciente);
        }
        
        //Verifica que exista elemento Pago Tercero Persona
        if($('#li-id_persona_label-'+id_operation).length > 0) {
      
            //Si la persona esta validada mostrara detail, sino mostrara edit
            var per_val = '<?php echo isset($data['id_persona_validado']) ? $data['id_persona_validado'] : 0 ?>';
            var per_id = '<?php echo isset($data['id_persona_id_ec']) ? $this->encrypt->encode($data['id_persona_id_ec']) : 0 ?>';
            
            if(per_val==0){
                var button_persona = [
                    '<a href="#" role="button" class="btn btn-primary" data-toggle="modal" onclick="request({url:\'<?php echo base_url("persona/persona/edit") ?>\', data:{id: \''+per_id+'\',target_on:\'window2\'}})">',
                    'Editar Persona</a>'
                ].join('');
            }
            else{
                var button_persona = [
                    '<a href="#" role="button" class="btn btn-primary" data-toggle="modal" onclick="request({url:\'<?php echo base_url("persona/persona/detail") ?>\', data:{id: \''+per_id+'\',target_on:\'window2\'}})">',
                    'Detalles de Persona</a>'
                ].join('');
            }
            
            var target = $("#button-id_persona_cat");
            target.html(button_persona);
        }
        
        //Verifica que exista elemento Pago Tercero Empresa
        if($('#li-id_empresa_label-'+id_operation).length > 0) {
        
            //Si la empresa esta validada mostrara detail, sino mostrara edit
            var emp_val = '<?php echo isset($data['id_empresa_validado']) ? $data['id_empresa_validado'] : 0 ?>';
            var emp_id = '<?php echo isset($data['id_empresa_id_ec']) ? $this->encrypt->encode($data['id_empresa_id_ec']) : 0 ?>';
          
           
            if(emp_val==0){
                var button_empresa = [
                    '<a href="#" role="button" class="btn btn-primary" data-toggle="modal" onclick="request({url:\'<?php echo base_url("empresa/empresa/edit") ?>\', data:{id: \''+emp_id+'\',target_on:\'window2\'}})">',
                    'Editar Empresa</a>'
                ].join('');
            }
            
            else{
                var button_empresa = [
                    '<a href="#" role="button" class="btn btn-primary" data-toggle="modal" onclick="request({url:\'<?php echo base_url("empresa/empresa/detail") ?>\', data:{id: \''+emp_id+'\',target_on:\'window2\'}})">',
                    'Detalles de Empresa</a>'
                ].join('');
            }
            
            var target = $("#button-id_empresa_cat");
            target.html(button_empresa);
        }
        
        //Verifica que exista el monto_total
        if($('#li-monto_label-'+id_operation).length > 0) {
            var monto = '<?php echo isset($data['monto']) ? $data['monto'] : 0.0 ?>';
            var monto_total = '<?php echo isset($data['monto_total']) ? $data['monto_total'] : 0.0 ?>';
            $('#monto-'+id_operation).val(monto);
            $('#monto_total-'+id_operation).val(monto_total);
        }
        
        //Verifica que exista la fecha de Instrumento para compararlo con la fecha de cirugia
        if($('#li-fecha_instrumento_label-'+id_operation).length > 0) {
            var fecha_instrumento = '<?php echo isset($data['fecha_instrumento']) ? date('d-m-Y', strtotime($data['fecha_instrumento'])) : '' ?>';
            var fecha_cirugia = '<?php echo isset($data['fecha_cirugia']) ? date('d-m-Y', strtotime($data['fecha_cirugia'])) : '' ?>';
            
            $('#fecha_instrumento-'+id_operation).val(fecha_instrumento);
            $('#fecha_instrumento_cirugia-'+id_operation).val(fecha_cirugia);
        }
        
        if($('#li-deducible_label-'+id_operation).length > 0) {
            var deducible = '<?php echo isset($data['deducible']) ? $data['deducible'] : '' ?>';
            var monto = '<?php echo isset($data['monto']) ? $data['monto'] : '' ?>';

            $('#deducible-'+id_operation).val(deducible);
        }
    });
    
   
</script>


