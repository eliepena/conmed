<div id="2-container_valid" ></div>
<div id="4-container_valid" ></div>
<script type="text/javascript">
    $(function(){
        name_add_table=' cotizacion ';
        type_cotizacion='cotizacion';
        id_form_for_save_cotizacion='#_form';
        $("li:has(a[href=#2]),#2").addClass('hide_element');
        $("li:has(a[href=#3]),#3").addClass('hide_element');
        $("li:has(a[href=#4]),#4").addClass('hide_element');
        $("li:has(a[href=#5]),#5").addClass('hide_element');
        $("#li-cat_letra-2101").hide();
        $("#li-id_tercero-2101").hide();
        $("#li-id_seguro-2101").hide();
        $("#li-direcciones_medico-2101").hide();

    });

    //Cambio de Precios
    $("select[name='id_precios']").change(function(e){
            tipo_precio = $(this).val();
        cirugia = (Boolean($("select[name='id_tecnica']").val())) ? $("select[name='id_tecnica']").val() : $("select[name='id_lesion']").val();
        if (cirugia != "" && tipo_precio != ""){
            load_select({container:{mat:'#2-container',backup:'#3-container',instru:'#4-container'},url:"<?= base_url() ?>cotizacion/EXC_cuerpo_cotizacion/", data:{cirugia:cirugia,tipo_precio:tipo_precio}})
            }         
    });

    $("select[name='id_centro_salud']").change(function(e){
        tipo_direccion = (Boolean($("select[name='tipo_direccion']").children("option:selected").val())) ? $("select[name='tipo_direccion']").children("option:selected").text() : false;
        if(tipo_direccion){
            if(tipo_direccion=='Centro de Salud'){
                $('#enviar-2101').text($('#278').children("option:selected").text());
            }
        }

    });


    //Cambio de tipo de direccion
    $("select[name='tipo_direccion']").change(function(e){
        $('#enviar-2101').text('');
        id_medico = (Boolean($("select[name='id_medico']").val())) ? $("select[name='id_medico']").val() : false;
        id_centro_salud = (Boolean($("select[name='id_centro_salud']").val())) ? $("select[name='id_centro_salud']").val() : false;
        id_select_direccion = $(this).attr('id');
        tipo_direccion = trim( $("#"+$(this).attr('id')+" option:selected").text() );
        if(tipo_direccion=='Medico'){
            if(id_medico){
                $("#li-direcciones_medico-2101").show();
                $('#enviar-2101').text($('#li-direcciones_medico-2101').children("option:selected").text());
            }else{
                bootbox.alert("Debe seleccionar el Medico para cargar la Direccion", function() {});
                $("#"+$(this).attr('id')).val('');
                $("#"+$(this).attr('id')+'_chzn span').html('Seleccione la Direccion de Envio');
                $("select[name='id_medico']").focus();
            }
        }else if(tipo_direccion=='Centro de Salud'){
            if(id_centro_salud){
                $('#enviar-2101').text('');
                $("#li-direcciones_medico-2101").hide();
                $('#enviar-2101').text($('#278').children("option:selected").text());
            }else{
                bootbox.alert("Debe seleccionar el Centro de Salud para cargar la Direccion", function() {});
                $("#li-direcciones_medico-2101").hide();
                $("#"+$(this).attr('id')).val('');
                $("#"+$(this).attr('id')+'_chzn span').html('Seleccione la Direccion de Envio');
                $("select[name='id_centro_salud']").focus();

            }
        }     
    });

    //Cambio de Tecnica
    $("select[name='id_tecnica']").change(function(e){
        cirugia = $(this).val();
        tipo_precio = $("select[name='id_precios']").val();
        if (cirugia != "" && tipo_precio != ""){
            load_select({container:{mat:'#2-container',backup:'#3-container',instru:'#4-container'},url:"<?= base_url() ?>cotizacion/EXC_cuerpo_cotizacion/", data:{cirugia:cirugia,tipo_precio:tipo_precio}})
            var exento = $('#monto-exc').val();
            var mgravable= $('#monto_gravable').val();
            var desc = (parseFloat($('#porc-desc').val())*parseFloat(mgravable))/100;
            var rec = (parseFloat($('#porc-rec').val())*parseFloat(mgravable))/100;
            var iva = (parseFloat(mgravable)*parseFloat(<?= ($this->config->item('valor_iva')) ?>))/100;
            var netototal = parseFloat(mgravable) - parseFloat(exento) - desc +rec +iva;
            $('#netototal').val(parseFloat(netototal).toFixed(2));
            $('#desc').text(parseFloat(desc).toFixed(2));
            $('#rec').text(parseFloat(rec).toFixed(2));
            $('#iva').text(parseFloat(iva).toFixed(2));
        }
    });

    //Cambio de Emisor de Pago
    $("select[name='cat_pagador']").change(function(e){
        cat_pagador = $("select[name='cat_pagador'] option:selected").text();

        //Si el pagador es tercero
        if (cat_pagador == "Tercero"){
            $("#li-cat_letra-2101").show();
            //se resetea la aseguradora
            $("select[name='id_seguro']").val('').trigger('liszt:updated');
        }
        //Si el pagador es diferente de Tercero,
        else{
            // Se oculta tipo de persona, y tercero
            $("#li-cat_letra-2101").hide();
            $("#li-id_tercero-2101").hide();
        }

        //Si el pagador es aseguradora
        if(cat_pagador == "Aseguradora"){
            $("#li-id_seguro-2101").show();

            //Se ingresa la opcion Carta Aval
            if($("select[name='cat_tipo_pago'] option[value='32']").length <= 0)
                $("select[name='cat_tipo_pago']").prepend("<option value='32'>Carta Aval</option>")

            //se resetea tipo persona/tercero
            $("select[name='cat_tipo_pago']").val('').trigger('liszt:updated');
            $("select[name='id_tercero']").val('').trigger('liszt:updated');
            $("select[name='cat_letra']").val('').trigger('liszt:updated');
        }
        //Si el pagador es diferente de aseguradora
        else{
            //Se oculta la aseguradora
            $("#li-id_seguro-2101").hide();
            //Se quita la opcion de Carta Aval y se actualiza
            $("select[name='cat_tipo_pago'] option[value='32']").remove();
            $("select[name='cat_tipo_pago']").val('').trigger('liszt:updated');
        }

        //Resetea los combos Tercero Y Aseguradora para que no quede algo seleccionado
        if (cat_pagador != "Tercero" && cat_pagador != "Aseguradora"){
            $("select[name='id_seguro']").val('').trigger('liszt:updated');
            $("select[name='id_tercero']").val('').trigger('liszt:updated');
            $("select[name='cat_letra']").val('').trigger('liszt:updated');
        }

    });

    //Cambio de Tipo de Persona N/J. Muestra un boton de creacion de Persona o Empresa en caso de ser N o J
    $("select[name='cat_letra']").change(function(e){

        $("select[name='id_tercero'] option").remove();
        $("select[name='id_tercero']").val('').trigger('liszt:updated');

        cat_letra = $("select[name='cat_letra'] option:selected").text();

        //Muestra el seleccionador de la persona o empresa
        if(cat_letra!='')
            $("#li-id_tercero-2101").show();

        if (cat_letra == "Natural"){
               var button_tercero = [
                '<a href="#" role="button" class="btn btn-primary" data-toggle="modal" onclick="request({url:\'<?php echo base_url("persona/persona/create") ?>\'})">',
                'Crear Persona</a>'
            ].join('');
        }

        if (cat_letra == "Jurídico"){
               var button_tercero = [
                '<a href="#" role="button" class="btn btn-primary" data-toggle="modal" onclick="request({url:\'<?php echo base_url("empresa/empresa/create") ?>\'})">',
                'Crear Empresa</a>'
            ].join('');
        }

        var target = $("#button-id_tercero");
        target.html(button_tercero);

    });

    //Busqueda de los registros del elemento tercero segun la letra N-J seleccionada
    $("select[name='id_tercero']").ajaxChosen({
        dataType: 'json',
        type: 'GET',
        url:"<?= base_url() ?>cotizacion/cotizacion/EXC_searchSelect?letra="+$("select[name='cat_letra']").val()
    },{
        generateUrl: function(q){ return "<?= base_url() ?>cotizacion/cotizacion/EXC_searchSelect?letra="+$("select[name='cat_letra']").val(); },
        loadingImg: '<?= base_url() ?>assets/img/loading.gif'
    });

</script>
<div id="alert_content"></div>
<div id="link"></div>
<form id="_form" name="_form" action="#" method="POST" class="form-horizontal">
    <?php echo $view ?>
</form>