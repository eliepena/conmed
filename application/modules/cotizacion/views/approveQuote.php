<?php
//echo '<pre>';
//var_dump($id_ec);
//echo '</pre>';
//exit;
?>
<script type="text/javascript">
    $(function(){
        var form = 'form_2108';
        var url = '<?php echo base_url(); ?>cotizacion/cotizacion/approveQuote/process';
        var url_two = '<?php echo base_url(); ?>cotizacion/cotizacion/EXC_changeToStatusReplaceArticles';
        var modal = '#modal_';
        var reload_content = {
            type: "POST",
            url: '<?php echo base_url(); ?>cotizacion/cotizacion/detail',
            data: {id:'<?php echo $id_ec; ?>'},
            success:success
        };
        var _alert = '#alert_2108';
        formWindow(form, url, modal, _alert, reload_content);

        customFields();




        function change_status_quote(form, url_two, modal, _alert,reload_content){

            var msg = 'La cotizacion cambiara a un nuevo Estatus para sustituir articulos ¿Esta usted de acuerdo? ';
            bootbox.confirm(msg, function(rs){
                if(rs === true){

                    var $modal = $(modal);
                    $modal.modal('loading');
                    $.ajax({
                        url: url_two,
                        data: {id:'<?php echo $id_ec; ?>'},
                        success: function(response){
                            $modal.modal('loading');
                            var textAlert = '', alertHtml = '';
                            //Se procesa cuando el registro se guarda exitosamente
                            if(response.view == 1){
                                if(typeof window.afterSuccess === 'function'){
                                    afterSuccess(response);
                                }
                                textAlert = 'Datos guardados satisfactoriamente';
                                alertHtml = [
                                    '<div class="modal-body"><div class="alert alert-success fade in">',
                                    '<button type="button" class="close" data-dismiss="alert">×</button>',
                                    textAlert,
                                    '</div></div>'
                                ].join('');

                                if(reload_content)
                                    $.ajax(reload_content);
                                else
                                    reloadGrid();

                                $("#"+form).hide();
                                if($('#modal_window2').attr('aria-hidden')=='false'){
                                    setTimeout(function(){
                                        $('#modal_window2').modal('hide');
                                    },1500);
                                }
                                else {
                                    setTimeout(function(){
                                        $('#modal_window').modal('hide');
                                    },1500);
                                }
                            }

                            //Se procesa cuando no se puede guardar el registro
                            else{
                                //Ocurre cuando hay un error en Base de Datos
                                if(response.view == false)
                                    textAlert = 'No se pudo guardar el registro, si el problema persiste comuniquese con sistemas';

                                //Ocurre cuando hay errores de validacion en el Servidor
                                else
                                    textAlert = response.view;

                                //Mensaje de Alerta Error
                                alertHtml = [
                                    '<div class="modal-body"><div class="alert alert-error fade in">',
                                    '<button type="button" class="close" data-dismiss="alert">×</button>',
                                    textAlert,
                                    '</div></div>'
                                ].join('');

                            }
                            //Elemento que muestra el Alert
                            var target = $(_alert);
                            target.html(alertHtml);
                        }
                    });


                }
            });
        }
        $('#replace_art').click(function(){
            change_status_quote(form, url_two, modal, _alert,reload_content)
        });
    });


</script>
<div id="alert_2108"></div>
<form id="form_2108" name="form_2108" action="#" method="POST"  class="form-horizontal">

    <table style="" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th colspan="3" style="width: 100%;">
        <center>DATOS DE COTIZACION</center>
        </th>
        </tr>
        <tr>
            <th >Nro.</th>
            <th >Fecha Cirugia</th>
            <th >Hora Cirugia</th>


        </tr>
        </thead>
        <tbody>

            <tr>
                <td style="width: 33%;"><?php echo $data['codigo']; ?></td>
                <td style="width: 33%;"><?php echo $data['fecha_cirugia']; ?></td>
                <td style="width: 33%;"><?php echo $data['hora_cirugia']; ?></td>
            </tr>
            <tr>
                <td style="width: 10%;"><b>Paciente</b></td>
                <td style="width: 75%;" colspan="2"><?php echo $data['id_paciente']; ?></td>
            </tr>
            <tr>
                <td style="width: 25%;"><b>Medico</b></td>
                <td style="width: 75%;" colspan="2"><?php echo $data['id_medico']; ?></td>
            </tr>
            <tr>
                <td style="width: 25%;"><b>Vendedor</b></td>
                <td style="width: 75%;" colspan="2"><?php echo $data['id_vendedor']; ?></td>
            </tr>
            <tr>
                <td style="width: 25%;"><b>Direccion de Envio</b></td>
                <td style="width: 75%;" colspan="2"><?php echo $data['enviar']; ?></td>
            </tr>
            <?php if (isset($no_disponibilidad) && $no_disponibilidad): ?>
                <tr>
                    <td style="width: 25%;color: red;"><b>Mensaje del Sistema</b></td>
                    <td style="width: 75%;" colspan="2">
                        La cotizacion posee articulos sin disponibilidad en almacen, debe sustituirlos para aplicar la aprobacion.
                    </td>
                </tr>
            <?php endif; ?>
        </tbody>
    </table>

    <?php if (isset($no_disponibilidad) && !$no_disponibilidad): ?>
        <br/>
        <button style=" margin-left: 20px" type ="button" id ="btn-cargaMat"class="btn btn-primary " role="button" data-toggle="modal" onclick="show_instrumentalist(event,<?php echo $data['cod_articulacion']; ?>, 'tableAddInstrumentista');">
            Agregar Instrumentistas
        </button>

        <div style="margin-top: 10px; margin-left: 10px;margin-right:10px">
            <table id="tableAddInstrumentista" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th style="width: 20px;"></th>
                        <th>Cod</th>
                        <th>Nombre</th>
                        <th>Cedula</th>
                        <th>Correo</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
                <tfoot>

                </tfoot>
            </table>

        </div>
        <input  type="hidden" name="validate">
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal">Cancelar</button>
            <input type="submit" class="btn btn-primary update" value="Si, Aprobar Cirugia" />
        </div>
    <?php else: ?>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal">Cancelar</button>
            <input id="replace_art" type="button" class="btn btn-primary update" value="Enviar a Sustitucion" />
        </div>
    <? endif; ?>
</form>