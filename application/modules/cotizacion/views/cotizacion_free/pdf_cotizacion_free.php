<html>
    <head>
        <title></title>
        <style>
            body {
                font-size:9.5pt;
            }
            .head{
                background-color: #A5A5A5;
                font-weight:bold;
            }
            .right{
                text-align:right;
            }
            .left{
                text-align:left;
            }
            .center{
                text-align: center;
            }
            .borde_ext{

                border: 1px black solid;
            }
            .borde{

                border-top: 1px black solid;
                border-right: 1px black solid;
            }
            .borde_bottom{
                border-bottom:  1px black solid;
            }
            .borde_right{
                border-right:   1px black solid;
            }
            .borde_top{
                border-top:   1px black solid;
            }
            .borde_left{
                border-left: 1px black solid;
            }
        </style>
    </head>
    <body>
        <table border="0" style="left: -640px">
            <thead>
                <tr>
                    <th colspan ="3" style="width:56%" ></th>

                    <th colspan ="1" style="width:9%;" class="  "></th>
                    <th colspan ="3" style="width:33%;" class="center">
                        <b><h2>COTIZACIÓN LIBRE</h2></b>
                    </th>
                    <!--<th colspan ="1" style="width:9%;" class="  "></th>-->
                </tr>
                <tr>
                    <th style="width:30%" rowspan="4" colspan ="1" class="borde_top borde_bottom borde_left left ">
                        <br/>
                        <b><?php echo $this->lang->line('PdfCot_Client'); ?>:</b>
                        <br>
                        <font size="10"><?php echo $cliente; ?></font>
                    </th>
                    <th style="width:15%" rowspan="4" colspan ="1" class="borde_top borde_bottom borde_left left ">
                        <br/>
                        <b><?php echo $this->lang->line('PdfCot_Rif'); ?>:</b>
                        <font size="10"><?php echo $rif; ?></font>
                    </th>
                    <th style="width:14%" rowspan="4" colspan ="1" class="borde_top borde_bottom borde_left borde_right left">
                        <br/>
                        <b><?php echo $this->lang->line('PdfCot_Nit'); ?>:</b>
                        <br/>
                        <font size="10"><?php echo $nit; ?></font>
                    </th>
                    <th style="width:18%" colspan ="2" class="  right">
                        <?php echo $this->lang->line('PdfCot_NroCot'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php echo $nro_cotizacion; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:18%" colspan ="2" class="  right">
                        <?php echo $this->lang->line('PdfCot_FecEmi'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php echo $fecha_emision; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:18%" colspan ="2" class="  right">
                        <?php echo $this->lang->line('PdfCot_ForPag'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php echo $forma_pago; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:18%" colspan ="2" class="  right">
                        <?php echo $this->lang->line('PdfCot_TiempEsp'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php echo $tiempo_entrega; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:59%"  colspan ="3" class="borde_top borde_bottom borde_left borde_right left ">
                        <br/>
                        <b><?php //echo $this->lang->line('PdfCot_Seg'); ?></b>
                        <font size="10"><?php //echo $seguro; ?></font>
                    </th>
                    <th style="width:18%" colspan ="2" class="  right">
                        <?php echo $this->lang->line('PdfCot_Vend'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php echo $vendedor; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:59%"  colspan ="3" class="borde_top borde_bottom borde_left borde_right left ">
                        <br/>
                        <b><?php //echo $this->lang->line('PdfCot_Tlf'); ?></b>
                        <font size="10"><?php //echo $tlf; ?></font>
                    </th>
                    <th style="width:18%" colspan ="2" class="  right">
                        <b><?php //echo $this->lang->line('PdfCot_MedTratant'); ?></b>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php //echo $dr_tratante; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:59%" colspan ="3" class="borde_top ">
                        <b><?php echo $this->lang->line('PdfCot_MsjTabla'); ?>:</b>
                    </th>
                    <th style="width:18%" colspan ="2" class="  right">&nbsp;</th>
                    <th style="width:23%" colspan ="2" >&nbsp;</th>
                </tr>
                <tr class="center">
                    <th style="width:6%" class="borde_top borde_bottom borde_left head">
                        <?php echo $this->lang->line('PdfCot_Cod'); ?>
                    </th>
                    <th style="width:15%" class="borde_top borde_bottom borde_left head">
                        <?php echo $this->lang->line('PdfCot_Mod'); ?>
                    </th>
                    <th style="width:41%" class="borde_top borde_bottom borde_left head">
                        <?php echo $this->lang->line('PdfCot_Descrip'); ?>
                    </th>
                    <th style="width:7%" class="borde_top borde_bottom borde_left head">
                        <?php echo $this->lang->line('PdfCot_Gart'); ?>
                    </th>
                    <th style="width:7%" class="borde_top borde_bottom borde_left head">
                        <?php echo $this->lang->line('PdfCot_Cant'); ?>
                    </th>
                    <th style="width:14%" class="borde_top borde_bottom borde_left head">
                        <?php echo $this->lang->line('PdfCot_Prec'); ?>
                    </th>
                    <th style="width:14%" class="borde_top borde_bottom borde_left borde_right head">
                        <?php echo $this->lang->line('PdfCot_NetTabla'); ?>
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                $cant_materiales = sizeof($materiales);
                $count = 1;
                foreach ($materiales AS $clave => $valor):
                    $style_row = ($count % 2 == 0 ? '' : '');
                    echo '<tr >
                                <td style="width:6%;' . $style_row . '" class="borde_left border_right">' . $valor['codigo_m'] . '</td>
                                <td style="width:15%;' . $style_row . '" class="borde_left border_right">' . $valor['modelo_m'] . '</td>
                                <td style="width:41%;' . $style_row . '" class="borde_left">' . $valor['descripcion_m'] . '</td>
                                <td style="width:7%;' . $style_row . '"  class="borde_left right">' . $valor['gart_m'] . '</td>
                                <td style="width:7%;' . $style_row . '"  class="borde_left right">' . $valor['cant_m'] . '</td>
                                <td style="width:14%;' . $style_row . '" class="borde_left right">' . $valor['precio_m'] . '</td>
                                <td style="width:14%;' . $style_row . '" class="borde_left borde_right right">' . $valor['neto_m'] . '</td>
                              </tr>';
                    $count = $count + 1;
                endforeach;
                $espacio_fijo = 26 - $cant_materiales;
                $count_space = 1;
                if ($espacio_fijo > 0):
                    for ($j = 1; $j <= $espacio_fijo; $j++) :
                        echo '<tr>
                                    <td class="borde_left border_right">&nbsp;</td>
                                    <td class="borde_left">&nbsp;</td>
                                    <td class="borde_left">&nbsp;</td>
                                    <td class="borde_left">&nbsp;</td>
                                    <td class="borde_left">&nbsp;</td>
                                    <td class="borde_left">&nbsp;</td>
                                    <td class="borde_left borde_right">&nbsp;</td>
                                  </tr>';
                        $count_space = $count_space + 1;
                    endfor;
                endif;
                ?>

            </tbody>
            <tfoot>
                <tr>
                    <td style="width:41%;font-weight: bold;text-decoration: underline;font-size:7pt;"   colspan = "2" class="borde_top">

                        <br/>
                        <?php echo $this->lang->line('PdfCot_Valid_Line_1'); ?>
                        <?php echo $this->lang->line('PdfCot_Valid_Line_2'); ?>
                    </td>
                    <td style="width:29%;font-weight: bold;text-decoration: underline;font-size:7pt;"   colspan = "1" class="borde_top center">
                        <br/>
                        <?php echo $this->lang->line('PdfCot_EmitCheq_Line_1'); ?>
                        <?php echo $this->lang->line('PdfCot_EmitCheq_Line_2'); ?>
                    </td>
                    <td style="width:20%" colspan = "3"  class="borde_top borde_left borde_right right">
                        <?php echo $this->lang->line('PdfCot_MontExent'); ?>:
                    </td>
                    <td style="width:14%" colspan = "1" class="borde_top borde_right right"><?php echo $monto_exento; ?></td>
                </tr>
                <tr>
                    <td style="width:70%;font-size:6.5pt;"  rowspan="3" colspan = "3"  class="borde_bottom">
                        <br/><br/>
                        &nbsp;<?php echo $this->lang->line('PdfCot_Nota_Line_1'); ?>
                        &nbsp;<?php echo $this->lang->line('PdfCot_Nota_Line_2'); ?><br/>
                        &nbsp;<?php echo $this->lang->line('PdfCot_Nota_Line_3'); ?>
                    </td>

                    <td style="width:20%" colspan = "3" class="borde_left borde_right right">
                        <?php echo $this->lang->line('PdfCot_MontGrav'); ?>:
                    </td>
                    <td style="width:14%" colspan = "1" class="borde_right right">
                        <?php echo $monto_gravable; ?>
                    </td>
                </tr>
                <tr>
                    <td colspan = "3" class="borde_left borde_right right">
                        <?php echo $porcentaje_descuento . $this->lang->line('PdfCot_Desc'); ?>:
                    </td>
                    <td colspan = "1" class="borde_right right"><?php echo $calculo_descuento; ?></td>
                </tr>
                <tr>
                    <td colspan = "3" class="borde_left borde_right right">
                        <?php echo $p_recargo . ' ' . $this->lang->line('PdfCot_Recarg'); ?>:
                    </td>
                    <td colspan = "1" class="borde_right right"><?php echo $recargo; ?></td>
                </tr>
                <tr>
                    <td colspan="3" >&nbsp;</td>
                    <td colspan = "3" class="borde_top borde_left borde_right right">
                        <?php echo $this->lang->line('PdfCot_Iva') . ' ' . $porcentaje_iva . ' % '; ?>:
                    </td>
                    <td colspan = "1" class="borde_top borde_right right"><?php echo $calculo_iva; ?></td>
                </tr>
                <tr>
                    <td colspan="3" >&nbsp;</td>
                    <td colspan = "3" class="borde_bottom borde_top borde_left right">
                        <b><?php echo $this->lang->line('PdfCot_NetTotal'); ?>:</b>
                    </td>
                    <td colspan = "1" class="borde_bottom borde_top borde_right borde_left right"><?php echo $neto; ?></td>
                </tr>
            </tfoot>
        </table>
    </body>
</html>