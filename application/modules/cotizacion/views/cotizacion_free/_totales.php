<div style="margin-top: 10px; margin-left: 20px">
    <table  id="totalesTableFree" class="table table-striped table-bordered">
        <thead>
            <tr id="enc">
                <th >C&oacute;digo</th>
                <th>Modelo</th>
                <th>Descripción</th>
                <th width="10px">Gart.</th>
                <th width="10px">Cant.</th>
                <th>Precio Bs.F.</th>
                <th>Neto Bs.F</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot border="0">
            <tr>
                <th colspan="4"></th>
                <td colspan="2">Monto exento</td>
                <td colspan="1">
                    <input
                    <?php
                    if ($this->config->item('desc_rec') == FALSE) {
                        echo 'readonly="readonly"';
                    }
                    ?> type="text" name="monto_exento" size="2" id="monto-exc" onchange="neto_total()"class="input-mini" value="0.00"/></td>
            </tr>
            <tr>
                <th colspan="4"></th>
                <td colspan="2">Monto gravable</td>
                <td colspan="1" ><input type="text" id="monto_gravable" name="monto_gravable" value="0.00"class="input-mini" readonly="readonly" placeholder="0.00"></td>
            </tr>
            <tr>
                <th colspan="4"></th>
                <td colspan="1" width="10px"><input <?php
                    if ($this->config->item('desc_rec') == FALSE) {
                        echo 'readonly="readonly"';
                    }
                    ?> type="text" width="10px" size="2" id="porc-desc"name="descuento" onchange="neto_total()"class="input-mini" value="0.00" /></td>
                <td colspan="1">% de Descuento</td>
                <td colspan="1" id="desc">0.00</td>
            </tr>
            <tr>
                <th colspan="4"></th>
                <td colspan="1"><input <?php
                    if ($this->config->item('desc_rec') == FALSE) {
                        echo 'readonly="readonly"';
                    }
                    ?> type="text" size="2" id="porc-rec"name="recargo" onchange="neto_total()"class="input-mini" value="0.00" /></td>
                <td colspan="1">% de Recargo</td>
                <td colspan="1" id="rec">0.00</td>
            </tr>
            <tr>
                <th colspan="4"></th>
                <td colspan="2">IVA <?= ($this->config->item('valor_iva')) ?>  %</td>
                <td colspan="1" id="iva">0.00</td>
            </tr>
            <tr>
                <th colspan="4"></th>
                <th colspan="2">Total neto Bs.F:</th>
                <td colspan="1"><input type="text" id="netototal" name="monto_total" class="input-mini" readonly="readonly" placeholder="0.00"/></td>
            </tr>

        </tfoot>
    </table>
    <div class="form-actions">
        <input type="button" onclick="navigate_tab('prev',3);" value="Anterior" class="btn btn-primary">
        <input type="button" onclick="print_mode(null,'Vista Previa',true)" value="PreVisualizar" class="btn btn-primary">
        <input type="button" onclick="save()" value="Guardar" class="btn btn-primary">
    </div>
</div>
<style>
    .icon-remove:hover{
        background-color: rgba(255,0,0,0.25);
    }
</style>

<script>
    /**
     * Define el modo de impresion del documento.
     *
     * @param  {string} id_cotizacion   Identificador de la Cotizacion
     * @param  {string} tipo_documento  Datos para la busqueda de datos
     * @param  {boolean} mode_preview   Identifica si la impresion en preview o no.
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V- 1.0  05/09/13 02:52 PM16/01/14 11:57 AM
     */
    function print_mode_quote_free(id_cotizacion,tipo_documento,mode_preview){
        var membrete = false;
        bootbox.dialog('Impresion del documento '+tipo_documento,[{
                'label': "Sin Membrete!",
                'class': "btn-danger",
                'callback': function() {
                    membrete = false;
                    if(mode_preview){
                        preview_pdf_quote_free(membrete);
                    }else{
                        cotizacionDocumentoQuoteFree(id_cotizacion,tipo_documento,membrete);
                    }
                }
            },{

                'label': "Con Membrete",
                'class': "btn-primary",
                'callback': function() {
                    membrete = true;
                    if(mode_preview){
                        preview_pdf_quote_free(membrete);
                    }else{
                        cotizacionDocumentoQuoteFree(id_cotizacion,tipo_documento,membrete);
                    }
                }
            }]);
    }

    /**
 * Obtiene el pdf Preview de la cotizacion
 *
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  29/08/13 04:51 PM
 */
function preview_pdf_quote_free(membrete){
    var formData = $('#_form').serializeArray();
    formData.push({
        name:"membrete" ,
        value:membrete
    });
    $('body').modalmanager('loading');
    //Dispara el evento
    $.ajax({
        url: BASE_URL +'cotizacion/cotizacion_free/create/process/preview',
        data: formData,

        success: function(response){
            $('body').modalmanager('loading');

            //Se procesa cuando se ejecuta exitosamente el controlador
            if(response.view == 1){
                pop_pup = window.open(BASE_URL +"cotizacion/cotizacion_free/EXC_force_download?url="+response.file+"&name=Pdf_Preview",'PdfPreview', 'width=1,height=1,scrollbars=NO');
                if(!pop_pup){
                    bootbox.alert('Disculpe! Verifique que las ventanas emergentes de su navegador no esten bloqueadas.');
                }else{
                    setInterval(function() {
                        pop_pup.close();
                    }, 9000);
                }

            }
            //Se procesa cuando no se ejecuta exitosamente el controlador
            else{
                //Muestra nuevamente el TAB inicial
                $("li:has(a[href=#2]),#2").addClass("active");
                $("li:has(a[href=#3]),#3").removeClass("active");

                var textAlert = null;

                //Ocurre cuando hay un error en Base de Datos
                if(response.view == false)
                    textAlert = 'No se pudo generar la vista previa, si el problema persiste comuniquese con sistemas';

                //Ocurre cuando hay errores de validacion en el Servidor
                else
                    textAlert = response.view;

                //Mensaje de Alerta Error
                var alertHtml = [
                '<div class="alert alert-error fade in">',
                '<button type="button" class="close" data-dismiss="alert">×</button>',
                textAlert,
                '</div>'
                ].join('');

                //Elemento que muestra el Alert
                $("#alert_content").html(alertHtml);
            }
        }
    });
}

/**
 * Obtiene el reporte de la cotizacion (PDF)  de acuerdo al tipo de documento 
 *
 * @param  {string} id_cotizacion  Identificador de la Cotizacion
 * @param  {string} tipo_documento  Datos para la busqueda de datos
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  16/01/14 12:05 PM
 */
function cotizacionDocumentoQuoteFree(id_cotizacion,tipo_documento,membrete){

    $('body').modalmanager('loading');
    //Dispara el evento
    $.ajax({
        url: BASE_URL +'cotizacion/cotizacion_free/EXC_quoteReport/',
        data: {
            id:id_cotizacion,
            tipo:tipo_documento,
            membrete:membrete
        },
        success: function(response){
            $('body').modalmanager('loading');
            if(response.sucess){
                pop_pup = window.open(BASE_URL +"cotizacion/cotizacion_free/EXC_force_download?url="+response.file+"&name="+tipo_documento,'Pdf'+tipo_documento, 'width=1,height=1,scrollbars=NO');
                if(!pop_pup){
                    bootbox.alert('Disculpe! Verifique que las ventanas emergentes de su navegador no esten bloqueadas.');
                }else{
                    setInterval(function() {
                        pop_pup.close();
                    }, 9000);
                }
            }else{
                var textAlert = 'No se pudo generar el documento, si el problema persiste comuniquese con sistemas';
                bootbox.alert(textAlert);
            }
        }
    });
}
</script>