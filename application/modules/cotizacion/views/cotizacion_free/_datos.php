<?php
if ($operation != 'detail') {
    ?>
    <script type="text/javascript">
        $(function() {

            new FormValidator('form_2118', buildRules('form_2118'),
                    function(errors, event) {
                        var flag = validateForm(errors, event);

                        if (event && event.preventDefault) {
                            event.preventDefault();
                        } else if (event) {
                            event.returnValue = false;
                        }

                        //validacion submit
                        if (flag) {
                            //este archivo es particular,
                            //Valida TAB DATOS luego de ser validada en el cliente.
                            validate_cotizacion_free_tab();
                        }
                    });
            customFields();
            build_table_articles();
        });
    </script>
    <?php
}
?>
<div class="modal-body">
    <ul>
        <?php
        if ($operation != 'detail') {
            ?>
            <li id="li-label_datos_cotizacion-2118">
                <br>
                <label for="label_datos_cotizacion-2118">
                    <strong>DATOS DEL CLIENTE</strong></label>
                <div id="label-datos_cotizacion-2118"></div>
            </li>

            <li id="li-cat_letra-2118">
                <div class="control-group">
                    <label class="control-label" for="636">Tipo Persona</label>
                    <div class="controls">
                        <select name="cat_letra"
                                data-placeholder="Seleccione el Tipo de Persona"
                                class="chzn-select"
                                data-rules="required|integer|rewrite"
                                data-display="Tipo Persona"
                                data-content="Indique el Tipo de Persona"
                                title="Tipo Persona"
                                >
                            <option value=""></option>
                            <?php if (isset($tipo_persona) && sizeof($tipo_persona) > 0): ?>
                                <?php foreach ($tipo_persona as $key => $value): ?>
                                    <option value="<?php echo $value['id']; ?>" ><?php echo $value['_label']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                        <span id="button-cat_letra"></span>
                    </div>
                </div>
            </li>
            <li id="li-id_cliente-2118">
                <div class="control-group">
                    <label class="control-label" ><b>Cliente</b>(Persona/Empresa)</label>
                    <div class="controls">
                        <select name="id_cliente"
                                id="id_cliente-2118"
                                data-placeholder="Seleccione el Cliente"
                                class="chzn-select"
                                data-rules="integer|rewrite"
                                data-display="Cliente (Persona/Empresa)"
                                data-content="Indique el Cliente al cual se le generara la cotizacion"
                                title="Cliente (Persona/Empresa)"
                                >
                            <option value=""></option>
                        </select>
                        <span id="button-id_cliente">
                            <a href="#" role="button" class="btn btn-primary" data-toggle="modal"
                               onclick="request({url: BASE_URL + 'persona/persona/create', data: {id: '', target_on: 'window2'}})">
                                Crear Persona
                            </a>
                        </span>
                    </div>
                </div>
            </li>
            <li id="li-cliente-2118">
                <div class="control-group">
                    <label class="control-label"  for="cliente-2118">Cliente</label>
                    <div class="controls">
                        <input type="text"
                               class="input-xlarge"
                               name="cliente-2118"
                               id="cliente-2118"
                               placeholder=""
                               data-rules=""
                               data-display="Cliente"
                               value=""
                               data-content=""
                               title="Cliente"
                               readonly="readonly"
                               />



                    </div>
                </div>
            </li>
            <li id="li-ci_rif-2118">
                <div class="control-group">
                    <label class="control-label"  for="ci_rif-2118">Cedula/Rif</label>
                    <div class="controls">
                        <input type="text"
                               class="input-xlarge"
                               name="ci_rif-2118"
                               id="ci_rif-2118"
                               placeholder=""
                               data-rules=""
                               data-display="Cedula/Rif"
                               data-content=""
                               title="Cedula/Rif"
                               readonly="readonly"
                               />
                    </div>
                </div>
            </li>
            <li id="li-email-2118">


                <div class="control-group">
                    <label class="control-label"  for="email-2118">Email</label>
                    <div class="controls">
                        <input type="text"
                               class="input-xlarge"
                               name="email-2118"
                               id="email-2118"
                               placeholder=""
                               data-rules=""
                               data-display="email"
                               data-content=""
                               title="email"
                               readonly="readonly"
                               />
                    </div>
                </div>
            </li>
            <?php
        }
        ?>

        <li id="li-label_datos_orden-2118">
            <br>
            <label for="label_datos_orden-2118">
                <strong>DATOS DE LA COTIZACION</strong></label>
            <div id="label-datos_orden-2118"></div>
        </li>

        <?php
        if ($operation != 'detail') {
            ?>


            <li id="li-fecha_cotizacion-2118">
                <div class="control-group">
                    <label class="control-label" for="fecha_cotizacion-2118">Fecha de Cotizacion<span style='color:red;font-size: 16px'> *</span></label>
                    <div class="controls">
                        <div id="fecha_cotizacion-2118-container" class="input-append date" data-date data-date-format="dd-mm-yyyy" data-date-start-date='<?php echo date("d-m-Y"); ?>'>
                            <input class="date input-small" type="text" id="fecha_cotizacion-2118" name="fecha_cotizacion" data-rules="required|valid_date" data-display="Fecha de Cotizacion"  title="Fecha de Cotizacion" placeholder="Ej: dd-mm-aaaa" readonly="readonly" value="<?php echo date("d-m-Y"); ?>">

                            <span class="add-on"><i class="icon-calendar"></i></span>
                        </div>
                    </div>
                </div>
            </li>
            <li id="li-enviar-2118">
                <div class="control-group">
                    <label class="control-label" for="enviar-2118">Dirección de Envio<span style='color:red;font-size: 16px'> *</span></label>
                    <div class="controls">
                        <textarea
                            class="span3"
                            name="enviar"
                            id="enviar-2118"
                            placeholder="Ej: calle uno, casa nro 2, residencia los cocos, Caracas, Distrito Capital. "
                            data-rules=""
                            data-display="Dirección de Envio"
                            data-content="Indique la dirección de envio"
                            title="Dirección de Envio"
                            rows="5"
                            ></textarea>
                    </div>
                </div>
            </li>

            <li id="li-label_forma_pago-2118">
                <br/>
                <label for="label_forma_pago">
                    <strong>FORMA DE PAGO</strong></label>
                <div id="label-label_forma_pago"></div>
            </li>
            <li id="li-cat_tipo_pago-2118">
                <div class="control-group">
                    <label class="control-label" for="300">Forma de Pago<span style='color:red;font-size: 16px'> *</span></label>
                    <div class="controls">
                        <select name="cat_tipo_pago"
                                id="300"
                                data-placeholder="Seleccione la forma de pago"
                                class="chzn-select"
                                data-rules="required|integer"
                                data-display="Forma de Pago"
                                data-content="Indique la forma de pago de la cotización"
                                title="Forma de Pago"
                                >

                            <option value=""></option>
                            <?php if (isset($tipo_pago) && sizeof($tipo_pago) > 0): ?>
                                <?php foreach ($tipo_pago as $key => $value): ?>
                                    <option value="<?php echo $value['id']; ?>" ><?php echo $value['_label']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>

                        </select>
                        <span id="button-cat_tipo_pago"></span>
                    </div>
                </div>
            </li>

            <li id="li-label_observacion-2118">
                <br/>
                <label for="label_forma_pago">
                    <strong>OBSERVACIONES</strong></label>
                <div id="label-label_observacion"></div>
            </li>
            <li id="li-observacion-2118">
                <div class="control-group">
                    <label class="control-label" for="enviar-2118">Observaciones</label>
                    <div class="controls">
                        <textarea
                            class="span3"
                            name="observacion"
                            id="observacion-2118"
                            placeholder="Ej: Procesar de manera rapida"
                            data-rules=""
                            data-display="Observaciones"
                            data-content="Llene el campo si posee Observaciones"
                            title="Observaciones"
                            rows="5"
                            ></textarea>
                    </div>
                </div>
            </li>
            <?php
        }
        ?>

    </ul>
</div>

<?php
if ($operation != 'detail') {
    ?>
    <div class="modal-footer">

        <input type="submit" class="btn btn-primary update" value="Siguiente" />
    </div>

    <?php
}
?>

