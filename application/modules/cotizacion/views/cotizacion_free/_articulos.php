<?php
$permissions = $this->session->userdata('permissions');
$role = $this->session->userdata('role_name');
$monto_sin_iva = 0.00;
$iva = 0.00;
if ($operation != 'detail'):
    ?>
    <br/>
    <button type ="button" id ="btn-cargaMat"class="btn btn-primary " role="button" data-toggle="modal" onclick="show_articles(event,'art', 'articuloTableFree');">
        Agregar Articulos
    </button>
    <div style="float:right;margin-top:-0px">
        <span style="float:left;font-size:7pt;"><b>No Disponible</b>&nbsp;&nbsp;</span>
        <div class="img-circle" style="background-color:red;float:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>

        <span style="float:left;font-size:7pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Bajo</b>&nbsp;&nbsp;</span>
        <div class="img-circle" style="background-color:yellow;float:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>

        <span style="float:left;font-size:7pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Disponible</b>&nbsp;&nbsp;</span>
        <div class="img-circle" style="background-color:blue;float:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
    </div>
    <br/><br/>
<?php endif; ?>

<div style="margin-top: 10px; margin-left: 20px">
    <table id="articuloTableFree" class=" <?php echo ($operation != 'detail' ? '' : ' js-table '); ?>  table table-striped table-bordered">
        <thead>
            <tr>
                <th width="10px"></th>
                <th width="10px"></th>
                <th>Modelo</th>
                <th>Descripción</th>
                <?php
                if ($operation == 'detail'):
                    if (!isset($permissions[2109]) || $role == 'Admin'):
                        ?>
                        <th width="10px">Precio</th>
                        <?php
                    endif;
                else:
                    ?>
                    <th width="10px">Precio</th>
                <?php
                endif;
                ?>
                <th width="10px">Cantidad</th>
                <?php if ($operation != 'detail'): ?>
                    <th width="10px">Stock</th>
                <?php endif; ?>
            </tr>
        </thead>
        <tbody>
            <?php
            $monto = 0;
            if (isset($articulos) and !empty($articulos)):
                //Posicion en array que indica cual es tipo de precio seleccionado.
                $pos_tipo_precio = -1;
                foreach ($articulos as $key => $articulo):
                    ?>
                    <tr id="tr-art-<?php echo $articulo['id']; ?>">
                        <?php if ($operation != 'detail'): ?>
                            <td>
                                <input class="indicador-art" id="art-<?= $articulo['id'] ?>" type="hidden" value="<?php echo $articulo['id'] ?>" key="<?= $articulo['id'] ?>" name="art[<?= $articulo['id'] ?>]" />
                                <i class="icon-remove" onclick="delete_tr('tr-art-<?php echo $articulo['id'] ?>','art');delete_tr('tr-total-<?php echo $articulo['id']; ?>');" />
                            </td>
                        <?php else: ?>
                            <td></td>
                        <?php endif; ?>

                        <td><a data-toggle="modal" href="#" onclick="show_img_article('<?= $articulo['modelo'] ?>',  '<?= $articulo['descripcion'] ?>', '<?= $articulo['imagen'] ?>');"><i class="icon-picture"></i></a></td>
                        <td id="model-art-<?= $articulo['id'] ?>"><?php echo $articulo['modelo'] ?></td>
                        <td id="name-art-<?= $articulo['id'] ?>"><?php echo $articulo['descripcion'] ?></td>
                        <?php
                        if ($operation != 'detail'):
                            $readonly = '';
                        else:
                            $readonly = 'readonly="readonly"';
                            $value = "value='{$articulo['precio']}'";

                        endif;

                        if ($operation == 'detail'):
                            if (!isset($permissions[2109]) || $role == 'Admin'):
                                ?>
                                <td><input type="text" onblur ="calculateTotal();" id="price-art-<?= $articulo['id'] ?>" value="<?= number_format($articulo['precio'], 2, ',', '.') ?>" name="precio_artl[<?= $articulo['id'] ?>]"<?= $readonly ?> <?= $value ?> ></td>
                                <?php
                            endif;
                        else:
                            ?>
                            <td>
                                <select id="price-art-<?= $articulo['id'] ?>" name="precio_art[<?= $articulo['id'] ?>]" onchange="calculateTotal();">
                                    <?php
                                    if ($pos_tipo_precio == -1):
                                        $pos_tipo_precio = array_search($tipo_precio, $articulo['arr_id_precio']);
                                    endif;
                                    ?>


                                    <?php
                                    foreach ($articulo['arr_precio'] as $key_price => $precio):
                                        $selected = ($key_price == $pos_tipo_precio) ? 'selected="selected"' : '';
                                        ?>
                                        <option value='<?= $precio ?>' <?= $selected ?> ><?= number_format($precio, 2, ',', '.') ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                        <?php
                        endif;

                        if ($operation != 'detail'):
                            $readonly = '';
                            $value = "value='{$articulo['cantidad']}'";
                        else:
                            $readonly = 'readonly="readonly"';
                            $value = "value='{$articulo['cantidad']}'";
                            $monto_sin_iva += $articulo['precio'] * $articulo['cantidad'];

                        endif;
                        ?>
                        <td><input min="1" type="number" onblur="calculateTotal();check_disponibilidad(this.id,'art','articuloTableFree');" id="cant-art-<?= $articulo['id'] ?>" class="input-mini" name="cant_art[<?= $articulo['id'] ?>]" placeholder="0" key="<?= $articulo['id'] ?>" <?= $readonly ?> <?= $value ?> /></td>
                        <?php
                        $leyenda = '';
                        $disponibilidad = (empty($articulo['disponibilidad']) || $articulo['disponibilidad'] < 1 ? 0 : $articulo['disponibilidad']);
                        $arrayLeyenda[1] = 'red';
                        $arrayLeyenda[2] = 'yellow';
                        $arrayLeyenda[3] = 'blue';
                        if ($articulo['cantidad'] > $disponibilidad):
                            $leyenda = $arrayLeyenda[1];
                        elseif ($articulo['cantidad'] == $disponibilidad):
                            $leyenda = $arrayLeyenda[2];
                        else:
                            $leyenda = $arrayLeyenda[3];
                        endif;
                        if ($operation != 'detail'):
                            ?>

                            <td >
                                <div class="img-circle" id="leyenda-art-<?= $articulo['id'] ?>" style="background-color:<?php echo $leyenda; ?>;">
                                    <input type="hidden" id="disp-art-<?= $articulo['id'] ?>" value="<?php echo $disponibilidad; ?>"/>
                                    &nbsp;&nbsp;&nbsp;
                                </div>
                            </td>
                        <?php endif; ?>
                    </tr>
                    <?php
                endforeach;
                $iva = ($monto_sin_iva * $this->config->item('valor_iva')) / 100;
                $monto = $monto_sin_iva + $iva;
                ?>
            <?php endif; ?>
        </tbody>
        <tfoot>
            <?php if ($operation != 'detail'): ?>
                <tr>
                    <th colspan="4">Total</th>
                    <th colspan="3"><input type="text" id ="total_price-art" name="total_price" placeholder="0" readonly="readonly"/></th>
                </tr>
            <?php else: ?>
                <?php if (!isset($permissions[2109]) || $role == 'Admin'): ?>
                    <tr>
                        <th colspan="4">Sub Total</th>
                        <th colspan="2"><?php echo number_format($monto_sin_iva, 2, ',', '.') ?></th>
                    </tr>
                    <tr>
                        <th colspan="4"><b>Iva </b>(<?php echo $this->config->item('valor_iva'); ?> %)</th>
                        <th colspan="2"><?php echo number_format($iva, 2, ',', '.') ?></th>
                    </tr>
                    <tr>
                        <th colspan="4">Total</th>
                        <th colspan="2"><?php echo number_format($monto, 2, ',', '.') ?></th>
                    </tr>
                    <?php
                endif;
            endif;
            ?>
        </tfoot>
    </table>
    <?php
    if ($operation != 'detail'):
        ?>
        <div class="form-actions">
            <input type="button" onclick="navigate_tab('prev',2);" value="Anterior" class="btn btn-primary">
            <input type="button" onclick="navigate_tab('next',2);" value="Siguiente" class="btn btn-primary">
        </div>
    <?php endif ?>
    <div id="articuloModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="articuloModalLabel"></h3>
        </div>
        <div class="m-body" >
            <table align="center" >
                <tr>
                    <td><img id='selected-articulo-image' width="100%" height="100%"/></td>
                    <td >
                        <label><strong>Descripción</strong></label>
                        <span id="selected-articulo-description"></span>
                    </td>
                </tr>
            </table>
            <br/>
        </div>
    </div>
</div>
<script type="text/javascript">
    //Ejecutamos la funcion para calcular el total de la cotizacion.
<?php if ($operation != 'detail'): ?>
        calculateTotal();
        cant_articulo('articuloTableFree','cant_art','cant-art','art');
<?php endif; ?>
</script>

