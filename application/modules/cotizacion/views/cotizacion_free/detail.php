<?php
$permissions = $this->session->userdata('permissions');
$filename_on = "uploads/cotizaciones_free/articulos/membrete_on/cot_$data[codigo].pdf";
$filename_off = "uploads/cotizaciones_free/articulos/membrete_on/cot_$data[codigo].pdf";
if (file_exists($filename_on)):
    $load_file = $filename_on;
    $onclick = '';
elseif (file_exists($filename_off)):
    $load_file = $filename_off;
    $onclick = '';
else:
    $load_file = '#';
    $onclick = 'onclick = \'alert("El archivo de la cotizacion no esta creado.")\'';
endif;
?>
<span class="pull-right">
    <strong>N° de Cotización -</strong> <?php echo $data['codigo'] ?> <br/>
    <strong>Vendedor -</strong> <?php echo $data['id_vendedor'] ?><br/>
    <strong>Descargas:</strong><br/>
    <?php // if (!isset($permissions[2109])): ?>
    <span>
        <button type="button" onclick = "print_mode('<?php echo $data['id_ec'] ?>','cotizacion',false)" class="btn btn-primary btn-mini"  data-toggle="modal" role="button" > Cotización </button>
    </span>


    <br/>
    <br/>
    <?php // endif; ?>
    <a href="#" role="button" class="link" data-toggle="modal" onclick="request({url:'<?php echo base_url("cotizacion/cotizacion_free/listAll") ?>'})">
        Lista de Cotizaciones</a>

</span>
<?php echo $view ?>

<script type="text/javascript">
    $(function(){
        type_cotizacion='cotizacion_free';
        //Pago de Tercero Persona
        var persona = '<?php echo isset($data['id_persona']) ?>';
        if(!persona)
            $("#li-id_persona-2106").hide();

        //Pago de Tercero Empresa
        var empresa = '<?php echo isset($data['id_empresa']) ?>';
        if(!empresa)
            $("#li-id_empresa-2106").hide();


        //Pago de Aseguradora
        var seguro = '<?php echo isset($data['id_seguro']) ?>';
        if(!seguro)
            $("#li-id_seguro-2106").hide();

        //Ocultamos los campos por no visibles en todas los medios.
        $('#li-vencimiento-2106, #li-deducible-2106').hide();

        //Obtenemos el valor de tipo de pago.
        var tipo = $("#cat_tipo_pago-2106").val();
        if(typeof tipo != "undefined"){

            switch(tipo.toLowerCase()){
                case 'carta aval':
                    $('#li-cat_banco-2106').hide();
                    $('#li-vencimiento-2106, #li-deducible-2106').show();
                    break;
                case 'cheque':
                    $('#li-vencimiento-2106').show();
                    break;
            }
        }
        //**se oculta la ventana modal cuando se carga la lista de cotizaciones
        $('.link').on('click',function(){
            $("#modal_window").modal('hide');
        });

    });
<?php
if (isset($tipo_cliente)):
    if ($tipo_cliente == 'persona'):
        ?>
                    $('#button-id_cliente').html('<a href="#" role="button" class="btn btn-primary" data-toggle="modal" onclick="request({url:\'http://conmed/persona/persona/detail\', data:{id:\'<?php echo $id_cliente_id_ec; ?>\',target_on:\'window\'}})">Detalles de Cliente          </a>');
    <?php elseif ($tipo_cliente == 'empresa'): ?>
                $('#button-id_cliente').html('<a href="#" role="button" class="btn btn-primary" data-toggle="modal" onclick="request({url:\'http://conmed/empresa/empresa/detail\', data:{id:\'<?php echo $id_cliente_id_ec; ?>\',target_on:\'window\'}})">Detalles de Cliente          </a>');

        <?php
    endif;
endif;
?>
</script>