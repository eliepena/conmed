<?php
$permissions = $this->session->userdata('permissions');
$role = $this->session->userdata('role_name');
?>
<span class="span10">

    <strong>N° de Cotización:</strong> <?php echo (isset($data['codigo'])?$data['codigo']:'');?> <br />
    <strong>Fecha de Cotización:</strong> <?php echo date('d-m-Y H:i', strtotime($data['fecha_cotizacion'])) ?><br />
    <strong>Vendedor:</strong> <?php echo (isset($data['id_vendedor'])?$data['id_vendedor']:''); ?><br />

    <?php if (!isset($permissions[2109]) || $role == 'Admin'): ?>
        <strong>Monto de la cotización:</strong> <?php echo number_format($data['monto_total'], 2, ',', '.') ?><br />
        <strong>Forma de Pago:</strong> <?php echo (isset($data['tipo_pago'])?$data['tipo_pago']:''); ?><br />
    <?php endif; ?>

    <strong>Estatus: </strong>
    <span>
        <?php if ($data['all_status']): ?>
            <ul>
                <?php foreach ($data['all_status'] as $status) : ?>
                    <li>
                        <?php echo $status['status_name'] ?>: <?php echo date('d-m-Y H:i', strtotime($status['status_date'])) ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </span>

<input type="hidden" id="target" value="<?php echo $target_on; ?>" />
<script>
    function createTableControls(){
        var asInitVals = new Array();
        var oTable = $('.js-table').dataTable( {
            "oLanguage": {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "fnInfoCallback": null,
                "oAria": {
                    "sSortAscending":  ": Activar para ordernar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordernar la columna de manera descendente"
                }
            }
        } );

        $("tfoot input").keyup( function () {
            /* Filter on the column (the index) of this element */
            oTable.fnFilter( this.value, $("tfoot input").index(this) );
        } );

        /*
         * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
         * the footer
         */
        $("tfoot input").each( function (i) {
            asInitVals[i] = this.value;
        } );

        $("tfoot input").focus( function () {
            if ( this.className == "search_init" )
            {
                this.className = "";
                this.value = "";
            }
        } );

        $("tfoot input").blur( function (i) {
            if ( this.value == "" )
            {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        } );
    } //end createTableControls
    //si la vista se abre desde el calendario en un window se oculta el link de listar cotizaciones
    if($('#target').val()=='window')
        $('.link').hide();
</script>