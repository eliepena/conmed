<?php echo $view ?>
<script type="text/javascript">

    $(function(){

        $("#li-cat_letra-2115").hide();
        $("#li-id_tercero-2115").hide();
        $("#li-id_seguro-2115").hide();
        $("#li-direcciones_medico-2115").hide();

    });
    function afterSuccess(rsp){
        reloadGrid();
    }

    //Cambio de Emisor de Pago
    $("select[name='cat_pagador']").change(function(e){
        cat_pagador = $("select[name='cat_pagador'] option:selected").text();

        //Si el pagador es tercero
        if (cat_pagador == "Tercero"){
            $("#li-cat_letra-2115").show();
            //se resetea la aseguradora
            $("select[name='id_seguro']").val('').trigger('liszt:updated');
        }
        //Si el pagador es diferente de Tercero,
        else{
            // Se oculta tipo de persona, y tercero
            $("#li-cat_letra-2115").hide();
            $("#li-id_tercero-2115").hide();
        }

        //Si el pagador es aseguradora
        if(cat_pagador == "Aseguradora"){
            $("#li-id_seguro-2115").show();

            //Se ingresa la opcion Carta Aval
            if($("select[name='cat_tipo_pago'] option[value='32']").length <= 0)
                $("select[name='cat_tipo_pago']").prepend("<option value='32'>Carta Aval</option>")

            //se resetea tipo persona/tercero
            $("select[name='cat_tipo_pago']").val('').trigger('liszt:updated');
            $("select[name='id_tercero']").val('').trigger('liszt:updated');
            $("select[name='cat_letra']").val('').trigger('liszt:updated');
        }
        //Si el pagador es diferente de aseguradora
        else{
            //Se oculta la aseguradora
            $("#li-id_seguro-2115").hide();
            //Se quita la opcion de Carta Aval y se actualiza
            $("select[name='cat_tipo_pago'] option[value='32']").remove();
            $("select[name='cat_tipo_pago']").val('').trigger('liszt:updated');
        }

        //Resetea los combos Tercero Y Aseguradora para que no quede algo seleccionado
        if (cat_pagador != "Tercero" && cat_pagador != "Aseguradora"){
            $("select[name='id_seguro']").val('').trigger('liszt:updated');
            $("select[name='id_tercero']").val('').trigger('liszt:updated');
            $("select[name='cat_letra']").val('').trigger('liszt:updated');
        }

    });

    //Cambio de Tipo de Persona N/J. Muestra un boton de creacion de Persona o Empresa en caso de ser N o J
    $("select[name='cat_letra']").change(function(e){

        $("select[name='id_tercero'] option").remove();
        $("select[name='id_tercero']").val('').trigger('liszt:updated');

        cat_letra = $("select[name='cat_letra'] option:selected").text();

        //Muestra el seleccionador de la persona o empresa
        if(cat_letra!='')
            $("#li-id_tercero-2115").show();

        if (cat_letra == "Natural"){
               var button_tercero = [
                '<a href="#" role="button" class="btn btn-primary" data-toggle="modal" onclick="request({url:\'<?php echo base_url("persona/persona/create") ?>\'})">',
                'Crear Persona</a>'
            ].join('');
        }

        if (cat_letra == "Jurídico"){
               var button_tercero = [
                '<a href="#" role="button" class="btn btn-primary" data-toggle="modal" onclick="request({url:\'<?php echo base_url("empresa/empresa/create") ?>\'})">',
                'Crear Empresa</a>'
            ].join('');
        }

        var target = $("#button-id_tercero");
        target.html(button_tercero);

    });

    //Busqueda de los registros del elemento tercero segun la letra N-J seleccionada
    $("select[name='id_tercero']").ajaxChosen({
        dataType: 'json',
        type: 'GET',
        url:"<?= base_url() ?>cotizacion/cotizacion/EXC_searchSelect?letra="+$("select[name='cat_letra']").val()
    },{
        generateUrl: function(q){ return "<?= base_url() ?>cotizacion/cotizacion/EXC_searchSelect?letra="+$("select[name='cat_letra']").val(); },
        loadingImg: '<?= base_url() ?>assets/img/loading.gif'
    });

</script>
