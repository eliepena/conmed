<html>
    <head>
        <title></title>
        <style>
            body {
                font-size:9.5pt;
            }
            .head{
                background-color: #A5A5A5;
                font-weight:bold;
            }
            .right{
                text-align:right;
            }
            .left{
                text-align:left;
            }
            .center{
                text-align: center;
            }
            .borde_ext{

                border: 1px black solid;
            }
            .borde{

                border-top: 1px black solid;
                border-right: 1px black solid;
            }
            .borde_bottom{
                border-bottom:  1px black solid;
            }
            .borde_right{
                border-right:   1px black solid;
            }
            .borde_top{
                border-top:   1px black solid;
            }
            .borde_left{
                border-left: 1px black solid;
            }
        </style>
    </head>
    <body>

        <table border="0" style="left: -640px">
            <thead>
                <tr>
                    <th colspan ="3" style="width:56%" >

                    </th>

                    <th colspan ="1" style="width:9%;" class="  ">

                    </th>
                    <th colspan ="2" style="width:24%;" class="center">
                        <b><h2>BACKUP</h2></b>
                    </th>
                    <th colspan ="1" style="width:9%;" class="  ">

                    </th>
                </tr>
                <tr>
                    <th style="width:30%" rowspan="4" colspan ="1" class="borde_top borde_bottom borde_left left ">
                        <br/>
                        <b><?php echo $this->lang->line('PdfCot_Client_backup'); ?>:</b>
                        <br>
                        <font size="10"><?php echo $cliente; ?></font>
                    </th>
                    <th style="width:15%" rowspan="4" colspan ="1" class="borde_top borde_bottom borde_left left ">
                        <br/>
                        <b><?php echo $this->lang->line('PdfCot_Rif_backup'); ?>:</b>
                        <font size="10"><?php echo $rif; ?></font>
                    </th>
                    <th style="width:14%" rowspan="4" colspan ="1" class="borde_top borde_bottom borde_left borde_right left">
                        <br/>
                        <b><?php echo $this->lang->line('PdfCot_Nit_backup'); ?>:</b>
                        <br/>
                        <font size="10"><?php echo $nit; ?></font>
                    </th>
                    <th style="width:18%" colspan ="2" class="  right">
                        <?php echo $this->lang->line('PdfCot_NroCot_backup'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php echo $nro_cotizacion; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:18%" colspan ="2" class="  right">
                        <?php echo $this->lang->line('PdfCot_FecEmi_backup'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php echo $fecha_emision; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:18%" colspan ="2" class="  right">
                        <?php echo $this->lang->line('PdfCot_ForPag_backup'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php echo $forma_pago; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:18%" colspan ="2" class="  right">
                        <?php echo $this->lang->line('PdfCot_TiempEsp_backup'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php echo $tiempo_entrega; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:59%"  colspan ="3" class="borde_top borde_bottom borde_left borde_right left ">
                        <br/>
                        <b><?php echo $this->lang->line('PdfCot_Seg_backup'); ?>:</b>
                        <font size="10"><?php echo $seguro; ?></font>
                    </th>
                    <th style="width:18%" colspan ="2" class="  right">
                        <?php echo $this->lang->line('PdfCot_Vend_backup'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php echo $vendedor; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:59%"  colspan ="3" class="borde_top borde_bottom borde_left borde_right left ">
                        <br/>
                        <b><?php echo $this->lang->line('PdfCot_Tlf_backup'); ?>:</b>
                        <font size="10"><?php echo $tlf; ?></font>

                    </th>
                    <th style="width:18%" colspan ="2" class="  right">
                        <b><?php echo $this->lang->line('PdfCot_MedTratant_backup'); ?></b>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php echo $dr_tratante; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:59%" colspan ="3" class="borde_top ">
                        <b><?php echo $this->lang->line('PdfCot_MsjTabla_backup'); ?>:</b>
                    </th>
                    <th style="width:18%" colspan ="2" class="  right">&nbsp;</th>
                    <th style="width:23%" colspan ="2" >&nbsp;</th>
                </tr>
                <tr class="center">
                    <th style="width:6%" class="borde_top borde_bottom borde_left head">
                        <?php echo $this->lang->line('PdfCot_Cod_backup'); ?>
                    </th>
                    <th style="width:15%" class="borde_top borde_bottom borde_left head">
                        <?php echo $this->lang->line('PdfCot_Mod_backup'); ?>
                    </th>
                    <th style="width:43%" class="borde_top borde_bottom borde_left head">
                        <?php echo $this->lang->line('PdfCot_Descrip_backup'); ?>
                    </th>
                    <th style="width:7%" class="borde_top borde_bottom borde_left borde_right  head">
                        <?php echo $this->lang->line('PdfCot_Cant_backup'); ?>
                    </th>
                    <th style="width:14%" class="borde_top borde_bottom borde_left borde_right  head">
                        <?php echo $this->lang->line('PdfCot_Precio_backup'); ?>
                    </th>
                    <th style="width:14%" class="borde_top borde_bottom borde_left borde_right  head">
                        <?php echo $this->lang->line('PdfCot_Total_backup'); ?>
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                $monto_sub_total = 0;
                $calculo_iva = 0;
                $monto_total = 0;
                $cant_elementos = sizeof($cuerpo_tabla);
                $count = 1;
                foreach ($cuerpo_tabla AS $clave => $valor):
                    $style_row = ($count % 2 == 0 ? '' : '');
                    echo '<tr>
                            <td style="width:6%;' . $style_row . '" class="borde_left border_right ">' . $valor['id'] . '</td>
                            <td style="width:15%;' . $style_row . '" class="borde_left border_right ">' . $valor['modelo'] . '</td>
                            <td style="width:43%;' . $style_row . '" class="borde_left ">' . $valor['descripcion'] . '</td>
                            <td style="width:7%;' . $style_row . '"  class="borde_left borde_right right ">' . number_format($valor['cantidad'], 2, ',', '.') . '</td>
                            <td style="width:14%;' . $style_row . '" class="borde_left ">' . number_format($valor['precio'], 2, ',', '.') . '</td>
                            <td style="width:14%;' . $style_row . '"  class="borde_left borde_right right ">' . number_format($valor['precio'] * $valor['cantidad'], 2, ',', '.') . '</td>
                          </tr>';
                    $count = $count + 1;
                    $monto_sub_total = $monto_sub_total + ($valor['precio'] * $valor['cantidad']);
                endforeach;
                $count_space = 1;
                $espacio_fijo = 20 - $cant_elementos;
                if ($espacio_fijo > 0):
                    for ($j = 1; $j <= $espacio_fijo; $j++) :
                        $borde_top = ($count == $espacio_fijo ? ' borde_bottom ' : '');
                        echo '<tr>
                                    <td class="borde_left border_right ">&nbsp;</td>
                                    <td class="borde_left ">&nbsp;</td>
                                    <td class="borde_left ">&nbsp;</td>
                                    <td class="borde_left ">&nbsp;</td>
                                    <td class="borde_left ">&nbsp;</td>
                                    <td class="borde_left borde_right ">&nbsp;</td>
                                  </tr>';
                        $count_space = $count_space + 1;
                    endfor;
                endif;
                $calculo_iva = ($monto_sub_total * $this->config->item('valor_iva')) / 100;
                $monto_total = $monto_sub_total + $calculo_iva;
                ?>
            </tbody>
            <tfoot>
                <tr>
                    <td style="width:40%;font-weight: bold;text-decoration: underline;font-size:7pt;"   colspan = "2" class="borde_top">

                        <br/>

                    </td>
                    <td style="width:24%;font-weight: bold;text-decoration: underline;font-size:7pt;"   colspan = "1" class="borde_top center">
                        <br/>

                    </td>
                    <td style="width:21%" colspan = "3"  class="borde_top borde_left borde_right right">
                        <?php echo $this->lang->line('PdfCot_MontSubTotal_backup'); ?>:
                    </td>
                    <td style="width:14%" colspan = "1" class="borde_top borde_right right"><?php echo number_format($monto_sub_total, 2, ',', '.'); ?></td>
                </tr>

                <tr>
                    <td colspan="3" >&nbsp;</td>
                    <td colspan = "3" class="borde_top borde_left borde_right right">
                        <?php echo $this->lang->line('PdfCot_Iva_backup') . ' ' . $this->config->item('valor_iva') . ' % '; ?>:
                    </td>
                    <td colspan = "1" class="borde_top borde_right right"><?php echo number_format($calculo_iva, 2, ',', '.'); ?></td>
                </tr>
                <tr>
                    <td colspan="3" >&nbsp;</td>
                    <td colspan = "3" class="borde_bottom borde_top borde_left right">
                        <b><?php echo $this->lang->line('PdfCot_MontTotal_backup'); ?>:</b>
                    </td>
                    <td colspan = "1" class="borde_bottom borde_top borde_right borde_left right"><?php echo number_format($monto_total, 2, ',', '.'); ?></td>
                </tr>
            </tfoot>
        </table>
    </body>
</html>