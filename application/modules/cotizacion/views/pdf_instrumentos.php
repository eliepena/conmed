<html>
    <head>
        <title></title>
        <style>
            body {
                font-size:9.5pt;
            }
            .head{
                background-color: #A5A5A5;
                font-weight:bold;
            }
            .right{
                text-align:right;
            }
            .left{
                text-align:left;
            }
            .center{
                text-align: center;
            }
            .borde_ext{

                border: 1px black solid;
            }
            .borde{

                border-top: 1px black solid;
                border-right: 1px black solid;
            }
            .borde_bottom{
                border-bottom:  1px black solid;
            }
            .borde_right{
                border-right:   1px black solid;
            }
            .borde_top{
                border-top:   1px black solid;
            }
            .borde_left{
                border-left: 1px black solid;
            }
        </style>
    </head>
    <body>

        <table border="0" style="left: -640px">
            <thead>
                <tr>
                    <th colspan ="3" style="width:56%" >

                    </th>

                    <th colspan ="1" style="width:9%;" class="  ">

                    </th>
                    <th colspan ="2" style="width:24%;" class="center">
                        <b><h2>INSTRUMENTAL</h2></b>
                    </th>
                    <th colspan ="1" style="width:9%;" class="  ">

                    </th>
                </tr>
                <tr>
                    <th style="width:30%" rowspan="4" colspan ="1" class="borde_top borde_bottom borde_left left ">
                        <br/>
                        <b><?php echo $this->lang->line('PdfCot_Client_instru'); ?>:</b>
                        <br>
                        <font size="10"><?php echo $cliente; ?></font>
                    </th>
                    <th style="width:15%" rowspan="4" colspan ="1" class="borde_top borde_bottom borde_left left ">
                        <br/>
                        <b><?php echo $this->lang->line('PdfCot_Rif_instru'); ?>:</b>
                        <font size="10"><?php echo $rif; ?></font>
                    </th>
                    <th style="width:14%" rowspan="4" colspan ="1" class="borde_top borde_bottom borde_left borde_right left">
                        <br/>
                        <b><?php echo $this->lang->line('PdfCot_Nit_instru'); ?>:</b>
                        <br/>
                        <font size="10"><?php echo $nit; ?></font>
                    </th>
                    <th style="width:18%" colspan ="2" class="  right">
                        <?php echo $this->lang->line('PdfCot_NroCot_instru'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php echo $nro_cotizacion; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:18%" colspan ="2" class="  right">
                        <?php echo $this->lang->line('PdfCot_FecEmi_instru'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php echo $fecha_emision; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:18%" colspan ="2" class="  right">
                        <?php echo $this->lang->line('PdfCot_ForPag_instru'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php echo $forma_pago; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:18%" colspan ="2" class="  right">
                        <?php echo $this->lang->line('PdfCot_TiempEsp_instru'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php echo $tiempo_entrega; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:59%"  colspan ="3" class="borde_top borde_bottom borde_left borde_right left ">
                        <br/>
                        <b><?php echo $this->lang->line('PdfCot_Seg_instru'); ?>:</b>
                        <font size="10"><?php echo $seguro; ?></font>
                    </th>
                    <th style="width:18%" colspan ="2" class="  right">
                        <?php echo $this->lang->line('PdfCot_Vend_instru'); ?>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php echo $vendedor; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:59%"  colspan ="3" class="borde_top borde_bottom borde_left borde_right left ">
                        <br/>
                        <b><?php echo $this->lang->line('PdfCot_Tlf_instru'); ?>:</b>
                        <font size="10"><?php echo $tlf; ?></font>
                    </th>
                    <th style="width:18%" colspan ="2" class="  right">
                        <b><?php echo $this->lang->line('PdfCot_MedTratant_instru'); ?></b>
                    </th>
                    <th style="width:23%" colspan ="2" class=" borde_left right">
                        <?php echo $dr_tratante; ?>
                    </th>
                </tr>
                <tr>
                    <th style="width:59%" colspan ="3" class="borde_top ">
                        <b><?php echo $this->lang->line('PdfCot_MsjTabla_instru'); ?>:</b>
                    </th>
                    <th style="width:18%" colspan ="2" class="  right">&nbsp;</th>
                    <th style="width:23%" colspan ="2" >&nbsp;</th>
                </tr>
                <tr class="center">
                    <th style="width:10%" class="borde_top borde_bottom borde_left head">
                        <?php echo $this->lang->line('PdfCot_Cod_instru'); ?>
                    </th>
                    <th style="width:20%" class="borde_top borde_bottom borde_left head">
                        <?php echo $this->lang->line('PdfCot_Mod_instru'); ?>
                    </th>
                    <th style="width:62%" class="borde_top borde_bottom borde_left head">
                        <?php echo $this->lang->line('PdfCot_Descrip_instru'); ?>
                    </th>
                    <th style="width:7%" class="borde_top borde_bottom borde_left borde_right  head">
                        <?php echo $this->lang->line('PdfCot_Cant_instru'); ?>
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                $cant_elementos = sizeof($cuerpo_tabla);
                $count = 1;
                foreach ($cuerpo_tabla AS $clave => $valor):
                    $style_row = ($count % 2 == 0 ? '' : '');
                    echo '<tr >
                            <td style="width:10%;' . $style_row . '" class="borde_left border_right ">' . $valor['id'] . '</td>
                            <td style="width:20%;' . $style_row . '" class="borde_left border_right ">' . $valor['modelo'] . '</td>
                            <td style="width:62%;' . $style_row . '" class="borde_left ">' . $valor['descripcion'] . '</td>
                            <td style="width:7%;' . $style_row . '"  class="borde_left borde_right right ">' . $valor['cantidad'] . '</td>
                          </tr>';
                    $count = $count + 1;
                endforeach;
                $espacio_fijo = 38 - $cant_elementos;
                $count_space = 1;
                if ($espacio_fijo > 0):
                    for ($j = 1; $j <= $espacio_fijo; $j++) :
                        $borde_top = ($count == $espacio_fijo ? ' borde_bottom ' : '');
                        echo '<tr>
                                    <td class="borde_left border_right ' . $borde_top . '">&nbsp;</td>
                                    <td class="borde_left ' . $borde_top . '">&nbsp;</td>
                                    <td class="borde_left ' . $borde_top . '">&nbsp;</td>
                                    <td class="borde_left borde_right ' . $borde_top . '">&nbsp;</td>
                                  </tr>';
                        $count_space = $count_space + 1;
                    endfor;
                endif;
                ?>
            </tbody>
        </table>
    </body>
</html>