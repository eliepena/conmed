<?php if ($operation != 'detail'): ?>
    <br/>
    <button type ="button" id ="btn-cargaInstru" class="btn btn-primary " role="button" data-toggle="modal" onclick="show_articles(event,'instru','instrumentoTable',null,true,'INS',2);">
        Agregar Articulos
    </button>
    <div style="float:right;margin-top:-0px">
        <span style="float:left;font-size:7pt;"><b>Cero</b>&nbsp;&nbsp;</span>
        <div class="img-circle" style="background-color:red;float:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>

        <span style="float:left;font-size:7pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Bajo</b>&nbsp;&nbsp;</span>
        <div class="img-circle" style="background-color:yellow;float:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>

        <span style="float:left;font-size:7pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Disponible</b>&nbsp;&nbsp;</span>
        <div class="img-circle" style="background-color:blue;float:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
    </div>
    <br/><br/>
<?php endif; ?>
<div style="margin-top: 10px; margin-left: 20px">
    <table id="instrumentoTable" class=" <?php echo ($operation != 'detail' ? '' : ' js-table '); ?>  table table-striped table-bordered">
        <thead>
            <tr>
                <th width="10px"></th>
                <th width="10px"></th>
                <th>Modelo</th>
                <th>Descripción</th>
                <th width="10px">Cantidad</th>
                <?php if ($operation != 'detail'): ?>
                    <th width="10px">Stock</th>
                <?php endif; ?>
            </tr>
        </thead>
        <tbody>
            <?php if (isset($instrumentos) and !empty($instrumentos)): ?>
                <?php foreach ($instrumentos as $key => $instrumento): ?>
                    <tr id="tr-instru-<?php echo $instrumento['id']; ?>" >
                        <?php if ($operation != 'detail'): ?>
                            <td>
                                <input class="indicador-instru" id="instru-<?php echo $instrumento['id']; ?>" type="hidden" value="<?php echo $instrumento['id']; ?>" key="<?php echo $instrumento['id']; ?>" name="instru[<?php echo $instrumento['id']; ?>]" />
                                <i class="icon-remove" onclick="delete_tr('tr-instru-<?php echo $instrumento['id']; ?>')"/>
                            </td>
                        <?php else: ?>
                            <td></td>
                        <?php endif; ?>
                        <td><a data-toggle="modal" href="#" onclick="show_img_article('<?php echo $instrumento['modelo'] ?>',  '<?php echo $instrumento['descripcion'] ?>', '<?php echo $instrumento['imagen'] ?>');"><i class="icon-picture"></i></a></td>
                        <td id="model-instru-<?= $instrumento['id']; ?>" ><?php echo $instrumento['modelo']; ?></td>
                        <td id="name-instru-<?= $instrumento['id']; ?>" ><?php echo $instrumento['descripcion']; ?></td>
                        <?php
                        if ($operation != 'detail'):
                            $value = "value='{$instrumento['cantidad']}'";
                            $readonly = ' ';
                        else:
                            $readonly = ' readonly="readonly" ';
                            $value = "value='{$instrumento['cantidad']}'";
                        endif;
                        ?>
                        <td><input min="1" onblur="check_disponibilidad(this.id,'instru','instrumentoTable');" id="cant-instru-<?php echo $instrumento['id']; ?>" type="number" class="input-mini" name="cant_instru[<?php echo $instrumento['id']; ?>]" placeholder="0" key="<?php echo $instrumento['id']; ?>" <?= $readonly ?> <?= $value ?>/></td>
                        <?php
                        $leyenda = '';
                        $disponibilidad = (empty($instrumento['disponibilidad']) || $instrumento['disponibilidad'] < 1 ? 0 : $instrumento['disponibilidad']);
                        $arrayLeyenda[1] = 'red';
                        $arrayLeyenda[2] = 'yellow';
                        $arrayLeyenda[3] = 'blue';
                        if ($instrumento['cantidad'] > $disponibilidad):
                            $leyenda = $arrayLeyenda[1];
                        elseif ($instrumento['cantidad'] == $disponibilidad):
                            $leyenda = $arrayLeyenda[2];
                        else:
                            $leyenda = $arrayLeyenda[3];
                        endif;
                        if ($operation != 'detail'):
                            ?>

                            <td >
                                <div class="img-circle" id="leyenda-instru-<?= $instrumento['id'] ?>" style="background-color:<?php echo $leyenda; ?>;">
                                    <input type="hidden" id="disp-instru-<?= $instrumento['id'] ?>" value="<?php echo $disponibilidad; ?>"/>
                                    &nbsp;&nbsp;&nbsp;
                                </div>
                            </td>
                        <?php endif; ?>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
    <?php if ($operation != 'detail'): ?>
        <div class="form-actions">
            <input type="button" onclick="navigate_tab('prev',4);" value="Anterior" class="btn btn-primary">
            <input type="button" onclick="navigate_tab('next',4);" value="Siguiente" class="btn btn-primary">
        </div>
    <?php endif ?>
</div>
<!-- Modal Imagen de Instrumento -->
<div id="instrumentoModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="instrumentoModalLabel"></h3>
    </div>
    <div class="m-body">
        <table align="center">
            <tr>
                <td><img id='selected-instrumento-image' width="100%" height="100%"/></td>
                <td >
                    <label><strong>Descripción</strong></label>
                    <span id="selected-instrumento-description"></span>
                </td>
            </tr>
        </table>
        <br/>
    </div>
</div>
<!-- Modal Nombre de Tecnica -->
<div id="myModal-tecnica" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel-tec">
            ¿Desea guardar el nombre de la técnica?</h4>
    </div>
    <div class="modal-body">
        <div id="validation_tecnica"></div>
        <label>Nombre</label> <input type="text" name="nombre_tecnica" id="nombre_tecnica" maxlength="50" class="input-xlarge">
    </div>

    <div class="modal-footer">
        <input  type="button" required="required" onclick="valid_tecnica()" value="Sí, Guardar Técnica" class="btn btn-primary"  />
        <input  type="button" required="required" onclick="save()" value="No, Guardar sin Técnica" class="btn"  />
    </div>
</div>

<!-- Modal Crear BackUp -->
<div id="myModal-backup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel-tec">
            ¿Desea Crear su Backup Personalizado?</h4>
    </div>
    <div class="modal-footer">
        <input  type="button" required="required" onclick=" create_backup()" value="Sí, Crear BackUp" class="btn btn-primary"  />
        <input  type="button" required="required" onclick="save_tecnica()" value="No, Guardar sin BackUp" class="btn"  />
    </div>
</div>
<script type="text/javascript">
    cant_articulo('instrumentoTable','cant_instru','cant-instru','instru');
</script>


<?php if ($operation == 'detail'): ?>
    <?php if (isset($articulos_sustituidos) and sizeof($articulos_sustituidos) > 0): ?>
        <div style="margin-top: 10px; margin-left: 20px">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th style="background-color: #F9F9F9;" width="25%"><center>INSTRUMENTO SUSTITUIDO</center></th>
                <th style="background-color: #F9F9F9;"><center>

                    <table  class="table table-striped table-bordered">
                        <tr>

                            <th colspan="5"><center>ARTICULO SUSTITUTO</center></th>

                        </tr>
                        <tr>

                            <th width="20px" ></th>
                            <th width="80px" >Modelo</th>
                            <th>Descripción</th>
                            <th width="50px">Cant</th>
                        </tr>

                    </table>

                </center></th> </tr>



                <?php foreach ($articulos_sustituidos as $key => $value): ?>


                    <tr width="25%" ><td ><center><b><?php echo $value['modelo']; ?></b></center></td>
                    <td>
                        <table  class="table table-striped table-bordered">
                            <tbody>
                                <?php foreach ($value['sustituidos'] as $clave => $material): ?>
                                    <tr>

                                        <td width="20px" ><a data-toggle="modal" href="#" onclick="show_img_article('<?= $material['modelo'] ?>',  '<?= $material['descripcion'] ?>', '<?= $material['imagen'] ?>');"><i class="icon-picture"></i></a></td>
                                        <td width="80px" ><?php echo $material['modelo'] ?></td>
                                        <td><?php echo $material['descripcion'] ?></td>
                                        <td width="50px" ><?php echo $material['cantidad']; ?> </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </td>
                    </tr>

                <?php endforeach; ?>
                </tbody>
            </table>


        </div>
    <?php endif; ?>
<?php endif; ?>