<?php
echo $detail_quote;
?>
<?php if (isset($instrumentalists) && $instrumentalists): ?>
    <table  class="table table-striped table-bordered">
        <thead>
            <tr>
                <th colspan="5"><center>INSTRUMENTISTAS ASOCIADOS</center></th>
    </tr>
    <tr>
        <th style="width: 20px;"></th>
        <th>Cod</th>
        <th>Nombre</th>
        <th>Cedula</th>
        <th>Correo</th>
    </tr>
    </thead>
    <tbody>


        <?php foreach ($instrumentalists as $key => $value): ?>
        <td style="width: 20px;"></td>
        <td><?php echo $value['id']; ?></td>
        <td><?php echo $value['label']; ?></td>
        <td><?php echo $value['cedula']; ?></td>
        <td><?php echo $value['correo_electronico']; ?></td>
    <?php endforeach; ?>


        <!--<td colspan="5"><center>No posee Instrumentistas Asociados</center></td>-->

    </tbody>
    <tfoot>

    </tfoot>
    </table>
<?php endif; ?>