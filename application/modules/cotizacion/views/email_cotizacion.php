<html>
    <body>
        <h1><?= $title; ?></h1>
        <p>Bienvenido al Sistema de Cotizaciones Conmed<br>
            <br></p>       
        <?= $html; ?>
        <br>
        <label><b>Medico: </b></label><?= $datos['id_medico']; ?><br>
        <label><b>Paciente: </b></label><?= $datos['id_paciente'] ?><br>
        <label><b>Centro de Salud: </b></label><?= htmlentities($datos['id_centro_salud']) ?><br>
        <label><b>Articulacion: </b></label><?= $datos['id_articulacion'] ?><br>
        <label><b>Tecnica: </b></label><?= $datos['id_tecnica'] ?><br>
        <label><b>Lesion: </b></label><?= $datos['id_lesion'] ?><br>
        <img src="<?php echo base_url() ?>assets/img/logo.png"><br>
        <small> si le parece que el mensaje no es correcto, por favor ignore este correo.</small>
    </body>
</html>

