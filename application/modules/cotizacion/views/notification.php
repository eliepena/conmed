<?php
$permissions = $this->session->userdata('permissions');
$role = $this->session->userdata('role_name');
?>
<span class="span10">

    <strong>N° de Cotización:</strong> <?php echo $data['codigo'] ?> <br />
    <strong>Fecha de Cotización:</strong> <?php echo date('d-m-Y H:i', strtotime($data['fecha_cotizacion'])) ?><br />
    <strong>Vendedor:</strong> <?php echo $data['id_vendedor'] ?><br />
    <strong>Médico Tratante:</strong> <?php echo $data['id_medico'] ?><br />
    <?php if (!isset($permissions[2109]) || $role == 'Admin'): ?>
        <strong>Monto de la cotización:</strong> <?php echo number_format($data['monto_total'], 2, ',', '.') ?><br />
        <strong>Forma de Pago:</strong> <?php echo $data['tipo_pago'] ?><br />
    <?php endif; ?>


    <strong>Estatus: </strong>
    <span>
        <?php if ($data['all_status']): ?>
            <ul>
                <?php foreach ($data['all_status'] as $status) : ?>
                    <li>
                        <?php echo $status['status_name'] ?>: <?php echo date('d-m-Y H:i', strtotime($status['status_date'])) ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </span>

    <?php $permissions = $this->session->userdata('permissions'); ?>
    <?php // if ($target_on != 'window'): ?>



    <?php if (isset($permissions[2105]) and $data['validate']): ?>
        <?php $permission = $permissions[2105] ?>
        <span id="button-<?php echo $permission['_name'] ?>">
            Pendiente por Validar:
            <br />
            <a href="#" role="button" class="btn btn-primary btn-mini" data-toggle="modal"
               onclick="request({url:'<?php echo base_url($permission['url']) ?>', data:{id:'<?php echo $data['id_ec'] ?>'}})">
                   <?php echo $permission['_name'] ?>
            </a>
            <br />
        </span>
    <?php endif; ?>


    <?php if (isset($permissions[2103]) and $data['current_status']['status_name'] == 'Procesada'): ?>
        <?php $permission = $permissions[2103] ?>
        <span id="button-<?php echo $permission['_name'] ?>">
            Medio de Pago:
            <br />
            <a href="#" role="button" class="btn btn-primary btn-mini" data-toggle="modal"
               onclick="request({url:'<?php echo base_url($permission['url']) ?>', data:{id:'<?php echo $data['id_ec'] ?>'}})">
                   <?php echo $permission['_name'] ?>
            </a>
            <br />
        </span>
    <?php elseif (isset($permissions[2104]) and $data['correct'] and $data['current_status']['status_name'] != 'Reprogramar Cirugia') : ?>
        <?php $permission = $permissions[2104] ?>
        <span id="button-<?php echo $permission['_name'] ?>">
            Pendiente por Corregir:
            <br />
            <a href="#" role="button" class="btn btn-primary btn-mini" data-toggle="modal"
               onclick="request({url:'<?php echo base_url($permission['url']) ?>', data:{id:'<?php echo $data['id_ec'] ?>'}})">
                   <?php echo $permission['_name'] ?>
            </a>
            <br />
        </span>
    <?php endif; ?>
    <?php if (isset($permissions[2107]) and ($data['current_status']['status_name'] == 'Reprogramar Cirugia' or $data['current_status']['status_name'] == 'Aprobada')): ?>
        <?php $permission = $permissions[2107] ?>
        <span id="button-<?php echo $permission['_name'] ?>">
            Fecha de cirugia:
            <br />
            <a href="#" role="button" class="btn btn-primary btn-mini" data-toggle="modal"
               onclick="request({url:'<?php echo base_url($permission['url']) ?>', data:{id:'<?php echo $data['id_ec'] ?>'}})">
                Reprogramar Fecha de Cirugia   <?php // echo $permission['_name']   ?>
            </a>
            <br />
        </span>

    <?php endif; ?>
    <?php if (isset($permissions[2111]) && $data['fecha_cirugia'] == ''): ?>
        <?php $permission = $permissions[2111] ?>
        <br />
        <span id="button-<?php echo $permission['_name'] ?>">
            Pendiente por fijar fecha de Cirugía:
            <br />
            <a href="#" role="button" class="btn btn-primary btn-mini" data-toggle="modal"
               onclick="request({url:'<?php echo base_url($permission['url']) ?>', data:{id:'<?php echo $data['id_ec'] ?>'}})">
                   <?php echo $permission['_name'] ?>
            </a>
            <br />
        </span>
        <?php
    endif;
    if (isset($permissions[2110]) and isset($permissions[2108]) and $data['current_status']['status_name'] == 'Pre-Aprobada' or $data['current_status']['status_name'] == 'Reprogramada'):
        if (isset($permissions[2110]) and isset($permissions[2108])):
            $permission = $permissions[2110]
            ?>
            <span id="button-<?php echo $permission['_name'] ?>">
                Acciones:
                <br />
                <a href="#" role="button" class="btn btn-primary btn-mini" data-toggle="modal"
                   onclick="request({url:'<?php echo base_url($permission['url']) ?>', data:{id:'<?php echo $data['id_ec'] ?>'}})">
                       <?php echo $permission['_name'] ?>
                </a>
                <?php $permission = $permissions[2108] ?>
                <a href="#" role="button" class="btn btn-primary btn-mini" data-toggle="modal"
                   onclick="request({url:'<?php echo base_url($permission['url']) ?>', data:{id:'<?php echo $data['id_ec'] ?>'}})">
                       <?php echo $permission['_name'] ?>
                </a>
                <br />
            </span><br/>
            <?php
        endif;

    endif;

    if (isset($permissions[2116]) and $data['current_status']['status_name'] == 'Sustituir Articulos'):
        if (isset($permissions[2116])):
            $permission = $permissions[2116]
            ?>
            <span id="button-<?php echo $permission['_name'] ?>">
                Pendiente por sustitucion de articulos:
                <br />
                <a href="#" role="button" class="btn btn-primary btn-mini" data-toggle="modal"
                   onclick="request({url:'<?php echo base_url($permission['url']) ?>', data:{id:'<?php echo $data['id_ec'] ?>',content:true}})">
                       <?php echo $permission['_name'] ?>
                </a>
                <br />
            </span><br/>
            <?php
        endif;
    endif;
    
    if (isset($permissions[15102]) and $data['current_status']['status_name'] == 'Aprobada'):
        if (isset($permissions[15102])):
            $permission = $permissions[15102]
            ?>
            <span id="button-<?php echo $permission['_name'] ?>">
                
                <br />
                <a href="#" role="button" class="btn btn-primary " data-toggle="modal"
                   onclick="request({url:'<?php echo base_url($permission['url']) ?>'})">
                       <?php echo $permission['_name'] ?>
                </a>
                <br />
            </span><br/>
            <?php
        endif;
    endif;
    ?>

</span>
<input type="hidden" id="target" value="<?php echo $target_on; ?>" />
<script>
    function createTableControls(){
        var asInitVals = new Array();
        var oTable = $('.js-table').dataTable( {
            "oLanguage": {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "fnInfoCallback": null,
                "oAria": {
                    "sSortAscending":  ": Activar para ordernar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordernar la columna de manera descendente"
                }
            }
        } );

        $("tfoot input").keyup( function () {
            /* Filter on the column (the index) of this element */
            oTable.fnFilter( this.value, $("tfoot input").index(this) );
        } );

        /*
         * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
         * the footer
         */
        $("tfoot input").each( function (i) {
            asInitVals[i] = this.value;
        } );

        $("tfoot input").focus( function () {
            if ( this.className == "search_init" )
            {
                this.className = "";
                this.value = "";
            }
        } );

        $("tfoot input").blur( function (i) {
            if ( this.value == "" )
            {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        } );
    } //end createTableControls
    //si la vista se abre desde el calendario en un window se oculta el link de listar cotizaciones
    if($('#target').val()=='window')
        $('.link').hide();
</script>