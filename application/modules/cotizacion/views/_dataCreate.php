<?php
$permissions = $this->session->userdata('permissions');
$role = $this->session->userdata('role_name');
?>
<script type="text/javascript">
    $(function(){
        //Ejecuta las Validaciones del Cliente
        new FormValidator('_form-2115', buildRules('_form-2115'),
        function(errors, event){
            var flag = validateForm(errors, event);

            if (event && event.preventDefault) {
                event.preventDefault();
            } else if (event) {
                event.returnValue = false;
            }

            //validacion submit
            if(flag){
                //este archivo es particular,
                //Valida TAB DATOS luego de ser validada en el cliente.
                validate_data_tab();
            }
        });

        customFields();

    });

    /**
     * Procesa todo el formulario
     *
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V- 1.0  02/09/13 08:43 AM
     */
    function save_method_of_payment(){
        var formData = $('#_form').serializeArray();
//        formData.push({
//            name:"nombre_tecnica" ,
//            value:$("#nombre_tecnica").val()
//        });
        $('#myModal-tecnica').modal('hide');


        $('body').modalmanager('loading');
        //Dispara el evento
        $.ajax({
            url: BASE_URL +'cotizacion/method_of_payment/create/process',
            data: formData,

            success: function(response){
                $('body').modalmanager('loading');

                //Se procesa cuando el registro se guarda exitosamente
                if(response.view == 1){
                    $("#_form").hide();
                    var textAlert = 'Datos guardados satisfactoriamente';
                    var alertHtml = [
                        '<div class="alert alert-success fade in">',
                        '<button type="button" class="close" data-dismiss="alert">×</button>',
                        textAlert,
                        '</div>'
                    ].join('');
                    var link ='<a id="link" href="#" class="btn btn-success" onclick="request({url:\''+BASE_URL+'cotizacion/method_of_payment/listAll\'})">Regresar al listado</a>' ;
                    $("#link").html(link);
                    $('#link').on('click', function() {
                        request({
                            url:BASE_URL +'cotizacion/cotizacion/listAll'
                        });
                    });
                    setTimeout(function(){
                        $('#link').trigger('click');
                    },2000);

                }

                //Se procesa cuando no se puede guardar el registro
                else{
                    //Ocurre cuando hay un error en Base de Datos
                    if(response.view == false)
                        var textAlert = 'No se pudo guardar el registro, si el problema persiste comuniquese con sistemas';

                    //Ocurre cuando hay errores de validacion en el Servidor
                    else
                        var textAlert = response.view;

                    //Mensaje de Alerta Error
                    var alertHtml = [
                        '<div class="alert alert-error fade in">',
                        '<button type="button" class="close" data-dismiss="alert">×</button>',
                        textAlert,
                        '</div>'
                    ].join('');

                }
                //Elemento que muestra el Alert
                var target = $("#alert_content");
                target.html(alertHtml);

            }
        });
    }

</script>
<ul>

    <li id="li-label_forma_pago-2115">
        <br/>
        <label for="label_forma_pago">
            <strong>FORMA DE PAGO</strong></label>
        <div id="label-label_forma_pago"></div>
    </li>
    <li id="li-cat_pagador-2115">
        <div class="control-group">
            <label class="control-label" for="623">Emisor de Pago<span style='color:red;font-size: 16px'> *</span></label>
            <div class="controls">
                <select name="cat_pagador"
                        id="623"
                        data-placeholder="Seleccione un Emisor de Pago"
                        class="chzn-select"
                        data-rules="required|integer"
                        data-display="Emisor de Pago"
                        data-content="Indique el Emisor de Pago"
                        title="Emisor de Pago"
                        >
                    <option value=""></option>
                    <?php if (isset($emisor_pago) && sizeof($emisor_pago) > 0): ?>
                        <?php foreach ($emisor_pago as $key => $value): ?>
                            <option value="<?php echo $value['id']; ?>" ><?php echo $value['_label']; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <span id="button-cat_pagador"></span>
            </div>
        </div>
    </li>
    <li id="li-id_seguro-2115">
        <div class="control-group">
            <label class="control-label" for="279">Aseguradora</label>
            <div class="controls">
                <select name="id_seguro"
                        id="279"
                        data-placeholder="Seleccione una Aseguradora"
                        class="chzn-select"
                        data-rules="integer|rewrite"
                        data-display="Aseguradora"
                        data-content="Indique la aseguradora"
                        title="Aseguradora"
                        >
                    <option value=""></option>
                    <?php if (isset($aseguradora) && sizeof($aseguradora) > 0): ?>
                        <?php foreach ($aseguradora as $key => $value): ?>
                            <option value="<?php echo $value['id']; ?>" ><?php echo $value['nombre']; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <span id="button-id_seguro"></span>
            </div>
        </div>
    </li>
    <li id="li-cat_letra-2115">
        <div class="control-group">
            <label class="control-label" for="636">Tercero (Persona/Empresa)</label>
            <div class="controls">
                <select name="cat_letra"
                        id="636"
                        data-placeholder="Seleccione el Tipo de Persona"
                        class="chzn-select"
                        data-rules="integer|rewrite"
                        data-display="Tercero (Persona/Empresa)"
                        data-content="Indique el tipo de persona"
                        title="Tercero (Persona/Empresa)"
                        >
                    <option value=""></option>
                    <?php if (isset($tipo_persona) && sizeof($tipo_persona) > 0): ?>
                        <?php foreach ($tipo_persona as $key => $value): ?>
                            <option value="<?php echo $value['id']; ?>" ><?php echo $value['_label']; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <span id="button-cat_letra"></span>
            </div>
        </div>
    </li>
    <li id="li-id_tercero-2115">
        <div class="control-group">
            <label class="control-label" for="628">Persona/Empresa</label>
            <div class="controls">
                <select name="id_tercero"
                        id="628"
                        data-placeholder="Seleccione el Tercero"
                        class="chzn-select"
                        data-rules="integer|rewrite"
                        data-display="Persona/Empresa"
                        data-content="Indique el tercero (Persona o Empresa) que emitirá el pago"
                        title="Persona/Empresa"
                        >
                    <option value=""></option>
                </select>
                <script type="text/javascript">
                </script>
                <span id="button-id_tercero">
                    <a href="#" role="button" class="btn btn-primary" data-toggle="modal"
                       onclick="request({url:BASE_URL +'persona/persona/create', data:{id:'',target_on:'window'}})">
                        Crear Persona            </a>
                </span>
            </div>
        </div>
    </li>
    <li id="li-cat_tipo_pago-2115">
        <div class="control-group">
            <label class="control-label" for="300">Forma de Pago<span style='color:red;font-size: 16px'> *</span></label>
            <div class="controls">
                <select name="cat_tipo_pago"
                        id="300"
                        data-placeholder="Seleccione la forma de pago"
                        class="chzn-select"
                        data-rules="required|integer"
                        data-display="Forma de Pago"
                        data-content="Indique la forma de pago de la cotización"
                        title="Forma de Pago"
                        >

                    <option value=""></option>
                    <?php if (isset($tipo_pago) && sizeof($tipo_pago) > 0): ?>
                        <?php foreach ($tipo_pago as $key => $value): ?>
                            <option value="<?php echo $value['id']; ?>" ><?php echo $value['_label']; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>

                </select>
                <span id="button-cat_tipo_pago"></span>
            </div>
        </div>
    </li>

    <li id="li-label_observacion-2115">
        <br/>
        <label for="label_forma_pago">
            <strong>OBSERVACIONES</strong></label>
        <div id="label-label_observacion"></div>
    </li>
    <li id="li-observacion-2115">
        <div class="control-group">
            <label class="control-label" for="enviar-2115">Observaciones</label>
            <div class="controls">
                <textarea
                    class="span3"
                    name="observacion"
                    id="observacion-2115"
                    placeholder="Ej: Procesar de manera rapida"
                    data-rules=""
                    data-display="Observaciones"
                    data-content="Llene el campo si posee Observaciones"
                    title="Observaciones"
                    rows="5"
                    ></textarea>
            </div>
        </div>
    </li>
    <li><br/>
        <div class="form-actions ">
            <input type="button" class="btn btn-primary" onclick="save_method_of_payment();" value="Guardar" />
        </div>
    </li>
</ul>