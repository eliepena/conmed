<?php
$permissions = $this->session->userdata('permissions');
$role = $this->session->userdata('role_name');
?>
<script type="text/javascript">

    $(function(){


        //Ejecuta las Validaciones del Cliente
        new FormValidator('_form', buildRules('_form'),
        function(errors, event){
            var flag = validateForm(errors, event);

            if (event && event.preventDefault) {
                event.preventDefault();
            } else if (event) {
                event.returnValue = false;
            }

            //validacion submit
            if(flag){
                //este archivo es particular,
                //Valida TAB DATOS luego de ser validada en el cliente.
                validate_data_tab();
            }
        });

        customFields();
        build_table_articles();
    });
</script>
<ul>
    <li id="li-label_lesion-2101">
        <br/>
        <label for="label_lesion">
            <strong>DATOS DE CIRUGíA</strong></label>
        <div id="label-label_lesion"></div>
    </li>

    <li id="li-fecha_cotizacion-2101">
        <div class="control-group">
            <label class="control-label" for="fecha_cotizacion-2101">Fecha de Cotizacion<span style='color:red;font-size: 16px'> *</span></label>
            <div class="controls">
                <div id="fecha_cotizacion-2101-container" class="input-append date" data-date data-date-format="dd-mm-yyyy" data-date-start-date='<?php echo date("d-m-Y"); ?>'>
                    <input class="date input-small" type="text" id="fecha_cotizacion-2101" name="fecha_cotizacion" data-rules="valid_date" data-display="Fecha de Cotizacion"  title="Fecha de Cotizacion" placeholder="Ej: dd-mm-aaaa" readonly="readonly" value="<?php echo date("d-m-Y"); ?>">

                    <span class="add-on"><i class="icon-calendar"></i></span>
                </div>
            </div>
        </div>
    </li>
    <?php if ($role == 'Recepcionista'): ?>
        <li id="li-id_paciente-2101">
            <div class="control-group">
                <label class="control-label" for="283">Vendedor</label>
                <div class="controls">
                    <select name="vendedor"
                            id=""
                            data-placeholder=" Seleccione el Vendedor"
                            class="chzn-select"
                            data-rules="required"
                            data-display="Vendedor"
                            data-content="Indique el vendedor"
                            title="Vendedor"
                            >
                        <option value=""></option>
                        <?php
                        if (isset($vendedor) && sizeof($vendedor) > 0):
                            $option_vendedor = '';
                            foreach ($vendedor as $key => $value):
                                $option_vendedor = $value['primer_nombre'] . ' ' . $value['primer_apellido'];
                                ?>
                                <option value="<?php echo $value['id']; ?>"  ><?php echo $option_vendedor; ?></option>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </select>
                </div>
            </div>
        </li>
    <?php endif; ?>
    <li id="li-id_paciente-2101">
        <div class="control-group">
            <label class="control-label" for="283">Paciente<span style='color:red;font-size: 16px'> *</span></label>
            <div class="controls">
                <select name="id_paciente"
                        id="283"
                        data-placeholder="Seleccione un Paciente"
                        class="chzn-select"
                        data-rules="required"
                        data-display="Paciente"
                        data-content="Indique el paciente"
                        title="Paciente"
                        >
                    <option value=""></option>
                    <?php
                    if (isset($paciente) && sizeof($paciente) > 0):
                        $option_paciente = '';
                        foreach ($paciente as $key => $value):
                            $option_paciente = $value['cedula'] . ' ' . $value['primer_nombre'] . ' ' . $value['primer_apellido'];
                            ?>
                            <option value="<?php echo $value['id']; ?>"  ><?php echo $option_paciente; ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
                <span id="button-id_paciente">
                    <a href="#" role="button" class="btn btn-primary" data-toggle="modal"
                       onclick="request({url:BASE_URL +'persona/paciente/create', data:{id:'',target_on:'window'}})">
                        Crear Paciente            </a>
                </span>
            </div>
        </div>
    </li>
    <li id="li-id_precios-2101">
        <div class="control-group">
            <label class="control-label" for="970">Tipo de Precio<span style='color:red;font-size: 16px'> *</span></label>
            <div class="controls">
                <select name="id_precios"
                        id="970"
                        data-placeholder="Selecione un Tipo de Precio"
                        class="chzn-select"
                        data-rules="required"
                        data-display="Tipo de Precio"
                        data-content="Indique el tipo de precio"
                        title="Tipo de Precio"
                        >
                    <option value=""></option>
                    <?php
                    if (isset($precio) && sizeof($precio) > 0):
                        foreach ($precio as $key => $value):
                            ?>
                            <option value="<?php echo $value['id']; ?>"  ><?php echo $value['nombre']; ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
                <script type="text/javascript">
                </script>
                <span id="button-id_precios"></span>
            </div>
        </div>
    </li>
    <li id="li-id_articulacion-2101">
        <div class="control-group">
            <label class="control-label" for="633">Articulación<span style='color:red;font-size: 16px'> *</span></label>
            <div class="controls">
                <select name="id_articulacion"
                        id="633"
                        data-placeholder="Seleccione la Articulación"
                        class="chzn-select"
                        data-rules="required|integer"
                        data-display="Articulación"
                        data-content="Indique la articulación lesionada"
                        title="Articulación"
                        >
                    <option value=""></option>
                    <?php
                    if (isset($articulacion) && sizeof($articulacion) > 0):
                        foreach ($articulacion as $key => $value):
                            ?>
                            <option value="<?php echo $value['id']; ?>"  ><?php echo $value['nombre']; ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
                <script type="text/javascript">
                </script>
                <span id="button-id_articulacion"></span>
            </div>
        </div>
    </li>
    <li id="li-id_lesion-2101">
        <div class="control-group">
            <label class="control-label" for="634">Lesión<span style='color:red;font-size: 16px'> *</span></label>
            <div class="controls">
                <select name="id_lesion"
                        id="634"
                        data-placeholder="Seleccione la Lesión"
                        class="chzn-select"
                        data-rules="required|integer"
                        data-display="Lesión"
                        data-content="Indique el tipo de lesión"
                        title="Lesión"
                        >

                    <option value=""></option>

                    <?php
                    if (isset($lesion) && sizeof($lesion) > 0):
                        foreach ($lesion as $key => $value):
                            ?>
                            <option value="<?php echo $value['id']; ?>" class='<?php echo $value['id_parent']; ?>' ><?php echo $value['nombre']; ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
                <script type="text/javascript">

                    $(function() {
                        $('#634').chained('#633');
                        $('#634').trigger('liszt:updated');
                        $('#633').bind('change', function(){
                            $('#634').trigger('liszt:updated')
                        });
                    });
                </script>
                <span id="button-id_lesion"></span>
            </div>
        </div>
    </li>
    <li id="li-id_tecnica-2101">
        <div class="control-group">
            <label class="control-label" for="635">Técnica</label>
            <div class="controls">
                <select name="id_tecnica"
                        id="635"
                        data-placeholder="Seleccione la Técnica Utilizada"
                        class="chzn-select"
                        data-rules="integer"
                        data-display="Técnica"
                        data-content="Indique la técnica utilizada en la cirugía"
                        title="Técnica"
                        >
                    <option value=""></option>
                    <?php
                    if (isset($tecnica) && sizeof($tecnica) > 0):
                        foreach ($tecnica as $key => $value):
                            ?>
                            <option value="<?php echo $value['id']; ?>" class='<?php echo $value['id_parent']; ?>' ><?php echo $value['nombre']; ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
                <script type="text/javascript">
                    $(function() {
                        $('#635').chained('#634');
                        $('#635').trigger('liszt:updated');
                        $('#634').bind('change', function(){
                            $('#635').trigger('liszt:updated')
                        });
                    });
                </script>
                <span id="button-id_tecnica"></span>
            </div>
        </div>
    </li>
    <li id="li-id_medico-2101">
        <div class="control-group">
            <label class="control-label" for="284">Médico<span style='color:red;font-size: 16px'> *</span></label>
            <div class="controls">
                <select name="id_medico"
                        id="284"
                        data-placeholder="Seleccione el Medico tratante"
                        class="chzn-select"
                        data-rules="required"
                        data-display="Médico"
                        data-content="Indique el médico tratante"
                        title="Médico"
                        >

                    <option value=""></option>
                    <?php
                    if (isset($medico) && sizeof($medico) > 0):
                        $option_medico = '';
                        foreach ($medico as $key => $value):
                            $option_medico = $value['cedula'] . ' ' . $value['primer_nombre'] . ' ' . $value['primer_apellido'];
                            ?>
                            <option value="<?php echo $value['id']; ?>" ><?php echo $option_medico; ?></option>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
                <span id="button-id_medico"></span>
            </div>
        </div>
    </li>
    <li id="li-id_centro_salud-2101">
        <div class="control-group">
            <label class="control-label" for="278">Centro de Salud<span style='color:red;font-size: 16px'> *</span></label>
            <div class="controls">
                <select name="id_centro_salud"
                        id="278"
                        data-placeholder="Seleccione un Centro de Salud"
                        class="chzn-select"
                        data-rules="required|integer"
                        data-display="Centro de Salud"
                        data-content="Indique el centro de salud."
                        title="Centro de Salud"
                        >
                    <option value=""></option>
                    <?php
                    if (isset($centro_salud) && sizeof($centro_salud) > 0):

                        foreach ($centro_salud as $key => $value):
                            $localidad_centro = ($value['localidad'] ? $value['localidad'] . ',' : '');
                            $direccion_centro = ($value['direccion'] ? $value['direccion'] : '');
                            ?>
                            <option
                                value="<?php echo $value['id']; ?>"
                                direccion="<?php echo "$localidad_centro $direccion_centro"; ?>"><?php echo $value['nombre']; ?>
                            </option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <span id="button-id_centro_salud"></span>

            </div>
        </div>
    </li>
    <li id="li-tipo_direccion-2101">
        <div class="control-group">
            <label class="control-label" for="971">Tipo de direccion<span style='color:red;font-size: 16px'> *</span></label>
            <div class="controls">
                <select name="tipo_direccion"
                        id="971"
                        data-placeholder="Seleccione la Direccion de Envio"
                        class="chzn-select"
                        data-rules="required"
                        data-display="Tipo de direccion"
                        data-content="Indique a quien pertenece la direccion "
                        title="Tipo de direccion"
                        >
                    <option value=""></option>
                    <?php if (isset($tipo_direccion) && sizeof($tipo_direccion) > 0): ?>
                        <?php foreach ($tipo_direccion as $key => $value): ?>
                            <option value="<?php echo $value['id']; ?>" ><?php echo $value['_label']; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <script type="text/javascript">
                </script>
                <span id="button-tipo_direccion"></span>
            </div>
        </div>
    </li>
    <li id="li-direcciones_medico-2101">
        <div class="control-group">
            <label class="control-label" for="635">Direcciones del Medico</label>
            <div class="controls">
                <select name="direcciones_medico"
                        id="dirs_med"
                        data-placeholder="Seleccione la Direccion Envio"
                        class="chzn-select"
                        data-rules="integer"
                        data-display="Direcciones de Envio"
                        data-content="Indique la Direccion de Envio"
                        title="Direcciones del Medico"
                        >
                    <option value=""></option>
                    <?php
                    if (isset($direccion_medico) && sizeof($direccion_medico) > 0):
                        foreach ($direccion_medico as $key => $value):
                            $localidad_med = (!empty($value['localidad']) ? $value['localidad'] . ',' : '');
                            $centro_salud_med = (!empty($value['centro_salud']) ? $value['centro_salud'] . ',' : '');
                            $direccion_med = (!empty($value['direccion']) ? $value['direccion'] : '');
                            ?>
                            <option
                                id="<?php echo $value['id']; ?>"
                                value="<?php echo $value['id']; ?>"
                                direccion ="<?php echo "$localidad_med $centro_salud_med $direccion_med"; ?>"
                                class='<?php echo $value['id_medico']; ?>' >Direccion</option>
                                <?php
                            endforeach;
                        endif;
                        ?>
                </select>
                <script type="text/javascript">
                    $(function() {
                        $('#dirs_med').chained('#284');
                        $('#dirs_med').trigger('liszt:updated');
                        $('#284').bind('change', function(){

                            $("#dirs_med option").each(function(){
                                if($(this).attr('value')!=''){
                                    $(this).html('Direccion Nro '+$(this).index());
                                }
                            });
                            $('#dirs_med').trigger('liszt:updated')
                        });
                        $( "#dirs_med" ).change(function() {
                            if($(this).val()!=''){
                                $('#enviar-2101').text($('#'+$(this).val()).attr('direccion'));
                            }
                        });
                    });
                </script>
                <span id="button-direcciones_medico"></span>
            </div>
        </div>
    </li>
    <li id="li-enviar-2101">
        <div class="control-group">
            <label class="control-label" for="enviar-2101">Dirección de Envio<span style='color:red;font-size: 16px'> *</span></label>
            <div class="controls">
                <textarea
                    class="span3"
                    name="enviar"
                    id="enviar-2101"
                    placeholder="Ej: Clinica Piso 2 Quirofano.  Ej: Consultorio del Medico"
                    data-rules=""
                    data-display="Dirección de Envio"
                    data-content="Indique la dirección de envio"
                    title="Dirección de Envio"
                    rows="5"
                    readonly="readonly"
                    ></textarea>
            </div>
        </div>
    </li>

    <li id="li-label_forma_pago-2101">
        <br/>
        <label for="label_forma_pago">
            <strong>FORMA DE PAGO</strong></label>
        <div id="label-label_forma_pago"></div>
    </li>
    <li id="li-cat_pagador-2101">
        <div class="control-group">
            <label class="control-label" for="623">Emisor de Pago<span style='color:red;font-size: 16px'> *</span></label>
            <div class="controls">
                <select name="cat_pagador"
                        id="623"
                        data-placeholder="Seleccione un Emisor de Pago"
                        class="chzn-select"
                        data-rules="required|integer"
                        data-display="Emisor de Pago"
                        data-content="Indique el Emisor de Pago"
                        title="Emisor de Pago"
                        >
                    <option value=""></option>
                    <?php if (isset($emisor_pago) && sizeof($emisor_pago) > 0): ?>
                        <?php foreach ($emisor_pago as $key => $value): ?>
                            <option value="<?php echo $value['id']; ?>" ><?php echo $value['_label']; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <span id="button-cat_pagador"></span>
            </div>
        </div>
    </li>
    <li id="li-id_seguro-2101">
        <div class="control-group">
            <label class="control-label" for="279">Aseguradora</label>
            <div class="controls">
                <select name="id_seguro"
                        id="279"
                        data-placeholder="Seleccione una Aseguradora"
                        class="chzn-select"
                        data-rules="integer|rewrite"
                        data-display="Aseguradora"
                        data-content="Indique la aseguradora"
                        title="Aseguradora"
                        >
                    <option value=""></option>
                    <?php if (isset($aseguradora) && sizeof($aseguradora) > 0): ?>
                        <?php foreach ($aseguradora as $key => $value): ?>
                            <option value="<?php echo $value['id']; ?>" ><?php echo $value['nombre']; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <span id="button-id_seguro"></span>
            </div>
        </div>
    </li>
    <li id="li-cat_letra-2101">
        <div class="control-group">
            <label class="control-label" for="636">Tercero (Persona/Empresa)</label>
            <div class="controls">
                <select name="cat_letra"
                        id="636"
                        data-placeholder="Seleccione el Tipo de Persona"
                        class="chzn-select"
                        data-rules="integer|rewrite"
                        data-display="Tercero (Persona/Empresa)"
                        data-content="Indique el tipo de persona"
                        title="Tercero (Persona/Empresa)"
                        >
                    <option value=""></option>
                    <?php if (isset($tipo_persona) && sizeof($tipo_persona) > 0): ?>
                        <?php foreach ($tipo_persona as $key => $value): ?>
                            <option value="<?php echo $value['id']; ?>" ><?php echo $value['_label']; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <span id="button-cat_letra"></span>
            </div>
        </div>
    </li>
    <li id="li-id_tercero-2101">
        <div class="control-group">
            <label class="control-label" for="628">Persona/Empresa</label>
            <div class="controls">
                <select name="id_tercero"
                        id="628"
                        data-placeholder="Seleccione el Tercero"
                        class="chzn-select"
                        data-rules="integer|rewrite"
                        data-display="Persona/Empresa"
                        data-content="Indique el tercero (Persona o Empresa) que emitirá el pago"
                        title="Persona/Empresa"
                        >
                    <option value=""></option>
                </select>
                <script type="text/javascript">
                </script>
                <span id="button-id_tercero">
                    <a href="#" role="button" class="btn btn-primary" data-toggle="modal"
                       onclick="request({url:BASE_URL +'persona/persona/create', data:{id:'',target_on:'window'}})">
                        Crear Persona            </a>
                </span>
            </div>
        </div>
    </li>
    <li id="li-cat_tipo_pago-2101">
        <div class="control-group">
            <label class="control-label" for="300">Forma de Pago<span style='color:red;font-size: 16px'> *</span></label>
            <div class="controls">
                <select name="cat_tipo_pago"
                        id="300"
                        data-placeholder="Seleccione la forma de pago"
                        class="chzn-select"
                        data-rules="required|integer"
                        data-display="Forma de Pago"
                        data-content="Indique la forma de pago de la cotización"
                        title="Forma de Pago"
                        >

                    <option value=""></option>
                    <?php if (isset($tipo_pago) && sizeof($tipo_pago) > 0): ?>
                        <?php foreach ($tipo_pago as $key => $value): ?>
                            <option value="<?php echo $value['id']; ?>" ><?php echo $value['_label']; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>

                </select>
                <span id="button-cat_tipo_pago"></span>
            </div>
        </div>
    </li>

    <li id="li-label_observacion-2101">
        <br/>
        <label for="label_forma_pago">
            <strong>OBSERVACIONES</strong></label>
        <div id="label-label_observacion"></div>
    </li>
    <li id="li-observacion-2101">
        <div class="control-group">
            <label class="control-label" for="enviar-2101">Observaciones</label>
            <div class="controls">
                <textarea
                    class="span3"
                    name="observacion"
                    id="observacion-2101"
                    placeholder="Ej: Procesar de manera rapida"
                    data-rules=""
                    data-display="Observaciones"
                    data-content="Llene el campo si posee Observaciones"
                    title="Observaciones"
                    rows="5"
                    ></textarea>
            </div>
        </div>
    </li>
    <li><br/>
        <div class="form-actions ">

            <input type="submit" class="btn btn-primary" value="Siguiente" />
        </div>
    </li>
</ul>