<?php
$permissions = $this->session->userdata('permissions');
$role = $this->session->userdata('role_name');
if ($operation != 'detail'):
    ?>
    <br/>
    <button type ="button" id ="btn-cargaMat"class="btn btn-primary " role="button" data-toggle="modal" onclick="show_articles(event,'mat', 'materialTable',null,true,'MAT',2);">
        Agregar Articulos
    </button>
    <div style="float:right;margin-top:-0px">
        <span style="float:left;font-size:7pt;"><b>No Disponible</b>&nbsp;&nbsp;</span>
        <div class="img-circle" style="background-color:red;float:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>

        <span style="float:left;font-size:7pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Bajo</b>&nbsp;&nbsp;</span>
        <div class="img-circle" style="background-color:yellow;float:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>

        <span style="float:left;font-size:7pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Disponible</b>&nbsp;&nbsp;</span>
        <div class="img-circle" style="background-color:blue;float:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
    </div>
    <br/><br/>
<?php endif; ?>

<div style="margin-top: 10px; margin-left: 20px">
    <table id="materialTable" class=" <?php echo ($operation != 'detail' ? '' : ' js-table '); ?>  table table-striped table-bordered">
        <thead>
            <tr>
                <th width="10px"></th>
                <th width="10px"></th>
                <th>Modelo</th>
                <th>Descripción</th>
                <?php
                if ($operation == 'detail'):
                    if (!isset($permissions[2109]) || $role == 'Admin'):
                        ?>
                        <th width="10px">Precio</th>
                        <?php
                    endif;
                else:
                    ?>
                    <th width="10px">Precio</th>
                <?php
                endif;
                ?>
                <th width="10px">Cantidad</th>
                <?php if ($operation != 'detail'): ?>
                    <th width="10px">Stock</th>
                <?php endif; ?>
            </tr>
        </thead>
        <tbody>
            <?php
            $monto = 0;
            if (isset($materiales) and !empty($materiales)):
                //Posicion en array que indica cual es tipo de precio seleccionado.
                $pos_tipo_precio = -1;
                foreach ($materiales as $key => $material):
                    ?>
                    <tr id="tr-mat-<?php echo $material['id']; ?>">
                        <?php if ($operation != 'detail'): ?>
                            <td>
                                <input class="indicador-mat" id="mat-<?= $material['id'] ?>" type="hidden" value="<?php echo $material['id'] ?>" key="<?= $material['id'] ?>" name="mat[<?= $material['id'] ?>]" />
                                <i class="icon-remove" onclick="delete_tr('tr-mat-<?php echo $material['id'] ?>','mat');delete_tr('tr-total-<?php echo $material['id']; ?>');" />
                            </td>
                        <?php else: ?>
                            <td></td>
                        <?php endif; ?>

                        <td><a data-toggle="modal" href="#" onclick="show_img_article('<?= $material['modelo'] ?>',  '<?= $material['descripcion'] ?>', '<?= $material['imagen'] ?>');"><i class="icon-picture"></i></a></td>
                        <td id="model-mat-<?= $material['id'] ?>"><?php echo $material['modelo'] ?></td>
                        <td id="name-mat-<?= $material['id'] ?>"><?php echo $material['descripcion'] ?></td>
                        <?php
                        if ($operation != 'detail'):
                            $readonly = '';
                        else:
                            $readonly = 'readonly="readonly"';
                            $value = "value='{$material['precio']}'";

                        endif;

                        if ($operation == 'detail'):
                            if (!isset($permissions[2109]) || $role == 'Admin'):
                                ?>
                                <td><input type="text" onblur ="calculateTotal();" id="price-mat-<?= $material['id'] ?>" value="<?= number_format($material['precio'], 2, ',', '.') ?>" name="precio_matl[<?= $material['id'] ?>]"<?= $readonly ?> <?= $value ?> ></td>
                                <?php
                            endif;
                        else:
                            ?>
                            <td>
                                <select id="price-mat-<?= $material['id'] ?>" name="precio_mat[<?= $material['id'] ?>]" onchange="calculateTotal();">
                                    <?php
                                    if ($pos_tipo_precio == -1):
                                        $pos_tipo_precio = array_search($tipo_precio, $material['arr_id_precio']);
                                    endif;
                                    ?>


                                    <?php
                                    foreach ($material['arr_precio'] as $key_price => $precio):
                                        $selected = ($key_price == $pos_tipo_precio) ? 'selected="selected"' : '';
                                        ?>
                                        <option value='<?= $precio ?>' <?= $selected ?> ><?= number_format($precio, 2, ',', '.') ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                        <?php
                        endif;

                        if ($operation != 'detail'):
                            $readonly = '';
                            $value = "value='{$material['cantidad']}'";
                        else:
                            $readonly = 'readonly="readonly"';
                            $value = "value='{$material['cantidad']}'";
                            $monto = $material['monto'];
                        endif;
                        ?>
                        <td><input min="1" type="number" onblur="calculateTotal();check_disponibilidad(this.id,'mat','materialTable');" id="cant-mat-<?= $material['id'] ?>" class="input-mini" name="cant_mat[<?= $material['id'] ?>]" placeholder="0" key="<?= $material['id'] ?>" <?= $readonly ?> <?= $value ?> /></td>
                        <?php
                        $leyenda = '';
                        $disponibilidad = (empty($material['disponibilidad']) || $material['disponibilidad'] < 1 ? 0 : $material['disponibilidad']);
                        $arrayLeyenda[1] = 'red';
                        $arrayLeyenda[2] = 'yellow';
                        $arrayLeyenda[3] = 'blue';
                        if ($material['cantidad'] > $disponibilidad):
                            $leyenda = $arrayLeyenda[1];
                        elseif ($material['cantidad'] == $disponibilidad):
                            $leyenda = $arrayLeyenda[2];
                        else:
                            $leyenda = $arrayLeyenda[3];
                        endif;
                        if ($operation != 'detail'):
                            ?>

                            <td >
                                <div class="img-circle" id="leyenda-mat-<?= $material['id'] ?>" style="background-color:<?php echo $leyenda; ?>;">
                                    <input type="hidden" id="disp-mat-<?= $material['id'] ?>" value="<?php echo $disponibilidad; ?>"/>
                                    &nbsp;&nbsp;&nbsp;
                                </div>
                            </td>
                        <?php endif; ?>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
        <tfoot>
            <?php if ($operation != 'detail'): ?>
                <tr>
                    <th colspan="4">Total</th>
                    <th colspan="3"><input type="text" id ="total_price-mat" name="total_price" placeholder="0" readonly="readonly"/></th>
                </tr>
            <?php else: ?>
                <?php if (!isset($permissions[2109]) || $role == 'Admin'): ?>
                    <tr>
                        <th colspan="4">Total</th>
                        <th colspan="2"><input type="text" id ="total_price-mat" name="total_price" readonly="readonly" value="<?php echo number_format($monto, 2, ',', '.') ?>"/></th>
                    </tr>
                    <?php
                endif;
            endif;
            ?>
        </tfoot>
    </table>
    <?php
    if ($operation != 'detail'):
        ?>
        <div class="form-actions">
            <input type="button" onclick="navigate_tab('prev',2);" value="Anterior" class="btn btn-primary">
            <input type="button" onclick="navigate_tab('next',2);" value="Siguiente" class="btn btn-primary">
        </div>
    <?php endif ?>
    <div id="materialModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="materialModalLabel"></h3>
        </div>
        <div class="m-body" >
            <table align="center" >
                <tr>
                    <td><img id='selected-material-image' width="100%" height="100%"/></td>
                    <td >
                        <label><strong>Descripción</strong></label>
                        <span id="selected-material-description"></span>
                    </td>
                </tr>
            </table>
            <br/>
        </div>
    </div>
</div>
<script type="text/javascript">
    //Ejecutamos la funcion para calcular el total de la cotizacion.
<?php if ($operation != 'detail'): ?>
        calculateTotal();
        cant_articulo('materialTable','cant_mat','cant-mat','mat');
<?php endif; ?>
</script>

<?php if ($operation == 'detail'): ?>
    <?php if (isset($articulos_sustituidos) and sizeof($articulos_sustituidos) > 0): ?>
        <div style="margin-top: 10px; margin-left: 20px">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th style="background-color: #F9F9F9;" width="25%"><center>MATERIAL SUSTITUIDO</center></th>
                <th style="background-color: #F9F9F9;"><center>

                    <table  class="table table-striped table-bordered">
                        <tr>
                            <th colspan="5"><center>ARTICULO SUSTITUTO</center></th>
                        </tr>
                        <tr>
                            <th width="20px" ></th>
                            <th width="80px" >Modelo</th>
                            <th>Descripción</th>
                            <th width="80px" >Precio</th>
                            <th width="50px">Cant</th>
                        </tr>
                    </table>
                </center></th> </tr>
                <?php foreach ($articulos_sustituidos as $key => $value): ?>
                    <tr width="25%" ><td ><center><b><?php echo $value['modelo']; ?></b></center></td>
                    <td>
                        <table  class="table table-striped table-bordered">
                            <tbody>
                                <?php foreach ($value['sustituidos'] as $clave => $material): ?>
                                    <tr>

                                        <td width="20px" ><a data-toggle="modal" href="#" onclick="show_img_article('<?= $material['modelo'] ?>',  '<?= $material['descripcion'] ?>', '<?= $material['imagen'] ?>');"><i class="icon-picture"></i></a></td>
                                        <td width="80px" ><?php echo $material['modelo'] ?></td>
                                        <td><?php echo $material['descripcion'] ?></td>
                                        <td width="80px"><?php echo number_format($material['precio'], 2, ',', '.') ?></td>
                                        <td width="50px" ><?php echo $material['cantidad']; ?> </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    <?php endif; ?>
<?php endif; ?>
