<script>
    //Ocultamos los campos por no visibles en todas los medios.
    $('#li-vencimiento-2103, #li-deducible-2103').hide();

    //Obtenemos el valor de tipo de pago.
    var tipo = $("select[name='cat_tipo_pago'] option:selected").text();
    switch(tipo.toLowerCase()){
        case 'carta aval':
            $('#li-cat_banco-2103').hide();
            $('#li-vencimiento-2103, #li-deducible-2103').show();
            break;
        case 'cheque':
            $('#li-vencimiento-2103').show();
            break;
    }
</script>
<?php echo $form ?>