<?php if ($operation != 'detail'): ?>
    <br/>
    <button type ="button" id ="btn-cargabackup"class="btn btn-primary " role="button" data-toggle="modal" onclick="show_articles(event,'backup','backupTable',null,true,'MAT',2);">
        Agregar Articulos
    </button>
    <div style="float:right;margin-top:-0px">
        <span style="float:left;font-size:7pt;"><b>Cero</b>&nbsp;&nbsp;</span>
        <div class="img-circle" style="background-color:red;float:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>

        <span style="float:left;font-size:7pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Bajo</b>&nbsp;&nbsp;</span>
        <div class="img-circle" style="background-color:yellow;float:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>

        <span style="float:left;font-size:7pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Disponible</b>&nbsp;&nbsp;</span>
        <div class="img-circle" style="background-color:blue;float:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
    </div>
    <br/><br/>
<?php endif; ?>

<div id="carga_materiales"></div>
<div style="margin-top: 10px; margin-left: 20px">

    <table id="backupTable" class=" <?php echo ($operation != 'detail' ? '' : ' js-table ') ?> table table-striped table-bordered">
        <thead>
            <tr>
                <th width="10px"></th>
                <th width="10px"></th>
                <th>Modelo</th>
                <th>Descripción</th>
                <th width="10px">Precio</th>
                <th width="10px">Cantidad</th>

                <?php if ($operation != 'detail'): ?>
                    <th width="10px">Stock</th>
                <?php endif; ?>
            </tr>
        </thead>
        <tbody>
            <?php
            if (isset($backup) and !empty($backup)):
                //Posicion en array que indica cual es tipo de precio seleccionado.
                $pos_tipo_precio = -1;

                foreach ($backup as $key => $material):
                    $value_mat = "value='{$material['id']}'";
                    ?>

                    <tr id="tr-backup-<?php echo $material['id'] ?>">
                        <?php if ($operation != 'detail'): ?>

                            <td id="codigo-0">
                                <input class="indicador-backup" id="mat-backup-<?php echo $material['id'] ?>" type="hidden" <?php echo $value_mat; ?> key="<?php echo $material['id']; ?>" name="backup[<?php echo $material['id'] ?>]" />
                                <i class="icon-remove" onclick="delete_tr('tr-backup-<?php echo $material['id'] ?>')"/>
                            </td>
                        <?php else: ?>
                            <td></td>
                        <?php endif; ?>
                        <td><a data-toggle="modal" href="#" onclick="show_img_article('<?php echo $material['modelo']; ?>',  '<?php echo $material['descripcion']; ?>', '<?php echo $material['imagen']; ?>');"><i class="icon-picture"></i></a></td>
                        <td id="model-backup-<?= $material['id'] ?>"><?php echo $material['modelo']; ?></td>
                        <td id="name-backup-<?= $material['id'] ?>"><?php echo $material['descripcion']; ?></td>
                        <td>
                            <select id="price-backup-<?php echo $material['id'] ?>" name="precio_backup[<?php echo $material['id'] ?>]" onchange="calculateTotalBackup();">
                                <?php
                                if ($pos_tipo_precio == -1):
                                    $pos_tipo_precio = array_search($tipo_precio, $material['arr_id_precio']);
                                endif;

                                foreach ($material['arr_precio'] as $key_price => $precio):
                                    $selected = ($key_price == $pos_tipo_precio) ? 'selected="selected"' : '';
                                    ?>
                                    <option value='<?= $precio ?>' <?= $selected ?> ><?= number_format($precio, 2, ',', '.') ?></option>
                                    <?php
                                endforeach;
                                $value_cant = "value='{$material['cantidad']}'";
                                ?>
                            </select>
                        </td>
                        <td><input min="1" type="number" onblur="calculateTotalBackup();check_disponibilidad(this.id,'backup','backupTable');" id="cant-backup-<?php echo $material['id']; ?>" class="input-mini" name="cant_backup[<?php echo $material['id']; ?>]"  placeholder="0" key="<?php echo $material['id']; ?>" <?php echo $value_cant; ?>  /></td>
                        <?php
                        $leyenda = '';
                        $disponibilidad = (empty($material['disponibilidad']) || $material['disponibilidad'] < 1 ? 0 : $material['disponibilidad']);
                        $arrayLeyenda[1] = 'red';
                        $arrayLeyenda[2] = 'yellow';
                        $arrayLeyenda[3] = 'blue';
                        if ($material['cantidad'] > $disponibilidad):
                            $leyenda = $arrayLeyenda[1];
                        elseif ($material['cantidad'] == $disponibilidad):
                            $leyenda = $arrayLeyenda[2];
                        else:
                            $leyenda = $arrayLeyenda[3];
                        endif;
                        if ($operation != 'detail'):
                            ?>

                            <td >
                                <div class="img-circle" id="leyenda-backup-<?= $material['id'] ?>" style="background-color:<?php echo $leyenda; ?>;">
                                    <input type="hidden" id="disp-backup-<?= $material['id'] ?>" value="<?php echo $disponibilidad; ?>"/>
                                    &nbsp;&nbsp;&nbsp;
                                </div>
                            </td>
                        <?php endif; ?>

                    </tr>
                    <?php
                endforeach;
            endif;
            ?>
        </tbody>
        <tfoot>
            <?php if ($operation != 'detail'): ?>
                <tr>
                    <th colspan="4">Total</th>
                    <th colspan="3"><input type="text" id ="total_price-backup" name="total_price-backup" placeholder="0" readonly="readonly"/></th>
                </tr>
            <?php else: ?>
                <?php if (!isset($permissions[2109]) || $role == 'Admin'): ?>
                    <tr>
                        <th colspan="4">Total</th>
                        <th colspan="2"><input type="text" id ="total_price-backup" name="total_price-backup" readonly="readonly" value="<?php echo number_format($monto, 2, ',', '.') ?>"/></th>
                    </tr>
                    <?php
                endif;
            endif;
            ?>
        </tfoot>
    </table>
    <?php if ($operation != 'detail'): ?>
        <div class="form-actions">
            <input type="button" onclick="navigate_tab('prev',3);" value="Anterior" class="btn btn-primary">
            <input type="button" onclick="navigate_tab('next',3);" value="Siguiente" class="btn btn-primary">
        </div>
    <?php endif ?>
</div>
<script type="text/javascript">
    calculateTotalBackup();
    cant_articulo('backupTable','cant_materialbackup','cant-backup','backup');
</script>