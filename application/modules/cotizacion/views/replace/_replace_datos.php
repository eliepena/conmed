<div class="modal-body">
    <ul>

        <li >
            <br>
            <label >
                <strong>DATOS DE COTIZACION</strong></label>
            <div ></div>
        </li>

        <li >




            <div class="control-group">
                <label class="control-label" >Nro Cotizacion</label>
                <div class="controls">
                    <input type="text"
                           class="input-xlarge"
                           placeholder=""
                           data-rules=""
                           data-display="Cliente"
                           data-content=""
                           title="Cliente"
                           readonly="readonly"
                           value="<?php echo $datos['codigo']; ?>"
                           />

                    <span id="button-codigo"></span>
                </div></div>


        </li>
        <li>
            <div class="control-group">
                <label class="control-label"  >Paciente</label>
                <div class="controls">
                    <input type="text"
                           class="input-xlarge"
                           placeholder=""
                           data-rules=""
                           data-display="Paciente"
                           value="<?php echo $datos['id_paciente']; ?>"
                           data-content=""
                           title="Paciente"
                           readonly="readonly"
                           />
                </div>
            </div>
        </li>
        <li >
            <div class="control-group">
                <label class="control-label"  >Medico Tratante</label>
                <div class="controls">
                    <input type="text"
                           class="input-xlarge"
                           placeholder=""
                           data-rules=""
                           data-display="Medico Tratante"
                           data-content=""
                           title="Medico Tratante"
                           readonly="readonly"
                           value="<?php echo $datos['id_medico']; ?>"
                           />
                </div>
            </div>
        </li>
        <li >
            <div class="control-group">
                <label class="control-label"  >Vendedor</label>
                <div class="controls">
                    <input type="text"
                           class="input-xlarge"
                           placeholder=""
                           data-rules=""
                           data-display="Vendedor"
                           data-content=""
                           title="Vendedor"
                           readonly="readonly"
                           value="<?php echo $datos['id_vendedor']; ?>"
                           />
                </div>
            </div>
        </li>
        <li >
            <div class="control-group">
                <label class="control-label">Tipo de Pago</label>
                <div class="controls">
                    <input type="text"
                           class="input-xlarge"
                           placeholder=""
                           data-rules=""
                           data-display="Tipo de Pago"
                           data-content=""
                           title="Tipo de pago"
                           readonly="readonly"
                           value="<?php echo $datos['tipo_pago']; ?>"
                           />
                </div>
            </div>
        </li>
        <li >
            <div class="control-group">
                <label class="control-label">Articulacion</label>
                <div class="controls">
                    <input type="text"
                           class="input-xlarge"
                           placeholder=""
                           data-rules=""
                           data-display="Articulacion"
                           data-content=""
                           title="Articulacion"
                           readonly="readonly"
                           value="<?php echo $datos['id_articulacion']; ?>"
                           />
                </div>
            </div>
        </li>
        <li >
            <div class="control-group">
                <label class="control-label">Lesion</label>
                <div class="controls">
                    <input type="text"
                           class="input-xlarge"
                           placeholder=""
                           data-rules=""
                           data-display="Lesion"
                           data-content=""
                           title="Lesion"
                           readonly="readonly"
                           value="<?php echo $datos['id_lesion']; ?>"
                           />
                </div>
            </div>
        </li>
        <li >
            <div class="control-group">
                <label class="control-label">Tecnica</label>
                <div class="controls">
                    <input type="text"
                           class="input-xlarge"
                           placeholder=""
                           data-rules=""
                           data-display="Tecnica"
                           data-content=""
                           title="Tecnica"
                           readonly="readonly"
                           value="<?php echo $datos['id_tecnica']; ?>"
                           />
                </div>
            </div>
        </li>
    </ul>
</div>


<div class="modal-footer">
     <input type="button" onclick="navigate_tab_string('1-0','1-1');" value="Siguiente" class="btn btn-primary">
</div>
