<div style="margin-top: 10px; margin-left: 20px">
    <table id="instrumentoTableReplace" class=" table table-striped table-bordered">
        <thead>
            <tr>
                <th width="10px"></th>
                <th width="10px"></th>
                <th>Modelo</th>
                <th>Descripción</th>
                <th width="10px">Cantidad</th>
                <th>Disponible?</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (isset($instrumentos) and !empty($instrumentos)):
                foreach ($instrumentos as $indice => $valor):
                    if ($valor['checked'] == 'checked'):
                        unset($instrumentos[$indice]);
                    endif;
                endforeach;
                ?>
                <?php foreach ($instrumentos as $key => $instrumento): ?>
                    <tr id="tr-instru-<?php echo $instrumento['id']; ?>Replace" >
                        <td>
                            <input class="indicador-instru" id="instru-<?php echo $instrumento['id']; ?>Replace" type="hidden" value="<?php echo $instrumento['id']; ?>" key="<?php echo $instrumento['id']; ?>" name="instru[<?php echo $instrumento['id']; ?>]" />
                            <i class="icon-plus" onclick="show_add('ins-<?= $instrumento['id'] ?>-Replace')" />
                        </td>

                        <td><a data-toggle="modal" href="#" onclick="show_img_article('<?php echo $instrumento['modelo'] ?>',  '<?php echo $instrumento['descripcion'] ?>', '<?php echo $instrumento['imagen'] ?>');"><i class="icon-picture"></i></a></td>
                        <td id="model-instru-<?= $instrumento['id']; ?>Replace" ><?php echo $instrumento['modelo']; ?></td>
                        <td id="name-instru-<?= $instrumento['id']; ?>Replace" ><?php echo $instrumento['descripcion']; ?></td>

                        <td><?php echo $instrumento['cantidad']; ?></td>

                        <td>
                <center>
                    <input type="checkbox" onclick="return false;" <?php echo $instrumento['checked']; ?> class="style3 hide" value="<?php echo $instrumento['id']; ?>" name="instru[<?php echo $instrumento['id']; ?>]" />
                </center>
                <button type ="button" id ="btn-cargaIns"class="btn btn-primary " role="button" data-toggle="modal" onclick="show_articles(event,'ins', 'instrumentoTableReplace',<?= $instrumento['id']; ?>);">
                    Sustituir
                </button>
                </td>
                </tr>
                <tr>
                    <td style="background-color:#C2DDB6"   colspan="6" class=" ins-<?= $instrumento['id']; ?>-Replace hide sustituto">
                        <table id="ins-<?php echo $instrumento['id']; ?>" class=" table table-striped table-bordered">

                            <thead>
                                <tr>
                                    <th width="10px"></th>
                                    <th width="10px"></th>
                                    <th>Modelo</th>
                                    <th>Descripción</th>
                                    <th width="10px">Cantidad</th>
                                </tr>
                            </thead>
                        </table>

                    </td></tr>
            <?php endforeach; ?>
        <?php endif; ?>
        </tbody>
    </table>
    <div class="modal-footer">
        <input type="button" onclick="navigate_tab_string('2-1','1-1');" value="Anterior" class="btn btn-primary">
        <input type="button" onclick="navigate_tab_string('2-1','3-1');" value="Siguiente" class="btn btn-primary">
    </div>
</div>
<script type="text/javascript">

    $('input[type="checkbox"].style3').checkbox({
        buttonStyle: 'btn-danger',
        buttonStyleChecked: 'btn-success',
        checkedClass: 'icon-check',
        uncheckedClass: 'icon-check-empty'
    });

</script>