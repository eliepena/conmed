<div style="margin-top: 10px; margin-left: 20px">
    <table id="materialBackupTableReplace" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th ></th>
                <th width="10px"></th>
                <th>Modelo</th>
                <th>Descripción</th>
                <th width="10px">Precio</th>
                <th width="4px">Cant</th>
                <th width="16px">Disponible?</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $monto = 0;

            if (isset($backup) and !empty($backup)):
                foreach ($backup as $indice => $valor):
                    if ($valor['checked'] == 'checked'):
                        unset($backup[$indice]);
                    endif;
                endforeach;
                ?>
                <?php foreach ($backup as $key => $material): ?>
                    <tr>
                        <td>
                            <input class="indicador-backup" id="mat-backup-<?php echo $material['id'] ?>Replace" type="hidden"  key="<?php echo $material['id']; ?>" name="backup[<?php echo $material['id'] ?>]" />
                            <i class="icon-plus" onclick="show_add('backup-<?= $material['id'] ?>-Replace')" />
                        </td>
                        <td><a data-toggle="modal" href="#" onclick="show_img_article('<?php echo $material['modelo']; ?>',  '<?php echo $material['descripcion']; ?>', '<?php echo $material['imagen']; ?>');"><i class="icon-picture"></i></a></td>
                        <td><?php echo $material['modelo']; ?></td>
                        <td><?php echo $material['descripcion']; ?></td>
                        <td><?php echo $material['precio']; ?></td>
                        <td><?php echo $material['cantidad']; ?></td>



                        <td>
                <center>
                    <input type="checkbox" onclick="return false;" <?php echo $material['checked']; ?>  class="style3 hide" value="<?php echo $material['id'] ?>" name="backup_check[<?php echo $material['id'] ?>]"/>

                    <button type ="button" id ="btn-cargaIns"class="btn btn-primary " role="button" data-toggle="modal" onclick="show_articles(event,'backup', 'materialBackupTableReplace',<?= $material['id']; ?>);">
                        Sustituir
                    </button>
                </center>
                </td>
                </tr>
                <tr>
                    <td style="background-color:#C2DDB6"   colspan="7" class="backup-<?= $material['id']; ?>-Replace hide sustituto">
                        <table id="backup-<?php echo $material['id']; ?>" class=" table table-striped table-bordered">

                            <thead>
                                <tr>
                                    <th width="10px"></th>
                                    <th width="10px"></th>
                                    <th>Modelo</th>
                                    <th>Descripción</th>
                                    <th width="10px">Cantidad</th>
                                </tr>
                            </thead>
                        </table>

                    </td></tr>
                <?php
            endforeach;

        endif;
        ?>
        </tbody>
        <tfoot>

        </tfoot>
    </table>
<!--    <div class="modal-footer">
        <input type="button" onclick="navigate_tab_string('3-1','2-1');" value="Anterior" class="btn btn-primary">
        <input type="button" onclick="navigate_tab_string('3-1','4-1');" value="Siguiente" class="btn btn-primary">
    </div>-->
</div>

<script type="text/javascript">

    $('input[type="checkbox"].style3').checkbox({
        buttonStyle: 'btn-danger',
        buttonStyleChecked: 'btn-success',
        checkedClass: 'icon-check',
        uncheckedClass: 'icon-check-empty'
    });


    function show_add(id){

        if ($("."+id).is (':visible')){

            $("."+id).hide();
        }else{
            $(".sustituto").hide();
            $("."+id).show();
        }


    }



    function save_articles_replace(){
        var formData = $('#_form_ra').serializeArray();
        $('body').modalmanager('loading');
        //Dispara el evento
        $.ajax({
            url: BASE_URL +'cotizacion/cotizacion/replaceArticle/process/',
            data: formData,

            success: function(response){
                $('body').modalmanager('loading');

                var textAlert = null, alertHtml = null;

                //Se procesa cuando el registro se guarda exitosamente
                if(response.view == 1){
                    $("#_form_ra").hide();
                    textAlert = 'Datos guardados satisfactoriamente';
                    alertHtml = [
                        '<div class="alert alert-success fade in">',
                        '<button type="button" class="close" data-dismiss="alert">×</button>',
                        textAlert,
                        '</div>'
                    ].join('');
//aqui se verifica si viene del content cotizacion/detail
// viene de notificacion modal
<?php if ($uri_content): ?>

                        var reload_content = {
                            type: "POST",
                            url: '<?php echo base_url(); ?>cotizacion/cotizacion/detail',
                            data: {id:'<?php echo $id_ec; ?>'},
                            success:success
                        };
                        $.ajax(reload_content);
<?php endif; ?>

                    setTimeout(function(){
                        $('#modal_window').modal('hide');
                    },1500);

                }

                //Se procesa cuando no se puede guardar el registro
                else{
                    //Ocurre cuando hay un error en Base de Datos
                    if(response.view == false)
                        textAlert = 'No se pudo guardar el registro, si el problema persiste comuniquese con sistemas';

                    //Ocurre cuando hay errores de validacion en el Servidor
                    else
                        textAlert = response.view;

                    //Mensaje de Alerta Error
                    alertHtml = [
                        '<div class="alert alert-error fade in">',
                        '<button type="button" class="close" data-dismiss="alert">×</button>',
                        textAlert,
                        '</div>'
                    ].join('');

                }
                //Elemento que muestra el Alert
                $("#alert_content_replace").html(alertHtml);

            }
        });
    }



    //
    //        function checkboxs_disponibilidad(id_table,tipo_articulo,disponible) {
    //            var articulos = new Array();
    //            var iBase = '#' + id_table;
    //            var iTBody = iBase + ' tbody ';
    //            var count=0;
    //            var add_conten='';
    //
    //            $(iTBody +' input[type=checkbox]').each(function(){
    //
    //                var _this = $(this);
    //                if(disponible==1){
    //                    if(_this.is(':checked')){
    //                        articulos.push(_this.val());
    //                        count=count+1;
    //
    //                    }
    //                }else{
    //                    if(!_this.is(':checked')){
    //                        articulos.push(_this.val());
    //                        count=count+1;
    //                        add_conten='not';
    //
    //                    }
    //                }
    //
    //            });
    //
    //            var sub_array="{'articulos':["+articulos.toString()+"]}";
    //            $('#'+tipo_articulo+'_resumen_'+add_conten).attr('onclick','articulos_resumen('+sub_array+')');
    //            $('#'+tipo_articulo+'_resumen_'+add_conten+' span').html(count);
    //        }
    //        function articulos_resumen(params){
    //            alert(params.articulos[0])
    //        }
    //
    //        /**
    //         * Define el modo de impresion del documento.
    //         *
    //         * @param  {string} id_cotizacion   Identificador de la Cotizacion
    //         * @param  {string} tipo_documento  Datos para la busqueda de datos
    //         * @param  {boolean} mode_preview   Identifica si la impresion en preview o no.
    //         * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
    //         * @version V- 1.0  05/09/13 02:52 PM
    //         */
    //        function print_mode_orden(id_cotizacion,tipo_documento,mode_preview){
    //            var membrete = false;
    //            bootbox.dialog('Impresion del documento '+tipo_documento,[{
    //                    'label': "Sin Membrete!",
    //                    'class': "btn-danger",
    //                    'callback': function() {
    //                        membrete = false;
    //                        if(mode_preview){
    //                            preview_pdf_orden(membrete);
    //                        }else{
    //                            cotizacionDocumento_orden(id_cotizacion,tipo_documento,membrete);
    //                        }
    //                    }
    //                },{
    //
    //                    'label': "Con Membrete",
    //                    'class': "btn-primary",
    //                    'callback': function() {
    //                        membrete = true;
    //                        if(mode_preview){
    //                            preview_pdf_orden(membrete);
    //                        }else{
    //                            cotizacionDocumento_orden(id_cotizacion,tipo_documento,membrete);
    //                        }
    //                    }
    //                }]);
    //        }
    //
    //        /**
    //         * Obtiene el reporte de la cotizacion (PDF)  de acuerdo al tipo de documento (backup, instrumental, cotizacion)
    //         *
    //         * @param  {string} id_cotizacion  Identificador de la Cotizacion
    //         * @param  {string} tipo_documento  Datos para la busqueda de datos
    //         * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
    //         * @version V- 1.0  26/08/13 03:55 PM
    //         */
    //        function cotizacionDocumento_orden(id_cotizacion,tipo_documento,membrete){
    //
    //            $('body').modalmanager('loading');
    //            //Dispara el evento
    //            $.ajax({
    //                url: BASE_URL +'delivery/delivery/EXC_quoteReport/',
    //                data: {
    //                    id:id_cotizacion,
    //                    tipo:tipo_documento,
    //                    membrete:membrete
    //                },
    //                success: function(response){
    //                    $('body').modalmanager('loading');
    //                    if(response.sucess){
    //                        pop_pup = window.open(BASE_URL +"delivery/delivery/EXC_force_download?url="+response.file+"&name="+tipo_documento,'Pdf'+tipo_documento, 'width=1,height=1,scrollbars=NO');
    //                        if(!pop_pup){
    //                            bootbox.alert('Disculpe! Verifique que las ventanas emergentes de su navegador no esten bloqueadas.');
    //                        }else{
    //                            setInterval(function() {
    //                                pop_pup.close();
    //                            }, 9000);
    //                        }
    //                    }else{
    //                        var textAlert = 'No se pudo generar el documento, si el problema persiste comuniquese con sistemas';
    //                        bootbox.alert(textAlert);
    //                    }
    //                }
    //            });
    //        }
    //
    //        /**
    //         * Obtiene el pdf Preview de la cotizacion
    //         *
    //         * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
    //         * @version V- 1.0  29/08/13 04:51 PM
    //         */
    //        function preview_pdf_orden(membrete){
    //            var formData = $('#_form').serializeArray();
    //            formData.push({
    //                name:"membrete" ,
    //                value:membrete
    //            });
    //            $('body').modalmanager('loading');
    //            //Dispara el evento
    //            $.ajax({
    //                url: BASE_URL +'cotizacion/cotizacion/create/process/preview',
    //                data: formData,
    //
    //                success: function(response){
    //                    $('body').modalmanager('loading');
    //
    //                    //Se procesa cuando se ejecuta exitosamente el controlador
    //                    if(response.view == 1){
    //                        pop_pup = window.open(BASE_URL +"cotizacion/cotizacion/EXC_force_download?url="+response.file+"&name=Pdf_Preview",'PdfPreview', 'width=1,height=1,scrollbars=NO');
    //                        if(!pop_pup){
    //                            bootbox.alert('Disculpe! Verifique que las ventanas emergentes de su navegador no esten bloqueadas.');
    //                        }else{
    //                            setInterval(function() {
    //                                pop_pup.close();
    //                            }, 9000);
    //                        }
    //
    //                    }
    //                    //Se procesa cuando no se ejecuta exitosamente el controlador
    //                    else{
    //                        //Muestra nuevamente el TAB inicial
    //                        $("li:has(a[href=#2]),#2").addClass("active");
    //                        $("li:has(a[href=#5]),#5").removeClass("active");
    //
    //                        var textAlert = null;
    //
    //                        //Ocurre cuando hay un error en Base de Datos
    //                        if(response.view == false)
    //                            textAlert = 'No se pudo generar la vista previa, si el problema persiste comuniquese con sistemas';
    //
    //                        //Ocurre cuando hay errores de validacion en el Servidor
    //                        else
    //                            textAlert = response.view;
    //
    //                        //Mensaje de Alerta Error
    //                        var alertHtml = [
    //                            '<div class="alert alert-error fade in">',
    //                            '<button type="button" class="close" data-dismiss="alert">×</button>',
    //                            textAlert,
    //                            '</div>'
    //                        ].join('');
    //
    //                        //Elemento que muestra el Alert
    //                        $("#alert_content").html(alertHtml);
    //                    }
    //                }
    //            });
    //        }
</script>




<div class="modal-footer">
    <!--<input type="button" onclick="navigate_tab_string('4-1','3-1');" value="Anterior" class="btn btn-primary">-->
    <input type="button" onclick="navigate_tab_string('3-1','2-1');" value="Anterior" class="btn btn-primary">
    <input type="button" onclick="save_articles_replace()" value="Guardar" class="btn btn-primary">
</div>