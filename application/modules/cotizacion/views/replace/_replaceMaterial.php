<div style="margin-top: 10px; margin-left: 20px">
    <table id="materialTableReplace" class=" <?php echo ($operation == 'detail' ? '' : ' js-table '); ?>  table table-striped table-bordered">
        <thead>
            <tr>
                <th width="10px"></th>
                <th width="10px"></th>
                <th>Modelo</th>
                <th>Descripción</th>
                <th width="10px">Precio</th>
                <th width="10px">Cantidad</th>
                <th>Disponible?</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (isset($materiales) and !empty($materiales)):
                //Posicion en array que indica cual es tipo de precio seleccionado.
                $pos_tipo_precio = -1;
                foreach ($materiales as $indice => $valor):
                    if ($valor['checked'] == 'checked'):
                        unset($materiales[$indice]);
                    endif;
                endforeach;
                foreach ($materiales as $key => $material):

                    if ($material['checked'] == 'checked'):
                        unset($materiales[$key]);
                    endif;
                    ?>
                    <tr id="tr-mat-<?php echo $material['id']; ?>-Replace">

                        <td>
                            <input class="indicador-mat" id="mat-<?= $material['id'] ?>Replace" type="hidden" value="<?php echo $material['id'] ?>" key="<?= $material['id'] ?>" name="mat[<?= $material['id'] ?>]" />
                            <i class="icon-plus" onclick="show_add('mat-<?= $material['id'] ?>-Replace')" />
                        </td>


                        <td><a data-toggle="modal" href="#" onclick="show_img_article('<?= $material['modelo'] ?>',  '<?= $material['descripcion'] ?>', '<?= $material['imagen'] ?>');"><i class="icon-picture"></i></a></td>
                        <td id="model-mat-<?= $material['id'] ?>Replace"><?php echo $material['modelo'] ?></td>
                        <td id="name-mat-<?= $material['id'] ?>Replace"><?php echo $material['descripcion'] ?></td>

                        <td><?= number_format($material['precio'], 2, ',', '.') ?></td>
                        <td><?= $material['cantidad'] ?></td>
                        <td>
                <center>
                    <input type="checkbox"  onclick="return false;" <?php echo $material['checked']; ?> class="style3 hide" value="<?php echo $material['id'] ?>" name="mat[<?= $material['id'] ?>]"/>
                    <button type ="button" id ="btn-cargaMat"class="btn btn-primary " role="button" data-toggle="modal" onclick="show_articles(event,'mat', 'materialTableReplace',<?= $material['id'] ?>);">
                        Sustituir
                    </button>
                </center>
                </td>
                </tr>
                <tr>
                    <td style="background-color:#C2DDB6"   colspan="7" class="mat-<?= $material['id'] ?>-Replace hide sustituto">
                        <table id="mat-<?php echo $material['id'] ?>" class=" table table-striped table-bordered">

                            <thead>
                                <tr>
                                    <th width="10px"></th>
                                    <th width="10px"></th>
                                    <th>Modelo</th>
                                    <th>Descripción</th>
                                    <th width="10px">Precio</th>
                                    <th width="10px">Cantidad</th>
                                </tr>
                            </thead>
                        </table>

                    </td></tr>
            <?php endforeach; ?>
        <?php endif; ?>
        </tbody>
        <tfoot>

        </tfoot>
    </table>

    <div class="modal-footer">
        <input type="button" onclick="navigate_tab_string('1-1','1-0');" value="Anterior" class="btn btn-primary">
        <input type="button" onclick="navigate_tab_string('1-1','2-1');" value="Siguiente" class="btn btn-primary">
    </div>


</div>
<script>



</script>
