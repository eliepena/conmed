<div style="margin-top: 10px; margin-left: 20px">
    <table  id="totalesTable" class="table table-striped table-bordered">
        <thead>
            <tr id="enc">
                <th >C&oacute;digo</th>
                <th>Modelo</th>
                <th>Descripción</th>
                <th width="10px">Gart.</th>
                <th width="10px">Cant.</th>
                <th>Precio Bs.F.</th>
                <th>Neto Bs.F</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot border="0">
            <tr>
                <th colspan="4"></th>
                <td colspan="2">Monto exento</td>
                <td colspan="1">
                    <input
                    <?php
                    if ($this->config->item('desc_rec') == FALSE) {
                        echo 'readonly="readonly"';
                    }
                    ?> type="text" name="monto_exento" size="2" id="monto-exc" onchange="neto_total()"class="input-mini" value="0.00"/></td>
            </tr>
            <tr>
                <th colspan="4"></th>
                <td colspan="2">Monto gravable</td>
                <td colspan="1" ><input type="text" id="monto_gravable" name="monto_gravable" value="0.00"class="input-mini" readonly="readonly" placeholder="0.00"></td>
            </tr>
            <tr>
                <th colspan="4"></th>
                <td colspan="1" width="10px"><input <?php
                        if ($this->config->item('desc_rec') == FALSE) {
                            echo 'readonly="readonly"';
                        }
                    ?> type="text" width="10px" size="2" id="porc-desc"name="descuento" onchange="neto_total()"class="input-mini" value="0.00" /></td>
                <td colspan="1">% de Descuento</td>
                <td colspan="1" id="desc">0.00</td>
            </tr>
            <tr>
                <th colspan="4"></th>
                <td colspan="1"><input <?php
                        if ($this->config->item('desc_rec') == FALSE) {
                            echo 'readonly="readonly"';
                        }
                    ?> type="text" size="2" id="porc-rec"name="recargo" onchange="neto_total()"class="input-mini" value="0.00" /></td>
                <td colspan="1">% de Recargo</td>
                <td colspan="1" id="rec">0.00</td>
            </tr>
            <tr>
                <th colspan="4"></th>
                <td colspan="2">IVA <?= ($this->config->item('valor_iva')) ?>  %</td>
                <td colspan="1" id="iva">0.00</td>
            </tr>
            <tr>
                <th colspan="4"></th>
                <th colspan="2">Total neto Bs.F:</th>
                <td colspan="1"><input type="text" id="netototal" name="monto_total" class="input-mini" readonly="readonly" placeholder="0.00"/></td>
            </tr>

        </tfoot>
    </table>
    <div class="form-actions">
        <input type="button" onclick="navigate_tab('prev',5);" value="Anterior" class="btn btn-primary">
        <input type="button" onclick="print_mode(null,'Vista Previa',true)" value="PreVisualizar" class="btn btn-primary">
        <input type="button" onclick="save_backup()" value="Guardar" class="btn btn-primary">
    </div>
</div>
<style>
    .icon-remove:hover{
        background-color: rgba(255,0,0,0.25);
    }
</style>