<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Instrumento_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad instrumento
 * y sus entidades dependientes o extendidas
 *
 * @package Modules
 * @category Model
 * @author Nohemi Rojas. <nohemir@gmail.com>
 * @version V-1.0 03/04/13 03:06 PM
 */
class Instrumento_model extends AST_Model {

    private $table;

    public function __construct() {
        $this->setSchema('business');
        parent::__construct();
        $this->table = 'business.instrumento';
    }

    /**
     * Lee el directorio que contiene las imagenes de instrumentos
     * y actualiza las imagenes de los registros de la entidad instrumentos
     *
     * @author  Nohemi Rojas
     * @version 1.0 08/04/13 10:48 AM
     */
    public function updateImageByDir() {

        $this->db->select('modelo , id');
        $query = $this->db->get($this->table);
        $results = $query->result_array();
        $path = realpath('uploads') . '/images/instrumentos/';

        foreach ($results as $result) {
            $imagen = NULL;
            //Diferencia entre minusculas-mayusculas / jpg-png
            if (file_exists($path . strtolower($result['modelo']) . '.jpg'))
                $imagen = strtolower($result['modelo']) . '.jpg';
            elseif (file_exists($path . strtolower($result['modelo']) . '.png'))
                $imagen = strtolower($result['modelo']) . '.png';
            elseif (file_exists($path . strtoupper($result['modelo']) . '.jpg'))
                $imagen = strtoupper($result['modelo']) . '.jpg';
            elseif (file_exists($path . strtoupper($result['modelo']) . '.png'))
                $imagen = strtoupper($result['modelo']) . '.png';
            if ($imagen)
                $this->update($result['id'], array('imagen' => $imagen), 'id');
        }
    }

    /**
     * Obtiene la lista de instrumentos que estan relacionados con una cirugia
     *
     * @param integer  $cirugia_id Identificador del tipo de cirugia
     * @return  mixed Array de los valores de los registros, false en caso de que no encuentre
     * @author  Nohemi Rojas
     * @version 03/12/12 12:50 PM
     */
    public function getAll($cirugia_id) {

        if (empty($cirugia_id))
            return FALSE;

        $query = $this->db->query("
            SELECT *  FROM (
                    SELECT 1 as checked, ins.id, ins.modelo, ins.descripcion,ins.imagen,rec.cantidad
                    FROM business.instrumento_recomendacion rec
                    JOIN business.instrumento ins ON ins.id = rec.id_instrumento
                    JOIN business.cirugia tipo_cir ON tipo_cir.id = rec.id_cirugia
                    WHERE tipo_cir.id = $cirugia_id

                   /* UNION

                    SELECT 0 as checked, ins.id, ins.modelo, ins.descripcion,ins.imagen,0 as cantidad
                    FROM business.instrumento AS ins
                    WHERE id NOT IN (SELECT ins.id
                                    FROM business.instrumento_recomendacion rec
                                    JOIN business.instrumento inst ON ins.id = rec.id_instrumento
                                    JOIN business.cirugia tipo_cir ON tipo_cir.id = rec.id_cirugia
                                    WHERE tipo_cir.id = $cirugia_id ) */
                                    )
            AS instrumentos ORDER BY checked DESC, modelo");
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

}
