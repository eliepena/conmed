<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Cotizacion_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad cotizacion
 * y sus entidades dependientes o extendidas
 *
 * @package Modules
 * @category Model
 * @author Nohemi Rojas. <nohemir@gmail.com>
 * @version V-1.0 29/11/12 01:26 PM
 */
class Cotizacion_model extends AST_Model {

    private $table;

    public function __construct() {
        $this->setSchema('business');
        parent::__construct();
        $this->table = 'business.cotizacion';
    }

    /**
     * Provee las implementacion para realizar inserciones dentro cotizaciones
     * y las entidades dependientes de ella
     *
     * @access  public
     * @param   Array  $data datos a ser guardados dentro de la base de datos.
     * @return  Boolean Indicando el resultado de la operacion.
     * @author  Nohemi Rojas
     * @version V-1.0 30/11/2012 09:42
     */
    function insert($data) {
        unset($data['cotizacion']['codigo']);
        $this->db->trans_start();
        $get_data_cotizacion = $this->insertWithReturning($data['cotizacion'], $this->table);
        $id_cotizacion = $get_data_cotizacion['id'];
        $codigo_cotizacion = $get_data_cotizacion['codigo'];

        //Almacena en business.cotizacion_cirugia
        $data['cotizacion_cirugia']['id_cotizacion'] = $id_cotizacion;
        $this->db->insert('business.cotizacion_cirugia', $data['cotizacion_cirugia']);

        //Almacena en business.cotizacion_pago
        $data['cotizacion_pago']['id_cotizacion'] = $id_cotizacion;
        $this->db->insert('business.cotizacion_pago', $data['cotizacion_pago']);

        //Almacena  en cotizacion_articulo con respecto a materiales
        $cotizacion_material = $data['cotizacion_material'];
        if ($cotizacion_material) {
            foreach ($cotizacion_material as $mat => $material)
                $cotizacion_material[$mat]['id_cotizacion'] = $id_cotizacion;
            $this->db->insert_batch('business.cotizacion_articulo', $cotizacion_material);
        }
        //Almacena  en cotizacion_articulo con respecto a materiales backup
        $cotizacion_material_backup = $data['cotizacion_material_backup'];
        if ($cotizacion_material_backup) {
            foreach ($cotizacion_material_backup as $mat_b => $material_b)
                $cotizacion_material_backup[$mat_b]['id_cotizacion'] = $id_cotizacion;
            $this->db->insert_batch('business.cotizacion_articulo', $cotizacion_material_backup);
        }
        //Almacena  en cotizacion_articulo con respecto a instrumentos
        $cotizacion_instrumento = $data['cotizacion_instrumento'];
        if ($cotizacion_instrumento) {
            foreach ($cotizacion_instrumento as $ins => $instrumento)
                $cotizacion_instrumento[$ins]['id_cotizacion'] = $id_cotizacion;
            $this->db->insert_batch('business.cotizacion_articulo', $cotizacion_instrumento);
        }

        //Almacena  en cotizacion_validacion
        $cotizacion_validacion = $data['cotizacion_validacion'];
        if ($cotizacion_validacion) {
            foreach ($cotizacion_validacion as $val => $validacion)
                $cotizacion_validacion[$val]['id_cotizacion'] = $id_cotizacion;
            $this->db->insert_batch('business.cotizacion_validacion', $cotizacion_validacion);
        }
        $name_file = "cot_" . $codigo_cotizacion;
        $destination_path = 'uploads/cotizaciones/membrete_on/';
        $destination_pdf = $destination_path . $name_file . '.pdf';
        $this->changeStatus($id_cotizacion, 'emitida', TRUE, $destination_pdf, FALSE);

        //Si la cotizacion no tiene ningun campo que validar, se procesa
        if ($this->isValid($id_cotizacion))
            $this->changeStatus($id_cotizacion, 'procesada', TRUE, '', FALSE);

        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    /**
     * Dado un arreglo se obtiene todas las coincidencias del arreglo por
     * clave/valor, donde clave es el campo la base de datos y valor el valor a
     * ser filtrado.
     *
     * @access  public
     * @param   Array $params <p>Contiene los pares clave valor a ser filtrados.</p>
     * @author  Nohemi Rojas, Frederick Bustamante
     * @version V-1.1 21/06/13 12:44 PM
     */
    function getByFields($params) {

        //Select
        $this->db->distinct('cot.id');
        $this->db->select('cot.fecha_cirugia');
        $this->db->select('cot.fecha_cotizacion');
        $this->db->select('cot.codigo');
        $this->db->select('cot.id');
        $this->db->select("med.letra || '-' || med.cedula || ' ' || med.primer_nombre || ' ' || med.primer_apellido AS id_medico");
        $this->db->select("pac.letra || '-' || pac.cedula || ' ' || pac.primer_nombre || ' ' || pac.primer_apellido AS id_paciente");
        $this->db->select("emp.letra || '-' || emp.cedula || ' ' || emp.primer_nombre || ' ' || emp.primer_apellido AS id_vendedor");
        $this->db->select('cen.nombre AS id_centro_salud');

        //From and join
        $this->db->from("$this->table AS cot");
        $this->db->join('business.medico AS med', 'cot.id_medico =  med.id');
        $this->db->join('business.paciente AS pac', 'cot.id_paciente =  pac.id');
        $this->db->join('business.centro_salud AS cen', 'cot.id_centro_salud =  cen.id');
        $this->db->join('business.empleado AS emp', 'cot.id_vendedor =  emp.id');
        if ($this->session->userdata('role_name') == 'Logistica') {
            $this->db->select('cat._label');
            $this->db->join('business.cotizacion_estatus as cot_est', 'cot.id=cot_est.id_cotizacion');
            $this->db->join('dynamic.category as cat', 'cot_est.cat_cotizacion_estatus=cat.id');
        }
        if (!empty($params)) {
            $value = $params['codigo'];

            unset($params['fecha_cotizacion']);
            unset($params['codigo']);
            unset($params['id_medico']);
            unset($params['id_paciente']);
            unset($params['id_centro_salud']);
            unset($params['fecha_cirugia']);
            unset($params['codigo']);

            //se aplica para que la poder realizar busqueda por fecha en el grid
            //------------------------------------------------------
            $params["to_char(fecha_cotizacion,'DD-MM-YYYY HH:MI PM')"] = $value;
            //-------------------------------------------------------

            $params['cot.codigo'] = $value;
            $params['med.primer_nombre'] = $value;
            $params['med.primer_apellido'] = $value;
            $params['med.cedula'] = $value;
            $params['pac.primer_nombre'] = $value;
            $params['pac.primer_apellido'] = $value;
            $params['pac.cedula'] = $value;
            $params['cen.nombre'] = $value;

            $this->_ilike($params, 'OR', TRUE);
        }

        $this->db->where('cot._delete', '0');
        if ($this->session->userdata('role_name') == 'vendedor')
            $this->db->where('cot.created_by', $this->session->userdata('user_id'));
        if ($this->session->userdata('role_name') == 'Logistica') {
            $this->db->where('cat._label', 'Pre-Aprobada');
            $this->db->where('cat.id =(select
				(cat_cotizacion_estatus)
			from
				"business"."cotizacion_estatus"
			where
				id = (select
				id
			from
				"business"."cotizacion_estatus"
			where
				id_cotizacion = cot.id
			group by id
			order by id
			desc limit 1))');
            $this->db->or_where('cat._label', 'Reprogramada');
            $this->db->where('cat.id =(select
				(cat_cotizacion_estatus)
			from
				"business"."cotizacion_estatus"
			where
				id = (select
				id
			from
				"business"."cotizacion_estatus"
			where
				id_cotizacion = cot.id
			group by id
			order by id
			desc limit 1))');
        }
    }

    /**
     * Dado un identificador obtiene los datos asociados al identifiador.
     *
     * @access  public
     * @param   Integer $id Identificador del registro a ser consultado.
     * @param   string $field campo por el cual se realizara la busqueda
     * @return  Array   Con los datos del registro consultado.
     * @author  Nohemi Rojas
     * @version 1.0 03/12/12 12:50 PM
     */
    public function getById($id, $field = 'id') {

        // Tables
        $table = $this->getSchema() . '.cotizacion';
        $t_medico = $this->getSchema() . '.medico';
        $t_centro_salud = $this->getSchema() . '.centro_salud';
        $t_empleado = $this->getSchema() . '.empleado';
        $t_paciente = $this->getSchema() . '.paciente';
        $t_cotizacion_pago = $this->getSchema() . '.cotizacion_pago';
        $t_cotizacion_cirugia = $this->getSchema() . '.cotizacion_cirugia';
        $t_seguro = $this->getSchema() . '.seguro';
        $t_cat_tipo_pago = 'dynamic.category';
        $t_cirugia = $this->getSchema() . '.cirugia';
        $t_lesion = 'lesion';
        $t_articulacion = 'articulacion';
        $t_tecnica = 'tecnica';

        // Selects
        $this->db->select($table . '.id');
        $this->db->select("(CASE WHEN $table.observacion IS NULL THEN
                                                    'SIN OBSERVACIONES'
                                              ELSE
                                                    $table.observacion
                                              END) as observacion", FALSE);
        $this->db->select($table . '.codigo');
        $this->db->select($table . '.fecha_cotizacion');
        $this->db->select($table . '.fecha_cirugia');
        $this->db->select($table . '.hora_cirugia');
        $this->db->select($table . '.descuento');
        $this->db->select($table . '.recargo');
        $this->db->select($table . '.id_validador');
        $this->db->select($table . '.id_paciente');
        $this->db->select($table . '.monto_total');
        $this->db->select($table . '.id_seguro');
        $this->db->select($table . '.enviar');
        $this->db->select($t_cotizacion_pago . '.id_empresa');
        $this->db->select($t_cotizacion_pago . '.id_persona');
        $this->db->select($t_cotizacion_pago . '.monto');
        $this->db->select($t_cotizacion_pago . '.fecha_instrumento');
        $this->db->select($t_cotizacion_pago . '.cat_tipo_pago');
        $this->db->select($t_cotizacion_pago . '.cat_pagador');
        $this->db->select($t_cotizacion_pago . '.numero_instrumento');
        $this->db->select($t_cotizacion_pago . '.cat_banco');
        $this->db->select($t_cotizacion_pago . '.vencimiento');
        $this->db->select($t_cotizacion_pago . '.deducible');
        $this->db->select($t_cat_tipo_pago . '._label AS tipo_pago');
        $this->db->select($t_medico . '.id AS id_medico_id_ec');
        $this->db->select("$t_medico.primer_nombre || ' ' || $t_medico.primer_apellido AS id_medico");
        $this->db->select("$t_empleado.primer_nombre || ' ' || $t_empleado.primer_apellido AS id_vendedor");
        $this->db->select("$t_paciente.primer_nombre || ' ' || $t_paciente.primer_apellido AS id_paciente");
        $this->db->select($t_paciente . '.id AS id_paciente_id_ec');
        $this->db->select($t_paciente . '.validado AS id_paciente_validado');
        $this->db->select($t_centro_salud . '.id AS id_centro_salud_id_ec');
        $this->db->select($t_centro_salud . '.nombre AS id_centro_salud');
        $this->db->select($t_articulacion . '.nombre AS id_articulacion');
        $this->db->select($t_articulacion . '.id AS cod_articulacion');
        $this->db->select($t_tecnica . '.nombre AS id_tecnica');
        $this->db->select($t_lesion . '.nombre AS id_lesion');
        //  $this->db->select($t_seguro . '.nombre AS id_seguro');
        // Froms and inners
        $this->db->from($table);
        $this->db->join($t_medico, "$t_medico.id=$table.id_medico");
        $this->db->join($t_centro_salud, "$t_centro_salud.id=$table.id_centro_salud");
        $this->db->join($t_empleado, "$t_empleado.id=$table.id_vendedor");
        $this->db->join($t_paciente, "$t_paciente.id=$table.id_paciente");
        $this->db->join($t_cotizacion_cirugia, "$t_cotizacion_cirugia.id_cotizacion=$table.id");
        $this->db->join("$t_cirugia AS $t_articulacion", "$t_articulacion.id=$t_cotizacion_cirugia.id_articulacion");
        $this->db->join("$t_cirugia AS $t_lesion", "$t_lesion.id=$t_cotizacion_cirugia.id_lesion");
        $this->db->join("$t_cirugia AS $t_tecnica", "$t_tecnica.id=$t_cotizacion_cirugia.id_tecnica", 'LEFT');
        $this->db->join($t_cotizacion_pago, "$t_cotizacion_pago.id_cotizacion=$table.id");
        $this->db->join($t_cat_tipo_pago, "$t_cat_tipo_pago.id=$t_cotizacion_pago.cat_tipo_pago");
        // $this->db->join($t_seguro, "$t_seguro.id=$table.id_seguro", 'LEFT');
        // Where
        $this->db->where($table . '.' . $field, $id);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row_array() : FALSE;
    }

    /**
     * Obtiene el maximo codigo a generar en la tabla cotizacion
     *
     * @access  public
     * @return  string  codigo maximo
     * @author  Nohemi Rojas
     * @version 09/01/13 11:06 AM
     */
    function generateCode() {
        $length = 15;
        $id_entity = $this->getEntityId('cotizacion');
        $current_value = $this->getCurrentSequentialCounter($id_entity);
        $code = ($current_value > 0) ? $current_value : str_repeat('0', $length);
        return @str_repeat('0', $length - strlen(++$code)) . $code;
    }

    /**
     * Crea una nueva tecnica asociada a la cirugia
     *
     * @access  public
     * @param array $data Arreglo con los datos asociados a la nueva tecnica
     * @param string $nombre_tecnica Nombre nuevo de la tecnica
     * @param array $materiales Arreglo de materiales asociados a la tecnica
     * @return  identificador de la tecnica asociada
     * @author  Nohemi Rojas
     * @version 09/01/13 11:06 AM
     */
    function createTecnica($data, $nombre_tecnica, $materiales) {
        $this->db->trans_begin();
        $arr_data = array(
            'created_by' => $data['created_by'],
            'chk_tabla' => 'tecnica',
            'nombre' => $nombre_tecnica,
            'id_parent' => $data['id_lesion']);
        $data_tecnica = $this->insertWithReturning($arr_data, 'business.cirugia');
        if ($data_tecnica) {

            $this->load->model('settings/categories_model', 'categories_model');
            $tipo_recomendacion = $this->categories_model->getCategory(array('_table' => 'tipo_recomendacion', '_label' => 'MAT'), ' id ');
            foreach ($materiales as $material) {
                $arr_material = array(
                    'id_articulo' => $material,
                    'id_cirugia' => $data_tecnica['id'],
                    'tipo_recomendacion' => $tipo_recomendacion[0]['id'],
                    'cantidad' => '1'
                );
                $this->db->insert('business.articulo_recomendacion', $arr_material);
            }
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return $data_tecnica['id'];
        }
    }

    /**
     * Obtiene el precio de un material
     *
     * @access  public
     * @param integer $id_material  Identificador del Material
     * @return  Precio del material
     * @author  Nohemi Rojas
     * @version 21/01/13 12:52 PM
     */
    function getPrice($id_material) {
        $query = $this->db->query("SELECT precio FROM  business.material WHERE id = $id_material");
        return ($query->num_rows() > 0) ? ($query->row()->precio) : FALSE;
    }

    /**
     * Obtiene los materiales asociados a una cotizacion
     *
     * @access  public
     * @param integer $id_cotizacion  Identificador de la cotizacion
     * @return  Array   Con los datos del registro consultado.
     * @author  Nohemi Rojas
     * @version 21/01/13 12:52 PM
     */
    function getMaterialesByCot($id_cotizacion) {

        //Tables
        $cotizacion_material = $this->getSchema() . '.cotizacion_material';
        $material = $this->getSchema() . '.material';
        $cotizacion = $this->concatSchemaTable();

        //Query
        $this->db->select($material . '.id');
        $this->db->select($cotizacion_material . '.precio');
        $this->db->select($cotizacion . '.monto_total AS monto');
        $this->db->select($cotizacion_material . '.cantidad');
        $this->db->select($material . '.modelo');
        $this->db->select($material . '.descripcion');
        $this->db->select($material . '.imagen');
        $this->db->from($cotizacion_material);
        $this->db->join($material, "$material.id=$cotizacion_material.id_material");
        $this->db->join($cotizacion, "$cotizacion.id=$cotizacion_material.id_cotizacion");
        $this->db->where(array("$cotizacion_material.id_cotizacion" => $id_cotizacion, "$cotizacion_material.backup" => "0"));
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

    /**
     * Obtiene los instrumentos asociados a una cotizacion
     *
     * @access  public
     * @param integer $id_cotizacion  Identificador de la cotizacion
     * @return  Array   Con los datos del registro consultado.
     * @author  Nohemi Rojas
     * @version  V-1.0 21/01/13 12:52 PM
     */
    function getInstrumentosByCot($id_cotizacion) {
        //Tables
        $cotizacion_instrumento = $this->getSchema() . '.cotizacion_instrumento';
        $instrumento = $this->getSchema() . '.instrumento';
        $cotizacion = $this->concatSchemaTable();

        //Query
        $this->db->select($instrumento . '.id');
        $this->db->select($cotizacion_instrumento . '.cantidad');
        $this->db->select($instrumento . '.modelo');
        $this->db->select($instrumento . '.descripcion');
        $this->db->select($instrumento . '.imagen');
        $this->db->from($cotizacion_instrumento);
        $this->db->join($instrumento, "$instrumento.id=$cotizacion_instrumento.id_instrumento");
        $this->db->join($cotizacion, "$cotizacion.id=$cotizacion_instrumento.id_cotizacion");
        $this->db->where("$cotizacion_instrumento.id_cotizacion", $id_cotizacion);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

    /**
     * Provee las implementacion para realizar validaciones en la entidad cotizacion
     *
     * @access  public
     * @param   integer  $id_cotizacion Identificador de la cotizacion
     * @param   array  $data datos a ser guardados dentro de la base de datos.
     * @return  Boolean Indicando el resultado de la operacion.
     * @author  Nohemi Rojas
     * @version V-1.0 23/01/13 12:47 PM
     */
    function validate($id_cotizacion, $data, $extra_data, $level_current, $level_last) {
        $this->db->trans_start();
        $cot_val = $data['cotizacion_validacion'];
        $cotizacion_pago = $data['cotizacion_pago'];

        $cat_level_current = $this->engine_model->getValueByCategory($level_current, '_label');
        $cat_level_last = $this->engine_model->getValueByCategory($level_last, '_label');

        foreach ($cot_val as $key => $value) {

            $id_validacion = $this->validacion_model->getFieldByName($key);

            //Verifica que este validado
            if ($level_current == 'validador') {
                switch ($key) {
                    case 'id_persona':
                        $this->persona_model->update($extra_data['id_persona'], array('validado' => '1'), 'id');
                        break;

                    case 'id_empresa':
                        $this->empresa_model->update($extra_data['id_empresa'], array('validado' => '1'), 'id');
                        break;

                    case 'id_paciente':
                        $this->paciente_model->update($extra_data['id_paciente'], array('validado' => '1'), 'id');
                        break;
                    default:
                        break;
                }
            }

            //En el caso de vendedor no tiene el campo _checked, es por ello que se asume como 0
            $value['_checked'] = isset($value['_checked']) ? $value['_checked'] : '0';

            //Si el valor no es valido se crea un nuevo registro
            if (!$value['_checked']) {
                $arr_data = array('id_validacion' => $id_validacion, 'id_cotizacion' => $id_cotizacion, 'cat_val_nivel' => $cat_level_last);
                $this->db->insert('business.cotizacion_validacion', $arr_data);
            }

            //Indeferente del estatus validado, los valores del Track son actualizados y cerrados
            $arr_data = array('estatus' => $value['_checked'], 'cat_val_observacion' => $value['_cat'],
                'abierto' => '0', 'modified_by' => $this->session->userdata('user_id'), 'modified_at' => date('Y-m-d H:i:s'));
            $this->db->where('id_validacion', $id_validacion);
            $this->db->where('id_cotizacion', $id_cotizacion);
            $this->db->where('cat_val_nivel', $cat_level_current);
            $this->db->where('modified_at IS NULL');
            $this->db->update('business.cotizacion_validacion', $arr_data);


            //se verifica si tiene valores el medio de pago, para editarlo
            if ($cotizacion_pago) {
                $this->table_name = 'cotizacion_pago';
                $this->update($id_cotizacion, $cotizacion_pago, 'id_cotizacion');
            }
        }

        //Se comprueba si la cotizacion ya esta totalmente validada
        if ($this->isValid($id_cotizacion)) {
            //Se obtiene el status actual, ya que esta funcion se llama repetidas veces,
            //cambiando el estado de la cotizacion, y No se desea que el mismo este repetido
            $status = $this->getStatus($id_cotizacion);

            if ($status['status_name'] == 'Emitida')
                $this->changeStatus($id_cotizacion, 'Procesada');
            if ($status['status_name'] == 'Procesada')
                $this->changeStatus($id_cotizacion, 'Procesada-Medio-Pago');
            if ($status['status_name'] == 'Procesada-Medio-Pago')
                $this->changeStatus($id_cotizacion, 'Pre-Aprobada');
        }

        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    /**
     * Obtiene el status actual de la cotizacion
     *
     * @access  public
     * @param   integer  $id_cotizacion Identificador de la cotizacion
     * @return  array Resultado de la operacion.
     * @author  Nohemi Rojas
     * @version V-1.0 11/04/13 06:38 PM
     */
    function getStatus($id_cotizacion) {
        $this->db->select('cat_cotizacion_estatus AS status_id, _label AS status_name');
        $this->db->from('business.cotizacion_estatus');
        $this->db->join('dynamic.category', 'cat_cotizacion_estatus=category.id');
        $this->db->where('id_cotizacion', $id_cotizacion);
        $this->db->order_by('created_at', 'DESC');
        $this->db->order_by('status_id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row_array() : FALSE;
    }

    /**
     * Obtiene todos los status de la cotizacion
     *
     * @access  public
     * @param   integer  $id_cotizacion Identificador de la cotizacion
     * @return  array Resultado de la operacion.
     * @author  Nohemi Rojas
     * @version V-1.0 11/04/13 06:38 PM
     */
    function getAllStatus($id_cotizacion) {
        $this->db->select('cat_cotizacion_estatus AS status_id, _label AS status_name, created_at AS status_date');
        $this->db->from('business.cotizacion_estatus');
        $this->db->join('dynamic.category', 'cat_cotizacion_estatus=category.id');
        $this->db->where('id_cotizacion', $id_cotizacion);
        $this->db->order_by('created_at', 'DESC');
        $this->db->order_by('cat_cotizacion_estatus', 'DESC');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    /**
     * Cambia el status de una cotizacion a aprobada, emitida,procesada
     *
     * @access  public
     * @param   integer  $id_cotizacion Identificador de la cotizacion
     * @param   string   $status Nuevo Estatus de la cotizacion, debe ser el valor que esta
     *                  en categoria cotizacion_estatus
     * @param   Boolean $send_email Indica si se enviara email con el cambio de estatus
     * @return  Boolean Resultado de la operacion.
     * @author  Nohemi Rojas
     * @version V-1.0 11/04/13 04:23 PM
     */
    function changeStatus($id_cotizacion, $status, $send_email = TRUE, $destination_path = '', $activate_mail = TRUE) {
        $attachment = '';
        $body = '';
        $status_name = mb_convert_case($status, MB_CASE_TITLE, "UTF-8");
        $status_id = $this->engine_model->getValueByCategory($status_name, '_label');

        if (!$status_id)
            return FALSE;

        $data = array('id_cotizacion' => $id_cotizacion, 'cat_cotizacion_estatus' => $status_id, 'created_by' => $this->session->userdata('user_id'));

        $result_ope = $this->db->insert('business.cotizacion_estatus', $data);

        $this->load->helper('view_engine');

        //si el resultado de la operacion es false, o si send_email es false
        if ($result_ope)
            writeUpdateTime();

        if (!$result_ope or !$send_email)
            return $result_ope;

        $code = $this->getCode($id_cotizacion);
        $html['datos'] = $this->getById($id_cotizacion);
        switch ($status_name) {
            case 'Emitida':
                $email = array($this->session->userdata('user_email'));
                //$email = 'epena@ast.com.ve' //cable ;
                $html['html'] = sprintf($this->lang->line('body_cot_emitida'), $code);
                $html['title'] = $this->lang->line('subject_cot_emitida');
                $body = $this->load->view('cotizacion/email_cotizacion', $html, TRUE);
                $subject = $this->lang->line('subject_cot_emitida');
                $attachment = $destination_path;

                break;
            case 'Procesada':
                //$email_medico = $this->getMedico($id_cotizacion)->correo_electronico;
                $email_medico = 'fbustamante@ast.com.ve'; //cable
                $email_user = $this->session->userdata('user_email');
                $email = array($email_user, $email_medico);
                $html['html'] = sprintf($this->lang->line('body_cot_procesada'), $code);
                $html['title'] = $this->lang->line('subject_cot_procesada');
                $body = $this->load->view('cotizacion/email_cotizacion', $html, TRUE);
                $subject = $this->lang->line('subject_cot_procesada');
                $attachment = $destination_path;
                break;
            case 'Procesada-Medio-Pago':
                $email = array($this->session->userdata('user_email'));
                $html['html'] = sprintf($this->lang->line('body_cot_procesada_medio_pago'), $code);
                $html['title'] = $this->lang->line('subject_cot_procesada_medio_pago');
                $body = $this->load->view('cotizacion/email_cotizacion', $html, TRUE);
                $subject = $this->lang->line('subject_cot_procesada_medio_pago');
                break;
            case 'Pre-Aprobada':
                $email = array($this->session->userdata('user_email'));
                $html['html'] = sprintf($this->lang->line('body_cot_pre_aprobada'), $code);
                $html['title'] = $this->lang->line('subject_cot_pre_aprobada');
                $body = $this->load->view('cotizacion/email_cotizacion', $html, TRUE);
                $subject = $this->lang->line('subject_cot_pre_aprobada');
                // se abre nortificacion a perfil logistica
                $cat_level_current = $this->engine_model->getValueByCategory('logistica', '_label');
                $id_validacion = $this->validacion_model->getFieldByName('cirugia_preaprobada');
                $this->notificationSend($id_cotizacion, $id_validacion, $cat_level_current);
                break;
            case 'Confirmada':
                $email = array($this->session->userdata('user_email'));
                $html['html'] = sprintf($this->lang->line('body_cot_confirmada'), $code);
                $html['title'] = $this->lang->line('subject_cot_confirmada');
                $body = $this->load->view('cotizacion/email_cotizacion', $html, TRUE);
                $subject = $this->lang->line('subject_cot_confirmada');
                break;
            case 'Reprogramada':
                $email = array($this->session->userdata('user_email'));
                $html['html'] = sprintf($this->lang->line('body_cot_reprogramada'), $code);
                $html['title'] = $this->lang->line('subject_cot_reprogramada');
                $body = $this->load->view('cotizacion/email_cotizacion', $html, TRUE);
                $subject = $this->lang->line('subject_cot_reprogramada');
                break;
            case 'Aprobada':
                $email = array($this->session->userdata('user_email'));
                $html['html'] = sprintf($this->lang->line('body_cot_aprobada'), $code);
                $html['title'] = $this->lang->line('subject_cot_aprobada');
                $body = $this->load->view('cotizacion/email_cotizacion', $html, TRUE);
                $subject = $this->lang->line('subject_cot_aprobada');
                break;
            case 'Reprogramar Cirugia':
                $vendedor = $this->getEmailVendedor($id_cotizacion);
                $email_vendedor = $vendedor[0]['correo_electronico'];
                $email_user = $this->session->userdata('user_email');
                $email = array($email_user, $email_vendedor);
                $html['html'] = sprintf($this->lang->line('body_cot_reprogramar'), $code);
                $html['title'] = $this->lang->line('subject_cot_reprogramar');
                $body = $this->load->view('cotizacion/email_cotizacion', $html, TRUE);
                $subject = $this->lang->line('subject_cot_reprogramar');
                break;
            case 'Sustituir Articulos':
                $vendedor = $this->getEmailVendedor($id_cotizacion);
                $email_vendedor = $vendedor[0]['correo_electronico'];
                $email_user = $this->session->userdata('user_email');
                $email = array($email_user, $email_vendedor);
                $html['html'] = sprintf($this->lang->line('body_cot_sustituir'), $code);
                $html['title'] = $this->lang->line('subject_cot_sustituir');
                $body = $this->load->view('email_cotizacion', $html, TRUE);
                $subject = $this->lang->line('subject_cot_sustituir');
                break;
            default:
                return FALSE;
                break;
        }

        return sendEmail($email, $body, $subject, $body, $attachment, $activate_mail);
    }

    /**
     * almacena los datos de los medios de pago.
     *
     * @access  public
     * @param   integer  $id_cotizacion identificador de la cotizacion
     * @param   array    $data  datos a ser actualizados.
     * @return  bool    resultado del update.
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 30/r01/13 14:55
     */
    public function addPayment($id_cotizacion, $data) {
        $this->db->trans_start();
        $this->table_name = 'cotizacion_pago';
        $this->update($id_cotizacion, $data['cotizacion_pago'], 'id_cotizacion');

        //inserta las validaciones correspondientes de medio de pago
        $this->db->insert_batch('business.cotizacion_validacion', $data['cotizacion_validacion']);
        $this->changeStatus($id_cotizacion, 'Procesada-Medio-Pago');
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    /**
     * Obtiene los datos del medico de una determinada cotizacion
     *
     * @access  public
     * @param   integer  $id_cotizacion identificador de la cotizacion
     * @return  array    resultado de la cosulta.
     * @author  Nohemi Rojas
     * @version V 1.0 12/04/13 11:25 AM
     */
    public function getMedico($id_cotizacion) {

        $this->db->select('correo_electronico, primer_nombre,segundo_nombre, primer_apellido, segundo_apellido, cedula');
        $this->db->from('business.cotizacion');
        $this->db->join('business.medico', 'medico.id = cotizacion.id_medico');
        $this->db->where('cotizacion.id', $id_cotizacion);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row() : FALSE;
    }

    /**
     * Consulta si la cotizacion esta completamente validada a o no.
     * Para ello no debe existir datos por correccion
     *
     * @access  public
     * @param   integer  $id_cotizacion  identificador de la cotizacion
     * @return  bool    True si esta validada, False caso contrario
     * @author  Nohemi Rojas
     * @version V 1.0 02/04/13 04:28 PM
     */
    public function isValid($id_cotizacion) {
        $this->db->select('*');
        $this->db->from('business.cotizacion_validacion');
        $this->db->where('id_cotizacion', $id_cotizacion);
        $this->db->where('abierto', '1');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? FALSE : TRUE;
    }

    /**
     * Obtiene el codigo de una cotizacion dado su identificador
     *
     * @access  public
     * @param   integer  $id_cotizacion identificador de la cotizacion
     * @return  string   codigo de la cotizacion
     * @author  Nohemi Rojas
     * @version V 1.0 03/04/13 04:01 PM
     */
    public function getCode($id_cotizacion) {
        $this->db->select('codigo');
        $this->db->from('business.cotizacion');
        $this->db->where('id', $id_cotizacion);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->row()->codigo) : NULL;
    }

    /**
     * Sobreescritura del metodo
     *
     * @access  public
     * @author Nohemi Rojas
     * @version V 1.0 11/04/13 06:05 PM
     */
    public function orderBy() {
        if ($this->session->userdata('role_name') == 'Logistica') {
            $this->db->order_by('cat._label', 'DESC');
            $this->db->order_by('fecha_cirugia', 'ASC');
        }
        $this->db->order_by('codigo', 'DESC');
    }

    /**
     * Metodo que retorna los materiales asociados a una cotizacion especifica.
     *
     * @access public
     * @param array $params Parametros de entrada
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 16/08/13 11:08 AM
     */
    function get_backup($params = array()) {
        extract($params);
        $t_articulo = $this->getSchema() . '.articulo';
        $t_cotizacion_articulo = $this->getSchema() . '.cotizacion_articulo';

        $this->db->select($t_articulo . ".id", false);
        $this->db->select($t_articulo . ".modelo", false);
        $this->db->select("(CASE WHEN $t_articulo.descripcion IS NULL THEN
                                                    'SIN DESCRIPCION'
                                              ELSE
                                                    $t_articulo.descripcion
                                              END) as descripcion", false);
        $this->db->select($t_articulo . ".imagen", false);
        $this->db->select($t_cotizacion_articulo . ".precio", false);
        $this->db->select($t_cotizacion_articulo . ".cantidad", false);
        $this->db->from($t_articulo);
        $this->db->join($t_cotizacion_articulo, "$t_articulo.id = $t_cotizacion_articulo.id_articulo");
        $this->db->where(array("$t_cotizacion_articulo.id_cotizacion" => $id, "$t_cotizacion_articulo.backup" => $backup));
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

    /**
     * Obtiene el precio de un material dado su identificador
     *
     * @access  public
     * @param   integer  $param identificador de la cotizacion
     * @return  string   precio material
     * @author  Frederick Bustamante
     * @version V 1.0 17/06/13 04:01 PM
     */
    public function get_Materiales($param) {
        $query = $this->db->query("SELECT  precio FROM  business.material WHERE id = $id_material");
        return ($query->num_rows() > 0) ? ($query->row()->precio) : FALSE;
    }

    /**
     * Obtiene los datos de los materiales seleccionados para cargarlos al pdf de cotizacion
     *
     * @access  public
     * @param   array  $param identificador de materiales
     * @return  array  $result
     * @author  Frederick Bustamante
     * @version V 1.0 17/06/13 04:01 PM
     */
    function getMaterialesPdf($param) {
        //Tables
        $articulo = $this->getSchema() . '.articulo';
        foreach ($param as $clave => $valor):
            //Query
            $query = $this->db->query("
                                SELECT
                                    id,
                                    '$valor[precio]' as precio,
                                    '$valor[cantidad]' as cantidad,
                                    modelo ,
                                    descripcion
                                FROM
                                    $articulo
                                WHERE
                                    id = $valor[id_articulo]");
            $result[] = $query->result_array();
        endforeach;
        return $result;
    }

    /**
     * Obtiene datos de una aseguradora dado su identificador
     *
     * @access  private
     * @param   integer  $id identificador de la aseguradora
     * @return  array    datos de la aseguradora
     * @author  Frederick Bustamante
     * @version V 1.0 17/06/13 04:01 PM
     */
    private function getAseguradora($id) {
        $this->db->select('nombre');
        $this->db->select('tlf');
        $this->db->from('business.seguro');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

    /**
     * Obtiene datos de un medico dado su identificador
     *
     * @access  private
     * @param   integer  $id identificador del medico
     * @return  array    datos del medico
     * @author  Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version V 1.0 17/06/13 05:01 PM
     */
    private function getNombreMedico($id) {
        $this->db->select("primer_nombre || ' ' || primer_apellido as medico");
        $this->db->from('business.medico');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->row()->medico) : NULL;
    }

    /**
     * Obtiene datos de un paciente dado su identificador
     *
     * @access  private
     * @param   integer  $id identificador del paciente
     * @return  array    datos del paciente
     * @author  Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version V 1.0 17/06/13 04:01 PM
     */
    private function getPaciente($id) {
        $this->db->select("primer_nombre || ' ' || primer_apellido as paciente");
        $this->db->select("letra || '-' || cedula as cedula");
        $this->db->from('business.paciente');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

    /**
     * Obtiene la categoria de pago usada dado su identificador
     *
     * @access  public
     * @param   integer  $id_pago identificador del paciente
     * @return  string   tipo_pago categoria de pago usada
     * @author  Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version V 1.0 17/06/13 04:01 PM
     */
    public function getTipoPago($id_pago) {
        $this->db->select('_label as tipo_pago');
        $this->db->from('dynamic.category');
        $this->db->where('id', $id_pago);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->row()->tipo_pago) : NULL;
    }

    /**
     * Obtiene datos de un vendedor dado su identificador
     *
     * @access  public
     * @param   integer  $id_vendedor identificador del vendedor
     * @return  array    datos del vendedor
     * @author  Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version V 1.0 17/06/13 05:40 PM
     */
    public function getVendedor($id_vendedor) {
        $this->db->select("primer_nombre || ' ' || primer_apellido as vendedor");
        $this->db->from('business.persona');
        $this->db->where('id', $id_vendedor);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->row()->vendedor) : NULL;
    }

    /**
     * funcion que recoge los datos para enviarlos a la generacion de pdf
     * @access public
     * @param integer $id_medico,$id_aseguradora,$id_paciente,$id_pago,$id_vendedor identificadores a usar
     * @return array $response arreglo con los datos a cargar en el pdf
     * @author Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version V 1.0 17/06/13 06:20 PM
     */
    public function getDatosPdf($id_medico, $id_aseguradora, $id_paciente, $id_pago, $id_vendedor) {
        $medico = $this->getNombreMedico($id_medico);
        $seguro = $this->getAseguradora($id_aseguradora);
        $paciente = $this->getPaciente($id_paciente);
        $tipo_pago = $this->getTipoPago($id_pago);
        $vendedor = $this->getVendedor($id_vendedor);
        $response = array(
            'medico' => $medico,
            'nseguro' => $seguro[0]['nombre'],
            'telseguro' => $seguro[0]['tlf'],
            'pnombre' => $paciente[0]['paciente'],
            'pcedula' => $paciente[0]['cedula'],
            'tipo_pago' => $tipo_pago,
            'vendedor' => $vendedor
        );
        return $response;
    }

    /**
     * Aplica el estatus de reprogramar cirugia a la cotizacion
     *
     * @access public
     * @param integer $id Identificador de la cotizacion a actualizar
     * @param array $data Datos y campos a actualizar
     * @return boolean Estatus de la transaccion (TRUE/FALSE)
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 25/07/13 03:22 PM
     */
    function rescheduleSurgery($id = null, $data = array()) {

        $id_validacion_nueva = $this->validacion_model->getFieldByName('cirugia_reprogramada');
        $id_validacion = $this->validacion_model->getFieldByName('fecha_cirugia');
        $cat_level_current = $this->engine_model->getValueByCategory('logistica', '_label');
        $cat_level_last = $this->engine_model->getValueByCategory('vendedor', '_label');
        //Se verifica si los/el parametro(s) de entrada posee(n) dato(s)
        if (!$id == null && sizeof($data) > 0):
            //Transaccion Iniciada
            $this->db->trans_start();

            //Se actualiza la hora y fecha de la cirugia
            $this->update($id, $data, 'id');
            //validar que la fecha de la cirugia sea mayor a la fecha de vencimento del instrumento de pago
            if (!$this->surgeryDateValidate($id, $data)) {
                return $process['validation_error'] = 'El campo Fecha de Cirugia debe ser menor a la fecha de vencimiento del instrumento de pago';
            }
            //Se inserta el nuevo estatus
            $this->changeStatus($id, 'Reprogramada');
            //cerrar notificacion a vendedor
            $this->notificationClose($id, $id_validacion, $cat_level_last);
            //abrir notificacion a logistica
            $this->notificationSend($id, $id_validacion_nueva, $cat_level_current);
            //Transaccion Finalizada
            $this->db->trans_complete();

            //Se retorna el estatus de la transaccion
            return $this->db->trans_status();

        else:
            return null;
        endif;
    }

    /**
     * Cambia el estatus de la Cotizacion de REPROGRAMADA o PRE-APROBADA a APROBADA
     *
     * @access public
     * @param integer $id Identificador de la cirugia a aprobar
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 26/07/13 01:25 PM
     */
    function approveQuote($params = array()) {
        //extraccion de parametros.
        extract($params);
        //Se verifica si los/el parametro(s) de entrada posee(n) dato(s)
        if (!$id == null && !empty($id)):
            $status = $this->getStatus($id);
            $id_validacion = $this->validacion_model->getFieldByName('cirugia_reprogramada');
            $id_validacion2 = $this->validacion_model->getFieldByName('cirugia_preaprobada');
            $id_validacion3 = $this->validacion_model->getFieldByName('cirugia_aprobada');
            $cat_level_last = $this->engine_model->getValueByCategory('logistica', '_label');
            $cat_level_current = $this->engine_model->getValueByCategory('almacenista', '_label');
            $id_of_cot_articles = $this->returnIdOfCotArticles($id);


            //Se verifica que la cotizacion este en los estatus pertinentes para aprobar
            if ($status['status_name'] == 'Reprogramada' || $status['status_name'] == 'Pre-Aprobada'):
                //Transaccion Iniciada
                $this->db->trans_start();


                //Se comprometen los articulos  de la cotizacion
                if ($id_of_cot_articles):
                    $where_update_id = array();
                    $comprometido = array('comprometido' => '1');
                    //$this->db->where('id_cotizacion', $id);
                    foreach ($id_of_cot_articles as $key => $value):
                        $where_update_id[] = $value['id'];
                    endforeach;
                    $this->db->where_in('id', $where_update_id);
                    $this->db->update("business.cotizacion_articulo", $comprometido);
                endif;




                //Se inserta el nuevo estatus
                $this->changeStatus($id, 'Aprobada');
                //abrir notificacion a almacenista 
                $this->notificationSend($id, $id_validacion3, $cat_level_current);

                //cerrar notificaciones de logistica
                $this->notificationClose($id, $id_validacion, $cat_level_last);
                $this->notificationClose($id, $id_validacion2, $cat_level_last);
                //Registro de los instrumentistas seleccionados
                $this->db->insert_batch('business.cotizacion_instrumentista', $id_instrumentista);
                //Transaccion Finalizada
                $this->db->trans_complete();

                //Se retorna el estatus de la transaccion
                return $this->db->trans_status();
            else:
                return null;
            endif;

        else:
            return null;
        endif;
    }

    /**
     * Aplica el estatus de reprogramar cirugia a la cotizacion
     *
     * @access public
     * @param integer $id Identificador de la cotizacion a actualizar
     * @return boolean Estatus de la transaccion (TRUE/FALSE)
     * @author  Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version 1.0 05/08/13 04:22 PM
     */
    function changeSurgery($id, $data) {
        $t_cotizacion_instru = $this->getSchema() . '.cotizacion_instrumentista';
        //Se verifica si los/el parametro(s) de entrada posee(n) dato(s)
        if (!$id == null && !empty($id)):
            $status = $this->getStatus($id);
            $id_validacion = $this->validacion_model->getFieldByName('fecha_cirugia');
            $id_validacion_cerrar = $this->validacion_model->getFieldByName('cirugia_reprogramada');
            $id_validacion_cerrar2 = $this->validacion_model->getFieldByName('cirugia_preaprobada');
            $cat_level_current = $this->engine_model->getValueByCategory('logistica', '_label');
            $cat_level_last = $this->engine_model->getValueByCategory('vendedor', '_label');
            //Se verifica que la cotizacion este en los estatus pertinentes para aprobar
            if ($status['status_name'] == 'Reprogramada' || $status['status_name'] == 'Pre-Aprobada'):
                //Transaccion Iniciada
                $this->db->trans_start();

                //Se inserta el nuevo estatus
                $this->changeStatus($id, 'Reprogramar Cirugia');
                //cerrar notificacion a logistica
                $this->notificationClose($id, $id_validacion_cerrar, $cat_level_current);
                $this->notificationClose($id, $id_validacion_cerrar2, $cat_level_current);
                //enviar notificacion a vendedor
                $this->notificationSend($id, $id_validacion, $cat_level_last);
                $this->db->insert('business.cirugia_observacion', Array('descripcion' => $data['descripcion'],
                    'fecha_tentativa' => $data['fecha_tentativa'], 'created_by' => $this->session->userdata('user_id'), 'id_cotizacion' => $id));

                //Se eliminan los instrumentistas de la cotizacion
                $delete_instrumentistas = array('_delete' => '1');
                $this->db->where('id_cotizacion', $id);
                $this->db->update($t_cotizacion_instru, $delete_instrumentistas);
                //Transaccion Finalizada
                $this->db->trans_complete();

                //Se retorna el estatus de la transaccion
                return $this->db->trans_status();
            else:
                return null;
            endif;

        else:
            return null;
        endif;
    }

    /**
     * obtiene el correo electronico de un vendedor de una cotizacion dado el codigo de la misma
     *
     * @access public
     * @param integer $id_cotizacion Identificador de la cotizacion
     * @return array $data email vendedor
     * @author  Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version 1.0 06/08/13 10:22 AM
     */
    function getEmailVendedor($id_cotizacion) {
        $this->db->select('per.correo_electronico');
        $this->db->from('business.cotizacion cot');
        $this->db->join('business.persona per', 'per.id=cot.id_vendedor');
        $this->db->where('cot.id', $id_cotizacion);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : NULL;
    }

    /**
     * Retorna los datos  principales de la cotizacion
     *
     * @access public
     * @param  integer $id Identificador de la cotizacion
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 08/08/13 04:11 PM
     */
    function get_data_cotizacion($id = NULL) {
        $table = $this->concatSchemaTable();
        $t_cotizacion_pago = $this->getSchema() . '.cotizacion_pago';
        $this->db->select($table . '.*', false);
        $this->db->select($t_cotizacion_pago . '.cat_tipo_pago', false);
        $this->db->from($table);
        $this->db->join($t_cotizacion_pago, "$t_cotizacion_pago.id_cotizacion=$table.id");
        $this->db->where(array($table . '.id' => $id));
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : false;
    }

    /**
     * Devuelve los datos basicos de la cirugia
     *
     * @access public
     * @param integer $id Identificador del tipo de la cirugia
     * @param string  $campo Campos a mostrar en el select (default:'*')
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 31/07/13 12:35 PM
     */
    function getCirugia($where = array(), $campo = '') {

        $this->db->select((empty($campo) ? ' * ' : ' ' . $campo . ' '), false);
        $this->db->from('business.cirugia');
        if (sizeof($where) > 0):
            $this->db->where($where);
        endif;
        $this->db->where(array('_delete' => '0'));
        $this->db->order_by('nombre');

        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

    /**
     * Guarda la recomendacion de backup en caso de ser tecnica libre
     *
     * @access  public
     * @param   integer $id_cirugia Identificador de la Cirugia (tecnica) Creada
     * @param   array   $articles_backup_recomendacion Arreglo de Articulos del Backup (Clave:'id del articulo,Valor:'Cantidad'')
     * @return  boolean Si fue exitosa o no la operacion
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 2.0 03/09/13 09:31 AM
     */
    function createBackupRecomendacion($id_cirugia, $articles_backup_recomendacion) {


        if ($id_cirugia):
            $this->load->model('settings/categories_model', 'categories_model');
            $tipo_recomendacion = $this->categories_model->getCategory(array('_table' => 'tipo_recomendacion', '_label' => 'BACKUP'), ' id ');
            $this->db->trans_start();
            foreach ($articles_backup_recomendacion as $key => $value):
                $arr_material = array(
                    'id_articulo' => $key,
                    'id_cirugia' => $id_cirugia,
                    'tipo_recomendacion' => $tipo_recomendacion[0]['id'],
                    'cantidad' => $value
                );
                $this->db->insert('business.articulo_recomendacion', $arr_material);
            endforeach;
            $this->db->trans_complete();
            return $this->db->trans_status();
        else:
            return false;
        endif;
    }

    /**
     * define la fecha de la cirugia
     *
     * @access public
     * @param integer $id Identificador de la cotizacion a actualizar
     * @param array $data Datos y campos a actualizar
     * @return boolean Estatus de la transaccion (TRUE/FALSE)
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 25/07/13 03:22 PM
     */
    function scheduleSurgery($id = null, $data = array()) {

        //Se verifica si los/el parametro(s) de entrada posee(n) dato(s)
        if (!$id == null && sizeof($data) > 0):
            //Transaccion Iniciada
            $this->db->trans_start();

            //Se actualiza la hora y fecha de la cirugia
            $this->update($id, $data, 'id');

            //Transaccion Finalizada
            $this->db->trans_complete();

            //Se retorna el estatus de la transaccion
            return $this->db->trans_status();

        else:
            return null;
        endif;
    }

    /**
     * Devuelve los datos basicos de la cotizacion
     *
     * @access public
     * @param integer $id Identificador del tipo de la cirugia
     * @param string  $campo Campos a mostrar en el select (default:'*')
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>, Eliezer Pe#a
     * @version 1.0 31/07/13 12:35 PM
     */
    function getCotizacion($where = array(), $campo = '') {

        $this->db->select((empty($campo) ? ' * ' : ' ' . $campo . ' '), false);
        $this->db->from($this->concatSchemaTable());
        if (sizeof($where) > 0):
            $this->db->where($where);
        endif;
        $this->db->where(array('_delete' => '0'));
        $this->db->order_by('nombre');

        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

    /**
     * Obtiene los articulos asociados a una cotizacion
     *
     * @access  public
     * @param   integer $id_cotizacion  Identificador de la cotizacion
     * @param   string  $category  Tipo de categoria
     * @param   string  $backup  Determina si el articulo es un backup
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 12/09/13 12:22 PM
     */
    function getArticulosByCot($id_cotizacion, $category, $backup = '0') {

        if ($id_cotizacion && $category):
            //Tables
            $cotizacion_articulo = $this->getSchema() . '.cotizacion_articulo';
            $articulo = $this->getSchema() . '.articulo';
            $cotizacion = $this->concatSchemaTable();
            $t_tramo_articulo = 'business.tramo_articulo';
            $table_category = 'dynamic.category';
            $this->db->select("(CASE WHEN (SELECT
                                            id_articulo
                                        FROM
                                            business.tramo_articulo
                                        WHERE
                                            id_articulo=$articulo.id
                                        GROUP BY id_articulo
                                        HAVING sum(cantidad) =0) is null THEN
                                                    'checked'
                                              ELSE
                                                    ''
                                              END) as checked", false);
            //Query
            $this->db->select($articulo . '.id');
            $this->db->select($cotizacion_articulo . '.id as id_cot_articulo');
            $this->db->select($cotizacion_articulo . '.precio');
            $this->db->select($cotizacion . '.monto_total AS monto');
            $this->db->select($cotizacion_articulo . '.cantidad');
            $this->db->select($cotizacion_articulo . '.precio');
            $this->db->select($articulo . '.modelo');
            $this->db->select($articulo . '.descripcion');
            $this->db->select($articulo . '.imagen');
            $this->db->from($cotizacion_articulo);
            $this->db->join($articulo, "$articulo.id=$cotizacion_articulo.id_articulo");
            $this->db->join($cotizacion, "$cotizacion.id=$cotizacion_articulo.id_cotizacion");
            $this->db->join($table_category, "$articulo.tipo_articulo=$table_category.id  ");
            $this->db->where(
                    array(
                        "$cotizacion_articulo.id_cotizacion" => $id_cotizacion,
                        "$cotizacion_articulo.backup" => $backup,
                        "$table_category._label" => $category,
                        "$table_category._table" => 'tipo_articulo',
                        "$cotizacion_articulo.id_parent" => NULL));
            $query = $this->db->get();
            return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;

        else:
            return FALSE;
        endif;
    }

    /**
     * valida que la fecha de cirugia supere la fecha de vencimiento del instrumento de pago
     *
     * @access  public
     * @param   integer $id  Identificador de la cotizacion
     * @param   array $data  arreglo de datos de la cirugia
     * @author  Eliezer Pe#a
     * @version 1.0 24/09/13 16:22 PM
     */
    function surgeryDateValidate($id, $data) {
        $param = $this->getById($id);
        $vencimiento = date('Y-m-d', strtotime($param['vencimiento']));
        $cirugia = date('Y-m-d', strtotime($data['fecha_cirugia']));
//        print_r($cirugia);
//        print_r($vencimiento);
        if ($cirugia <= $vencimiento) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * devuelve la observacion de una cirugia dado el id de cotizacion
     *
     * @access  public
     * @param   integer $id  Identificador de la cotizacion
     * @author  Eliezer Pe#a
     * @version 1.0 24/09/13 16:22 PM
     */
    function getObservacionById($id) {
        $observacion = ' business.cirugia_observacion ';
        $this->db->select('descripcion ');
        $this->db->select('fecha_tentativa ');
        $this->db->where('id_cotizacion', $id);
        $this->db->order_by('id', 'desc');
        $this->db->limit(1);
        $this->db->from($observacion);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

    /**
     * Aplica el estatus de sustituir articulos a la cotizacion
     *
     * @access public
     * @param integer $id Identificador de la cotizacion a actualizar
     * @param array $data Datos y campos a actualizar
     * @return boolean Estatus de la transaccion (TRUE/FALSE)
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 30/10/13 04:13 PM
     */
    function replaceArticles($id = null) {

        if (!$id == null):
            //Id de notificacion nueva
            $id_notificacion_nueva = $this->validacion_model->getFieldByName('sustituir_articulos');

            //Id de las notificaciones actuales para cerrar
            $id_notificacion = $this->validacion_model->getFieldByName('cirugia_reprogramada');
            $id_notificacion2 = $this->validacion_model->getFieldByName('cirugia_preaprobada');

            //Nivel de categoria que vera la notificacion
            $cat_level_current = $this->engine_model->getValueByCategory('logistica', '_label');
            $cat_level_last = $this->engine_model->getValueByCategory('vendedor', '_label');
            //Se verifica si los/el parametro(s) de entrada posee(n) dato(s)
            //Transaccion Iniciada
            $this->db->trans_start();
            //Se inserta el nuevo estatus
            $this->changeStatus($id, 'Sustituir Articulos');
            //cerrar notificacion a logistica
            $this->notificationClose($id, $id_notificacion, $cat_level_current);
            //cerrar notificacion a logistica
            $this->notificationClose($id, $id_notificacion2, $cat_level_current);


            //abrir notificacion a vendedor
            $this->notificationSend($id, $id_notificacion_nueva, $cat_level_last);
            //Transaccion Finalizada
            $this->db->trans_complete();

            //Se retorna el estatus de la transaccion
            return $this->db->trans_status();

        else:
            return null;
        endif;
    }

    /**
     * Provee las implementacion para realizar inserciones dentro cotizacion_articulo
     *
     * @access  public
     * @param   Array  $data datos a ser guardados dentro de la base de datos.
     * @return  Boolean Indicando el resultado de la operacion.
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 05/11/13 03:54 PM
     */
    function replace_articles($data) {
        $this->db->trans_start();
        //Almacena  en cotizacion_articulo con respecto a materiales
        $cotizacion_articulo_replace_mat = $data['cotizacion_articulo_replace_mat'];
        if ($cotizacion_articulo_replace_mat) {

            $this->db->insert_batch('business.cotizacion_articulo', $cotizacion_articulo_replace_mat);
        }
        //Almacena  en cotizacion_articulo con respecto a instrumental
        $cotizacion_articulo_replace_ins = $data['cotizacion_articulo_replace_ins'];
        if ($cotizacion_articulo_replace_ins) {
            $this->db->insert_batch('business.cotizacion_articulo', $cotizacion_articulo_replace_ins);
        }
        //Almacena  en cotizacion_articulo con respecto a materiales backup
        $cotizacion_articulo_replace_backup = $data['cotizacion_articulo_replace_backup'];
        if ($cotizacion_articulo_replace_backup) {
            $this->db->insert_batch('business.cotizacion_articulo', $cotizacion_articulo_replace_backup);
        }

        $this->db->trans_complete();

        if ($this->db->trans_status() == TRUE):
            $id_cotizacion = $this->session->userdata("cot_replace.id");
            //Id de notificacion nueva
            $id_notificacion_nueva = $this->validacion_model->getFieldByName('cirugia_preaprobada');

            //Id de las notificaciones actuales para cerrar
            $id_notificacion_actual = $this->validacion_model->getFieldByName('sustituir_articulos');

            //Nivel de categoria que vera la notificacion
            $cat_level_last = $this->engine_model->getValueByCategory('logistica', '_label');
            $cat_level_current = $this->engine_model->getValueByCategory('vendedor', '_label');


            //cerrar notificacion a vendedor
            $this->notificationClose($id_cotizacion, $id_notificacion_actual, $cat_level_current);
            //Se inserta el nuevo estatus
            $this->changeStatus($id_cotizacion, 'Pre-Aprobada');


        //No aplica ya que el cambio de estatus a pre-aprobada tiene el metodo incluido
        //para abrir la notificacion
        //abrir notificacion a logistica
//            $this->notificationSend($id_cotizacion, $id_notificacion_nueva, $cat_level_last);

        endif;
        return $this->db->trans_status();
    }

    /**
     * Verifica si un articulo tiene sustitucion o no
     *
     * @access  public
     * @param   integer $id_article  Identificador de articulo
     * @param   string  $category  Tipo de categoria
     * @param   string  $backup  Determina si el articulo es un backup
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 06/11/13 02:36 PM
     */
    function checkReplaceArticle($id_article = NULL) {
        if ($id_article):
            //Tables


            $articulo = $this->getSchema() . '.articulo';
            $cotizacion = $this->concatSchemaTable();
            $t_tramo_articulo = 'business.tramo_articulo';
            $table_category = 'dynamic.category';
            $cotizacion_articulo = $this->getSchema() . '.cotizacion_articulo';

            //Query
            $this->db->select($articulo . '.id');
            $this->db->select($cotizacion_articulo . '.precio');
            $this->db->select($cotizacion . '.monto_total AS monto');
            $this->db->select($cotizacion_articulo . '.cantidad');
            $this->db->select($cotizacion_articulo . '.precio');
            $this->db->select($articulo . '.modelo');
            $this->db->select($articulo . '.descripcion');
            $this->db->select($articulo . '.imagen');




            $this->db->from($cotizacion_articulo);
            $this->db->join($articulo, "$articulo.id=$cotizacion_articulo.id_articulo");
            $this->db->join($cotizacion, "$cotizacion.id=$cotizacion_articulo.id_cotizacion");
            $this->db->join($table_category, "$articulo.tipo_articulo=$table_category.id  ");
            $this->db->where(
                    array(
                        "$cotizacion_articulo.id_parent" => $id_article));
            $query = $this->db->get();
            return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;

        else:
            return FALSE;
        endif;
    }

    /**
     * Retorna los Identificadores de cotizacion_articulos
     * dado un id cotizacion tomando en cuenta sustituidos
     *
     * @access private
     * @param integer $id Identificador de la cotizacion
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 13/11/13 04:06 PM
     */
    private function returnIdOfCotArticles($id = NULL) {
        if (!$id)
            return FALSE;
        $query = $this->db->query("
                        SELECT
                                business.cotizacion_articulo.id
                        FROM
                                business.cotizacion_articulo
                        WHERE
                                business.cotizacion_articulo.id_parent is  null
                                AND business.cotizacion_articulo.id_cotizacion =  $id
                                AND business.cotizacion_articulo.id_articulo not in (
                                            SELECT
                                                    business.cotizacion_articulo.id_parent
                                            FROM
                                                    business.cotizacion_articulo
                                            WHERE
                                                    business.cotizacion_articulo.id_parent is not null

                                            GROUP BY business.cotizacion_articulo.id_parent
                                                                                    )
		UNION ALL

			SELECT
                                business.cotizacion_articulo.id
                        FROM
                                business.cotizacion_articulo
                        WHERE
                                business.cotizacion_articulo.id_parent is not null
                                AND business.cotizacion_articulo.id_cotizacion =  $id");


        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

    /**
     * Retorna los datos  principales de la cotizacion
     *
     * @access public
     * @param  integer $id Identificador de la cotizacion
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 13/01/14 01:34 PM
     */
    function get_data_cotizacion_free($id = NULL) {
        $t_libre_cotizacion = $this->getSchema() . ".libre_cotizacion";
        $t_cotizacion_pago = $this->getSchema() . '.libre_cotizacion_pago';
        $this->db->select($t_libre_cotizacion . '.*', false);
        $this->db->from("$t_libre_cotizacion");

        $this->db->where(array($t_libre_cotizacion . '.id' => $id));
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : false;
    }

    /**
     * funcion que recoge los datos para enviarlos a la generacion de pdf
     * @access public
     * @param integer $id_medico,$id_aseguradora,$id_paciente,$id_pago,$id_vendedor identificadores a usar
     * @return array $response arreglo con los datos a cargar en el pdf
     * @author Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version V 1.0 17/06/13 06:20 PM
     */
    public function getDatosPdf_free($id_vendedor) {
        $vendedor = $this->getVendedor($id_vendedor);
        $response = array(
            'vendedor' => $vendedor
        );
        return $response;
    }

    /**
     * Retorna los articulos  principales de la cotizacion ya sea libre o normal
     *
     * @access public
     * @param  integer $id Identificador de la cotizacion
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 14/01/14 12:11 PM
     */
    function get_articulos_quote($id = NULL) {
        $query = $this->db->query("
                        SELECT
                                id_cotizacion, id_articulo, precio, cantidad
                        FROM
                                business.cotizacion_articulo
                        WHERE
                                business.cotizacion_articulo.id_parent is  null
                                AND business.cotizacion_articulo.id_cotizacion =  $id
                                AND business.cotizacion_articulo.id_articulo not in (
                                            SELECT
                                                    business.cotizacion_articulo.id_parent
                                            FROM
                                                    business.cotizacion_articulo
                                            WHERE
                                                    business.cotizacion_articulo.id_parent is not null

                                            GROUP BY business.cotizacion_articulo.id_parent
                                                                                    )
		UNION ALL
                        SELECT
                                id_cotizacion, id_articulo, precio, cantidad
                        FROM
                                business.libre_cotizacion_articulo
                        WHERE
                                business.libre_cotizacion_articulo.id_parent is  null
                                AND business.libre_cotizacion_articulo.id_cotizacion =  $id
                                AND business.libre_cotizacion_articulo.id_articulo not in (
                                            SELECT
                                                    business.libre_cotizacion_articulo.id_parent
                                            FROM
                                                    business.libre_cotizacion_articulo
                                            WHERE
                                                    business.libre_cotizacion_articulo.id_parent is not null

                                            GROUP BY business.libre_cotizacion_articulo.id_parent)");


        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

}
