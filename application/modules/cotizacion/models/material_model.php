<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Material_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad material
 * y sus entidades dependientes o extendidas
 *
 * @package Modules
 * @category Model
 * @author Nohemi Rojas. <nohemir@gmail.com>
 * @version V-1.0 03/04/13 03:06 PM
 */
class Material_model extends AST_Model {

    private $table;

    public function __construct() {
        $this->setSchema('business');
        parent::__construct();
        $this->table = 'business.material';
    }

    /**
     * Obtiene la lista de materiales que estan relacionados con una cirugia
     *
     * @param integer  $cirugia_id Identificador del tipo de cirugia
     * @return  mixed Array de los valores de los registros, false en caso de que no encuentre
     * @author Eliel Parra, Nohemi Rojas, Frederick Bustamante <frederickdanielb@gmail.com>
     * @version 3.0 28/06/13 03:45 PM
     */
    public function getAll($cirugia_id) {
        if (empty($cirugia_id)):
            return FALSE;
        else:
            $id_articulacion = $this->getIdArticulacion($cirugia_id);
            $id_articulacion = $id_articulacion[0]['id_art'];
            if ($id_articulacion == FALSE):
                return FALSE;
            endif;

            $query = $this->db->query("SELECT distinct id, descripcion, modelo, imagen,
                (
                    CASE WHEN m_r_id in (
                        SELECT id
                        FROM business.material_recomendacion
                        WHERE id_cirugia=$cirugia_id
                    ) THEN 1 ELSE 0 END
                ) AS checked,
                ( -- Obtengo el ID Articulacion
                    CASE WHEN m_r_id in (
                        SELECT id
                        FROM business.material_recomendacion
                        WHERE id_cirugia=$cirugia_id
                    ) THEN cantidad ELSE 0 END
                  -----------------------------
                ) AS cantidad,
                array(
                    SELECT precio
                    FROM business.material_precio
                    WHERE id_material = business.view_materiales_full.id
                    ORDER BY business.material_precio.id_precio
                ) AS arr_precio,
                array(
                    SELECT id_precio
                    FROM business.material_precio
                    WHERE id_material = business.view_materiales_full.id
                    ORDER BY business.material_precio.id_precio
                ) AS arr_id_precio
                FROM business.view_materiales_full
                WHERE id_cirugia=$id_articulacion and m_r_id in (
                        SELECT id
                        FROM business.material_recomendacion
                        WHERE id_cirugia=$cirugia_id
                    )
                ORDER BY checked DESC, modelo");
            return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
        endif;
    }

    /**
     * Obtiene el tipo de cirugia mediante el id cirugia
     *
     * @access private
     * @param integer  $cirugia_id Identificador del tipo de cirugia
     * @return  string Tipo de cirugia
     * @author Frederick Bustamante  <frederickdanielb@gmail.com>
     * @version 1.0 26/06/13 05:07 PM
     */
    function _get_tipo_cirugia($param) {
        $t_cirugia = $this->getSchema() . '.cirugia';
        $this->db->select('chk_tabla');
        $this->db->from(" $t_cirugia ");
        $this->db->where(array("id" => $param));
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row_array() : FALSE;
    }

    /**
     * Devuelve el id articulacion
     *
     * @access public
     * @param $integer $id Identificador de la cirugia
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 26/07/13 04:50 PM
     */
    public function getIdArticulacion($id) {
        $query = $this->db->query("
                               SELECT
                                id_art
                               FROM
                                business.view_id_articulacion
                               WHERE
				id_cir = $id
                                    ");
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    /**
     * Obtiene todos los materiales para crear el backup
     *
     * @return  mixed Array de los valores de los registros, false en caso de que no encuentre
     * @author Frederick Bustamante <frederickdanielb@gmail.com>
     * @version 1.0 18/07/13 02:45 PM
     */
    public function getAll_for_backup() {

        $query = $this->db->query("
                                    SELECT
                                            *
                                    FROM ( --Listo todos los materiales
                                            SELECT

                                                    mat.id,
                                                    0 as checked,
                                                    mat.descripcion,
                                                    mat.modelo,
                                                    0 as cantidad,
                                                    mat.imagen,
                                                    mat.precio
                                            FROM
                                                    business.material AS mat
                                                    ) AS materiales
                                        ORDER BY
                                                checked
                                        DESC, materiales.modelo
            ");
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

   
}