<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Material_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad cotizacion_pago
 * y sus entidades dependientes o extendidas
 *
 * @package Modules
 * @category Model
 * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V-1.0 24/09/13 04:47 PM
 */
class Method_of_payment_model extends AST_Model {

    public function __construct() {

        $this->setSchema('business');
        $this->init('cotizacion_pago');
        $this->table = $this->concatSchemaTable();
    }

    /**
     * Provee las implementacion para realizar inserciones dentro cotizaciones
     * y las entidades dependientes de ella
     *
     * @access  public
     * @param   Array  $data datos a ser guardados dentro de la base de datos.
     * @return  Boolean Indicando el resultado de la operacion.
     * @author  Nohemi Rojas
     * @version V-1.0 30/11/2012 09:42
     */
    function insert($data) {
        $id_cotizacion = $this->encrypt->decode($this->session->userdata('id_cot'));
        $this->db->trans_start();
        //Almacena en business.cotizacion_pago
        $data['cotizacion_pago']['id_cotizacion'] = $id_cotizacion;
        $this->db->insert('business.cotizacion_pago', $data['cotizacion_pago']);
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    /**
     * Dado un arreglo se obtiene todas las coincidencias del arreglo por
     * clave/valor, donde clave es el campo la base de datos y valor el valor a
     * ser filtrado.
     *
     * @access  public
     * @param   Array   $params <p>Contiene los pares clave valor a ser filtrados.</p>
     * @param   boolean $returned [optional] <p>
     *      <b>TRUE(default)</b> retorna el resultado de la consulta a BD.
     *      <b>FALSE</b> realiza la ejecucion del metodo where de CI.</p>
     * @return  mixed   <p>
     *  <b>Array<b/> con el resultado de la consulta.
     *  <b>Void<b/> setea el where del metodo de CI.</p>
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V-1.0 27/08/13 05:48 PM
     */
    function getByFields($params = array()) {
        $id_cotizacion = $this->encrypt->decode($this->session->userdata('id_cot'));

        $cotizacion = 'business.cotizacion';
        $category = 'dynamic.category';
        //Select
        $this->db->select($this->table . '.id', FALSE);
        $this->db->select($this->table . '.fecha_instrumento', FALSE);
        $this->db->select($this->table . '.numero_instrumento', FALSE);
        $this->db->select($cotizacion . '.id as id_cotizacion', FALSE);
        $this->db->select($this->table . '.cat_banco');
        $this->db->select('cat_1._label as cat_tipo_pago', FALSE);
        $this->db->select('cat_2._label as cat_banco', FALSE);
        $this->db->select('cat_3._label as cat_pagador', FALSE);


        $this->db->from($cotizacion);
        $this->db->join("$this->table", "$this->table.id_cotizacion=$cotizacion.id");
        $this->db->join("$category as cat_1", "$this->table.cat_tipo_pago=cat_1.id LEFT ");
        $this->db->join("$category as cat_2", "$this->table.cat_banco=cat_2.id ");
        $this->db->join("$category as cat_3", "$this->table.cat_pagador=cat_3.id");
        $this->whereDelete();
//        if (!empty($params)) {
//            unset($params['id_cotizacion']);
//
//
//            $this->_ilike($params, 'OR', TRUE);
//        }
        $this->db->where(array("$cotizacion.id" => $id_cotizacion));
    }

    /**
     * Dado un identificador obtiene los datos asociados al identifiador.
     *
     * @access  public
     * @param   Integer $id Identificador del registro a ser consultado.
     * @param   string $field campo por el cual se realizara la busqueda
     * @return  Array   Con los datos del registro consultado.
     * @author  Nohemi Rojas
     * @version 1.0 03/12/12 12:50 PM
     */
    public function getById($id) {

        $cotizacion = 'business.cotizacion';
        $category = 'dynamic.category';
        $cotizacion_pago = 'business.cotizacion_pago';
        $empresa = 'business.empresa';
        //Select
        $this->db->select($this->table . '.id', FALSE);
        $this->db->select($this->table . '.fecha_instrumento', FALSE);
        $this->db->select($this->table . '.numero_instrumento', FALSE);
        $this->db->select($cotizacion . '.id as id_cotizacion', FALSE);
        $this->db->select($cotizacion . '.id as id_cotizacion', FALSE);
        $this->db->select($this->table . '.cat_banco');
        $this->db->select('cat_1._label as cat_tipo_pago', FALSE);
        $this->db->select('cat_2._label as cat_banco', FALSE);
        $this->db->select('cat_3._label as cat_pagador', FALSE);
        $this->db->select('cat_2._label as cat_banco', FALSE);
        $this->db->select($empresa.'.nombre as id_empresa', FALSE);
        $this->db->select($this->table . '.deducible', FALSE);
//SELECT id, created_by, created_at, modified_by, modified_at, deleted_by,
//       deleted_at, _delete, id_cotizacion, cat_tipo_pago, cat_pagador,
//       numero_instrumento, fecha_instrumento, monto, cat_banco, vencimiento,
//       deducible, digital, is_valid, id_persona, id_empresa
//  FROM business.cotizacion_pago;

        $this->db->from($cotizacion);
        $this->db->join("$this->table", "$this->table.id_cotizacion=$cotizacion.id");
        $this->db->join("$category as cat_1", "$this->table.cat_tipo_pago=cat_1.id LEFT ");
        $this->db->join("$category as cat_2", "$this->table.cat_banco=cat_2.id ");
        $this->db->join("$category as cat_3", "$this->table.cat_pagador=cat_3.id LEFT");
        $this->db->join("$empresa", "$this->table.id_empresa=$empresa.id ");
        $this->db->where($cotizacion_pago . '.id', $id);
        $this->whereDelete();
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row_array() : FALSE;
    }

}
