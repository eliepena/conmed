<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Cotizacion_free_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad libre_cotizacion
 * y sus entidades dependientes o extendidas
 *
 * @package Modules
 * @category Model
 * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V-1.0 07/01/14 10:15 AM
 */
class Cotizacion_free_model extends AST_Model {

    public function __construct() {
        parent::__construct();
        $this->setSchema('business');
        $this->init('libre_cotizacion');
        $this->load->model('settings/categories_model', 'categories_model');
    }

    /**
     * Dado un arreglo se obtiene todas las coincidencias del arreglo por
     * clave/valor, donde clave es el campo la base de datos y valor el valor a
     * ser filtrado.
     *
     * @access  public
     * @param   Array $params <p>Contiene los pares clave valor a ser filtrados.</p>
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V-1.1 21/06/13 12:44 PM
     */
    function getByFields($params) {
        //Tables
        $t_libre_cotizacion = $this->getSchema() . ".libre_cotizacion";
        $t_persona = $this->getSchema() . ".persona";
        $t_empresa = $this->getSchema() . ".empresa";
        $t_empleado = $this->getSchema() . ".empleado";

        //Selects
        $this->db->distinct("$t_libre_cotizacion.id");
        $this->db->select("$t_libre_cotizacion.fecha_cotizacion");
        $this->db->select("$t_libre_cotizacion.codigo");
        $this->db->select("$t_libre_cotizacion.id");
        $this->db->select("COALESCE($t_persona.primer_nombre || ' ' || $t_persona.primer_apellido , $t_empresa.nombre) as id_cliente", FALSE);
        $this->db->select("COALESCE($t_empleado.primer_nombre || ' ' || $t_empleado.primer_apellido ) as id_vendedor", FALSE);

        //From and join
        $this->db->from("$t_libre_cotizacion left ");
        $this->db->join("$t_persona", "$t_libre_cotizacion.id_cliente =  $t_persona.id left ");
        $this->db->join("$t_empresa", "$t_libre_cotizacion.id_cliente =  $t_empresa.id");
        $this->db->join("$t_empleado", "$t_libre_cotizacion.id_vendedor =  $t_empleado.id");
        if (!empty($params)) {

            $value = $params['codigo'];
            unset($params['fecha_cotizacion']);
            unset($params['codigo']);
            unset($params['id_cliente']);
            unset($params['id_vendedor']);
            unset($params['id']);


            //se aplica para que la poder realizar busqueda por fecha en el grid
            //-------------------------------------------------------------------
            $params["to_char(fecha_cotizacion,'DD-MM-YYYY HH:MI PM')"] = $value;
            //-------------------------------------------------------------------


            $params["CAST($t_persona.primer_nombre || ' ' || $t_persona.primer_apellido AS TEXT)"] = $value;
            $params["CAST($t_empresa.nombre AS TEXT)"] = $value;
            $params["CAST(codigo AS TEXT)"] = $value;
            $params["CAST($t_empleado.primer_nombre || ' ' || $t_empleado.primer_apellido AS TEXT)"] = $value;


            $this->_ilike($params, 'OR', TRUE);
        }

        $this->db->where("$t_libre_cotizacion._delete", '0');
    }

    /**
     * Provee las implementacion para realizar inserciones dentro de cotizacion libre
     * y las entidades dependientes de ella
     *
     * @access  public
     * @param   Array  $data datos a ser guardados dentro de la base de datos.
     * @return  Boolean Indicando el resultado de la operacion.
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V-1.0 08/01/14 03:26 PM
     */
    function insert($data) {
        $this->db->trans_start();
        $get_data_cotizacion = $this->insertWithReturning($data['cotizacion_art'], $this->getSchema() . ".libre_cotizacion");
        $id_cotizacion = $get_data_cotizacion['id'];

        //Almacena en business.libre_cotizacion_pago
        $data['cotizacion_pago']['id_cotizacion'] = $id_cotizacion;
        $data['cotizacion_pago']['created_by'] = $this->session->userdata('user_id');
        $this->db->insert('business.libre_cotizacion_pago', $data['cotizacion_pago']);

        //Almacena  en cotizacion_articulo con respecto a articulos
        $cotizacion_articulo = $data['cotizacion_articulo'];
        if ($cotizacion_articulo) {
            foreach ($cotizacion_articulo as $art => $articulo)
                $cotizacion_articulo[$art]['id_cotizacion'] = $id_cotizacion;
            $this->db->insert_batch('business.libre_cotizacion_articulo', $cotizacion_articulo);
        }
        $this->changeStatus($id_cotizacion, 'emitida', FALSE, "", FALSE);
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    /**
     * Dado un identificador obtiene los datos asociados al identifiador.
     *
     * @access  public
     * @param   Integer $id Identificador del registro a ser consultado.
     * @param   string $field campo por el cual se realizara la busqueda
     * @return  Array   Con los datos del registro consultado.
     * @author  Nohemi Rojas
     * @version 1.0 03/12/12 12:50 PM
     */
    public function getById($id, $field = 'id') {

        // Tables
        $table = $this->getSchema() . '.libre_cotizacion';
        $t_empleado = $this->getSchema() . '.empleado';
        $t_persona = $this->getSchema() . '.persona';
        $t_empresa = $this->getSchema() . '.empresa';


        $t_cotizacion_pago = $this->getSchema() . '.libre_cotizacion_pago';

        $t_cat_tipo_pago = 'dynamic.category';
        // Selects
        $this->db->select($table . '.*', FALSE);
        $this->db->select("COALESCE($t_empleado.primer_nombre || ' ' || $t_empleado.primer_apellido ) as vendedor", FALSE);
        $this->db->select("COALESCE($t_empleado.primer_nombre || ' ' || $t_empleado.primer_apellido ) as id_vendedor", FALSE);

        $this->db->select("
            (CASE WHEN $t_persona.id is not null THEN 'natural'
            WHEN $t_empresa.id is not null THEN 'juridica' END) as tipo_persona",FALSE);
        $this->db->select($t_cotizacion_pago . '.id_empresa');
        $this->db->select($t_cotizacion_pago . '.id_persona');
        $this->db->select($t_cotizacion_pago . '.monto');
        $this->db->select($t_cotizacion_pago . '.fecha_instrumento');
        $this->db->select($t_cotizacion_pago . '.cat_tipo_pago');
        $this->db->select($t_cotizacion_pago . '.numero_instrumento');
        $this->db->select($t_cotizacion_pago . '.cat_banco');
        $this->db->select($t_cotizacion_pago . '.vencimiento');
        $this->db->select($t_cotizacion_pago . '.deducible');
        $this->db->select("$table.id_cliente as id_persona_empresa");
        $this->db->select("$table.enviar");
        $this->db->select("COALESCE($t_persona.primer_nombre || ' ' || $t_persona.primer_apellido , $t_empresa.nombre) as id_cliente", FALSE);
        $this->db->select("COALESCE($t_persona.cedula , $t_empresa.rif) as cedula_rif", FALSE);
        // Froms and inners
        $this->db->from("$table");
        $this->db->join("$t_empleado", "$table.id_vendedor=$t_empleado.id LEFT ");
        $this->db->join($t_cotizacion_pago, "$t_cotizacion_pago.id_cotizacion=$table.id LEFT ");
        $this->db->join($t_cat_tipo_pago, "$t_cat_tipo_pago.id=$t_cotizacion_pago.cat_tipo_pago LEFT ");
        $this->db->join($t_persona, "$table.id_cliente=$t_persona.id LEFT ");
        $this->db->join($t_empresa, "$table.id_cliente=$t_empresa.id");
        $this->db->where("$table" . '.' . $field, $id);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row_array() : FALSE;
    }

    /**
     * Obtiene los articulos asociados a una cotizacion libre
     *
     * @access  public
     * @param   integer $id_cotizacion  Identificador de la cotizacion
     * @param   string  $category  Tipo de categoria
     * @param   string  $backup  Determina si el articulo es un backup
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 10/01/14 10:31 AM
     */
    function getArticulosByCotFree($id_cotizacion, $category = NULL, $backup = '0') {

        if ($id_cotizacion):
            //Tables
            $libre_cotizacion_articulo = $this->getSchema() . '.libre_cotizacion_articulo';
            $articulo = $this->getSchema() . '.articulo';
            $libre_cotizacion = $this->concatSchemaTable();
            $t_tramo_articulo = 'business.tramo_articulo';
            $table_category = 'dynamic.category';
            $this->db->select("(CASE WHEN (SELECT
                                            id_articulo
                                        FROM
                                            business.tramo_articulo
                                        WHERE
                                            id_articulo=$articulo.id
                                        GROUP BY id_articulo
                                        HAVING sum(cantidad) =0) is null THEN
                                                    'checked'
                                              ELSE
                                                    ''
                                              END) as checked", FALSE);
            //Query
            $this->db->select($articulo . '.id');
            $this->db->select($libre_cotizacion_articulo . '.id as id_cot_articulo');
            $this->db->select($libre_cotizacion_articulo . '.precio');
            $this->db->select($libre_cotizacion . '.monto_total AS monto');
            $this->db->select($libre_cotizacion_articulo . '.cantidad');
            $this->db->select($libre_cotizacion_articulo . '.precio');
            $this->db->select($articulo . '.modelo');
            $this->db->select($articulo . '.descripcion');
            $this->db->select($articulo . '.imagen');
            $this->db->from($libre_cotizacion_articulo);
            $this->db->join($articulo, "$articulo.id=$libre_cotizacion_articulo.id_articulo");
            $this->db->join($libre_cotizacion, "$libre_cotizacion.id=$libre_cotizacion_articulo.id_cotizacion");
            $this->db->join($table_category, "$articulo.tipo_articulo=$table_category.id  ");
            $this->db->where(
                    array(
                        "$libre_cotizacion_articulo.id_cotizacion" => $id_cotizacion,
                        "$libre_cotizacion_articulo.id_parent" => NULL));
            $query = $this->db->get();
            return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;

        else:
            return FALSE;
        endif;
    }

    /**
     * Obtiene los datos de los articulos seleccionados para cargarlos al pdf de cotizacion
     *
     * @access  public
     * @param   array  $param identificador de articulos
     * @return  array  $result
     * @author  Frederick Bustamante
     * @version V 1.0 16/01/14 11:27 AM
     */
    function getMaterialesPdf($param) {
        //Tables
        $articulo = $this->getSchema() . '.articulo';
        foreach ($param as $clave => $valor):
            //Query
            $query = $this->db->query("
                                SELECT
                                    id,
                                    '$valor[precio]' as precio,
                                    '$valor[cantidad]' as cantidad,
                                    modelo ,
                                    descripcion
                                FROM
                                    $articulo
                                WHERE
                                    id = $valor[id_articulo]");
            $result[] = $query->result_array();
        endforeach;
        return $result;
    }

    /**
     * funcion que recoge los datos para enviarlos a la generacion de pdf
     * @access public
     * @param integer $id_pago Identificador del tipo de pago
     * @param Id $name Description
     * @return array $response arreglo con los datos a cargar en el pdf
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 16/01/14 11:38 AM
     */
    public function getDatosPdf($id_pago = NULL, $id_vendedor = NULL, $id_cliente = NULL) {
        $tipo_pago = NULL;
        $vendedor = NULL;
        $cliente = NULL;
        if ($id_pago)
            $tipo_pago = $this->categories_model->getCategory(array('id' => $id_pago), '_label');

        if ($id_vendedor)
            $vendedor = $this->getVendedor($id_vendedor);

        if ($id_cliente)
            $cliente = $this->getDatacliente($id_cliente);


        return array(
            'tipo_pago' => ($tipo_pago ? $tipo_pago['_label'] : ''),
            'vendedor' => ($vendedor ? $vendedor : ''),
            'cliente' => ($cliente ? $cliente[0] : '')
        );
    }

    /**
     * Obtiene datos de un vendedor dado su identificador
     *
     * @access  public
     * @param   integer  $id_vendedor identificador del vendedor
     * @return  array    datos del vendedor
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 16/01/14 11:38 AM
     */
    public function getVendedor($id_vendedor) {
        $this->db->select("primer_nombre || ' ' || primer_apellido as vendedor");
        $this->db->from('business.persona');
        $this->db->where('id', $id_vendedor);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->row()->vendedor) : FALSE;
    }

    /**
     * Obtiene el maximo codigo a generar en la tabla cotizacion
     *
     * @access  public
     * @return  string  codigo maximo
     * @author  Nohemi Rojas
     * @version 09/01/13 11:06 AM
     */
    function generateCode() {
        $length = 15;
        $id_entity = $this->getEntityId('cotizacion');
        $current_value = $this->getCurrentSequentialCounter($id_entity);
        $code = ($current_value > 0) ? $current_value : str_repeat('0', $length);
        return @str_repeat('0', $length - strlen( ++$code)) . $code;
    }

    /**
     * Dado un arreglo se obtiene todas las coincidencias del arreglo por
     * clave/valor, donde clave es el campo la base de datos y valor el valor a
     * ser filtrado.
     *
     * @access  public
     * @param   Array $params <p>Contiene los pares clave valor a ser filtrados.</p>
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V - 1.0 09/10/13 11:54 AM
     */
    function getDatacliente($id_cliente = NULL) {
        if (!$id_cliente)
            return FALSE;
        //tables

        $t_persona = $this->getSchema() . ".persona";
        $t_empresa = $this->getSchema() . ".empresa";

        //Selects
        $this->db->distinct("COALESCE($t_persona.id, $t_empresa.id) as id", FALSE);
        $this->db->select("COALESCE($t_persona.primer_nombre || ' ' || $t_persona.primer_apellido , $t_empresa.nombre) as nombre_cliente", FALSE);
        $this->db->select("COALESCE($t_persona.letra || '-' || $t_persona.cedula, $t_empresa.rif) as cedula_rif", FALSE);

        //From and join
        $this->db->from("$t_persona");
        $this->db->from("$t_empresa");


        $this->db->where("$t_persona.id", $id_cliente);
        $this->db->where("$t_persona._delete", '0');

        $this->db->or_where("$t_empresa.id", $id_cliente);
        $this->db->where("$t_empresa._delete", '0');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    /**
     * Obtiene los articulos asociados a una cotizacion
     *
     * @access  public
     * @param   integer $id_cotizacion  Identificador de la cotizacion
     * @param   string  $category  Tipo de categoria
     * @param   string  $backup  Determina si el articulo es un backup
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 12/09/13 12:22 PM
     */
    function getArticulosByCot($id_cotizacion, $category, $backup = '0') {

        if ($id_cotizacion):
            //Tables
            $libre_cotizacion_articulo = $this->getSchema() . '.libre_cotizacion_articulo';
            $articulo = $this->getSchema() . '.articulo';
            $libre_cotizacion = $this->concatSchemaTable();
            $t_tramo_articulo = 'business.tramo_articulo';
            $table_category = 'dynamic.category';
            $this->db->select("(CASE WHEN (SELECT
                                            id_articulo
                                        FROM
                                            business.tramo_articulo
                                        WHERE
                                            id_articulo=$articulo.id
                                        GROUP BY id_articulo
                                        HAVING sum(cantidad) =0) is null THEN
                                                    'checked'
                                              ELSE
                                                    ''
                                              END) as checked", false);
            //Query
            $this->db->select($articulo . '.id');
            $this->db->select($libre_cotizacion_articulo . '.id as id_cot_articulo');
            $this->db->select($libre_cotizacion_articulo . '.precio');
            $this->db->select($libre_cotizacion . '.monto_total AS monto');
            $this->db->select($libre_cotizacion_articulo . '.cantidad');
            $this->db->select($libre_cotizacion_articulo . '.precio');
            $this->db->select($articulo . '.modelo');
            $this->db->select($articulo . '.descripcion');
            $this->db->select($articulo . '.imagen');
            $this->db->from($libre_cotizacion_articulo);
            $this->db->join($articulo, "$articulo.id=$libre_cotizacion_articulo.id_articulo");
            $this->db->join($libre_cotizacion, "$libre_cotizacion.id=$libre_cotizacion_articulo.id_cotizacion");
            $this->db->join($table_category, "$articulo.tipo_articulo=$table_category.id  ");
            $this->db->where(
                    array(
                        "$libre_cotizacion_articulo.id_cotizacion" => $id_cotizacion,
                        "$libre_cotizacion_articulo.id_parent" => NULL));
            $query = $this->db->get();
            return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;

        else:
            return FALSE;
        endif;
    }

    /**
     * Cambia el status de una cotizacion libre
     *
     * @access  public
     * @param   integer  $id_cotizacion Identificador de la cotizacion
     * @param   string   $status Nuevo Estatus de la cotizacion, debe ser el valor que esta
     *                  en categoria cotizacion_estatus
     * @param   Boolean $send_email Indica si se enviara email con el cambio de estatus
     * @return  Boolean Resultado de la operacion.
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 12/09/13 12:22 PM
     */
    function changeStatus($id_cotizacion, $status, $send_email = FALSE, $destination_path = '', $activate_mail = FALSE) {
        $attachment = '';
        $body = '';
        $status_name = mb_convert_case($status, MB_CASE_TITLE, "UTF-8");
        $status_id = $this->engine_model->getValueByCategory($status_name, '_label');

        if (!$status_id)
            return FALSE;

        $data = array('id_cotizacion' => $id_cotizacion, 'cat_libre_cotizacion_estatus' => $status_id, 'created_by' => $this->session->userdata('user_id'));

        $result_ope = $this->db->insert('business.libre_cotizacion_estatus', $data);

        $this->load->helper('view_engine');

        //si el resultado de la operacion es false, o si send_email es false
//        if ($result_ope)
//            writeUpdateTime();

        if (!$result_ope or !$send_email)
            return $result_ope;

        $code = $this->getCode($id_cotizacion);
        $html['datos'] = $this->getById($id_cotizacion);
        switch ($status_name) {
            case 'Emitida':
                $email = array($this->session->userdata('user_email'));
                //$email = 'epena@ast.com.ve' //cable ;
                $html['html'] = sprintf($this->lang->line('body_cot_emitida'), $code);
                $html['title'] = $this->lang->line('subject_cot_emitida');
                $body = $this->load->view('cotizacion/email_cotizacion', $html, TRUE);
                $subject = $this->lang->line('subject_cot_emitida');
                $attachment = $destination_path;

                break;
            case 'Procesada':
                //$email_medico = $this->getMedico($id_cotizacion)->correo_electronico;
                $email_medico = 'fbustamante@ast.com.ve'; //cable
                $email_user = $this->session->userdata('user_email');
                $email = array($email_user, $email_medico);
                $html['html'] = sprintf($this->lang->line('body_cot_procesada'), $code);
                $html['title'] = $this->lang->line('subject_cot_procesada');
                $body = $this->load->view('cotizacion/email_cotizacion', $html, TRUE);
                $subject = $this->lang->line('subject_cot_procesada');
                $attachment = $destination_path;
                break;
            case 'Procesada-Medio-Pago':
                $email = array($this->session->userdata('user_email'));
                $html['html'] = sprintf($this->lang->line('body_cot_procesada_medio_pago'), $code);
                $html['title'] = $this->lang->line('subject_cot_procesada_medio_pago');
                $body = $this->load->view('cotizacion/email_cotizacion', $html, TRUE);
                $subject = $this->lang->line('subject_cot_procesada_medio_pago');
                break;
            case 'Pre-Aprobada':
                $email = array($this->session->userdata('user_email'));
                $html['html'] = sprintf($this->lang->line('body_cot_pre_aprobada'), $code);
                $html['title'] = $this->lang->line('subject_cot_pre_aprobada');
                $body = $this->load->view('cotizacion/email_cotizacion', $html, TRUE);
                $subject = $this->lang->line('subject_cot_pre_aprobada');
                // se abre nortificacion a perfil logistica
                $cat_level_current = $this->engine_model->getValueByCategory('logistica', '_label');
                $id_validacion = $this->validacion_model->getFieldByName('cirugia_preaprobada');
                $this->notificationSend($id_cotizacion, $id_validacion, $cat_level_current);
                break;
            case 'Confirmada':
                $email = array($this->session->userdata('user_email'));
                $html['html'] = sprintf($this->lang->line('body_cot_confirmada'), $code);
                $html['title'] = $this->lang->line('subject_cot_confirmada');
                $body = $this->load->view('cotizacion/email_cotizacion', $html, TRUE);
                $subject = $this->lang->line('subject_cot_confirmada');
                break;
            case 'Reprogramada':
                $email = array($this->session->userdata('user_email'));
                $html['html'] = sprintf($this->lang->line('body_cot_reprogramada'), $code);
                $html['title'] = $this->lang->line('subject_cot_reprogramada');
                $body = $this->load->view('cotizacion/email_cotizacion', $html, TRUE);
                $subject = $this->lang->line('subject_cot_reprogramada');
                break;
            case 'Aprobada':
                $email = array($this->session->userdata('user_email'));
                $html['html'] = sprintf($this->lang->line('body_cot_aprobada'), $code);
                $html['title'] = $this->lang->line('subject_cot_aprobada');
                $body = $this->load->view('cotizacion/email_cotizacion', $html, TRUE);
                $subject = $this->lang->line('subject_cot_aprobada');
                break;
            case 'Reprogramar Cirugia':
                $vendedor = $this->getEmailVendedor($id_cotizacion);
                $email_vendedor = $vendedor[0]['correo_electronico'];
                $email_user = $this->session->userdata('user_email');
                $email = array($email_user, $email_vendedor);
                $html['html'] = sprintf($this->lang->line('body_cot_reprogramar'), $code);
                $html['title'] = $this->lang->line('subject_cot_reprogramar');
                $body = $this->load->view('cotizacion/email_cotizacion', $html, TRUE);
                $subject = $this->lang->line('subject_cot_reprogramar');
                break;
            case 'Sustituir Articulos':
                $vendedor = $this->getEmailVendedor($id_cotizacion);
                $email_vendedor = $vendedor[0]['correo_electronico'];
                $email_user = $this->session->userdata('user_email');
                $email = array($email_user, $email_vendedor);
                $html['html'] = sprintf($this->lang->line('body_cot_sustituir'), $code);
                $html['title'] = $this->lang->line('subject_cot_sustituir');
                $body = $this->load->view('email_cotizacion', $html, TRUE);
                $subject = $this->lang->line('subject_cot_sustituir');
                break;
            default:
                return FALSE;
                break;
        }

        //return sendEmail($email, $body, $subject, $body, $attachment, $activate_mail);
    }

    /**
     * Retorna los datos  principales de la cotizacion
     *
     * @access public
     * @param  integer $id Identificador de la cotizacion
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 13/01/14 01:34 PM
     */
    function get_data_cotizacion_free($id = NULL) {
        $t_libre_cotizacion = $this->getSchema() . ".libre_cotizacion";
        $t_cotizacion_pago = $this->getSchema() . '.libre_cotizacion_pago';
        $this->db->select($t_libre_cotizacion . '.*', false);
        $this->db->from("$t_libre_cotizacion");

        $this->db->where(array($t_libre_cotizacion . '.id' => $id));
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : false;
    }

    /**
     * Obtiene el status actual de la cotizacion libre
     *
     * @access  public
     * @param   integer  $id_cotizacion Identificador de la cotizacion libre
     * @return  array Resultado de la operacion.
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 12/09/13 12:22 PM
     */
    function getStatus($id_cotizacion) {
        if (!$id_cotizacion)
            return FALSE;

        //tables
        $libre_cotizacion_estatus = $this->getSchema() . '.libre_cotizacion_estatus';
        $t_category = 'dynamic.category';

        //selects
        $this->db->select("$libre_cotizacion_estatus.cat_libre_cotizacion_estatus AS status_id", FALSE);
        $this->db->select("$t_category._label AS status_name", FALSE);
        $this->db->from("$libre_cotizacion_estatus");
        $this->db->join("$t_category", "$libre_cotizacion_estatus.cat_libre_cotizacion_estatus=$t_category.id");

        //where
        $this->db->where("$libre_cotizacion_estatus.id_cotizacion", $id_cotizacion);
        $this->db->order_by("$libre_cotizacion_estatus.created_at", 'DESC');
        $this->db->order_by("$libre_cotizacion_estatus.cat_libre_cotizacion_estatus", 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row_array() : FALSE;
    }

    /**
     * Obtiene todos los status de la cotizacion libre
     *
     * @access  public
     * @param   integer  $id_cotizacion Identificador de la cotizacion
     * @return  array Resultado de la operacion.
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 12/09/13 12:22 PM
     */
    function getAllStatus($id_cotizacion) {
        if (!$id_cotizacion)
            return FALSE;

        //tables
        $libre_cotizacion_estatus = $this->getSchema() . '.libre_cotizacion_estatus';
        $t_category = 'dynamic.category';

        //selects
        $this->db->select("$libre_cotizacion_estatus.cat_libre_cotizacion_estatus AS status_id", FALSE);
        $this->db->select("$t_category._label AS status_name", FALSE);
        $this->db->select("$libre_cotizacion_estatus.created_at AS status_date", FALSE);
        $this->db->from("$libre_cotizacion_estatus");
        $this->db->join("$t_category", "$libre_cotizacion_estatus.cat_libre_cotizacion_estatus=$t_category.id");

        //where
        $this->db->where("$libre_cotizacion_estatus.id_cotizacion", $id_cotizacion);
        $this->db->order_by("$libre_cotizacion_estatus.created_at", 'DESC');
        $this->db->order_by("$libre_cotizacion_estatus.cat_libre_cotizacion_estatus", 'DESC');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

}
