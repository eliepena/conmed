<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Articulo_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad articulo
 * y sus entidades dependientes o extendidas
 *
 * @package Modules
 * @category Model
 * @author Nohemi Rojas. <frederickdanielb@gmail.com>
 * @version V-1.0 12/09/13 03:26 PM
 */
class Articulo_model extends AST_Model {

    private $table;

    public function __construct() {
        parent::__construct();
        $this->setSchema('business');
        $this->table = 'business.articulo';
        //$this->setSchema('business');
        $this->init('articulo');
    }

    /**
     * Lee el directorio que contiene las imagenes de instrumentos
     * y actualiza las imagenes de los registros de la entidad instrumentos
     *
     * @author  Nohemi Rojas
     * @version 1.0 08/04/13 10:48 AM
     */
    public function updateImageByDir() {

        $this->db->select('modelo , id');
        $query = $this->db->get($this->table);
        $results = $query->result_array();
        $path = realpath('uploads') . '/images/instrumentos/';

        foreach ($results as $result) {
            $imagen = NULL;
            //Diferencia entre minusculas-mayusculas / jpg-png
            if (file_exists($path . strtolower($result['modelo']) . '.jpg'))
                $imagen = strtolower($result['modelo']) . '.jpg';
            elseif (file_exists($path . strtolower($result['modelo']) . '.png'))
                $imagen = strtolower($result['modelo']) . '.png';
            elseif (file_exists($path . strtoupper($result['modelo']) . '.jpg'))
                $imagen = strtoupper($result['modelo']) . '.jpg';
            elseif (file_exists($path . strtoupper($result['modelo']) . '.png'))
                $imagen = strtoupper($result['modelo']) . '.png';
            if ($imagen)
                $this->update($result['id'], array('imagen' => $imagen), 'id');
        }
    }

    /**
     * Obtiene la lista de articulos que estan relacionados con una cirugia
     *
     * Si se envia como parametros false, false  retorna todo los articulos by carlos bello
     *
     * @param integer  $cirugia_id Identificador del tipo de cirugia
     * @return  mixed Array de los valores de los registros, false en caso de que no encuentre
     * @author  Frederick D. Bustamante G.
     * @version V-1.0 03/12/12 12:50 PM
     */
    public function articuloRecomendacion($cirugia_id = '', $tipo_recomendacion = '') {

        if ($cirugia_id == false && $tipo_recomendacion == false) {
            //Este if se coloco para que no entrara en el if de abajo
        } else if (empty($cirugia_id) || empty($tipo_recomendacion))
            return FALSE;

        $t_articulo = $this->concatSchemaTable();
        $t_articulo_recomendacion = 'business.articulo_recomendacion';
        $t_articulo_precio = 'business.articulo_precio';
        $t_cirugia = 'business.cirugia';
        $t_category = 'dynamic.category';

        $this->db->distinct("$t_articulo.id");
        $this->db->select("$t_articulo.id");
        $this->db->select("$t_articulo.modelo");
        $this->db->select("$t_articulo.descripcion");
        $this->db->select("$t_articulo.imagen");
        $this->db->select("$t_articulo_recomendacion.cantidad");
        $this->db->select("$t_articulo.stock as disponibilidad");
        $this->db->select("
                            array(
                                    SELECT
                                        precio
                                    FROM
                                        business.articulo_precio
                                    WHERE
                                        id_articulo = $t_articulo.id
                                    ORDER BY
                                        $t_articulo_precio.id_precio) AS arr_precio", FALSE);
        $this->db->select("
                            array(
                                    SELECT
                                        id_precio
                                    FROM
                                        business.articulo_precio
                                    WHERE
                                        id_articulo = $t_articulo.id
                                    ORDER BY
                                        $t_articulo_precio.id_precio) AS  arr_id_precio", FALSE);
        $this->db->from($t_articulo_recomendacion);
        $this->db->join($t_articulo, "$t_articulo_recomendacion.id_articulo = $t_articulo.id");
        $this->db->join($t_cirugia, "$t_articulo_recomendacion.id_cirugia = $t_cirugia.id");
        $this->db->join($t_category, "$t_articulo_recomendacion.tipo_recomendacion = $t_category.id");
        if ($cirugia_id == false && $tipo_recomendacion == false) {
            $this->db->select("$t_articulo_recomendacion.id_cirugia");
            $this->db->select("$t_category._label");
            $this->db->where(
                    array(
                        "$t_category._table" => 'tipo_recomendacion'
                    )
            );
        } else {
            $this->db->where(
                    array(
                        "$t_cirugia.id" => $cirugia_id,
                        "$t_category._label" => $tipo_recomendacion,
                        "$t_category._table" => 'tipo_recomendacion'
                    )
            );
        }
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

    function get_articles() {
        $this->db->from('business.view_articles');
        $this->db->order_by('id', 'asc');
        // $this->db->limit(50);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    /**
     * Obtiene las cabeceras de los articulos.
     * @return array headers
     * @version 1.0 15/08/2013 16:55
     * @author Jose Rodriguez <josearodrigueze@gmail.com>
     */
    function get_headers_articles() {
        $query = $this->db->query('SELECT column_name AS "sTitle" FROM information_schema.columns WHERE table_name =\'view_articles\' AND table_schema = \'business\' ORDER BY ordinal_position');
        return $query->result_array();
    }

}
