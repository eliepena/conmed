<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Validacion_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad validacion
 * y sus entidades dependientes o extendidas como cotizacion
 *
 * @package Modules
 * @category Model
 * @author Nohemi Rojas. <nohemir@gmail.com>
 * @version V-1.0 22/01/13 11:40 AM
 */
class Validacion_model extends AST_Model {

    private $table;

    public function __construct() {
        $this->setSchema('business');
        parent::__construct();
        $this->table = 'business.validacion';
    }

    /**
     * Obtiene las reglas de validacion creadas en la entidad validacion
     *
     * @access  public
     * @return  Array Con los registros encontrados
     * @author  Nohemi Rojas
     * @version V-1.0 21/01/13 12:52 PM
     */
    function getAll($is_validation = true) {
        $this->db->select('campo, id');
        $this->db->where(array("is_validation" => ($is_validation ? '1' : '0')));
        $query = $this->db->get($this->table);
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

    /**
     * Obtiene las reglas de validacion especificadas para una cotizacion
     *
     * @access  public
     * @param integer $id_cotizacion Identificador de la cotizacion
     * @return  Array Con los registros encontrados
     * @author  Nohemi Rojas
     * @version V-1.0 21/01/13 12:52 PM
     */
    function getAllByCot($id_cotizacion, $nivel) {

        $this->db->select('val.campo,cot_val.cat_val_observacion');
        $this->db->from('business.cotizacion_validacion AS cot_val');
        $this->db->join('business.validacion AS val', 'cot_val.id_validacion = val.id');
        $this->db->join('dynamic.category AS cat', 'cot_val.cat_val_nivel= cat.id', 'left');
        $this->db->where('id_cotizacion', $id_cotizacion);
        $this->db->where('abierto', '1');
        $this->db->where('cat._label', $nivel);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    /**
     * Se encarga de obtener los campos para la operacion que se esta ejecutando
     * y los que fueron definidos en la entidad cotizacion_validacion
     * Esta operacion reemplaza getFields de View_Engine
     *
     * @access	public
     * @return  array arreglo con los resultados del proceso
     * @author  Nohemi Rojas
     * @version V-1.0 23/01/13 12:44 PM
     */
    function getFields($id_cotizacion, $nivel) {

        $fields_db = $this->view_engine->getFields();

        $fields_val = $this->getAllByCot($id_cotizacion, $nivel);

        if (!$fields_val)
            return FALSE;

        $arr_field = array();

        foreach ($fields_val as $val) {
            foreach ($fields_db as $db) {
                if (strstr($db['name'], $val['campo']))
                    $arr_field[$db['_order']] = $db;
            }
        }

        ksort($arr_field);
        $this->setFormCategory($arr_field, $id_cotizacion);

        return $arr_field;
    }

    /**
     * Se encarga de recorrer un arreglo de campos para verificar cual de ellos es
     * de tipo categoria, en caso de que lo sean, buscara el valor de dicha categoria
     * para colocarlo como parte del arreglo
     * Esta operacion reemplaza setFormCategory de View_Engine
     * @access	private
     * @param array $fields Arreglo con los campos a mostrar en el form
     * @author  Nohemi Rojas
     * @version V-1.0 23/01/13 12:44 PM
     */
    private function setFormCategory(&$fields, $id_cotizacion) {
        if (!$fields)
            return;

        foreach ($fields as $key => $field) {
            //estos campos son particuales asi que contienen _cat y no cat_
            if ($field['type'] == 'select' and strstr($field['name'], '_cat')) {
                $fields[$key]['value'] = $this->engine_model->getCategorys('val_observacion');
                $fields[$key]['cat_parent'] = $this->engine_model->getParentCategory('val_observacion');
            }
            if ($field['type'] == 'label')
                $fields[$key]['history'] = $this->getHistoryByField($id_cotizacion, preg_replace("/_label/i", '', $field['name']));
        }
    }

    /**
     * Se encarga de retornar el id del campo registrado en la entidad validacion, dado su nombre
     *
     * @access	public
     * @param string $fields Nombre del campo
     * @return mixed Identificador del campo / FALSE
     * @author  Nohemi Rojas
     * @version V-1.0 23/01/13 12:44 PM
     */
    public function getFieldByName($field) {
        $this->db->select('id');
        $this->db->where('campo', $field);
        $query = $this->db->get($this->table);
        return ($query->num_rows() > 0) ? ($query->row()->id) : FALSE;
    }

    //por evaluar
    public function getHistoryByField($id_cotizacion, $field) {

        $this->db->select('cat_val_observacion');
        $this->db->from('business.cotizacion_validacion AS cot_val');
        $this->db->join('business.validacion AS val', 'cot_val.id_validacion = val.id');
        $this->db->join('dynamic.category AS cat', 'cat_val_observacion = cat.id', 'left');
        $this->db->where('id_cotizacion', $id_cotizacion);
        $this->db->where('campo', $field);
        $this->db->where('abierto', '0');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

}