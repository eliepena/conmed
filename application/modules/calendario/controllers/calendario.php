<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Descripcion de calendario
 *
 * @package conmed.application.modules
 * @subpackage calendario
 * @category Controlador
 * @author Jose A. Rodriguez E. <josearodrigueze@gmail.com>
 * @version V - 1.0 25-01-2013 14:17:57
 */
class Calendario extends AST_Controller {

    function __construct() {
        parent::__construct();
    }

    /**
     * Muestra imprime el calendario en pantalla si hay parametros GET.
     * Si GET, imprime JSON de los eventos.
     *
     * @access  public
     * @author  Jose A. Rodriguez E. <josearodrigueze@gmail.com>
     * @version V 1.0 23/04/2013 12:39:22 pm
     */
    function ver() {
        //Obtenemos la informacion pertinente a la peticion a fin de realizar
        //las validaciones necesarias
        $get_params = $this->input->get();

        //Evaluamos si la peticion posee parametros GET
        //Evaluamos si se esta consultado eventos.
        if (!empty($get_params) AND $get_params['return'] == 'events') {

            $this->offOutput();
            // echo json_encode($this->_getEvents($get_params['lastTimeUpdated']));
            echo json_encode($this->_getEvents());
        }

        //Si no se cumplen ninguna de la anteriores se esta pidiendo la vista.
        else {
            $params = array(
//                'save_url' => base_url('calendario/EXC_save'),
                'frequency' => 10, //Segundos
                'render' => $this->load->view('render.js.php', NULL, TRUE)
            );

            $this->setContent($this->view_engine->calendar($params));
            $this->setTitle('Calendario');
            $this->setTargetOn('content');
        }
    }

    /**
     * Consulta los eventos programados en el calendario a parit de una fecha data.
     *
     * @access  private
     * @param   timestamp   $lastTimeUpdated
     * @return  array    datos a se mostrados en el calendario.
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 28/01/13 14:55
     */
    private function _getEvents($lastTimeUpdated = 0) {
        //@todo posible soporte a agregar solo los eventos nuevos.
//        array(
//            'id' => 1,
//            'title' => 'Echo en PHP', {required}
//            'start' => '', {required}
//            'end',
//            'url',
//            'className',
//            'editable',
//            'color',
//            'backgroundColor',
//            'borderColor',
//            'textColor'
//        );
        $lastTimeUpdated = date('Y-m-d H:i:s', ($lastTimeUpdated / 1000));
        $events = $this->Calendario_model->getEvents($lastTimeUpdated);
        if ($events) {
            //@to-do Colocar esta URL configurable
//            $url = base_url('cotizacion/cotizacion_free/detail');
            foreach ($events as &$event) {
                $event['id'] = $this->encrypt->encode($event['id']);
//                $event['_url'] = $url;
            }
        }
        return $events;
    }

    /**
     * Almacena los eventos modificados en el calendario.
     *
     * @access  private
     * @param   array   $event  datos del evento a ser actualizado.
     * @return  bool    resultado de la operacion de guardado.
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 28/01/13 15:46
     */
    private function _updateEvent($event) {
        $this->setContent('<pre>' . print_r($this->input->post(), TRUE) . '</pre>');
        $this->setTitle('Update Evento');
        $data = TRUE;
        return $data;
    }

    /**
     * Formatea el atributo URL de los eventos para que una presionado el evento
     * nos muestre la informacion detallada del mismo.
     *
     * Basicamente retorna el siguiente string con loas valores sustituidos
     * javascript:request({no_remove:true, url:'%s', data:{id_ec:'%s'}})
     *
     * @access  private
     * @param   string    $id  Identificador unico del evento.
     * @param   string    $url URL a ser llamada al presionar el evento.
     * @return  string    llamado a funcion js.
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 29/01/13 9:54
     */
    private function _formatEventURL($id, $url) {
        // Llamado js a la funcion request() de view_engine.js
        $format = "javascript:request({no_remove:true,url:'%s',data:{target_on:'window',id:'%s'}})";
        $id = $this->encrypt->encode($id);
        return sprintf($format, $url, $id);
    }

    function EXC_save($params = NULL) {

        if (empty($params)) {
            $this->setContent('<pre>' . print_r($this->input->post(), TRUE) . '</pre>');
            $this->setTitle('Guardar Evento');
        } else {
            $this->setContent($this->input->post('id_ec'));
            $this->setTitle('Link ID Evento');
        }
    }

}
