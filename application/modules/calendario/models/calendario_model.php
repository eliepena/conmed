<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Descripcion de calendario_model
 *
 * @package conmed.application.modules
 * @subpackage calendario
 * @category Modelo
 * @author Jose A. Rodriguez E. <josearodrigueze@gmail.com>
 * @version V - 1.0 25-01-2013 14:17:57
 */
class Calendario_model extends AST_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Obtiene los eventos a ser mostrados dentro del calendario.
     * a partir de la ultima fecha de actualizacion.
     * EVENTOS = CIRUGIAS.
     *
     * @access  public
     * @param   timestamp   $lastTimeUpdate
     * @return  array    Eventos
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 31/01/13 9:51
     */
    public function getEvents($lastTimeUpdate = '1900-01-01') {

        $events = array(
            array('status' => 'Emitida', 'theme' => array('color' => 'grey', 'icon' => 'icon-list-alt')),
            array('status' => 'Procesada', 'theme' => array('color' => 'grey', 'icon' => 'icon-list-alt')),
            array('status' => 'Procesada-Medio-Pago', 'theme' => array('color' => 'grey', 'icon' => 'icon-list-alt')),
            array('status' => 'Pre-Aprobada', 'theme' => array('color' => '#0044cc', 'icon' => 'icon-heart')),
            array('status' => 'Aprobada', 'theme' => array('color' => 'green', 'icon' => 'icon-ok')),
            // array('status' => 'Confirmada', 'theme' => array('color' => 'green', 'icon' => 'icon-ok')),
            array('status' => 'Reprogramada', 'theme' => array('color' => 'orange', 'icon' => 'icon-calendar')),
            array('status' => 'Reprogramar Cirugia', 'theme' => array('color' => 'red', 'icon' => 'icon-calendar')),
            array('url' => base_url('cotizacion/cotizacion_free/detail')),
            array('url' => base_url('cotizacion/cotizacion/detail'))
        );

        $rs = $this->db->query("
            SELECT
                cot.id,
                cot.codigo,
                category._label,
                cot._url,
                CASE
                    WHEN (category._label ilike '{$events[0]['status']}' OR category._label ilike '{$events[1]['status']}' OR category._label ilike '{$events[2]['status']}') THEN cot.start
                    ELSE cast(cot.fecha_cirugia as timestamp without time zone)
                END AS start,
                CASE
                    WHEN (category._label ilike '{$events[0]['status']}' OR category._label ilike '{$events[1]['status']}' OR category._label ilike '{$events[2]['status']}') THEN to_char(cot.start, 'HH24:MI')::time
                    ELSE cast(cot.hora_cirugia as time)
                END AS hora,
                CASE
                    WHEN (category._label ilike '{$events[0]['status']}' OR category._label ilike '{$events[1]['status']}' OR category._label ilike '{$events[2]['status']}') THEN 'Cotizacion '
                    ELSE 'Cirugia '
                END  || category._label AS title,
                CASE
                    WHEN category._label ilike '{$events[0]['status']}' THEN '{$events[0]['theme']['icon']}'
                    WHEN category._label ilike '{$events[1]['status']}' THEN '{$events[1]['theme']['icon']}'
                    WHEN category._label ilike '{$events[2]['status']}' THEN '{$events[2]['theme']['icon']}'
                    WHEN category._label ilike '{$events[3]['status']}' THEN '{$events[3]['theme']['icon']}'
                    WHEN category._label ilike '{$events[4]['status']}' THEN '{$events[4]['theme']['icon']}'
                    WHEN category._label ilike '{$events[5]['status']}' THEN '{$events[5]['theme']['icon']}'
                    WHEN category._label ilike '{$events[6]['status']}' THEN '{$events[6]['theme']['icon']}'
                END AS icon,
                CASE
                    WHEN category._label ilike '{$events[0]['status']}' THEN '{$events[0]['theme']['color']}'
                    WHEN category._label ilike '{$events[1]['status']}' THEN '{$events[1]['theme']['color']}'
                    WHEN category._label ilike '{$events[2]['status']}' THEN '{$events[2]['theme']['color']}'
                    WHEN category._label ilike '{$events[3]['status']}' THEN '{$events[3]['theme']['color']}'
                    WHEN category._label ilike '{$events[4]['status']}' THEN '{$events[4]['theme']['color']}'
                    WHEN category._label ilike '{$events[5]['status']}' THEN '{$events[5]['theme']['color']}'
                    WHEN category._label ilike '{$events[6]['status']}' THEN '{$events[6]['theme']['color']}'
                END AS color
            FROM(
                SELECT DISTINCT
                    cotizacion.id,
                    cotizacion.codigo,
                    '{$events[8]['url']}' as _url,
                    cotizacion.created_at AS start,
                    cast (cotizacion.fecha_cirugia as text),
                    cast (cotizacion.hora_cirugia as text),
                    (SELECT cat_cotizacion_estatus FROM business.cotizacion_estatus
                    WHERE id_cotizacion = cotizacion.id
                    ORDER BY id DESC LIMIT 1) AS status_id
                FROM business.cotizacion
                    JOIN business.cotizacion_estatus ON cotizacion.id = cotizacion_estatus.id_cotizacion
                WHERE cotizacion_estatus.created_at > '$lastTimeUpdate'
                GROUP BY
                    cotizacion.id,
                    cotizacion.codigo,
                    cotizacion.created_at,
                    cotizacion.fecha_cirugia,
                    cotizacion.hora_cirugia
                    
UNION

                SELECT DISTINCT
                    libre_cotizacion.id,
                    libre_cotizacion.codigo,
                    '{$events[7]['url']}' as _url,
                    libre_cotizacion.created_at AS start,
                     '' as  fecha_cirugia,
                    '' as hora_cirugia,
                    (SELECT cat_libre_cotizacion_estatus FROM business.libre_cotizacion_estatus
                    WHERE id_cotizacion = libre_cotizacion.id
                    ORDER BY id DESC LIMIT 1) AS status_id
                FROM business.libre_cotizacion
                    JOIN business.libre_cotizacion_estatus ON libre_cotizacion.id = libre_cotizacion_estatus.id_cotizacion
                WHERE libre_cotizacion_estatus.created_at > '$lastTimeUpdate'
                GROUP BY
                    libre_cotizacion.id,
                    libre_cotizacion.codigo,
                    libre_cotizacion.created_at
            ) cot JOIN dynamic.category ON cot.status_id = category.id");
        // echo '/*LastQuery:',$this->db->last_query(),'*/';
        return ($rs->num_rows() > 0) ? $rs->result_array() : FALSE;
    }
}