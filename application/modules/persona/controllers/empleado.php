<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Empleado
 *
 * Esta clase provee la implementacion para la capa de control de
 * acciones de la entidad empleado y sus entidades dependientes o extendidas
 *
 * @package Module
 * @category Controller
 * @version V-1.0 29/11/12 01:26 PM
 */
class Empleado extends AST_Controller {

    public function __construct() {
        parent::__construct();
    }

    function _avancedFormat($format_type, $data) {
        switch ($format_type) {
            case 'INSERT':
                $data['validado'] = '1';
                break;
            default:
                break;
        }
        return $data;
    }

}