<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Instrumentista
 *
 * Esta clase provee la implementacion para la capa de control de
 * acciones de la entidad instrumentista y sus entidades dependientes o extendidas
 *
 * @package Module
 * @category Controller
 * @version V-1.0 26/09/13 04:36 PM
 */
class Instrumentista extends AST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('instrumentista_model');
        $this->config->load('instrumentalist');
    }

    /**
     * Provee el acceso al formulario de ingreso de nuevos datos, asi mismo,
     * provee el acceso a la insercion de datos nuevos dentro en base de datos.
     *
     * @access public
     * @param array $params Parametros adicionales enviados al controlador.
     * @author Eliezer A. Pe#a
     * @version V 1.0 30/09/2013 11:30 am
     */
    function create($params) {
        if (!$this->input->post()) {

            $this->setContent($this->view_engine->form());
        } elseif ($params[0] == 'process') {
            $process = $this->view_engine->process();
            $success = FALSE;
            if ($process['result']) {
                $params = array('format_type' => 'insert', 'data' => $process['data']);
                $especialidad = $params['data']['Especialidad'];
                //separar los datos de especialidad de los datos del instrumentista
                foreach ($especialidad as $key => $value) {
                    $especialidad[$key] = array('id_especialidad' => $value, 'created_by' => $this->session->userdata('user_id'));
                }
                $params['data']['created_by'] = $this->session->userdata('user_id');
                unset($params['data']['Especialidad']);
                $params['especialidad'] = $especialidad;
                $data = $params;
                $success = $this->instrumentista_model->insert($data);
                $this->setContent($success);
            } else {
                $this->setContent($process['validation_error']);
            }

            //seteamos los datos de auditoria
            $this->audit->setOperationData($process);
            $this->audit->setChkResult($success);
        }
    }

    /**
     * Edit de Instrumentista.
     *
     * @access public
     * @param array $data contiene los datos del articulo.
     * @param integer $id contiene el idnetificador del articulo
     * @author Eliezer Pe#a
     * @version V 1.0 30/09/2013
     */
    function edit($params) {
        $array_post = $this->input->post();
        $id = NULL;
        if (empty($params) AND isset($array_post['id'])) {
            $id = $this->encrypt->decode($array_post['id']);
            $data = $this->instrumentista_model->getById($id);
            $esp = $this->instrumentista_model->getEspecialidad($id);
            $data['Especialidad'] = $esp;
            $view['data'] = $data;
//            echo '<pre>',  print_r($data),'</pre>';
            $this->session->set_userdata('instrumentista.id', $id);
            $this->setContent($this->view_engine->form(array('data' => $data)), $view, TRUE);
        } elseif ($params[0] == 'process') {
            $process = $this->view_engine->process();
            $success = FALSE;
            $id = $this->session->userdata('instrumentista.id');
            if ($process['result']) {
                $params = array('format_type' => 'update', 'data' => $process['data']);
                $especialidad = $params['data']['Especialidad'];
                //separar los datos de especialidad de los datos del instrumentista
                foreach ($especialidad as $key => $value) {
                    $especialidad[$key] = array('id_especialidad' => $value, 'created_by' => $this->session->userdata('user_id'));
                }
                $params['data']['created_by'] = $this->session->userdata('user_id');
                unset($params['data']['Especialidad']);
                $params['especialidad'] = $especialidad;
                $data = $params;
                $success = $this->instrumentista_model->edit($id, $data);
                $this->setContent($success);

                if ($success) {
                    $this->session->unset_userdata('instrumentista.id');
                    //$this->updateArticles();
                }
            } else {
                $this->setContent($process['validation_error']);
            }
            //seteamos los datos de auditoria
            $operation_data = $process;
            $this->audit->setChkResult($success);
        }
        //seteamos los datos de auditoria
        $operation_data['id'] = $id;
        $this->audit->setOperationData($operation_data);
    }

    /**
     * Muestra el detalle un registro de instrumentista.
     *
     * @access public
     * @author Eliezer Pe#a
     * @version V 1.0 30/09/2013
     */
    function detail() {
        $array_post = $this->input->post();
        $id = $this->encrypt->decode($array_post['id']);

        if (!empty($id)) {
            $this->session->set_userdata("medico.id", $id);
            $data = $this->Instrumentista_model->getById($id);
            $esp = $this->instrumentista_model->getNameEspecialidad($id);
            $data['Especialidad'] = $esp;
            $view['data'] = $data;
            $view = $this->view_engine->form($view);
            $this->setContent($view);
        }
        //seteamos los datos de auditoria
        $this->audit->setOperationData(array('id' => $id));
    }

    /**
     * Provee la agenda del instrumentista
     *
     * @access  public
     * @param   array $param Parametros de filtros para la busqueda.
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 01/10/13 12:34 PM
     */
    function EXC_diaryInstrumentalist($param) {
        $this->offOutput();

        //Inicializacion de variables
        $array_post = $this->input->post();


        $cantidad_cirugias = $this->config->item('cantidad_cirugias'); //0;
        $tope_cirugias_por_dia = $this->config->item('tope_cirugias_por_dia'); //2;
        $tope_horas_entre_cirugias = $this->config->item('tope_cirugias_por_dia'); //4;
        $fecha_cirugia_cotizacion = NULL;
        $hora_cirugia_cotizacion = NULL;
        $detail_medico = '';
        $detail = '';
        $list = '';

        //Agenda de Cotizacion para cirugia y id del medico
        $data['diaryQuote'] = $this->instrumentista_model->diaryQuote($this->session->userdata("Cotizacion_model.id"));

        if ($data['diaryQuote']):
            $fecha_cirugia_cotizacion = $data['diaryQuote'][0]['fecha_cirugia'];
            $hora_cirugia_cotizacion = $data['diaryQuote'][0]['hora_cirugia'];

            if (isset($array_post['id_instrumentista']) && !empty($array_post['id_instrumentista'])):
                $data_medico['concurrencesWithMedico'] = $this->instrumentista_model->workingConcurrencesWithMedico($data['diaryQuote'][0]['id_medico']);
                $data_medico['id_instrumentista'] = $array_post['id_instrumentista'];
                $data_medico['medico'] = $data['diaryQuote'][0]['medico'];
                $detail_medico = $this->load->view('instrumentista/detail_medico', $data_medico, TRUE);
            endif;

        endif;

        if (isset($array_post['id_instrumentista']) && !empty($array_post['id_instrumentista'])):
            $data ['detail'] = $this->instrumentista_model->getInstrumentalistsAvailable($array_post, TRUE);
            $detail = $this->load->view('instrumentista/detail_instrumentista', $data, TRUE);
        endif;

        if (isset($array_post['id_articulacion']) && !empty($array_post['id_articulacion'])):


            //Agenda de instrumentista
            $data ['data'] = $this->instrumentista_model->getInstrumentalistsAvailable($array_post, FALSE, TRUE);

            if ($data['data']):
                foreach ($data ['data'] as $indice => $valor):

                    $cotInstru = $this->instrumentista_model->getInstrumentalistsAvailableCot($valor['id'], $data['diaryQuote'][0]['id']);

                    //Se verifica si el instrumentista posee agenda con la cotizacion consultada
                    if ($cotInstru):
                        unset($data['data'][$indice]);
                    else:
                        $cirugias_instrumentista = $this->instrumentista_model->getInstrumentalistsAvailable(array('id_instrumentista' => $valor['id']), TRUE);
                        if ($cirugias_instrumentista):
                            foreach ($cirugias_instrumentista as $key => $value):
                                $time = $this->diffBetweenTwoTimes($hora_cirugia_cotizacion, $value['hora_cirugia']);
                                if ($value['fecha_cirugia'] == $fecha_cirugia_cotizacion):
                                    if ($cantidad_cirugias >= $tope_cirugias_por_dia):
                                        $data['data'][$indice]['disponibilidad'] = FALSE;
                                    elseif ($time['type_time'] == 'hour' && $time['time'] > $tope_horas_entre_cirugias):
                                        $data['data'][$indice]['disponibilidad'] = TRUE;
                                    else:
                                        $data['data'][$indice]['disponibilidad'] = FALSE;
                                    endif;
                                    $cantidad_cirugias++;
                                else:
                                    $data['data'][$indice]['disponibilidad'] = TRUE;
                                endif;
                            endforeach;
                        else:
                            $data['data'][$indice]['disponibilidad'] = TRUE;
                        endif;

                    endif;
                endforeach;
            endif;
            $list = $this->load->view('instrumentista/list_instrumentista', $data, TRUE);
        endif;
        $response = array(
            'success' => TRUE,
            'list' => $list,
            'detail' => $detail,
            'detail_medico' => $detail_medico
        );


        print(json_encode($response));
    }

    /**
     * Determina la diferencia de tiempo entre dos horas
     *
     * @access  public
     * @param   string $hora1 Primera hora de comparacion
     * @param   string $hora1 Segunda hora de comparacion
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 04/10/13 11:51 AM
     */
    function diffBetweenTwoTimes($hora1, $hora2) {
        $response = array('type_time' => NULL, 'time' => 0);
        $separar[1] = explode(':', $hora1);
        $separar[2] = explode(':', $hora2);

        $total_minutos_trasncurridos[1] = ($separar[1][0] * 60) + $separar[1][1];
        $total_minutos_trasncurridos[2] = ($separar[2][0] * 60) + $separar[2][1];
        $total_minutos_trasncurridos = $total_minutos_trasncurridos[1] - $total_minutos_trasncurridos[2];
        if ($total_minutos_trasncurridos <= 59) {
            $response = array('type_time' => 'minute', 'time' => $total_minutos_trasncurridos);
            return $response;
        } elseif ($total_minutos_trasncurridos > 59) {
            $HORA_TRANSCURRIDA = round($total_minutos_trasncurridos / 60);
            if ($HORA_TRANSCURRIDA <= 9)
                $HORA_TRANSCURRIDA = '0' . $HORA_TRANSCURRIDA;
            $MINUITOS_TRANSCURRIDOS = $total_minutos_trasncurridos % 60;
            if ($MINUITOS_TRANSCURRIDOS <= 9)
                $MINUITOS_TRANSCURRIDOS = '0' . $MINUITOS_TRANSCURRIDOS;
            $response = array('type_time' => 'hour', 'time' => $HORA_TRANSCURRIDA);
            return $response;
        }
    }

}