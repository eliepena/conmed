<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Persona
 *
 * Esta clase provee la implementacion para la capa de control de
 * acciones de la entidad persona y sus entidades dependientes o extendidas
 *
 * @package Module
 * @category Controller
 * @author Nohemi Rojas. <nohemir@gmail.com>
 * @version V-1.0 29/11/12 01:26 PM
 */
class Persona extends AST_Controller {

    public function __construct() {
        parent::__construct();
    }
    
        /**
     * Provee el acceso al formulario de ingreso de nuevos datos, asi mismo,
     * provee el acceso a la insercion de datos nuevos dentro en base de datos.
     *
     * @access public
     * @param array $params Parametros adicionales enviados al controlador.
     * @author Frederick Bustamante <frederickdanielb@gmail.com>
     * @version V 1.0 11/07/13 05:26 PM
     */
    function create($params = NULL) {
        if (empty($params)) {
            $view ['view'] = $this->view_engine->form();
            //se carga  una nueva vista para aplicar eventos personalizados
            $view = $this->load->view('persona/persona/create', $view, TRUE);
            $this->setContent($view);
        } elseif ($params[0] == 'process') {
            $process = $this->view_engine->process();
            $insert = FALSE;
            if ($process['result']) {
                $params = array('format_type' => 'insert', 'data' => $process['data']);

                $data = $this->_format($params);

                $insert = $this->Persona_model->insertWithReturning($data);
                if ($insert):
                    //variables para personalizar la vista luego de la insercion
                    $json = array(
                        'view' => TRUE,
                        'id_persona' => $insert['id'],
                        'cedula' => $data['cedula'],
                        'nombre' => $data['primer_nombre'],
                        'apellido' => $data['primer_apellido']
                    );
                else:
                    $json = array(
                        'view' => FALSE
                    );
                endif;
                $this->setTargetOn('json');

                $this->setContent($json);
            } else {
                $this->setContent($process['validation_error']);
            }
            // -----------------------------
            //seteamos los datos de auditoria
            $this->audit->setOperationData($process);
            $this->audit->setChkResult($insert);
            // -----------------------------
        }
    }
}