<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Esta clase provee la implementacion para la capa de control de
 * acciones de la entidad medico y sus entidades dependientes o extendidas
 *
 * @package Module
 * @category Controller
 * @author Nohemi Rojas. <nohemir@gmail.com>
 * @version V-1.0 29/11/12 01:26 PM
 */
class Medico extends AST_Controller {

    public function __construct() {
        parent::__construct();
          $this->load->model('medico_model', 'medico_model');
    }

    function _avancedFormat($format_type, $data) {
        switch ($format_type) {
            case 'INSERT':
                $data['validado'] = '1';
                break;
        }
        return $data;
    }

    /**
     * Provee el acceso al formulario de ingreso de nuevos datos, asi mismo,
     * provee el acceso a la insercion de datos nuevos dentro en base de datos.
     *
     * @access public
     * @param array $params Parametros adicionales enviados al controlador.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 24/04/2013 05:57:57 pm
     */
    function create($params = NULL) {
        if (!$this->input->post()) {
            $view = array('view'=> $this->view_engine->form());
            $view = $this->load->view('medico/create.php', $view, TRUE);
            $this->setContent($view);
        } elseif ($params[0] == 'process') {
            $process = $this->view_engine->process();
            $success = FALSE;
            if ($process['result']) {
                $params = array('format_type' => 'insert', 'data' => $process['data']);

                $data = $this->_format($params);
                $success = $this->medico_model->insert($data);

                $this->setContent($success);
            } else {
                $this->setContent($process['validation_error']);
            }

            //seteamos los datos de auditoria
            $this->audit->setOperationData($process);
            $this->audit->setChkResult($success);
        }
    }

    /**
     * Muestra el detalle un registro de medico.
     *
     * @access public
     * @author Jose A. Rodriguez E.
     * @version V 1.0 26/04/2013 12:25:53 pm
     */
    function detail() {

        $array_post = $this->input->post();
        $id = $this->encrypt->decode($array_post['id']);

        if (!empty($id)) {
            $this->session->set_userdata("medico.id", $id);
            $data = $this->Medico_model->getById($id);
            $tabs = array(
                array('view' => 'form', 'url' => trim($this->uri->uri_string(), '/'), 'params' => array('data' => $data)),
                array('view' => 'grid', 'url' => 'persona/medico/listDirections', 'params' => array('data' => array()))
            );
            $view = $this->view_engine->tab($tabs);
            $this->setContent($view);
        }

        //seteamos los datos de auditoria
        $this->audit->setOperationData(array('id' => $id));
    }

    /**
     * Muestra las direcciones del medico
     *
     * @access  public
     * @param     array    $params    parametros a ser procesados en el metodo
     * @return  array    dato a ser retornado
     * @author  Jose A. Rodriguez E. <josearodrigueze@gmail.com>
     * @version V 1.0 14/05/2013 14:52:14
     */
    public function listDirections($params) {
        $this->pagination();
    }

    /**
     * Edit de Medico.
     *
     * @access public
     * @param array $params Indica si la operacion es process.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 03/06/2013 10:32:44
     */
    function edit($params = NULL) {
        $array_post = $this->input->post();
        $id = NULL;
        if (empty($params) AND isset($array_post['id'])) {
            $id = $this->encrypt->decode($array_post['id']);
            $this->session->set_userdata('medico.id', $id);
            $data = $this->Medico_model->getById($id);
            $tabs = array(
                array('view' => 'form', 'url' => trim($this->uri->uri_string(), '/'), 'params' => array('data' => $data)),
                array('view' => 'grid', 'url' => 'persona/medico/edit_list_directions', 'params' => array('data' => array())),
            );
            $view = $this->view_engine->tab($tabs);
            $this->setContent($view);
        } elseif ($params[0] == 'process') {
            $process = $this->view_engine->process();
            $success = FALSE;
            $id = $this->session->userdata('medico.id');
            if ($process['result']) {
                $params = array('format_type' => 'update','data' => $process['data']);

                $data = $this->_format($params);
                $success = $this->Medico_model->update($id, $data);

                if ($success)
                    $this->session->unset_userdata('medico.id');

                $this->setContent($success);
            } else {
                $this->setContent($process['validation_error']);
            }

            //seteamos los datos de auditoria
            $operation_data = $process;
            $this->audit->setChkResult($success);
        }

        //seteamos los datos de auditoria
        $operation_data['id'] = $id;
        $this->audit->setOperationData($operation_data);
    }

    /**
     * Muestra las direcciones del medico
     *
     * @access  public
     * @param     array    $params    parametros a ser procesados en el metodo
     * @return  array    dato a ser retornado
     * @author  Jose A. Rodriguez E. <josearodrigueze@gmail.com>
     * @version V 1.0 date
     */
    public function edit_list_directions($params) {
        $this->pagination();
    }

    /**
     * Obtiene los datos a se paginados y mostrados en el grid. La idea de esta funcion
     * es para cuando deseamos paginar datos que no son asociados directamente con el
     * modelo del controlador. Es necesario que retone siempre un array con las posiciones
     * data y count
     *
     * @access  public
     * @param   string  array[offset]
     * @param   string  array[search_fields]
     * @param   string  array[sort]
     * @return  array   con las posiciones [data] y [count]
     * @author  Jose A. Rodriguez E. <josearodrigueze@gmail.com>
     * @version V 1.0 02/05/2013 03:52:52 pm
     */
    public function getPaginationData($params) {
        $uri = $this->uri->uri_string();
        if (strpos($uri, 'listDirections') OR strpos($uri, 'edit_list_directions')) {
            $params['id'] = $this->session->userdata('medico.id');
            $data['data'] = $this->Medico_model->getDirections($params);
            $data['count'] = $this->Medico_model->countDirections($params);
            return $data;
        } else {
            return parent::getPaginationData($params);
        }
    }

    /**
     * Provee un formulario para la carga de nuevas direcciones asocociadas a los
     * medicos
     *
     * @access  public
     * @param array $data datos obtenidos del formulario de creacion de direcciones
     * @author Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version V 1.0 03/06/2013 02:42:52 pm
     */
    function create_direction() {

        $this->setTargetOn('window2');
        $id = $this->session->userdata('medico.id');
        if (!$this->input->post()) {

            $view = array('view' => $this->view_engine->form());
            $view = $this->load->view('medico/create.php', $view, TRUE);
            $data['id_medico'] = $id;
            $this->setContent($view);
        } elseif ($this->input->post()) {
            $process = $this->view_engine->process();
            $success = FALSE;
            if ($process['result']) {
                $params = array('format_type' => 'insert', 'data' => $process['data']);

                $data = $this->_format($params);
                $data['id_medico'] = $id;

                $success = $this->Medico_model->saveDirection($data);

                $this->setContent($success);
            } else {
                $this->setContent($process['validation_error']);
            }

            //seteamos los datos de auditoria
            $this->audit->setOperationData($process);
            $this->audit->setChkResult($success);
        }
    }

    /**
     * Edita las direcciones de los medicos
     *
     * @access  public
     * @param   array $params Indica si la operacion es process.
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 03/06/13 11:15 AM
     */
    public function edit_adresses_doctor($params = NULL) {
        $array_post = $this->input->post();
        $id = NULL;
        if (empty($params) AND isset($array_post['id'])) {
            $id = $this->encrypt->decode($array_post['id']);
            $this->session->set_userdata('medico_localidad.id', $id);
            $data = $this->Medico_model->get_direction_doctor($id);

            $this->setTargetOn('window2');
            //$this->setContent($this->view_engine->form(array('data' => $data)));
            $view = array('view'=> $this->view_engine->form(array('data' => $data)));
            $view = $this->load->view('medico/create_medico.php', $view, TRUE);
             $this->setContent($view);
        } elseif ($params[0] == 'process') {

            $process = $this->view_engine->process();
            $success = FALSE;
            $id = $this->session->userdata('medico_localidad.id');
            if ($process['result']) {
                $params = array(
                    'format_type' => 'update',
                    'data' => $process['data']
                );

                $data = $this->_format($params);
                $success = $this->Medico_model->update_direction_doctor($id, $data);

                if ($success)
                    $this->session->unset_userdata('medico_localidad.id');

                $this->setContent($success);
            } else {
                $this->setContent($process['validation_error']);
            }

            // -----------------------------
            //seteamos los datos de auditoria
            $operation_data = $process;
            $this->audit->setChkResult($success);
            // -----------------------------
        }

        // -----------------------------
        //seteamos los datos de auditoria
        $operation_data['id'] = $id;
        $this->audit->setOperationData($operation_data);
        // -----------------------------
    }

     /**
     * Edita las direcciones de los medicos
     *
     * @access  public
     * @param   array $params Indica si la operacion es process.
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 03/06/13 11:15 AM
     */
     public function EXC_get_localidad_doctor() {
         $id = $this->input->get('id');
         $data = $this->Medico_model->get_localidad($id);
         $this->setTargetOn('json');
         $this->setContent($data);
     }
}