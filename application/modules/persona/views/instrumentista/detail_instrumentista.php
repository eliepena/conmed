


<?php if (isset($detail) and !empty($detail)): ?>
    <table id="instrumentistaTable" class=" table table-striped table-bordered">
        <thead>
            <tr>
                <th>Cod</th>
                <th >Medico</th>
                <th >Paciente</th>
                <th>Fecha Cirugia</th>
                <th>Hora Cirugia</th>
                <th >Tipo Cirugia</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($detail as $key => $value): ?>
                <tr id="tr-detail_instru-<?php echo $key; ?>" >
                    <td><?php echo $value['cod_cotizacion']; ?></td>
                    <td><?php echo $value['medico']; ?></td>
                    <td><?php echo $value['paciente']; ?></td>
                    <td><?php echo $value['fecha_cirugia']; ?></td>
                    <td><?php echo $value['hora_cirugia']; ?></td>
                    <td><?php echo $value['nombre_articulacion']; ?></td>
                </tr>
            <?php endforeach; ?>
        <?php else:

            echo "<center>el instrumentista seleccionado no posee cirugias pendientes.</center>";

            endif; ?>
    </tbody>
</table>
<style></style>
