<?php
if (isset($concurrencesWithMedico) && $concurrencesWithMedico):
    $categories = '';
    $series = '';
    $porcentaje_total = 0;
    $calculo = 0;
    $coincidencias = FALSE;
    foreach ($concurrencesWithMedico AS $key => $value):
        $porcentaje_total = $porcentaje_total + $value['coincidencias'];
    endforeach;
    foreach ($concurrencesWithMedico AS $key => $value):
        if ($value['id'] == $id_instrumentista):
            $categories.="'" . $value['label'] . "',";
            $series.=($value['coincidencias'] * 100) / $porcentaje_total . ",";
            $coincidencias = TRUE;
        endif;

    endforeach;
    $categories = trim((float)$categories, ',');
    $series =number_format($categories, 2, '.', '.');
    if ($coincidencias):
        ?>


        <div id="container" style="max-width: 90%; "></div>
        <script>

            $(function () {
                $('#container').highcharts({
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Relacion de Trabajo Medico-Instrumentista'
                    },
                    subtitle: {
                        text: 'Medico: <?php echo $medico; ?>'
                    },
                    xAxis: {

                        categories: [<?php echo $categories; ?>],
                        title: {
                            text: 'Instrumentista'
                        }
                    },
                    series: [{
                            name: 'Coincidencia de Trabajo:',
                            data: [<?php echo $series; ?>]
                        }],
                    yAxis: {
                        min: 0,
                        max:100,
                        title: {
                            text: 'Porcentaje de Cirugias',
                            align: 'high'
                        },
                        labels: {
                            overflow: 'justify'
                        }
                    },
                    tooltip: {
                        valueSuffix: ' %'
                    },
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },

                    credits: {
                        enabled: false
                    }
                });
            });
        </script>

        <?php
    else:
        echo "<center>El medico <b>$medico</b> no ha realizado  cirugias con este instrumentista.</center>";
    endif;
else:
    echo "<center>El medico  <b>$medico</b> no ha realizado  cirugias con este instrumentista.</center>";
endif;
?>