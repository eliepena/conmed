<?php
if (isset($data) && is_array($data) && sizeof($data) > 0):

    foreach ($data as $key => $value):
        $data_instrumenstalist = "{'nombre':'" . $value['label'] . "','cedula':'" . $value['cedula'] . "','correo':'" . $value['correo_electronico'] . "'}";
        ?>
        <li id="list_instrumentalist-<?php echo $value['id']; ?>" class="list_instrumentalist">
            <a  href="#" id="i-<?php echo $value['id']; ?>" onclick="var sub_array={'datos':{'disponibilidad':'<? echo $value['disponibilidad']; ?>','nombre':'<? echo $value['label']; ?>','cedula':'<? echo $value['cedula']; ?>','correo':'<? echo $value['correo_electronico']; ?>'}};detail_instrumentalist(<?php echo $value['id']; ?>,sub_array);">
                <i class="icon-book"></i>
                <?php echo $value['label']; ?>
                <span
                    class="<?php echo ($value['disponibilidad'] ? ' avalaible ' : ' notAvalaible '); ?>badge" >
                    <font size="0.2 em">
                    <?php echo ($value['disponibilidad'] ? ' Diponible ' : ' No Disponible '); ?>
                    </font>
                </span>
            </a>
        </li>
        <?php
    endforeach;
else:
    ?>
    <li id="list_instrumentalist-none" class="list_instrumentalist">
        <span class="notAvalaible badge">
            <font size="0.2 em"> No hay Disponibilidad </font>
        </span>
    </li>

<?php
endif;
?>
<style>
    .avalaible{
        color: green;
    }
    .notAvalaible{
        color: red;
    }
</style>
