<?php


if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Empleado_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad Empleado
 * y sus entidades dependientes o extendidas
 *
 * @package Modules
 * @category Model
 * @version V-1.0 17/01/13 03:23 PM
 */
class Empleado_model extends AST_Model {

    public function __construct() {
        $this->setSchema('business');
        parent::__construct();
    }

    /**
     * Busca el identificador de un empleado dado su usuario, 
     * 
     * @param integer $user_id Identificador del usuario
     * @return integer Identificador del empleado
     * @author Nohemi Rojas
     * @version 1.0 03/04/13 04:17 PM
     */
    public function getIdByUser($user_id) {
        $this->db->select('business.empleado.id');
        $this->db->from('business.empleado');
        $this->db->join('acl.users', "id_usuario = users.id");
        $this->db->where('id_usuario', $user_id);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row()->id : FALSE;
    }

 	/**
     * Indica por que campo se deben ordenar los datos a ser mostrados en el grid
     *
     * @access  public
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 03/04/2013 04:41:41 pm
     */
    public function orderBy() {
    	$this->db->order_by('cedula');
    }
}
