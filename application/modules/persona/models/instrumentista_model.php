<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Instrumentista_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad Instrumentista
 * y sus entidades dependientes o extendidas
 *
 * @package Modules
 * @category Model
 * @version V-1.0 17/01/13 03:23 PM
 */
class Instrumentista_model extends AST_Model {

    var $today;

    public function __construct() {
        $this->setSchema('business');
        parent::__construct();
        $this->today = date('Y-m-d');
    }

    /**
     * create de Instrumentista.
     *
     * @access public
     * @param array $data contiene los datos del articulo.
     * @author Eliezer Pe#a
     * @version V 1.0 17/09/2013
     */
    function insert($data) {
        $this->db->trans_start();
        $get_id_instrumentista = $this->insertWithReturning($data['data'], 'business.instrumentista');
        $Especialidad = $data['especialidad'];
        if ($Especialidad) {
            foreach ($Especialidad as $key => $val) {
                $Especialidad [$key]['id_instrumentista'] = $get_id_instrumentista['id'];
            }
            $this->db->insert_batch('business.instrumentista_especialidad', $Especialidad);
            $this->db->trans_complete();
            return $this->db->trans_status();
        }
    }

    /**
     * Obtiene el id de las especialidades correspondientes a un instrumentista
     *
     * @access public
     * @param integer $id identificador del instrumentista.
     * @author Eliezer Pe#a
     * @version V 1.0 30/09/2013
     */
    function getEspecialidad($id) {
        $this->db->select('id_especialidad as id');
        $this->db->from('business.instrumentista_especialidad');
        $this->db->where('id_instrumentista', $id);
        $query = $this->db->get();
        $esp = $query->result_array();
        foreach ($esp as $key => $value) {
            $especialidad[$key] = $value['id'];
        }
        return ($query->num_rows() > 0) ? $especialidad : FALSE;
    }

    /**
     * Edit de Instrumentista.
     *
     * @access public
     * @param array $data contiene los datos del instrumentista.
     * @param integer $id contiene el idnetificador del instrumentista
     * @author Eliezer Pe#a
     * @version V 1.0 30/09/2013
     */
    function edit($id, $data) {
        $this->db->trans_start();
        $this->db->where('instrumentista.id', $id);
        $this->db->update('business.instrumentista', $data['data']);
        $this->db->delete('business.instrumentista_especialidad', array('id_instrumentista' => $id));
        $especialidad = $data['especialidad'];
        if ($especialidad) {
            foreach ($especialidad as $key => $value) {
                $especialidad[$key]['id_instrumentista'] = $id;
            }
            $this->db->insert_batch('business.instrumentista_especialidad', $especialidad);
        }
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    /**
     * Obtiene el nombre de las especialidades correspondientes a un instrumentista
     *
     * @access public
     * @param integer $id identificador del instrumentista.
     * @author Eliezer Pe#a
     * @version V 1.0 01/10/2013
     */
    function getNameEspecialidad($id) {
        $this->db->select('c.nombre as nombre');
        $this->db->from('business.instrumentista_especialidad ie');
        $this->db->join('business.cirugia c', 'ie.id_especialidad=c.id');
        $this->db->where('ie.id_instrumentista', $id);
        $query = $this->db->get();
        $esp = $query->result_array();
        $especialidad = "";
        foreach ($esp as $key => $value) {
            $especialidad = $value['nombre'] . ", " . $especialidad;
        }
        return ($query->num_rows() > 0) ? trim($especialidad, ", ") : FALSE;
    }

    /**
     * Provee los instrumentistas de acuerdo a su disponibilidad
     *
     * @access  public
     * @param   array   $param
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 30/09/13 11:53 AM
     */
    function getInstrumentalistsAvailable($param = array(), $mode_detail = FALSE, $group = FALSE) {


        //tables
        $t_instrumentista = $this->concatSchemaTable();
        $t_instrumentista_especialidad = $this->getSchema() . '.instrumentista_especialidad';
        $t_cirugia = $this->getSchema() . '.cirugia';
        $t_cotizacion_instru = $this->getSchema() . '.cotizacion_instrumentista';
        $t_cotizacion = $this->getSchema() . '.cotizacion';
        $t_medico = $this->getSchema() . '.medico';
        $t_paciente = $this->getSchema() . '.paciente';
        $t_cotizacion_cirugia = $this->getSchema() . '.cotizacion_cirugia';

        //params
        extract($param);

        //select
        $this->db->distinct("$t_instrumentista.id ");
        $this->db->select("$t_instrumentista.id ", FALSE);
        $this->db->select("$t_instrumentista.cedula ", FALSE);
        $this->db->select("$t_instrumentista.correo_electronico ", FALSE);
        $this->db->select("$t_instrumentista.primer_nombre || ' ' || $t_instrumentista.primer_apellido AS label ");



        if ($mode_detail):

//            $this->db->select("$t_cotizacion.id AS id_cotizacion ", FALSE);
//            $this->db->select("$t_cotizacion_instru._delete as delete ", FALSE);
            $this->db->select("$t_medico.primer_nombre || ' ' || $t_medico.primer_apellido AS medico ");
            $this->db->select("$t_paciente.primer_nombre || ' ' || $t_paciente.primer_apellido AS paciente ");
            $this->db->select("$t_cotizacion_cirugia.id_articulacion ", FALSE);
            $this->db->select("articulacion.nombre AS nombre_articulacion ", FALSE);
            $this->db->select("lesion.nombre AS nombre_lesion ", FALSE);
            $this->db->select("tecnica.nombre AS nombre_tecnica ", FALSE);
            $this->db->select("$t_cotizacion.codigo AS cod_cotizacion ", FALSE);
        endif;

        //from and join
        $this->db->from("$t_instrumentista");
        $this->db->join($t_instrumentista_especialidad, "$t_instrumentista.id=$t_instrumentista_especialidad.id_instrumentista LEFT ");
        $this->db->join($t_cirugia, "$t_instrumentista_especialidad.id_especialidad=$t_cirugia.id LEFT ");
        $this->db->join($t_cotizacion_instru, "$t_cotizacion_instru.id_instrumentista=$t_instrumentista.id LEFT ");
        $this->db->join($t_cotizacion, "$t_cotizacion_instru.id_cotizacion=$t_cotizacion.id LEFT ");
        $this->db->join($t_medico, "$t_cotizacion.id_medico=$t_medico.id LEFT ");
        $this->db->join($t_paciente, "$t_cotizacion.id_paciente=$t_paciente.id LEFT ");
        $this->db->join("$t_cotizacion_cirugia", "$t_cotizacion_cirugia.id_cotizacion=$t_cotizacion.id LEFT ");
        $this->db->join("$t_cirugia as articulacion", "$t_cotizacion_cirugia.id_articulacion=articulacion.id  LEFT ");
        $this->db->join("$t_cirugia as lesion", "$t_cotizacion_cirugia.id_lesion=lesion.id LEFT ");
        $this->db->join("$t_cirugia as tecnica", "$t_cotizacion_cirugia.id_tecnica=tecnica.id ");

        //where
        if (isset($id_articulacion) && !empty($id_articulacion)):
            $this->db->where(array("$t_instrumentista_especialidad.id_especialidad" => $id_articulacion));

        endif;
        if (isset($id_instrumentista) && !empty($id_instrumentista)):
            $this->db->where(array("$t_instrumentista.id" => $id_instrumentista, "$t_cotizacion_instru._delete" => '0'));
            $this->db->where("$t_cotizacion.fecha_cirugia >=", $this->today);
        endif;
//        $this->db->where(array("$t_cotizacion_instru._delete" => '0'));
        $this->whereDelete();

        if ($group):
            $this->db->group_by("$t_instrumentista.id");
//            $this->db->group_by("$t_cotizacion.id");
//            $this->db->group_by("$t_cotizacion_instru._delete");
            $this->db->group_by("business.instrumentista.cedula");
            $this->db->group_by("$t_instrumentista.correo_electronico");
            $this->db->group_by("business.instrumentista.primer_nombre");
            $this->db->group_by("business.instrumentista.primer_apellido");
        else:
            $this->db->select("$t_cotizacion.fecha_cirugia", FALSE);
            $this->db->select("$t_cotizacion.hora_cirugia", FALSE);
        endif;
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    /**
     * Concurrencias de instrumentistas que han trabajado con medicos
     *
     * @access  public
     * @param   integer $id Identificador
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 01/10/13 01:44 PM
     */
    function workingConcurrencesWithMedico($id = NULL) {
        if (!$id)
            return FALSE;

        //tables
        $t_instrumentista = $this->concatSchemaTable();
        $t_instrumentista_especialidad = $this->getSchema() . '.instrumentista_especialidad';
        $t_cirugia = $this->getSchema() . '.cirugia';
        $t_cotizacion_instru = $this->getSchema() . '.cotizacion_instrumentista';
        $t_cotizacion = $this->getSchema() . '.cotizacion';
        $t_medico = $this->getSchema() . '.medico';
        $t_paciente = $this->getSchema() . '.paciente';
        $t_cotizacion_cirugia = $this->getSchema() . '.cotizacion_cirugia';

        $this->db->select("count($t_instrumentista.id) as coincidencias", FALSE);
        $this->db->select("$t_instrumentista.id", FALSE);
        $this->db->select("$t_instrumentista.primer_nombre || ' ' || $t_instrumentista.primer_apellido AS label");


        $this->db->from("$t_medico");
        $this->db->join("$t_cotizacion", "$t_medico.id=$t_cotizacion.id_medico");
        $this->db->join("$t_cotizacion_instru", "$t_cotizacion_instru.id_cotizacion=$t_cotizacion.id");
        $this->db->join("$t_instrumentista", "$t_cotizacion_instru.id_instrumentista=$t_instrumentista.id");

        $this->db->where(array("$t_medico.id" => $id));
        $this->db->group_by("$t_instrumentista.id");
        $this->db->group_by("$t_instrumentista.primer_nombre");
        $this->db->group_by("$t_instrumentista.primer_apellido");


        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    /**
     * Detalla la agenda de cirugia de una cotizacion
     *
     * @access  public
     * @param   integer $id Identificador del medico
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 01/10/13 01:44 PM
     */
    function diaryQuote($id_quote = NULL) {
        $t_medico = $this->getSchema() . '.medico';
        $id = $this->session->userdata("Cotizacion_model.id");
        if (!$id_quote)
            return FALSE;
        $t_cotizacion = $this->getSchema() . '.cotizacion';
        $this->db->select("$t_cotizacion.id", FALSE);
        $this->db->select("$t_cotizacion.fecha_cirugia", FALSE);
        $this->db->select("$t_cotizacion.hora_cirugia", FALSE);
        $this->db->select("$t_cotizacion.id_medico", FALSE);
        $this->db->select("$t_medico.primer_nombre || ' ' || $t_medico.primer_apellido AS medico");
        $this->db->from("$t_cotizacion", ' LEFT ');
        $this->db->join($t_medico, "$t_cotizacion.id_medico=$t_medico.id  ");
        $this->db->where(array("$t_cotizacion.id" => $id, "$t_cotizacion._delete" => '0'));
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    /**
     * Provee los instrumentistas de una cotizacion
     *
     * @access  public
     * @param   array   $id Identificador de la Cotizacion
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 14/10/13 04:25 PM
     */
    function instrumentalistsByCot($id = NULL) {

        if (!$id)
            return false;

        //tables
        $t_instrumentista = $this->concatSchemaTable();
        $t_cotizacion_instru = $this->getSchema() . '.cotizacion_instrumentista';

        //select
        $this->db->distinct("$t_instrumentista.id");
        $this->db->select("$t_instrumentista.id", FALSE);
        $this->db->select("$t_instrumentista.cedula", FALSE);
        $this->db->select("$t_instrumentista.correo_electronico", FALSE);
        $this->db->select("$t_instrumentista.primer_nombre || ' ' || $t_instrumentista.primer_apellido AS label");



        //from and join
        $this->db->from("$t_cotizacion_instru");
        $this->db->join($t_instrumentista, "$t_cotizacion_instru.id_instrumentista=$t_instrumentista.id");

        //where
        $this->db->where(array("$t_cotizacion_instru.id_cotizacion " => $id, "$t_cotizacion_instru._delete" => '0'));


//        $this->whereDelete();

        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    /**
     * Verifica si la cotizacion a aprobar
     * ya esta asignada al instrumentista
     *
     * @access  public
     * @param   integer $id_instrumentista Identificador del instrumentista
     * @param   integer $id_cotizacion Identificador de la cotizacion
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 28/10/13 11:59 AM
     */
    function getInstrumentalistsAvailableCot($id_instrumentista = NULL, $id_cotizacion = NULL) {
        if (!$id_instrumentista or !$id_cotizacion)
            return FALSE;

        //tables
        $t_cotizacion_instru = $this->getSchema() . '.cotizacion_instrumentista';
        $t_cotizacion = $this->getSchema() . '.cotizacion';

        $this->db->distinct("$t_cotizacion.id", FALSE);
        $this->db->select("$t_cotizacion.id AS id_cotizacion", FALSE);



        //from and join
        $this->db->from("$t_cotizacion_instru");
        $this->db->join($t_cotizacion, "$t_cotizacion_instru.id_cotizacion=$t_cotizacion.id");

        //where

        $this->db->where(
                array(
                    "$t_cotizacion_instru.id_instrumentista" => $id_instrumentista,
                    "$t_cotizacion._delete" => '0',
                    "$t_cotizacion.id" => $id_cotizacion
                )
        );




        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

}