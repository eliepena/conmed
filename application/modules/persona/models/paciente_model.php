<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Paciente_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad Paciente
 * y sus entidades dependientes o extendidas
 *
 * @package Modules
 * @category Model
 * @author Nohemi Rojas. <nohemir@gmail.com>
 * @version V-1.0 17/01/13 03:22 PM
 */
class Paciente_model extends AST_Model {

    public function __construct() {
        $this->setSchema('business');
        parent::__construct();
    }

    /**
     * Indica por que campo se deben ordenar los datos a ser mostrados en el grid
     *
     * @access  public
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 03/04/2013 04:41:41 pm
     */
    public function orderBy() {
        $this->db->order_by('cedula', 'ASC');
    }

    /**
     * Provee las implementacion basica para realizar inserciones dentro de la
     * base de datos.
     *
     * @access  public
     * @param   Array   $data datos a ser guardados dentro de la base de datos.
     * @return  Boolean Indicando el resultado de la operacion.
     * @author  Frederick Bustamante <frederickdanielb@gmail.com>
     * @version V-1.0 11/07/13 05:28 PM
     */
    function insert($data) {
        $get_data_paciente = $this->insertWithReturning($data['cotizacion'], $this->table);
        return $get_data_paciente ? array('id_paciente' => $get_data_paciente['id'], 'sucess' => TRUE) : array('sucess' => FALSE);
    }

    /**
     * Devuelve los datos basicos del paciente
     *
     * @access public
     * @param integer $id Identificador del paciente
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 31/07/13 12:35 PM
     */
    function getPaciente($id = NULL, $campo = '') {

        $this->db->select((empty($campo) ? ' * ' : ' ' . $campo . ' '), false);
        $this->db->from('business.paciente');
        if ($id):
            $this->db->where(array('id' => $id));
        endif;
        $this->db->where(array('_delete' => '0'));
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

}