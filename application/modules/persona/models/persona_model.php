<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Persona_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad persona
 * y sus entidades dependientes o extendidas
 *
 * @package Modules
 * @category Model
 * @version V-1.0 29/11/12 01:26 PM
 */
class Persona_model extends AST_Model {

    public function __construct() {
        $this->setSchema('business');
        parent::__construct();
    }

    /**
     * Obtiene datos de la persona
     *
     * @access  public
     * @param   integer  $id_persona identificador de la persona
     * @return  array    datos de la persona
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 19/12/13 03:37 PM
     */
    public function getPersona($id_persona) {
        $this->db->select("*");
        $this->db->from('business.persona');
        $this->db->where('id', $id_persona);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : NULL;
    }

}

?>
