<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Medico_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad Medico
 * y sus entidades dependientes o extendidas
 *
 * @package Modules
 * @category Model
 * @author Nohemi Rojas. <nohemir@gmail.com>
 * @version V-1.0 17/01/13 03:23 PM
 */
class Medico_model extends AST_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Indica por que campo se deben ordenar los datos a ser mostrados en el grid
     *
     * @access  public
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 03/04/2013 04:41:41 pm
     */
    public function orderBy() {
        // $this->db->order_by('cedula');
    }

    /**
     * Inserta los datos del medico en las tablas correspondientes.
     *
     * @access  public
     * @param     array    $data    datos del medico
     * @return  bool    resultado del insert
     * @author  Jose A. Rodriguez E. <josearodrigueze@gmail.com>
     * @version V 1.0 24/04/2013 12:13:12 pm
     */
    public function insert($data) {
        $medico = $medico_localidad = array();

        $fields = $this->db->list_fields('medico');
        $medico = array_intersect_key($data, array_flip($fields));
        unset($medico['id_localidad']);

        $fields = $this->db->list_fields('medico_localidad');
        $medico_localidad = array_intersect_key($data, array_flip($fields));

        // Iniciamos la insercion en base de datos.
        $this->db->trans_start();
        //FORMA ANTERIOR
//        $this->db->insert($this->concatSchemaTable(), $medico);
//        @$id_medico = $this->db->affected_rows() ? $this->db->insert_id() : FALSE;

        $get_data_medico = $this->insertWithReturning($medico, $this->concatSchemaTable());

        if (!$get_data_medico)
            return FALSE;

        $medico_localidad['id_medico'] = $get_data_medico['id'];

        $this->db->insert('business.medico_localidad', $medico_localidad);

        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    /**
     * Obtiene los registros de las direcciones asocaidas a los medicos
     *
     * @access  public
     * @param     array    $params    filtros para la consulta
     * @return  array    direcciones del medico
     * @author  Jose A. Rodriguez E. <josearodrigueze@gmail.com>
     * @version V 1.0 02/05/2013 05:23:44 pm
     */
    public function getDirections($params) {
        extract($params);
        $this->setDirections($id);

        $search = $this->filterSearchFields($search_fields);

        return parent::getAll($offset, $search, $sort);
    }

    /**
     * Cuenta el numero total de direcciones para un medico
     *
     * @access  public
     * @param   array    $params    filtros de la consulta
     * @return  integer    total de registros
     * @author  Jose A. Rodriguez E. <josearodrigueze@gmail.com>
     * @version V 1.0 02/05/2013 05:52:45 pm
     */
    public function countDirections($params) {
        extract($params);
        $search = $this->filterSearchFields($search_fields);

        $this->setDirections($id);
        return parent::getCount($search);
    }

    /**
     * Provve las configuraciones para las direcciones de los medicos
     *
     * @access  private
     * @param   integer $id     Identificador del medico
     * @author  Jose A. Rodriguez E. <josearodrigueze@gmail.com>
     * @version V 1.0 02/05/2013 05:53:39 pm
     */
    private function setDirections(&$id) {
        $this->table_name = 'medico_localidad';
        $this->db->where('id_medico', $id);

        $table = $this->getSchema() . '.' . $this->table_name;
        $t_clinica = $this->getSchema() . '.centro_salud';

        $this->db->select($table . '.id');
        $this->db->select("CASE WHEN ($table.validado = '1') THEN 'SI' ELSE 'NO' END AS validado", FALSE);
        $this->db->select("COALESCE($t_clinica.direccion|| ' ' ||$table.direccion, $table.direccion) AS direccion", FALSE);
        $this->db->select('COALESCE(cli_loc.nombre,med_loc.nombre) AS id_localidad', FALSE);
        $this->db->select($t_clinica . '.nombre AS id_centro_salud');
        $this->db->select($table . '.telefono');

        $this->db->join("{$this->getSchema()}.localidad AS med_loc", "med_loc.id={$this->table_name}.id_localidad", 'LEFT');
        $this->db->join($t_clinica, "$t_clinica.id={$this->table_name}.id_centro_salud", 'LEFT');
        $this->db->join("{$this->getSchema()}.localidad AS cli_loc", "cli_loc.id=$t_clinica.id_localidad", 'LEFT');
    }

    /**
     * Filtra los datos para el listado de direcciones
     *
     * @access  private
     * @param     array    $search    Search Fields
     * @return  array    Search Fields Filtrados
     * @author  Jose A. Rodriguez E. <josearodrigueze@gmail.com>
     * @version V 1.0 03/05/2013 03:45:12 pm
     */
    private function filterSearchFields($search) {

        if (empty($search))
            return NULL;

        $searching = $search['validado'];
        $search = array(
            'med_loc.nombre' => $searching,
            'cli_loc.nombre' => $searching,
            "{$this->getSchema()}.centro_salud.direccion" => $searching,
            "{$this->getSchema()}.{$this->table_name}.direccion" => $searching,
            "{$this->getSchema()}.{$this->table_name}.telefono" => $searching,
        );
        return $search;
    }

    /**
     * Almacena las direcciones asociadas a un medico
     * @param type $params valores a insertar en la base de datos
     * @return resultado de la transaccion
     * @author Eliezer Pe#a  <pe.eliezer@gmail.com >
     */
    function saveDirection($params) {

        $fields = $this->db->list_fields('medico_localidad');
        $medico_localidad = array_intersect_key($params, array_flip($fields));
        $this->db->trans_start();
        $this->db->insert('business.medico_localidad', $medico_localidad);
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    /**
     * Muestra la direccion a editar del medico
     *
     * @access  public
     * @param   integer $id  Identificador de medico_localidad
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 03/06/13 12:50 PM
     */
    public function get_direction_doctor($id) {
        $this->table_name = 'medico_localidad';
        $this->db->where('medico_localidad.id', $id);

        $table = $this->getSchema() . '.' . $this->table_name;
        $t_clinica = $this->getSchema() . '.centro_salud';

        $this->db->select("$table.direccion AS direccion", FALSE);
        $this->db->select('COALESCE(cli_loc.id,med_loc.id) AS id_localidad', FALSE);
        $this->db->select($t_clinica . '.id AS id_centro_salud');
        $this->db->select($table . '.telefono');

        $this->db->from($table);
        $this->db->join("{$this->getSchema()}.localidad AS med_loc", "med_loc.id={$this->table_name}.id_localidad", 'LEFT');
        $this->db->join($t_clinica, "$t_clinica.id={$this->table_name}.id_centro_salud", 'LEFT');
        $this->db->join("{$this->getSchema()}.localidad AS cli_loc", "cli_loc.id=$t_clinica.id_localidad", 'LEFT');

        $query = $this->db->get();
        $data = $query->result_array();
        return ($query->num_rows() > 0) ? ($data[0]) : false;
    }

    /**
     * Actualiza las direcciones de los medicos
     *
     * @access  public
     * @param   array $params recibe los campos a editar.
     * @return  array $id Identificador medico_localidad para el where en el update
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 03/06/13 11:15 AM
     */
    public function update_direction_doctor($id, $params = array()) {
        $this->table_name = 'medico_localidad';
        $this->db->where('medico_localidad.id', $id);

        $table = $this->getSchema() . '.' . $this->table_name;
        $data = array(
            'direccion' => $params['direccion'],
            'id_localidad' => $params['id_localidad'],
            'id_centro_salud' => $params['id_centro_salud'],
            'telefono' => $params['telefono']
        );
        return $this->db->update($table, $data);
    }

    /**
     * Obtiene Localidad del medico segun su centro de salud
     *
     * @access  public
     * @param   int $id Identificador del Centro de Salud.
     * @param   array $data Datos retornados de la consulta (id de la localidad)
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 04/06/13 10:44 AM
     */
    function get_localidad($id) {
        $this->table_name = 'medico_localidad';
        $this->db->where('centro_salud.id', $id);

        $table = $this->getSchema() . '.' . $this->table_name;
        $t_clinica = $this->getSchema() . '.centro_salud';


        $this->db->select('cli_loc.id AS id_localidad', FALSE);
        $this->db->select('cli_loc.nombre AS nombre_localidad', FALSE);


        $this->db->from($t_clinica);
        $this->db->join("{$this->getSchema()}.localidad AS cli_loc", "cli_loc.id= $t_clinica.id_localidad");

        $query = $this->db->get();
        $data = $query->result_array();
        return ($query->num_rows() > 0) ? ($data[0]) : false;
    }

    /**
     * Muestra la direccion a editar del medico
     *
     * @access  public
     * @param   integer $id  Identificador de medico_localidad
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 28/08/13 05:29 PM
     */
    public function get_direction_medico($id) {
        $this->table_name = 'medico_localidad';
        $this->db->where('medico_localidad.id_medico', $id);

        $table = $this->getSchema() . '.' . $this->table_name;
        $t_clinica = $this->getSchema() . '.centro_salud';

        $this->db->select("$table.direccion AS direccion", FALSE);
        $this->db->select('COALESCE(cli_loc.id,med_loc.id) AS id_localidad', FALSE);
        $this->db->select('COALESCE(cli_loc.chk_tabla,med_loc.chk_tabla) AS tipo', FALSE);
        $this->db->select('COALESCE(cli_loc.nombre,med_loc.nombre) AS tipo_nombre', FALSE);
        $this->db->select($t_clinica . '.nombre AS nombre_centro_salud');
        $this->db->select($table . '.telefono');

        $this->db->from($table);
        $this->db->join("{$this->getSchema()}.localidad AS med_loc", "med_loc.id={$this->table_name}.id_localidad", 'LEFT');
        $this->db->join($t_clinica, "$t_clinica.id={$this->table_name}.id_centro_salud", 'LEFT');
        $this->db->join("{$this->getSchema()}.localidad AS cli_loc", "cli_loc.id=$t_clinica.id_localidad", 'LEFT');


        $query = $this->db->get();
        $data = $query->result_array();
        return ($query->num_rows() > 0) ? ($data[0]) : false;
    }

    /**
     * Devuelve los datos basicos del Medico
     *
     * @access public
     * @param integer $id Identificador del tipo del medico
     * @param string  $campo Campos a mostrar en el select (default:'*')
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 31/07/13 12:35 PM
     */
    function getMedico($where = array(), $campo = '') {

        $this->db->select((empty($campo) ? ' * ' : ' ' . $campo . ' '), false);
        $this->db->from('business.medico');
        if (sizeof($where) > 0):
            $this->db->where($where);
        endif;
        $this->db->where(array('_delete' => '0'));
        $this->db->order_by('primer_nombre');

        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

    /**
     * Provee todas las direcciones de los medicos
     *
     * @access  public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 03/09/13 12:25 PM
     */
    public function searchDirections() {
        $this->table_name = 'medico_localidad';
        $table = $this->getSchema() . '.' . $this->table_name;
        $t_clinica = $this->getSchema() . '.centro_salud';
        $this->db->select($table . '.id');
        $this->db->select($table . '.id_medico');
        $this->db->select("CASE WHEN ($table.validado = '1') THEN 'SI' ELSE 'NO' END AS validado", FALSE);
        $this->db->select("COALESCE($t_clinica.direccion|| ' ' ||$table.direccion, $table.direccion) AS direccion", FALSE);
        $this->db->select('COALESCE(cli_loc.nombre,med_loc.nombre) AS localidad', FALSE);
        $this->db->select($t_clinica . '.nombre AS id_centro_salud');
        $this->db->select($table . '.telefono');
        $this->db->from("$table");
        $this->db->join("{$this->getSchema()}.localidad AS med_loc", "med_loc.id={$this->table_name}.id_localidad", 'LEFT');
        $this->db->join($t_clinica, "$t_clinica.id={$this->table_name}.id_centro_salud", 'LEFT');
        $this->db->join("{$this->getSchema()}.localidad AS cli_loc", "cli_loc.id=$t_clinica.id_localidad", 'LEFT');
        $this->db->order_by("$table.id_medico");
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

}