<?php

/**
 *
 * Controlador engine que manejara todas las acciones centralizadas que
 * son necesarias para la ejecucion de las aplicaciones.
 *
 * @package modules
 * @subpackage engine
 * @category controller
 * @author Jose A. Rodriguez E. <josearodrigueze@gmail.com>, Nohemi Rojas <nohemir@gmail.com>
 * @version V - 1.0 9/01/13 17:24
 */
class engine extends AST_Controller {

    private $max_size = 1024;

    function __construct($options = null, $initialize = true, $error_messages = null) {
        parent::__construct();
        $this->load->model('engine/engine_model', 'engine_model');
        $this->load->library(array('upload'));
        $this->load->helper('file');
    }

    /**
     * Provee el acceso a la busqueda de los combos tipo select y sus valores en BD
     *
     * @access public
     * @author  Nohemi Rojas
     * @version V 1.0 8/10/12 01:17 PM
     */
    function EXC_searchSelect() {
        $data = $this->input->get('data');
        $name = $this->input->get('name');
        $filter = $this->input->get('filter');

        $this->offOutput();

        $arr_values = array();
        if (preg_match('/^id_/', $name))
            $arr_values = $this->engine_model->getComboByTable($name, $filter, $data['q']);

        echo json_encode(array('q' => $data['q'], 'results' => $arr_values));
    }

    /**
     * Sube un archivo al servidor
     *
     * @access public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 17/09/13 12:13 PM
     */
    function EXC_uploadFile() {
//        sleep(10);
        $this->offOutput();
        $response = array(
            'estatus' => FALSE
        );
        $array_post = $this->input->post();
        $estatus = '';
        $message = '';
        $background = '';
        $is_image = FALSE;
        $image_thumb = '';
        $image = '';
        $file_path = '';
        $origin_name = '';
        $file_size = '';
        $path_file = (isset($array_post['custom_dir']) && !empty($array_post['custom_dir']) ? $array_post['custom_dir'] : $this->config->item('uploads') . '/');
        $name_element_file = $array_post['element_id'];
        if ($estatus != 'error') {
            //Ruta donde se guarda la imagen completa
            $configuracion['upload_path'] = $path_file;
            $configuracion['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|txt|xsl|xslx|html|odf|rar|zip|7zip|csv'; //Formatos permitidos
//            $configuracion['allowed_types'] = 'gif|jpg|png';
            $configuracion['max_size'] = $this->max_size * 8;
            $configuracion['overwrite'] = FALSE;
            $configuracion['encrypt_name'] = TRUE;
            $configuracion['remove_spaces'] = TRUE;
            $this->upload->initialize($configuracion);


            //Se verifica si el archivo fue cargado al servidor
            if (!$this->upload->do_upload($name_element_file)) {
                $estatus = false;
                $mensaje = $this->upload->display_errors('', '');
            } else {
                $data = $this->upload->data();
                $file_path = $data['file_name'];
                $origin_name = $data['orig_name'];
                //$file_size = $data['file_size'];
                if ($data['is_image'] == 1) {
                    $is_image = TRUE;
                    $thumbnail = $this->_createThumbnail($path_file, $data['file_name']);
                    if ($thumbnail) {
                        $estatus = TRUE;
                        $mensaje = 'Archivo subido correctamente';
                        $image =
                                $image_thumb = explode('.', $data['file_name']);
//                        $image_thumb = $path_file . $image_thumb[0] . '_thumb.' . $image_thumb[1];
                        $image_thumb = $path_file . $file_path;
                    } else {
                        unlink($path_file . $data['file_name']); #borra archivo
                        $estatus = FALSE;
                        $mensaje = 'No se pudo adjuntar el archivo';
                    }
                } else {
                    $mensaje = 'Archivo subido correctamente';
                    $estatus = TRUE;
                    $is_image = FALSE;
                }
            }
            @unlink($_FILES[$name_element_file]);
        }
        $response = array(
            'is_image' => $is_image,
            'mensaje' => $mensaje,
            'estatus' => $estatus,
            'image_thumb' => $image_thumb,
            'file_path' => $file_path,
            'origin_name' => $origin_name,
            'file_size' => $file_size
        );
        $json_encode = json_encode($response);
        print($json_encode);
    }

    /**
     * Crea un Thumbnail de una imagen
     *
     * @access  private
     * @param   string  $filename Nombre de la imagen a redimencionar
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 17/09/13 12:13 PM
     */
    function _createThumbnail($dir_file, $filename) {
        $configuracion['image_library'] = 'gd2';
        //CARPETA EN LA QUE ESTÁ LA IMAGEN A REDIMENSIONAR
        $configuracion['source_image'] = $dir_file . $filename;
        $configuracion['create_thumb'] = TRUE;
        $configuracion['maintain_ratio'] = TRUE;
        //CARPETA EN LA QUE GUARDAMOS LA MINIATURA
        $configuracion['new_image'] = $dir_file;
        $configuracion['width'] = 150;
        $configuracion['height'] = 100;
        $this->load->library('image_lib', $configuracion);
        return $this->image_lib->resize();
    }

    /**
     * Forza la descarga de un archivo
     *
     * @access public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 19/08/13 11:18 AM
     */
    function EXC_force_download() {
        $array_get = $this->input->get();
        $this->offOutput();
        $array_url = explode('/', $array_get['url']);
        $array_url_invertido = array_reverse($array_url);


        $len = filesize($array_get['url']);
        header('Content-type: application/force-download');
        header('Content-Disposition: attachment; filename="' . $array_get['name'] . '_' . $array_url_invertido[0] . '"');
        header('Content-Length: ' . $len);
        readfile($array_get['url']);
    }

}