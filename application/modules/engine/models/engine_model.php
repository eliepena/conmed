<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @subpackage	        Modules
 * @category		Model
 * @author		Nohemi Rojas<nohemir@gmail.com>
 * @since               V 1.0 23/11/12 02:40 PM
 */

/**
 * CodeIgniter Engine_model Class
 *
 * This class is Engine_model
 */
class Engine_model extends AST_Model {

    /**
     * Constructor
     *
     * @access	public
     * @author  Nohemi Rojas
     * @version V-1.0 21/11/12 02:13 PM
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Se encarga de buscar lo valores de los campos relacionados a una determinada operacion
     *
     * @access	public
     * @param integer $operation_id Identificador de la operacion
     * @return  mixed Array de los valores de los registros, false en caso de que no encuentre
     * @author  Nohemi Rojas
     * @version V-1.0 21/11/12 02:13 PM
     */
    public function getFields($operation_id) {

        //Select
        $this->db->select('fie.id');
        $this->db->select('fie._name AS name');
        $this->db->select('fie.length');
        $this->db->select('fie.chk_data_type');

        $this->db->select('fie_ope.tooltip');
        $this->db->select('fie_ope.id_operation');
        $this->db->select('fie_ope.chk_component_type as type');
        $this->db->select('fie_ope.placeholder');
        $this->db->select('fie_ope.hidden');
        $this->db->select('fie_ope.read_only');
        $this->db->select('fie_ope.validations');
        $this->db->select('fie_ope._label AS label');
        $this->db->select('fie_ope.help');
        $this->db->select('fie_ope.field_filter');
        $this->db->select('fie_ope.button');
        $this->db->select('fie_ope.id_parent');
        $this->db->select('fie_ope.filter_params');
        $this->db->select('fie_ope._order');
        $this->db->select('fie_ope.disabled');
        $this->db->select('fie_ope.dir_file');

        //From e inners
        $this->db->from('acl.field_operation as fie_ope');
        $this->db->join('dynamic.field AS fie', 'fie.id = fie_ope.id_field');

        //where
        $this->db->where('fie_ope.id_operation', $operation_id);
        $this->db->where('fie_ope.deleted', '0');

        //Order
        $this->db->order_by('fie_ope._order', 'ASC');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    /**
     * Busca los valores de las categorias relacionados a una determinada tabla
     *
     * @access	public
     * @param   string $category Nombre de la categoria sin el prefijo cat_
     * @return  mixed Array de los valores de los registros, false en caso de que no encuentre
     * @author  Nohemi Rojas
     * @version V-1.0 24/11/12 02:13 PM
     */
    public function getCategorys($category) {

        $table = 'dynamic.category';
        $table_join = 'dynamic.category_category';
        //Select
        $this->db->select('id');
        $this->db->select('_label AS label');
        $this->db->select('id_parent AS class');

        //From e inners
        $this->db->from($table);
        $this->db->join($table_join, 'id = id_child', 'LEFT');

        //Where
        $this->db->where('_table', $category);
        $this->db->where('is_business', '1');

        //Order
        $this->db->order_by('_order');

        //Execute query
        $query = $this->db->get();

        //return data
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    /**
     * Busca los valores padres de las categorias relacionados a una determinada tabla
     *
     * @access	public
     * @param   string $category Nombre de la categoria sin el prefijo cat_
     * @return  mixed Array de los valores de los registros, false en caso de que no encuentre
     * @author  Nohemi Rojas
     * @version V-1.0 23/04/13 03:33 PM
     */
    public function getParentCategory($category) {
        $this->db->select('DISTINCT(id_parent) AS cat_parent, parent._label AS label');

        $this->db->from('dynamic.category_category');
        $this->db->join('dynamic.category AS child', 'id_child=child.id');
        $this->db->join('dynamic.category AS parent', 'id_parent=parent.id');

        $this->db->where('child._table', $category);

        $query = $this->db->get();

        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    /**
     * Retorna los valores de una consulta a una entidad filtrado por determinados campos
     *
     * @access  public
     * @param   string $table nombre del campo identificador de la tabla en donde se consultara
     * @param   string $fields cadena que contiene los campos separados por coma
     * @param   string $filter cadena que contiene el filtro que se desea para los campos buscados
     * @param   string $label cadena que define como se llamara el campo label en el select, por defecto text
     * @return  array Arreglo con los valores de la consulta
     * @author  Nohemi Rojas
     * @version V-1.0 15/12/12 01:02:46
     */
    public function getComboByTable($table, $fields, $filter = FALSE, $label = 'text', $where = false) {

        if (!$fields)
            return array();

        if ($filter) {
            $arr_fields = explode(',', $fields);
            foreach ($arr_fields as $key => $arr) {
                unset($arr_fields[$key]);
                $arr_fields[$arr] = $filter;
            }
        }

        //limpia los campos table y field para realizar la consulta
        $table = preg_replace('/^id_/i', '', $table);

        //Determina si la cadena de campos contiene el esquema de la tabla, de ser asi, extrae el esquema y los campos
        //si no, deja por defecto business, este esquema debe estar definido al principio y separado por |
        $schema = (strstr($fields, '|')) ? substr($fields, 0, strrpos($fields, '|')) : 'business';
        $fields = (strstr($fields, '|')) ? substr($fields, strrpos($fields, '|') + 1, strlen($fields)) : $fields;

        //Concatena los campos que se fijaron en la BD por el caracter (,)
        $fields = preg_replace('/,/i', " || ' ' || ", $fields);

        $this->db->select("$fields  AS $label");
        $this->db->select("*");

        $this->db->from("$schema.$table");

        if ($filter)
            $this->_ilike($arr_fields, 'OR', TRUE);
        if ($where)
        	$this->db->where($where);
        $query = $this->db->get();

        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    /**
     * Realiza la consulta para obtener la data encadenada de los combos
     * anidados. Como parametro recibe el siguiente fomrato JSON
     * {
     *      select: {'id', 'name AS label', 'id_COMODIN AS class'},
     *      from: '',
     *      alias: '', [Optional]
     *      where: {'field':'value'},
     *      order: '', [Optional]
     *      join:[
     *          {from:'', alias:'', inner:''}
     *      ] [Optional]
     * }
     *
     * @access  public
     * @param   array    $param  JSON con estructura active record.
     * @return  array    resultado la ejecucion del JSON.
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 10/01/13 11:52
     */
    public function getFilterParamsData($param) {
        $obj = json_decode($param, TRUE);

        //Agregamos alias si la tabla tiene alias
        $alias = empty($obj['alias']) ? '' : " AS {$obj['alias']}";

        $this->db->select($obj['select']);
        $this->db->from($obj['from'] . $alias);
        
        foreach ($obj['where'] as &$value) {
            if(strpos($value, '.') > 0)
                $value = $this->session->userdata($value);
        }
        
        $this->db->where($obj['where']);

        if (!empty($obj['join']))
            foreach ($obj['join'] as $join)
                $this->db->join("{$join['from']} AS {$join['alias']}", $join['join']);

        //Agregamos un order a la consulta por defecto label
        $this->db->order_by(empty($obj['order']) ? 'label' : $obj['order']);

        $rs = $this->db->get();

        $data = $rs->result_array();
        return $data;
    }

    /**
     * Buscar el valor de una categoria dado su identificador.
     *
     * @access  public
     * @param   string $value Valor a buscar
     * @param   string $field Campo por el cual se realizara la busqueda (id,_label)
     * @return  mixed  identificador/label de la categoria. False en caso de no encontrar
     * @author  Nohemi Rojas
     * @version V-1.0 17/01/13 11:44 AM
     *
     */
    public function getValueByCategory($value, $field = 'id') 
    {

        $filter = ($field == 'id') ? '_label' : 'id';
        $this->db->select("$filter AS value");
        $this->db->from('dynamic.category');
        $this->db->where($field, $value);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row()->value : FALSE;
    }

    /**
     * Buscar el padre de una categoria dado su identificador.
     *
     * @access  public
     * @param   string $value Valor a buscar
     * @param   string $field Campo por el cual se realizara la busqueda (id,_label)
     * @return  mixed  identificador/label de la categoria. False en caso de no encontrar
     * @author  Nohemi Rojas
     * @version V-1.0 17/01/13 11:44 AM
     *
     */
    public function getParentByCategory($id) {
        $this->db->select("parent._table AS table");
        $this->db->from('dynamic.category_category');
        $this->db->join('dynamic.category AS child', 'id_child=child.id');
        $this->db->join('dynamic.category AS parent', 'id_parent=parent.id');
        $this->db->where('child.id', $id);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row_array() : FALSE;
    }

}

// END Engine_model class
/* End of file Engine_model.php */
/* Location: application/modules/engine/models/engine_model.php */

