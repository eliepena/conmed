<?php if ($actions): ?>
<div class="form-actions">
  <?php foreach ($actions as $action) : ?>
    <?php $visible = (empty($action['visible']) ? 'dynamic-btn hide' : '') ?>
    <?php $actionJS = (empty($action['visible']) ? '_massiveAction' : 'request') ?>
    <a href="#" role="button" class="btn btn-primary <?php echo $visible ?>" data-toggle="modal" onclick="<?php echo $actionJS ?>({url:'<?= base_url($action['url']) ?>'})">
        <?php echo $action['label'] ?>
    </a>
  <?php endforeach; ?>
</div>
<?php endif; ?>