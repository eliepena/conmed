<div id="engine-calendar"></div>
<script>
    var _delete = false;
    lastTimeUpdated = '<?php echo floor(microtime(true) * 1000); ?>';
    $(document).ready(function() {

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        $('#engine-calendar').fullCalendar({
            firstDay:1, //Primer dia de la semana.
            //            header: {left: 'prev,next today',center: 'title',right: 'month,agendaWeek,agendaDay'},
            //            defaultView: 'agendaWeek',
            header: {left: 'prev,next today',center: 'title',right: 'month,basicWeek,basicDay'},
            defaultView: 'month',
            allDaySlot:false,
            allDayText:'Todo el dia',
            monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthNamesShort:['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            dayNames:['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'],
            dayNamesShort:['Dom','Lun','Mar','Mie','Jue','Vie','Sab'],
            buttonText:{
                today:'Hoy',month:'Meses',week:'Semanas',day:'Dias',
                prev:'&nbsp;&#9668;&nbsp;',next:'&nbsp;&#9658;&nbsp;',
                prevYear: '&nbsp;&lt;&lt;&nbsp;', // <<
                nextYear: '&nbsp;&gt;&gt;&nbsp;' // >>
            },
            columnFormat:{
                month: 'dddd',     // Lunes
                week: 'ddd dd/MM', // Lun 21/01
                day: 'dddd dd/MM'  // Lunes 21/01
            },
            titleFormat:{
                month: 'MMMM yyyy',                             // September 2009
                week: "MMM dd[ yyyy]{ '&#8212;'[ MMM] dd / yyyy}", // Sep 7 - 13 2009
                day: 'dddd, dd / MMM / yyyy'                  // Tuesday, Sep 8, 2009
            },
            editable: <?= $editable ?>,
            events:{url:'<?= $source_url ?>'},
            eventResize: function(event,dayDelta,minuteDelta,revertFunc) {
                if (!confirm("is this okay?"))
                    return revertFunc();
                sendEvent(event, '<?= $save_url ?>');
            },
            eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {
                if (!confirm("Are you sure about this change?"))
                    return revertFunc();
                sendEvent(event, '<?= $save_url ?>');
            },
            eventClick: function(calEvent) {
                request({no_remove:true,url:calEvent._url,data:{target_on:'window',id:calEvent.id}})
            },
            eventRender: <?php echo (isset($render)) ? $render : 'false'?>
        });
    });

    //Insertamos el intervalo creado en un arreglo para su posterior eliminacion.
    intervals['event_checker'] = setInterval("pollForNewEvents('<?= $checker_url ?>')", <?= $frequency ?>);
</script>