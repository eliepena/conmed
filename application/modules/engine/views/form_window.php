<script type="text/javascript">
    $(function(){
        <?php $reload_content = ($reload_content) ? $this->load->view("$reload_content", NULL, TRUE) : 'false'; ?>
        var form = 'form_<?php echo $operation_id?>';
        var url = '<?php echo base_url($url . '/process'); ?>';
        var modal = '#modal_<?php echo $target_on?>';
        var reload_content = <?php echo $reload_content?>;
        var _alert = '#alert_<?php echo $operation_id?>';
        formWindow(form, url, modal, _alert, reload_content);
        customFields();
    });
</script>

<?php if ($actionbar): ?>
    <?php echo $actionbar ?>
<?php endif; ?>
<div id="alert_<?php echo $operation_id?>"></div>

<form id="form_<?php echo $operation_id?>" name="form_<?php echo $operation_id?>" action="#" method="POST"  class="form-horizontal">
    <div class="modal-body">
        <ul>
            <?php $this->load->view('form.php') ?>
        </ul>
    </div>

    <div class="modal-footer">
        <?php if (!strstr($short_operation, 'detail')): ?>
            <button class="btn" data-dismiss="modal">Cancelar</button>
            <input type="submit" class="btn btn-primary update" value="<?php echo $button_submit; ?>" />
        <?php endif; ?>
    </div>
</form>