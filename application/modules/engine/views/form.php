<?php if ($fields) : ?>

    <?php foreach ($fields as $field) : ?>
        <?php $data = isset($data) ? $data : NULL ?>

        <li id="li-<?php echo $field['name'] . '-' . $field['id_operation'] ?>">

            <?php $required = (strstr($field['validations'], 'required')) ? "<span style='color:red;font-size: 16px'> *</span>" : '' ?>

            <?php $arr = array('target_on' => $target_on, 'field' => $field, 'required' => $required, 'data' => $data, 'short_operation' => $short_operation) ?>

            <?php if ($field['hidden'] == '0'): //Verifica que el campo no se oculto ?>

                <?php if ($field['type'] == 'text' OR $field['type'] == 'password'): ?>
                    <?php echo $this->load->view('form/text', $arr) ?>

                <?php elseif ($field['type'] == 'phone'): ?>
                    <?php echo $this->load->view('form/phone', $arr) ?>

                <?php elseif ($field['type'] == 'label'): ?>
                    <?php echo $this->load->view('form/label', $arr) ?>

                <?php elseif ($field['type'] == 'checkbox'): ?>
                    <?php echo $this->load->view('form/checkbox', $arr) ?>

                <?php elseif ($field['type'] == 'textarea'): ?>
                    <?php echo $this->load->view('form/textarea', $arr) ?>

                <?php elseif ($field['type'] == 'select' OR $field['type'] == 'select_dyn' OR $field['type'] == 'multiselect'): ?>
                    <?php echo $this->load->view('form/select', $arr) ?>

                <?php elseif ($field['type'] == 'date'): ?>
                    <?php echo $this->load->view('form/date', $arr) ?>

                <?php elseif ($field['type'] == 'checklist'): ?>
                    <?php echo $this->load->view('form/checklist', $arr) ?>

                <?php elseif ($field['type'] == 'time'): ?>
                    <?php echo $this->load->view('form/time', $arr) ?>

                <?php elseif ($field['type'] == 'file'): ?>
                    <?php echo $this->load->view('form/file', $arr) ?>

                <?php endif;  //Final Campos?>

            <?php else: //si el campo es oculto ?>
                <?php echo $this->load->view('form/hidden', $arr) ?>

            <?php endif; ?>

        </li>

    <?php endforeach; ?>
<?php endif; ?>