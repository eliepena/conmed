<ul class="nav nav-tabs" id="myTab">
    <?php $i = 0?>
    <?php foreach ($tabs as $key => $tab): ?>
    <li <?php echo ($i == 0) ? 'class=active' : ''; $i++?>><a data-toggle="tab" href="#<?php echo $key ?>"><?php echo $tab['label'] ?></a></li>
    <?php endforeach ?>
</ul>
<div class="tab-content" style="overflow: visible;">
    <?php $i = 0?>
    <?php foreach ($tabs as $key => $tab): ?>
        <div class="tab-pane <?php echo ($i == 0) ? 'active' : ''; $i++?> " id="<?php echo $key ?>">
            <div id="<?php echo $key ?>-container">
                <?php echo $tab['view'] ?>
            </div>
        </div>
    <?php endforeach ?>
</div>