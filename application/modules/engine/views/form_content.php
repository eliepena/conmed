<script type="text/javascript">
    $(function(){
        //Ejecuta las Validaciones del Cliente
        new FormValidator('_form', buildRules('_form'),
        function(errors, event){
            var flag = validateForm(errors, event);

            if (event && event.preventDefault) {
                event.preventDefault();
            } else if (event) {
                event.returnValue = false;
            }

            //validacion submit
            if(flag){
                
            //este archivo es particular,
            <?php echo $this->load->view("$after_validation", array('url' => $url)) ?>
            }
        }
    );
    
        customFields();

    });
</script>
<?php if (isset($actionbar)): ?>
    <?php echo $actionbar ?>
<?php endif; ?>

<div id="alert_content"></div>
<div id="link"></div>
<h3><?php echo $operation_title ?></h3>

<form id="_form" name="_form" action="#" method="POST" class="form-horizontal">
    <ul>
        <?php $this->load->view('form.php') ?>

        <?php if (!strstr($short_operation, 'detail')): ?>
            <li><br/>
                <div class="form-actions ">
                    <input type="submit" class="btn btn-primary" value="<?php echo $button_submit ?>" />
                </div>
            </li>
        <?php endif; ?>
    </ul>
</form>