<div class="control-group">
    <label class="control-label" for="<?php echo $field['name'] . '-' . $field['id_operation'] ?>"><?php echo $field['label'] ?><?php echo $required ?></label>

    <div class="controls"><input type="<?php echo $field['type'] ?>"
                                 class="checkbox"
                                 name="<?php echo $field['name'] ?>"
                                 id="<?php echo $field['name'] . '-' . $field['id_operation'] ?>"
                                 data-rules="<?php echo $field['validations'] ?>"
                                 data-display="<?php echo $field['label'] ?>"
                                 value="1"
                                 data-content="<?php echo $field['tooltip'] ?>"
                                 title="<?php echo $field['label'] ?>"
                                 <?php echo ((isset($data[$field['name']])) and $data[$field['name']] == '1') ? "checked=checked" : '' ?>
                                 <?php echo ($field['read_only']) ? "disabled=disabled" : '' ?>
                                 />
    </div>
</div>