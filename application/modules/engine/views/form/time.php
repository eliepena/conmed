<div class="control-group">
    <label class="control-label" for="<?php echo $field['name'] . '-' . $field['id_operation'] ?>"><?php echo $field['label'] ?><?php echo $required ?></label>
    <div class="controls">
        
        <?php if ($short_operation != 'detail'): ?>
        <?php // documentacion de Timepicker en http://jdewit.github.io/bootstrap-timepicker/# ?>
        <div class="input-append bootstrap-timepicker">
            <input
                name="<?php echo $field['name'] ?>"
                id="<?php echo $field['name'] . '-' . $field['id_operation'] ?>"
                value="<?php echo (isset($data[$field['name']])) ? $data[$field['name']] : '' ?>"
                type="text" 
                data-rules="<?php echo $field['validations'] ?>"
                class="input-small bootstrap-timepicker-input"
                data-content="<?php echo $field['tooltip'] ?>"
                data-display="<?php echo $field['label'] ?>"
                title="<?php echo $field['label'] ?>"
                placeholder="<?php echo $field['placeholder'] ?>"
                >
            
            <span class="add-on"><i class="icon-time"></i></span>
        </div>
        <?php else: ?>

            <input type="text"
                   name="<?php echo $field['name'] ?>"
                   id="<?php echo $field['name'] . '-' . $field['id_operation'] ?>"
                   class="input-medium"
                   value="<?php echo (isset($data[$field['name']])) ? $data[$field['name']] : NULL ?>"
                   data-display="<?php echo $field['label'] ?>"
                   data-content="<?php echo $field['tooltip'] ?>"
                   title="<?php echo $field['label'] ?>"
                   <?php echo ($field['read_only']) ? "readonly=readonly" : '' ?>
                   >
               <?php endif; ?>
    </div>
</div>