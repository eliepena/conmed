<div class="control-group">
    <label class="control-label" for="<?php echo $field['id'] ?>"><?php echo $field['label'] ?><?php echo $required ?></label>
    <div class="controls">
        <?php if (!strstr($short_operation, 'detail')): ?>
            <select name="<?php echo ($field['type'] == 'multiselect') ? $field['name'] . '[]' : $field['name'] ?>"
                    id="<?php echo $field['id'] ?>"
                    data-placeholder="<?php echo $field['placeholder'] ?>"
                    class="chzn-select"
                    data-rules="<?php echo $field['validations'] ?>"
                    data-display="<?php echo $field['label'] ?>"
                    data-content="<?php echo $field['tooltip'] ?>"
                    title="<?php echo $field['label'] ?>"
                    <?php echo ($field['type'] == 'multiselect') ? "multiple='multiple'" : '' ?>
                    <?php echo ($field['disabled']) ? "disabled=disabled" : '' ?>
                    >

                <option value=""></option>

                <?php if (isset($field['cat_parent'])): //Verifica que tiene OptionGroup ?>

                    <?php foreach ($field['cat_parent'] as $cat_parent): ?>
                        <optgroup label="<?php echo $cat_parent['label'] ?>">
                            <?php foreach ($field['value'] as $value): ?>
                                <?php if ($value['class'] == $cat_parent['cat_parent']): ?>
                                    <option value = "<?php echo $value['id'] ?>"><?php echo $value['label'] ?></option>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </optgroup>
                    <?php endforeach; ?>
                <?php else: ?>
                    <?php if (isset($field['value']) && !empty($field['value'])): ?>
                        <?php foreach ($field['value'] as $value): ?>
                            <?php
                            //Variable Provee la funcionalidad de encadenamiento de combos
                            $chained = '';
                            if (!empty($field['id_parent'])):
                                $chained = "class='{$value['class']}'";
                            endif;
                            $selected = '';
                            //valida si el campo tiene valor o si es un arreglo para select multiple
                            if (
                                    isset($data[$field['name']])
                                    and (
                                            ($value['id'] == $data[$field['name']])
                                            or
                                            (is_array($data[$field['name']]) and in_array($value['id'], $data[$field['name']]) )
                                        )
                                ) :
                                $selected = 'selected="true"';
                            endif;
                            ?>
                            <option value="<?php echo $value['id'] ?>" <?= $chained ?> <?= $selected ?>><?php echo $value['label'] ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                <?php endif; ?>
            </select>
            <script type="text/javascript">
    <?php if (!isset($field['value'])): ?>
            $('#<?php echo $field['id'] ?>').ajaxChosen({
                dataType: 'json',
                type: 'GET',
                url:"<?= base_url() ?>engine/engine/EXC_searchSelect?name=<?php echo $field['name'] ?>&filter=<?php echo $field['field_filter'] ?>"
            },{
                loadingImg: '<?= base_url() ?>assets/img/loading.gif'
            });

        <?php
    else:
        if (isset($field['dependent'])):
            echo $field['dependent'];
        endif;
    endif;
    ?>
            </script>
            <?php if (isset($field['button'])): ?>
                <?php $arr = array('target_on' => $target_on, 'field' => $field, 'data' => $data) ?>
                <?php echo $this->load->view('form/button', $arr) ?>
            <?php else: ?>
                <span id="button-<?php echo $field['name'] ?>"></span>
            <?php endif; ?>
        <?php else: //si el campo es diferente de detail y es select, entonces coloca un campo text  ?>
            <?php if (isset($field['value'])): ?>
                <?php $label = '' ?>
                <?php foreach ($field['value'] as $value): ?>
                    <?php if ($value['id'] == $data[$field['name']]) : ?>
                        <?php $label = $value['label'] ?>
                        <?php break; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
                <input type="text"
                       class="input-xlarge"
                       name="<?php echo $field['name'] ?>"
                       id="<?php echo $field['name'] . '-' . $field['id_operation'] ?>"
                       placeholder="<?php echo $field['placeholder'] ?>"
                       data-rules="<?php echo $field['validations'] ?>"
                       data-display="<?php echo $field['label'] ?>"
                       value="<?php echo $label ?>"
                       data-content="<?php echo $field['tooltip'] ?>"
                       title="<?php echo $field['label'] ?>"
                       <?php echo ($field['read_only']) ? "readonly=readonly" : '' ?>
                       />
            <?php endif; ?>
        <?php endif; ?>
    </div></div>