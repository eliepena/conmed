<?php $valor = '';?>
<div class="control-group">
    <label class="control-label"><?php echo $field['label'] ?><?php echo $required ?></label>
    <div class="controls" data-content="<?php echo $field['tooltip'] ?>">
        <ul>
        <?php foreach ($field['value'] as $value): ?>
            <?php
                $checked = '';
                if(isset($data) AND in_array($value['id'], $data[$field['name']])){
                    $checked = 'checked=checked';
                    $valor .= $value['id'] . ',';
                }
            ?>
            <li>
                <div class="control-group">
                    <label class="control-label checklist-control-label" for="<?php echo $field['id'],'-',$value['id'] ?>"><?php echo $value['label'] ?></label>
                    <div class="controls checklist-controls">
                        <input type="checkbox"
                           class="checkbox checklist-<?php echo $field['id']?>"
                           name="<?php echo $field['name'] ?>[]"
                           id="<?php echo $field['id'],'-',$value['id'] ?>"
                           value="<?php echo $value['id']?>"
                           <?php echo $checked ?>
                           <?php echo ($field['read_only']) ? "disabled=disabled" : '' ?>
                        />
                        <!--
                        data-rules="<?php echo $field['validations'] ?>"
                        title="<?php echo $value['label'] ?>"
                        data-display="<?php echo $field['label'] ?>"
                        -->
                    </div>
                </div>
            </li>
        <?php endforeach;?>
        <?php $valor = trim($valor,',');?>
        </ul>
        <input type="hidden"
           name="<?php echo $field['name'] ?>"
           id="<?php echo $field['name'] . '-' . $field['id_operation'] ?>"
           value="<?php echo $valor?>"
        />
    </div>
</div>
<script type="text/javascript">
    checklist("checklist-<?php echo $field['id']?>", "<?php echo $field['name'],'-',$field['id_operation'] ?>");
</script>