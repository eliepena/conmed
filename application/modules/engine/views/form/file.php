<?php
//VARIABLES DE INICIALIZACION
$path_uploads = empty($field['dir_file'])?$this->config->item('uploads').'/':$field['dir_file'];
$thum_show = ' hide ';
$link_file_show = 'hide';
$thumb = $this->config->item('path_image_no_found');
$file_url = '';
$url_file_down = " bootbox.alert('Disculpe! No se puede descargar el archivo.');";
$img_not_thumb = '';
$image_types = array('gif', 'jpg', 'jpeg', 'png', 'jpe');
if (isset($data[$field['name']])):
    $file = explode('.', $data[$field['name']]);
    $ext = end($file);
    if (file_exists($path_uploads.$data[$field['name']]) && isset($ext)):
        if (in_array($ext, $image_types)):
            $thumb = $path_uploads.$file[0] . '.'. $ext;
            $img_not_thumb = $data[$field['name']];
            $thum_show = '';
        else:
            $file_url = $data[$field['name']];
            $url_file_down = "force_download('$file_url');";
            $link_file_show = '';
        endif;
    endif;
endif;
?>


<div class="control-group">
    <label class="control-label"  for="<?php echo $field['name'] . '-' . $field['id_operation'] ?>"><?php echo $field['label'] ?><?php echo $required ?></label>
    <div class="controls">
        <?php
        if (!strstr($short_operation, 'detail')):
            ?>
            <div class="fileupload fileupload-new" data-provides="fileupload">
                <div id="thumb-div-<?php echo $field['name'] . '-' . $field['id_operation'] ?>" class="fileupload-new thumbnail <?php echo $thum_show; ?>" style="width: 150px; height: 150px;">
                    <center>
                        <img
                            width="100%"
                            height="100%"
                            id="thumbnail-<?php echo $field['name'] . '-' . $field['id_operation'] ?>"

                            src="<?php echo $thumb; ?>" />
                    </center>
                </div>
                <div id ="progress-parent" class="fileupload-preview fileupload-exists thumbnail hide" style="max-width: 150px; max-height: 20px; line-height: 20px;">

                    <div id ="progress" class="progress progress-striped "><div class="bar" style="width: 100%;"></div></div>
                </div>
                <div>
                    <input type="text"
                           name="<?php echo $field['name']; ?>"
                           id="<?php echo $field['name'] . '-' . $field['id_operation']; ?>"
                           placeholder="Sin archivo"
                           data-rules="<?php echo $field['validations']; ?>"
                           data-display="<?php echo $field['label']; ?>"
                           data-content="<?php echo $field['tooltip']; ?>"
                           readonly="readonly"
                           style="width:150px;"
                           value="<?php echo (isset($data[$field['name']])) ? $data[$field['name']] : '' ?>"


                           />
                    <span class="btn btn-success fileinput-button">
                        <i class="icon icon-plus"></i>
                        <input type="file"
                               name="<?php echo $field['name'] . '-' . $field['id_operation']; ?>-file"
                               id="<?php echo $field['name'] . '-' . $field['id_operation']; ?>-file"
                               onchange="sube_archivo_img('<?= base_url() ?>engine/engine/EXC_uploadFile/','<?php echo $field['name'] . '-' . $field['id_operation']; ?>-file','<?php echo $field['dir_file']; ?>');"
                               />
                    </span>

                    <span id ="<?php echo $field['name'] . '-' . $field['id_operation'] ?>-trash" class="btn btn-danger">
                        <i class="icon icon-trash"></i>
                    </span>
                </div>
            </div>
            <div class='<?php echo $link_file_show; ?>' id="dir-file-<?php echo $field['name'] . '-' . $field['id_operation'] ?>"><a onclick="<?php echo $url_file_down; ?>" src="#">Descargar Archivo Adjunto</a></div>
        <?php else: ?>
            <a onclick="show_img_file('<?php echo $path_uploads.$img_not_thumb; ?>');">
                <div class="fileupload-new thumbnail <?php echo $thum_show; ?>" style="width: 150px; height: 150px;">
                    <center>
                        <img
                            width="100%"
                            height="100%"
                            id="thumbnail-<?php echo $field['name'] . '-' . $field['id_operation'] ?>"

                            src="<?php echo $thumb; ?>" />

                    </center>
                </div>
            </a>
            <div class='<?php echo $link_file_show; ?>' id="dir-file-<?php echo $field['name'] . '-' . $field['id_operation'] ?>"><a onclick="<?php echo $url_file_down; ?>" src="<?php echo $file_url; ?>">Descargar Archivo Adjunto</a></div>


        <?php endif; ?>
    </div>
</div>
<script type="text/javascript" >
    $(function() {
        show_img_file=function (img){

            //Seteamos el titulo de la ventana modal.
            $('#fileModalLabel').text('Vista Previa  ');

            //Seteamos la vista al modal window
            var src = ($.trim(img).length > 0)?BASE_URL +  img: PATH_IMG_NO_FOUND;
            var eImg = $('#selected-file-image');
            eImg.attr('src', src);
            eImg.attr('title', 'Vista Previa ');

            //Mostramos el modal.
            $('#fileModal').modal('show');
            return false;
        }
        force_download=function (file){
            if(file !=''){
                pop_pup = window.open(BASE_URL +"engine/engine/EXC_force_download?url="+file+"&name=file",'file', 'width=1,height=1,scrollbars=NO');
                if(!pop_pup){
                    bootbox.alert('Disculpe! Verifique que las ventanas emergentes de su navegador no esten bloqueadas.');
                }else{
                    setInterval(function() {
                        pop_pup.close();
                    }, 9000);
                }

            }
        }
        sube_archivo_img = function(url,element,dir_file){

            var element_progressBar_parent = '#progress-parent';
            var element_progressBar = '#progress';
            $(element_progressBar_parent).removeClass('hide');
            $(element_progressBar).addClass('active');
            $.ajaxFileUpload({
                url : url,
                secureuri : false,
                fileElementId :element,
                dataType : 'json',
                data : { 'element_id' : element, 'custom_dir':dir_file},
                success  : function (data) {
                    if(data.estatus){
                        if(data.is_image){
                            $('#dir-file-<?php echo $field['name'] . '-' . $field['id_operation'] ?>').addClass('hide');
                            $('#thumb-div-<?php echo $field['name'] . '-' . $field['id_operation'] ?>').removeClass('hide');
                            $('#thumbnail-<?php echo $field['name'] . '-' . $field['id_operation'] ?>').attr('src', data.image_thumb);
                            $('#<?php echo $field['name'] . '-' . $field['id_operation'] ?>').val(data.file_path);

                        }else{
                            $('#thumb-div-<?php echo $field['name'] . '-' . $field['id_operation'] ?>').addClass('hide');
                            $('#<?php echo $field['name'] . '-' . $field['id_operation'] ?>').val(data.file_path);
                            $('#dir-file-<?php echo $field['name'] . '-' . $field['id_operation'] ?>').removeClass('hide');
                            $('#dir-file-<?php echo $field['name'] . '-' . $field['id_operation'] ?> a').attr('onclick', 'force_download(\''+data.file_path+'\');');
                            $('#dir-file-<?php echo $field['name'] . '-' . $field['id_operation'] ?> a').html(data.origin_name+'  '+data.file_size);
                        }
                        $(element_progressBar).removeClass('active');
                        $(element_progressBar_parent).addClass('hide');
                    }else{
                        bootbox.alert(data.mensaje);
                        $(element_progressBar).removeClass('active');
                        $(element_progressBar_parent).addClass('hide');

                    }

                    if(data.estatus != 'error') {
                        $(element_progressBar).removeClass('active');
                        $(element_progressBar_parent).addClass('hide');
                    }else{
                        $(element_progressBar).removeClass('active');
                        $(element_progressBar_parent).addClass('hide');

                    }
                }

            });
            return false;
        }

        $('#<?php echo $field['name'] . '-' . $field['id_operation'] ?>-trash').click(function(){
            $('#<?php echo $field['name'] . '-' . $field['id_operation'] ?>').val('');
            $('#thumb-div-<?php echo $field['name'] . '-' . $field['id_operation'] ?>').addClass('hide');
            $('#dir-file-<?php echo $field['name'] . '-' . $field['id_operation'] ?>').addClass('hide');
            $('#thumbnail-<?php echo $field['name'] . '-' . $field['id_operation'] ?>').attr('src', PATH_IMG_NO_FOUND);
        });


    });



</script>
<style>

    .fileinput-button {
        position: relative;
        overflow: hidden;
    }


    .fileinput-button input {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        opacity: 0;
        filter: alpha(opacity=0);
        transform: translate(-300px, 0) scale(4);
        font-size: 23px;
        direction: ltr;
        cursor: pointer;
    }
</style>

