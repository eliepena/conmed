<?php if ($field['type'] == 'text'): ?>
    <input type="hidden"
           class="input-xlarge"
           name="<?php echo $field['name'] ?>"
           id="<?php echo $field['name'] . '-' . $field['id_operation'] ?>"
           value="<?php echo (isset($data[$field['name']])) ? $data[$field['name']] : '' ?>"
           />
<?php endif; ?>
