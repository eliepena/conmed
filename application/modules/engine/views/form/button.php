<?php $target_on = ($target_on == 'window') ? 'window2' : 'window' ?>


<?php foreach ($this->session->userdata('permissions') as $permission) : ?>

    <?php if ($permission['id'] == $field['button']): ?>
        <span id="button-<?php echo $field['name'] ?>">
            <?php //Si un campo text necesita mostrar sus valores, entonces debera contener un valor del tipo nombre_id_ec?>
            <?php $id_ec = (isset($data[$field['name']])) ? $data[$field['name'] . '_id_ec'] : NULL ?>
            <a href="#" role="button" class="btn btn-primary" data-toggle="modal"
               onclick="request({url:'<?php echo base_url($permission['url']) ?>', data:{id:'<?php echo $id_ec ?>',target_on:'<?php echo $target_on ?>'}})">
                   <?php echo $permission['_name'] ?>
            </a>
        </span>
        <?php break; ?>
    <?php endif; ?>

<?php endforeach; ?>