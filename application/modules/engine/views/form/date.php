<div class="control-group">
    <label class="control-label" for="<?php echo $field['name'] . '-' . $field['id_operation'] ?>"><?php echo $field['label'] ?><?php echo $required ?></label>
    <div class="controls">
        <?php if (!strstr($short_operation, 'detail')): ?>
<?php //** Documentacion del plugin Datepicker  
      http://www.eyecon.ro/bootstrap-datepicker/ **//
?>
            <div class="input-append date" data-date="" data-date-format="dd-mm-yyyy">
                <input class="date input-small" type="text"
                       id="<?php echo $field['name'] . '-' . $field['id_operation'] ?>"
                       name="<?php echo $field['name'] ?>"
                       data-rules="<?php echo $field['validations'] ?>"
                       data-display="<?php echo $field['label'] ?>"
                       data-content="<?php echo $field['tooltip'] ?>"
                       title="<?php echo $field['label'] ?>"
                       placeholder="<?php echo $field['placeholder'] ?>"
                       readonly="readonly"
                       value="<?php echo (isset($data[$field['name']])) ? date('d-m-Y', strtotime($data[$field['name']])) : '' ?>">
                       
                <span class="add-on"><i class="icon-calendar"></i></span>
                </div>
        <?php else: ?>

            <input type="text"
                   name="<?php echo $field['name'] ?>"
                   id="<?php echo $field['name'] . '-' . $field['id_operation'] ?>"
                   class="input-medium"
                   value="<?php echo (isset($data[$field['name']])) ? date('d-m-Y', strtotime($data[$field['name']])) : NULL ?>"
                   data-display="<?php echo $field['label'] ?>"
                   data-content="<?php echo $field['tooltip'] ?>"
                   title="<?php echo $field['label'] ?>"
                   <?php echo ($field['read_only']) ? "readonly=readonly" : '' ?>
                   >
               <?php endif; ?>
    </div>
</div>