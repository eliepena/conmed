<div class="control-group">
    <label class="control-label"  for="<?php echo $field['name'] . '-' . $field['id_operation'] ?>"><?php echo $field['label'] ?><?php echo $required ?></label>
    <div class="controls">
        <input type="<?php echo $field['type'] ?>"
               class="input-xlarge"
               name="<?php echo $field['name'] ?>"
               id="<?php echo $field['name'] . '-' . $field['id_operation'] ?>"
               placeholder="<?php echo $field['placeholder'] ?>"
               data-rules="<?php echo $field['validations'] ?>"
               data-display="<?php echo $field['label'] ?>"
               value="<?php echo (isset($data[$field['name']])) ? $data[$field['name']] : '' ?>"
               data-content="<?php echo $field['tooltip'] ?>"
               title="<?php echo $field['label'] ?>"
               onblur="limpiar(this.id)"
               <?php echo ($field['read_only']) ? "readonly=readonly" : '' ?>
               <?php echo ($field['disabled']) ? "disabled=disabled" : '' ?>
               />


        <?php if (isset($field['button'])): ?>
            <?php $arr = array('target_on' => $target_on, 'field' => $field, 'data' => $data) ?>
            <?php echo $this->load->view('form/button', $arr) ?>
        <?php endif; ?>
        
    </div>
</div>
