<div class="control-group">
    <label  class="control-label" for="<?php echo $field['name'] . '-' . $field['id_operation'] ?>"><?php echo $field['label'] ?><?php echo $required ?></label>
    <div class="controls">
        <?php if (!strstr($short_operation, 'detail')): ?>
            <input type="text"
                   class="input-medium bfh-phone"
                   name="<?php echo $field['name'] ?>"
                   id="<?php echo $field['name'] . '-' . $field['id_operation'] ?>"
                   placeholder="<?php echo $field['placeholder'] ?>"
                   data-rules="<?php echo $field['validations'] ?>"
                   data-display="<?php echo $field['label'] ?>"
                   data-number="<?php echo (isset($data[$field['name']])) ? $data[$field['name']] : '' ?>"
                   data-format="<?php echo (isset($data[$field['name']]))? '(0ddd) ddd-dddd' : '(dddd) ddd-dddd' ?>"
                   data-content="<?php echo $field['tooltip'] ?>"
                   title="<?php echo $field['label'] ?>"
                   <?php echo ($field['read_only']) ? "readonly=readonly" : '' ?>
                   <?php echo ($field['disabled']) ? "disabled=disabled" : '' ?>
                   />
               <?php else: ?>

            <input type="text"
                   name="<?php echo $field['name'] ?>"
                   id="<?php echo $field['name'] . '-' . $field['id_operation'] ?>"
                   class="input-medium"
                   value="<?php echo (isset($data[$field['name']])) ? $data[$field['name']] : NULL ?>"
                   data-display="<?php echo $field['label'] ?>"
                   data-content="<?php echo $field['tooltip'] ?>"
                   title="<?php echo $field['label'] ?>"
                   <?php echo ($field['read_only']) ? "readonly=readonly" : '' ?>
                   >
               <?php endif; ?>
    </div>
</div>