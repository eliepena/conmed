<div class="control-group">
    <label class="control-label" for="<?php echo $field['name'] . '-' . $field['id_operation'] ?>"><?php echo $field['label'] ?><?php echo $required ?></label>
    <div class="controls">
        <textarea
            class="span3"
            name="<?php echo $field['name'] ?>"
            id="<?php echo $field['name'] . '-' . $field['id_operation'] ?>"
            placeholder="<?php echo $field['placeholder'] ?>"
            data-rules="<?php echo $field['validations'] ?>"
            data-display="<?php echo $field['label'] ?>"
            data-content="<?php echo $field['tooltip'] ?>"
            title="<?php echo $field['label'] ?>"
            rows="5"
            <?php echo ($field['read_only']) ? "readonly=readonly" : '' ?>
            <?php echo ($field['disabled']) ? "disabled=disabled" : '' ?>
            ><?php echo (isset($data[$field['name']])) ? $data[$field['name']] : '' ?></textarea>
    </div>
</div>