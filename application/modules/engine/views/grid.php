<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * Provee el esqueleto basico la creacion de grids por parte del ViewEngine.
 * @params  array   $headers    Contiene las caberas del grid.
 * @params  array   $data    Es una matriz con los datos a ser mostrados en el grid.
 * @author  Jose A Rodriguez E. <josearodrigueze@gmail.com>
 * @version V 1.0 23/11/2012 09:00:00
 */

//Definimos la cantida que podra seleccionar la cantidad de registros a ver.
$limit = $this->config->item('limit');
$limits = json_encode(array($limit, ($limit * 2), ($limit * 4)));

//Definimos el id del grid
$grid_id = "grid_$operation_id";

/**
 * Variable que contendra el nombre de la variable JS donde se cargara el datatables
 * @var String
 */
$tablaJS = NULL;
//Creamos la variable JS en funcion de donde se esta cargando la tabla.
switch ($target_on) {
    case 'window':
        $tablaJS = 'tabla_window';
        break;

    case 'window2':
        $tablaJS = 'tabla_window2';
        break;

    default:
        $tablaJS = 'tabla';
        break;
}


?>
<script type="text/javascript">
$(function(){
    oCache={iCacheLower:-1};
    <?php echo $tablaJS?>=$('#<?php echo $grid_id?>').dataTable({
        aLengthMenu:[<?= $limits ?>,<?= $limits ?>],
        oLanguage:{sUrl:"<?= base_url('assets/i18n/es/datatables.txt') ?>"},
        <?php if (!empty($definition_columns)): ?>
            "aoColumnDefs":[
            <?php foreach ($definition_columns as $position_column => $definition_column) : ?>
                {
                    "mRender":function(data,type,row){return'<?= $definition_column ?>';},
                    "aTargets":[<?= $position_column ?>]
                },
            <?php endforeach; ?>
            ],
        <?php endif; ?>
        'aoColumns':[
          <?php foreach ($fields_name as $field_name) : ?>
            {'mData':'<?= $field_name ?>'},
          <?php endforeach; ?>
        ],
        bProcessing:true,bServerSide:true,
        sServerMethod:'POST',iDisplayLength:<?= $limit ?>,
        sAjaxSource:'<?= base_url($operation_url) ?>',
        fnServerData:fnDataTablesPipeline
    });
});
</script><h3><?php echo $title ?></h3>
<?php if (isset($actionbar)): ?>
    <?php echo $actionbar ?>
<?php endif; ?>
<div style="margin-top: 10px; margin-left: 20px">
  <table id="<?= $grid_id ?>" class="js-table table table-striped table-bordered">
    <thead>
      <tr>
        <?php foreach ($headers as $header) : ?>
        <th><?= $header ?></th>
        <?php endforeach; ?>
      </tr>
    </thead>
<?php /* ?>
        <tbody>
            <?php if (!empty($data)): ?>
                <?php foreach ($data as $row_array) : ?>
                    <tr>
                        <?php foreach ($row_array as $row_data) : ?>
                            <td><?= $row_data ?></td>
                        <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
        <tfoot>
            <tr>
            <?php foreach ($fields_name as $field_name) : ?>
                <th><input type="text" name="<?= $field_name ?>" value="Busqueda por <?= $field_name ?>" class="search_init" /></th>
            <?php endforeach; ?>
            </tr>
        </tfoot>
<?php */ ?>
  </table>
</div>