    $('body').modalmanager('loading');
    $.ajax({
        url: '<?php echo base_url($url . '/process') ?>',
        data: $('#_form').serialize(),
        success: function(response){
           $('body').modalmanager('loading');
            //Se procesa cuando el registro se guarda exitosamente
            if(response.view == 1){
            
                var textAlert = 'Datos guardados satisfactoriamente';
                var alertHtml = [
                    '<div class="alert alert-success fade in">',
                    '<button type="button" class="close" data-dismiss="alert">×</button>',
                    textAlert,
                    '</div>'
                ].join('');

                $("#_form").hide();

                var link ='<a href="#" class="btn btn-success" onclick="request({url:\'<?php echo $redirect ?>\'})">Regresar al listado</a>' ;
                $("#link").html(link);
                $('#link').on('click', function() {
                             request({url:'<?php echo $redirect ?>'})    
                     });
                    setTimeout(function(){$('#link').trigger('click')},1500);
            }

            //Se procesa cuando no se puede guardar el registro
            else{
                //Ocurre cuando hay un error en Base de Datos
                if(response.view == false)
                    var textAlert = 'No se pudo guardar el registro, si el problema persiste comuniquese con sistemas';

                //Ocurre cuando hay errores de validacion en el Servidor
                else
                    var textAlert = response.view;

                //Mensaje de Alerta Error
                var alertHtml = [
                    '<div class="alert alert-error fade in">',
                    '<button type="button" class="close" data-dismiss="alert">×</button>',
                    textAlert,
                    '</div>'
                ].join('');

            }
            //Elemento que muestra el Alert
            var target = $("#alert_content");
            target.html(alertHtml);

        }
    })