<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


//Definimos la cantida que podra seleccionar la cantidad de registros a ver.
$limit = $this->config->item('limit');
$limits = json_encode(array($limit, ($limit * 2), ($limit * 4)));

//Definimos el id del grid
$grid_id = "grouping_grid_$operation_id";
?>
<script type="text/javascript">
    $(function(){

        tabla=$('#<?php echo $grid_id ?>').dataTable({
        
            oLanguage:{ "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "fnInfoCallback": null}, bProcessing:true
            ,
<?php if (!empty($definition_columns)): ?>
            "aoColumnDefs":[
            <?php foreach ($definition_columns as $position_column => $definition_column) : ?>
                {
                    "mRender":function(data,type,row){return'<?= $definition_column ?>';},
                    "aTargets":[<?= ($position_column ) ?>]
                },
            <?php endforeach; ?>
            ],
        <?php endif; ?>
            bLengthChange: false
            , bPaginate: false
            
        }).rowGrouping({bExpandableGrouping: true,iGroupingColumnIndex: <?= $grouping ?>});
    });
</script>
<h3><?php echo $title ?></h3>
<?php if (isset($actionbar)): ?>
    <?php echo $actionbar ?>
<?php endif; ?>
<div style="margin-top: 10px; margin-left: 20px">
    <table id="<?= $grid_id ?>" class="js-table table table-striped table-bordered">
        <thead>
            <tr>
                <?php foreach ($headers as $header) : ?>
                    <th><?= $header ?></th>
                <?php endforeach;
                ?>
            </tr>
        </thead>
        
        <tbody>
            <?php if (!empty($data)): ?>

                <?php foreach ($data as $key => $value) : ?>
                    <tr>
                        <?php if (!empty($definition_columns)): ?>
                            <?php foreach ($definition_columns as $position_column => $definition_column) : ?>
                                <td> <?= $value['id'] ?></td>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <?php foreach ($value as $k => $val) : ?>
                            <?php if ($k != 'id'): ?>

                                <td name="<?= $k ?>"><?= $val ?></td>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </tr>

                <?php endforeach; ?>

            <?php endif; ?>
        </tbody>
         
      
    </table>
</div>