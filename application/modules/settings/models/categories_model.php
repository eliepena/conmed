<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Categories_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad Categories
 * y sus entidades dependientes o extendidas
 *
 * @package Modules
 * @category Model
 * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V-1.0 27/08/13 12:05 PM
 */
class Categories_model extends AST_Model {

    private $table;

    public function __construct() {

        parent::__construct();
        $this->setSchema('dynamic');
        $this->init('category');
        $this->super_table = false;
        $this->table = $this->concatSchemaTable();
    }

    public function whereDelete($delete = '0') {

    }

    /**
     * Dado un arreglo se obtiene todas las coincidencias del arreglo por
     * clave/valor, donde clave es el campo la base de datos y valor el valor a
     * ser filtrado.
     *
     * @access  public
     * @param   Array   $params <p>Contiene los pares clave valor a ser filtrados.</p>
     * @param   boolean $returned [optional] <p>
     *      <b>TRUE(default)</b> retorna el resultado de la consulta a BD.
     *      <b>FALSE</b> realiza la ejecucion del metodo where de CI.</p>
     * @return  mixed   <p>
     *  <b>Array<b/> con el resultado de la consulta.
     *  <b>Void<b/> setea el where del metodo de CI.</p>
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V-1.0 27/08/13 03:38 PM
     */
    function getByFields($params = array()) {

        //Select
        $this->db->select($this->table . '.id');
        $this->db->select($this->table . '._table');
        $this->db->select($this->table . '._label');
        $this->db->select($this->table . '.alt_value');
        $this->db->select($this->table . '._order');
        $this->db->select("(CASE WHEN $this->table.is_business='1' THEN 'SI' ELSE 'NO' END) as is_business", FALSE);


        //From e inners
        $this->db->from($this->table);

        if (!empty($params)) {
            unset($params['_order']);
            unset($params['is_business']);
            $this->_ilike($params, 'OR', TRUE);
        }

        $debug = debug_backtrace();
        if ($debug[1]['function'] == 'getAll')
            $this->db->order_by('_label ASC');
    }

    /**
     * Devuelve los datos basicos de Categorias
     *
     * @access public
     * @param integer $id Identificador de Categorias
     * @param string  $campo Campos a mostrar en el select (default:'*')
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 31/07/13 12:35 PM
     */
    function getCategory($where = array(), $campo = '') {

        $this->db->select((empty($campo) ? ' * ' : ' ' . $campo . ' '), false);
        $this->db->from('dynamic.category');
        if (sizeof($where) > 0):
            $this->db->where($where);
        endif;
        $this->db->order_by('_label');

        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

}