<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Categories
 *
 * Esta clase provee la implementacion para la capa de control de
 * acciones de la entidad  Categoriesy sus entidades dependientes o extendidas
 *
 * @package Module
 * @category Controller
 * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V-1.0 27/08/13 12:05 PM
 */
class Categories extends AST_Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Implementacion basica para la edicion de registros, por un lado llama a
     * a la vista de edicion de registros y por otro lado guarda el registro en
     * base de datos.
     *
     * @access public
     * @param array $params Parametros adicionales enviados al controlador.
     * @author Jose  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 27/08/13 05:38 PM
     */
    function delete() {

        if ($this->input->is_ajax_request() && $this->input->post()) {

            $content = 'La pagina solicitada';
            $title = 'Error!!!';

            // Verificamos si el borrado es masivo o de uno solo
            $id_request = $this->input->post('id');
            if (is_array($id_request))
                foreach ($id_request as &$id_ec) {
                    $id_ec = $this->encrypt->decode($id_ec);
                }
            else
                $id_request = $this->encrypt->decode($id_request);


            //Arreglo de parametros a ser pasados al format.
            $params = array(
                'format_type' => 'delete',
                'data' => array()
            );
            $data = $this->_format($params);
            $success = $this->Categories_model->delete($id_request);

            if ($success) {
                $content = '<div class="modal-body">Registro eliminado saisfactoriamente.</div>';
                $title = 'Exito';
            } else
                $content = '<div class="modal-body">No se pudo eliminar el registro. Por favor intente de nuevo.</div>';

            $this->setTitle($title);
            $this->setContent($content);

            // -----------------------------
            //seteamos los datos de auditoria
            $this->audit->setOperationData(array('id' => $id_request));
            $this->audit->setChkResult($success);
            // -----------------------------
        }else {
            show_404();
        }
    }

}
