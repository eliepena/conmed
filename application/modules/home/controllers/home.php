<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @subpackage	        Modules
 * @category		Controllers
 * @author		Nohemi Rojas<nohemir@gmail.com>
 * @since               V 1.0
 */
class Home extends AST_Controller {

    /**
     * Constructor
     *
     * @access	public
     * @author  Nohemi Rojas
     * @version V-1.0 21/11/12 02:34 PM
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('view_engine');
        $this->load->model('calendario/calendario_model', 'calendario_model');
        $this->load->model('Home_model', 'home_model');
        $this->load->model('administration/groups_model', 'Groups_model');
    }

    /**
     * Metodo inicial del home
     *
     * @access	public
     * @author  Nohemi Rojas
     * @version V-1.0 21/11/12 02:34 PM
     */
    public function index($menu = array()) {
        $get_params = $this->input->get();
        $menu = (isset($get_params['menu']) && !empty($get_params['menu']) ? $get_params['menu'] : 'sidebar');
        $activate_notification = ($menu == 'sidebar' ? TRUE : FALSE);
        $this->setActiveNotification($activate_notification);
        $notification = $this->notifications->getNotification($activate_notification);
        //Evaluamos si la peticion posee parametros GET
        //Evaluamos si se esta consultado eventos.
        if (!empty($get_params) AND isset($get_params['return']) AND $get_params['return'] == 'events') {
            $this->offOutput();
            // echo json_encode($this->_getEvents($get_params['lastTimeUpdated']));
            echo json_encode($this->_getEvents());
        }

        //Si no se cumplen ninguna de la anteriores se esta pidiendo la vista.
        else {

            $this->setMenu($menu);
            $this->setTargetOn('content');

            if ($menu == 'sidebar'):
                $this->setMenu($this->load->view('sidebar', array(), TRUE));
                $params = array(
                    'save_url' => base_url('calendario/EXC_save'),
                    'frequency' => 30, //Segundos
                    'render' => $this->load->view('index', $notification, TRUE),
                    'source_url' => 'calendario/calendario/ver?return=events'
                );
                $this->setContent($this->view_engine->calendar($params));
                $this->setTitle('Calendario');
            elseif ($menu == 'doc'):
                $this->setTopBar('topbar_doc');
                $data['html'] = $this->menu_doc();
                $this->setContent($this->help());
                $this->setMenu($this->load->view('doc', $data, TRUE));
            endif;
        }
    }

    /**
     * Metodo Acerca de
     *
     * @access	public
     * @author  Nohemi Rojas
     * @version V-1.0 03/04/13 03:02 PM
     */
    public function about() {
        $this->setTargetOn('content');
        $data = $this->load->config('version');
        $this->setContent($this->load->view('about', $data, TRUE));
    }

    /**
     * Metodo Ayuda en el Sistema
     *
     * @access	public
     * @author  Nohemi Rojas
     * @version V-1.0 03/04/13 03:02 PM
     */
    public function help() {
        $this->setTargetOn('content');
        $data = $this->home_model->getOperation();
        foreach ($data as $key => $value) {
            $data[$key]['field'] = $this->home_model->fieldByOperation($data[$key]['id']);
            $data[$key]['imagen'] = $this->home_model->imageByOperation($data[$key]['id']);
        }
        return $this->load->view('help', array('data' => $data), TRUE);
    }

    /**
     * Metodo Perfil del usuario
     *
     * @access	public
     * @author  Nohemi Rojas
     * @version V-1.03/04/13 03:02 PM
     */
    public function profile() {
        $this->setTargetOn('content');
        $this->setContent($this->load->view('profile', array(), TRUE));
    }

    /**
     * Provee el acceso a la carga de notificaciones
     *
     * @access public
     * @author  Nohemi Rojas
     * @version V 1.0 29/01/13 02:56 PM
     */
    public function EXC_notification() {
        $this->offOutput();
        $notification = $this->notifications->getNotification();
        echo json_encode($notification);
    }

    /**
     * Consulta los eventos programados en el calendario a parit de una fecha data.
     *
     * @access  private
     * @param   timestamp   $lastTimeUpdated
     * @return  array    datos a se mostrados en el calendario.
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 28/01/13 14:55
     */
    private function _getEvents($lastTimeUpdated = 0) {
        //@todo posible soporte a agregar solo los eventos nuevos.
//        array(
//            'id' => 1,
//            'title' => 'Echo en PHP', {required}
//            'start' => '', {required}
//            'end',
//            'url',
//            'className',
//            'editable',
//            'color',
//            'backgroundColor',
//            'borderColor',
//            'textColor'
//        );
        $lastTimeUpdated = date('Y-m-d H:i:s', ($lastTimeUpdated / 1000));
        $events = $this->calendario_model->getEvents($lastTimeUpdated);
        print_r($events);
        if ($events) {
            //@to-do Colocar esta URL configurable
            // $url = base_url('cotizacion/cotizacion/detail');
            foreach ($events as &$event) {
                $event['id'] = $this->encrypt->encode($event['id']);
                // $event['url'] = $this->_formatEventURL($event['id'], $url);
            }
        }
        return $events;
    }

    /**
     * Formatea el atributo URL de los eventos para que una presionado el evento
     * nos muestre la informacion detallada del mismo.
     *
     * Basicamente retorna el siguiente string con loas valores sustituidos
     * javascript:request({no_remove:true, url:'%s', data:{id_ec:'%s'}})
     *
     * @access  private
     * @param   string    $id  Identificador unico del evento.
     * @param   string    $url URL a ser llamada al presionar el evento.
     * @return  string    llamado a funcion js.
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 29/01/13 9:54
     */
    private function _formatEventURL($id, $url) {
        // Llamado js a la funcion request() de view_engine.js
        $format = "javascript:request({no_remove:true,url:'%s',data:{target_on:'window',id:'%s'}})";
        $id = $this->encrypt->encode($id);
        return sprintf($format, $url, $id);
    }

    /**
     * Almacena los eventos modificados en el calendario.
     *
     * @access  private
     * @param   array   $event  datos del evento a ser actualizado.
     * @return  bool    resultado de la operacion de guardado.
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 28/01/13 15:46
     */
    private function _updateEvent($event) {
        $this->setContent('<pre>' . print_r($this->input->post(), TRUE) . '</pre>');
        $this->setTitle('Update Evento');
        $data = TRUE;
        return $data;
    }

    function menu_doc() {
        $menu = $this->Groups_model->menu_operation(TRUE);
        return $this->operation($menu);
    }

    /**
     * Agrega el formato de lista a las operaciones de forma jerarquica
     * para ser mostrados en la vista
     *
     * @access  private
     * @param array $menu Arreglo de datos de operaciones
     * @author  Frederick D. Bustamante G.  <frederickdanielb@gmail.com>
     * @author  Jose A. Rodriguez E.    <josearodrigueze@gmail.com>
     * @version 1.0 111/12/13 10:41 AM
     */
    private function operation($menu = array()) {
        //----------------Inicializacion de Variables-------------////
        $menu['data'] = $menu; //Arreglos con datos a evaluar
        $menu['position_level']['anterior'] = 1; //Posicion Anterior recorrida en filas
        $output = ''; //Retorno de cadena ya evaluada
        //---------------Fin Inicializacion de Variables--------/////

        if (sizeof($menu['data']) > 0):
            $output.="<ul id='main-menu-doc' class='sm sm-simple'>";
            foreach ($menu['data'] as $key => $value):
                $value['level'] = (int) $value['level']; //Casteo de variable a entero

                if ($value['level'] == $menu['position_level']['anterior']):
                    $output.= ($key > 0 ? '</li>' : '') . '<li ><a href="#' . trim($value["?column?"]) . '">' . trim($value["_name"]) . '</a>';
                elseif ($value['level'] > $menu['position_level']['anterior']):
                    $output.='<ul><li ><a href="#' . trim($value["?column?"]) . '">' . trim($value["_name"]) . '</a>';
                elseif ($menu['position_level']['anterior'] > $value['level']):
                    $diff = (int) $menu['position_level']['anterior'] - $value['level'];
                    $output.=str_repeat("</ul>", $diff) . '<li ><a href="#' . trim($value["?column?"]) . '">' . trim($value["_name"]) . '</a>';
                endif;
                $menu['position_level']['anterior'] = $value['level'];
            endforeach;
            $output.="</ul>";
            return (!empty($output) ? $output : FALSE);
        endif;
    }

}

// END home class
/* End of file home.php */
/* Location: application/modules/home/home.php */