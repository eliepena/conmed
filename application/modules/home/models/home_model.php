<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home_model extends AST_Model {

    /**
     * Obtiene los datos de ayuda de las operaciones asociadas al usuario en sesion
     *
     * @access	public
     * @author  Eliezer Pe#a
     * @version V-1.0
     */
    function getOperation() {
        $this->db->select("op.id");
        $this->db->select("op._name");
        $this->db->select("op.descripcion");
        $this->db->select("op.icon");
        $this->db->from("acl.operation op");
        $this->db->join("acl.group_operation go", "go.id_operation=op.id and op.visible='1'");
        $this->db->join("acl.users_groups ug", "go.id_group=ug.group_id");
        $this->db->where("ug.user_id", $this->session->userdata("user_id"));
        $this->db->order_by("op.id,op._name,op.descripcion,op.icon");
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

    /**
     * obtiene los campos asociados a la operacion dada
     *
     * @access	public
     * @param $id identificador de la operacion
     * @author  Eliezer Pe#a
     * @version V-1.0
     */
    function fieldByOperation($id) {
        $this->db->select("fo._label");
        $this->db->select("fo.tooltip as help");
        $this->db->select("fo.id_operation");
        $this->db->from("acl.field_operation fo");
        $this->db->join("dynamic.field fi", "fo.id_field=fi.id and fo.hidden='0'  and fo.deleted='0'");
        $this->db->where("fo.id_operation", $id);

        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

    /**
     * obtiene las imagenes asociadas a la operacion dada
     *
     * @access	public
     * @param $id identificador de la operacion
     * @author  Eliezer Pe#a
     * @version V-1.0
     */
    function imageByOperation($id) {
        $this->db->select('io.imagen');
        $this->db->from('acl.image_operation io');
        $this->db->where('io.id_operation', $id);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

}

// END home_model class
/* End of file home_model.php */
/* Location: application/modules/home/models/home_model.php */

