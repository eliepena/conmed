<dl class="dl-horizontal">
    <dt>Empresa</dt>
    <dd><?php echo $this->config->item('company'); ?></dd>

    <dt>Versión del sistema</dt>
    <dd><?php echo $this->config->item('version'); ?></dd>
    
     <dt>Fecha</dt>
    <dd><?php echo $this->config->item('date'); ?></dd>

    <dt>Desarrolladores</dt>
    <?php foreach ($this->config->item('developers') as $value): ?>
        <dd><?php echo $value['name'] . ' ' . $value['email'] ?></dd>
    <?php endforeach; ?>

</dl>