<br>
<br>
<br>
<br>   
<br> 
<div class="" id="<?php echo $id ?>">
    <div id="">
        <div class="">

            <h4><?php echo $_name ?></h4>  
        </div>
        <div id="" class="">
            <p> <?php echo $descripcion ?></p>
            <h5>Datos de Cirugia</h5>

            <?php if (!empty($field)): ?>
                <table class="table table-striped table-bordered">
                    <?php foreach ($field as $k => $val): ?>
                        <tr>
                            <td>  <b><?php echo $field[$k]['_label'] ?></b>:</td>
                            <td>  <?php echo $field[$k]['help'] ?></td> 
                        </tr>
                        <?php
                    endforeach;
                endif;
                ?>
            </table>
            <h5>Materiales</h5>
            <div class="" id="2101-ins">
               <table  class="table table-striped table-bordered">
                   <tr>
                            <td>  <b>Materiales</b>:</td>
                            <td>Permite agregar los materiales a asociar a la cotizacion</td> 
                   </tr>
               </table>
            </div>
            <h5>Instrumental</h5>
            <div class="" id="2101-ins">
               <table  class="table table-striped table-bordered">
                   <tr>
                            <td>  <b>Instrumentos</b>:</td>
                            <td>Permite agregar los instrumentos a asociar a la cotizacion</td> 
                   </tr>
               </table>
            </div>
            <h5>Backup</h5>
             <table  class="table table-striped table-bordered">
                   <tr>
                            <td>  <b>Backup</b>:</td>
                            <td>Permite agregar los articulos incluidos en backup a asociar a la cotizacion</td> 
                   </tr>
               </table>

        </div>
    </div>

</div>
