<h3>Ayuda del Sistema</h3>
<div class="content" id="help">
    <?php foreach ($data as $key => $value): ?>
        <?php
        if ($data[$key]['id'] == 2101) :
            $this->load->view('help/help_cot', $data[$key]);
        else:
            ?>
            <?php $campo = (isset($data[$key]['field'])) ? $data[$key]['field'] : ""; ?>
            <?php $imagen = (isset($data[$key]['imagen'])) ? $data[$key]['imagen'] : ""; ?>
            <br>
            <br>
            <br>
            <br>   
            <br>   

            <div id="<?php echo $data[$key]['id'] ?>">
                <div class="">  
                    <h4><?php echo $data[$key]['_name'] ?><small><?php if (isset($data[$key]['icon'])): ?> &nbsp;&nbsp;&nbsp; icono: &nbsp; <i class="<?php echo $data[$key]['icon'] ?>"></i>
                    <?php endif; ?></small> </h4> 
                    <div class="">
                        <p> <?php echo $data[$key]['descripcion'] ?></p>
                        <p><?php if (!empty($campo)): ?>
                            <table class="table table-bordered table-striped">
                                <?php foreach ($campo as $k => $val): ?>
                                    <tr>
                                        <td>  <b><?php echo $campo[$k]['_label'] ?></b>:</td>
                                        <td>  <?php echo $campo[$k]['help'] ?><br></td> 
                                    </tr>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </table>
                        </p>
                    </div>
                    <?php if (!empty($imagen)): ?>
                        <div class="well" >
                            <ul class="thumbnails">
                                <?php foreach ($imagen as $k => $val): ?>
                                    <li class="span3">
                                        <a onclick="show_img_help('<?php echo $data[$key]['_name'] ?>','<?= base_url($imagen[$k]['imagen']) ?>')" href="javascript:void(0);" class="thumbnail">
                                            <img data-src="" onclick="" alt="260x180" style="width: 260px; height: 180px;" src="<?= base_url($imagen[$k]['imagen']) ?>">
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>

                </div>
            </div>
        <?php endif;
    endforeach;
    ?>
</div>
