<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Clase de acceso a datos del modulo de auditoria
 *
 * @package conmed.application.modules
 * @subpackage audit
 * @category Modelo
 * @author Jose A. Rodriguez E. <josearodrigueze@gmail.com>
 * @version V - 1.0 11-04-2013 17:26:24
 */
class Audit_model extends AST_Model {

    function __construct() {
        parent::__construct();
        $this->setSchema('acl');
    }

    protected function whereDelete($delete = '0') {
        //$this->db->where($this->concatSchemaTable() . '._delete', $delete);
    }

    /**
     * Dado un arreglo se obtiene todas las coincidencias del arreglo por
     * clave/valor, donde clave es el campo la base de datos y valor el valor a
     * ser filtrado.
     *
     * @access  public
     * @param   Array $params <p>Contiene los pares clave valor a ser filtrados.</p>
     * @author  Jose Rodriguez, Frederick Bustamante
     * @version V-1.1 21/06/13 12:44 PM
     */
    function getByFields($params = array()) {
    	$table = $this->concatSchemaTable();
    	$t_users = 'acl.users';
    	// Select
    	$this->db->select($table . '.id');
    	$this->db->select($t_users . '.username AS username');
    	$this->db->select($table . '.ip');
    	$this->db->select($table . '.access_time');
    	$this->db->select($table . '.url');
    	$this->db->select($table . '._data');
    	$this->db->select($table . '.chk_result');
    	$this->db->select($table . '.os');
    	$this->db->select($table . '.browser');
    	$this->db->select($table . '._version');

    	// From e inners
    	$this->db->from($table);
    	$this->db->join($t_users, "$t_users.id=$table.id_user");
    	
    	if (!empty($params)){
            //Eliminamos este parametro xq por ahora no tenemos busqueda de tiempo.
            $value = $params['access_time'];
            unset($params['access_time']);
            
            //se aplica para que la poder realizar busqueda por fecha en el grid
            //-------------------------------------------------------------                
            $params["to_char(access_time,'DD-MM-YYYY HH:MM:SS')"] = $value;
            //-------------------------------------------------------------
            
            $this->_ilike($params, 'OR', TRUE);
        }
    }

    /**
     * Proporciona el metodo de ordenamiento de las consultas para getByFields().
     *
     * @access  public
     * @author  Jose A. Rodriguez E. <josearodrigueze@gmail.com>
     * @version V 1.0 17/04/2013 10:58:46 am
     */
    public function orderBy() {
    	$this->db->order_by($this->concatSchemaTable() . '.access_time', 'DESC');
    }
}