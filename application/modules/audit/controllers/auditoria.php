<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Descripcion de audit
 *
 * @package conmed.application.modules
 * @subpackage audit
 * @category Controlador
 * @author Jose A. Rodriguez E. <josearodrigueze@gmail.com>
 * @version V - 1.0 11-04-2013 17:26:24
 */
class Auditoria extends AST_Controller {

    function __construct() {
    	parent::__construct('audit_model');
    }

    //put your code here
}
