<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Payment_types_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad Category
 * con respecto a la categoria tipo de pago
 *
 * @package Modules
 * @category Model
 * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V-1.0 27/08/13 12:05 PM
 */
class Payment_types_model extends AST_Model {

    private $table;

    public function __construct() {

        parent::__construct();
        $this->setSchema('dynamic');
        $this->init('category');
        $this->super_table = false;
        $this->table = $this->concatSchemaTable();
    }

    public function whereDelete($delete = '0') {

    }

    /**
     * Dado un arreglo se obtiene todas las coincidencias del arreglo por
     * clave/valor, donde clave es el campo la base de datos y valor el valor a
     * ser filtrado.
     *
     * @access  public
     * @param   Array   $params <p>Contiene los pares clave valor a ser filtrados.</p>
     * @param   boolean $returned [optional] <p>
     *      <b>TRUE(default)</b> retorna el resultado de la consulta a BD.
     *      <b>FALSE</b> realiza la ejecucion del metodo where de CI.</p>
     * @return  mixed   <p>
     *  <b>Array<b/> con el resultado de la consulta.
     *  <b>Void<b/> setea el where del metodo de CI.</p>
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V-1.0 27/08/13 05:48 PM
     */
    function getByFields($params = array()) {

        //Select
        $this->db->select($this->table . '.id');
        $this->db->select($this->table . '._table');
        $this->db->select($this->table . '._label');



        //From e inners
        $this->db->where('_table', 'tipo_pago');
        $this->db->from($this->table);

        if (!empty($params)) {
            $this->_ilike($params, 'OR', TRUE);
        }

        $debug = debug_backtrace();
        if ($debug[1]['function'] == 'getAll')
            $this->db->order_by('_label ASC');
    }

}
