<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Payment_types
 *
 * Esta clase provee la implementacion para la capa de control de
 * acciones de la entidad  Payment_types y sus entidades dependientes o extendidas
 *
 * @package Module
 * @category Controller
 * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V-1.0 27/08/13 05:48 PM
 */
class Payment_types extends AST_Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Provee el acceso al formulario de ingreso de nuevos datos, asi mismo,
     * provee el acceso a la insercion de datos nuevos dentro en base de datos.
     *
     * @access public
     * @param array $params Parametros adicionales enviados al controlador.
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 20/11/2012 17:00
     */
    function create($params = NULL) {
        if (empty($params)) {
            $this->setContent($this->view_engine->form());
        } elseif ($params[0] == 'process') {
            $process = $this->view_engine->process();
            $success = FALSE;
            if ($process['result']) {
                $params = array('format_type' => 'insert', 'data' => $process['data']);

                $data = $this->_format($params);
                $data['_table'] = 'tipo_pago';
                $success = $this->Payment_types_model->insert($data);

                $this->setContent($success);
            } else {
                $this->setContent($process['validation_error']);
            }
            // -----------------------------
            //seteamos los datos de auditoria
            $this->audit->setOperationData($process);
            $this->audit->setChkResult($success);
            // -----------------------------
        }
    }

    /**
     * Implementacion basica para la edicion de registros, por un lado llama a
     * a la vista de edicion de registros y por otro lado guarda el registro en
     * base de datos.
     *
     * @access public
     * @param array $params Parametros adicionales enviados al controlador.
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 20/11/2012 17:00
     */
    function delete() {

        if ($this->input->is_ajax_request() && $this->input->post()) {

            $content = 'La pagina solicitada';
            $title = 'Error!!!';

            // Verificamos si el borrado es masivo o de uno solo
            $id_request = $this->input->post('id');
            if (is_array($id_request))
                foreach ($id_request as &$id_ec) {
                    $id_ec = $this->encrypt->decode($id_ec);
                }
            else
                $id_request = $this->encrypt->decode($id_request);


            //Arreglo de parametros a ser pasados al format.
            $params = array(
                'format_type' => 'delete',
                'data' => array()
            );
            $data = $this->_format($params);
            $success = $this->Payment_types_model->delete($id_request);

            if ($success) {
                $content = '<div class="modal-body">Registro eliminado saisfactoriamente.</div>';
                $title = 'Exito';
            } else
                $content = '<div class="modal-body">No se pudo eliminar el registro. Por favor intente de nuevo.</div>';

            $this->setTitle($title);
            $this->setContent($content);

            // -----------------------------
            //seteamos los datos de auditoria
            $this->audit->setOperationData(array('id' => $id_request));
            $this->audit->setChkResult($success);
            // -----------------------------
        }else {
            show_404();
        }
    }

}