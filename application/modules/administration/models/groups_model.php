<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Paciente_model
 *
 * Esta clase provee la implementacion de las operaciones de la entidad Paciente
 * y sus entidades dependientes o extendidas
 *
 * @package Modules
 * @category Model
 * @author Nohemi Rojas. <nohemir@gmail.com>
 * @version V-1.0 17/01/13 03:22 PM
 */
class Groups_model extends AST_Model {

    public function __construct() {
        parent::__construct();
        $this->setSchema('acl');
    }

    /**
     * Provee la sentencia condicional where para no obtener los valores eliminados.
     *
     * @access  protected
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 09/05/2013 10:48:11 am
     */
    protected function whereDelete() {

    }

    /**
     * Provee los datos de la entidad acl.aperation ordenados en forma jerarquica
     *
     * @access  public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @author  Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version V 1.0 11/12/13 10:32 AM
     */
    function menu_operation($with_session_user = FALSE) {
        $user_id = $this->session->userdata('user_id');
        $query_string = "
                                WITH RECURSIVE q AS
                                    (
                                        SELECT
                                                distinct h,
                                                1 AS level,
                                                ARRAY[h.id] AS breadcrumb
                                        FROM
						acl.users_groups as user_g
						INNER JOIN acl.groups as g ON user_g.group_id=g.id
						INNER JOIN acl.group_operation g_ope ON g.id=g_ope.id_group
						INNER JOIN acl.operation h ON g_ope.id_operation=h.id


                                        WHERE

                                                h.id_operation IS NULL ";
        $query_string.=($with_session_user ? 'AND user_g.user_id=' . (int) $user_id : '');

        $query_string.="       UNION ALL
                                        SELECT
                                                distinct hi,
                                                q.level + 1 AS level,
                                                breadcrumb || hi.id
                                        FROM
                                                q
                                                JOIN acl.operation hi ON hi.id_operation = (q.h).id
                                                INNER JOIN acl.group_operation g_ope ON hi.id=g_ope.id_operation
                                                INNER JOIN acl.users_groups as user_g ON g_ope.id_group=user_g.group_id ";
        $query_string.=($with_session_user ? 'AND user_g.user_id=' . (int) $user_id : '');

        $query_string.=")
                SELECT
                        REPEAT('', level) || (q.h).id,
                        (q.h).id_operation,
                        (q.h)._name,
                        level,
                        breadcrumb::VARCHAR AS path
                FROM
                        q
                ORDER BY
                        breadcrumb";
        $query = $this->db->query($query_string);
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    /**
     * Dado el identificador del grupo provee todas las operaciones asociadas
     * @access  public
     * @param   integer $id Identificador del grupo
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 11/12/13 10:55 AM
     */
    function getGroupOperation($id = NULL) {

        if (!$id):
            return FALSE;
        endif;

        //tables
        $t_group_operation = 'acl.group_operation';

        //Select
        $this->db->select("$t_group_operation.id_operation");

        //From and join
        $this->db->from("$t_group_operation");
        $this->db->where("$t_group_operation.id_group", $id);

        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0):
            foreach ($query->result_array() as $key => $value):
                $response[] = (int) $value['id_operation'];
            endforeach;
            return $response;
        else:
            return FALSE;
        endif;
    }

    /**
     * Provee las implementacion para realizar inserciones dentro de acl.groups_operation
     *
     * @access  public
     * @param   Array  $data datos a ser guardados dentro de la base de datos.
     * @return  Boolean Indicando el resultado de la operacion.
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V - 1.0 09/10/13 11:54 AM
     */
    function insert($data) {
        if (isset($data['id_group_operation']) && isset($data['operation'])):
            $id = $data['id_group_operation'];
            $operations = $data['operation'];
            $this->db->trans_start();


            $this->db->where('id_group', $id);
            $this->db->delete('acl.group_operation');


            foreach ($operations as $key => $value):
                $operations[$key]['id_group'] = $id;
            endforeach;
            $this->db->insert_batch('acl.group_operation', $operations);


            $this->db->trans_complete();
            return $this->db->trans_status();
        else:
           return parent::insert($data);
        endif;
    }

}
