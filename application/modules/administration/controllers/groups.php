<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Paciente
 *
 * Esta clase provee la implementacion para la capa de control de
 * acciones de la entidad paciente y sus entidades dependientes o extendidas
 *
 * @package Module
 * @category Controller
 * @author Nohemi Rojas. <nohemir@gmail.com>
 * @version V-1.0 29/11/12 01:26 PM
 */
class Groups extends AST_Controller {

    public function __construct() {
        parent::__construct();
    }

    function add_remove_operation($params = NULL) {
        if (isset($params[0]) && $params[0] == 'process'):

            $id = $this->session->userdata('id_group_operation');

            $success = FALSE;
            $this->_avancedFormValidation('add_operation');

            $process = $this->view_engine->process();

            if ($process['result']) {
                $process['data']['id_group_operation'] = $this->encrypt->decode($id);

                $params = array('format_type' => 'add_operation', 'data' => $process['data']);
                $data = $this->_format($params);

                $success = $this->Groups_model->insert($data);

                $this->setContent($success);
            } else {
                $this->setContent($process['validation_error']);
            }

            // -----------------------------
            //seteamos los datos de auditoria
            $this->audit->setOperationData($process);
            $this->audit->setChkResult($success);
        // -----------------------------

        else:
            $array_post = $this->input->post();
            if ($array_post['id']):
                $this->session->set_userdata('id_group_operation', $array_post['id']);
                $id = $this->encrypt->decode($array_post['id']);
                $groupOperationsId = $this->Groups_model->getGroupOperation($id);
                $menu = $this->Groups_model->menu_operation();
                $data['html'] = $this->operation($menu, $groupOperationsId);
                $this->setContent($this->load->view('administration/groups', $data, TRUE));
            endif;
        endif;
    }

    /**
     * Agrega el formato de lista a las operaciones de forma jerarquica
     * para ser mostrados en la vista
     *
     * @access  private
     * @param array $menu Arreglo de datos de operaciones
     * @author  Frederick D. Bustamante G.  <frederickdanielb@gmail.com>
     * @author  Jose A. Rodriguez E.    <josearodrigueze@gmail.com>
     * @version 1.0 111/12/13 10:41 AM
     */
    private function operation($menu = array(), $groupOperationsId = NULL) {
        //----------------Inicializacion de Variables-------------////
        $menu['data'] = $menu; //Arreglos con datos a evaluar
        $menu['position_level']['anterior'] = 1; //Posicion Anterior recorrida en filas
        $output = ''; //Retorno de cadena ya evaluada
        //---------------Fin Inicializacion de Variables--------/////

        if (sizeof($menu['data']) > 0):
            $output.="<div id='tree1'><ul >";
            foreach ($menu['data'] as $key => $value):
                $value['level'] = (int) $value['level']; //Casteo de variable a entero
                $checkbox = '<input type="checkbox" name="operation[]"  value="' . $value['?column?'] . '">';
                if ($groupOperationsId):
                    if (in_array((int) $value['?column?'], $groupOperationsId)):
                        $checkbox = '<input type="checkbox" name="operation[]" checked="checked" value="' . $value['?column?'] . '">';
                    endif;
                endif;
                if ($value['level'] == $menu['position_level']['anterior']):
                    $output.= '<li id="' . trim($value["?column?"]) . '">' . $checkbox . $value['_name'];
                elseif ($value['level'] > $menu['position_level']['anterior']):
                    $output.='<ul><li id="' . trim($value["?column?"]) . '">' . $checkbox . $value['_name'];
                elseif ($menu['position_level']['anterior'] > $value['level']):
                    $diff = (int) $menu['position_level']['anterior'] - $value['level'];
                    $output.=str_repeat("</ul>", $diff) . '<li id="' . trim($value["?column?"]) . '">' . $checkbox . $value['_name'];
                endif;
                $menu['position_level']['anterior'] = $value['level'];
            endforeach;
            $output.="</ul></div>";
            return (!empty($output) ? $output : FALSE);
        endif;
    }

    /**
     * Validaciones extras realizadas a la entidad, que no las soporta el View_Engine
     *
     * @access public
     * @param array $data Datos para aplicar el formateo.
     * @return Mixed , string  Validaciones o FALSE en caso de que no tenga errores
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.2 18/10/13 09:59 AM
     */
    private function _avancedFormValidation($format_type) {
        $this->load->library('form_validation');
        $format_type = strtoupper($format_type);

        //Insercion
        if ($format_type == 'INSERT') {
            $post = $this->input->post();

            //valida la existencia del codigo de la cotizacion
            if (sizeof($post['operation']) < 1)
                $this->form_validation->set_rules('operation', 'Operacion', 'required');
            $this->form_validation->set_message('required', 'Debe seleccionar una Operacion');
        }
    }

    /**
     * Formateo avanzado de los datos a insertar group_operation
     *
     * @access public
     * @param array $format_type Tipo de Formateo [INSERT o UPDATE].
     * @param array $data Datos para aplicar el formateo.
     * @return array Arreglo con los datos formateados segun la regla de negocio.
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.2 18/10/13 09:59 AM
     */
    function _avancedFormat($format_type, $data) {
        
        $format_type = strtoupper($format_type);
        if ($format_type == 'ADD_OPERATION') {
            unset($data["created_by"]);
            unset($data["modified_by"]);
            unset($data["modified_at"]);


            $operation = $data['operation'];

            foreach ($operation as $key => $value):
                $group_operation[$key]['id_operation'] = $value;

            endforeach;

            //-------------FIN FORMAT  orden de despacho
            //--------------Retorno de datos
            return array(
                'operation' => $group_operation,
                'id_group_operation' => $data['id_group_operation']
            );
        }else {
            return $data;
        }
    }

}
