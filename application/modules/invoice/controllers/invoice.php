<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Descripcion de Invoice
 *
 * @package conmed.application.modules
 * @subpackage Invoice
 * @category Controlador
 * @author Jose A. Rodriguez E. <josearodrigueze@gmail.com>
 * @version V - 1.0 11-11-2013 17:26:24
 */
class Invoice extends AST_Controller {

    function __construct() {
    	parent::__construct();
    }
}