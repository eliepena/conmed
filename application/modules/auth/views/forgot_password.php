<?php echo form_open("auth/forgot_password", array('class' => 'form-signin')); ?>

<h4 class="form-signin-heading">¿Olvidaste tu contraseña?</h4>
<span class="help-inline">Ingresa tu dirección de correo electrónico</span>

<div class="control-group error">
    <span class="help-inline"><?php echo $message; ?></span>
</div>

<?php echo form_input($email); ?>

<br/>
<button type="submit" class="btn btn-info">Enviar</button>

<?php echo form_close(); ?>