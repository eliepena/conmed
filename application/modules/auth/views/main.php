<!DOCTYPE html>
<html>
    <head>
        <title>Conmed</title>
        <?php echo meta(array('name' => 'Content-type', 'content' => 'text/html; charset=utf-8', 'type' => 'equiv')) ?>

        <link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/favicon.ico" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/auth.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/bootstrap-modal.css" />


        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>

        <script src="<?php echo base_url() ?>assets/js/min/jquery-1.10.2.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/min/jquery-ui-1.10.1.custom.min.js"></script>
        <script>var intervals = Array();</script>
    </head>
    <body>
        <div class="row">
            <div class="container"><?php echo $this->load->view($content) ?></div>
        </div>
        <script src="<?php echo base_url() ?>assets/js/min/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/min/bootbox.min.js" async></script>
        <script src="<?php echo base_url() ?>assets/js/min/bootstrap-modal.min.js" async></script>
        <script src="<?php echo base_url() ?>assets/js/min/bootstrap-modalmanager.min.js" async></script>
        <script src="<?php echo base_url() ?>assets/js/min/bootstrap-typeahead.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/min/DT_bootstrap.min.js"></script>

        <script>
            var navegador = navigator.userAgent;
            if (navigator.userAgent.indexOf('MSIE') !=-1) {
                //        alert('está usando Internet Explorer ...');
                bootbox.alert('Usted está usando Internet Explorer, la aplicacion puede presentar fallos debido al navegador.');
            } else if (navigator.userAgent.indexOf('Firefox') !=-1) {
                //                bootbox.alert('está usando Firefox ...');

            } else if (navigator.userAgent.indexOf('Chrome') !=-1) {
                //                bootbox.alert('está usando Google Chrome ...');

            } else if (navigator.userAgent.indexOf('Opera') !=-1) {
                //                bootbox.alert('está usando Opera ...');

            } else {
                //                bootbox.alert('está usando un navegador no identificado ..');
            }

        </script>
    </body>
</html>