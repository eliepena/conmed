 <div id="login">
     <div id="infoMessage"><?php echo $message;?></div>
             <?php echo form_open('auth/reset_password/' . $code, array('class' => 'form-signin'));?>

            <img src="<?php echo base_url() ?>assets/img/logo.png">

            <div class="control-group error">
                <span class="help-inline"><?php echo $message; ?></span>
            </div>
            
            <div class="control-group">
                <label for="password">Nueva Contrase&ntilde;a</label>
                <?php echo form_input($new_password); ?>
            </div>
            
            <div class="control-group">
                <label for="password">Confirme Contrase&ntilde;a</label>
                <?php echo form_input($new_password_confirm); ?>
           
            </div>
            
                <?php echo form_input($user_id);?>
                 <?php echo form_hidden($csrf); ?>
            
            <div class="control-group">
                <button type="submit" class="btn btn-info">Guardar</button>
            </div>
            
            <?php echo form_close(); ?>
 </div>

