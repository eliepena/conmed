        <div id="login">
            <?php echo form_open("auth/login", array('class' => 'form-signin')); ?>

            <img src="<?php echo base_url() ?>assets/img/logo.png">

            <div class="control-group error">
                <span class="help-inline"><?php echo $message; ?></span>
            </div>

            <div class="control-group">
                <label for="identity">Correo/Usuario</label>
                <?php echo form_input($identity); ?>
            </div>

            <div class="control-group">
                <label for="password">Clave</label>
                <?php echo form_input($password); ?>
            </div>

            <div class="control-group">
                <label class="checkbox">
                    <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"'); ?> <label for="remember">Recordarme</label>
                </label>
            </div>

            <div class="control-group">
                <button type="submit" class="btn btn-info">Iniciar sesión</button>
                <a href="forgot_password">¿Olvidaste tu contraseña?</a>
            </div>

            <?php echo form_close(); ?>
        </div>