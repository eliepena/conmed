<html>
<body>
	<h1><?php echo $this->lang->line('email_reset_password') ?> <?php echo $identity;?></h1>
	<p><?php echo $this->lang->line('email_reset_password_instruction') ?> <?php echo anchor('auth/reset_password/'. $forgotten_password_code, $this->lang->line('email_reset_password_action'));?>.</p>
</body>
</html>