<html>
<body>
        <h1><?php echo $this->lang->line('activate_email_header') ?></h1>
        <p><?php echo $this->lang->line('activate_email_welcome') ?><br>
           <?php echo $this->lang->line('activate_email_title') ?> <?php echo $identity;?><br>
           <?php echo $this->lang->line('activate_email_message') ?><?php echo anchor('auth/reset_password/'. $forgotten_password_code,  $this->lang->line('activate_email_link'));?>.</p>
        <img src="<?php echo base_url() ?>assets/img/logo.png">
</body>
</html>