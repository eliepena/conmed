<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Users
 *
 * Esta clase provee la implementacion para la capa de control de
 * acciones de la entidad usuarios y sus entidades dependientes o extendidas
 *
 * @package Module
 * @category Controller
 * @author Nohemi Rojas. <nohemir@gmail.com>
 * @version V-1.0 14/01/13 10:26 PM
 */
class Users extends AST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('persona/empleado_model', 'empleado_model');
        $this->load->model('ion_auth_model', 'ion_auth_model');
        $this->load->model('users_model', 'users_model');
        $this->load->library('ion_auth');
    }

    /**
     * Provee el acceso al formulario de ingreso de nuevos datos, asi mismo,
     * provee el acceso a la insercion de datos nuevos dentro en base de datos.
     *
     * @access public
     * @param array $params Parametros adicionales enviados al controlador.
     * @author  Nohemi Rojas
     * @version V 1.0 14/01/13 04:25 PM
     */
    function create($params = NULL) {
        if (!$this->input->post()) {
            $this->setContent($this->view_engine->form());
        } else {
            $process = $this->view_engine->process();

            if ($process['result']) {
                $params = array('format_type' => 'insert', 'data' => $process['data']);

                $data = $this->_format($params);
                $success = $this->ion_auth->register($data['username'], $data['password'], $data['email'], $data['additional_data'], $data['additional_data']['id_groups']);

                if ($success) {
                    $success = $this->empleado_model->update($this->session->userdata('id_empleado_user'), array('id_usuario' => $success), 'id');
                    $this->session->unset_userdata('id_empleado_user');
                } else {
                    $success = $this->ion_auth->errors();
                }

                $this->setContent($success);
            } else {
                $this->setContent($process['validation_error']);
            }
        }
    }

    /**
     * Provee la implementacion basica para activar/desactivar registros.
     *
     * @access public
     * @param array $params Parametros adicionales enviados al controlador.
     * @author Nohemi Rojas
     * @version V 1.0 21/01/13 11:06 AM
     */
    function enable($params = NULL) {
        $array_post = $this->input->post();
        if ($params == NULL) {
            $id = $this->encrypt->decode($array_post['id']);
            $this->session->set_userdata("users_model.id_ec", $id);
            $data = $this->users_model->getById($id);

            $this->setContent($this->view_engine->form(array('data' => $data)));
        } elseif ($params[0] == 'process') {
            $process = $this->view_engine->process();

            if ($process['result']) {
                $id_ec = $this->session->userdata("users_model.id_ec");
                $arr_user = $this->users_model->getById($id_ec);

                //Desactivacion o Activacion
                $success = (!$process['data']['active']) ? $this->ion_auth->deactivate($arr_user['id']) : $this->ion_auth->activate($arr_user['id']);

                if ($success)
                    $this->session->unset_userdata("users_model.id_ec");

                $this->setContent($success);
            } else {
                $this->setContent($process['validation_error']);
            }
        }
    }

    /**
     * Formateo avanzado de los datos a insertar en usuario
     *
     * @access public
     * @param array $format_type Tipo de Formateo [INSERT o UPDATE].
     * @param array $data Datos para aplicar el formateo.
     * @return array Arreglo con los datos formateados segun la regla de negocio.
     * @author Nohemi Rojas
     * @version V 1.0 14/01/13 02:52 PM
     */
    function _avancedFormat($format_type, $data) {

        $arr_empleado = $this->empleado_model->getById($data['id_empleado'], 'id');

        $this->session->set_userdata('id_empleado_user', $data['id_empleado']);
        if ($format_type == 'INSERT') {

            $additional_data = array(
                'first_name' => $arr_empleado['primer_nombre'],
                'last_name' => $arr_empleado['primer_apellido'],
                'id_groups' => array($data["id_groups"])
            );
            unset($data["id_empleado"]);
            unset($data["passconf"]);
            unset($data["id_groups"]);
            $data['email'] = $arr_empleado['correo_electronico'];
            $data['additional_data'] = $additional_data;
        }
        return $data;
    }

    /**
     * Provee una vista de los datos principales del usuario actualmente en sesion
     *
     * @access public
     * @param
     * @author Eliezer Pe#a
     * @version V 1.0 28/05/13 04:25 PM
     */
    function user_profile() {

        $id = $this->session->userdata('user_id');

        if (!empty($id)) {
            $data = $this->users_model->getById($id);
            $this->setContent($this->view_engine->form(array('data' => $data, 'short_operation' => 'details')));
        }
    }

    /**
     * Provee un formulario para el cambio de contrasena
     *
     * @access public
     * @author Eliezer Pe#a
     * @version V 1.0 30/05/13 10:00 AM
     */
    function change_password() {
        if (!$this->input->post()) {
            $this->setContent($this->view_engine->form());
        } elseif ($this->input->post()) {
            $process = $this->view_engine->process();

            if ($process['result']) {
                $user = $this->ion_auth->user()->row();
                $identity = $user->email;
                $old = $this->input->post('password_old');
                $new = $this->input->post('password');

                $success = $this->ion_auth->change_password($identity, $old, $new);


                $this->setContent($success);
            } else {
                $this->setContent($process['validation_error']);
            }
        }
    }

    /**
     * Edita datos de usuario y su empleado relacionado
     *
     * @access public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 27/06/13 03:41 PM
     */
    function edit_user() {
        //Extraigo el id de usuario de la sesion
        $id = $this->session->userdata('user_id');

        //obtengo los datos del usuario para pasarlos al
        //formulario dibulado con ViewEngine
        $data = $this->users_model->getDataByUser($id);
        $data['id'] = $this->encrypt->encode($data['id']);
        if (!$this->input->post()) {
            //paso los datos al formulario
            $this->setContent($this->view_engine->form(array('data' => $data, 'short_operation' => 'edit')));
        } elseif ($this->input->post()) {
            $process = $this->view_engine->process($data);

            if ($process['result']) {
                $success = $this->users_model->edit_user_model($process['data']);
                $this->setContent($success);
            } else {
                $this->setContent($process['validation_error']);
            }
        }
    }

}