<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->library('acl');
        $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
    }

    //redirect if needed, otherwise display the user list
    function index() {
        if (!$this->ion_auth->logged_in()) {
            //redirect them to the login page
            redirect('auth/login', 'refresh');
        } elseif (!$this->ion_auth->is_admin()) {
            //redirect them to the home page because they must be an administrator to view this
            $this->acl->homePage();
        } else {
            $this->acl->homePage();
        }
    }

    //log the user in
    function login() {

        //-----------------------------
        //Seteamos los datos de auditoria
        $operation_data = NULL;
        if ($remember = (bool) $this->input->post('remember'))
            $operation_data['remember'] = $remember;
        if ($identity = $this->input->post('identity'))
            $operation_data['identity'] = $identity;
        //-----------------------------
        // si el usuario esta en session lo redirecciona
        if ($this->ion_auth->logged_in()) {
            //Registramos la auditoria
            $this->audit->register();

            $this->acl->homePage();
        }

        $this->data['title'] = "Login";

        //validate form input
        $this->form_validation->set_rules('identity', 'Correo/Usuario', 'required');
        $this->form_validation->set_rules('password', 'Clave', 'required');

        if ($this->form_validation->run() == true) {
            //check to see if the user is logging in
            //check for "remember me"
            $remember = (bool) $this->input->post('remember');
            $user = $this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember);

            //-----------------------------------
            //Registramos la auditoria
            $this->audit->setOperationData($operation_data);
            $this->audit->register();
            //-----------------------------------
            //if the login is successful
            if ($user) {
                //ACL - init 
                $this->acl->init($user);
            } else {
                //if the login was un-successful
                //redirect them back to the login page
                $this->session->set_flashdata('message', $this->ion_auth->errors());

                redirect('auth/login', 'refresh'); //use redirects instead of loading views for compatibility with MY_Controller libraries
            }
        } else {
            //the user is not logging in so display the login page
            //set the flash data error message if there is one
            //ADICIONALMENTE IGUALAMOS LA VARIABLE Q SERA GUARDADA EN BD PARA LA AUDITORIA
            $this->data['message'] = $operation_data['errors'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $this->data['identity'] = array('name' => 'identity',
                'id' => 'identity',
                'type' => 'text',
                'value' => $this->form_validation->set_value('identity'),
                'required' => true,
                'class' => 'input-xlarge'
            );
            $this->data['password'] = array('name' => 'password',
                'id' => 'password',
                'type' => 'password',
                'required' => true,
                'class' => 'input-xlarge'
            );
            $this->data['content'] = 'login';
            $this->load->view('main', $this->data);

            //-----------------------------------
            //Registramos la auditoria
            if (empty($operation_data['errors']))
                unset($operation_data['errors']);
            $this->audit->setOperationData($operation_data);
            //-----------------------------------
        }
    }

    //log the user out
    function logout($msg = NULL) {
        $message = (!$msg) ? $this->ion_auth->messages() : $this->lang->line($msg);
        $this->data['title'] = "Logout";

        //log the user out
        $logout = $this->ion_auth->logout();

        //redirect them to the login page
        $this->session->set_flashdata('message', $message);

        //Registramos auditoria.
        $this->audit->register();

        redirect('auth/login', 'refresh');
    }

    //forgot password
    function forgot_password() {
        //-----------------------------
        //Seteamos los datos de auditoria
        $operation_data = NULL;
        if ($email = $this->input->post('email'))
            $operation_data['email'] = $email;
        $this->audit->setOperationData($operation_data);
        //-----------------------------

        $this->form_validation->set_rules('email', 'Correo', 'required|valid_email');
        if ($this->form_validation->run() == false) {
            //setup the input
            $this->data['email'] = array('name' => 'email',
                'id' => 'email',
                'required' => true
            );

            //set any errors and display the form
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $this->data['content'] = 'forgot_password';
            $this->load->view('main', $this->data);
        } else {
            // get identity for that email
            $config_tables = $this->config->item('tables', 'ion_auth');
            $identity = $this->db->where('email', $this->input->post('email'))->limit('1')->get($config_tables['users'])->row();

            $forgotten = FALSE;
            if ($identity)
            //run the forgotten password method to email an activation code to the user
                $forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

            if ($forgotten) {
                //Registro la auditoria
                $this->audit->register();
                //if there were no errors
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect("auth/login", 'refresh'); //we should display a confirmation page here instead of the login page
            } else {
                //-----------------------------
                //Registro la auditoria y el mensaje de error
                if ($this->ion_auth->errors()) {
                    $operation_data['errors'] = $this->ion_auth->errors();
                    $this->audit->setOperationData($operation_data);
                }
                $this->audit->register();
                //-----------------------------

                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect("auth/forgot_password", 'refresh');
            }
        }
    }

    //Working code for this example is in the example Auth controller in the github repo
    public function reset_password($code = NULL) {
        if (!$code) {
            show_404();
        }

        $user = $this->ion_auth->forgotten_password_check($code);

        if ($user) {
            //if the code is valid then display the password reset form
            $this->form_validation->set_rules('new', 'New Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
            $this->form_validation->set_rules('new_confirm', 'Confirm New Password', 'required');

            if ($this->form_validation->run() == false) {
                //display the form
                //set the flash data error message if there is one
                $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

                $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
                $this->data['new_password'] = array(
                    'name' => 'new',
                    'id' => 'new',
                    'type' => 'password',
                    'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$'
                );
                $this->data['new_password_confirm'] = array(
                    'name' => 'new_confirm',
                    'id' => 'new_confirm',
                    'type' => 'password',
                    'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$'
                );
                $this->data['user_id'] = array(
                    'name' => 'user_id',
                    'id' => 'user_id',
                    'type' => 'hidden',
                    'value' => $user->id
                );
                $this->data['csrf'] = $this->_get_csrf_nonce();
                $this->data['code'] = $code;

                $this->data['title'] = "Reset";
                $this->data['content'] = 'reset_password';
                $this->load->view('main', $this->data);
            } else {
                // do we have a valid request?
                if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id')) {
                    //something fishy might be up
                    $this->ion_auth->clear_forgotten_password_code($code);
                    show_error('This form post did not pass our security checks.');
                } else {
                    // finally change the password
                    $identity = $user->{$this->config->item('identity', 'ion_auth')};
                    $change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

                    if ($change) {
                        //if the password was successfully changed
                        $this->session->set_flashdata('message', $this->ion_auth->messages());
                        $this->logout();
                    } else {
                        $this->session->set_flashdata('message', $this->ion_auth->errors());
                        redirect('reset_password/' . $code, 'refresh');
                    }
                }
            }
        } else {
            //if the code is invalid then send them back to the forgot password page
            $this->session->set_flashdata('message', $this->ion_auth->errors());
            redirect("forgot_password", 'refresh');
        }
    }

    function _get_csrf_nonce() {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    function _valid_csrf_nonce() {
        if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
                $this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Descripcion de metodo
     *
     * @access  public
     * @param     array    $params    parametros a ser procesados en el metodo
     * @return  array    dato a ser retornado
     * @author  Jose A. Rodriguez E. <josearodrigueze@gmail.com>
     * @version V 1.0 date
     */
    public function _output($output) {
        echo $output;
        $this->audit->register();
    }

}
