<?php

class Users_model extends AST_Model {

    public function __construct() {
        parent::__construct();
        $this->super_table = FALSE;
        $this->setSchema('acl');
    }

    /**
     * Provee la sentencia condicional where para no obtener los valores eliminados.
     *
     * @access  protected
     * @author  NOhemi Rojas
     * @version V 1.0 22/01/13 02:35 PM
     */
    protected function whereDelete() {

    }

    /**
     * Dado un arreglo se obtiene todas las coincidencias del arreglo por
     * clave/valor, donde clave es el campo la base de datos y valor el valor a
     * ser filtrado.
     *
     * @access  public
     * @param   Array   $params <p>Contiene los pares clave valor a ser filtrados.</p>
     * @author  Nohemi Rojas, Frederick Bustamante
     * @version V-1.1 21/06/13 12:44 PM
     */
    function getByFields($params) {

        $this->db->select('u.id');
        $this->db->select('u.first_name');
        $this->db->select('u.last_name');
        $this->db->select('u.last_login');
        $this->db->select('u.email');
        $this->db->select('u.username');
        $this->db->select('g.name');
        $this->db->select("CASE u.active WHEN '1' THEN 'SI' WHEN '0' THEN 'NO' END active", FALSE);

        $this->db->from('acl.users AS u');
        $this->db->join('acl.users_groups AS u_g', 'u_g.user_id =  u.id');
        $this->db->join('acl.groups AS g', 'u_g.group_id =  g.id');

        if (!empty($params)) {
            $value = $params['active'];
            unset($params['active']);
            unset($params['last_login']);

            //se aplica para que la poder realizar busqueda por fecha en el grid
            //------------------------------------------------------
            $params["to_char(u.last_login,'DD-MM-YYYY HH:MM:SS')"] = $value;
            //-------------------------------------------------------------

            $params['g.name'] = $value;
            $this->_ilike($params, 'OR', TRUE);
        }
    }

    /**
     * Busca los datos de un empleado dado su usuario,
     *
     * @param integer $user_id Identificador del usuario
     * @return mixed  Datos
     * @author Frederick Bustamante <frederickdanielb@gmail.com>
     * @version 1.0 28/06/13 03:42 PM
     */
    public function getDataByUser($user_id) {

        //datos de empleado asociado al usuario
        $this->db->select('empleado.cat_nivel_educativo');
        $this->db->select('empleado.letra');
        $this->db->select('empleado.cat_tipo_empleado');
        $this->db->select('empleado.cedula');
        $this->db->select('empleado.direccion');
        $this->db->select('empleado.fecha_ingreso');
        $this->db->select('empleado.fecha_nacimiento');
        $this->db->select('empleado.id_localidad');
        $this->db->select('empleado.sexo');
        $this->db->select('empleado.tlf_movil');
        $this->db->select('empleado.primer_apellido');
        $this->db->select('empleado.primer_nombre');
        $this->db->select('empleado.segundo_apellido');
        $this->db->select('empleado.segundo_nombre');

        //Datos de usuario
        $this->db->select('users.id');
        $this->db->select('users.username');
        $this->db->select('users.email');


        $this->db->from('business.empleado');
        $this->db->join('acl.users', "id_usuario = users.id");
        $this->db->where('id_usuario', $user_id);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row_array() : FALSE;
    }

    /**
     * Actualiza los datos del usuario y su empleado asociado
     *
     * @param array $data Datos para la insercion de datos
     * @return mixed  Datos
     * @author Frederick Bustamante <frederickdanielb@gmail.com>
     * @version 1.0 28/06/13 03:44 PM
     */
    function edit_user_model($data = array()) {
        $return_empleado = FALSE;
        $return_user = FALSE;
        $data_empleado = $data;
        $id_user = $this->encrypt->decode($data_empleado['id']);

        $id_empleado = $this->getIdByUser($id_user);
        $data_empleado['correo_electronico'] = $data_empleado['email'];
        unset($data_empleado['username']);
        unset($data_empleado['email']);
        unset($data_empleado['id']);

        $data_user = array();

        $data_user['first_name'] = $data['primer_nombre'];
        $data_user['last_name'] = $data['primer_apellido'];


        $this->db->where('id', $id_empleado)
                ->update('business.empleado', $data_empleado);
        $return_empleado = $this->db->affected_rows() ? TRUE : FALSE;

        $this->db->where('id', $id_user)
                ->update('acl.users', $data_user);
        $return_user = $this->db->affected_rows() ? TRUE : FALSE;

        return ($return_user || $return_empleado ? TRUE : FALSE);
    }

    /**
     * Busca el identificador de un empleado dado su usuario,
     *
     * @param integer $user_id Identificador del usuario
     * @return integer Identificador del empleado
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 27/06/13 03:41 PM
     */
    public function getIdByUser($user_id) {
        $this->db->select('business.empleado.id');
        $this->db->from('business.empleado');
        $this->db->join('acl.users', "id_usuario = users.id");
        $this->db->where('id_usuario', $user_id);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row()->id : FALSE;
    }

    /**
     * Devuelve los datos basicos del vendedor
     *
     * @access public
     * @return arreglo con el listado de vendedores
     * @author  Eliezer Pe#a
     * @version 1.0 06/09/13 11:35 AM
     */
    function getVendedor() {

        $this->db->select(array('e.id', 'e.primer_nombre', 'e.primer_apellido'));
        $this->db->from('acl.users AS u');
        $this->db->join('acl.users_groups AS ug', 'u.id=user_id');
        $this->db->join('business.empleado AS e', 'u.id=id_usuario');
        $this->db->where('ug.group_id', 3);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->result_array()) : FALSE;
    }

}
