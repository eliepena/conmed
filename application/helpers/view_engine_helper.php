<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * Este helper provee funciones necesarias para complemento de view_engine
 * como la actualizacion de eventos, notificaciones, envio de correo entre otros
 *
 * @package application
 * @category helper
 * @author Jose A. Rodriguez E. <josearodrigueze@gmail.com>
 * @version V-1.0 09/04/2013 06:21:59 pm
 */
if (!function_exists('writeUpdateTime')) {

    /**
     * Crea y Escribe un archivo en la ruta assets/system/$file_name
     * que contiene el microtime actual indicando el tiempo de marca
     * de actualizaCIon del calendario.
     *
     * @param 	string	$file_name
     * @author  Jose A. Rodriguez E. <josearodrigueze@gmail.com>
     * @version V 1.0 09/04/2013 06:23:43 pm
     */
    function writeUpdateTime($file_name = NULL) {
        $CI = &get_instance();
        $CI->config->load('poll');
        $CI->load->helper('file');
        $path = $CI->config->item('calendar_path_mark');

        if (empty($file_name))
            $file_name = $CI->config->item('calendar_file_mark');

        $file = $path . $file_name;
        $time = round(microtime(true) * 1000);
        write_file($file, $time);
    }

}

if (!function_exists('sendEmail_old')) {

    /**
     * Envia un correo electronico
     *
     * @param 	string	$email Correo(s)
     * @param 	string	$body Cuerpo del correo
     * @param 	string	$subject Asunto del correo
     * @author  Nohemi Rojas
     * @version V 1.0 11/04/13 05:26 PM
     */
    function sendEmail_old($email, $body, $subject) {

        if (!$email)
            return;

        $CI = &get_instance();
        $CI->config->load('view_engine');

        $app_name = $CI->config->item('app_name');
        $from_email = $CI->config->item('app_from_email');

        $CI->load->library('email');
        $CI->email->from($from_email, $app_name);
        $CI->email->to($email);
        $CI->email->subject($subject);
        $CI->email->message($body);
        return $CI->email->send();
    }

}
if (!function_exists('sendEmail')) {

    /**
     * Envia un correo electronico
     *
     * @param 	string	$email Correo(s)
     * @param 	string	$body Cuerpo del correo
     * @param 	string	$subject Asunto del correo
     * @param 	string	$plain_body Cuerpo del correo sin formato
     * @param 	string	$attachment archivo adjunto del correo
     * @author  Frederick Bustamante <frederickdanielb@gmail.com>
     * @version V 1.0 07/06/13 02:33 PM
     */
    function sendEmail($email, $body, $subject, $plain_body, $attachment = '', $activate_mail = TRUE) {


        if (!$email)
            return;

        $CI = &get_instance();
        $CI->config->load('view_engine');

        $app_name = $CI->config->item('app_name');
        $from_email = $CI->config->item('app_from_email');

        $config['protocol'] = $CI->config->item('protocol');
        $config['smtp_host'] = $CI->config->item('smtp_host');
        $config['smtp_port'] = $CI->config->item('smtp_port');
        $config['smtp_user'] = $CI->config->item('smtp_user');
        $config['smtp_pass'] = $CI->config->item('smtp_pass');
        $config['charset'] = $CI->config->item('charset');
        $config['newline'] = $CI->config->item('newline');
        $config['mailtype'] = $CI->config->item('mailtype');

        $CI->load->library('email');
        $CI->email->clear();
        $CI->email->initialize($config);


        //ENVIO Y EVALUACION DE CORREO
        $CI->email->from($from_email, $app_name);
        $CI->email->to($email);
        $CI->email->subject($subject);
        $CI->email->message($body);
        $CI->email->set_alt_message($plain_body);
        if (!empty($attachment)):
            $CI->email->attach($attachment);
        endif;

//        if (is_array($attachment) && sizeof($attachment)>0):
//            foreach ($attachment AS $value):
//                $CI ->email->attach($value);
//            endforeach;
//        endif;
        if ($activate_mail):
            return $CI->email->send();
        else:
            return TRUE;
        endif;
    }

}
if (!function_exists('generate_pdf')) {

    /**
     * Generacion de PDF
     *
     * @access  public
     * @param   string $name_file El nombre del archivo cuando se guarda.
     * @param   string $destination_path Ruta para descarga del archivo (Default : vacia)
     * @param   string $opc_destination Destino dónde enviar el documento.
     * @param   string $html Html para generar documento
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 2.0 31/07/13 04:39 PM
     */
    function generate_pdf($name_file = 'doc', $destination_path = '', $opc_destination = 'F', $html, $background_no_validate = FALSE, $membrete = FALSE) {
        /*         * Descripcion detallada de variables:
         *
         * $name_file:
         *          Tenga en cuenta que los caracteres especiales se eliminan
         *          los espacios en blanco y caracteres se reemplazan por el
         *          carácter de subrayado.
         * $opc_destination
         *          Puede tomar uno de los siguientes valores:
         *          I:  enviar el archivo en línea para el navegador (por defecto).
         *              El plug-in se utiliza si está disponible. El nombre dado por
         *              el nombre se utiliza cuando se selecciona la opción "Guardar
         *              como" opción en el enlace de generar el PDF.
         *
         *          D:  enviar al navegador y forzar la descarga del archivo con el nombre dado por su nombre.
         *          F:  guardar en un archivo de servidor local con el nombre dado por su nombre.
         *          S:  devolver el documento como una cadena (nombre se ignora).
         *          FI: equivalente a la opción F + I
         *          FD: equivalente a la opción F D +
         *          E:  devolver el documento como base64 mime adjunto de correo electrónico de varias partes (RFC 2045)
         */

        if ($membrete == 'true') {
            $membrete = TRUE;
        } elseif ($membrete == 'false') {
            $membrete = false;
        }
        $CI = &get_instance();
        $CI->load->library('Pdf');
        $CI->config->load('view_engine');

        $app_logo = $CI->config->item('app_logo');
        $app_dir = $CI->config->item('app_dir');
        $global_img_validate = $CI->config->item('global_img_validate');
        if ($global_img_validate) {
            $background_no_validate = true;
        }
        $pdf_background_image = ($background_no_validate ? $CI->config->item('pdf_background_image_no_validate') : $CI->config->item('pdf_background_image_validate'));
        define('LOGO_BACKGROUND', $pdf_background_image);
        define('LOGO_HEADER', '../../../../' . $app_logo);
        define('LOGO_HEADER_WIDTH', 50);
        $pdf = new Pdf_cotizacion('P', 'mm', 'Letter', true, 'UTF-8', false);

        $pdf->SetCreator(PDF_CREATOR);
        $pdf->logo_background(LOGO_BACKGROUND);
        //$pdf->SetPrintFooter(false);
        $pdf->show_footer($membrete);


        $pdf->SetAuthor('');
        $pdf->SetTitle('');
        $pdf->SetSubject('');
        $pdf->SetKeywords('');

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config
        $pdf->SetHeaderData(LOGO_HEADER, LOGO_HEADER_WIDTH, PDF_HEADER_TITLE . ' ', PDF_HEADER_STRING, array(0, 64, 255), array(0, 64, 128));

        $pdf->setFooterData($tc = array(0, 0, 0), $lc = array(0, 64, 128));

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
//        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetMargins(2, 27, 10);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        //relación utilizada para ajustar la conversión de los píxeles
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


        // ---------------------------------------------------------
        // establecer el modo de fuente por defecto
        $pdf->setFontSubsetting(true);

        // Establecer el tipo de letra
        //Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
        // Helvetica para reducir el tamaño del archivo.
        $pdf->SetFont('freemono', '', 14, '', true);

        // Añadir una página
        // Este método tiene varias opciones, consulta la documentación para más información.
        $pdf->AddPage();

        //fijar efecto de sombra en el texto
        $pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));

        // Establecemos el contenido para imprimir
        // $data['nro_pagina'] = $pdf->PageNo();
        // Imprimimos el texto con writeHTMLCell()
        // -- Nuevo fondo establecido --- ---
        // Obtener el margen de salto de página actual
        $bMargin = $pdf->getBreakMargin();
        // Obtener el modo de auto-page-break actual
        $auto_page_break = $pdf->getAutoPageBreak();
        // Desactivar el auto-page-break
        $pdf->SetAutoPageBreak(false, 0);
        // imagen de fondo
        $img_file = LOGO_BACKGROUND;
        if ($membrete):
            $pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
        endif;
        // Restaurar el estado de auto-page-break
        $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
        // Establecer el punto de partida para el contenido de la página
        $pdf->setPageMark();


        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);

        // ---------------------------------------------------------
        // Cerrar el documento PDF y preparamos la salida
        // Este método tiene varias opciones, consulte la documentación para más información.
        $name_file = utf8_decode("{$destination_path}" . "{$name_file}.pdf");
        return $pdf->Output($name_file, $opc_destination);
    }

}


/* End of file view_engine_helper.php */
/* Location: ./application/helpers/view_engine_helper.php */