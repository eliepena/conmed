<?php

if (!function_exists('writeLastUpdated')) {

    /**
     * Registra el timestand en un archivo de la fecha enviada por parametro
     *
     * @param 	string	$time timestand
     * @author  Carlos Bello <calink124@gmail.com>
     * @version V 1.0 07/06/13 02:33 PM
     */
    function writeLastUpdated($time) {
        $fp = fopen(dirname(__FILE__) . '/../logs/last_updated_system.txt', 'w');
        fwrite($fp, $time);
        fclose($fp);
    }

}

function _extendsFormat($data) {
    $data_ext = array();
    $data_ext['created_by'] = $data['created_by'];
    $data_ext['modified_by'] = $data['created_by'];
    //$data['modified_at'] = date("Y-m-d H:i:s");
    return $data_ext;
}
if (!function_exists('prepareDataCotizacionFree')) {
	function prepareDataCotizacionFree($request, $data)
	{
		$CI = & get_instance();
		$post = $request;
		
		$cotizacion_art = _extendsFormat($data);
		$cotizacion_articulo = array();
		
		
		//------------- INICIO FORMAT cotizacion
		$cotizacion_art['fecha_cotizacion'] = $data['fecha_cotizacion'];
		$cotizacion_art['id_cliente'] = $data['id_cliente'];
		$cotizacion_art['id_vendedor'] = $CI->empleado_model->getIdByUser($CI->session->userdata('user_id'));
		$cotizacion_art['descuento'] = $data['descuento'];
		$cotizacion_art['recargo'] = $data['recargo'];
		$cotizacion_art['enviar'] = $data['enviar'];
		$cotizacion_art['observacion'] = $data['observacion'];
		$cotizacion_art['monto_exento'] = str_replace(',', '.', str_replace('.', '', $data['monto_exento']));
		$cotizacion_art['codigo'] = $CI->Cotizacion_free_model->generateCode();
		
		//            $cotizacion_art['observacion'] = $data['observacion'];
		//-------------FIN FORMAT  cotizacion
		//------------- INICIO FORMAT  cotizacion_articulo
		$articulos = $post['art'];
		$cantidad_articulo = $post['cant_art'];
		$precio_articulo = $post['precio_art'];
		$monto_total = str_replace(',', '.', str_replace('.', '', $data['monto_total']));
		$monto_gravable = str_replace(',', '.', str_replace('.', '', $data['monto_gravable']));
		
		$cotizacion_art['monto_total'] = $monto_total;
		$cotizacion_art['monto_gravable'] = $monto_gravable;
		
		//            $id_precio = $post['id_precios'];
		if ($articulos) {
			foreach ($articulos as $art => $articulo) {
				$cotizacion_articulo[$art]['id_articulo'] = $articulo;
				$cotizacion_articulo[$art]['cantidad'] = empty($cantidad_articulo[$art]) ? 0 : $cantidad_articulo[$art];
				$cotizacion_articulo[$art]['precio'] = $precio_articulo[$art];
			}
		}
		
		//------------- FIN FORMAT cotizacion_articulo
		$cotizacion_pago['cat_tipo_pago'] = $data['cat_tipo_pago'];
		
		//Verifica que exista tercero
		if (isset($data['id_cliente']) and isset($data['cat_letra'])) {
			$cliente = $CI->engine_model->getValueByCategory($data['cat_letra']);
			if ($cliente == 'Natural') {
				$cotizacion_pago['id_persona'] = $data['id_cliente'];
			}
			if ($cliente == 'Jurídico') {
				$cotizacion_pago['id_empresa'] = $data['id_cliente'];
			}
		}
		//--------------Retorno de datos
		return array(
				'cotizacion_art' => $cotizacion_art,
				'cotizacion_articulo' => $cotizacion_articulo,
				'cotizacion_pago' => $cotizacion_pago
		);
		
	}
}
if (!function_exists('prepareDataCotizacion')) {

    /**
     * Registra el timestand en un archivo de la fecha enviada por parametro
     *
     * @param 	string	$time timestand
     * @author  Carlos Bello <calink124@gmail.com>
     * @version V 1.0 07/06/13 02:33 PM
     */
    function prepareDataCotizacion($request, $data, $role, $validations_cot = array(), $movil = false) {
        $CI = & get_instance();
        $post = $request;

        $cotizacion = _extendsFormat($data);
        $cotizacion_cirugia = _extendsFormat($data);
        $cotizacion_pago = _extendsFormat($data);
        $cotizacion_material = array();
        $cotizacion_material_backup = array();
        $cotizacion_instrumento = array();
        $cotizacion_validacion = array();

        //------------- INICIO FORMAT cotizacion
        $cotizacion['fecha_cotizacion'] = $data['fecha_cotizacion'];
        $cotizacion['id_paciente'] = $data['id_paciente'];
        $cotizacion['id_medico'] = $data['id_medico'];
        $cotizacion['id_seguro'] = $data['id_seguro'];
        $cotizacion['id_centro_salud'] = $data['id_centro_salud'];
        $cotizacion['tipo_direccion'] = $data['tipo_direccion'];
        $cotizacion['enviar'] = $data['enviar'];
        $cotizacion['codigo'] = $CI->Cotizacion_model->generateCode();
        if ($role == 'Recepcionista') {
            $cotizacion['id_vendedor'] = $data['vendedor'];
        } else
            $cotizacion['id_vendedor'] = $CI->empleado_model->getIdByUser($CI->session->userdata('user_id'));
        $cotizacion['descuento'] = $data['descuento'];
        $cotizacion['recargo'] = $data['recargo'];
        $cotizacion['monto_exento'] = str_replace(',', '.', str_replace('.', '', $data['monto_exento']));
        $cotizacion['observacion'] = $data['observacion'];
        //-- FIN FORMAT  cotizacion
        //-------------FIN FORMAT  cotizacion
        //------------- INICIO FORMAT cotizacion_cirugia

        $cotizacion_cirugia['id_articulacion'] = $data['id_articulacion'];
        $cotizacion_cirugia['id_lesion'] = $data['id_lesion'];
        if (!$data['preview']):
            if (isset($data['id_tecnica']) and $data['id_tecnica'] != ''):
                $cotizacion_cirugia['id_tecnica'] = $data['id_tecnica'];
            elseif (isset($post['nombre_tecnica']) && $post['nombre_tecnica'] != '') :
                $cotizacion_cirugia['id_tecnica'] = $CI->Cotizacion_model->createTecnica($data, $post['nombre_tecnica'], $post['mat']);
                if ($cotizacion_cirugia['id_tecnica']):
                    if (isset($post['backup'])):
                        $backup_cant_articles = $post['cant_backup'];
                        $CI->Cotizacion_model->createBackupRecomendacion($cotizacion_cirugia['id_tecnica'], $backup_cant_articles);
                    endif;
                endif;
            endif;
        endif;
        //------------- FIN FORMAT cotizacion_cirugia
        //------------- INICIO FORMAT  cotizacion_material
        $materiales = $post['mat'];
        $cantidad_material = $post['cant_mat'];
        $precio_material = $post['precio_mat'];
        $monto_total = str_replace(',', '.', str_replace('.', '', $post['monto_total']));
        $monto_gravable = str_replace(',', '.', str_replace('.', '', $post['monto_gravable']));

        $id_precio = $post['id_precios'];
        if ($materiales) {
            foreach ($materiales as $mat => $material) {
                $cotizacion_material[$mat]['id_articulo'] = $material;
                $cotizacion_material[$mat]['cantidad'] = empty($cantidad_material[$mat]) ? 0 : $cantidad_material[$mat];
                $cotizacion_material[$mat]['precio'] = $precio_material[$mat];
                $cotizacion_material[$mat]['id_tipo_precio'] = $id_precio;
                $cotizacion_material[$mat]['backup'] = '0';
            }
        }

        if ($movil == false) {
            if (isset($post['backup'])) {
                $backup_id = $post['backup'];
                $backup_precio = $post['precio_backup'];
                $backup_cantidad = $post['cant_backup'];
                foreach ($backup_id as $mat) {
                    $cotizacion_material_backup[$mat]['id_articulo'] = $mat;
                    $cotizacion_material_backup[$mat]['cantidad'] = $backup_cantidad[$mat];
                    $cotizacion_material_backup[$mat]['precio'] = $backup_precio[$mat];
                    $cotizacion_material_backup[$mat]['id_tipo_precio'] = $id_precio;
                    $cotizacion_material_backup[$mat]['backup'] = '1';
                }
            }
        } else {
            if (isset($post['backup'])) {
                $backup_id = $post['backup'];
                $backup_precio = $post['precio_backup'];
                $backup_cantidad = $post['cant_backup'];
                foreach ($backup_id as $mat => $backup) {
                    $cotizacion_material_backup[$mat]['id_articulo'] = $backup;
                    $cotizacion_material_backup[$mat]['cantidad'] = $backup_cantidad[$mat];
                    $cotizacion_material_backup[$mat]['precio'] = $backup_precio[$mat];
                    $cotizacion_material_backup[$mat]['id_tipo_precio'] = $id_precio;
                    $cotizacion_material_backup[$mat]['backup'] = '1';
                }
            }
        }
        //------------- FIN FORMAT cotizacion_material
        //------------- INICIO FORMAT cotizacion_instrumento
        if (isset($post['instru'])) {
            $instrumentos = $post['instru'];
            $cantidad_instrumento = $post['cant_instru'];

            foreach ($instrumentos as $ins => $instrumento) {
                $cotizacion_instrumento[$ins]['id_articulo'] = $instrumento;
                $cotizacion_instrumento[$ins]['cantidad'] = $cantidad_instrumento[$ins];
            }
        }

        //------------- FIN FORMAT cotizacion_instrumento
        //------------- INICIO FORMAT cotizacion_pago
        $cotizacion_pago['cat_tipo_pago'] = $data['cat_tipo_pago'];
        $cotizacion_pago['cat_pagador'] = $data['cat_pagador'];

        $cotizacion['monto_total'] = $monto_total;
        $cotizacion['monto_gravable'] = $monto_gravable;
        //Verifica que exista tercero
        if (isset($data['id_tercero']) and isset($data['cat_letra'])) {
            $tercero = $CI->engine_model->getValueByCategory($data['cat_letra']);
            if ($tercero == 'Natural') {
                $cotizacion_pago['id_persona'] = $data['id_tercero'];
                $data['id_persona'] = $data['id_tercero']; //se crea a fin de hacer Validacion
            }
            if ($tercero == 'Jurídico') {
                $cotizacion_pago['id_empresa'] = $data['id_tercero'];
                $data['id_empresa'] = $data['id_tercero']; //se crea a fin de hacer Validacion
            }
        }

        //------------- FIN FORMAT cotizacion_pago
        //------------- INICIO FORMAT cotizacion_validacion


        $paciente = $CI->paciente_model->getById($data['id_paciente'], 'id');


        foreach ($validations_cot as $value) {

            //Verifica que el paciente este validado antes de ingresar dicha validacion
            if ($value['campo'] == 'id_paciente' and $paciente['validado'])
                continue;

            //Verifica si existe persona como Tercero, y ademas si esta validado o no
            if ($value['campo'] == 'id_persona') {
                if (!isset($data['id_persona']))
                    continue;
                else {
                    $persona = $CI->persona_model->getById($data['id_persona'], 'id');
                    if ($persona['validado'])
                        continue;
                }
            }

            if ($value['campo'] == 'fecha_cirugia' or $value['campo'] == 'cirugia_reprogramada')
                continue;

            if ($value['campo'] == 'cirugia_preaprobada')
                continue;
            //Verifica si existe empresa como Tercero, y ademas si esta validado o no
            if ($value['campo'] == 'id_empresa') {
                if (!isset($data['id_empresa']))
                    continue;
                else {
                    $empresa = $CI->empresa_model->getById($data['id_empresa'], 'id');
                    if ($empresa['validado'])
                        continue;
                }
            }

            //Agrega los campos definidos en la entidad validacion
            if (in_array($value['campo'], array_keys($data))) {
                $cotizacion_validacion[] = array(
                    'id_validacion' => $value['id'],
                    'cat_val_nivel' => $CI->engine_model->getValueByCategory('validador', '_label'),
                );
            }
        }

        //------------- FIN FORMAT cotizacion_validacion
        //--------------Retorno de datos
        return array('cotizacion' => $cotizacion, 'cotizacion_cirugia' => $cotizacion_cirugia, 'cotizacion_material' => $cotizacion_material, 'cotizacion_material_backup' => $cotizacion_material_backup,
            'cotizacion_instrumento' => $cotizacion_instrumento, 'cotizacion_pago' => $cotizacion_pago, 'cotizacion_validacion' => $cotizacion_validacion
        );
    }

}
if (!function_exists('sendEmailError')) {

    /**
     * Envia un correo electronico
     *
     * @param 	string	$email Correo(s)
     * @param 	string	$body Cuerpo del correo
     * @param 	string	$subject Asunto del correo
     * @param 	string	$plain_body Cuerpo del correo sin formato
     * @param 	string	$attachment archivo adjunto del correo
     * @author  Frederick Bustamante <frederickdanielb@gmail.com>
     * @version V 1.0 07/06/13 02:33 PM
     */
    function sendEmailError($email, $body, $subject, $plain_body, $attachment = '') {

        if (!$email)
            return FALSE;

        $CI = &get_instance();
        $CI->config->load('view_engine');

        $app_name = "Info";
        $from_email = $CI->config->item('smtp_user');

        $config['protocol'] = $CI->config->item('protocol');
        $config['smtp_host'] = $CI->config->item('smtp_host');
        $config['smtp_port'] = $CI->config->item('smtp_port');
        $config['smtp_user'] = $CI->config->item('smtp_user');
        $config['smtp_pass'] = $CI->config->item('smtp_pass');
        $config['charset'] = $CI->config->item('charset');
        $config['newline'] = $CI->config->item('newline');
        $config['mailtype'] = $CI->config->item('mailtype');

        $CI->load->library('email');
        $CI->email->clear();
        $CI->email->initialize($config);


        //ENVIO Y EVALUACION DE CORREO
        $CI->email->from($from_email, $app_name);
        $CI->email->to($email);
        $CI->email->subject($subject);
        $CI->email->message($body);
        $CI->email->set_alt_message($plain_body);
        //$CI ->email->attach($attachment);
        //        if (is_array($attachment) && sizeof($attachment)>0):
        //            foreach ($attachment AS $value):
        //                $CI ->email->attach($value);
        //            endforeach;
        //        endif;

        return $CI->email->send();
    }

}

/**
 * pg_array_to_php
 *
 * Convierte un array postgresql a una array php
 *
 * @access	public
 * @param	string	pg_array
 * @return	array
 * @author  Jose Rodriguez <josearodrigueze@gmail.com>
 * @version V 1.0 01/08/13 02:33 PM
 */
if (!function_exists('pg_array_to_php')) {

    function pg_array_to_php($pg_array) {
        return explode(',', preg_replace('/\{|\}/', '', $pg_array));
    }

}