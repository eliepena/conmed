<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of AST_Form_validation
 *
 * Extension de la Libreria CI_Form_Validation
 *
 * @package	Libraries
 * @category	Validation
 * @author	Nohemi Rojas <nohemir@gmail.com>
 */
class AST_Form_validation extends CI_Form_validation {

    /**
     * Constructor
     */
    public function __construct($rules = array()) {
        $this->CI = & get_instance();
        parent::__construct($rules);
    }

    /**
     * Match one field to another
     *
     * @access	public
     * @param	string
     * @param	field
     * @return	bool
     */
    public function is_unique($str, $field) {
        list($schema, $table, $field) = explode('.', $field);
        $query = $this->CI->db->limit(1)->get_where($schema . '.' . $table, array($field => $str));
        return $query->num_rows() === 0;
    }

    /**
     * Valid URL
     *
     * @access	public
     * @param	string $str valor del campo a validar
     * @author Nohemi Rojas
     * @return	bool
     */
    public function valid_url($str) {
        return filter_var($str, FILTER_VALIDATE_URL);
    }

    /**
     *
     * Valida un campo con letras, acentos y espacios en blanco
     * @param string $str valor del campo a validar
     * @return bool
     * @author Nohemi Rojas
     */
    function valid_alpha_space($str) {
        if (empty($str))
            return TRUE;
        return (bool) (preg_match("/^[A-zÑñÁáÉéÍíÓóÚúäëïöüÿÄËÏÖÜŸ\s]+$/", $str));
    }

    /**
     *
     * Valida un campo con letras, acentos, espacios en blanco y numeros
     * @param string $str valor del campo a validar
     * @return bool
     * @author Nohemi Rojas
     */
    function valid_alpha_numeric_space($str) {
        if (empty($str))
            return TRUE;
        return (bool) (preg_match("/^[A-Za-z0-9ÑñÁáÉéÍíÓóÚúäëïöüÿÄËÏÖÜŸ\.\,\s\-\_]+$/", $str));
    }

    /**
     * Valida que un numero telefonoco dado sea valido.
     * Valido solo para VENEZUELA.
     *
     * @access  public
     * @param   string  $number  numero telefonico.
     * @return  bool    indica si es un numero telefonico valido.
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 date21/01/13 15:48
     */
    function valid_phone($number) {
        //Si el campo no es requerido
        if (empty($number))
            return TRUE;

        $valid_format_phone = array(
            //  "^04(1|2)(4|6)\d{7}$", //Celulares movilnet y movistar
            //  "^0412\d{7}$", //Digitel
            //  "^02\d{9}$" //PSTN

            "^\(04(1|2)(4|6)\)\s\d{3}-\d{4}$", //Celulares movilnet y movistar
            "^\(0412\)\s\d{3}-\d{4}$", //Digitel
            "^\(02\d{2}\)\s\d{3}-\d{4}$" //PSTN
        );
        return (bool) preg_match("/(" . implode($valid_format_phone, "|") . ")/i", $number);
    }

    /**
     * Valida que un rif dado sea valido.
     * Valido solo para VENEZUELA.
     *
     * @access  public
     * @param   string  $rif    cadena que cotiene el rif
     * @return  bool    indica si el rif valido.
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 date21/01/13 15:48
     */
    function valid_rif($rif) {
        //Si el campo no es requerido
        if (empty($rif))
            return TRUE;

        //Si se desea agregar mas letras para RIF se hace aqui.
        $letras = 'VEJPG';
        return (bool) preg_match("/^[$letras]\d{9}$/", strtoupper($rif));
    }

    /**
     * Valida que el valor dao sea una fecha valida en formato d-m-Y.
     *
     * @access  public
     * @param   string  $date  fecha a validar
     * @return  bool    resultado de la validacion.
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 30/01/13 17:49
     */
    function valid_date($date) {
        '/[(0?[0-9])([12][0-9])(3[01])]-[(0?[0-9])(1[012])]-([12][0-9]{3})/';
        if (!empty($date))
            if (preg_match('/([0-3][0-9])-([0-9]{1,2})-([1-2][0-9]{3})/', $date, $matches))
                if (checkdate($matches[2], $matches[1], $matches[3]))
                    return TRUE;
        return TRUE;
    }

    /**
     * Campo de solo caracteres alfabeticos.
     *
     * @access  public
     * @param   string  $str    Texto a validar
     * @return  bool    resultado de la validacion.
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 30/01/13 17:58
     */
    function alpha($str) {
        if (!empty($str))
            if (!preg_match("/^[a-zÑñÁáÉéÍíÓóÚúäëïöüÿÄËÏÖÜŸ]+$/i", $str))
                return FALSE;
        return TRUE;
    }

    /**
     * Convierte una cadena dada en minusculas y coloca el primer caracter de
     * cada palabra en mayuscula.
     *
     * EJ: JOse miguel = Jose Miguel
     *
     * @access  public
     * @param   string    $str  cade a formatear
     * @return  srting    cadena formateada
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 5/02/13 13:53
     */
    public function srtToTitle($str) {
        return mb_convert_case($str, MB_CASE_TITLE);
    }

    /**
     * Campo comodin para colocar validaciones que seran sobreescritas
     *
     * @access  public
     * @return  bool   FALSE
     * @author  Nohemi Rojas
     * @version V 1.0 05/02/13 01:43 PM
     */
    function rewrite() {
        return FALSE;
    }

    /**
     * Valida que una fecha no sea menor que hoy
     *
     * @access  public
     * @param   string  $str    Texto a validar
     * @return  bool    resultado de la validacion.
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 30/08/13 11:41 AM
     */
    function valid_date_greater_than_or_equal_today($str) {

        if (empty($str))
            return TRUE;

        if (strtotime(date('Y-m-d', strtotime($str))) >= strtotime(date('Y-m-d')))
            return TRUE;
        else {
            $this->CI->form_validation->set_message('valid_date_greater_than_or_equal_today', 'El campo %s debe ser una fecha mayor o igual a la fecha de hoy');
            return FALSE;
        }
    }

}

// END AST Form Validation Class

/* End of file AST_Form_validation.php */
/* Location: ./application/libraries/AST_Form_validation.php */