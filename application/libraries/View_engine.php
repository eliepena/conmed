<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * Libreria que provee la construccion de vistas dinamicas por medio de los permisos existentes
 * en base de datos. Contiene un conjunto de conjunto de funcionalidades que generan interfaces como
 * formularios, grids, ventanas modales etc.
 *
 * @package		conmed.application
 * @subpackage  Libraries
 * @category	Library
 * @author		Nohemi Rojas<nohemir@gmail.com>, Jose Rodriguez  <josearodrigueze@gmail.com>
 * @since       V 1.0 21/11/12 02:40 PM
 */
class View_engine {

    /**
     * Instancia de CodeIgniter
     *
     * @access private
     * @var string
     */
    private $CI;

    /**
     * Identificador de la Operacion en curso
     *
     * @access private
     * @var string
     */
    private $operation_id;

    /**
     * Nombre de la URL en curso
     *
     * @access private
     * @var string
     */
    private $operation_url;

    /**
     * Titulo de la operacion en curso
     *
     * @access private
     * @var string
     */
    private $operation_title;

    /**
     * Indica donde se cargara la operacion en curso
     *
     * @access private
     * @var string
     */
    private $target_on;

    /**
     * Nombres de los los campos a ser mostrados dentro del grid.
     *
     * @access private
     * @var array
     * @author Jose A. Rodriguez E. <josearodrigueze@gmail.com>
     */
    private $grid_fields_name;

    /**
     * Cabeceras de los los campos a ser mostrados dentro del grid.
     *
     * @access private
     * @var array
     * @author Jose A. Rodriguez E. <josearodrigueze@gmail.com>
     */
    private $grid_fields_label;

    /**
     * Constructor
     *
     * @access	public
     * @author  Nohemi Rojas
     * @version V-1.0 21/11/12 02:13 PM
     */
    public function __construct() {
        $this->CI = &get_instance();
    }

    /**
     * Getter de fields_name
     *
     * @access private
     * @return array Con los nombres de los campos.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 23/11/2012 13:17
     */
    private function getGridFieldsName() {
        return (!empty($this->grid_fields_name)) ? $this->grid_fields_name : array();
    }

    /**
     * Setter de fields_name
     *
     * @access private
     * @param array $names Con los nombres de los campos.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 23/11/2012 13:17
     */
    private function setGridFieldsName($names) {
        $this->grid_fields_name = $names;
    }

    /**
     * Getter de fields_label
     *
     * @access private
     * @return array Con los cabeceras de los campos.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 23/11/2012 13:17
     */
    private function getGridFieldsLabel() {
        if (empty($this->grid_fields_label))
            die('no hay campos asociados al grid.');

        return $this->grid_fields_label;
    }

    /**
     * Setter de fields_label
     *
     * @access private
     * @param array $names Con las cabeceras de los campos.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 23/11/2012 13:17
     */
    private function setGridFieldsLabel($headers) {
        $this->grid_fields_label = $headers;
    }

    /**
     * Metodo Inicial que se realiza un Set del id de la operacion y de la url
     *
     * @access	public
     * @param integer $operation_id Identificador de la operacion en curso
     * @param string $operatioo_url Nombre de la url
     * @author  Nohemi Rojas
     * @version V-1.0 21/11/12 02:13 PM
     */
    public function init($operation_id, $operation_url, $operation_title, $target_on) {
        $this->operation_id = $operation_id;
        $this->operation_url = $operation_url;
        $this->operation_title = $operation_title;
        $this->target_on = $target_on;
        $this->CI->load->model('engine/engine_model');
    }

    /**
     * Se encarga de crear un formulario dinamico con los campos de la operacion que se esta ejecutando
     *
     * @access	public
     * @param array $data Arreglo con los datos de la operacion
     * @return string Vista con el contenido a renderizar
     * @author  Nohemi Rojas
     * @version V-1.0 21/11/12 02:13 PM
     */
    public function form($params = array()) {

        //extracion de parametros
        if (!empty($params))
            extract($params);

        //indica si hay valores de en el formulario para el caso de edicion o visualizacion
        if (isset($data))
            $view_params['data'] = $data;

        //indica la url de la operacion en caso de ser particular
        $view_params['url'] = (isset($operation_url)) ? $operation_url : $this->operation_url;

        //indica el id de la operacion en caso de ser particular
        $operation_id = (isset($operation_id)) ? $operation_id : $this->operation_id;

        //indica si se muestra el action bar
        $actionbar = (isset($actionbar)) ? $actionbar : TRUE;

        if ($actionbar)
            $view_params['actionbar'] = $this->getActionbar($operation_id);

        //indica los campos asociados a la operacion
        $view_params['fields'] = (isset($fields)) ? $fields : $this->buildComboListener($this->getFields($operation_id));

        //indica hacia donde redirecciona una vez que se registra correctamente para el caso del content
        $arr_link = explode('/', $view_params['url']);
        $view_params['redirect'] = $arr_link[0] . '/' . $arr_link[1] . '/listAll';

        //indica el nombre de la operacion corta como editar, crear etc.
        $view_params['short_operation'] = (isset($short_operation)) ? $short_operation : $arr_link[2];


        //indica el titulo de la operacion
        $view_params['operation_title'] = (isset($show_title) and $show_title == TRUE ) ? $this->operation_title : '';

        //indica si se ha personalizado el boton submit
        $view_params['button_submit'] = (isset($button_submit)) ? $button_submit : 'Guardar';

        //indica si se ha personalizado la validacion del form
        $view_params['after_validation'] = (isset($after_validation)) ? $after_validation : 'engine/after_validation.js.php';

        //indica si se recargara un contenido
        $view_params['reload_content'] = (isset($reload_content)) ? $reload_content : NULL;

        $view_params['target_on'] = (isset($target_on)) ? $target_on : NULL;

        $view_params['operation_id'] = $operation_id;
        //indica donde se carga el formularo si en una ventana o en el contentenedor principal
        if ($this->target_on == 'window' or $this->target_on == 'window2')
            return $this->CI->load->view('engine/form_window', $view_params, TRUE);
        if ($this->target_on == 'content' or $this->target_on == 'tab_content' or $this->target_on == 'tab')
            return $this->CI->load->view('engine/form_content', $view_params, TRUE);
    }

    /**
     * Se encarga de procesar el formulario, verificando que cumpla las reglas de
     * validacion en el servidor
     *
     * @access	public
     * @return array arreglo con los resultados del proceso, TRUE/FALSE en caso de que las
     *         validaciones cumplan con las reglas. En caso de que cumpla se retorna los datos con
     *         los valores  de cada campo
     * @author  Nohemi Rojas
     * @version V-1.0 21/11/12 02:13 PM
     */
    public function process($params = array()) {

        //extracion de parametros
        if (!empty($params))
            extract($params);

        $output = array('result' => TRUE);
        $data = array();

        $view_params['fields'] = (isset($fields)) ? $fields : $this->getFields();
        $view_params['url'] = $this->operation_url;
        $this->CI->load->library('form_validation');

        //Indica si el formulario tiene validaciones del server por defecto NO
        $validation = FALSE;

        //validaciones del servidor de cada campo
        foreach ($view_params['fields'] as $field) {
            if ($field['validations'] and !(strstr($field['validations'], 'rewrite'))) {
                //Se prende indicando que existe validacion
                $validation = TRUE;
                $this->CI->form_validation->set_rules($field['name'], $field['label'], $field['validations']);
            }
        }

        //Caso en el que el formulario contiene validaciones del servidor
        if ($validation AND ($this->CI->form_validation->run() == FALSE)) {
                $output['validation_error'] = validation_errors();
                $output['result'] = false;
        }
        $output['data'] = $this->CI->input->post();
        return $output;
    }

    /**
     * Se encarga de crear un contenedor de tabs con operaciones hijas
     *
     * @access	public
     * @param array $data Arreglo de parametros opcionales
     * @return string Vista con los tabs y con el contenido a renderizar
     * @author  Nohemi Rojas
     * @version V-1.0 10/01/13 02:30 PM
     */
    public function tab($params) {

        $permissions = $this->CI->session->userdata('permissions');
        $tabs = array();

        //En caso de que se sobreescriba su contenido, Busca cada tab para construir su vista
        foreach ($params as $key => $param) {

            if ($param['view'] != 'particular') {

                foreach ($permissions as $permission) {

                    if ($permission['url'] == $param['url']) {
                        $label = (isset($param['label'])) ? $param['label'] : $permission['_name'];

                        $view_params = (isset($param['params'])) ? $param['params'] : array();
                        $view_params['operation_url'] = $permission['url'];
                        $view_params['operation_id'] = $permission['id'];

                        switch ($param['view']) {
                            case 'form':
                                $view = $this->form($view_params);
                                $tabs[$key] = array('label' => $label, 'view' => $view);
                                break;

                            case'grid':
                                $view_params['show_title'] = FALSE;
                                $view = $this->grid($view_params);
                                $tabs[$key] = array('label' => $label, 'view' => $view);
                                break;
                        }

                        //Este break rompe el recorrido de permissions
                        break;
                    }
                }
            } else {
                $view = $this->CI->load->view($param['params']['file'], $param['params']['data'], TRUE);
                $tabs[$key] = array('label' => $param['label'], 'view' => $view);
            }
        }
        $view_params['tabs'] = $tabs;
        return $this->CI->load->view('engine/tab', $view_params, TRUE);
    }

    /**
     * Realizada el llamado a la vista de grids, recibido un arreglo de
     * parametros de configuracion.
     *
     * @access public
     * @param array $params['data']    Datos a ser mostrados dentro del grid.
     * @param integer $params['count']   Indica el total de datos obtenidos en la consulta.
     * @param array $params['checkbox'] Indica si debeb mostrarse checkbox o no. default FALSE (no se muestra)
     * @return string   Grid generado por el ViewEngine
     * @author Jose A. Rodriguez E.
     * @version V 1.0 23/11/2012 13:30
     */
    public function grid($params) {

        $this->CI->config->load('pagination');

        extract($params);

        //Comentado xq no se usa
        // if (!isset($data))
        //     die('Uno o mas parametros necesarios para la construccion del grid, no estan definidos');

        if (empty($data))
            $data = array();

        //parametro checkbox
        $checkbox = (empty($checkbox)) ? FALSE : $checkbox;

        $this->CI->lang->load('app_grid');

        $operation_id = (isset($operation_id)) ? $operation_id : $this->operation_id;

        $children_operation_content = $this->getChildrenContentByOperation($operation_id);

        $definition_columns = $this->buildContentOperations($children_operation_content, $checkbox);

        //Contamos el toTal de operaciones.
        //Agregamos una columna y un fieldname para ubicar el checkbox. si es necesario.
        $total = count($children_operation_content) + (integer) $checkbox;

        $this->setGridInformationFields($total, $operation_id);

//        $data = $this->orderDataByFields($data); //falta el filtro de categoria

        $headers = $this->getGridFieldsLabel();
        $fields_name = $this->getGridFieldsName();

        $view_params = array(
            'headers' => $headers,
//            'data' => $data,
            'fields_name' => $fields_name,
            // 'total_rows' => $count, //Comentado xq no se usa actualmente
            'operation_url' => (isset($operation_url)) ? $operation_url : $this->operation_url,
            'operation_id' => $operation_id,
            'definition_columns' => $definition_columns,
            'actionbar' => $this->getActionbar($operation_id),
            'title' => ($this->target_on == 'window' OR (isset($show_title) and $show_title == FALSE )) ? '' : $this->operation_title,
            'target_on' => $this->target_on
        );
        return $this->CI->load->view('engine/grid', $view_params, TRUE);
    }

    /**
     * Se encarga de obtener los campos para la operacion que se esta ejecutando
     *
     * @access	public
     * @return  array arreglo con los resultados del proceso
     * @author  Nohemi Rojas
     * @version V-1.0 23/11/12 11:35 AM
     */
    public function getFields($operation_id = NULL) {

        $ope_id = ($operation_id) ? $operation_id : $this->operation_id;
        $fields = $this->CI->engine_model->getFields($ope_id);
        if (!$fields)
            die('No hay campos asociados a esta operacion');
        $this->setFormCategory($fields);
        return $fields;
    }

    /**
     * Se encarga de recorrer un arreglo de campos para verificar cual de ellos es
     * de tipo categoria, en caso de que lo sean, buscara el valor de dicha categoria
     * para colocarlo como parte del arreglo
     *
     * @access	private
     * @param array $fields Arreglo con los campos a mostrar en el form
     * @author  Nohemi Rojas
     * @version V-1.0 24/11/12 11:35 AM
     */
    private function setFormCategory(&$fields) {
        if (!$fields)
            return;

        foreach ($fields as $key => $field) {

            //Caso Comobos anidados
            // if ($field['type'] == 'select' and !empty($field['filter_params'])) {
            if (preg_match('/select|checklist/', $field['type']) and !empty($field['filter_params'])) {
                $fields[$key]['value'] = $this->CI->engine_model->getFilterParamsData($field['filter_params']);
            } else {

                //Caso Categorias
                if (preg_match('/select|checklist/', $field['type']) and strstr($field['name'], 'cat_')) {
                    $fields[$key]['value'] = $this->CI->engine_model->getCategorys(preg_replace('/cat_/i', '', $field['name']));
                }

                //Caso de relacion foranea
                if (preg_match('/select|checklist/', $field['type']) and preg_match('/^id_/', $field['name'])) {
                    $fields[$key]['value'] = $this->CI->engine_model->getComboByTable($field['name'], $field['field_filter'], FALSE, 'label');
                }
            }
            //Caso chk_sexo
            if ($field['type'] == 'select' and $field['name'] == 'sexo') {
                $fields[$key]['value'] = array(
                    array('id' => 'Masculino', 'label' => 'Masculino'),
                    array('id' => 'Femenino', 'label' => 'Femenino')
                );
            }

            //Caso chk_letra
            if ($field['type'] == 'select' and $field['name'] == 'letra') {
                $fields[$key]['value'] = array(
                    array('id' => 'V', 'label' => 'V'),
                    array('id' => 'P', 'label' => 'P'),
                    array('id' => 'E', 'label' => 'E')
                );
            }
        }
    }

    /**
     * Realiza la construccion del actionbar para un determinada operacion
     *
     * @access private
     * @author Nohemi Rojas
     * @version V 1.0 23/11/2012 12:40:00
     */
    private function getActionbar($operation_id) {

        $permissions = $this->CI->session->userdata('permissions');
        $action = array();

        foreach ($permissions as $permission) {
            if ($permission['id_operation'] == $operation_id) {

                //Operaciones Permanetemente visibles en el actionbar
                if ($permission['render_on'] == "actionbar") {
                    $action[] = array('label' => $permission['_name'], 'url' => $permission['url'], 'visible' => TRUE);
                }
                //Operaciones que no son visibles en actionbar
                elseif ($this->CI->config->item('hide_operation')) {
                    if (preg_match('/(' . implode('|', $this->CI->config->item('hide_pattern')) . ')/is', $permission['url']))
                        $action[] = array('label' => $permission['_name'], 'url' => $permission['url'], 'visible' => FALSE);
                }
            }
        }

        if (!empty($action))
            return $this->CI->load->view('engine/actionbar', array('actions' => $action), TRUE);
        return FALSE;
    }

    /**
     * Realiza setters de las variables $names y $headers con la data obtenida
     * de la informaicon de fields
     *
     * @access private
     * @param   integer $total  Indica el todal de operaciones hijas a ser ubicadas dentro del grid.
     * @param   integer $operation_id  operacion del cual se quieren los campos.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 23/11/2012 12:40:00
     */
    private function setGridInformationFields($total, $operation_id = NULL) {
        $headers = $names = array();
        $fields = $this->getFields($operation_id);

        foreach ($fields as $field) {
            //Para no mostrar los campos hidden
            if (empty($field['hidden'])) {
                array_push($headers, $field['label']);
                array_push($names, $field['name']);
            }
        }

        //Ingresamos tantos headers y id_ec como tantas operaciones content existan
        for ($index = 0; $index < $total; $index++) {
            array_unshift($headers, '');
            array_unshift($names, 'id');
        }

        $this->setGridFieldsLabel($headers);
        $this->setGridFieldsName($names);
    }

    /**
     * Ordena un arreglo ($array) a partir de otro ($order_array).
     * Retornando $array ordenado.
     *
     * @access public
     * @param array $array arreglo asociativo con los valores.
     * @param array $order_array arreglo asociativo por el cual se  hara el ordenamiento.
     * @return array arreglo con los valores ordenados.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 24/11/2012 11:00
     */
    function sortArrayByArray($array, $order_array) {
        $ordered = array();
        foreach ($order_array as $key) {
            if (array_key_exists($key, $array)) {
                $ordered[$key] = $array[$key];
                unset($array[$key]);
            }
        }
        return $ordered + $array;
    }

    /**
     * Toma los datos enviados por el controlador y los filtra y ordena segun
     * el orden de la tabla field_operation.
     *
     * @access public
     * @param array $data datos a ser mostrados en la vista.
     * @return array de datos ordenados por field
     * @author Jose A. Rodriguez E.
     * @version V 1.0 24/11/2012 11:00
     */
    public function orderDataByFields($data) {
        if (empty($data))
            return;

        return array_map(
                        function ($row) {
                            //Convertimos los names en key de arreglo asociativo.
                            $this->setGridFieldsName(array_flip($this->getGridFieldsName()));
                            $row = array_intersect_key($row, $this->getGridFieldsName());

                            //Reconvertimos los names a values de arreglo asociativo para
                            //poder ordenar los campos de la consulta sql segun el
                            //atributo order definido por el metodo getFields().
                            $this->setGridFieldsName(array_flip($this->getGridFieldsName()));

                            $row = $this->sortArrayByArray($row, $this->getGridFieldsName());

                            return $row;
                        }, $data);
    }

    /**
     * Obtiene a partir de los permisos en session las operaciones hijas de un
     * grid particular.
     *
     * @access  private
     * @return  array    con las operaciones hijas del grid.
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 27/11/2012 10:32:06
     */
    private function getChildrenContentByOperation($operation_id) {
        $operations = $this->CI->session->userdata('permissions');
        $operation_children = array();
        foreach ($operations as $operation) {
            if ($operation['id_operation'] == $operation_id AND $operation['render_on'] == 'grid')
                array_push($operation_children, $operation);
        }
        return $operation_children;
    }

    /**
     * realiza el formato de las columnas que contendran las operaciones content del grid.
     *
     * @access  private
     * @param   array   $children_operation  operaciones hijas del grid.
     * @param   bool    $show_checkbox  inidica si se muestra un checkbox en el grid.
     * @return  array   con la extructura de formateo a ser aplicada.
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 27/11/12 18:00
     */
    private function buildContentOperations($children_operation, $show_checkbox = TRUE) {
        $aoColumnsDefs = array();

        if($show_checkbox){
            $mCheckbox = '<input type="checkbox" name="id[]" onclick="_checkActions(this);" value="\'+data+\'"/>';
            array_push($aoColumnsDefs, $mCheckbox);
        }

        foreach ($children_operation as $child_operation) {
            unset($child_operation['id']);
            unset($child_operation['id_operation']);
            unset($child_operation['content']);

            if ($child_operation['visual_type'] == 'button')
                $child_operation['visual_type'] = 'btn';

            //Indica la funciona ser empleada por la funcion js empleada por la operacion.
            $child_operation['js_function'] = 'request';

            $pattern = '/delete$/';
            if (preg_match($pattern, $child_operation['url']))
                $child_operation['js_function'] = '_delete';

            $child_operation['url'] = base_url($child_operation['url']);

            $format = '<a href="#" title="%s" class="%s" onclick="%s({url:\\\'%s\\\',data:{id:\\\'\'+data+\'\\\'}});"><i class="%s"></i></a>';
            $format = sprintf($format, $child_operation['_name'], $child_operation['visual_type'], $child_operation['js_function'], $child_operation['url'], $child_operation['icon']);

            array_push($aoColumnsDefs, $format);
        }

        return $aoColumnsDefs;
    }

    /**
     * Arma el script JS que encadena los combos dependientes a partir del campo
     * "id_parent" de la tabla fields, Totalmente amarrado al plugin de JQuery
     * jCombo
     *
     * @access  private
     * @param   array    $fields campos de la operacion
     * @return  Array    fields con el codigo JS
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 8/01/13 16:35
     */
    private function buildComboListener($fields) {

        foreach ($fields as &$field) {
            if (!empty($field['id_parent'])) {

                //Snipet de codigo JS necesario para el combo
                $chained = "
                    $(function() {
                        $('#{$field['id']}').chained('#{$field['id_parent']}');
                        $('#{$field['id']}').trigger('liszt:updated');
                        $('#{$field['id_parent']}').bind('change', function(){
                            $('#{$field['id']}').trigger('liszt:updated')
                        });
                    });
                ";
                $field['dependent'] = $chained;
            }
        }
        return $fields;
    }

    /**
     * Permite implementa la creacion de calendarios a partir de diferenetes parametros de configuracion.
     *
     * @access  public
     * @param   array    $param  parametros de configuracion.
     *      [save_url][OPTIONAL]      string  URL donde se guardaran la actualizacion los eventos.
     *                                          Si no posee, esta variable se entiende que el calendario no es editable.
     *      [source_url][OPTIONAL]    string  URL el cual contiene los eventos a ser visualizados.
     *                                          Por defecto es la url de la operacion, que se le adicionara el siguiente snippets.
     *                                          GET ?return=events
     *      [checker_url][OPTIONAL]   string  URL para validar si se realiza la peticion de los nuevos eventos.
     *                                          Por defecto es la url de la operacion, que se le adicionara el siguiente snippets.
     *                                          GET ?return=checker
     *      [frequency][OPTIONAL]     integer Frecuencia de actualizacion del calendario.
     *                                          El parametro es recibido en segundos, esto porque la funcion setInterval
     *                                          recibe un parametro microtime. Por defecto es 60 segundos.
     *      [render][OPTIONAL]        string Funcion JS que modifica el renderizado de los eventos en el calendario'
     *                                          Ej de la funcion JS function(event, element){var span_title = elem.find('span.fc-event-title');}
     *                                          La variable interna del js es para obtener la zona donde se renderiza el texto del evento.
     * @return  string    snippet del calendario.
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 25/01/13 14:51
     */
    public function calendar($params) {
        //Si el calendario no posee asociado operacion save, el mismo no sera editable.
        $params['editable'] = (empty($params['save_url'])) ? 'false' : 'true';

        //setamos la variable save_url, para evitar erros con JS.
        $params['save_url'] = (empty($params['save_url'])) ? '' : $params['save_url'];

        //Si no tiene source url, la url sera la de la operacion mas un snippets GET
        if (empty($params['source_url']))
            $params['source_url'] = base_url($this->operation_url . '?return=events');

        //Si no tiene checker url
        if (empty($params['checker_url'])) {
            $this->CI->load->config('poll');
            $params['checker_url'] = base_url($this->CI->config->item('calendar_path_mark') . $this->CI->config->item('calendar_file_mark'));
        }

        //Agregamos un valor por defecto a la frecuencia de actulizacion
        $params['frequency'] = (empty($params['frequency'])) ? 60000 : $params['frequency'] * 1000;

        $view = 'engine/calendar';
        return $this->CI->load->view($view, $params, TRUE);
    }

     public function grouping_grid($params=NULL) {

        extract($params);  

        if (empty($data))
            $data = array();

        //parametro checkbox
        $checkbox = (empty($checkbox)) ? FALSE : $checkbox;

        $this->CI->lang->load('app_grid');

        $operation_id = (isset($operation_id)) ? $operation_id : $this->operation_id;

        $children_operation_content = $this->getChildrenContentByOperation($operation_id);

        $definition_columns = $this->buildContentOperations($children_operation_content, $checkbox);

        //Contamos el toTal de operaciones.
        //Agregamos una columna y un fieldname para ubicar el checkbox. si es necesario.
        $total = count($children_operation_content) + (integer) $checkbox;

        $this->setGridInformationFields($total, $operation_id);

//        $data = $this->orderDataByFields($data); //falta el filtro de categoria

        $headers = $this->getGridFieldsLabel();
        $fields_name = $this->getGridFieldsName();

        $view_params = array(
            'headers' => $headers,
            'data' => $data,
            'fields_name' => $fields_name,
            // 'total_rows' => $count, //Comentado xq no se usa actualmente
            'operation_url' => (isset($operation_url)) ? $operation_url : $this->operation_url,
            'operation_id' => $operation_id,
            'definition_columns' => $definition_columns,
            'actionbar' => $this->getActionbar($operation_id),
            'title' => ($this->target_on == 'window' OR (isset($show_title) and $show_title == FALSE )) ? '' : $this->operation_title,
            'grouping' => $total
        );
        return $this->CI->load->view('engine/grouping_grid', $view_params, TRUE);
    }
}

// END View_engine class
/* End of file View_Engine.php */
/* Location: application/libraries/View_engine.php */