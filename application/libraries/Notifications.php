<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Notification
 *
 * Se encarga de definir y mostrar las reglas de notificacion para un usuario en session
 *
 * @package		CodeIgniter
 * @subpackage	        Libraries
 * @category		Libraries
 * @author		Nohemi Rojas<nohemir@gmail.com>, 
 * @since               V 1.0 28/01/13 03:19 PM
 */
class Notifications {

    private $CI;

    /**
     * Constructor
     * 
     * @access	public
     * @author  Nohemi Rojas
     * @version V-1.0 28/01/13 02:34 PM
     */
    public function __construct() {
        $this->CI = &get_instance();
        $this->CI->load->model('notification/notification_model');
    }

    /**
     * Metodo que obtiene las notificaciones
     * 
     * @access	public
     * @param boolean $param Condicion que indica si se desea escapar las comillas de la url de la notificacion
     * @return array Arreglo con los datos de cantidad de mensajes y los mensajes definidos
     * @author  Nohemi Rojas
     * @version V-1.0 29/01/13 02:52 PM
     */
    public function getNotification($param = false) {

        $role_name = $this->CI->session->userdata('role_name');

        //$role_name='validador';
        // $role_name='vendedor';
        switch ($role_name) {
            case 'validador':
                $level = 'validador';
                $data['url'] = 'cotizacion/cotizacion/validate';
                break;
            case 'Admin':
                $level = 'validador';
                $data['url'] = 'cotizacion/cotizacion/validate';
                break;
            case 'vendedor':
                $level = 'vendedor';
                $data['url'] = 'cotizacion/cotizacion/correct';
                break;
            case 'Logistica':
                $level = 'logistica';
                $data['url'] = 'cotizacion/cotizacion/detail';
                break;
            case 'Almacenista Despacho':
                $level = 'almacenista';
                $data['url'] = 'cotizacion/cotizacion/detail';
                break;
            default:
                return array('msg' => '', 'cant' => 0);
                break;
        }
        $data['data'] = $this->CI->notification_model->getValidationOpen($level);
        $data['param'] = $param;

        //Encriptamos el ID
        if (!empty($data['data']))
            foreach ($data['data'] as &$value)
                $value['id'] = $this->CI->encrypt->encode($value['id']);

        $cant = ($data['data']) ? count($data['data']) : 0;

        $msg = ($cant > 0) ? $this->CI->load->view('notification/list', $data, TRUE) : '';
        return array('msg' => preg_replace("[\n|\r|\n\r]", " ", $msg), 'cant' => $cant);
    }

}

// END Notification class
/* End of file Notification.php */
/* Location: application/libraries/Notification.php */