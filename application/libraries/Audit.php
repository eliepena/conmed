<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * Libreria que recopila la informacion a ser almacenada dentro de la
 * auditoria del sistema
 *
 * @package     CodeIgniter
 * @subpackage  Libraries
 * @category    Libraries
 * @author		Jose Rodriguez  <josearodrigueze@gmail.com>
 * @since       V 1.0 11/04/2013 04:21:25 pm
 */
class Audit {

    /**
     * Instancia de CodeIgniter
     *
     * @access private
     * @var string
     */
    private $CI;

    /**
     * Resulado de la operacion ejecutada
     * @var string
     * @access private
     * @author Jose A. Rodriguez E. <josearodrigueze@gmail.com>
     */
    private $chk_result = NULL;

    /**
     * Datos manejados por la operacion
     * @var array
     * @access private
     * @author Jose A. Rodriguez E. <josearodrigueze@gmail.com>
     */
    private $operation_data = NULL;

    /**
     * Constructor
     *
     * @access	public
     * @author  Jose Rodriguez
     * @version V-1.0 11/04/2013 04:19:57 pm
     */
    public function __construct() {
        $this->CI = &get_instance();
        $this->CI->load->library('user_agent');
        $this->CI->load->model('audit/audit_model');
        $this->chk_result = 'ACCESS';
    }

    public function getChkResult() {
        return $this->chk_result;
    }

    /**
     * Setea el valor de la variable privada chk_result, segun el valor pasado
     * TRUE = SUCCESS, FALSE = FAILRE, X = ACCESS (Donde x es cualquier otro valor)
     *
     * @access  public
     * @param   mixed   $chk_result resultado de la operacion
     * @author  Jose A. Rodriguez E. <josearodrigueze@gmail.com>
     * @version V 1.0 12/04/2013 02:00:38 pm
     */
    public function setChkResult($chk_result) {

        //Evaluamos las posibles condiciones de para el chk_result
        if($chk_result === TRUE)
            $chk_result = 'SUCCESS';
        elseif($chk_result === FALSE)
            $chk_result = 'FAILURE';
        else
            $chk_result = 'ACCESS';

        $this->chk_result = $chk_result;
    }

    public function getOperationData() {
        return $this->operation_data;
    }

    public function setOperationData($operation_data) {
        $this->operation_data = $operation_data;
    }

    /**
     * Realiza la recopilzacion d elos datos para a ser almanados en la tabla auditoria
     *
     * @access  public
     * @author  Jose A. Rodriguez E. <josearodrigueze@gmail.com>
     * @version V 1.0 11/04/2013 05:30:44 pm
     */
    public function register() {
        $uri = $this->CI->uri->uri_string();

        //Si es una excepcion no registra en el log.
        if(strpos($uri, 'EXC_'))
            return FALSE;

        $id_user = $this->CI->session->userdata('user_id');
        if(empty($id_user))
            $id_user = NULL;

        $data = array(
            'id_user' => $id_user,
            'ip' => $this->CI->input->ip_address(),
            'access_time' => date('Y-m-d H:i:s'),
            'os' => $this->CI->agent->platform(),
            'browser' => $this->CI->agent->browser(),
            '_version' => $this->CI->agent->version(),
            'url' => base_url($uri),
            'chk_result' => $this->getChkResult()
        );

        //Si existe data se guarda
        //Eso la variable privada y no el metodo get xq empty no evalua rentorno fns
        if(!empty($this->operation_data))
            $data['_data'] = json_encode($this->getOperationData());

        $this->CI->audit_model->insert($data);
    }

}

/* End of Audit class */
/* End of file Audit.php */
/* Location: application/libraries/Audit.php */