<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of almacen
 *
 * Esta clase provee la implementacion para la capa de control de
 * acciones de la entidad almacen y sus entidades dependientes o extendidas
 *
 * @package Module
 * @category Controller
 * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V-1.0 02/10/13
 */
class check_almacen extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('check_almacen_model');
        $this->load->helper('view_engine');
    }

    /**
     * Provee la disponibilidad de articulos en el almacen
     * Este metodo es consultado mediante un CronJob
     *
     * @access  public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 21/10/13 10:51 AM
     */
    function index() {
        if (!$this->input->is_cli_request()) {
            echo utf8_decode("Acceso denegado") . PHP_EOL;
            return;
        }
        $cotizaciones_encontradas = array();
        $articulos_almacen = $this->check_almacen_model->availabilityOfItems();
        $cotizaciones = $this->check_almacen_model->getQuoteWithFilterStatus('aprobada');
        //$this->offOutput();
        if ($articulos_almacen):
            foreach ($articulos_almacen as $key => $value):
                foreach ($cotizaciones as $clave => $valor):
                    foreach ($valor as $clave_cot => $valor_cot):
                        if ($value['id'] == $valor_cot['id_articulo']):
                            $cotizaciones_encontradas[$clave]['id_cotizacion'] = $clave;
                            $cotizaciones_encontradas[$clave]['id_articulo'] = $value['id'];
                        endif;
                    endforeach;


                endforeach;
            endforeach;
            $this->sendQuoteNotification($cotizaciones_encontradas);
        endif;
    }

    /**
     * Envia correo masivos a las cotizaciones seleccionadas
     *
     * @access  public
     * @param   array   $params Parametros de entrada
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 22/10/13 02:21 PM
     */
    public function sendQuoteNotification($params = array()) {

        if (sizeof($params) > 0):
            foreach ($params as $key => $value):

                $destination_path = '';
                $html = array();
                $code = '';
                $email = array();
                $body = '';
                $subject = '';

                $get_email_vendedor = $this->check_almacen_model->getEmailVendedor(array('cot.id' => $value['id_cotizacion']));
                $code = $this->check_almacen_model->getCode($value['id_cotizacion']);
                $html['datos'] = $this->check_almacen_model->getById($value['id_cotizacion']);
                $html ['articulo'] = $this->check_almacen_model->getInfoArticle($value['id_articulo']);
                $html['html'] = sprintf($this->lang->line('body_cot_almacen'), $code);
                $html['title'] = $this->lang->line('subject_cot_almacen');

                $email = array($get_email_vendedor[0]['correo_electronico']);
                $body = $this->load->view('check_almacen_email', $html, TRUE);
                $subject = $this->lang->line('subject_cot_almacen');
                $attachment = $destination_path;
                sendEmail($email, $body, $subject, $body, $attachment);
            endforeach;
        endif;
    }

}