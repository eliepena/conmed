<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of send_notification
 *
 * Esta clase provee la implementacion para la capa de control de
 * acciones de envio de correos masivos mediante un ConJob
 *
 * @package Controller
 * @category Controller
 * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V-1.0 28/11/13 04:29 PM
 */
class send_notification extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('send_notification_model');
        $this->load->model('cotizacion/cotizacion_model', 'cotizacion_model');
        $this->load->model('persona/paciente_model', 'paciente_model');
        $this->load->helper('view_engine');
    }

    /**
     * Provee la disponibilidad de cotizaciones
     * pendientes para enviar via correo electronico
     * Este metodo es consultado mediante un CronJob
     *
     * @access  public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 28/11/13 04:33 PM
     */
    function mail_cotizacion() {
        if (!$this->input->is_cli_request()) {
            echo utf8_decode("Acceso Denegado") . PHP_EOL;
            return;
        }
        $cotizaciones_encontradas = array();
        $cotizaciones_para_enviar = $this->send_notification_model->getIdQuotesForSendMail();
        if ($cotizaciones_para_enviar):
            foreach ($cotizaciones_para_enviar as $key => $value):
                $this->sendQuoteNotification($value['id'], 'emitida');
                $is_valid = $this->cotizacion_model->isValid($value['id']);
                if ($is_valid):
                    $this->sendQuoteNotification($value['id'], 'procesada');
                endif;
            endforeach;
        endif;
    }

    /**
     * Envia correo masivos a las cotizaciones pendientes
     *
     * @access  public
     * @param   array   $params Parametros de entrada
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 22/10/13 02:21 PM
     */
    public function sendQuoteNotification($id_cotizacion = NULL, $status = '') {
        $destination_path = '';
        $html = array();
        $code = '';
        $email = array();
        $body = '';
        $subject = '';

        if ($id_cotizacion):
            $status_name = mb_convert_case($status, MB_CASE_TITLE, "UTF-8");
            $code = $this->cotizacion_model->getCode($id_cotizacion);
            $html['datos'] = $this->cotizacion_model->getById($id_cotizacion);
            switch ($status_name) {
                case 'Emitida':

                    $get_email_vendedor = $this->cotizacion_model->getEmailVendedor($id_cotizacion);
                    $email = array($get_email_vendedor[0]['correo_electronico']);
                    $html['html'] = sprintf($this->lang->line('body_cot_emitida'), $code);
                    $html['title'] = $this->lang->line('subject_cot_emitida');
                    $body = $this->load->view('cotizacion/email_cotizacion', $html, TRUE);
                    $subject = $this->lang->line('subject_cot_emitida');

                    $name_file = "cot_" . $code;
                    $destination_path = 'uploads/cotizaciones/membrete_on/';
                    $destination_path = $destination_path . $name_file . '.pdf';

                    if (!file_exists($destination_path)):
                        $parametros = array('id' => $id_cotizacion, 'membrete' => TRUE, 'tipo' => 'cotizacion');
                        $retorno = $this->quoteReport($parametros);
                        if ($retorno['sucess'] && file_exists($retorno['file'])):
                            $destination_path = $retorno['file'];

                        else:
                            $destination_path = '';
                        endif;

                    endif;
                    $attachment = $destination_path;
                    break;
                case 'Procesada':
                    $email_medico = $this->cotizacion_model->getMedico($id_cotizacion)->correo_electronico;
                    $get_email_vendedor = $this->cotizacion_model->getEmailVendedor($id_cotizacion);
                    $email = array($get_email_vendedor[0]['correo_electronico'], $email_medico);
                    $html['html'] = sprintf($this->lang->line('body_cot_procesada'), $code);
                    $html['title'] = $this->lang->line('subject_cot_procesada');
                    $body = $this->load->view('cotizacion/email_cotizacion', $html, TRUE);
                    $subject = $this->lang->line('subject_cot_procesada');
                    $attachment = $destination_path;
                    break;
                default:
                    return FALSE;
                    break;
            }
            if (sendEmail($email, $body, $subject, $body, $attachment)):
                $this->send_notification_model->changeValueSendMailByQuote($id_cotizacion);
            endif;

        endif;
    }

    /**
     * Genera o ubica documentos de la cotizacion de acuerdo al tipo de documento.
     *
     * @access public
     * @param array $params Datos de entrada para funcionamiento del metodo
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 02/12/13 11:28 AM
     */
    private function quoteReport($params = array()) {
        $id = (isset($params['id']) ? $params['id'] : NULL);
        $type_report = (isset($params['tipo']) ? $params['tipo'] : NULL);
        $membrete = (isset($params['membrete']) ? $params['membrete'] : NULL);
        $dir_modo_impresion = ($membrete == 'true' ? 'membrete_on' : 'membrete_off');
        $alvuelo = $this->config->item('pdf_al_vuelo');
        if ($id):
            $data = $this->cotizacion_model->get_data_cotizacion($id);
            $data = $data[0];

            switch ($type_report) {
                case 'backup':
                    $filename = "uploads/cotizaciones/{$dir_modo_impresion}/backup/backup_$data[codigo].pdf";
                    if (file_exists($filename) && !$alvuelo):
                        $response = array(
                            'sucess' => true,
                            'file' => $filename
                        );
                    else:
                        $getDatosPdf = $this->cotizacion_model->getDatosPdf($data['id_medico'], $data['id_seguro'], $data['id_paciente'], $data['cat_tipo_pago'], $data['id_vendedor']);
                        $datos_paciente = $this->paciente_model->getPaciente($data['id_paciente'], 'tlf_movil');
                        $params_pdf = array(
                            'nro_cotizacion' => $data['codigo'],
                            'fecha_emision' => date('d/m/Y'),
                            'forma_pago' => $getDatosPdf['tipo_pago'],
                            'tiempo_entrega' => 'INMEDIATA',
                            'vendedor' => $getDatosPdf['vendedor'],
                            'dr_tratante' => $getDatosPdf['medico'],
                            'cliente' => $getDatosPdf['pnombre'],
                            'rif' => $getDatosPdf['pcedula'],
                            'nit' => '',
                            'seguro' => ($getDatosPdf['nseguro'] == null ? 'No Aplica' : $getDatosPdf['nseguro']),
                            'tlf' => (empty($datos_paciente[0]['tlf_movil']) || $datos_paciente[0]['tlf_movil'] == null ? 'Sin Informacion' : $datos_paciente[0]['tlf_movil']),
                        );
                        $params_pdf['cuerpo_tabla'] = $this->cotizacion_model->getArticulosByCot($data['id'], 'MAT', '1');
                        $response = array(
                            'sucess' => true,
                            'file' => $this->_create_document($params_pdf, false, 'backup', $membrete)
                        );
                    endif;

                    break;
                case 'instrumento':
                    $filename = "uploads/cotizaciones/{$dir_modo_impresion}/instrumentos/instru_$data[codigo].pdf";
                    if (file_exists($filename) && !$alvuelo):
                        $response = array(
                            'sucess' => true,
                            'file' => $filename
                        );
                    else:

                        $getDatosPdf = $this->cotizacion_model->getDatosPdf($data['id_medico'], $data['id_seguro'], $data['id_paciente'], $data['cat_tipo_pago'], $data['id_vendedor']);
                        $datos_paciente = $this->paciente_model->getPaciente($data['id_paciente'], 'tlf_movil');
                        $params_pdf = array(
                            'nro_cotizacion' => $data['codigo'],
                            'fecha_emision' => date('d/m/Y'),
                            'forma_pago' => $getDatosPdf['tipo_pago'],
                            'tiempo_entrega' => 'INMEDIATA',
                            'vendedor' => $getDatosPdf['vendedor'],
                            'dr_tratante' => $getDatosPdf['medico'],
                            'cliente' => $getDatosPdf['pnombre'],
                            'rif' => $getDatosPdf['pcedula'],
                            'nit' => '',
                            'seguro' => ($getDatosPdf['nseguro'] == null ? 'No Aplica' : $getDatosPdf['nseguro']),
                            'tlf' => (empty($datos_paciente[0]['tlf_movil']) || $datos_paciente[0]['tlf_movil'] == null ? 'Sin Informacion' : $datos_paciente[0]['tlf_movil']),
                        );
                        $params_pdf['cuerpo_tabla'] = $this->cotizacion_model->getArticulosByCot($data['id'], 'INS');
                        $response = array(
                            'sucess' => true,
                            'file' => $this->_create_document($params_pdf, false, 'instrumento', $membrete)
                        );
                    endif;
                    break;
                case 'cotizacion':
                    $filename = "uploads/cotizaciones/{$dir_modo_impresion}/cot_$data[codigo].pdf";
                    if (file_exists($filename) && !$alvuelo):
                        $response = array(
                            'sucess' => true,
                            'file' => $filename
                        );
                    else:
                        $getDatosPdf = $this->cotizacion_model->getDatosPdf($data['id_medico'], $data['id_seguro'], $data['id_paciente'], $data['cat_tipo_pago'], $data['id_vendedor']);
                        $datos_paciente = $this->paciente_model->getPaciente($data['id_paciente'], 'tlf_movil');
                        $params_pdf = array(
                            'nro_cotizacion' => $data['codigo'],
                            'fecha_emision' => date('d/m/Y'),
                            'forma_pago' => $getDatosPdf['tipo_pago'],
                            'tiempo_entrega' => 'INMEDIATA',
                            'vendedor' => $getDatosPdf['vendedor'],
                            'dr_tratante' => $getDatosPdf['medico'],
                            'cliente' => $getDatosPdf['pnombre'],
                            'rif' => $getDatosPdf['pcedula'],
                            'nit' => '',
                            'seguro' => ($getDatosPdf['nseguro'] == null ? 'No Aplica' : $getDatosPdf['nseguro']),
                            'tlf' => (empty($datos_paciente[0]['tlf_movil']) || $datos_paciente[0]['tlf_movil'] == null ? 'Sin Informacion' : $datos_paciente[0]['tlf_movil']),
                        );
                        $getMaterialesPdf = $this->cotizacion_model->getArticulosByCot($data['id'], 'MAT');
                        $materiales = array();
                        foreach ($getMaterialesPdf as $clave => $valor):

                            $materiales[] = array(
                                'codigo_m' => $valor['id'],
                                'modelo_m' => $valor['modelo'],
                                'descripcion_m' => $valor['descripcion'],
                                'gart_m' => '',
                                'cant_m' => $valor['cantidad'],
                                'precio_m' => number_format($valor['precio'], 2, ',', '.'),
                                'neto_m' => number_format($valor['precio'] * $valor['cantidad'], 2, ',', '.')
                            );
                        endforeach;
                        $params_pdf['monto_exento'] = number_format($data['monto_exento'], 2, ',', '.');
                        $params_pdf['monto_gravable'] = number_format($data['monto_gravable'], 2, ',', '.');
                        $params_pdf['porcentaje_descuento'] = $data['descuento'];
                        $params_pdf['calculo_descuento'] = number_format(($data['monto_gravable'] * $data['descuento']) / 100, 2, ',', '.');
                        $params_pdf['recargo'] = number_format(($data['monto_gravable'] * $data['recargo']) / 100, 2, ',', '.');
                        $params_pdf['p_recargo'] = $data['recargo'];
                        $params_pdf['porcentaje_iva'] = $this->config->item('valor_iva');
                        $params_pdf['calculo_iva'] = number_format(($data['monto_gravable'] * $this->config->item('valor_iva')) / 100, 2, ',', '.');
                        $params_pdf['neto'] = number_format($data['monto_total'], 2, ',', '.');
                        $params_pdf['materiales'] = $materiales;
                        $response = array(
                            'sucess' => true,
                            'file' => $this->_create_document($params_pdf, false, 'cotizacion', $membrete)
                        );
                    endif;
                    break;
                default:
                    $params_pdf['cuerpo_tabla'] = array();
                    break;
            }
        else:
            $response = array(
                'sucess' => false
            );
        endif;
        return $response;
    }

    /**
     * Crea los documentos pertinentes a la cotizacion
     *
     * @access  private
     * @param   array   $params Arreglos de datos a mostrar en el documento
     * @param boolean $background_no_validate Indica si el documento lleva o no la marca NO VALIDO de fondo
     * @param string $document Indica el tipo de documento a crear
     * @param boolean $membrete Determina si el documento lleva o no el fondo predeterminado
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version 1.0 02/12/13 11:38 AM
     */
    function _create_document($params = array(), $background_no_validate = FALSE, $document = '', $membrete = FALSE) {
        $this->lang->load('app_doc');
        $this->lang->load('app_email');
        $this->load->library('encrypt');
        $this->load->helper('view_engine_helper');
        extract($params);
        $dir_modo_impresion = ($membrete == 'true' ? 'membrete_on' : 'membrete_off');
        switch ($document) {
            case 'cotizacion':
                $view = 'pdf_cotizacion';
                $name = 'cot';
                $dir = 'cotizaciones/' . $dir_modo_impresion;
                break;
            case 'backup':
                $view = 'pdf_backup';
                $name = 'backup';
                $dir = 'cotizaciones/' . $dir_modo_impresion . '/backup';
                break;
            case 'instrumento':
                $view = 'pdf_instrumentos';
                $name = 'instru';
                $dir = 'cotizaciones/' . $dir_modo_impresion . '/instrumentos';
                break;

            default:
                $view = 'pdf_cotizacion';
                $name = 'cot';
                break;
        }
        //Cadena que contiene el string para generar el pdf
        $html = $this->load->view($view, $params, TRUE);
        // -----------------------------
        // -----------------------------
        //Asignacion de nombre, ruta y generacion del documento
        $name_file = "{$name}_{$nro_cotizacion}";
        $destination_path = "uploads/{$dir}/";
        $destination_path = ($background_no_validate ? $destination_path . "preview/" : $destination_path);

        generate_pdf($name_file, $destination_path, 'F', $html, $background_no_validate, $membrete);
        return utf8_decode("{$destination_path}" . "{$name_file}.pdf");
        // -----------------------------
    }

}