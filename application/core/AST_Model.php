<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of AST_Model
 *
 * Esta clase provee la implementacion basica para la capa de acceso a base
 * de datos, entre la implemtacion de sus metodos encontramos CRUD basicos y
 * algunos metodos de filtrado de datos.
 *
 * @package Core
 * @category Application Core
 * @author Jose A. Rodriguez E. <josearodrigueze@gmail.com>
 * @version V-1.0 20/11/2012 09:42
 */
class AST_Model extends CI_Model {

    /**
     * Contiene el nombre del esquema asociado a tabla del modelo.
     * @var string
     * @access private
     * @author Jose A. Rodriguez E.
     */
    private $schema = NULL;

    /**
     * Contiene el nombre de la tabla asociada al modelo.
     * @var string
     * @access private
     * @author Jose A. Rodriguez E.
     */
    protected $table_name = NULL;

    /**
     * Indica si la entidad tratada herada o no de la entidad super table
     * @var boolean
     * @access protected
     * @author Nohemi Rojas <nohemir@gmail.com>
     */
    protected $super_table = TRUE;

    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        $this->schema = 'business';
        $this->init();
    }

    /**
     * Metodo getter de schema.
     *
     * @access public
     * @return string Nombre del esquema al que pertence la tabla asociada al modelo.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 20/11/2012 16:20
     */
    public function getSchema() {
        return $this->schema;
    }

    /**
     * Metodo setter de schema.
     *
     * @access public
     * @param string Nombre del esquema al que pertence la tabla asociada al modelo.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 20/11/2012 16:20
     */
    public function setSchema($schema) {
        $this->schema = $schema;
    }

    /**
     * Concatena el nombre del squema con el nombre de la tabla.
     *
     * @access public
     * @return string Nombre de la tabla al que pertence el modelo.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 20/11/2012 16:20
     */
    public function concatSchemaTable() {
        return $this->getSchema() . '.' . $this->table_name;
    }

    /**
     * Provee las configuraciones inicales para el acceso a datos. Si se pasa un
     * parametro este toma ese parametro como nombre de la tabla a ser accedida.
     *
     * @access public
     * @param string $table_name Nombre de la tabla asociada al modelo.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 20/11/2012 16:20
     */
    public function init($table_name = NULL) {
        $this->table_name = $this->getTableName($table_name);
    }

    /**
     * Retorna el nombre de la clase que esta instanciando esta clase.
     *
     * @access private
     * @return string Nombre de la clase.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 20/11/2012 14:07
     */
    private function getCurrentClassName() {
        $class_name = get_class($this);
        return $class_name;
    }

    /**
     * Elimina el sufijo "_model" dato un nombre de modelo, retorna asi el
     * nombre de la tabla a en base de datos.
     *
     * @access protected
     * @param string $table_name Nombre de la tabla. Si es Vacio se toma a partir del nombre de la clase actual
     * @return string Nombre de la tabla.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 date
     */
    protected function getTableName($table_name = NULL) {
        if (empty($table_name)) {
            $subject = strtolower($this->getCurrentClassName());
            $search = '_model';
            $replace = '';
            $table_name = str_replace($search, $replace, $subject);
            return $table_name;
        }

        return strtolower($table_name);
    }

    /**
     * Provee las implementacion basica para realizar inserciones dentro de la
     * base de datos.
     *
     * @access  public
     * @param   Array   $data datos a ser guardados dentro de la base de datos.
     * @return  Boolean Indicando el resultado de la operacion.
     * @author  Jose A. Rodriguez E.
     * @version V-1.0 20/11/2012 09:42
     */
    function insert($data) {
        $this->db->insert($this->concatSchemaTable(), $data);
        return $this->db->affected_rows() ? TRUE : FALSE;
    }

    /**
     * Dado un identificador obtiene los datos asociados al identifiador.
     *
     * @access  public
     * @param   Integer $id Identificador del registro a ser consultado.
     * @param   string $field campo por el cual se realizara la busqueda
     * @return  Array   Con los datos del registro consultado.
     * @author  Jose A. Rodriguez E.
     * @version V-1.0 20/11/2012 09:42
     */
    function getById($id, $field = 'id') {
        $this->selects();
        $this->db->where($field, $id);
        $this->whereDelete();
        $rs = $this->db->get($this->concatSchemaTable());
        return ($rs->num_rows() > 0) ? $rs->row_array() : FALSE;
    }

    /**
     * Dado un arreglo se obtiene todas las coincidencias del arreglo por
     * clave/valor, donde clave es el campo la base de datos y valor el valor a
     * ser filtrado.
     *
     * @access  public
     * @param   Array   $params <p>Contiene los pares clave valor a ser filtrados.</p>
     * @param   boolean $returned [optional] <p>
     *      <b>TRUE(default)</b> retorna el resultado de la consulta a BD.
     *      <b>FALSE</b> realiza la ejecucion del metodo where de CI.
     * </p>
     * @return  mixed   <p>
     *  <b>Array<b/> con el resultado de la consulta.
     *  <b>Void<b/> setea el where del metodo de CI.
     * </p>
     * @author  Jose A. Rodriguez E.
     * @version V-1.0 20/11/2012 09:42
     */
    function getByFields($params = array(), $returned = TRUE) {
        $this->selects();
        $this->db->from($this->concatSchemaTable());

        if (!empty($params))
            $this->_ilike($params, 'OR', TRUE);

        $this->whereDelete();

        if ($returned) {
            $rs = $this->db->get();
            return ($rs->num_rows() > 0) ? $rs->result_array() : FALSE;
        }
    }

    /**
     * Obtiene todos los registros de una tabla
     *
     * @access  public
     * @param   integer $offset <p>Indica desde donde se realiza la busqueda del registro.</p>
     * @param   Array   $params [optional] <p>Contiene los pares clave valor a ser filtrados.</p>
     * @param   Array   $sort [optional] <p>Contiene los pares clave valor indicando el orden de la cosulta.</p>
     * @return  Array   Con los datos del registro consultado.
     * @author  Jose A. Rodriguez E.
     * @version V-1.0 20/11/2012 09:42
     */
    function getAll($offset, $params = array(), $sort = array()) {
        $returned = FALSE;

        $this->getByFields($params, $returned);

        if(!empty($offset))
            $this->_limit($offset);

        if (empty($sort))
            $this->orderBy();
        else
        //Define el orden con que se mostraran los datos.
            foreach ($sort as $field_bd => $order)
                $this->db->order_by($field_bd, $order);

        $rs = $this->db->get();
        return ($rs->num_rows() > 0) ? $rs->result_array() : FALSE;
    }

    /**
     * Obtiene el numero total de resultados para una consulta.
     *
     * @access public
     * @param array $params parametros de busqueda donde clave es campo en bd y valor es el filtro.
     * @return integer Total de resultados
     * @author Jose A. Rodriguez E.
     * @version V 1.0 24/11/2012 11:40
     */
    public function getCount($params = array()) {
        $returned = FALSE;

        $this->getByFields($params, $returned);

        return $this->db->count_all_results();
    }

    /**
     * Provee las implementacion basica para realizar la actualizacion de una
     * registro dentro de la base de datos.
     *
     * @access  public
     * @param   Integer $id Identificador del registro a ser actualizado..
     * @param   Array   $data datos a ser guardados dentro de la base de datos.
     * @param   string $field campo por el cual se realizara el filtrado
     * @return  Boolean Indicando el resultado de la operacion.
     * @author  Jose A. Rodriguez E.
     * @version V-1.0 20/11/2012 09:42
     */
    function update($id, $data, $field = 'id') {

        //Si $id es un arreglo WHERE or esto para el delete masivo.
        if (is_array($id))
            foreach ($id as $id_ec)
                $this->db->or_where($field, $id_ec);

        //funcionamiento normal actualizar un registro.
        else
            $this->db->where($field, $id);

        $this->db->update($this->concatSchemaTable(), $data);
        return $this->db->affected_rows() ? TRUE : FALSE;
    }

    /**
     * Provee las implementacion basica para realizar eliminado fisico de un
     * registro dentro de la base de datos.
     *
     * @access  public
     * @param   Integer $id Identificador del registro a ser actualizado.
     * @return  Boolean Indicando el resultado de la operacion.
     * @author  Jose A. Rodriguez E.
     * @version V-1.0 20/11/2012 09:42
     */
    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete($this->concatSchemaTable());
        return $this->db->affected_rows() ? TRUE : FALSE;
    }

    /**
     * Define el limite y el offset a ser empleados en las consultas a BD,
     * haciendo el llamado al metodo limit de CI.
     *
     * @access public
     * @param integer $offset Indica indice donde se empiezan a mostrar los resultados.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 21/11/2012 15:41
     */
    public function _limit($offset = 0) {
        $limit = $this->config->item('limit');
        if ($limit >= 0) {
            $this->db->limit($limit, $offset);
        }
    }

    /**
     * Obtiene la metadata de la tabla perteneciente al modelo.
     *
     * @access  public
     * @param   array    $param[optional]  paramDescripcion
     * @return  array    arreglos de objetos.
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 26/11/2012 12:41
     */
    public function getFieldsMetadata() {
        return $this->db->field_data($this->concatSchemaTable());
    }

    /**
     * Provee la sentencia condicional where para no obtener los valores eliminados.
     *
     * @access  protected
     * @param   integer    $delete[optional]  el valor que tendra eliminado.
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 30/11/12 14:42
     */
    protected function whereDelete($delete = '0') {
        $this->db->where($this->concatSchemaTable() . '._delete', $delete);
    }

    /**
     * Metodo que provee la posibilidad de agregar select activerecord de campos
     * a las consultas de getById() y getByFields()(includo los metodos que lo
     * implementen como getAll()).
     * por defecto este metodo no tiene implementacion alguna.
     *
     * @access  protected
     * @return  void
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 6/12/12 11:29
     */
    protected function selects() {

    }

    /**
     * Provee la implementacion del "ILIKE" de postgresql. Ejecuta el metodo
     * $this->db->where() de CI para hacer los where ilike de los querys.
     *
     * @access  protected
     * @param   array    $params datos a los que se le aplica el ilike
     * @param   string   $operator[OPTIONAL]    Indica se usara el sentencia OR o no. Default AND;
     * @param   boolean  $parenthesis[OPTIONAL] Indica si el ilike se encirra entra parentesis. Default FALSE;
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 6/12/12 13:15
     */
    protected function _ilike($params, $operator = 'AND', $parenthesis = FALSE) {
        $query = '';

        foreach ($params as $field => $value) {
            $value = $this->db->escape_like_str($value);
            $query.= " $field ILIKE '%$value%'    $operator";
        }

        //Quitamos el operador al final del query
        $query = substr($query, 0, -5);

        //Evaluamos si se debe colocar parentesis.
        if ($parenthesis)
            $query = "($query)";

        $this->db->where($query, NULL, FALSE);
    }

    /**
     *
     * Indica si la entidad tratada hereda o no de la entidad super_table
     *
     * @access  public
     * @return boolean
     * @author  Nohemi Rojas
     * @version V 1.0 14/01/13 02:50 PM
     */
    function inheritsSuperTable() {
        return $this->super_table;
    }

    /**
     * Funcion que determina si un campo es unico
     *
     * @param string $search_field esquema, tabla y campo a determinar si es unico
     * @param string $value_field valor del campo a determinar si es unico
     * @param string $filter_field campo de filtro para para comparacion (id_ec)
     * @param string $filter_value valor del campo de filtro para para comparacion (id_ec)
     * @return boolean
     *
     * @access public
     * @author Nohemi Rojas
     * @version V 1.0 15/01/13 03:49 PM
     */
    function is_unique($search_field, $value_field, $filter_field, $filter_value) {
        list($schema, $table, $field) = explode('.', $search_field);

        $this->db->select($field);
        $this->db->where("$filter_field != ", $filter_value, TRUE);
        $this->db->where($field, $value_field);
        $this->db->from("$schema.$table");
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? FALSE : TRUE;
    }

    /**
     * Metodo vacio que es implementado el metodo getAll(),
     * para proveer soporte a ordenamiento por defecto.
     *
     * @access  protected
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 03/04/2013 04:28:08 pm
     */
    protected function orderBy() {

    }

    /**
     * Metodo de insercion con retorno de datos
     *
     * @access  public
     * @param array $data Datos a insertar.
     * @author  Elieze Peña <pe.eliezer@gmail.com>
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 2.0 20/09/13 11:26 AM
     */
    function insertWithReturning($data = array(), $table = '') {

        if (sizeof($data) > 0 && is_array($data)):
            $table = (empty($table) ? $this->concatSchemaTable() : $table);
            foreach ($data as $key => $value) {
                if (empty($value)) {
                    unset($data[$key]);
                    continue;
                }
                if (is_string($key)) {
                    $data[$key] = "'" . $value . "'";
                }
            }

            $keys_array = array_keys($data);
            $keys = join(',', $keys_array);
            $values_array = array_values($data);
            $values = join(',', $values_array);

            $insert = ' INSERT INTO %s (%s) values (%s) returning * ';
            $query = $this->db->query(sprintf($insert, $table, $keys, $values));
            return $this->db->affected_rows() ? $query->row_array() : FALSE;
        endif;
    }

    /**
     * Inserta una validacion en la tabla cotizacion_validacion, la cual funciona
     * como notificacion
     *
     * @access public
     * @param integer $id Identificador de la cotizacion
     * @param integer $id_validacion identificador de la validacion/ notificacion
     * @param integer $cat_level_current categoria de el nivel de usuario (vendedor, validador, etc. ) nota: no es lo mismo que rol
     * @author Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version 1.0 11/09/13 12:35 PM
     */
    function notificationSend($id, $id_validacion, $cat_level_current, $id_entity = NULL) {
        $arr_data = array('id_validacion' => $id_validacion, 'id_cotizacion' => $id
            , 'cat_val_nivel' => $cat_level_current, 'abierto' => '1', 'modified_by' => $this->session->userdata('user_id'), 'id_entity' => $id_entity);
        $this->db->insert('business.cotizacion_validacion', $arr_data);
    }

    /**
     * Actualiza la notificacion/validacion como cerrada para que no aparezca visible al usuario
     *
     * @access public
     * @param integer $id Identificador de la cotizacion
     * @param integer $id_validacion identificador de la validacion/ notificacion
     * @param integer $cat_level_current categoria de el nivel de usuario (vendedor, validador, etc. ) nota: no es lo mismo que rol
     * @author Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version 1.0 11/09/13 12:35 PM
     */
    function notificationClose($id, $id_validacion, $cat_level_current) {
        $arr_data = array('abierto' => '0', 'modified_by' => $this->session->userdata('user_id'), 'modified_at' => date('Y-m-d H:i:s'));
        $this->db->where('id_validacion', $id_validacion);
        $this->db->where('id_cotizacion', $id);
        $this->db->where('cat_val_nivel', $cat_level_current);
        $this->db->update('business.cotizacion_validacion', $arr_data);
    }

    /**
     * Obtiene el identificador de una entidad dado su nombre
     *
     * @access  public
     * @param   integer  $label
     * @return  string   id identificador de la entidad
     * @author  Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version V 1.0
     */
    public function getEntityId($label) {
        $this->db->select('id');
        $this->db->from('dynamic.entity');
        $this->db->where('_name', $label);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->row()->id) : NULL;
    }

    /**
     * Obtiene el identificador de una entidad dado su nombre
     *
     * @access  public
     * @param   integer  $label
     * @return  string   id identificador de la entidad
     * @author  Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version V 1.0
     */
    public function getEntityData($id) {
        $this->db->select('_name');
        $this->db->select('_schema');
        $this->db->from('dynamic.entity');
        $this->db->where($id);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    /**
     * Dato el identificador del registro contador obtiene el valor actual
     *
     * @access  public
     * @param   integer  $id Identificador
     * @return  string   id identificador de la entidad
     * @author  Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version V 1.0
     */
    public function getCurrentSequentialCounter($id) {
        if (!$id)
            return FALSE;

        $this->db->select('
            		(
			CASE	WHEN current_value IS NULL THEN initial_value
				WHEN initial_value=current_value THEN initial_value
				WHEN current_value>initial_value THEN current_value
				ELSE initial_value

			END

		) AS current_value',FALSE);
        $this->db->from('dynamic.sequential_counter');
        $this->db->where(array('id_entity' => $id));

        $query = $this->db->get();
        return ($query->num_rows() > 0) ? ($query->row()->current_value) : FALSE;
    }

}
