<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of AST_Controller
 *
 * Esta clase provee la implementacion basica para la capa de control de
 * acciones de usuario con la vista, haciendo llamdos a la capa de acceso a
 * datos y llamdos a la capa de interfaz de usuario.
 *
 * @package Core
 * @category Application Core
 * @author Jose A. Rodriguez E. <josearodrigueze@gmail.com>
 * @version V-1.0 20/11/2012 09:42
 */
class AST_Controller extends CI_Controller {

    /**
     * Contiene el nombre del modelo asociado al controlador.
     * @var string
     * @access private
     * @author Jose A. Rodriguez E.
     */
    private $model_name;

    /**
     * Contiene el html generado por el ViewEngine.
     * @var string
     * @access private
     * @author Jose A. Rodriguez E.
     */
    protected $content;

    /**
     * Indica donde se va renderizar la vista.
     * @var string
     * @access private
     * @author Jose A. Rodriguez E.
     */
    private $target_on;

    /**
     * Indica si el set_interval de la notificacion estara activo
     * @var boolean
     * @access private
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     */
    protected $activeNotification = TRUE;

    /**
     * Indica el titulo de la ventana.
     * @var string
     * @access private
     * @author Jose A. Rodriguez E.
     */
    private $title;

    /**
     * Indica si el output(callback de CI) esta o no prendido
     * @var string
     * @access private
     * @author Nohemi Rojas
     */
    private $fire_output;

    /**
     * Indica si la operacion fue exitosa
     * @var bool
     * @access private
     * @author Jose A. Rodriguez E.
     */
    private $result;

    /**
     * Identificador de la Operacion en curso
     *
     * @access protected
     * @var string
     */
    protected $operation_id;

    /**
     * Nombre de la vista a cargar en topbar
     *
     * @access private
     * @var string
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     */
    protected $topBarView = 'topbar';

    /**
     * Indica que vista para el menu se va a renderizar
     *
     * @access private
     * @var string
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     */
    protected $menu;

    /**
     * Constructor
     *
     */
    function __construct($model_name = NULL) {
        parent::__construct();
        $this->model_name = NULL;
        $this->_init($model_name);
        $this->fire_output = TRUE;
    }

    /**
     * Metodo Getter de content
     *
     * @access public
     * @return string HTML generado por el ViewEngine.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 21/11/2012 15:00
     */
    function getContent() {
        return $this->content;
    }

    /**
     * Metodo Getter de toolbar
     *
     * @access public
     * @return string Nombre de la vista a cargar en el toolbar.
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 17/12/13 09:21 AM
     */
    function getTopBar() {
        return $this->topBarView;
    }

    /**
     * Metodo Setter de topbar
     *
     * @access public
     * @param string $view_name Vista a cargar en el topbar.
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 17/12/13 09:21 AM
     */
    function setTopBar($view_name) {
        $this->topBarView = $view_name;
    }

    /**
     * Metodo menu de content
     *
     * @access public
     * @return string Vista donde se renderizara el menu.
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 17/12/13 09:21 AM
     */
    function getMenu() {
        return $this->menu;
    }

    /**
     * Metodo Setter de content
     *
     * @access public
     * @param string $content HTML generado por el ViewEngine.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 21/11/2012 15:00
     */
    function setContent($content) {
        $this->content = $content;
    }

    /**
     * Metodo Setter de content
     *
     * @access public
     * @param string $menu Setea el nombre de la vista donde se renderizara el menu
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 17/12/13 09:21 AM
     */
    function setMenu($menu) {
        $this->menu = $menu;
    }

    /**
     * Metodo getter de target_on por defecto "window"
     *
     * @access public
     * @return string target_on.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 21/11/2012 15:00
     */
    public function getTargetOn() {
        $r = (empty($this->target_on)) ? 'window' : $this->target_on;
        return strtolower($r);
    }

    /**
     * Metodo para setear el valor de notificacion activa
     *
     * @access public
     * @param string $activate Setea el valor de la activacion del notificacador
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 17/12/13 09:21 AM
     */
    function setActiveNotification($activate) {
        $this->activeNotification = $activate;
    }

    /**
     * Metodo getter de target_on por defecto "window"
     *
     * @access public
     * @return boolean activeNotification.
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 17/12/13 09:21 AM
     */
    public function getActiveNotification() {
        return $this->activeNotification;
    }

    /**
     * Metodo Setter de content
     *
     * @access public
     * @param string $target_on indica donde se cargara el componente.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 21/11/2012 15:00
     */
    public function setTargetOn($target_on) {
        $this->target_on = $target_on;
    }

    /**
     * Metodo Setter de title
     *
     * @access public
     * @return string title de la operacion
     * @author Jose A. Rodriguez E.
     * @version V 1.0 21/11/2012 15:00
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Metodo Setter de result
     *
     * @access  public
     * @param   bool    $result resultado de la operacion.
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 28/01/13 15:54
     */
    public function setResult($result) {
        $this->result = $result;
    }

    /**
     * Metodo Setter de result
     *
     * @access  public
     * @return  bool     Resultado de la operacion.
     * @author  Jose A. Rodriguez E.
     * @version V 1.0 28/01/13 15:54
     */
    public function getResult() {
        return $this->result;
    }

    /**
     * Metodo Setter de title
     *
     * @access public
     * @param string $title titulo de la operacion.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 21/11/2012 15:00
     */
    public function setTitle($title) {
        $this->title = $title;
    }

    /**
     * Obtiene el nombre del modulo al cual pertence el controlador ejecutado.
     *
     * @return		string
     * @author		Jose A. Rodriguez E.
     * @version		V 1.0 21/11/12 15:28:00
     */
    protected final function getModule() {
        $reflector = new ReflectionClass(get_class($this));
        return array_pop(explode('/', dirname(dirname($reflector->getFileName()))));
    }

    /**
     * Callback de CI que se ejecuta antes del llamado a cualquier metodo de
     * controlador que herede AST_Controller.
     *
     * @access public
     * @param string $method Nombre del metodo solicitado.
     * @param array $paramas Parametros adicionales enviados al metodo solicitado.
     * @return returnType returnDescripcion
     * @author Jose A. Rodriguez E.
     * @version V 1.0 21/11/2012 15:27:40
     */
    public function _remap($method, $params) {

        //Verifica si el usario tiene sesion
        if ($this->session->userdata('user_id')) {

            $this->load->library('acl');
            $arr_permission = $this->acl->hasPermissions($this->getModule(), get_class($this), $method);

            //Caso de no tener permisos
            if ($arr_permission == FALSE)
                die('No tiene permisos para esta operacion.');

            //Caso de que tener permisos
            elseif (is_array($arr_permission)) {
                $this->load->library('view_engine');
                $this->operation_id = $arr_permission['operation_id'];

                $aux_target_on = $this->input->post('target_on');
                $arr_permission['target_on'] = (empty($aux_target_on)) ? $arr_permission['target_on'] : $aux_target_on;

                $this->view_engine->init($arr_permission['operation_id'], $arr_permission['operation_url'], $arr_permission['operation_title'], $arr_permission['target_on']);
                $this->setTitle($arr_permission['operation_title']);
                $this->setTargetOn($arr_permission['target_on']);
            }

            //Caso de tener permisos, cubre ademas excepciones
            if (method_exists($this, $method))
                return call_user_func(array($this, $method), $params);
            else
                show_404();
        }
        //El usuario no tiene session.
        else {
            //redirect('auth/logout', 'refresh');
            $this->setTargetOn('out');
        }
    }

    /**
     * Callback de CI que se ejecuta al finalizar la ejecucion del metodo
     * solicitado.
     *
     * @access public
     * @author Jose A. Rodriguez E. ,Frederick Bustamante
     * @version V 1.1 21/06/13 10:45 AM
     * @
     */
    function _output() {
        if ($this->fire_output) {//rgomez@iweb.com
            // Esto es para inidcar que la salida son solo los datos en formato JSON
            if ($this->getTargetOn() === 'json') {
                echo json_encode($this->getContent());
            } elseif ($this->input->is_ajax_request()) {

                $view['target_on'] = $this->getTargetOn();
                // $view['view'] = preg_replace("/\\n\s*/", '', $this->getContent());
                $view['view'] = $this->getContent();
                $view['title'] = $this->getTitle();
                $view['result'] = $this->getResult();
                echo json_encode($view);
            } else {
                $data['content'] = $this->getContent();
                $data['menu'] = $this->getMenu();
                $data['activeNotification'] = $this->getActiveNotification();
                $data['topBarView'] = $this->getTopBar();
                //se aplica para determinar si hay session activa y redirigir la pagina que se requiera
                $this->load->config('ion_auth', TRUE);
                $this->load->config('view_engine', TRUE);
                $identity_main = $this->config->item('identity', 'ion_auth');
                if ($this->session->userdata($identity_main)):
                    echo $this->load->view('main', $data, TRUE);
                else:
                    redirect('auth/login', 'refresh');
                endif;
            }
        }

        $this->audit->register();
    }

    /**
     * Inicializa todas las configuraciones para el funcionamiento del
     * controlador generico. Puede recibir un nombre de modelo si se desea
     * utilizar un modelo diferente al prederminado para la clase controlador.
     *
     * @access private
     * @param string $model_name nombre del modelo a ser utilizado
     * @author Jose A. Rodriguez E.
     * @version V 1.0 20/11/2012 15:30
     */
    private function _init($model_name = NULL) {
        $this->model_name = $this->_getModelClassName($model_name);
        $this->load->model($this->model_name);
    }

    /**
     * Retorna el nombre de la clase que esta instanciando esta clase.
     *
     * @access private
     * @return string Nombre de la clase.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 20/11/2012 14:07
     */
    private function _getCurrentClassName() {
        return get_class($this);
    }

    /**
     * Coloca el sufijo "_model" al nombre de la clase actual. Si se pasa un
     * parametro se valida que el mismo posee el sufijo "_model", en caso
     * contrario devuelve un FALSE.
     *
     * @access protected
     * @param string $model_name nombre del modelo a utilizar.
     * @return string Nombre de la clase model.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 20/11/2012 14:07
     */
    protected function _getModelClassName($model_name = NULL) {
        if (empty($model_name))
            return $this->_getCurrentClassName() . '_model';

        $pattern = '/_model$/';
        if (preg_match($pattern, $model_name))
            return $model_name;

        return FALSE;
    }

    /**
     * Genera codigos que seran asociados a los registros(no como identificador)
     * a fin de ser los valores presentados a los usuarios.
     *
     * @access protected
     * @param integer $lenght Numero que indica la longitud del codigo
     * @return string Codigo Generado.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 03/12/12 10:01 AM
     */
    protected function getCode($length = 25) {
        $code = md5(uniqid(rand(), true));
        if ($length != "")
            return substr($code, 0, $length);
        else
            return $code;
    }

    /**
     * Realiza un formateo basico data, limpiando la informacion sensible,
     * segun se este ingresando o actualizando los datos.
     * @todo Borrar el seteo de la sesion al implemtar el logeo.
     *
     * @access private
     * @param array $format_type Tipo de Formateo [INSERT o UPDATE].
     * @param array $data Datos a ser formateados.
     * @return array Data formateada.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 20/11/2012 17:35
     */
    private function _basicFormat($format_type, $data) {

        unset($data['id']);
        unset($data['id_ec']);
        unset($data['_delete']);
        unset($data['deleted_by']);
        unset($data['deleted_at']);

        foreach ($data as &$field) {
            //         $field = trim($field);
            if ($field == '')
                $field = NULL;
        }

        switch ($format_type) {
            case 'INSERT':
                $data['created_by'] = $this->session->userdata('user_id');
                $data['modified_by'] = $this->session->userdata('user_id');
                //$data['modified_at'] = date("Y-m-d H:i:s");
//                $data['codigo'] = isset($data['codigo']) ? $data['codigo'] : $this->getCode();
//                foreach ($data as &$value)
//                    if (is_string($value) and empty($value))
//                        $value = NULL;
                break;

            case 'UPDATE':
                unset($data['created_at']);
                unset($data['created_by']);
                $data['modified_by'] = $this->session->userdata('user_id');
                $data['modified_at'] = date("Y-m-d H:i:s");
            case 'ENABLE':
            case 'DISABLE':
                unset($data['created_at']);
                unset($data['created_by']);

                $current_date = date('Y-m-d H:i:s');
                $data['modified_by'] = $this->session->userdata('user_id');
                $data['modified_at'] = "$current_date";

                if ($format_type == 'ENABLE')
                    $data['active'] = '1';
                elseif ($format_type == 'DISABLE')
                    $data['active'] = '0';

                break;

            case 'DELETE':
                unset($data);
                $current_date = date('Y-m-d H:i:s');
                $data['deleted_by'] = $this->session->userdata('user_id');
                $data['deleted_at'] = "$current_date";
                $data['_delete'] = '1';
                break;

            default:
                break;
        }

        return $data;
    }

    /**
     * Provee la posibilidad de aplicar formateos personalizados en los
     * controladores hijos, por defecto este metodo retorna los datos
     * recibidos sin formateo alguno.
     *
     * @access protected
     * @param array $format_type Tipo de Formateo [INSERT o UPDATE].
     * @param array $data Datos a ser retornados.
     * @return array Los parametros sin procesamiento alguno.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 20/11/2012 17:45
     */
    protected function _avancedFormat($format_type, $data) {
        return $data;
    }

    /**
     * Implementa los metodos _basicFormat() y _avancedFormat() para formatear
     * la data a ser enviada al modelo.
     *
     * @access protected
     * @param array $params arreglo asociativo con las siguientes variables:
     *             string  [format_type]   Tipo de formateo a ser aplicado.
     *             Array  [data]   data a ser formateada.
     * @return array data luego de ser formateada.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 21/11/2012 09:45
     */
    protected function _format($params) {
        extract($params);

        $format_type = strtoupper($format_type);
        if ($this->{$this->model_name}->inheritsSuperTable())
            $data = $this->_basicFormat($format_type, $data);
        $data = $this->_avancedFormat($format_type, $data);
        return $data;
    }

    /**
     * Formatea la presentacion de la data haciendo uso del helper gridFormat. recibe un arreglo asociativo donde clave es campo a formatear y valor la funcion helper a ser empleada.
     *
     * @access protected
     * @param   array $data arreglo asociativo indicando el formato.
     * @param   array $formating arreglo asociativo indicando el formato.
     * @return  array data formateada
     * @author Jose A. Rodriguez E.
     * @version V 1.0 26/11/2012 10:52
     */
    protected function _formatGrid($data, $formating) {
        if (empty($data))
            return FALSE;

        $fields_metadata = $this->{$this->model_name}->getFieldsMetadata();

        //Recorremos el arreglo data
        foreach ($data as &$row_data) {
            $row_data['id'] = $this->encrypt->encode($row_data['id']);

            //unset($row_data['id']);
            unset($row_data['id_ec']);
            unset($row_data['created_by']);
            unset($row_data['created_at']);
            unset($row_data['modified_by']);
            unset($row_data['modified_at']);
            unset($row_data['deleted_by']);
            unset($row_data['deleted_at']);
            if (isset($row_data['cedula']) and isset($row_data['letra']))
                $row_data['cedula'] = $row_data['letra'] . '-' . $row_data['cedula'];
            foreach ($row_data as $field_name => &$value) {
                foreach ($fields_metadata as $key => $obj_field_metada) {
                    if ($obj_field_metada->name == $field_name) {
                        $type = strtolower($obj_field_metada->type);
                        switch ($type) {
                            //Validamos para formatear campos de tipo date
                            case (preg_match('/^date/', $type) ? $type : !$type):
                                $value = date('d-m-Y', strtotime($value));
                                break;

                            case (preg_match('/^timestamp/', $type) ? $type : !$type):
                                $value = date('d-m-Y h:i:s A', strtotime($value));
                                break;

                            case $value:

                                break;

                            default:
                                break;
                        }
                        break;
                    }
                }
            }
        }

        return $data;
    }

    /**
     * Lista todas los datos asociados con esta entidad.
     * Carga el config de paginacion.
     *
     * @access public
     * @author Jose A. Rodriguez E.
     * @version V 1.0 20/11/2012 16:30
     */
    function listAll() {

        $view_params = $this->pagination();

        //Evaluamos si view paran tiene datos, si los tiene lazamos la vista
        // caso contrario mandamos a imprimer los datos
        if ($view_params)
            $this->setContent($this->view_engine->grid($view_params));
    }

    /**
     * Lista todas los datos asociados con esta entidad, ademas realiza la paginacion de datos.
     * Carga el config de paginacion.
     *
     * @access public
     * @author Jose A. Rodriguez E.
     * @version V 1.0 20/11/2012 16:30
     */
    protected function pagination() {
        //Cargamos el archivo de configuracion pagination.
        //Lo hacemos aqui para NO cargar archivos que no seran necesarios
        //en otros operaciones
        $this->config->load('pagination');

        //Indica la cantidad de registros a ser consultados.
        if ($this->input->post('iDisplayLength'))
            $this->config->set_item('limit', $this->input->post('iDisplayLength'));

        //Inicializamos estas variables con array vario
        $search_fields = $sort = array();

        $total_columns = $this->input->post('iColumns');

        //Indica el numero de peticiones enviadas al servidor
        $request_number = $this->input->post('sEcho');

        for ($i = 0; $i < $total_columns; $i++) {
            //Obtenemos el search y los campos del search
            if ($search = $this->input->post('sSearch')) {

                $field = $this->input->post('mDataProp_' . $i);
                //Filtramos los campos.
                if (!preg_match('/^id$/', $field) AND !preg_match('/^fecha_*/', $field))
                    $search_fields[$field] = $search;
            }

            if (($request_number > 1) AND ($this->input->post('iSortCol_' . $i)))
                $sort[$this->input->post('mDataProp_' . $this->input->post('iSortCol_' . $i))] = $this->input->post('sSortDir_' . $i);
        }

        //Obtenemos la data y el total de resultados
        $param_data = array(
            'offset' => $this->input->post('iDisplayStart'),
            'search_fields' => $search_fields,
            'sort' => $sort
        );
        $results = $this->getPaginationData($param_data);
        $data = $results['data'];
        $count = $results['count'];

        // Registramos los datos a ser guardados ne la uditoria
        // Se registran los datos aqui porque mas abajo hay diferentes returns
        $operation_data = NULL;
        if (!empty($sort))
            $operation_data['sort'] = $sort;
        if (!empty($search_fields))
            $operation_data['search_fields'] = $search_fields;
        $this->audit->setOperationData($operation_data);

        //Formateamos la presentacion de la data.
        $formating = array('date' => 'funcion');
        $data = $this->_formatGrid($data, $formating);

        if (empty($data))
            $data = array();

        $post_request = $this->input->post();
        if (!empty($post_request) and isset($post_request['iDisplayStart'])) {
            $output = array(
                "sEcho" => $request_number,
                "iTotalRecords" => $count,
                "iTotalDisplayRecords" => $count,
                "aaData" => $data
            );

            $this->setTargetOn('json');
            $this->setContent($output);
            return FALSE;
        }

        return array('data' => $data, 'count' => $count);
    }

    /**
     * Provee el acceso al formulario de ingreso de nuevos datos, asi mismo,
     * provee el acceso a la insercion de datos nuevos dentro en base de datos.
     *
     * @access public
     * @param array $params Parametros adicionales enviados al controlador.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 20/11/2012 17:00
     */
    function create($params = NULL) {
        if (empty($params)) {
            $this->setContent($this->view_engine->form());
        } elseif ($params[0] == 'process') {
            $process = $this->view_engine->process();
            $success = FALSE;
            if ($process['result']) {
                $params = array('format_type' => 'insert', 'data' => $process['data']);

                $data = $this->_format($params);
                $success = $this->{$this->model_name}->insert($data);

                $this->setContent($success);
            } else {
                $this->setContent($process['validation_error']);
            }
            // -----------------------------
            //seteamos los datos de auditoria
            $this->audit->setOperationData($process);
            $this->audit->setChkResult($success);
            // -----------------------------
        }
    }

    /**
     * Implementacion basica para la edicion de registros, por un lado llama a
     * a la vista de edicion de registros y por otro lado guarda el registro en
     * base de datos.
     *
     * @access public
     * @param array $params Parametros adicionales enviados al controlador.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 20/11/2012 17:00
     */
    function edit($params = NULL) {
        $array_post = $this->input->post();
        $id = NULL;
        if (empty($params)) {
            $id = $this->encrypt->decode($array_post['id']);
            $this->session->set_userdata("{$this->model_name}.id_ec", $id);
            $data = $this->{$this->model_name}->getById($id);

            $this->setContent($this->view_engine->form(array('data' => $data)));
        } elseif ($params[0] == 'process') {

            $process = $this->view_engine->process();
            $success = FALSE;
            $id = $this->session->userdata("{$this->model_name}.id_ec");
            if ($process['result']) {
                $params = array(
                    'format_type' => 'update',
                    'data' => $process['data']
                );

                $data = $this->_format($params);
                $success = $this->{$this->model_name}->update($id, $data);

                if ($success)
                    $this->session->unset_userdata("{$this->model_name}.id_ec");

                $this->setContent($success);
            } else {
                $this->setContent($process['validation_error']);
            }

            // -----------------------------
            //seteamos los datos de auditoria
            $operation_data = $process;
            $this->audit->setChkResult($success);
            // -----------------------------
        }

        // -----------------------------
        //seteamos los datos de auditoria
        $operation_data['id'] = $id;
        $this->audit->setOperationData($operation_data);
        // -----------------------------
    }

    /**
     * Implementacion basica para la edicion de registros, por un lado llama a
     * a la vista de edicion de registros y por otro lado guarda el registro en
     * base de datos.
     *
     * @access public
     * @param array $params Parametros adicionales enviados al controlador.
     * @author Jose A. Rodriguez E.
     * @version V 1.0 20/11/2012 17:00
     */
    function delete() {

        if ($this->input->is_ajax_request() && $this->input->post()) {

            $content = 'La pagina solicitada';
            $title = 'Error!!!';

            // Verificamos si el borrado es masivo o de uno solo
            $id_request = $this->input->post('id');
            if (is_array($id_request))
                foreach ($id_request as &$id_ec) {
                    $id_ec = $this->encrypt->decode($id_ec);
                } else
                $id_request = $this->encrypt->decode($id_request);


            //Arreglo de parametros a ser pasados al format.
            $params = array(
                'format_type' => 'delete',
                'data' => array()
            );
            $data = $this->_format($params);
            $success = $this->{$this->model_name}->update($id_request, $data);

            if ($success) {
                $content = '<div class="modal-body">Registro eliminado saisfactoriamente.</div>';
                $title = 'Exito';
            } else
                $content = '<div class="modal-body">No se pudo eliminar el registro. Por favor intente de nuevo.</div>';

            $this->setTitle($title);
            $this->setContent($content);

            // -----------------------------
            //seteamos los datos de auditoria
            $this->audit->setOperationData(array('id' => $id_request));
            $this->audit->setChkResult($success);
            // -----------------------------
        }else {
            show_404();
        }
    }

    /**
     * Muestra el detalle un registro particular.
     *
     * @access public
     * @author Jose A. Rodriguez E.
     * @version V 1.0 21/11/2012 13:40
     */
    function detail() {
        $array_post = $this->input->post();
        $id = $this->encrypt->decode($array_post['id']);

        if (!empty($id)) {
            $data = $this->{$this->model_name}->getById($id);

            $this->setContent($this->view_engine->form(array('data' => $data)));
        }
        //seteamos los datos de auditoria
        $this->audit->setOperationData(array('id' => $id));
    }

    /**
     * Provee la implementacion basica para activar registros de BD.
     *
     * @access public
     * @author Jose A. Rodriguez E.
     * @version V 1.0 21/11/2012 14:00
     */
    function enable($params = NULL) {
        /**
         * Ojo con activado y desactivado en masa.
         *
         *
         */
        if ($this->input->is_ajax_request() && $this->input->post()) {

            $content = 'La pagina solicitada';
            $title = 'Error!!!';

            $id = $this->encode->decode($this->input->post('id'));

            //Arreglo de parametros a ser pasados al format.
            $params = array('format_type' => 'enable', 'data' => array());
            $data = $this->_format($params);
            $success = $this->{$this->model_name}->update($id, $data);

            if ($success) {
                $content = '<div class="modal-body">Registro activado satisfactoriamente.</div>';
                $title = 'Exito';
            } else
                $content = '<div class="modal-body">No se pudo eliminar el registro. Por favor intente de nuevo.</div>';

            $this->setTitle($title);
            $this->setContent($content);

            // -----------------------------
            //seteamos los datos de auditoria
            $this->audit->setOperationData(array('id' => $id));
            $this->audit->setChkResult($success);
            // -----------------------------
        }else {
            show_404();
        }
    }

    /**
     * Apaga el callback del output de CI, de manera que permite imprimir
     * una vista particular
     *
     * @access public
     * @author Nohemi Rojas
     * @version V 1.0 30/11/2012 14:00
     */
    function offOutput() {
        $this->fire_output = FALSE;
    }

    /**
     * Funcion callback de validacion, verifica que un campo sea unico en la edicion del mismo
     * Para la creacion se debe usar is_unique
     *
     * @param string $str valor del campo envia por post
     * @param string $field parametros del campo
     * @return boolean TRUE/FALSE si el campo es unique o no
     *
     * @access public
     * @author Nohemi Rojas
     * @version V 1.0 15/01/13 03:49 PM
     */
    function unique($str, $field) {

        $id = $this->session->userdata("{$this->model_name}.id_ec");
        $result = $this->{$this->model_name}->is_unique($field, $str, 'id', $id);

        if ($result == FALSE) {
            $this->form_validation->set_message('unique', 'El campo %s ya se encuentra registrado en el sistema');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Obtiene los datos a se paginados y mostrados en el grid. La idea de esta funcion
     * es para cuando deseamos paginar datos que no son asociados directamente con el
     * modelo del controlador. Es necesario que retone siempre un array con las posiciones
     * data y count
     *
     * @access  public
     * @param   string  array[offset]
     * @param   string  array[search_fields]
     * @param   string  array[sort]
     * @return  array   con las posciones [data] y [count]
     * @author  Jose A. Rodriguez E. <josearodrigueze@gmail.com>
     * @version V 1.0 02/05/2013 03:52:52 pm
     */
    public function getPaginationData($params) {
        extract($params);
        $data['data'] = $this->{$this->model_name}->getAll($offset, $search_fields, $sort);
        $data['count'] = $this->{$this->model_name}->getCount($search_fields);
        return $data;
    }

    function get_post_id() {
        return $this->encrypt->decode($this->input->post('id'));
    }

}
