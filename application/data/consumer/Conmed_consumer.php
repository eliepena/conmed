<?php 
set_time_limit(0);
ini_set('mssql.charset', 'ISO-8859-1');

/**
 * 
 * @author cbello
 *
 */
class Conmed_consumer
{
    protected $sqlsrv_server = "";
    protected $sqlsrv_db = "";
    protected $hour_hard_init = 8;
    protected $hour_hard_end = 23;
	protected $hour_end_process = 3;
    protected $rest_url = "";
    protected $rest_user = "";
    protected $rest_password = "";
	protected $seconds_sleep_hard = "10";
    protected $seconds_sleep_soft = "120";
	protected $vendedores_encontrados = array();
	protected $clientes_encontrados = array();
	protected $medicos_encontrados = array();
	protected $materiales_encontrados = array();
	
	protected $vendedores_bd= array();
	
	protected $cotizacion_days;
	
	protected $smtp_host = "smtp.gmail.com";
    protected $smtp_auth = TRUE;
    protected $smtp_port = 465;
	protected $smtp_secure = "ssl";
    protected $smtp_username = "desarrollo.ast@gmail.com";
    protected $smtp_password = "3m4i1ast";
	protected $conn = FALSE;
	protected $bd = "";
	
	/*
	 * CONSTANTES PARA EL MANEJO DE ERRORES
	 */
	CONST ERROR_FAIL_MEDICO = 1001;
	CONST ERROR_FAIL_VENDEDOR = 1002;
	CONST ERROR_FAIL_PRODUCTO = 1003;
	CONST ERROR_FAIL_COTIZACION	 = 1004;
	CONST ERROR_FAIL_CLIENTE = 1005;
	
	CONST ISLR_JURIDICA = 3;
	CONST ISLR_NATURAL	 = 1;
	
	CONST SEGMENTO_MEDICOS = "MED";
	CONST SEGMENTO_SEGUROS	 = "SEG";
	CONST SEGMENTO_PACIENTES = "PAC";
	CONST SEGMENTO_OTROS_SERVICIOS	= "OS";
	
   	/**
   	 * funcion que inicializa los parametros para el funcionamiento del consumidor
   	 */
    protected function __init_parameter()
    {
        $this->sqlsrv_server = "DCFS02";
        $this->sqlsrv_db = "AMBPRU";
        $this->rest_user = 'admin';
        $this->rest_password = '1234';   
        $this->rest_url = 'http://movil.corporacionconmed.com/app/rest/api/';
        $this->cotizacion_days = 20;
        $this->bd = 'AMBPRU';
    }
    
    /**
     * agrega en la tabla pista del sistema profit el registro
     * de creacion de cotizacion
     * 
     * @param unknown $id_cotizacion
     * @throws Exception
     * @return boolean
     */
    protected function __addPista($id_cotizacion)
    {
    	$fc = date("Y-m-d H:i:s.000");
    
    	$sql = "
			INSERT INTO [". $this->bd ."].[dbo].[pistas] (
    		  	[usuario_id]
			    ,[usuario]
			    ,[fecha]
			    ,[empresa]
			    ,[co_sucu]
			    ,[tabla]
			    ,[num_doc]
			    ,[codigo]
			    ,[tipo_op]
			    ,[maquina]
			    ,[campos]
			    ,[trasnfe]
			    ,[AUX01]
			    ,[AUX02]
    		)
    		values (
    		'AST',
    		'AST',
    		'$fc',
    		'AMBPRU',
    		'CCS',  	
    		'COTIZ_C',
    		'$id_cotizacion',
    		'',
    		'I',
    		'DCFS02 # AST',
    		'',
    		'',
    		'0.00000',
    		''
    		);
    	";
       
    	if( mssql_query($sql, $this->conn ) === FALSE ) {
    		throw new Exception("Error - __addPista Error: ".mssql_get_last_message(), self::ERROR_FAIL_COTIZACION);
    	}
    	else
    	{
    		return TRUE;
    	}
    }
    
    /**
     * agrega un cliente con los datos suministrados.
     * 
     * @param unknown $nombre
     * @param unknown $cedula
     * @param unknown $direccion
     * @param unknown $localidad
     * @param unknown $telefonos
     * @param unknown $islr
     * @param unknown $segmento
     * @param unknown $id_vendedor
     * @param unknown $correo_electronico
     * @throws Exception
     * @return Ambigous <number, boolean>
     */
    protected function __createCliente($nombre, $cedula, $direccion, $localidad, $telefonos, $islr, $segmento, $id_vendedor, $correo_electronico)
    {
    	$id_cliente = $this->__generateIdCliente();
    	if($id_cliente == FALSE)
    	{
    		throw new Exception("Error __createCliente a __generateIdCliente - no se genero id.", self::ERROR_FAIL_CLIENTE);
    	}
    
    	$fc = date("Y-m-d H:i:s");
    	
    	//OJO -
    	$zona = "VAR";
    
    	$sql = "
		INSERT INTO [". $this->bd ."].[dbo].[clientes] (
    		  [co_cli]
		      ,[tipo]
		      ,[cli_des]
		      ,[direc1]
		      ,[direc2]
		      ,[telefonos]
		      ,[fax]
		      ,[inactivo]
		      ,[comentario]
		      ,[respons]
		      ,[fecha_reg]
		      ,[puntaje]
		      ,[saldo]
		      ,[saldo_ini]
		      ,[fac_ult_ve]
		      ,[fec_ult_ve]
		      ,[net_ult_ve]
		      ,[mont_cre]
		      ,[plaz_pag]
		      ,[desc_ppago]
		      ,[co_zon]
		      ,[co_seg]
		      ,[co_ven]
		      ,[desc_glob]
		      ,[horar_caja]
		      ,[frecu_vist]
		      ,[lunes]
		      ,[martes]
		      ,[miercoles]
		      ,[jueves]
		      ,[viernes]
		      ,[sabado]
		      ,[domingo]
		      ,[dir_ent2]
		      ,[tipo_iva]
		      ,[iva]
		      ,[rif]
		      ,[contribu]
		      ,[dis_cen]
		      ,[nit]
		      ,[email]
		      ,[co_ingr]
		      ,[fe_us_in]
		      ,[fe_us_mo]
		      ,[fe_us_el]
		      ,[revisado]
		      ,[trasnfe]
		      ,[co_sucu]
		      ,[juridico]
		      ,[tipo_adi]
		      ,[matriz]
		      
		      ,[co_tab]
		      ,[tipo_per]
		      ,[serialp]
		      ,[valido]
		      ,[estado]
		      ,[Id]
		      ,[co_pais]
		      ,[ciudad]
		      ,[zip]
		      ,[contribu_e]
		      ,[porc_esp]
		      ,[sincredito]
    		)
    		values (
    		$id_cliente,
    		'03-C',
    		'$nombre',
    		'$direccion',
    		'',
    		'$telefonos',
    		'',
    		0,
    		'',
    		'',
    		'$fc',
    		0,
    		0,
    		0,
    		0,
    		'$fc',
    		0.00,
    		0.00,
    		0,
    		0.00,
    		'$zona',
    		'$segmento',
    		$id_vendedor,
    		0.00,
    		'',
    		'',
    		0,
    		0,
    		0,
    		0,
    		0,
    		0,
    		0,
    		'',
    		'',
    		0.00,
    		'$cedula',
    		1,
    		'',
    		'',
    		'$correo_electronico',
    		'I-01',
    		'$fc',
    		'$fc',
    		'$fc',
    		'',
    		'',
    		'CCS',
    		0,
    		1,
    		'',
    		$islr,
    		$islr,
    		'',
    		0,
    		'A',
    		-1,
    		'VE',
    		'$localidad',
    		'',
    		0,
    		0,
    		0
    		);
    
    		";
       
    	if( mssql_query($sql, $this->conn ) === FALSE ) {
    		throw new Exception("Error - __createCliente Error: ".mssql_get_last_message(), self::ERROR_FAIL_CLIENTE);
    	}
    	else
    	{
    		return $id_cliente;
    	}
    }
	
	/**
	 * crea un vendedor 
	 * 
	 * @deprecated no se esta utilizando
	 * @param unknown $nombre
	 * @param unknown $cedula
	 * @param unknown $direccion
	 * @param unknown $telefonos
	 * @param unknown $fecha_creacion
	 * @param unknown $fecha_ingreso
	 * @param unknown $fecha_modificacion
	 * @param unknown $correo_electronico
	 * @throws Exception
	 * @return Ambigous <number, boolean>
	 */
	protected function __createVendedor($nombre, $cedula, $direccion, $telefonos, $fecha_creacion, $fecha_ingreso, $fecha_modificacion, $correo_electronico)
	{
		$id_vendedor = $this->__generateIdVendedor();
		if($id_vendedor == FALSE)
		{
			throw new Exception("Error vendedor cedula: $cedula - no se genero id.");
		}
		
		$fc = date("Y-m-d H:i:s",strtotime($fecha_creacion));
		$fi = date("Y-m-d H:i:s",strtotime($fecha_ingreso));
		$fm = date("Y-m-d H:i:s",strtotime($fecha_modificacion));
		
		$sql = "
		INSERT INTO [". $this->bd ."].[dbo].[vendedor] ( 
			[co_ven]
			,[tipo]
			,[ven_des]
			,[cedula]
			,[direc1]
			,[telefonos]
			,[fecha_reg]
			,[condic]
			,[comision]
			,[fun_cob]
			,[fun_ven]
			,[comisionv]
			,[fac_ult_ve]
			,[net_ult_ve]
			,[fe_us_in]
			,[fe_us_mo]
			,[fe_us_el]
			,[fec_ult_ve]
			,[email]
			
       	)
		values (
			$id_vendedor,
			'A',
			'$nombre',
			'$cedula',
			'$direccion',
			'$telefonos',
			'$fc',
			1,
			0.00,
			1,
			1,
			0.00,
			0,
			0.00,
			'$fi',
			'$fm',
			'$fm',
			'$fc',
			'$correo_electronico'
		);
        		
        ";
		
		
        	
		if( mssql_query($sql, $this->conn ) === FALSE ) {
			throw new Exception("Error - __createVendedor Error: ", self::ERROR_FAIL_VENDEDOR);
		}
		else{
			return $id_vendedor;
		}
	}
	
	
	
	
	/**
	 * Busca el id del vendedor por el parametro cedula
	 * 
	 * @param unknown $cedula
	 * @throws Exception
	 * @return unknown|boolean
	 */
	protected function __getIdVendedorByCedula($cedula)
	{
		//Se limpia la cedula de los caracteres 
		$sin_caracteres = array("V", "v", "-", "E", "e", ".");
		$cedula_limpia = trim(str_replace($sin_caracteres, "", $cedula));
		
		//Verifica si la cedula no se ha buscado en la BD anteriormente 
		if(array_key_exists($cedula, $this->vendedores_encontrados))
		{
			return $this->vendedores_encontrados[$cedula];
		}
		
		/*
		 * verifico los 4 tipos de cedulas que pueden llegar
		 * 18130008 18.130.008 v-18130008 v-18.130.008
		 */
		$vendedor_cedula = $cedula;
		$vendedor_cedula2 = number_format ( $vendedor_cedula, 0 ,  ',' ,'.' );
		$sql = "SELECT co_ven, cedula FROM [". $this->bd ."].[dbo].[vendedor] 
				WHERE (cedula = 'V-$vendedor_cedula2' OR cedula = 'V-$vendedor_cedula' OR cedula = '$vendedor_cedula2' OR cedula = '$vendedor_cedula');";
	    
		$sql = "SELECT co_ven, cedula FROM [". $this->bd ."].[dbo].[vendedor]";
		
		//Ejecuto el $sql
		$query_result = mssql_query($sql, $this->conn);
		if($query_result === FALSE){
			throw new Exception("Error vendedor cedula: $cedula Error sql: ", self::ERROR_FAIL_VENDEDOR);
		}
		while($row = mssql_fetch_array($query_result, MSSQL_BOTH) )
		{
			$cedula_bd = $row['cedula'];
			$cedula_bd = trim(str_replace($sin_caracteres, "", $cedula_bd));
			
			if($cedula_limpia == $cedula_bd)
			{
				//se agrega a vendedores encontrados para no volver a ejecutar el query
				$this->vendedores_encontrados[$cedula] = trim($row['co_ven']);
				return trim($row['co_ven']);
			}
		}
		//si no se encuentra ningun vendedor se guarda como FALSE
		$this->vendedores_encontrados[$cedula] = FALSE;
		
		return FALSE;
	}
	
	/**
	 * devuelve el id del cliente por la cedula del mismo enviada como parametro
	 * 
	 * @param unknown $cedula
	 * @throws Exception
	 * @return multitype:|string|boolean
	 */
	protected function __getIdClienteByCedula($cedula)
	{
		//Verifica si la cedula no se ha buscado en la BD anteriormente
		if(array_key_exists($cedula, $this->clientes_encontrados))
		{
			return $this->clientes_encontrados[$cedula];
		}
	
		/*
		 * verifico los 4 tipos de cedulas que pueden llegar
		* 18130008 18.130.008 v-18130008 v-18.130.008
		*/
		$cliente_cedula = $cedula;
		$cliente_cedula2 = number_format ( $cliente_cedula, 0 ,  ',' ,'.' );
		$sql = "SELECT co_cli, rif FROM [". $this->bd ."].[dbo].[clientes]
		WHERE (rif = 'V-$cliente_cedula2' OR rif = 'V-$cliente_cedula' OR rif = '$cliente_cedula2' OR rif = '$cliente_cedula');";
	
		//Ejecuto el $sql
		$query_result = mssql_query($sql, $this->conn);
		if($query_result === FALSE)
		{
			throw new Exception("Error cliente rif-cedula: $cedula Error sql: ", self::ERROR_FAIL_CLIENTE);
		}
		while($row = mssql_fetch_array($query_result, MSSQL_BOTH) )
		{
			//se agrega a vendedores encontrados para no volver a ejecutar el query
			$this->clientes_encontrados[$cedula] = trim($row['co_cli']);
			return trim($row['co_cli']);
		}
		//si no se encuentra ningun vendedor se guarda como FALSE
		$this->clientes_encontrados[$cedula] = FALSE;
	
		return FALSE;
	}
	
	function latin1($txt) 
	{
		$encoding = mb_detect_encoding($txt, 'ASCII,UTF-8,ISO-8859-1');
		if ($encoding == "UTF-8") {
			$txt = utf8_decode($txt);
		}
		return $txt;
	}
	
	/**
	 * busca un doctor por el nombre y retorna su id
	 * 
	 * @param unknown $nombre
	 * @throws Exception
	 * @return unknown|boolean
	 */
	protected function __getIdMedicoByNombre($nombre)
	{
		$nombre = $this->latin1(trim($nombre),'utf8');
		//Verifica si el nombre del medico no se ha buscado en la BD anteriormente 
		if(array_key_exists($nombre, $this->medicos_encontrados))
		{
			return $this->medicos_encontrados["$nombre"];
		}
	
		$sql = "SELECT co_tran FROM [". $this->bd ."].[dbo].[transpor] WHERE des_tran LIKE '%$nombre%';";
		
		
		$query_result = $this->ast_db_query($sql);
		if($query_result === FALSE){
			throw new Exception("Error medico: $nombre Error sql: ", self::ERROR_FAIL_MEDICO);
		}
		
		while(($row = mssql_fetch_array($query_result, MSSQL_BOTH) ) )
		{
			//se agrega a medicos encontrados para no volver a ejecutar el query
			$this->medicos_encontrados["$nombre"] = trim($row['co_tran']);
			return trim($row['co_tran']);
		}
		//si no se encuentra ningun medico se guarda como FALSE
		$this->medicos_encontrados["$nombre"] = FALSE;
		return FALSE;
	}
	
	/**
	 * 
	 * busca el id de un articulo por el modelo y lo retorna
	 * 
	 * @param unknown $modelo
	 * @throws Exception
	 * @return unknown|boolean
	 */
	protected function __getIdArticuloByModelo($modelo)
	{
		$modelo = trim($modelo);
		//Verifica si el modelo del material no se ha buscado en la BD anteriormente 
		if(array_key_exists($modelo, $this->materiales_encontrados))
		{
			return $this->materiales_encontrados[$modelo];
		}
		
        $sql = "SELECT co_art FROM [". $this->bd ."].[dbo].[art]  WHERE modelo = '$modelo'; ";
		$query_result = mssql_query($sql, $this->conn );
		if($query_result === FALSE){
			throw new Exception("Error articulo: $modelo Error sql: ", self::ERROR_FAIL_PRODUCTO);
		}
		while($row = mssql_fetch_array($query_result, MSSQL_BOTH) )
		{
			//se agrega los materiales encontrados para no volver a ejecutar el query
			$this->materiales_encontrados[$modelo] = trim($row['co_art']);
			return trim($row['co_art']);
		}
		//si no se encuentra ningun material se guarda como FALSE
		$this->materiales_encontrados[$modelo] = FALSE;

		return FALSE;
	}
	
	/**
	 * genera un id de cotizacion basado en el ultimo registrado
	 * 
	 * @throws Exception
	 * @return number|boolean
	 */
	protected function __generateIdCotizacion()
	{
		$sql = "SELECT fact_num FROM [". $this->bd ."].[dbo].[cotiz_c] ORDER BY fact_num DESC; ";
		
		$query_result = mssql_query($sql, $this->conn);
		if($query_result === FALSE){
			throw new Exception("Error __generateIdCotizacion: Error sql: ", self::ERROR_FAIL_COTIZACION);
		}
		while($row = mssql_fetch_array($query_result, MSSQL_BOTH) )
		{
			return ($row['fact_num'] + 1);
		}
		mssql_free_result($query_result);
		return FALSE;
	}
	
	/**
	 * 
	 * genera un id cliente basado en el ultimo insertado
	 * 
	 * @throws Exception
	 * @return number|boolean
	 */
	protected function __generateIdCliente()
	{
		//Se busca el ultimo id numerico
		$sql = "SELECT co_cli FROM [". $this->bd ."].[dbo].[clientes] WHERE ISNUMERIC(co_cli) = 1 ORDER BY co_cli DESC; ";
	
		$query_result = mssql_query($sql, $this->conn);
		if($query_result === FALSE){
			throw new Exception("Error __generateIdCliente: Error sql: ", self::ERROR_FAIL_CLIENTE);
		}
		while($row = mssql_fetch_array($query_result, MSSQL_BOTH) )
		{
			return ($row['co_cli'] + 1);
		}
		return FALSE;
	}
	
	/**
	 * genera un id de vendedor basado en el ultimo registrado en la tabla.
	 * 
	 * @deprecated no se esta utilizando
	 * @throws Exception
	 * @return number|boolean
	 */
	protected function __generateIdVendedor()
	{
		
		$sql = "SELECT co_ven FROM [". $this->bd ."].[dbo].[vendedor] ORDER BY co_ven DESC; ";
				
		$query_result = mssql_query($sql, $this->conn);
		if($query_result === FALSE){
			throw new Exception("Error __generateIdVendedor: Error sql: ", self::ERROR_FAIL_VENDEDOR);
		}
		while($row = mssql_fetch_array($query_result, MSSQL_BOTH) )
		{
			return ($row['co_ven'] + 1);
		}
		return FALSE;
	}
	
	
	/**
	 * funcion que envia el mensaje de error
	 * 
	 * @param unknown $msg
	 */
	protected function __sendEmailError($msg)
	{
		$mail = new PHPMailer();
		$mail->IsSMTP(); // telling the class to use SMTP
		//$mail->SMTPDebug  = 2; 
		$mail->SMTPAuth      = $this->smtp_auth;                  // enable SMTP authentication                // SMTP connection will not close after each email sent
		$mail->Host          = $this->smtp_host; // sets the SMTP server
		$mail->Port          = $this->smtp_port;                    // set the SMTP port for the GMAIL server
		$mail->Username      = $this->smtp_username; // SMTP account username
		$mail->Password      = $this->smtp_password;        // SMTP account password
		$mail->SMTPSecure = $this->smtp_secure;
		$mail->SetFrom( $this->smtp_username, "AdministradorAST");		

		$mail->Subject       = "Error";


		$mail->AltBody    = "test!"; // optional, comment out and test
		$mail->MsgHTML($msg);
		$mail->AddAddress("cbello@ast.com.ve", "programador");


		if(!$mail->Send()) {
			echo "Mailer Error (" . str_replace("@", "&#64;", 'cbello@ast.com.ve') . ') ' . $mail->ErrorInfo . '<br />';
		} else {
			echo "Message sent to :<br />";
		}
		//$mail->ClearAddresses();
		//$mail->ClearAttachments();
	}
	
	/**
	 * revisa el status del rest 
	 * 
	 * @throws Exception
	 * @return Ambigous <number, mixed>
	 */
	protected function __verificarServicio()
    {
        $is_stop = 0;
       
        $obj_curl = curl_init($this->rest_url . 'conmed/is_stop');
        curl_setopt($obj_curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($obj_curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($obj_curl, CURLOPT_USERPWD, "" . $this->rest_user . ":" . $this->rest_password . "");
        $response = curl_exec($obj_curl);
        $resultStatus = curl_getinfo($obj_curl);
       
        if($resultStatus['http_code'] == 200)
        {
            $is_stop = $response;
        }
        else
        {
            throw new Exception("Error - __verificarServicio: ". var_export($resultStatus, TRUE));
        }
        return $is_stop;
	}

	/**
	 * consume el rest de conmed y devulve las cotizaciones
	 * 
	 * @throws Exception
	 * @return Ambigous <multitype:, mixed>
	 */
    protected function __getCotizaciones()
    {
        $cotizaciones = array();
       
        $obj_curl = curl_init($this->rest_url . 'conmed/cotizaciones');
        curl_setopt($obj_curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($obj_curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($obj_curl, CURLOPT_USERPWD, "" . $this->rest_user . ":" . $this->rest_password . "");
        $response = curl_exec($obj_curl);
        $resultStatus = curl_getinfo($obj_curl);
       
        if($resultStatus['http_code'] == 200)
        {
			$cotizaciones = json_decode($response);
        }
        else
        {
            throw new Exception("Error - __getCotizaciones: ". var_export($resultStatus, TRUE));
        }
        curl_close($obj_curl);
        return $cotizaciones;
       
    }   
   
    /**
     * envia las cotizaciones que fueron insertadas en la bd con sus id's nuevos
     * 
     * @param unknown $cotizaciones
     * @throws Exception
     */
    protected function __sendCotizaciones($cotizaciones = array())
    {   
        $obj_curl = curl_init($this->rest_url . 'conmed/cotizaciones_agregadas');
        curl_setopt($obj_curl, CURLOPT_POST, 1);
        curl_setopt($obj_curl, CURLOPT_POSTFIELDS, "cotizaciones=".json_encode($cotizaciones));
        curl_setopt($obj_curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($obj_curl, CURLOPT_USERPWD, $this->rest_user . ":" . $this->rest_password);
        $response = curl_exec($obj_curl);
       
        $resultStatus = curl_getinfo($obj_curl);
        if($resultStatus['http_code'] == 200)
        {
        }
        else
        {
            throw new Exception("Error - __sendCotizaciones: ". var_export($resultStatus, TRUE));
        }
        curl_close($obj_curl);
    }
   
    /**
     * inserta las cotizaciones recibidas al consumir el servicio rest
     * 
     * @param unknown $cotizaciones
     * @throws Exception
     * @return multitype:multitype:multitype:Ambigous <number, boolean> NULL
     */
    protected function __insertCotizacionesInSqlServer($cotizaciones = array())
    {
        $this->conn = mssql_connect($this->sqlsrv_server);
        
        if( $this->conn === FALSE) {
            throw new Exception("Error connect server -  __insertCotizacionesInSqlServer: " . $this->sqlsrv_server . " db: " . $this->bd );
        }
        mssql_select_db($this->bd, $this->conn);

        $id = 0;
		
		$s_cotizaciones = array();
		$e_cotizaciones = array();
        foreach( $cotizaciones as $i => $cotizacion) 
        {
			
			try
			{
				//Inicia la transacion de la cotizacion
				if ( mssql_query("BEGIN TRAN", $this->conn ) === FALSE ) {
					throw new Exception("Fallo el inicio de la transaccion - Error sql: ");
				}
				
				if(($id_vendedor = $this->__getIdVendedorByCedula($cotizacion->vendedor_cedula)) === FALSE)
				{
					throw new Exception("Fallo cotizacion $id ".$cotizacion->id_cotizacion." , vendedor '".$cotizacion->vendedor_cedula."' no existe", self::ERROR_FAIL_VENDEDOR);			
				}
			
				/*
				 * Lleno los datos en el profit dependiendo de el pagador
				 * $islr I.S.L.R Juridico/Natural
				 * 
				*/
				switch($cotizacion->cat_pagador)
				{
					case 41 :  //PACIENTE
						$segmento = self::SEGMENTO_PACIENTES;
						$islr = self::ISLR_NATURAL;
						$cedula = $cotizacion->paciente_cedula;
						$nombre = $cotizacion->paciente_primer_nombre." ".$cotizacion->paciente_primer_apellido;
						$direccion = is_null( $cotizacion->paciente_direccion) ? "" : $cotizacion->paciente_direccion;
						$telefono = is_null( $cotizacion->paciente_telefono) ? "" : $cotizacion->paciente_telefono_opcional;
						$telefono_opcional = is_null( $cotizacion->paciente_telefono_opcional) ? "" : $cotizacion->paciente_telefono_opcional;
						$localidad = is_null( $cotizacion->paciente_localidad) ? "" : $cotizacion->paciente_localidad;
						break;
					case 42 : //TERCERO
						if(is_null($cotizacion->id_empresa))
						{
							$segmento = self::SEGMENTO_OTROS_SERVICIOS;
							$islr = self::ISLR_NATURAL;
							$cedula = $cotizacion->persona_cedula;
							$nombre = $cotizacion->persona_primer_nombre." ".$cotizacion->persona_primer_apellido;
							$direccion = is_null( $cotizacion->persona_direccion) ? "" : $cotizacion->persona_direccion;
							$telefono = is_null( $cotizacion->persona_telefono) ? "" : $cotizacion->persona_telefono_opcional;
							$telefono_opcional = is_null( $cotizacion->persona_telefono_opcional) ? "" : $cotizacion->persona_telefono_opcional;
							$localidad = is_null( $cotizacion->persona_localidad) ? "" : $cotizacion->persona_localidad;
						}
						else
						{
							$segmento = self::SEGMENTO_OTROS_SERVICIOS;
							$islr = self::ISLR_JURIDICA;
							$cedula = $cotizacion->empresa_rif;
							$nombre = $cotizacion->empresa_nombre;
							$direccion = is_null( $cotizacion->empresa_direccion) ? "" : $cotizacion->empressa_direccion;
							$telefono = is_null( $cotizacion->empresa_telefono) ? "" : $cotizacion->empresa_telefono_opcional;
							$telefono_opcional = is_null( $cotizacion->empresa_telefono_opcional) ? "" : $cotizacion->empresa_telefono_opcional;
							$localidad = is_null( $cotizacion->empresa_localidad) ? "" : $cotizacion->empresa_localidad;
						}
						break;
					case 43 : //ASEGURADORA
						$segmento = self::SEGMENTO_SEGUROS;
						$islr = self::ISLR_JURIDICA;
						$cedula = is_null($cotizacion->seguro_rif) ? "" : $cotizacion->seguro_rif;
						$nombre = is_null($cotizacion->seguro_nombre) ? "" : $cotizacion->seguro_nombre;
						$direccion = is_null( $cotizacion->seguro_direccion) ? "" : $cotizacion->seguro_direccion;
						$telefono = is_null( $cotizacion->seguro_telefono) ? "" : $cotizacion->seguro_telefono_opcional;
						$telefono_opcional = is_null( $cotizacion->seguro_telefono_opcional) ? "" : $cotizacion->seguro_telefono_opcional;
						$localidad = is_null( $cotizacion->seguro_localidad) ? "" : $cotizacion->seguro_localidad;
						break;
					case 44 : //MEDICO
						$segmento = self::SEGMENTO_OTROS_SERVICIOS;
						$islr = self::ISLR_NATURAL;
						$cedula = $cotizacion->medico_cedula;
						$nombre = $cotizacion->medico_primer_nombre." ".$cotizacion->medico_primer_apellido;
						$direccion = is_null( $cotizacion->medico_direccion) ? "" : $cotizacion->medico_direccion;
						$telefono = is_null( $cotizacion->medico_telefono) ? "" : $cotizacion->medico_telefono_opcional;
						$telefono_opcional = is_null( $cotizacion->medico_telefono_opcional) ? "" : $cotizacion->medico_telefono_opcional;
						$localidad = is_null( $cotizacion->medico_localidad) ? "" : $cotizacion->medico_localidad;
						break;
					case 45 : //CLINICA
						$segmento = self::SEGMENTO_OTROS_SERVICIOS;
						$islr = self::ISLR_JURIDICA;
						$cedula = is_null($cotizacion->centro_salud_rif ) ? "" : $cotizacion->centro_salud_rif  ;
						$nombre = is_null( $cotizacion->centro_salud_nombre) ? "" : $cotizacion->centro_salud_nombre;
						$direccion = is_null( $cotizacion->centro_salud_direccion) ? "" : $cotizacion->centro_salud_direccion;
						$telefono = is_null( $cotizacion->centro_salud_telefono) ? "" : $cotizacion->centro_salud_telefono_opcional;
						$telefono_opcional = is_null( $cotizacion->centro_salud_telefono_opcional) ? "" : $cotizacion->centro_salud_telefono_opcional;
						$localidad = is_null( $cotizacion->centro_salud_localidad) ? "" : $cotizacion->centro_salud_localidad;
						break;
					default:
						$nombre = "";
						$cedula = "";
						break;
				}
				
				$correo_electronico = "";
				$id_cliente = $this->__getIdClienteByCedula($cedula);
				if(	$id_cliente === FALSE)
				{
					/*
					 * Se omite la creacion debido a la tabla clientes 
					 * en el profit que tiene un unique key con el campo vendedor
					 */
					//$id_cliente = $this->__createCliente($nombre, $cedula, $direccion, $localidad, $telefono, $islr, $segmento, $id_vendedor, $correo_electronico);
					//if(	$id_cliente === FALSE)
					//	throw new Exception("Fallo cotizacion $id ".$cotizacion->id_cotizacion." , cliente : $cedula no exite", self::ERROR_FAIL_MEDICO);
					
				}
				//Codigo general que se le asigna a todos los clientes (conmed)
				$id_cliente_code = "GEN01";
				
				$id = $this->__generateIdCotizacion();
				$nombre_medico = $cotizacion->medico_primer_nombre." ".$cotizacion->medico_primer_apellido;
				$id_medico = $this->__getIdMedicoByNombre($nombre_medico);
				if(	$id_medico === FALSE)
				{
					throw new Exception("Fallo cotizacion $id ".$cotizacion->id_cotizacion." , medico : $nombre_medico no exite", self::ERROR_FAIL_MEDICO);
				}
				
				if(	$id === FALSE)
				{
					throw new Exception("Fallo cotizacion $id ".$cotizacion->id_cotizacion." , id no se genero.", self::ERROR_FAIL_COTIZACION);
				}
				
				$paciente_cedula = "V-".$cotizacion->paciente_cedula;
				$paciente_nombre = trim($cotizacion->paciente_primer_nombre." ".$cotizacion->paciente_primer_apellido);
				
	            $monto_total = number_format($cotizacion->monto_total, 2, '.', '');
	            $fecha_creacion = date("Y-m-d H:i:s", strtotime($cotizacion->fecha_creacion));
	            $fecha_finalizacion = date("Y-m-d H:i:s", strtotime("+".$this->cotizacion_days." day", strtotime($cotizacion->fecha_creacion) ));
	            
	           
	            $monto_subtotal = number_format($cotizacion->monto_subtotal, 2, '.', '');
				$monto_total = $cotizacion->monto_total;
	            $monto_iva = $cotizacion->monto_iva;
	            $descuento = number_format($cotizacion->descuento, 2, '.', '') ;
	            $monto_exento = number_format($cotizacion->monto_exento, 2, '.', '') ;
	            $sql = "
					INSERT INTO [". $this->bd ."].[dbo].[cotiz_c] (
	            		[fact_num]
						,[contrib]
						,[nombre]
						,[rif]
						,[nit]
						,[status]
						,[comentario]
						,[descrip]
						,[saldo]
						,[fec_emis]
						,[fec_venc]
						,[co_cli]
						,[co_ven]
						,[co_tran]
						,[dir_ent]
						,[forma_pag]
						,[tot_bruto]
						,[tot_neto]
						,[glob_desc]
						,[tot_reca]
						,[porc_gdesc]
						,[total_uc]
						,[total_cp]
						,[tot_flete]
						,[monto_dev]
						,[totklu]
						,[anulada]
						,[impresa]
						,[iva]
						,[iva_dev]
						,[feccom]
						,[numcom]
						,[tasa]
						,[moneda]
						,[seriales]
						,[tasag]
						,[tasag10]
						,[tasag20]
						,[co_us_in]
						,[fe_us_in]
						,[co_us_mo]
						,[fe_us_mo]
						,[fe_us_el]
						,[co_sucu]
						,[mon_ilc]
						,[otros1]
						,[otros2]
						,[otros3]
						,[aux01]
	            		)
	            	values (
	            		$id,
						1,
						'$paciente_nombre',
						'$paciente_cedula',
						'',
						0,
						'',
						'$nombre',
						$monto_total,
						'$fecha_creacion',
						'$fecha_finalizacion',
						'$id_cliente_code',
						'$id_vendedor',
						'$id_medico',
						'ccs',
						'30',
						$monto_subtotal,
						$monto_total,
						$monto_exento,
						0.00,
						$descuento,
						0.00,
						0.00,
						0.00,
						0.00,
						0.00,
						0,
						0,
						$monto_iva,
						0.00,
						'$fecha_creacion',
						0,
						1.00000,
						'BSF',
						0,
						12.00000,
						8.00000,
						0.00000,
						'AST',
						'$fecha_creacion',
						'AST',
						'$fecha_creacion',
						'$fecha_creacion',
						'CCS',
						0.00000,
						0.00000,
						0.00000,
						0.00000,
						0.00000
	            	);
	            
	            ";
	            
	            if( mssql_query( $sql, $this->conn ) === FALSE ) {
					throw new Exception("Fallo ".$cotizacion->id_cotizacion." sql: $sql Error: ", self::ERROR_FAIL_COTIZACION);
	            }
	            else{
					//echo "cotizacion add $id ".$cotizacion->id_cotizacion." <br/>" ;
					
					$this->__addPista($id);
	                //Recorro los materias para agregarlos
					foreach($cotizacion->materiales as $i => $material)
					{
						$id_cotizacion = $id;
						$indice = $i + 1;
						$modelo = $this->__getIdArticuloByModelo($material->modelo);
						if($modelo == FALSE)
						{
							throw new Exception("Ho hay producto modelo: ".$material->modelo, self::ERROR_FAIL_PRODUCTO);
						}
						
						$precio_material = number_format ( ($material->precio * $material->cantidad ), 2 ,  '.' ,'' );
						$precio_material2 = number_format ( $material->precio, 5,  '.' ,'' );
						$fecha_creacion_material = $fecha_creacion;
						$cantidad_material = number_format ( $material->cantidad, 5 ,  '.' ,'' );
						$almacen  = "BOL";
						$sql_material = "
							INSERT INTO [". $this->bd ."].[dbo].[reng_cac] (
									[fact_num]
									,[reng_num]
									,[tipo_doc]
									,[reng_doc]
									,[num_doc]
									,[co_art]
									,[co_alma]
									,[total_art]
									,[stotal_art]
									,[pendiente]
									,[uni_venta]
									,[prec_vta]
									,[porc_desc]
									,[tipo_imp]
									,[reng_neto]
									,[cos_pro_un]
									,[ult_cos_un]
									,[ult_cos_om]
									,[cos_pro_om]
									,[total_dev]
									,[monto_dev]
									,[prec_vta2]
									,[anulado]
									,[des_art]
									,[seleccion]
									,[cant_imp]
									,[comentario]
									,[total_uni]
									,[mon_ilc]
									,[otros]
									,[nro_lote]
									,[fec_lote]
									,[pendiente2]
									,[tipo_doc2]
									,[reng_doc2]
									,[num_doc2]
									,[co_alma2]
									,[aux01]
									,[aux02]
									,[cant_prod]
									,[imp_prod]
									)
								values (
									$id_cotizacion,
									$indice,
									'',
									0,
									0,
									'$modelo',
									'$almacen',
									$cantidad_material,
									0.00000,
									$cantidad_material,
									'UNI',
									$precio_material2,
									'',
									1,
									$precio_material,
									0.00000,
									0.00000,
									0.00000,
									0.00000,
									0.00000,
									0.00000,
									$precio_material2,
									0,
									'',
									0,
									0.00000,
									'',
									1.00000,
									0.00000,
									0.00000,
									'',
									'$fecha_creacion_material',
									0.00000,
									'',
									0,
									0,
									'',
									0.00000,
									'',
									0.00000,
									0.00000
				
							)
						";
						if( mssql_query($sql_material, $this->conn ) === FALSE ) {
							throw new Exception("Error - cotizacion: ".$cotizacion->id_cotizacion." Fallo articulo modelo: $modelo Error sql: ", self::ERROR_FAIL_PRODUCTO);
						}
						else{
							//echo " producto add c: $id  p: $modelo \n";
						}
						
					}
					$s_cotizaciones[] = array('id_cotizacion'=> $cotizacion->id_cotizacion, 'id_cotizacion_nueva' => $id, 'error_code' => null );
	            }
				mssql_query("COMMIT TRAN", $this->conn );	
				
			}
			catch(Exception $e){
				$e_cotizaciones[] = array('id_cotizacion' => $cotizacion->id_cotizacion, 'error_code' => $e->getCode(), 'error_content' => $e->getMessage());
				echo $e->getMessage()."\n";
				mssql_query("ROLLBACK TRAN", $this->conn );
				//$this->__sendEmailError($e->getMessage());
			}		
        }
        //se retorna las cotizaciones enviadas y las generadas con error
        return array_merge($e_cotizaciones, $s_cotizaciones);
    }

    /**
     * matiene el control del consumo del servicio rest
     * 
     */
    function init_search_cotizaciones()
    {
		$hour = date('G');
		$is_stop = 0;
		
		while($is_stop == 0 || $hour == $this->hour_end_process){
			try{
				$this->__init_parameter();
				$is_stop = $this->__verificarServicio();
				$cotizaciones = $this->__getCotizaciones();
				$cotizaciones_send = $this->__insertCotizacionesInSqlServer($cotizaciones);
				$this->__sendCotizaciones($cotizaciones_send);
				$is_stop = $this->__verificarServicio();
			}catch(Exception $e )
			{
				//$this->__sendEmailError($e->getMessage());
			}
			if($this->hour_hard_init <= $hour && $this->hour_hard_end >= $hour)
			{
				sleep(1 * $this->seconds_sleep_hard  );
			}
			else
			{
				sleep(1 * $this->seconds_sleep_soft);
			}
			
		}
    }
    
    /**
     * Ejecuta un query 
     * 
     * @param unknown $sql 
     * @param string $conexion
     * @return mixed
     */
    function ast_db_query($sql, $conexion = null)
    {
    	$conn = null;
    	if(is_null($conn))
    	{
    		$conn = $this->conn;
    	}
    	else
    	{
    		$conn = $conexion;
    	}
    	$result = mssql_query($this->latin1($sql), $conn);
    	return $result;
    }

}

$consumer = new Conmed_consumer();
$consumer->init_search_cotizaciones();

