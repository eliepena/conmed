/*
 * validate.js 1.1
 * Copyright (c) 2012 Rick Harrison, http://rickharrison.me
 * validate.js is open sourced under the MIT license.
 * Portions of validate.js are inspired by CodeIgniter.
 * http://rickharrison.github.com/validate.js
 */

(function(k,l,i){
    var m={
        required:"El campo %s es requerido.",
        matches:"El campo %s  no es igual al campo %s .",
        valid_email:"El campo %s debe contener una dirección de correo válida.",
        valid_emails:"El campo %s debe contener todas las direcciones de correo válidas.",
        min_length:"El campo %s debe contener al menos %s caracteres de longitud.",
        max_length:"El campo %s no debe exceder los %s caracteres de longitud.",
        exact_length:"El campo %s debe tener exactamente %s caracteres de longitud.",
        greater_than:"El campo %s debe contener un número mayor que %s.",
        less_than:"El campo %s debe contener un número menor que %s.",
        alpha:"El campo %s solamente puede contener caracteres alfabéticos.",
        alpha_numeric:"El campo %s solamente puede contener caracteres alfanuméricos.",
        alpha_dash:"El campo %s solamente puede contener caracteres alfanuméricos, guiones bajos y saltos.",
        numeric:"El campo %s debe contener un número.",
        integer:"El %s campo debe contener un número entero.",
        decimal:"The %s field must contain a decimal number.",
        is_natural:"El campo %s debe contener solo numeros positivos.",
        is_natural_no_zero:"El campo %s debe contener un número mayor que cero.",
        valid_ip:"El campo %s debe contener una direccion ip válida.",
        valid_base64:"El campo %s debe contener una cadena base64.",
        valid_alpha_space:'El campo %s debe contener solo letras/espacios.',
        valid_alpha_numeric_space:'El campo %s debe contener solo letras, espacios, números, comas y puntos.',
        valid_phone:'El campo %s debe contener un telefono valido Ejm: 04121233443.',
        valid_rif:'El campo %s debe contener un rif valido. Ejm: J123456789.',
        valid_url:'El campo %s debe contener una URL valida.',
        valid_date:'El campo %s debe ser una fecha valida con formato 16-12-1985.'
    },
    n=function(){},
    o=/^(.+)\[(.+)\]$/,
    h=/^[0-9]+$/,
    p=/^\-?[0-9]+$/,
    g=/^\-?[0-9]*\.?[0-9]+$/,
    j=/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,6}$/i,
    q=/^[a-zÑñÁáÉéÍíÓóÚúäëïöüÿÄËÏÖÜŸ]+$/i,
    r=/^[a-z0-9]+$/i,
    s=/^[a-z0-9_-]+$/i,
    t=/^[0-9]+$/i,
    u=/^[1-9][0-9]*$/i,
    v=/^((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){3}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})$/i,
    w=/[^a-zA-Z0-9\/\+=]/i,
    as=/^[a-zÑñÁáÉéÍíÓóÚúäëïöüÿÄËÏÖÜŸ\s]+$/i,
    ans=/^[a-z0-9ÑñÁáÉéÍíÓóÚúäëïöüÿÄËÏÖÜŸ\.\,\s\-\_]+$/i,
    // ph=/^04(1|2)(4|6)\d{7}$|^0412\d{7}$|^02\d{9}$/i,
    ph=/^\(04(1|2)(4|6)\)\s\d{3}-\d{4}$|^\(0412\)\s\d{3}-\d{4}$|^\(02\d{2}\)\s\d{3}-\d{4}$/i,
    rif=/^[VEJPG]\d{9}$/i,
    url=/(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/,
    vd=/([0-3][0-9])-([0-9]{1,2})-([1-2][0-9]{3})/,
    e=function(a,b,c){
        this.callback=c||n;
        this.errors=[];
        this.fields={};

        this.form=l.forms[a]||{};

        this.messages={};

        this.handlers={};

        a=0;
        for(c=b.length;a<c;a++){
            var d=b[a];
            d.name&&d.rules&&(this.fields[d.name]={
                name:d.name,
                display:d.display||d.name,
                rules:d.rules,
                id:null,
                type:null,
                value:null,
                checked:null
            })
        }
        this.form.onsubmit=function(a){
            return function(b){
                try{
                    return a._validateForm(b)
                }catch(c){}
            }
        }(this)
    };

    e.prototype.setMessage=function(a,b){
        this.messages[a]=b;
        return this
    };

    e.prototype.registerCallback=
    function(a,b){
        a&&("string"===typeof a&&b&&"function"===typeof b)&&(this.handlers[a]=b);
        return this
    };

    e.prototype._validateForm=function(a){
        this.errors=[];
        for(var b in this.fields)if(this.fields.hasOwnProperty(b)){
            var c=this.fields[b]||{},d=this.form[c.name];
            d&&d!==i&&(c.id=d.id,c.type=d.type,c.value=d.value,c.checked=d.checked);
            this._validateField(c)
        }
        "function"===typeof this.callback&&this.callback(this.errors,a);
        if(0<this.errors.length)if(a&&a.preventDefault)a.preventDefault();else return!1;
        return!0
    };
    e.prototype._validateField=function(a){
        var b=a.rules.split("|");
        if(!(-1===a.rules.indexOf("required")&&(!a.value||""===a.value||a.value===i)))for(var c=0,d=b.length;c<d;c++){
            var f=b[c],e=null,g=!1;
            if(parts=o.exec(f))f=parts[1],e=parts[2];
            "function"===typeof this._hooks[f]?this._hooks[f].apply(this,[a,e])||(g=!0):"callback_"===f.substring(0,9)&&(f=f.substring(9,f.length),"function"===typeof this.handlers[f]&&!1===this.handlers[f].apply(this,[a.value])&&(g=!0));
            if(g){
                b=this.messages[f]||m[f];
                c="An error has occurred with the "+
                a.display+" field.";
                b&&(c=b.replace("%s",a.display),e&&(c=c.replace("%s",this.fields[e]?this.fields[e].display:e)));
                this.errors.push({
                    id:a.id,
                    name:a.name,
                    message:c
                });
                break
            }
        }
    };

    e.prototype._hooks={
        required:function(a){
            var b=a.value;
            return"checkbox"===a.type?!0===a.checked:null!==b&&""!==b
        },
        matches:function(a,b){
            return(el=this.form[b])?a.value===el.value:!1
        },
        valid_email:function(a){
            return j.test(a.value)
        },
        valid_emails:function(a){
            for(var a=a.value.split(","),b=0;b<a.length;b++)if(!j.test(a[b]))return!1;
            return!0
        },
        min_length:function(a,b){
            return!h.test(b)?!1:a.value.length>=parseInt(b,10)
        },
        max_length:function(a,b){
            return!h.test(b)?!1:a.value.length<=parseInt(b,10)
        },
        exact_length:function(a,b){
            return!h.test(b)?!1:a.value.length===parseInt(b,10)
        },
        greater_than:function(a,b){
            return!g.test(a.value)?!1:parseFloat(a.value)>parseFloat(b)
        },
        less_than:function(a,b){
            return!g.test(a.value)?!1:parseFloat(a.value)<parseFloat(b)
        },
        alpha:function(a){
            return q.test(a.value)
        },
        alpha_numeric:function(a){
            return r.test(a.value)
        },
        alpha_dash:function(a){
            return s.test(a.value)
        },
        numeric:function(a){
            return g.test(a.value)
        },
        integer:function(a){
            return p.test(a.value)
        },
        decimal:function(a){
            return g.test(a.value)
        },
        is_natural:function(a){
            return t.test(a.value)
        },
        is_natural_no_zero:function(a){
            return u.test(a.value)
        },
        valid_ip:function(a){
            return v.test(a.value)
        },
        valid_base64:function(a){
            return w.test(a.value)
        },
        valid_alpha_space:function(a){
            return as.test(a.value)
        },
        valid_alpha_numeric_space:function(a){
            return ans.test(a.value)
        },
        valid_phone:function(a){
            return ph.test(a.value)
        },
        valid_rif:function(a){
            return rif.test(a.value)
        },
        valid_url:function(a){
            return url.test(a.value)
        },
        valid_date:function(a){
            return vd.test(a.value)
        }
    };

    k.FormValidator=e
})(window,document);