var article_type = null, table_to_add_global = null, articles_table = null, id_article_global = null, name_add_table = ' cotizacion ', id_table_total = '#totalesTable', id_table_for_add_table_total = '#materialTable', type_articulo_for_calcule_total = 'mat', id_form_for_save_cotizacion = '#_form', type_cotizacion = 'cotizacion';
function build_table_articles() {
    $('#detail-article').hide();
    $.getJSON(BASE_URL + 'assets/system/articles.json', {
        q: new Date().getTime()
    }, function(data) {
        var articles = data.data;
        articles_table = $('.js-table-articulos').dataTable({
            "aoColumns": data.headers,
            "aaData": articles,
            'bDestroy': true
        });

        /* Add a click handler to the rows - this could be used as a callback */
        $(".js-table-articulos tbody").click(function(event) {
            event.preventDefault();
            $(articles_table.fnSettings().aoData).each(function() {
                $(this.nTr).removeClass('article_selected');
            });
            $(event.target.parentNode).addClass('article_selected');
            var selected = fnGetSelected(articles_table);
            selected = $(selected).find('td:nth-child(1)').text();
            for (var j = 0, len = articles.length; j < len; j++) {
                if (articles[j][1] == selected) {
                    $('#article-model div').text(articles[j][1]);
                    $('#article-description div').text(articles[j][2]);
                    var img = (articles[j][3]) ? 'uploads/images/instrumentos/' + articles[j][3] : 'assets/img/image_no_found.jpg';
                    $('#article-image img').attr('src', BASE_URL + img);

                    //Obtener Precios del Articulo
                    var prices = string_to_array(articles[j][5], ',', '{}');
                    var price_names = string_to_array(articles[j][7], ',', '{}');
                    var price_body = '';
                    for (var i = 0, ln = price_names.length; i < ln; i++)
                        price_body += '<tr><td>' + price_names[i] + '</td>' + '<td>' + money_format(prices[i]) + '</td></tr>';
                    $('#article-prices div table tbody').empty().append($(price_body));

                    var stock = string_to_array(articles[j][8], ',', '{}');
                    var stock_body = '';
                    for (i = 0, ln = stock.length; i < ln; i++) {
                        var arr = stock[i].split('|');
                        //Valido que el arreglo posee mas de 2 pocisiones (almacen, cantidad).
                        if (arr.length > 1) {
                            stock_body += '<tr><td>' + arr[0] + '</td>' + '<td>' + arr[1] + '</td></tr>';
                        } else { //caso contrario rompo ciclo y muestro mensaje de no existencia.
                            stock_body = '<tr><td colspan="2" class="alert-error">Este articulo no posee stock disponible.</td></tr>';
                            break;
                        }
                    }
                    $('#article-stock div table tbody').empty().append($(stock_body));
                    break;
                }
            }
            $('#detail-article').show();
        }).dblclick(function(event) {
            event.preventDefault();
            $(event.target.parentNode).addClass('article_selected');
            var selected = fnGetSelected(articles_table);
            selected = $(selected).find('td:nth-child(1)').text();
            for (var j = 0, len = articles.length; j < len; j++) {
                if (articles[j][1] == selected) {
                    var msg = '¿Desea agregar el material ';
                    msg += articles[j][1] + ' - ' + articles[j][2];
                    msg += ' a la ' + name_add_table + ' actual?';

                    // $('.bootbox.modal').remove();
                    bootbox.confirm(msg, function(rs) {
                        if (rs === true) {
                            get_material(article_type, table_to_add_global, articles[j]);
                            hide_articles();
                        }
                        bootbox.hideAll();
                    });
                    break;
                }
            }
        });
    });
    return true;
}

/* Get the rows which are currently selected */
function fnGetSelected(oTableLocal)
{
    var aReturn = new Array();
    var aTrs = oTableLocal.fnGetNodes();

    for (var i = 0; i < aTrs.length; i++)
    {
        if ($(aTrs[i]).hasClass('article_selected'))
        {
            aReturn.push(aTrs[i]);
        }
    }
    return aReturn;
}


/**
 * Elimina multiples caracteres, limpia espacios a los extremos de una cadena
 *
 * @param  {string} str                 Cadena a tratar
 * @param  {string} charlist            Caracteres a eliminar
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  23/08/13 10:36 AM
 */
function trim(str, charlist) {
    var whitespace, l = 0,
    i = 0;
    str += '';

    if (!charlist) {
        // lista por defecto
        whitespace = " \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000";
    } else {
        // lista personalizada
        charlist += '';
        whitespace = charlist.replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '$1');
    }

    l = str.length;
    for (i = 0; i < l; i++) {
        if (whitespace.indexOf(str.charAt(i)) === -1) {
            str = str.substring(i);
            break;
        }
    }

    l = str.length;
    for (i = l - 1; i >= 0; i--) {
        if (whitespace.indexOf(str.charAt(i)) === -1) {
            str = str.substring(0, i + 1);
            break;
        }
    }

    return whitespace.indexOf(str.charAt(0)) === -1 ? str : '';
}


/**
 * Agrega los nuevos articulos a las tablas de cotizacion (backup, materiales, instrumentos)
 *
 * @param  {array} params           Datos de para la insercion de filas de articulos
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  23/08/13 10:33 AM
 */

function add_tr(params) {
    if (params.tipo.toLowerCase() == 'mat' || params.tipo.toLowerCase() == 'backup') {
        if (params.data.type.toLowerCase() != 'mat') {
            bootbox.alert('Debe seleccionar solo Materiales.');
            return false;
        }
    } else if (params.tipo.toLowerCase() == 'instru') {
        if (params.data.type.toLowerCase() != 'ins') {
            bootbox.alert('Debe seleccionar solo Instrumentos.');
            return false;
        }
    }
    var color = '';
    var zero = true;
    var option_precio = '', control = '', delete_totales = '';
    var id_precios = string_to_array(params.data.id_precios, ',', '{}');
    var precios = string_to_array(params.data.precios, ',', '{}');
    if (params.tipo == 'mat')
        delete_totales = ' delete_tr(\'tr-total-' + params.id + '\'); ';

    if (trim(precios).length <= 0)
        zero = true;
    else
        for (var i = precios.length - 1; i >= 0; i--)
            if (parseInt(precios[i], 10) !== 0)
                zero = false;

    if (zero) {
        control = '<input type=\'text\' id="price-' + params.tipo + '-' + params.id + '" name="precio_' + params.tipo + '[' + params.id + ']" onchange="calculateTotal();calculateTotalBackup();" value="0"/>';
        control_for_replace = '<input type=\'text\' id="price-' + params.tipo + '-' + params.id + '" name="precio_' + params.tipo + 'Replace[' + params.id + ']" value="0"/>';
    } else {
        for (var indice in id_precios) {
            option_selected = (id_precios[indice] == $("select[name='id_precios']").val() ? ' selected="selected" ' : '');
            option_precio += '<option ' + option_selected + ' value="' + precios[indice] + '">' + money_format(parseFloat(precios[indice])) + '</option>';
        }
        control = '<select onchange="calculateTotal();calculateTotalBackup();" id="price-' + params.tipo + '-' + params.id + '" name="precio_' + params.tipo + '[' + params.id + ']">' + option_precio + '</select>';
        control_for_replace = '<select id="price-' + params.tipo + '-' + params.id + '" name="precio_' + params.tipo + 'Replace[' + params.id + ']">' + option_precio + '</select>';
    }
    if (id_article_global) {
        $(".sustituto").hide();
        $("." + params.tipo.toLowerCase() + "-" + id_article_global + "-Replace").show();
        if (params.data.modelo != $('#model-' + params.tipo + '-' + params.id + '-replace').text()) {

            $('#' + params.tipo.toLowerCase() + "-" + id_article_global).append('<tr id="tr-' + params.tipo + '-' + params.id + '-replace">');
            $('#tr-' + params.tipo + '-' + params.id + '-replace').append('<td ><input class="indicador-' + params.tipo + '" id="' + params.tipo + '-' + params.id + '-replace" type="hidden" value="' + id_article_global + '" key="' + params.id + '" name="' + params.tipo.toLowerCase() + '_replace[' + params.id + ']" /><i class="icon-remove" onclick="delete_tr(\'tr-' + params.tipo + '-' + params.id + '\',\'' + params.tipo + '-replace\'); ' + delete_totales + ' "></i></td>');
            $('#tr-' + params.tipo + '-' + params.id + '-replace').append('<td id="img-' + params.tipo + '-' + params.id + '-replace"><a data-toggle="modal" href="#" onclick="show_img_article(\'' + params.data.modelo + '\',\'' + params.data.descripcion + '\',\'' + params.data.img + '\');"><i class="icon-picture"></i></a></td>');
            $('#tr-' + params.tipo + '-' + params.id + '-replace').append('<td id="model-' + params.tipo + '-' + params.id + '-replace">' + params.data.modelo + '</td>');
            $('#tr-' + params.tipo + '-' + params.id + '-replace').append('<td id="name-' + params.tipo + '-' + params.id + '-replace">' + params.data.descripcion + '</td>');
            if (params.tipo != 'ins')
                $('#tr-' + params.tipo + '-' + params.id + '-replace').append('<td>' + control_for_replace + '</td>');
            $('#tr-' + params.tipo + '-' + params.id + '-replace').append('<td><input min="1" type="number"  id="cant-' + params.tipo + '-' + params.id + '-replace" class="input-mini" name="cant_' + params.tipo.toLowerCase() + '_replace[' + params.id + ']" placeholder="0" key="' + params.id + '" value="' + params.data.cantidad + '"></td>');
            $('#tr-' + id_article_global).append('</tr>');

        } else {
            $('#cant-' + params.tipo + '-' + params.id).val(parseInt($('#cant-' + params.tipo + '-' + params.id).val(), 10) + 1);
        }
    } else {
        if (params.data.modelo != $('#model-' + params.tipo + '-' + params.id).text()) {
            if (params.data.cantidad > params.data.stock) {
                color = 'red';
            } else if (params.data.cantidad > 0 && params.data.cantidad == params.data.stock) {
                color = 'yellow';
            } else if (params.data.cantidad < params.data.stock) {
                color = 'blue';
            }

            $('#' + params.table).append('<tr id="tr-' + params.tipo + '-' + params.id + '">');
            $('#tr-' + params.tipo + '-' + params.id).append('<td ><input class="indicador-' + params.tipo + '" id="' + params.tipo + '-' + params.id + '" type="hidden" value="' + params.id + '" key="' + params.id + '" name="' + params.tipo + '[' + params.id + ']" /><i class="icon-remove" onclick="delete_tr(\'tr-' + params.tipo + '-' + params.id + '\',\'' + params.tipo + '\'); ' + delete_totales + ' "></i></td>');
            $('#tr-' + params.tipo + '-' + params.id).append('<td id="img-' + params.tipo + '-' + params.id + '"><a data-toggle="modal" href="#" onclick="show_img_article(\'' + params.data.modelo + '\',\'' + params.data.descripcion + '\',\'' + params.data.img + '\');"><i class="icon-picture"></i></a></td>');
            $('#tr-' + params.tipo + '-' + params.id).append('<td id="model-' + params.tipo + '-' + params.id + '">' + params.data.modelo + '</td>');
            $('#tr-' + params.tipo + '-' + params.id).append('<td id="name-' + params.tipo + '-' + params.id + '">' + params.data.descripcion + '</td>');

            if (params.table != 'table_articles_selections') {
                if (params.tipo != 'instru')
                    $('#tr-' + params.tipo + '-' + params.id).append('<td>' + control + '</td>');
                $('#tr-' + params.tipo + '-' + params.id).append('<td><input min="1" type="number" onblur="calculateTotal();calculateTotalBackup();check_disponibilidad(this.id,\'' + params.tipo + '\',\'' + params.table + '\');" id="cant-' + params.tipo + '-' + params.id + '" class="input-mini" name="cant_' + params.tipo + '[' + params.id + ']" placeholder="0" key="' + params.id + '" value="' + params.data.cantidad + '"></td>');
                $('#tr-' + params.tipo + '-' + params.id).append('<td ><div class="img-circle" id="leyenda-' + params.tipo + '-' + params.id + '" style="background-color:' + color + '"> <input type="hidden" id="disp-' + params.tipo + '-' + params.id + '" value="' + params.data.stock + '"/>&nbsp;&nbsp;&nbsp;</div></td>');
            }
            $('#tr-' + params.table).append('</tr>');
        } else {
            $('#cant-' + params.tipo + '-' + params.id).val(parseInt($('#cant-' + params.tipo + '-' + params.id).val(), 10) + 1);
        }

        if (params.table != 'table_articles_selections') {
            if (params.tipo == 'mat')
                calculateTotal();
            cant_articulo(params.table, 'cant_' + params.tipo, 'cant-' + params.tipo, params.tipo);
            calculateTotalBackup();
        }
    }
    if (params.tipo == 'mat')
        calculateTotal();
    cant_articulo(params.table, 'cant_' + params.tipo, 'cant-' + params.tipo, params.tipo);
    calculateTotalBackup();
    calculateTotal();
}


/**
 * Suprime los articulos a las tablas de cotizacion (backup, materiales, instrumentos)
 *
 * @param  {string} id_fila               Identificador de la fila a eliminar
 * @param  {string} calculo_mat           Bandera para determinar si se aplica calculo de precios (solo para materiales)
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  23/08/13 10:31 AM
 */
function delete_tr(id_fila, calculo_mat) {
    $('#' + id_fila).hide(500, function() {
        if ($(this).remove()) {
            if (calculo_mat == 'mat') {
                calculateTotal();
            }
        }
    });
}

/**
 * Aplica el funcionamiento personalizado a los campos cantidad
 *
 * @param  {string} id_table              Identificador de la tabla a tratar
 * @param  {string} name_input_cantidad   Name del input cantidad a tratar
 * @param  {string} id_input_cantidad     Identificador del input cantidad a tratar
 * @param  {string} tipo_articulo         Tipo de articulo (Bandera para tratar distintas tablas (instrumentos,materiales,backup))
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  23/08/13 10:29 AM
 */
function cant_articulo(id_table, name_input_cantidad, id_input_cantidad, tipo_articulo) {
    var iBase = '#' + id_table;
    var sTBody = iBase + ' tbody ';
    var cant_temp;
    $(sTBody + ' .indicador-' + tipo_articulo).each(function() {
        var _this = $(this);
        var input = $(sTBody + 'input[name="' + name_input_cantidad + '[' + _this.attr('key') + ']"]');
        var item_cant = $(sTBody + '#' + id_input_cantidad + '-' + _this.attr('key')).val();
        input.attr('readonly', false);
        input.focus(function() {
            cant_temp = $(sTBody + '#' + id_input_cantidad + '-' + _this.attr('key')).val();
            input.val('');
        });
        input.blur(function() {
            var item_cant_now = $(sTBody + '#' + id_input_cantidad + '-' + _this.attr('key')).val();
            if (item_cant_now === '') {
                input.val(cant_temp);
            } else {
                input.val(item_cant_now);
            }
        });
    });
}

/**
 * Convierte un string en un arreglo
 *
 * @param  {string} string       Cadena a tratar
 * @param  {string} separador    Caracter por el cual se separara la cadena
 * @param  {string} excepciones  Caracteres excluidos de la cadena
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  23/08/13 10:25 AM
 */
function string_to_array(string, separador, excepciones) {
    return trim(trim(string), excepciones).split(separador);
}

/**
 * Muestra el modal de Articulos
 *
 * @param  {object} e            evento generado en el DOM
 * @param  {string} type         tipo de articulo. Utir para agregar al TR de las tablas
 * @param  {string} table_to_add table al cual se van agregar los articulos selecionados
 * @author  Jose A. Rodriguez E.
 * @version V- 1.0  22/08/13 14:01
 */
function show_articles(e, type, table_to_add, id_article, individual_filter, individual_filter_chain, individual_filter_column) {
    $('.js-table-articulos').empty()
    $('.js-table-articulos').dataTable({
        'bDestroy': true
    })
    build_table_articles();

    if (typeof individual_filter != "undefined" && individual_filter) {
        $('.js-table-articulos').dataTable().fnFilter(individual_filter_chain, individual_filter_column, 0, 1);
    }
    e.preventDefault();
    article_type = type;
    table_to_add_global = table_to_add;
    if (typeof id_article != "undefined") {
        id_article_global = id_article;
    } else {
        id_article_global = null;
    }


    $('#container-articles').modal({
        show: true
    });

}

/**
 * Obtiene los datos para la insercion de un nuevo articulo
 *
 * @param  {string} tipo    Bandera utilizada para diferenciar el  tipo elemento insertado
 * @param  {string} table   Identificador de la tabla a insertar el elemento
 * @param  {array}  article Datos del articulo a insertar
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  23/08/13 04:30 PM
 */
function get_material(tipo, table, article) {
    var stock = string_to_array(article[8], ',', '{}');
    var arr = stock.toString().split('|');
    var intAdd = '1';
    if (table == 'articuloTableFree') {
        id_table_total = '#totalesTableFree', id_table_for_add_table_total = '#articuloTableFree', type_articulo_for_calcule_total = 'art';
    }
    if (table == 'articuloTableOrden') {
        add_tr_delivery_free({
            table: table,
            tipo: tipo,
            id: article[0],
            data: {
                modelo: article[1],
                descripcion: article[2],
                img: article[3],
                cantidad: intAdd,
                precios: article[5],
                id_precios: article[6],
                type: article[4],
                stock: (typeof (arr[1]) == "undefined" ? 0 : arr[1])
            }
        });
    } else if (table == 'table_articles_selections') {
        add_tr({
            table: table,
            tipo: tipo,
            id: article[0],
            data: {
                modelo: article[1],
                descripcion: article[2],
                img: article[3],
            }
        });
    } else {
        add_tr({
            table: table,
            tipo: tipo,
            id: article[0],
            data: {
                modelo: article[1],
                descripcion: article[2],
                img: article[3],
                cantidad: intAdd,
                precios: article[5],
                id_precios: article[6],
                type: article[4],
                stock: (typeof (arr[1]) == "undefined" ? 0 : arr[1])
            }
        });
    }
}

function carga_sesion_first(datos) {
    var total = 0;
    for (var i = datos.length - 1; i >= 0; i--) {
        var tmp = parseFloat(datos[i].item_precio).toFixed(2) * datos[i].item_cant;
        total += parseFloat(tmp);
    }
    total = parseFloat(total).toFixed(2);
    $('#total_price-' + type_articulo_for_calcule_total + '').val(money_format(total));
    $('#monto_gravable').val(money_format(total));
    var iva = parseFloat((total * IVA) / 100).toFixed(2);
    $('#iva').text(money_format(iva));
    $('#netototal').val(money_format(parseFloat(parseFloat(total) + parseFloat(iva)).toFixed(2)));
}

function calculateTotal() {
    var sBase = id_table_for_add_table_total;
    var sTBody = sBase + ' tbody ';
    var tBase = id_table_total;
    var tTBody = tBase + ' tbody ';
    var insercion = new Array();
    var arr_cant = $(sTBody + ':input[name^="cant_' + type_articulo_for_calcule_total + '"]:not([readonly="readonly"]) ');
    $.each(arr_cant, function(i, obj) {
        var cant = $(obj);
        var control = $(sTBody + '#price-' + type_articulo_for_calcule_total + '-' + cant.attr('key'));

        //Si el campo es tipo text obtengo el valor
        //Si control es tipo select-one ontengo el option text
        var item_price = (control.prop('type') == 'text') ? control.val() : control.find('option:selected').val();

        var item_model = $(sTBody + '#model-' + type_articulo_for_calcule_total + '-' + cant.attr('key')).text();
        var item_name = $(sTBody + '#name-' + type_articulo_for_calcule_total + '-' + cant.attr('key')).text();
        var item_cant = $(sTBody + '#cant-' + type_articulo_for_calcule_total + '-' + cant.attr('key')).val();

        insercion [i] = {
            'item_model': item_model,
            'item_name': item_name,
            'item_precio': item_price,
            'item_cant': item_cant
        };

        if (item_model != $(tTBody + '#modelo-' + cant.attr('key')).text()) {
            $(id_table_total).append('<tr id="tr-total-' + cant.attr('key') + '">');//onclick="delete_tr('tr-mat-','mat')"
            $('#tr-total-' + cant.attr('key')).append('<td id="codigo-' + cant.attr('key') + '"><i class="icon-remove" onclick="delete_tr(\'tr-total-' + cant.attr('key') + '\');delete_tr(\'tr-' + type_articulo_for_calcule_total + '-' + cant.attr('key') + '\',\'' + type_articulo_for_calcule_total + '\');"></i></td>');
            $('#tr-total-' + cant.attr('key')).append('<td id="modelo-' + cant.attr('key') + '"></td>');
            $('#tr-total-' + cant.attr('key')).append('<td id="descripcion-' + cant.attr('key') + '"></td>');
            $('#tr-total-' + cant.attr('key')).append('<td id="gart-' + cant.attr('key') + '"></td>');
            $('#tr-total-' + cant.attr('key')).append('<td id="cantidad-' + cant.attr('key') + '"></td>');
            $('#tr-total-' + cant.attr('key')).append('<td id="precio-' + cant.attr('key') + '"></td>');
            $('#tr-total-' + cant.attr('key')).append('<td id="neto-' + cant.attr('key') + '"></td>');
            $(id_table_total).append('</tr>');
        }

        $('#modelo-' + cant.attr('key')).text(item_model);
        $('#descripcion-' + cant.attr('key')).text(item_name);
        $('#cantidad-' + cant.attr('key')).text(item_cant);
        $('#precio-' + cant.attr('key')).text(money_format(parseFloat(item_price).toFixed(2)));
        $('#neto-' + cant.attr('key')).text(money_format(parseFloat(parseFloat(item_price) * parseInt(item_cant, 10)).toFixed(2)));
    });

    carga_sesion_first(insercion);
}

/**
 * Obtiene los datos de los articulos con respecto al precio, articulacion, lesion o tecnica seleccionada
 *
 * @param  {array}  params  Datos para la busqueda de datos
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  26/08/13 03:55 PM
 */
function load_select(params) {
    $('body').modalmanager('loading');
    //Dispara el evento

    $.ajax({
        type: "GET",
        url: params.url,
        data: params.data,
        dataType: 'json',
        success: function(response) {
            $('body').modalmanager('loading');
            $(params.container.mat).html(response.materiales);
            $(params.container.backup).html(response.backup);
            $(params.container.instru).html(response.instrumentos);
        }
    });
}

/**
 * Valida el tab de DATOS de la cotizacion para poder continuar con la carga de informacion
 *
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  26/08/13 03:55 PM
 */
function validate_data_tab() {
    $('body').modalmanager('loading');
    //Dispara el evento
    $.ajax({
        url: BASE_URL + 'cotizacion/cotizacion/create/process/validate_data',
        data: $('#_form').serialize(),
        success: function(response) {
            $('body').modalmanager('loading');

            //Verifica si el TAB DATOS se valida exitosamente y muestra los demas TABS
            if (response.validate == 1) {
                $("li:has(a[href=#1]),#1").removeClass("active");
                $("li:has(a[href=#2]),#2").removeClass('hide_element');
                $("li:has(a[href=#3]),#3").removeClass('hide_element');
                $("li:has(a[href=#4]),#4").removeClass('hide_element');
                $("li:has(a[href=#5]),#5").removeClass('hide_element');
                $("li:has(a[href=#2]),#2").addClass("active");

                //Quita el mensaje de Alerta Error
                $("#alert_content").empty();
            }

            //Se procesa cuando no se pueden validar los datos
            else {
                //Se mantiene o muestra nuevamente el TAB inicial y oculta los demas TABS
                $("li:has(a[href=#2]),#2").removeClass("active");
                $("li:has(a[href=#1]),#1").addClass("active");
                $("li:has(a[href=#2]),#2").addClass('hide_element');
                $("li:has(a[href=#3]),#3").addClass('hide_element');
                $("li:has(a[href=#4]),#4").addClass('hide_element');
                $("li:has(a[href=#5]),#5").addClass('hide_element');

                var textAlert = null;

                //Ocurre cuando hay un error en Base de Datos
                if (response.view == false)
                    textAlert = 'No se pudo validar los datos, si el problema persiste comuniquese con sistemas';

                //Ocurre cuando hay errores de validacion en el Servidor
                else
                    textAlert = response.view;

                //Mensaje de Alerta Error
                var alertHtml = [
                '<div class="alert alert-error fade in">',
                '<button type="button" class="close" data-dismiss="alert">×</button>',
                textAlert,
                '</div>'
                ].join('');
                //Elemento que muestra el Alert
                $("#alert_content").html(alertHtml);
            }
        }
    });
}

/**
 * Obtiene el reporte de la cotizacion (PDF)  de acuerdo al tipo de documento (backup, instrumental, cotizacion)
 *
 * @param  {string} id_cotizacion  Identificador de la Cotizacion
 * @param  {string} tipo_documento  Datos para la busqueda de datos
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  26/08/13 03:55 PM
 */
function cotizacionDocumento(id_cotizacion, tipo_documento, membrete) {

    $('body').modalmanager('loading');
    //Dispara el evento
    $.ajax({
        url: BASE_URL + 'cotizacion/' + type_cotizacion + '/EXC_quoteReport/',
        data: {
            id: id_cotizacion,
            tipo: tipo_documento,
            membrete: membrete
        },
        success: function(response) {
            $('body').modalmanager('loading');
            if (response.sucess) {
                pop_pup = window.open(BASE_URL + "cotizacion/" + type_cotizacion + "/EXC_force_download?url=" + response.file + "&name=" + tipo_documento, 'Pdf' + tipo_documento, 'width=1,height=1,scrollbars=NO');
                if (!pop_pup) {
                    bootbox.alert('Disculpe! Verifique que las ventanas emergentes de su navegador no esten bloqueadas.');
                } else {
                    setInterval(function() {
                        pop_pup.close();
                    }, 9000);
                }
            } else {
                var textAlert = 'No se pudo generar el documento, si el problema persiste comuniquese con sistemas';
                bootbox.alert(textAlert);
            }
        }
    });
}

function isDefined(variable) {
    return (typeof (window[variable]) == "undefined") ? false : true;
}

function create_backup() {
    $('#myModal-backup').modal('hide');
    //Muestra nuevamente el TAB inicial
    $("li:has(a[href=#3]),#3").addClass("active");
    $("li:has(a[href=#5]),#5").removeClass("active");
    $("li:has(a[href=#1]),#1").removeClass("active");
}

/**
 * Controla la navegacion entre TABS
 *
 * @param  {string} direction    Define la direccion del evento (Anterior, Siguiente)
 * @param  {string} current_tab  Define la tab donde se llama el este evento
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  27/08/13 01:20 PM
 */
function navigate_tab(direction, current_tab) {
    var next_tab = parseInt(current_tab, 10) + 1;
    var prev_tab = parseInt(current_tab, 10) - 1;
    if (direction == 'next') {
        $("li:has(a[href=#" + next_tab + "]),#" + next_tab).addClass("active");
        $("li:has(a[href=#" + current_tab + "]),#" + current_tab).removeClass("active");
    } else if (direction == 'prev') {
        $("li:has(a[href=#" + prev_tab + "]),#" + prev_tab).addClass("active");
        $("li:has(a[href=#" + current_tab + "]),#" + current_tab).removeClass("active");
    }
}
function save_backup() {
    var id_table = 'backupTable';
    var iBaseBackup = '#' + id_table;
    var sTBodybackup = iBaseBackup + ' tbody ';
    var id_tecnica = $("select[name='id_tecnica']").val();
    if (id_tecnica === '') {
        if ($(sTBodybackup + '.indicador-backup').length > 0)
            save_tecnica();
        else
            $('#myModal-backup').modal({
                show: true
            });
    } else {
        if ($(sTBodybackup + '.indicador-backup').length > 0)
            save();
        else
            $('#myModal-backup').modal({
                show: true
            });
    }
}

/**
 * Valida que el nombre de la tecnica sea requerido.
 *
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  26/08/13 05:33 PM
 */
function valid_tecnica() {
    var id_tecnica = $("select[name='id_tecnica']").val();
    if (id_tecnica === '') {
        var nombre_tecnica = $("#nombre_tecnica").val();
        if (nombre_tecnica === '') {
            var alertHtml = [
            '<div class="alert alert-error fade in">',
            '<button type="button" class="close" data-dismiss="alert">×</button>',
            'El campo Nombre es requerido',
            '</div>'
            ].join('');

            //Elemento que muestra el Alert de validacion del nombre de la tecnica
            var target = $("#validation_tecnica");
            target.html(alertHtml);
        } else {
            save();
        }
    } else {
        save();
    }
}

/**
 * Guarda la tecnica en caso de que no se haya seleccionado una tecnica
 *
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  26/08/13 05:33 PM
 */
function save_tecnica() {
    $('#myModal-backup').modal('hide');
    //valida que se haya seleccionado al menos un elemento del form principal
    var id_articulacion = $("select[name='id_articulacion']").val();

    if (id_articulacion === '') {
        $("li:has(a[href=#1]),#1").addClass("active");
        $("li:has(a[href=#3]),#3").removeClass("active");
        $("li:has(a[href=#4]),#4").removeClass("active");
    }
    else {
        var id_tecnica = $("select[name='id_tecnica']").val();
        if (id_tecnica === '') {
            $('#myModal-tecnica').modal({
                show: true
            });
        } else {
            save();
        }
    }
}

/**
 * Muestra el modal de la imagen del articulo
 *
 * @param  {string} mod  Modelo del Articulo
 * @param  {string} des  Descripcion del Articulo
 * @param  {string} img  Nombre de la imagen asociada al articulo
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  26/08/13 05:33 PM
 */
function show_img_article(mod, des, img) {
    //Seteamos el titulo de la ventana modal.
    $('#articleModalLabel').text('Modelo: ' + mod);

    //Seteamos la vista al modal window
    var src = ($.trim(img).length > 0) ? BASE_URL + PATH_IMG_ARTICLES + img : PATH_IMG_NO_FOUND;
    var alt = 'Modelo: ' + mod + ' Descripción: ' + des;

    //Elemento img donde seran actualizados los valorres del material actual.
    var eImg = $('#selected-article-image');
    eImg.attr('src', src);
    eImg.attr('alt', alt);
    eImg.attr('title', 'Modelo: ' + mod);

    $('#selected-article-description').text(des);

    //Mostramos el modal.
    $('#articleModal').modal('show');
    return false;
}
/**
 * Formatea un monto a 1.000,00 Usa la libreria numeral.js
 * @param  {float} amount Monto a ser formateado
 * @return {string}        monto formateado
 * @author  Jose A. Rodriguez E. <josearodrigueze@gmail.com>
 * @version V- 1.0  27/08/13 13:30
 */
function money_format(amount) {
    return accounting.formatMoney(amount, '', 2, ".", ",");
}

/**
 * Obtiene el pdf Preview de la cotizacion
 *
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  29/08/13 04:51 PM
 */
function preview_pdf(membrete) {
    var formData = $(id_form_for_save_cotizacion).serializeArray();
    formData.push({
        name: "membrete",
        value: membrete
    });
    $('body').modalmanager('loading');
    //Dispara el evento
    $.ajax({
        url: BASE_URL + 'cotizacion/' + type_cotizacion + '/create/process/preview',
        data: formData,
        success: function(response) {
            $('body').modalmanager('loading');

            //Se procesa cuando se ejecuta exitosamente el controlador
            if (response.view == 1) {
                pop_pup = window.open(BASE_URL + "cotizacion/" + type_cotizacion + "/EXC_force_download?url=" + response.file + "&name=Pdf_Preview", 'PdfPreview', 'width=1,height=1,scrollbars=NO');
                if (!pop_pup) {
                    bootbox.alert('Disculpe! Verifique que las ventanas emergentes de su navegador no esten bloqueadas.');
                } else {
                    setInterval(function() {
                        pop_pup.close();
                    }, 9000);
                }

            }
            //Se procesa cuando no se ejecuta exitosamente el controlador
            else {
                //Muestra nuevamente el TAB inicial
                $("li:has(a[href=#2]),#2").addClass("active");
                $("li:has(a[href=#5]),#5").removeClass("active");

                var textAlert = null;

                //Ocurre cuando hay un error en Base de Datos
                if (response.view == false)
                    textAlert = 'No se pudo generar la vista previa, si el problema persiste comuniquese con sistemas';

                //Ocurre cuando hay errores de validacion en el Servidor
                else
                    textAlert = response.view;

                //Mensaje de Alerta Error
                var alertHtml = [
                '<div class="alert alert-error fade in">',
                '<button type="button" class="close" data-dismiss="alert">×</button>',
                textAlert,
                '</div>'
                ].join('');

                //Elemento que muestra el Alert
                $("#alert_content").html(alertHtml);
            }
        }
    });
}

/**
 * Procesa todo el formulario
 *
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  02/09/13 08:43 AM
 */
function save() {
    var formData = $(id_form_for_save_cotizacion).serializeArray();
    var last_tab = '3';
    if (type_cotizacion == 'cotizacion') {
        last_tab = '5';
        formData.push({
            name: "nombre_tecnica",
            value: $("#nombre_tecnica").val()
        });
        $('#myModal-tecnica').modal('hide');

    }
    //Muestra nuevamente el TAB inicial
    $("li:has(a[href=#1]),#1").addClass("active");
    $("li:has(a[href=#" + last_tab + "]),#" + last_tab).removeClass("active");

    $('body').modalmanager('loading');
    //Dispara el evento
    $.ajax({
        url: BASE_URL + 'cotizacion/' + type_cotizacion + '/create/process',
        data: formData,
        success: function(response) {
            $('body').modalmanager('loading');

            var textAlert = null, alertHtml = null;

            //Se procesa cuando el registro se guarda exitosamente
            if (response.view == 1) {
                $(id_form_for_save_cotizacion).hide();
                textAlert = 'Datos guardados satisfactoriamente';
                alertHtml = [
                '<div class="alert alert-success fade in">',
                '<button type="button" class="close" data-dismiss="alert">×</button>',
                textAlert,
                '</div>'
                ].join('');
                var link = '<a id="link" href="#" class="btn btn-success" onclick="request({url:\'' + BASE_URL + 'cotizacion/' + type_cotizacion + '/listAll\'})">Regresar al listado</a>';
                $("#link").html(link);
                $('#link').on('click', function() {
                    request({
                        url: BASE_URL + 'cotizacion/' + type_cotizacion + '/listAll'
                    });
                });
                setTimeout(function() {
                    $('#link').trigger('click');
                }, 2000);

            }

            //Se procesa cuando no se puede guardar el registro
            else {
                //Ocurre cuando hay un error en Base de Datos
                if (response.view == false)
                    textAlert = 'No se pudo guardar el registro, si el problema persiste comuniquese con sistemas';

                //Ocurre cuando hay errores de validacion en el Servidor
                else
                    textAlert = response.view;

                //Mensaje de Alerta Error
                alertHtml = [
                '<div class="alert alert-error fade in">',
                '<button type="button" class="close" data-dismiss="alert">×</button>',
                textAlert,
                '</div>'
                ].join('');

            }
            //Elemento que muestra el Alert
            $("#alert_content").html(alertHtml);

        }
    });
}

/**
 * Actualiza los montos en tab totales
 *
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  02/09/13 03:16 PM
 */
function update_totales(id) {
    if ($(id).val().length < 1) {
        $(id).val(0.00);
    }
    var exento = $('#monto-exc').val();
    var mgravable = $('#monto_gravable').val();
    var desc = (parseFloat($('#porc-desc').val()) * parseFloat(mgravable)) / 100;
    var rec = (parseFloat($('#porc-rec').val()) * parseFloat(mgravable)) / 100;
    var iva = (parseFloat(mgravable) * IVA) / 100;
    var netototal = parseFloat(mgravable) - parseFloat(exento) - desc + rec + iva;
    $('#netototal').val(parseFloat(netototal).toFixed(2));
    $('#desc').text(parseFloat(desc).toFixed(2));
    $('#rec').text(parseFloat(rec).toFixed(2));
    $('#iva').text(parseFloat(iva).toFixed(2));
}

/**
 * Actualiza el monto total en tab totales
 *
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  02/09/13 03:16 PM
 */
function neto_total() {
    $('#monto-exc').blur(function() {
        update_totales(this);
    });
    $('#porc-rec').blur(function() {
        update_totales(this);
    });
    $('#porc-desc').blur(function() {
        update_totales(this);
    });
}

/**
 * Define el modo de impresion del documento.
 *
 * @param  {string} id_cotizacion   Identificador de la Cotizacion
 * @param  {string} tipo_documento  Datos para la busqueda de datos
 * @param  {boolean} mode_preview   Identifica si la impresion en preview o no.
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  05/09/13 02:52 PM
 */
function print_mode(id_cotizacion, tipo_documento, mode_preview) {
    var membrete = false;
    bootbox.dialog('Impresion del documento ' + tipo_documento, [{
        'label': "Sin Membrete!",
        'class': "btn-danger",
        'callback': function() {
            membrete = false;
            if (mode_preview) {
                preview_pdf(membrete);
            } else {
                cotizacionDocumento(id_cotizacion, tipo_documento, membrete);
            }
        }
    }, {
        'label': "Con Membrete",
        'class': "btn-primary",
        'callback': function() {
            membrete = true;
            if (mode_preview) {
                preview_pdf(membrete);
            } else {
                cotizacionDocumento(id_cotizacion, tipo_documento, membrete);
            }
        }
    }]);
}

/**
 * Calcula el total del backup.
 *
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  05/09/13 02:52 PM
 */
function calculateTotalBackup() {
    var sBase = '#backupTable ';
    var sTBody = sBase + ' tbody ';

    var insercion = new Array();
    var arr_cant = $(sTBody + ':input[name^="cant_backup"]:not([readonly="readonly"]) ');
    $.each(arr_cant, function(i, obj) {
        var cant = $(obj);
        var control = $(sTBody + '#price-backup-' + cant.attr('key'));

        //Si el campo es tipo text obtengo el valor
        //Si control es tipo select-one ontengo el option text
        var item_price = (control.prop('type') == 'text') ? control.val() : control.find('option:selected').val();

        var item_model = $(sTBody + '#model-backup-' + cant.attr('key')).text();
        var item_name = $(sTBody + '#name-backup-' + cant.attr('key')).text();
        var item_cant = $(sTBody + '#cant-backup-' + cant.attr('key')).val();

        insercion [i] = {
            'item_model': item_model,
            'item_name': item_name,
            'item_precio': item_price,
            'item_cant': item_cant
        };

        $('#modelo-' + cant.attr('key')).text(item_model);
        $('#descripcion-' + cant.attr('key')).text(item_name);
        $('#cantidad-' + cant.attr('key')).text(item_cant);
        $('#precio-' + cant.attr('key')).text(money_format(parseFloat(item_price).toFixed(2)));
        $('#neto-' + cant.attr('key')).text(money_format(parseFloat(parseFloat(item_price) * parseInt(item_cant, 10)).toFixed(2)));
    });
    var total = 0;
    for (var i = insercion.length - 1; i >= 0; i--) {
        var tmp = parseFloat(insercion[i].item_precio).toFixed(2) * insercion[i].item_cant;
        total += parseFloat(tmp);
    }
    total = parseFloat(total).toFixed(2);
    $('#total_price-backup').val(money_format(total));
}

/**
 * Cierra el modal de Articulos
 *
 * @author  Jose A. Rodriguez E.
 * @version V- 1.0  22/08/13 14:01
 */
function hide_articles() {
    $('#container-articles').modal('hide');
}

/**
 * Lista los instrumentistas en el modal.
 *
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  03/10/13 08:58 AM
 */
function list_instrumentalist(id_articulacion) {
    $("#instrumentalist-1").empty();
    $('#myTab_instrumentalist li:eq(0) a').tab('show');
    $.ajax({
        url: BASE_URL + 'persona/instrumentista/EXC_diaryInstrumentalist',
        data: {
            id_articulacion: id_articulacion
        },
        success: function(response) {
            if (response.success) {
                var msg = '<center>Elija un Instrumentista en el menu a su izquierda para ver detalles.</center>';
                $("#instrumentalist-1").html(msg);
                $("#instrumentalist-2").html(msg);
                $("#nav_list-instrumentalist").html(response.list);
            }
        }
    });
}

/**
 * Detalle del  instrumentistas en el modal.
 *
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  03/10/13 08:58 AM
 */
function detail_instrumentalist(id_instrumentista, data) {
    $('.list_instrumentalist').removeClass('active');
    $('#list_instrumentalist-' + id_instrumentista).addClass('active');
    $('#myTab_instrumentalist li:eq(0) a').tab('show');
    var table = 'tableAddInstrumentista';
    var sub_array = "{'table':'" + table + "','id':'" + id_instrumentista + "','data':{'nombre':'" + data.datos.nombre + "','cedula':'" + data.datos.cedula + "','correo':'" + data.datos.correo + "'}}";

    $.ajax({
        url: BASE_URL + 'persona/instrumentista/EXC_diaryInstrumentalist',
        data: {
            id_instrumentista: id_instrumentista
        },
        success: function(response) {
            if (response.success) {
                //$("#instrumentalist-1").hide().html(response.detail).slideDown('slow');
                $("#instrumentalist-1").html(response.detail);
                $("#instrumentalist-2").html(response.detail_medico);
                $('#add-instrumentalist').removeClass('hide');
                $('#add-instrumentalist').attr('onclick', 'add_tr_instrumentalist(' + sub_array + ')');
                if (data.datos.disponibilidad == 1) {
                    $('#add-instrumentalist').attr('disabled', false);
                    $('#add-instrumentalist').removeClass('btn-danger');
                    $('#add-instrumentalist').addClass('btn-success');
                    $('#add-instrumentalist').html('Agregar a Cirugia');
                } else {
                    $('#add-instrumentalist').attr('disabled', true);
                    $('#add-instrumentalist').removeClass('btn-success');
                    $('#add-instrumentalist').addClass('btn-danger');
                    $('#add-instrumentalist').html('No Disponible');
                }
            }
        }
    });
}
/**
 * Muestra el modal de instrumentalist
 *
 * @param  {object} e            evento generado en el DOM
 * @param  {string} type         tipo de articulo. Utir para agregar al TR de las tablas
 * @param  {string} table_to_add table al cual se van agregar los articulos selecionados
 * @author  Jose A. Rodriguez E.
 * @version V- 1.0  22/08/13 14:01
 */
function show_instrumentalist(e, id, table_to_add) {
    e.preventDefault();
    id_articulacion = id;
    table_to_add_global = table_to_add;
    $('#container-instrumentalist').modal({
        show: true,
        width: 900
    });
    $("#instrumentalist-1").empty();
    $('#add-instrumentalist').addClass('hide');
    list_instrumentalist(id_articulacion);
//    $('#container-instrumentalist').on('shown', function() {
//        list_instrumentalist(id_articulacion);
//    });
}

/**
 * Agrega los nuevos instrumentistas a la tabla
 *
 * @param  {array} params   Datos de para la insercion de filas de instrumentistas
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  03/10/13 11:05 AM
 */

function add_tr_instrumentalist(params) {
    var tr_table = '#tr-add_instrumentista';
    var bandera_id = 'instrumentista';
    var id_instrumentista = $('#' + params.table + ' #codigo-' + bandera_id + '-' + params.id).text();
    if (params.id == id_instrumentista) {
        bootbox.alert('Ya fue agregado el instrumentista');
        return false;
    } else {
        $('#' + params.table).append('<tr id="tr-add_instrumentista-' + params.id + '">');
        $(tr_table + '-' + params.id).append('<td ><input " id="' + bandera_id + '-' + params.id + '" type="hidden" value="' + params.id + '"  name="id_instrumentista[]" /><i class="icon-remove" onclick="delete_tr(\'tr-add_instrumentista-' + params.id + '\',\'' + bandera_id + '\');"></i></td>');
        $(tr_table + '-' + params.id).append('<td id="codigo-' + bandera_id + '-' + params.id + '">' + params.id + '</td>');
        $(tr_table + '-' + params.id).append('<td id="nombre-' + bandera_id + '-' + params.id + '">' + params.data.nombre + '</td>');
        $(tr_table + '-' + params.id).append('<td id="cedula-' + bandera_id + '-' + params.id + '">' + params.data.cedula + '</td>');
        $(tr_table + '-' + params.id).append('<td id="correo-' + bandera_id + '-' + params.id + '">' + params.data.correo + '</td>');
        $('#tr-' + params.table).append('</tr>');

    }
}

/**
 * Controla la navegacion entre TABS con ID string o personalizad
 *
 * @param  {string} next_tab     Define la tab donde se quiere ir
 * @param  {string} current_tab  Define la tab donde se llama el este evento
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  04/11/13 10:51 AM
 */
function navigate_tab_string(current_tab, next_tab) {

    $("li:has(a[href=#" + next_tab + "]),#" + next_tab).addClass("active");
    $("li:has(a[href=#" + current_tab + "]),#" + current_tab).removeClass("active");

}

/**
 * Verifica la stock de articulos en la creacion de cotizacion
 * y muestra la leyenda de colores de acuerdo a la disponibilidad
 *
 * @param  {integer} id             Identificador del elemento donde se llama la funcion
 * @param  {string} tipo_articulo   Bandera que define el tipo de articulo de la fila (mat,instru,backup)
 * @param  {string} table           Identificador de la tabla padre del elemento
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  13/12/13 04:20 PM
 */
function check_disponibilidad(id, tipo_articulo, table) {
    var sBase = '#' + table;
    var sTBody = sBase + ' tbody ';
    var elemento = $(sTBody + "#" + id).attr('key');
    var cantidad_stock = parseInt($('#disp-' + tipo_articulo + '-' + elemento).val());
    var cant_solicitada = parseInt($(sTBody + '#' + id).val());
    if (cant_solicitada > cantidad_stock) {
        $('#leyenda-' + tipo_articulo + '-' + elemento).css('background-color', 'red');
    } else if (cant_solicitada > 0 && cant_solicitada == cantidad_stock) {
        $('#leyenda-' + tipo_articulo + '-' + elemento).css('background-color', 'yellow');
    } else if (cant_solicitada < cantidad_stock) {
        $('#leyenda-' + tipo_articulo + '-' + elemento).css('background-color', 'blue');
    }
}


/**
 * Agrega los nuevos articulos a la tabla de articulos en corden de despacho libre
 *
 * @param  {array} params           Datos de para la insercion de filas de articulos
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  21/12/13 10:08 AM
 */

function add_tr_delivery_free(params) {
    $('body').modalmanager('loading');
    //Dispara el evento

    $.ajax({
        type: "POST",
        url: BASE_URL + 'delivery/delivery_free/EXC_selectForArticles',
        data: {
            id: params.id
        },
        dataType: 'json',
        success: function(response) {
            $('body').modalmanager('loading');
            //            $('#modal_window').css('width','980');
            var delete_totales = '';
            if (params.data.modelo != $('#model-' + params.tipo + '-' + params.id).text()) {

                $('#' + params.table).append('<tr id="tr-' + params.tipo + '-' + params.id + '">');
                $('#tr-' + params.tipo + '-' + params.id).append('<td ><input class="indicador-' + params.tipo + '" id="' + params.tipo + '-' + params.id + '" type="hidden" value="' + params.id + '" key="' + params.id + '" name="' + params.tipo + '[' + params.id + ']" /><i class="icon-remove" onclick="delete_tr(\'tr-' + params.tipo + '-' + params.id + '\',\'' + params.tipo + '\'); ' + delete_totales + ' "></i></td>');
                $('#tr-' + params.tipo + '-' + params.id).append('<td id="img-' + params.tipo + '-' + params.id + '"><a data-toggle="modal" href="#" onclick="show_img_article(\'' + params.data.modelo + '\',\'' + params.data.descripcion + '\',\'' + params.data.img + '\');"><i class="icon-picture"></i></a></td>');
                $('#tr-' + params.tipo + '-' + params.id).append('<td id="model-' + params.tipo + '-' + params.id + '">' + params.data.modelo + '</td>');

                $('#tr-' + params.tipo + '-' + params.id).append('<td id="name-' + params.tipo + '-' + params.id + '">' + params.data.descripcion + '</td>');

                $('#tr-' + params.tipo + '-' + params.id).append('<td id="lote-' + params.tipo + '-' + params.id + '">' + response.option + '</td>');
                $('#tr-' + params.tipo + '-' + params.id).append('<td id="lote-' + params.tipo + '-' + params.id + '">' + response.option_precio + '</td>');
                $('#tr-' + params.tipo + '-' + params.id).append('<td><input min="1" type="number" id="cant-' + params.tipo + '-' + params.id + '" class="input-mini" name="cant_' + params.tipo + '[' + params.id + ']" placeholder="0" key="' + params.id + '" value="' + params.data.cantidad + '"></td>');
                $('#tr-' + params.tipo + '-' + params.id).append('<input  type="hidden" id="type-' + params.tipo + '-' + params.id + '"  name="type_' + params.tipo + '[' + params.id + ']"   value="' + params.data.type.toLowerCase() + '">');
                $('#tr-' + params.table).append('</tr>');
            } else {
                $('#cant-' + params.tipo + '-' + params.id).val(parseInt($('#cant-' + params.tipo + '-' + params.id).val(), 10) + 1);
            }
        }
    });
}

/**
 * Agrega los nuevos articulos a la tabla de articulos en cotizacion libre
 *
 * @param  {array} params   Datos de para la insercion de filas de articulos
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  21/12/13 10:08 AM
 */

function add_tr_cotizacion_free(params) {
    $('body').modalmanager('loading');
    //Dispara el evento

    $.ajax({
        type: "POST",
        url: BASE_URL + 'delivery/delivery_free/EXC_selectForArticles',
        data: {
            id: params.id
        },
        dataType: 'json',
        success: function(response) {
            $('body').modalmanager('loading');
            //            $('#modal_window').css('width','980');
            var delete_totales = '';
            if (params.data.modelo != $('#model-' + params.tipo + '-' + params.id).text()) {

                $('#' + params.table).append('<tr id="tr-' + params.tipo + '-' + params.id + '">');
                $('#tr-' + params.tipo + '-' + params.id).append('<td ><input class="indicador-' + params.tipo + '" id="' + params.tipo + '-' + params.id + '" type="hidden" value="' + params.id + '" key="' + params.id + '" name="' + params.tipo + '[' + params.id + ']" /><i class="icon-remove" onclick="delete_tr(\'tr-' + params.tipo + '-' + params.id + '\',\'' + params.tipo + '\'); ' + delete_totales + ' "></i></td>');
                $('#tr-' + params.tipo + '-' + params.id).append('<td id="img-' + params.tipo + '-' + params.id + '"><a data-toggle="modal" href="#" onclick="show_img_article(\'' + params.data.modelo + '\',\'' + params.data.descripcion + '\',\'' + params.data.img + '\');"><i class="icon-picture"></i></a></td>');
                $('#tr-' + params.tipo + '-' + params.id).append('<td id="model-' + params.tipo + '-' + params.id + '">' + params.data.modelo + '</td>');

                $('#tr-' + params.tipo + '-' + params.id).append('<td id="name-' + params.tipo + '-' + params.id + '">' + params.data.descripcion + '</td>');

                $('#tr-' + params.tipo + '-' + params.id).append('<td id="lote-' + params.tipo + '-' + params.id + '">' + response.option + '</td>');
                $('#tr-' + params.tipo + '-' + params.id).append('<td id="lote-' + params.tipo + '-' + params.id + '">' + response.option_precio + '</td>');
                $('#tr-' + params.tipo + '-' + params.id).append('<td><input min="1" type="number" id="cant-' + params.tipo + '-' + params.id + '" class="input-mini" name="cant_' + params.tipo + '[' + params.id + ']" placeholder="0" key="' + params.id + '" value="' + params.data.cantidad + '"></td>');
                $('#tr-' + params.tipo + '-' + params.id).append('<input  type="hidden" id="type-' + params.tipo + '-' + params.id + '"  name="type_' + params.tipo + '[' + params.id + ']"   value="' + params.data.type.toLowerCase() + '">');
                $('#tr-' + params.table).append('</tr>');
            } else {
                $('#cant-' + params.tipo + '-' + params.id).val(parseInt($('#cant-' + params.tipo + '-' + params.id).val(), 10) + 1);
            }
        }
    });
}


