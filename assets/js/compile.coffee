<?php

/**
 * To change this template use File | Settings | File Templates.
 */
$opts = getopt('d');

function closure_compile($file) {
    $script = file_get_contents($file);
    $ch = curl_init('http://closure-compiler.appspot.com/compile');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, 'output_info=compiled_code&output_format=text&compilation_level=SIMPLE_OPTIMIZATIONS&js_code=' . urlencode($script));
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}

$PATH = dirname(__FILE__);

foreach (glob("*.js") as $filename) {
    $filename_dest = 'min/' . pathinfo($filename, PATHINFO_FILENAME) . '.min.js';
    $compiled = closure_compile($filename);
    file_put_contents($filename_dest, $compiled);
    sleep(100); //previene el bloqueo por muchas llamadas
}
