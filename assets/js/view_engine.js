//Arreglo de intervalos ejecutados en la aplicacion.
// var intervals = Array();

/**
 * Almacena el objeto tabla segun el target_on.
 * @type {object}
 */
var tabla = null, tabla_window = null, tabla_window2 = null;

//Indica el momento de la ultima actualizazion de evento.
//Por ahora funciona oslo con el calendario
var lastTimeUpdated;

/**
 * @config  configuraciones para las peticiones ajax.
 * @author  Jose A. Rodriguez E.
 * @version V- 1.0  28/11/12 17:08
 */
$.ajaxSetup({
    type: 'POST',
    dataType: 'json',
    error: function(jqXHR, exception) {
        if (jqXHR.status === 0) {
        //alert('Not connect.\n Verify Network.');
        } else if (jqXHR.status == 404) {
            alert('Requested page not found. [404]');
        } else if (jqXHR.status == 500) {
            alert('Internal Server Error [500].');
        } else if (exception === 'parsererror') {
            alert('Requested JSON parse failed.');
        }
        else if (exception === 'timeout') {
            alert('Time out error.');
        } else if (exception === 'abort') {
            alert('Ajax request aborted.');
        } else {
            alert('Uncaught Error.\n' + jqXHR.responseText);
        }
    },
    beforeSend: function() {

    },
    complete: function() {

    }
});


/**
 * Llamada para realizar peticiones al servidor.
 *
 * @param   params    object  JSON con los valores a ser sustituados en el modal.
 * @author  Jose A. Rodriguez E.
 * @version V- 1.0  28/11/12 17:08
 */
function request(params) {

    //Validamos que reques tenga params
    if (params.length <= 0) {
        alert('No ha indicado parametros para la funcion request.');
        return false;
    }

    //Validamos que exista la URL
    if (params.url.length <= 0) {
        alert('URL invalida.');
        return false;
    }

    $.ajax({
        url: params.url,
        data: params.data,
        success: success
    });

    //Si no existe la variable no_remove detenemos el intreval
    if (!params.no_remove)
        removeIntervals();

    // Example call to reload from original file
    return false;
}

/**
 * Luego de una llama ajax exitosa se ejecuta success.
 *
 * @param   response    object  JSON con la respuesta del rquest al servidor.
 * @param   status      string  estatus de la peticion http.
 * @param   jqXHR       object  XMLHTTPRequest
 * @author  Jose A. Rodriguez E.
 * @version V- 1.0  28/11/12 17:08
 */
function success(response, status, jqXHR) {
    switch (response.target_on) {
        case 'out':
            location.href = BASE_URL;
            break;
        case 'window':
            replaceWindow(response);
            break;
        case 'window2':
            replaceWindow(response, 'modal_window2');
            break;
        case 'alert':
            replaceAlert(response);
            break;
        default:
            replaceContent(response);
            break;
    }

    //    if(typeof window.afterSuccess === 'function'){
    //        afterSuccess();
    //    }
    return true;
}


/**
 * Realiza el llamado a modalWindow reemplazando sus componentes por los
 * enviados en el parametro.
 *
 * @param   view    object  JSON con los valores a ser sustituados en el modal.
 * @author  Jose A. Rodriguez E.
 * @version V- 1.0  28/11/12 17:08
 */
function replaceWindow(view, modal) {
    modal = (modal) ? modal : 'modal_window';

    //Seteamos el titulo de la ventana modal.
    $('#' + modal + ' .myModalLabel').html(view.title);

    //Seteamos la vista al modal window
    $('#' + modal + ' .m-body').html(view.view);

    //Mostramos el modal window.
    $('#' + modal).modal({
        show: true
    });
}

/**
 * Reemplaza el contenido del content por lo enviado en json_view.view
 *
 * @param   response    object  JSON con los valores a ser sustituados en el content.
 * @author  Jose A. Rodriguez E.
 * @version V- 1.0  28/11/12 17:08
 */
function replaceContent(response) {
    $('#center-content').html(response.view);
}

/**
 * muestra una ventana de alerta con el resultado de una operacion.
 *
 * @param   response    object  JSON con los valores a ser sustituados en el content.
 * @author  Jose A. Rodriguez E.
 * @version V- 1.0  28/11/12 17:08
 */
function replaceAlert(response) {
    $('.alert').alert(response.view);
}

/**
 * Recarga el grid actualmente visible.
 *
 * @author  Jose A. Rodriguez E.
 * @version V- 1.0  28/11/12 17:08
 */
function reloadGrid() {
    // if($('#modal_window2').length > 0 && tabla_window2)
    //     tabla_window2.fnReloadAjax();
    // else
    if (tabla_window)
        tabla_window.fnReloadAjax();
    if (tabla)
        tabla.fnReloadAjax();
    _hideMassiveButtons();
}

/**
 * Provee la implementacion para borrar datos del grid.
 *
 * @param   params    object  JSON con los valores.
 * @author  Jose A. Rodriguez E.
 * @version V- 1.0  28/11/12 17:08
 */
function _delete(params) {
    bootbox.confirm(
        'Estas seguro de eliminar el/los registro(s) seleccionado(s)?',
        function(result) {
            if (result) {
                $.ajax({
                    url: params.url,
                    data: params.data,
                    success: success,
                    complete: reloadGrid
                });
            }
        });
}

/**
 * Funcion al cual llaman los checkbox de los grids para mostrar las acciones en
 * lotes que se pueden realizar.
 *
 * @param   _this   object  el objeto que llama a la funcion.
 * @author  Jose A. Rodriguez E.
 * @version V- 1.0  15/01/13 10:12
 */
function _checkActions(_this) {
    var hide_actions = $('div[class="form-actions"] a[class*="dynamic-btn"]');
    var checks = $('table[id^="grid_"] input:checkbox[name="id[]"]');
    if (checks.is(':checked'))
        hide_actions.removeClass('hide');
    else
        hide_actions.addClass('hide');
}

/**
 * Hace el llamado a las operaciones en masa.
 *
 * @param   param   object  JSON con la url de la operacion
 * @author  Jose A. Rodriguez E.
 * @version V- 1.0  15/01/13 15:52
 */
function _massiveAction(param) {
    var checks = $('table[id^="grid_"] input:checkbox[name="id[]"]:checked');
    param.data = checks.serialize();
    _delete(param);
}

/**
 * Hace el llamado a las operaciones en masa.
 *
 * @param   param   object  JSON con la url de la operacion
 * @author  Jose A. Rodriguez E.
 * @version V- 1.0  15/01/13 15:52
 */
function _hideMassiveButtons() {
    $('div[class="form-actions"] a[class*="dynamic-btn"]').addClass('hide');
}

/**
 * Formatea la fecha enviada por el calendario fullCalendar
 *
 * @param   date    date    Fecha a ser formateada
 * @param   format  string[optional]  formato al cual sera convertida la fecha default 'yyyy-MM-dd HH:mm'
 * @author  Jose A. Rodriguez E.
 * @version V- 1.0  15/01/13 15:52
 */
function getFormattedDate(date, format) {
    format = format || 'yyyy-MM-dd HH:mm';
    return $.fullCalendar.formatDate(date, format);
}

/**
 * Envia un evento al servidor para actualizado.
 *
 * @param   event   object  Contiene los datos del evento.
 * @param   url     string  url donde se guardara el evento.
 * @author  Jose A. Rodriguez E.
 * @version V- 1.0  15/01/13 15:52
 */
function sendEvent(event, url) {
    data = {
        id: event._id,
        start: getFormattedDate(event.start),
        end: getFormattedDate(event.end),
        process: true
    };
    $.post(url, data, success);
}

function pollForNewEvents(check_url, id_calendar) {
    if (check_url) {
        id_calendar = id_calendar || '#engine-calendar';
        $.get(check_url, {
            q: new Date().getTime()
        }, function(data) {
            data = parseInt(data, 10); // assume data is milliseconds since epoch
            if (data > lastTimeUpdated) {
                $(id_calendar).fullCalendar('refetchEvents');
                lastTimeUpdated = data;
            }
        });
    }
}

/**
 * Remueve los intervalos definifos y que ya no se estan utilizando.
 *
 * @author  Jose A. Rodriguez E.
 * @version V- 1.0  28/01/13 14:01
 */
function removeIntervals() {
    clearInterval(intervals['event_checker']);
}

/**
 * Realiza el llamado a las notificaciones
 *
 * @author  Nohemi Rojas
 * @version V- 1.0  28/01/13 02:54 PM
 */
function notification() {
    $.get('home/EXC_notification', function(response) {
        if (response.cant == 0) {
            $('#notification').hide();
        }
        else {
            var notif = ['<span id="not" class="badge badge-warning">' + response.cant + '</span>'].join('');
            var target = $("#notification");
            target.html(notif);
            $('#notification').show();
            $("#notification-msg").attr("data-content", response.msg);
        }
        setTimeout("notification()", 180000);
    });
}

function customFields() {
    $('.bfh-phone').each(function() {
        var $phone = $(this);
        $phone.bfhphone($phone.data());
    });

    $('input.bootstrap-timepicker-input').each(function() {
        var $timepicker = $(this);
        $timepicker.timepicker({
            minuteStep: 5,
            showMeridian: false
        });
    });

    $('div.bfh-datepicker-toggle').each(function() {
        var $datepicker = $(this);
        $datepicker.datepicker();
        $datepicker.on('changeDate', function() {
            $datepicker.datepicker('hide');
        });
    });

    $('div.date').each(function() {
        var $picker = $(this);
        $picker.datepicker({
            weekStart: 1
        });
        $picker.on('changeDate', function() {
            $picker.datepicker('hide');
        });
    });

    $(".chzn-select").chosen({
        allow_single_deselect: true
    });
    //tooltips
    $('.input-xlarge').popover({
        placement: 'right',
        trigger: 'hover'
    });
    $('.input-large').popover({
        placement: 'right',
        trigger: 'hover'
    });
    $('.input-date').popover({
        placement: 'right',
        trigger: 'hover'
    });

    $('.checkbox').popover({
        placement: 'right',
        trigger: 'hover'
    });

    $('.input-medium').popover({
        placement: 'right',
        trigger: 'hover'
    });
    $(".chzn-select").chosen().each(function() {
        var dropDown = $(this);
        var _chosen = dropDown.siblings("div");

        //uso los atributos del select para armar el popover
        _chosen.popover({
            title: dropDown.attr('title'),
            content: dropDown.data('content'),
            trigger: 'hover'
        });
    });
}

function formWindow(form, url, modal, _alert, reload_content, box) {
    //Ejecuta las Validaciones del Cliente
    new FormValidator(form, buildRules(form),
        function(errors, event) {
            var flag = validateForm(errors, event);

            if (event && event.preventDefault) {
                event.preventDefault();
            } else if (event) {
                event.returnValue = false;
            }
            if (flag) {
                   
                var $modal = $(modal);
                $modal.modalmanager('loading');
                $(modal + ' .modal-backdrop').css('z-index', '1500');
                $(modal + ' .modal-scrollable').css('z-index', '1510');
                $(modal + ' .loading-spinner').css('z-index', '1510');
                $.ajax({
                    url: url,
                    data: $('#' + form).serialize(),
                    success: function(response) {
                        $modal.modalmanager('loading');
                        var textAlert = '', alertHtml = '';
                        //Se procesa cuando el registro se guarda exitosamente
                        if (response.view == 1) {
                            if (typeof window.afterSuccess === 'function') {
                                afterSuccess(response);
                            }
                            if (typeof response.msg != "undefined") {
                                textAlert = response.msg;
                            } else {
                                textAlert = 'Datos guardados satisfactoriamente';
                            }
                            alertHtml = [
                            '<div class="modal-body"><div class="alert alert-success fade in">',
                            '<button type="button" class="close" data-dismiss="alert">×</button>',
                            textAlert,
                            '</div></div>'
                            ].join('');

                            if (reload_content) {
                                request({
                                    url: reload_content
                                });
                            } else {
                                reloadGrid();
                            }
                            $("#" + form).hide();
                            if ($('#modal_window2').attr('aria-hidden') == 'false') {
                                setTimeout(function() {
                                    $('#modal_window2').modal('hide');
                                }, 1500);
                            }
                            else {
                                setTimeout(function() {
                                    $('#modal_window').modal('hide');
                                }, 1500);
                            }
                        }

                        //Se procesa cuando no se puede guardar el registro
                        else {
                            //Ocurre cuando hay un error en Base de Datos
                            if (response.view == false) {
                                if (typeof response.msg != "undefined") {
                                    textAlert = response.msg;
                                } else {
                                    textAlert = 'No se pudo guardar el registro, si el problema persiste comuniquese con sistemas';
                                }

                            //Ocurre cuando hay errores de validacion en el Servidor
                            } else {
                                textAlert = response.view;
                            }
                            //Mensaje de Alerta Error
                            alertHtml = [
                            '<div class="modal-body"><div class="alert alert-error fade in">',
                            '<button type="button" class="close" data-dismiss="alert">×</button>',
                            textAlert,
                            '</div></div>'
                            ].join('');

                        }
                        //Elemento que muestra el Alert
                        var target = $(_alert);
                        if (box) {
                            bootbox.alert(alertHtml);
                        } else {
                            target.html(alertHtml);
                        }
                    }
                }
                );
            }
        }
        );
}

/**
 * Llena el campo hidden de los checklist. View Engine se encarga de llenar los valores de los parametros.
 * @param  {string} checklist_class clase los campos checklist
 * @param  {string} hidden_id       ID del campo hidden a llenarse
 * @author Jose Rodriguez
 * @version 1.0 27/09/2013 15:45:00
 */
function checklist(checklist_class, hidden_id) {
    checklist_class = '.' + checklist_class;
    $(checklist_class).change(function(e) {
        var arr = [];
        $(checklist_class + ':checked').each(function(i, el) {
            arr.push(el.value);
        });
        $('#' + hidden_id).val(arr.join());
        e.preventDefault();
    });
}

$(function() {
    setInterval(function() {
        $('#not').effect("shake", {}, 500);
    }, 7000);
}
);
// setTimeout("notification()", 10000);


/**
 * Provee la informacion del navegador el cual se encuentra el cliente
 * @author Frederick D. Bustamante G <frederickdanielb@gmail.com>
 * @version 1.0 27/09/2013 15:45:00
 */
function getUserAgent() {
    var navegador = navigator.userAgent;
    if (navigator.userAgent.indexOf('MSIE') != -1) {
        bootbox.alert('Usted está usando Internet Explorer, la aplicacion puede presentar fallos debido al navegador.');
    } else if (navigator.userAgent.indexOf('Firefox') != -1) {
    //        bootbox.alert('Usted está usando Firefox ...');

    } else if (navigator.userAgent.indexOf('Chrome') != -1) {
    //        bootbox.alert('Usted está usando Google Chrome ...');

    } else if (navigator.userAgent.indexOf('Opera') != -1) {
    //        bootbox.alert('Usted está usando Opera ...');

    } else {
    //        bootbox.alert('Usted está usando un navegador no identificado ..');
    }
}
function limpiar(value) {
    var field = '#' + value + '';
    var val = $(field).val();
    var b = $.trim(val);
    $(field).val(b);
    return true
}


/**
 * Muestra el modal de la imagen de ayuda
 *
 * @param  {string} mod  Modelo del Articulo
 * @param  {string} des  Descripcion del Articulo
 * @param  {string} img  Nombre de la imagen asociada al articulo
 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
 * @version V- 1.0  26/08/13 05:33 PM
 */
function show_img_help(des, img) {
    //Seteamos el titulo de la ventana modal.
    $('#helpModalLabel').text('Módulo: ' + des);

    //Seteamos la vista al modal window
    var src = img;
    var alt = ' Descripción: ' + des;
    //Elemento img donde seran actualizados los valorres del material actual.
    var eImg = $('#selected-help-image');
    eImg.attr('src', src);
    eImg.attr('alt', alt);
    // eImg.attr('title', 'Modelo: ' + mod);

    // $('#selected-help-description').text(des);

    //Mostramos el modal.
    $('#helpModal').modal('show');
    return false;
}