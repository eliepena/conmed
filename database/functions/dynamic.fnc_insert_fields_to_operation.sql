-- Author: Eliel Parra <elielparra@gmail.com>
-- Last Modified: 23/11/12
-- Name: Procedimiento que inserta los campos de la entidad seleccionada en la operacion seleccionada

-------------------
-- DROP FUNCTION
-------------------

-- DROP FUNCTION dynamic.fnc_insert_fields_to_operation(operation integer,entity integer) CASCADE;

-------------------
-- FUNCTION
-------------------

CREATE OR REPLACE FUNCTION dynamic.fnc_insert_fields_to_operation(operation integer,entity integer) RETURNS boolean AS $$
DECLARE

BEGIN

	INSERT INTO acl.field_operation(id_operation, id_field, _label)
	SELECT  $1, fie.id, fie._label
	FROM dynamic.field AS fie
	JOIN dynamic.entity AS ent ON ent.id = fie.id_entity
	WHERE ent.id = $2
	ORDER BY fie.id;

RETURN TRUE;
END;
$$ LANGUAGE plpgsql;