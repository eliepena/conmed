-- Author: Eliel Parra <elielparra@gmail.com>
-- Last Modified: 23/11/12
-- Name: Procedimiento que inserta los campos asignados a una operacion en otra operacion seleccionada

-------------------
-- DROP FUNCTION
-------------------

-- DROP FUNCTION dynamic.fnc_clone_operation_fields(origen integer,destino integer) CASCADE;

-------------------
-- FUNCTION
-------------------

CREATE OR REPLACE FUNCTION dynamic.fnc_clone_operation_fields(origen integer,destino integer) RETURNS boolean AS $$
DECLARE

BEGIN

	INSERT INTO acl.field_operation(id_operation, id_field, _label, help, tooltip, _order, validations, placeholder, disabled, read_only, hidden, chk_component_type)
	SELECT  $2, fop.id_field, fop._label, fop.help, fop.tooltip, fop._order, fop.validations, fop.placeholder, fop.disabled, fop.read_only, fop.hidden, fop.chk_component_type
	FROM acl.field_operation AS fop
	WHERE fop.id_operation = $1;

RETURN TRUE;
END;
$$ LANGUAGE plpgsql;