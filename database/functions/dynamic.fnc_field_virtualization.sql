-- Author: Eliel Parra <elielparra@gmail.com>
-- Last Modified: 22/11/12
-- Name: Procedimiento que virtualiza los campos del esquema seleccionado

-------------------
-- DROP FUNCTION
-------------------

-- DROP FUNCTION dynamic.fnc_field_virtualization(varchar) CASCADE;

-------------------
-- FUNCTION
-------------------

CREATE OR REPLACE FUNCTION dynamic.fnc_field_virtualization(varchar) RETURNS boolean AS $$
DECLARE

BEGIN
--Borrar las campos que ya no estan en el esquema
	DELETE FROM dynamic.field
	WHERE id IN(
	SELECT fie.id
	FROM dynamic.field fie
	JOIN dynamic.entity ent ON ent.id = fie.id_entity
	WHERE ent._schema = $1
	AND ent._name||'.'||fie._name NOT IN(SELECT table_name||'.'||column_name as nombre 
										FROM information_schema.columns 
										WHERE table_schema = $1));
--Insertar nuevos campos
	INSERT INTO dynamic.field (_name, id_entity, length, chk_data_type) 
	SELECT col.column_name, ent.id, col.character_maximum_length, col.data_type
	FROM information_schema.columns col
	JOIN dynamic.entity ent ON ent._name = col.table_name 
	WHERE col.table_schema = $1
	AND col.table_name||'.'||col.column_name NOT IN(SELECT ent._name||'.'||fie._name 
													FROM dynamic.field fie
													JOIN dynamic.entity ent ON ent.id = fie.id_entity
													WHERE _schema = $1);
RETURN TRUE;
END;
$$ LANGUAGE plpgsql;