-- Author: Eliel Parra <elielparra@gmail.com>
-- Last Modified: 22/11/12
-- Name: Procedimiento que virtualiza las entidades del esquema seleccionado

-------------------
-- DROP FUNCTION
-------------------

-- DROP FUNCTION dynamic.fnc_entity_virtualization(varchar) CASCADE;

-------------------
-- FUNCTION
-------------------

CREATE OR REPLACE FUNCTION dynamic.fnc_entity_virtualization(varchar) RETURNS boolean AS $$
DECLARE

BEGIN
--Borrar las entidades que ya no estan en el esquema
DELETE FROM dynamic.entity 
WHERE _schema = $1
AND _name IN (	SELECT _name
				FROM dynamic.entity
				WHERE _schema = $1
				AND _name NOT IN(SELECT table_name FROM information_schema.tables WHERE table_schema = $1));

--Insertar nuevas entidades
	INSERT INTO dynamic.entity (_name,_schema) 
	SELECT table_name AS _name, table_schema AS _schema 
	FROM information_schema.tables
	WHERE table_schema = $1
	AND table_name NOT IN(SELECT _name FROM dynamic.entity WHERE _schema = $1);
RETURN TRUE;
END;
$$ LANGUAGE plpgsql;