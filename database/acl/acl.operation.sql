SET search_path = acl,pg_catalog;
CREATE TABLE  operation(
--Campos Basicos
    id serial NOT NULL,
--Campos propios
    _name character varying(150) NOT NULL,
    url text NOT NULL,
    _order integer DEFAULT NULL,
    visible bit(1) DEFAULT B'1'::"bit",
    icon character varying(50) DEFAULT NULL,
    help text DEFAULT NULL,
    tooltip text DEFAULT NULL,
--Campos FK
    id_operation integer DEFAULT NULL,
    cat_render_on integer NOT NULL,
    cat_visual_type integer NOT NULL
);
--CONSTRAINTS
    --PK
    ALTER TABLE operation ADD CONSTRAINT pk_operation PRIMARY KEY(id);
    --FK
    ALTER TABLE ONLY operation ADD CONSTRAINT fk_ope_operation FOREIGN KEY (id_operation) REFERENCES operation(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY operation ADD CONSTRAINT fk_ope_cat_render_on FOREIGN KEY (cat_render_on) REFERENCES dynamic.category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY operation ADD CONSTRAINT fk_ope_cat_visual_type FOREIGN KEY (cat_visual_type) REFERENCES dynamic.category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE operation IS 'Entidad que contiene las operaciones del sistema';
    --Campos basicos
    COMMENT ON COLUMN operation.id IS 'Identificador del registro';
    --Campos propios
    COMMENT ON COLUMN operation._name IS 'Nombre visible de la operacion, se utiliza en la etiqueta del boton o en el tooltip del icono';
    COMMENT ON COLUMN operation.url IS 'URL con la accion de la operacion';
    COMMENT ON COLUMN operation._order IS 'Numero que define el orden de las operaciones';
    COMMENT ON COLUMN operation.visible IS 'Define si la operacion es visible o no';
    COMMENT ON COLUMN operation.icon IS 'Ruta del icono de la operacion';
    COMMENT ON COLUMN operation.help IS 'Ayuda del campo, es usada en la documentacion';
    COMMENT ON COLUMN operation.tooltip IS 'Ayuda del campo, es usada en el tooltip';
    --Campos FK
    COMMENT ON COLUMN operation.id_operation IS 'Identificador de la operacion|';
    COMMENT ON COLUMN operation.cat_render_on IS 'Identificador de la categoria que define el lugar donde se muestra la operacion';
    COMMENT ON COLUMN operation.cat_visual_type IS 'Identificador de la categoria que define el formato visual como se mostrara la operacion';