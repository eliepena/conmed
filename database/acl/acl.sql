CREATE SCHEMA acl AUTHORIZATION postgres;

GRANT ALL ON SCHEMA acl TO postgres;

COMMENT ON SCHEMA acl IS 'Esquema que contiene las tablas de autenticacion, manejo de operaciones y registro de logs';

SET search_path = acl,pg_catalog;
