SET search_path = acl,pg_catalog;
CREATE TABLE  group_operation(
--Campos Basicos
    
--Campos propios
    _date date DEFAULT now(),
--Campos FK
    id_operation integer NOT NULL,
    id_group integer NOT NULL
);
--CONSTRAINTS
    --PK
    ALTER TABLE group_operation ADD CONSTRAINT pk_group_operation PRIMARY KEY(id_operation,id_group);
    --FK
    ALTER TABLE ONLY group_operation ADD CONSTRAINT fk_gro_ope_operation FOREIGN KEY (id_operation) REFERENCES operation(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY group_operation ADD CONSTRAINT fk_gro_ope_group FOREIGN KEY (id_group) REFERENCES groups(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE group_operation IS 'Entidad que contiene las operaciones asignadas a los grupos de usuarios del sistema';
    --Campos basicos
    
    --Campos propios
    COMMENT ON COLUMN group_operation._date IS 'Fecha de creacion de la asignacion de la operacion al grupo';
    --Campos FK
    COMMENT ON COLUMN group_operation.id_operation IS 'Identificador de la operacion';
    COMMENT ON COLUMN group_operation.id_group IS 'Identificador del grupo';