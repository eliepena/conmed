SET search_path = acl,pg_catalog;
CREATE TABLE  field_operation(
--Campos Basicos
    id serial NOT NULL,
--Campos propios
    _label character varying(150) DEFAULT NULL,
    help text DEFAULT NULL,
    tooltip text DEFAULT NULL,
    _order integer DEFAULT NULL,
    validations text DEFAULT NULL,      
    placeholder character varying(100) DEFAULT NULL,    
    disabled bit(1) DEFAULT B'0'::"bit",
    read_only bit(1) DEFAULT B'0'::"bit",
    hidden bit(1) DEFAULT B'0'::"bit",    
--Campos FK
    id_operation integer NOT NULL,
    id_field integer NOT NULL,
    cat_component_type integer NOT NULL
);
--CONSTRAINTS
    --PK
    ALTER TABLE field_operation ADD CONSTRAINT pk_field_operation PRIMARY KEY(id_operation,id_field);
    --FK
    ALTER TABLE ONLY field_operation ADD CONSTRAINT fk_fie_ope_operation FOREIGN KEY (id_operation) REFERENCES operation(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY field_operation ADD CONSTRAINT fk_fie_ope_field FOREIGN KEY (id_field) REFERENCES dynamic.field(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY field_operation ADD CONSTRAINT fk_fie_ope_cat_component_type FOREIGN KEY (cat_component_type) REFERENCES dynamic.category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE field_operation IS 'Entidad que que relaciona las operaciones y los campos, se utilza para construir las vistas de forma dinamica';
    --Campos basicos
    COMMENT ON COLUMN field_operation.id IS 'Identificador del registro';
    --Campos propios
    COMMENT ON COLUMN field_operation._label IS 'Nombre del campos, sobreescribe el nombre del campo de la tabla field';
    COMMENT ON COLUMN field_operation.help IS 'Ayuda del campo del tipo documentacion, sobreescribe la ayuda del campo de la tabla fields';
    COMMENT ON COLUMN field_operation.tooltip IS 'Ayuda del campo del tipo tooltip, sobreescribe la ayuda del campo de la tabla fields';
    COMMENT ON COLUMN field_operation._order IS 'Numero que permite ordenar los campos dentro de la operacion';
    COMMENT ON COLUMN field_operation.validations IS 'Validaciones del campo, sobreescribe las validaciones del campo de la tabla fields';
    COMMENT ON COLUMN field_operation.placeholder IS 'Texto que se muestra por defecto dentro del campo';
    COMMENT ON COLUMN field_operation.disabled IS 'Define si el campo esta activo o no';
    COMMENT ON COLUMN field_operation.read_only IS 'Define si el campo puede ser editado o no';
    COMMENT ON COLUMN field_operation.hidden IS 'Define si el campo esta oculto o no';    
    --Campos FK
    COMMENT ON COLUMN field_operation.id_operation IS 'Identificador de la operacion';
    COMMENT ON COLUMN field_operation.id_field IS 'Identificador del campo';
    COMMENT ON COLUMN field_operation.cat_component_type IS 'Identificador de la categoria del tipo de componente. Ejemplo: text, select, textarea';