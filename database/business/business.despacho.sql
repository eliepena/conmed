SET search_path = business,pg_catalog;
CREATE TABLE  despacho(
--Campos Basicos
    id bigint NOT NULL,
--Campos propios
    fecha timestamp NOT NULL,
--Campos FK
    id_orden_despacho integer NOT NULL,
    id_tansporte integer DEFAULT NULL,
    cat_compania_tansporte integer DEFAULT NULL
)INHERITS(super_table);
--CONSTRAINTS
    --PK
    ALTER TABLE despacho ADD CONSTRAINT pk_despacho PRIMARY KEY(id);
    --FK
    ALTER TABLE ONLY despacho ADD CONSTRAINT fk_des_orden_despacho FOREIGN KEY (id_orden_despacho) REFERENCES orden_despacho(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY despacho ADD CONSTRAINT fk_des_transporte FOREIGN KEY (id_tansporte) REFERENCES empleado(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY despacho ADD CONSTRAINT fk_des_compania_transporte FOREIGN KEY (cat_compania_tansporte) REFERENCES dynamic.category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE despacho IS 'Entidad que contiene los despachos, tambien conocidos como maletas de despacho';
    --Campos basicos
    COMMENT ON COLUMN despacho.id IS 'Identificador del registro';
    --Campos propios
    COMMENT ON COLUMN despacho.fecha IS 'Fecha de creacion del despacho';
    --Campos FK
    COMMENT ON COLUMN despacho.id_orden_despacho IS 'Identificador de la orden de despacho que genera este despacho';
    COMMENT ON COLUMN despacho.id_tansporte IS 'Identificador del empleado de transporte que lleva el despacho, en caso de que aplique';
    COMMENT ON COLUMN despacho.id_tansporte IS 'Identificador de la categoría de transporte que lleva el despacho, en caso de que aplique';