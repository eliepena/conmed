SET search_path = business,pg_catalog;
CREATE TABLE  informe(
--Campos Basicos
    id bigint NOT NULL,
--Campos propios
    fecha timestamp NOT NULL,
    archivo text DEFAULT NULL,
--Campos FK
    id_empleado integer DEFAULT NULL,
    id_medico integer DEFAULT NULL,
    cat_tipo_informe integer NOT NULL
)INHERITS(super_table);
--CONSTRAINTS
    --PK
    ALTER TABLE informe ADD CONSTRAINT pk_informe PRIMARY KEY(id);
    --FK
    ALTER TABLE ONLY informe ADD CONSTRAINT fk_inf_empleado FOREIGN KEY (id_empleado) REFERENCES empleado(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY informe ADD CONSTRAINT fk_inf_medico FOREIGN KEY (id_medico) REFERENCES medico(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY informe ADD CONSTRAINT fk_inf_cat_tipo_informe FOREIGN KEY (cat_tipo_informe) REFERENCES dynamic.category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE informe IS 'Entidad que contiene las informes';
    --Campos basicos
    COMMENT ON COLUMN informe.id IS 'Identificador del registro';
    --Campos propios
    COMMENT ON COLUMN informe.fecha IS 'Fecha de elaboracion del informe';
    COMMENT ON COLUMN informe.archivo IS 'Ruta del archivo de informe relacionado';
    --Campos FK
    COMMENT ON COLUMN informe.id_empleado IS 'Identificador del empleado que elabora el informe';
    COMMENT ON COLUMN informe.id_medico IS 'Identificador del medico que elabora el informe';
    COMMENT ON COLUMN informe.cat_tipo_informe IS 'Identificador de la categoria de tipo de informe';