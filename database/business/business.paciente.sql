SET search_path = business,pg_catalog;
CREATE TABLE  paciente(
--Campos Basicos
    id bigint NOT NULL,
--Campos propios

--Campos FK
    id_persona integer NOT NULL,
    cat_tipo_sangre integer DEFAULT NULL
)INHERITS(persona);
--CONSTRAINTS
    --PK
    ALTER TABLE paciente ADD CONSTRAINT pk_paciente PRIMARY KEY(id );
    --FK
    ALTER TABLE ONLY paciente ADD CONSTRAINT fk_pac_persona FOREIGN KEY (id_persona) REFERENCES persona(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY paciente ADD CONSTRAINT fk_pac_cat_tipo_sangre FOREIGN KEY (cat_tipo_sangre) REFERENCES dynamic.category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE paciente IS 'Entidad que contiene las pacientees';
    --Campos basicos
    COMMENT ON COLUMN paciente.id IS 'Identificador del registro';
    --Campos propios

    --Campos FK
    COMMENT ON COLUMN paciente.id_persona IS  'Identificador del registro de superclase persona asociado al paciente';
    COMMENT ON COLUMN paciente.cat_tipo_sangre IS  'Identificador de la categoria de tipo de sangre';