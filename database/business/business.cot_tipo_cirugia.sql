SET search_path = business,pg_catalog;
CREATE TABLE  cot_tipo_cirugia(
--Campos Basicos
    id bigserial NOT NULL,
    id_ec character varying(50) NOT NULL,
    codigo character varying(10) NOT NULL,  
    reg_cre_u integer NOT NULL,
    reg_cre_f timestamp DEFAULT now() NOT NULL,
    reg_mod_u integer,
    reg_mod_f timestamp,
    reg_eli_u integer,
    reg_eli_f timestamp,
    reg_eli bit(1) DEFAULT B'0'::"bit" NOT NULL,
--Campos propios

--Campos FK
    id_cotizacion integer NOT NULL,
    cat_tipo_cirugia integer NOT NULL
);
--CONSTRAINTS
    --PK
    ALTER TABLE cot_tipo_cirugia ADD CONSTRAINT pk_cot_tipo_cirugia PRIMARY KEY(id);
    --FK
    ALTER TABLE ONLY cot_tipo_cirugia ADD CONSTRAINT fk_cot_tip_cir_cotizacion FOREIGN KEY (id_cotizacion) REFERENCES cotizacion(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY cot_tipo_cirugia ADD CONSTRAINT fk_cot_tip_cir_cat_tipo_cirugia FOREIGN KEY (cat_tipo_cirugia) REFERENCES dynamic.category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE cot_tipo_cirugia IS 'Entidad que contiene los tipos de cirugia que se pueden asignar a una cotizacion, se usa para sugerir materiales, instrumentos e instrumentista';
    --Campos basicos
    COMMENT ON COLUMN cot_tipo_cirugia.id IS 'Identificador del registro';
    COMMENT ON COLUMN cot_tipo_cirugia.id_ec IS 'Identicador encriptado del registro';
    COMMENT ON COLUMN cot_tipo_cirugia.codigo IS 'Codigo del registro visible al usuario';
    COMMENT ON COLUMN cot_tipo_cirugia.reg_cre_u IS 'Identificador del usuario que crea el registro';
    COMMENT ON COLUMN cot_tipo_cirugia.reg_cre_f IS 'Fecha de creacion del registro';
    COMMENT ON COLUMN cot_tipo_cirugia.reg_mod_u IS 'Identificador del usuario que modifica el registro';
    COMMENT ON COLUMN cot_tipo_cirugia.reg_mod_f IS 'Fecha de modificacion del registro';
    COMMENT ON COLUMN cot_tipo_cirugia.reg_eli_u IS 'Identificador del usuario que elimina el registro';
    COMMENT ON COLUMN cot_tipo_cirugia.reg_eli_f IS 'Fecha de eliminacion del registro';
    COMMENT ON COLUMN cot_tipo_cirugia.reg_eli IS  'Identifica si el registro esta o no eliminado: 1 - Eliminado, 0 - No eliminado';
    --Campos propios

    --Campos FK
    COMMENT ON COLUMN cot_tipo_cirugia.id_cotizacion IS 'Identificador de la cotizacion';
    COMMENT ON COLUMN cot_tipo_cirugia.cat_tipo_cirugia IS 'Identificador de la categoria de tipo de cirugia';