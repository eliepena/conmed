SET search_path = business,pg_catalog;
CREATE TABLE  empleado(
--Campos basicos
    id bigint NOT NULL,
--Campos propios
    fecha_ingreso date DEFAULT NULL,
--Campos FK
    id_persona integer NOT NULL,
    id_usuario integer DEFAULT NULL,
    cat_tipo_empleado integer NOT NULL,
    cat_nivel_educativo integer NOT NULL
)INHERITS(persona);
--CONSTRAINTS
    --PK
    ALTER TABLE empleado ADD CONSTRAINT pk_empleado PRIMARY KEY(id);
    --FK
    ALTER TABLE ONLY empleado ADD CONSTRAINT fk_emp_persona FOREIGN KEY (id_persona) REFERENCES persona(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY empleado ADD CONSTRAINT fk_emp_usuario FOREIGN KEY (id_usuario) REFERENCES acl.users(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY empleado ADD CONSTRAINT fk_emp_cat_tipo_empleado FOREIGN KEY (cat_tipo_empleado) REFERENCES dynamic.category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY empleado ADD CONSTRAINT fk_emp_cat_nivel_educativo FOREIGN KEY (cat_nivel_educativo) REFERENCES dynamic.category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE empleado IS 'Entidad que contiene las empleados';
    --Campos basicos
    COMMENT ON COLUMN empleado.id IS 'Identificador del registro';
    --Campos propios
    COMMENT ON COLUMN empleado.fecha_ingreso IS 'Fecha de ingreso del empleado a la compania';
    --Campos FK
    COMMENT ON COLUMN empleado.id_persona IS 'Identificador del registro de superclase persona asociado al empleado';
    COMMENT ON COLUMN empleado.id_usuario IS 'Identificador del usuario asociado al empleado';
    COMMENT ON COLUMN empleado.cat_tipo_empleado IS 'Identificador de la categoria de tipo de empleado';
    COMMENT ON COLUMN empleado.cat_nivel_educativo IS 'Identificador de la categoria de nivel educativo';