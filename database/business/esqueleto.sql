CREATE TABLE  nametable(
--Campos Basicos
    id integer NOT NULL,
--Campos propios

--Campos FK
    id_campo integer,
)INHERITS(super_table);
--CONSTRAINTS
    --PK
    ALTER TABLE nametable ADD CONSTRAINT pk_nametable PRIMARY KEY(id);
    --FK
    ALTER TABLE ONLY nametable ADD CONSTRAINT fk_nametable_field FOREIGN KEY (id_fk_field) REFERENCES fk_table(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE nametable IS 'Entidad que contiene las nametablees';
    --Campos basicos
    COMMENT ON COLUMN nametable.id IS 'Identificador del registro';
    --Campos propios
    COMMENT ON COLUMN nametable.field IS 'Comment';
    --Campos FK
    COMMENT ON COLUMN nametable.id_fk_field IS 'Comment';