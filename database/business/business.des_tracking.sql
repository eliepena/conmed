SET search_path = business,pg_catalog;
CREATE TABLE  des_tracking(
--Campos Basicos
    id bigserial NOT NULL,
    id_ec character varying(50) NOT NULL,
    codigo character varying(10) NOT NULL,  
    reg_cre_u integer NOT NULL,
    reg_cre_f timestamp DEFAULT now() NOT NULL,
    reg_mod_u integer,
    reg_mod_f timestamp,
    reg_eli_u integer,
    reg_eli_f timestamp,
    reg_eli bit(1) DEFAULT B'0'::"bit" NOT NULL,
--Campos propios
    fecha_inicio timestamp NOT NULL,
    fecha_fin timestamp DEFAULT NULL,
    observaciones text DEFAULT NULL,
--Campos FK
    id_despacho integer NOT NULL,
    id_empleado integer NOT NULL,
    cat_estatus_despacho integer NOT NULL
);
--CONSTRAINTS
    --PK
    ALTER TABLE des_tracking ADD CONSTRAINT pk_des_tracking PRIMARY KEY(id);
    --FK
    ALTER TABLE ONLY des_tracking ADD CONSTRAINT fk_des_tra_despacho FOREIGN KEY (id_despacho) REFERENCES despacho(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY des_tracking ADD CONSTRAINT fk_des_tra_empleado FOREIGN KEY (id_empleado) REFERENCES empleado(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY des_tracking ADD CONSTRAINT fk_des_tra_cat_estatus_despacho FOREIGN KEY (cat_estatus_despacho) REFERENCES dynamic.category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE des_tracking IS 'Entidad que contiene las des_trackinges';
    --Campos basicos
    COMMENT ON COLUMN des_tracking.id IS 'Identificador del registro';
    COMMENT ON COLUMN des_tracking.id_ec IS 'Identicador encriptado del registro';
    COMMENT ON COLUMN des_tracking.codigo IS 'Codigo del registro visible al usuario';
    COMMENT ON COLUMN des_tracking.reg_cre_u IS 'Identificador del usuario que crea el registro';
    COMMENT ON COLUMN des_tracking.reg_cre_f IS 'Fecha de creacion del registro';
    COMMENT ON COLUMN des_tracking.reg_mod_u IS 'Identificador del usuario que modifica el registro';
    COMMENT ON COLUMN des_tracking.reg_mod_f IS 'Fecha de modificacion del registro';
    COMMENT ON COLUMN des_tracking.reg_eli_u IS 'Identificador del usuario que elimina el registro';
    COMMENT ON COLUMN des_tracking.reg_eli_f IS 'Fecha de eliminacion del registro';
    COMMENT ON COLUMN des_tracking.reg_eli IS  'Identifica si el registro esta o no eliminado: 1 - Eliminado, 0 - No eliminado';
    --Campos propios
    COMMENT ON COLUMN des_tracking.fecha_inicio IS 'Fecha de inicio del cambio de estatus';
    COMMENT ON COLUMN des_tracking.fecha_fin IS 'Fecha de fin del cambio de estatus';
    COMMENT ON COLUMN des_tracking.observaciones IS 'Observaciones relacionadas con el cambio de estatus';
    --Campos FK
    COMMENT ON COLUMN des_tracking.id_despacho IS 'Identificador del despacho asociado al tracking';
    COMMENT ON COLUMN des_tracking.id_empleado IS 'Identificador del empleado que ejecuta el cambio de estatus del despacho';
    COMMENT ON COLUMN des_tracking.cat_estatus_despacho IS 'Identificador de la categoria del estatus de despacho';