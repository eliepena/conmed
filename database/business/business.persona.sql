SET search_path = business,pg_catalog;
CREATE TABLE persona(
--Campos Basicos
    id bigint NOT NULL,
--Campos propios
   letra character varying(1) NOT NULL CHECK(letra IN('V','E','P')),
   cedula character varying(10) DEFAULT NULL,
   primer_nombre character varying(25) NOT NULL,
   segundo_nombre character varying(25) DEFAULT NULL,
   primer_apellido character varying(25) NOT NULL,
   segundo_apellido character varying(25) DEFAULT NULL,
   sexo character varying(25) NOT NULL CHECK(sexo IN('Femenino','Masculino')),
   fecha_nacimiento date DEFAULT NULL      
)INHERITS(super_table);
--CONSTRAINTS
  --PK
  ALTER TABLE persona ADD CONSTRAINT pk_persona PRIMARY KEY(id);
  --FK

--COMMENTS
  --Tabla
  COMMENT ON TABLE persona IS 'Entidad que contiene las personas';
  --Campos basicos
  COMMENT ON COLUMN persona.id IS 'Identificador del registro';
  --Campos propios
  COMMENT ON COLUMN persona.letra IS  'Letra que define el tipo de identificacion P-V-E';
  COMMENT ON COLUMN persona.cedula IS  'Numero de cedula de la persona';
  COMMENT ON COLUMN persona.primer_nombre IS  'Primer Nombre de la persona';
  COMMENT ON COLUMN persona.segundo_nombre IS  'Segundo Nombre la persona';
  COMMENT ON COLUMN persona.primer_apellido IS  'Primer Apellido la persona';
  COMMENT ON COLUMN persona.segundo_apellido IS  'Segundo Apellido la persona';
  COMMENT ON COLUMN persona.sexo IS  'Sexo la persona';
  COMMENT ON COLUMN persona.fecha_nacimiento IS  'Fecha de Nacimiento la persona';