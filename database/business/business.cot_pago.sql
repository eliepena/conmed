SET search_path = business,pg_catalog;
CREATE TABLE  cot_pago(
--Campos Basicos
    id bigserial NOT NULL,
    id_ec character varying(50) NOT NULL,
    codigo character varying(10) NOT NULL,  
    reg_cre_u integer NOT NULL,
    reg_cre_f timestamp DEFAULT now() NOT NULL,
    reg_mod_u integer,
    reg_mod_f timestamp,
    reg_eli_u integer,
    reg_eli_f timestamp,
    reg_eli bit(1) DEFAULT B'0'::"bit" NOT NULL,
--Campos propios
    fecha date NOT NULL,
    monto character varying(25) NOT NULL,
    numero_cheque character varying(25) DEFAULT NULL,
    numero_cuenta_cheque character varying(25) DEFAULT NULL,
    numero_referencia character varying(25) DEFAULT NULL,
--Campos FK
    id_cotizacion integer NOT NULL,
    cat_banco integer DEFAULT NULL,
    cat_tipo_pago integer NOT NULL
);
--CONSTRAINTS
    --PK
    ALTER TABLE cot_pago ADD CONSTRAINT pk_cot_pago PRIMARY KEY(id);
    --FK
    ALTER TABLE ONLY cot_pago ADD CONSTRAINT fk_cot_pag_cotizacion FOREIGN KEY (id_cotizacion) REFERENCES cotizacion(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY cot_pago ADD CONSTRAINT fk_cot_pag_cat_banco FOREIGN KEY (cat_banco) REFERENCES dynamic.category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY cot_pago ADD CONSTRAINT fk_cot_pag_cat_tipo_pago FOREIGN KEY (cat_tipo_pago) REFERENCES dynamic.category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE cot_pago IS 'Entidad que contiene los pagos de cotizaciones';
    --Campos basicos
    COMMENT ON COLUMN cot_pago.id IS 'Identificador del registro';
    COMMENT ON COLUMN cot_pago.id_ec IS 'Identicador encriptado del registro';
    COMMENT ON COLUMN cot_pago.codigo IS 'Codigo del registro visible al usuario';
    COMMENT ON COLUMN cot_pago.reg_cre_u IS 'Identificador del usuario que crea el registro';
    COMMENT ON COLUMN cot_pago.reg_cre_f IS 'Fecha de creacion del registro';
    COMMENT ON COLUMN cot_pago.reg_mod_u IS 'Identificador del usuario que modifica el registro';
    COMMENT ON COLUMN cot_pago.reg_mod_f IS 'Fecha de modificacion del registro';
    COMMENT ON COLUMN cot_pago.reg_eli_u IS 'Identificador del usuario que elimina el registro';
    COMMENT ON COLUMN cot_pago.reg_eli_f IS 'Fecha de eliminacion del registro';
    COMMENT ON COLUMN cot_pago.reg_eli IS  'Identifica si el registro esta o no eliminado: 1 - Eliminado, 0 - No eliminado';
    --Campos propios
    COMMENT ON COLUMN cot_pago.fecha IS 'Fecha del pago de la cotizacion';
    COMMENT ON COLUMN cot_pago.monto IS 'Monto del pago de la cotizacion';
    COMMENT ON COLUMN cot_pago.numero_cheque IS 'Numero de cheque con el que se realizo el pago, en caso de que aplique';
    COMMENT ON COLUMN cot_pago.numero_cuenta_cheque IS 'Numero de cuenta del cheque con el que se realizo el pago, en caso de que aplique';
    COMMENT ON COLUMN cot_pago.numero_referencia IS 'Numero de transferencia o deposito del pago, en caso de que aplique';
    --Campos FK
    COMMENT ON COLUMN cot_pago.id_cotizacion IS 'Identificador de la cotizacion';
    COMMENT ON COLUMN cot_pago.cat_banco IS 'Identificador de la categoria con el nombre del banco';
    COMMENT ON COLUMN cot_pago.cat_tipo_pago IS 'Identificador de la categoria del tipo de pago';