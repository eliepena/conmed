SET search_path = business,pg_catalog;
CREATE TABLE  perfil_instrumentista(
--Campos Basicos
     id bigserial NOT NULL,
    id_ec character varying(50) NOT NULL,
    codigo character varying(10) NOT NULL,  
    reg_cre_u integer NOT NULL,
    reg_cre_f timestamp DEFAULT now() NOT NULL,
    reg_mod_u integer,
    reg_mod_f timestamp,
    reg_eli_u integer,
    reg_eli_f timestamp,
    reg_eli bit(1) DEFAULT B'0'::"bit" NOT NULL,
--Campos propios
    fecha_inicio timestamp NOT NULL,
    observaciones text DEFAULT NULL,
--Campos FK
    id_empleado integer NOT NULL,
    cat_tipo_cirugia integer NOT NULL
);
--CONSTRAINTS
    --PK
    ALTER TABLE perfil_instrumentista ADD CONSTRAINT pk_perfil_instrumentista PRIMARY KEY(id);
    --FK
    ALTER TABLE ONLY perfil_instrumentista ADD CONSTRAINT fk_per_ins_empleado FOREIGN KEY (id_empleado) REFERENCES empleado(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY perfil_instrumentista ADD CONSTRAINT fk_per_ins_cat_tipo_cirugia FOREIGN KEY (cat_tipo_cirugia) REFERENCES dynamic.category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE perfil_instrumentista IS 'Entidad que contiene los tipos de cirugias dominados por los distintos instrumentistas';
    --Campos basicos
    COMMENT ON COLUMN perfil_instrumentista.id IS 'Identificador del registro';
    COMMENT ON COLUMN perfil_instrumentista.id_ec IS 'Identicador encriptado del registro';
    COMMENT ON COLUMN perfil_instrumentista.codigo IS 'Codigo del registro visible al usuario';
    COMMENT ON COLUMN perfil_instrumentista.reg_cre_u IS 'Identificador del usuario que crea el registro';
    COMMENT ON COLUMN perfil_instrumentista.reg_cre_f IS 'Fecha de creacion del registro';
    COMMENT ON COLUMN perfil_instrumentista.reg_mod_u IS 'Identificador del usuario que modifica el registro';
    COMMENT ON COLUMN perfil_instrumentista.reg_mod_f IS 'Fecha de modificacion del registro';
    COMMENT ON COLUMN perfil_instrumentista.reg_eli_u IS 'Identificador del usuario que elimina el registro';
    COMMENT ON COLUMN perfil_instrumentista.reg_eli_f IS 'Fecha de eliminacion del registro';
    COMMENT ON COLUMN perfil_instrumentista.reg_eli IS  'Identifica si el registro esta o no eliminado: 1 - Eliminado, 0 - No eliminado';
    --Campos propios
    COMMENT ON COLUMN perfil_instrumentista.fecha_inicio IS 'Fecha en la que el instrumentista domino la cirugia';
    COMMENT ON COLUMN perfil_instrumentista.observaciones IS 'Observaciones relacionadas con el perfil del instrumentista';
    --Campos FK
    COMMENT ON COLUMN perfil_instrumentista.id_empleado IS 'Identificador del empleado instrumentista';
    COMMENT ON COLUMN perfil_instrumentista.cat_tipo_cirugia IS 'Identificador de la categoria del tipo de cirugia';