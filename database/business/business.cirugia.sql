SET search_path = business,pg_catalog;
CREATE TABLE  cirugia(
--Campos Basicos
    id bigint NOT NULL,
--Campos propios
    fecha_hora timestamp NOT NULL,
    observaciones text DEFAULT NULL,
--Campos FK
    id_cotizacion integer NOT NULL,
    id_carta_aval integer DEFAULT NULL,
    id_empleado_logistica integer NOT NULL,
    id_centro_salud integer NOT NULL,
    cat_forma_pago integer NOT NULL,
    cat_tipo_cirugia integer NOT NULL
)INHERITS(super_table);
--CONSTRAINTS
    --PK
    ALTER TABLE cirugia ADD CONSTRAINT pk_cirugia PRIMARY KEY(id);
    --FK
    ALTER TABLE ONLY cirugia ADD CONSTRAINT fk_cir_cotizacion FOREIGN KEY (id_cotizacion) REFERENCES cotizacion(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY cirugia ADD CONSTRAINT fk_cir_carta_aval FOREIGN KEY (id_carta_aval) REFERENCES carta_aval(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY cirugia ADD CONSTRAINT fk_cir_empleado_logistica FOREIGN KEY (id_empleado_logistica) REFERENCES empleado(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY cirugia ADD CONSTRAINT fk_cir_clinica FOREIGN KEY (id_centro_salud) REFERENCES centro_salud(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY cirugia ADD CONSTRAINT fk_cir_cat_forma_pago FOREIGN KEY (cat_forma_pago) REFERENCES dynamic.category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY cirugia ADD CONSTRAINT fk_cir_cat_tipo_cirugia FOREIGN KEY (cat_tipo_cirugia) REFERENCES dynamic.category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE cirugia IS 'Entidad que contiene las cirugias';
    --Campos basicos
    COMMENT ON COLUMN cirugia.id IS 'Identificador del registro';
    --Campos propios
    COMMENT ON COLUMN cirugia.fecha_hora IS 'Fecha y hora de la cirugia';
    COMMENT ON COLUMN cirugia.observaciones IS 'Observaciones sobre la cirugia';
    --Campos FK
    COMMENT ON COLUMN cirugia.id_cotizacion IS 'Identificador de la cotizacion';
    COMMENT ON COLUMN cirugia.id_carta_aval IS 'Identificador de la carta aval';
    COMMENT ON COLUMN cirugia.id_empleado_logistica IS 'Identificador del empleado de logistica que registra la cirugia';
    COMMENT ON COLUMN cirugia.id_centro_salud IS 'Identificador del centro de salud donde se realizara la cirugia';
    COMMENT ON COLUMN cirugia.cat_forma_pago IS 'Identificador de la categoria de forma de pago';
    COMMENT ON COLUMN cirugia.cat_tipo_cirugia IS 'Identificador de la categoria de tipo de cirugia';