SET search_path = business,pg_catalog;
CREATE TABLE  material(
--Campos Basicos
    id bigint NOT NULL,
--Campos propios
    modelo character varying(50) NOT NULL,
    descripcion text DEFAULT NULL,
--Campos FK
    cat_fabricante_material integer NOT NULL
)INHERITS(super_table);
--CONSTRAINTS
    --PK
    ALTER TABLE material ADD CONSTRAINT pk_material PRIMARY KEY(id);
    --FK
    ALTER TABLE ONLY material ADD CONSTRAINT fk_ins_cat_fabricante_material FOREIGN KEY (cat_fabricante_material) REFERENCES dynamic.category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE material IS 'Entidad que contiene los materiales';
    --Campos basicos
    COMMENT ON COLUMN material.id IS 'Identificador del registro';
    --Campos propios
    COMMENT ON COLUMN material.modelo IS 'Modelo del material';
    COMMENT ON COLUMN material.descripcion IS 'Descripcion del material';
    --Campos FK
    COMMENT ON COLUMN material.cat_fabricante_material IS 'Identificador de la categoria de fabricante del material';