SET search_path = business,pg_catalog;
CREATE TABLE  ins_recomendacion(
--Campos Basicos
    id bigserial NOT NULL,
    id_ec character varying(50) NOT NULL,
    codigo character varying(10) NOT NULL,  
    reg_cre_u integer NOT NULL,
    reg_cre_f timestamp DEFAULT now() NOT NULL,
    reg_mod_u integer,
    reg_mod_f timestamp,
    reg_eli_u integer,
    reg_eli_f timestamp,
    reg_eli bit(1) DEFAULT B'0'::"bit" NOT NULL,
--Campos propios

--Campos FK
    id_instrumento integer NOT NULL,
    cat_tipo_cirugia integer NOT NULL
);
--CONSTRAINTS
    --PK
    ALTER TABLE ins_recomendacion ADD CONSTRAINT pk_ins_recomendacion PRIMARY KEY(id_instrumento,cat_tipo_cirugia);
    --FK
    ALTER TABLE ONLY ins_recomendacion ADD CONSTRAINT fk_ins_rec_instrumento FOREIGN KEY (id_instrumento) REFERENCES instrumento(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY ins_recomendacion ADD CONSTRAINT fk_ins_rec_cat_tipo_cirugia FOREIGN KEY (cat_tipo_cirugia) REFERENCES dynamic.category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE ins_recomendacion IS 'Entidad que contiene las recomendaciones de instrumentos por tipo de cirugia';
    --Campos basicos
    COMMENT ON COLUMN ins_recomendacion.id IS 'Identificador del registro';
    COMMENT ON COLUMN ins_recomendacion.id_ec IS 'Identicador encriptado del registro';
    COMMENT ON COLUMN ins_recomendacion.codigo IS 'Codigo del registro visible al usuario';
    COMMENT ON COLUMN ins_recomendacion.reg_cre_u IS 'Identificador del usuario que crea el registro';
    COMMENT ON COLUMN ins_recomendacion.reg_cre_f IS 'Fecha de creacion del registro';
    COMMENT ON COLUMN ins_recomendacion.reg_mod_u IS 'Identificador del usuario que modifica el registro';
    COMMENT ON COLUMN ins_recomendacion.reg_mod_f IS 'Fecha de modificacion del registro';
    COMMENT ON COLUMN ins_recomendacion.reg_eli_u IS 'Identificador del usuario que elimina el registro';
    COMMENT ON COLUMN ins_recomendacion.reg_eli_f IS 'Fecha de eliminacion del registro';
    COMMENT ON COLUMN ins_recomendacion.reg_eli IS  'Identifica si el registro esta o no eliminado: 1 - Eliminado, 0 - No eliminado';
    --Campos propios

    --Campos FK
    COMMENT ON COLUMN ins_recomendacion.id_instrumento IS 'Identificador del instrumento recomendado';
    COMMENT ON COLUMN ins_recomendacion.cat_tipo_cirugia IS 'Identificador de la categoria del tipo de cirugia';