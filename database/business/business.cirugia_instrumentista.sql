SET search_path = business,pg_catalog;
CREATE TABLE  cirugia_instrumentista(
--Campos Basicos
    id bigserial NOT NULL,
    id_ec character varying(50) NOT NULL,
    codigo character varying(10) NOT NULL,  
    reg_cre_u integer NOT NULL,
    reg_cre_f timestamp DEFAULT now() NOT NULL,
    reg_mod_u integer,
    reg_mod_f timestamp,
    reg_eli_u integer,
    reg_eli_f timestamp,
    reg_eli bit(1) DEFAULT B'0'::"bit" NOT NULL,
--Campos propios
    fecha timestamp NOT NULL,
--Campos FK
    id_empleado integer NOT NULL,
    id_cirugia integer NOT NULL,
    cat_rol_instrumentista integer NOT NULL
);
--CONSTRAINTS
    --PK
    ALTER TABLE cirugia_instrumentista ADD CONSTRAINT pk_cirugia_instrumentista PRIMARY KEY(id);
    --FK
    ALTER TABLE ONLY cirugia_instrumentista ADD CONSTRAINT fk_cir_ins_empleado FOREIGN KEY (id_empleado) REFERENCES empleado(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY cirugia_instrumentista ADD CONSTRAINT fk_cir_ins_cirugia FOREIGN KEY (id_cirugia) REFERENCES cirugia(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY cirugia_instrumentista ADD CONSTRAINT fk_cir_ins_cat_rol_instrumentista FOREIGN KEY (cat_rol_instrumentista) REFERENCES dynamic.category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE cirugia_instrumentista IS 'Entidad que contiene las cirugias agendadas y practicadas por los intrumentistas';
    --Campos basicos
    COMMENT ON COLUMN cirugia_instrumentista.id IS 'Identificador del registro';
    COMMENT ON COLUMN cirugia_instrumentista.id_ec IS 'Identicador encriptado del registro';
    COMMENT ON COLUMN cirugia_instrumentista.codigo IS 'Codigo del registro visible al usuario';
    COMMENT ON COLUMN cirugia_instrumentista.reg_cre_u IS 'Identificador del usuario que crea el registro';
    COMMENT ON COLUMN cirugia_instrumentista.reg_cre_f IS 'Fecha de creacion del registro';
    COMMENT ON COLUMN cirugia_instrumentista.reg_mod_u IS 'Identificador del usuario que modifica el registro';
    COMMENT ON COLUMN cirugia_instrumentista.reg_mod_f IS 'Fecha de modificacion del registro';
    COMMENT ON COLUMN cirugia_instrumentista.reg_eli_u IS 'Identificador del usuario que elimina el registro';
    COMMENT ON COLUMN cirugia_instrumentista.reg_eli_f IS 'Fecha de eliminacion del registro';
    COMMENT ON COLUMN cirugia_instrumentista.reg_eli IS  'Identifica si el registro esta o no eliminado: 1 - Eliminado, 0 - No eliminado';
    --Campos propios
    COMMENT ON COLUMN cirugia_instrumentista.fecha IS 'Fecha y hora de la asignacion';
    --Campos FK
    COMMENT ON COLUMN cirugia_instrumentista.id_empleado IS 'Identificador del empleado instrumentista que participara en la cirugia';
    COMMENT ON COLUMN cirugia_instrumentista.id_cirugia IS 'Identificador de la cirugia';
    COMMENT ON COLUMN cirugia_instrumentista.cat_rol_instrumentista IS 'Identificador de la categoria de rol del instrumentista';