SET search_path = business,pg_catalog;
CREATE TABLE  carta_aval(
--Campos Basicos
    id bigint NOT NULL,
--Campos propios
    numero_poliza character varying(25) NOT NULL,
    fecha_emision date NOT NULL,
    fecha_vencimiento date NOT NULL,
    monto character varying(25) NOT NULL,
    deducible character varying(25) DEFAULT NULL,
    descripcion_servicio text DEFAULT NULL,
    condiciones text DEFAULT NULL,
    observaciones text DEFAULT NULL,
--Campos FK
    id_seguro integer NOT NULL,
    id_paciente integer NOT NULL
)INHERITS(super_table);
--CONSTRAINTS
    --PK
    ALTER TABLE carta_aval ADD CONSTRAINT pk_carta_aval PRIMARY KEY(id);
    --FK
    ALTER TABLE ONLY carta_aval ADD CONSTRAINT fk_car_ava_seguro FOREIGN KEY (id_seguro) REFERENCES seguro(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY carta_aval ADD CONSTRAINT fk_car_ava_paciente FOREIGN KEY (id_paciente) REFERENCES paciente(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE carta_aval IS 'Entidad que contiene las carta_avales';
    --Campos basicos
    COMMENT ON COLUMN carta_aval.id IS 'Identificador del registro';
    --Campos propios
    COMMENT ON COLUMN carta_aval.numero_poliza IS 'Numero de poliza, se registra con la notacion del seguro';
    COMMENT ON COLUMN carta_aval.fecha_emision IS 'Fecha de emision de la carta aval';
    COMMENT ON COLUMN carta_aval.fecha_vencimiento IS 'Fecha de vencimiento de la carta aval';
    COMMENT ON COLUMN carta_aval.monto IS 'Monto total aprobado por la carta aval';
    COMMENT ON COLUMN carta_aval.deducible IS 'Monto deducible en caso de que aplique';
    COMMENT ON COLUMN carta_aval.descripcion_servicio IS 'Descricion del tratamiento, intervencion o servicio que cancela la carta aval';
    COMMENT ON COLUMN carta_aval.condiciones IS 'Condiciones de la carta aval, usualmente son estandares para todas las cartas avales de un seguro';
    COMMENT ON COLUMN carta_aval.observaciones IS 'Observaciones adicionales de la carta aval';
    --Campos FK
    COMMENT ON COLUMN carta_aval.id_seguro IS 'Identificador de la compania aseguradora emisora de la carta aval';
    COMMENT ON COLUMN carta_aval.id_paciente IS 'Identificador del paciente cubierto por la carta aval';