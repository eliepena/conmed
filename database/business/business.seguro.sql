SET search_path = business,pg_catalog;
CREATE TABLE  seguro(
--Campos Basicos
    id bigint NOT NULL,
--Campos propios
    nombre  character varying(150) NOT NULL,
    rif character varying(25) NOT NULL,
    nit character varying(25) NOT NULL,    
    telefono_principal character varying(25) NOT NULL,
    telefono_secundario character varying(25) DEFAULT NULL,
    email character varying(100) DEFAULT NULL,
    sitio_web character varying(100) DEFAULT NULL,
--Campos FK
    cat_tipo_seguro integer NOT NULL
)INHERITS(super_table);
--CONSTRAINTS
    --PK
    ALTER TABLE seguro ADD CONSTRAINT pk_seguro PRIMARY KEY(id);
    --FK
    ALTER TABLE ONLY seguro ADD CONSTRAINT fk_seg_cat_tipo_seguro FOREIGN KEY (cat_tipo_seguro) REFERENCES dynamic.category(id) ON UPDATE CASCADE ON DELETE RESTRICT;

--COMMENTS
    --Tabla
    COMMENT ON TABLE seguro IS 'Entidad que contiene las companias aseguradoras';
    --Campos basicos
    COMMENT ON COLUMN seguro.id IS 'Identificador del registro';
    --Campos propios
    COMMENT ON COLUMN seguro.nombre IS 'Nombre de la compania aseguradora';
    COMMENT ON COLUMN seguro.rif IS 'Numero RIF de la compania aseguradora';
    COMMENT ON COLUMN seguro.nit IS 'Numero NIT de la compania aseguradora';    
    COMMENT ON COLUMN seguro.telefono_principal IS 'Telefono principal de la compania aseguradora';
    COMMENT ON COLUMN seguro.telefono_secundario IS 'Telefono secundario de la compania aseguradora';
    COMMENT ON COLUMN seguro.email IS 'Direccion de correo electronico de la compania aseguradora';
    COMMENT ON COLUMN seguro.sitio_web IS 'Direccion web de la compania aseguradora';
    --Campos FK
    COMMENT ON COLUMN seguro.cat_tipo_seguro IS 'Identificador de la categoria de tipo de seguro. Se utiliza para clasificar seguros por confiabilidad';
