SET search_path = business,pg_catalog;
CREATE TABLE  mat_tracking(
--Campos Basicos
    id bigserial NOT NULL,
    id_ec character varying(50) NOT NULL,
    codigo character varying(10) NOT NULL,  
    reg_cre_u integer NOT NULL,
    reg_cre_f timestamp DEFAULT now() NOT NULL,
    reg_mod_u integer,
    reg_mod_f timestamp,
    reg_eli_u integer,
    reg_eli_f timestamp,
    reg_eli bit(1) DEFAULT B'0'::"bit" NOT NULL,
--Campos propios
    fecha_inicio timestamp NOT NULL,
    fecha_fin timestamp DEFAULT NULL,
    observaciones text DEFAULT NULL,
--Campos FK
    id_item_material integer NOT NULL,
    id_despacho integer NOT NULL,
    id_empleado integer NOT NULL,
    cat_mat_tracking_status integer NOT NULL
);
--CONSTRAINTS
    --PK
    ALTER TABLE mat_tracking ADD CONSTRAINT pk_mat_tracking PRIMARY KEY(id);
    --FK
    ALTER TABLE ONLY mat_tracking ADD CONSTRAINT fk_mat_tra_mat_item FOREIGN KEY (id_item_material) REFERENCES mat_item(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY mat_tracking ADD CONSTRAINT fk_mat_tra_despacho FOREIGN KEY (id_despacho) REFERENCES despacho(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY mat_tracking ADD CONSTRAINT fk_mat_tra_empleado FOREIGN KEY (id_empleado) REFERENCES empleado(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY mat_tracking ADD CONSTRAINT fk_mat_tra_cat_mat_tracking_status FOREIGN KEY (cat_mat_tracking_status) REFERENCES dynamic.category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE mat_tracking IS 'Entidad que contiene los cambios de estatus de un item de material';
    --Campos basicos
    COMMENT ON COLUMN mat_tracking.id IS 'Identificador del registro';
    COMMENT ON COLUMN mat_tracking.id_ec IS 'Identicador encriptado del registro';
    COMMENT ON COLUMN mat_tracking.codigo IS 'Codigo del registro visible al usuario';
    COMMENT ON COLUMN mat_tracking.reg_cre_u IS 'Identificador del usuario que crea el registro';
    COMMENT ON COLUMN mat_tracking.reg_cre_f IS 'Fecha de creacion del registro';
    COMMENT ON COLUMN mat_tracking.reg_mod_u IS 'Identificador del usuario que modifica el registro';
    COMMENT ON COLUMN mat_tracking.reg_mod_f IS 'Fecha de modificacion del registro';
    COMMENT ON COLUMN mat_tracking.reg_eli_u IS 'Identificador del usuario que elimina el registro';
    COMMENT ON COLUMN mat_tracking.reg_eli_f IS 'Fecha de eliminacion del registro';
    COMMENT ON COLUMN mat_tracking.reg_eli IS  'Identifica si el registro esta o no eliminado: 1 - Eliminado, 0 - No eliminado';
    --Campos propios
    COMMENT ON COLUMN mat_tracking.fecha_inicio IS 'Fecha de inicio del cambio de estatus';
    COMMENT ON COLUMN mat_tracking.fecha_fin IS 'Fecha de fin del cambio de estatus';
    COMMENT ON COLUMN mat_tracking.observaciones IS 'Observaciones relacionadas con el cambio de estatus';
    --Campos FK
     COMMENT ON COLUMN mat_tracking.id_item_material IS 'Identificador del item del instrumento';
    COMMENT ON COLUMN mat_tracking.id_despacho IS 'Identificador del despacho';
    COMMENT ON COLUMN mat_tracking.id_empleado IS 'Identificador del empleado que registra el cambio de estatus';
    COMMENT ON COLUMN mat_tracking.cat_mat_tracking_status IS 'Identificador de la categoria de estatus de tracking de instrumento';