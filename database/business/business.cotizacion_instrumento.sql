SET search_path = business,pg_catalog;
CREATE TABLE  cotizacion_instrumento(
--Campos Basicos
    id bigserial NOT NULL,
    id_ec character varying(50) NOT NULL,
    codigo character varying(10) NOT NULL,  
    reg_cre_u integer NOT NULL,
    reg_cre_f timestamp DEFAULT now() NOT NULL,
    reg_mod_u integer,
    reg_mod_f timestamp,
    reg_eli_u integer,
    reg_eli_f timestamp,
    reg_eli bit(1) DEFAULT B'0'::"bit" NOT NULL,
--Campos propios
    fecha date NOT NULL,
    cantidad integer NOT NULL,
--Campos FK
    id_cotizacion integer NOT NULL,
    id_instrumento integer NOT NULL
);
--CONSTRAINTS
    --PK
    ALTER TABLE cotizacion_instrumento ADD CONSTRAINT pk_cotizacion_instrumento PRIMARY KEY(id_cotizacion,id_instrumento);
    --FK
    ALTER TABLE ONLY cotizacion_instrumento ADD CONSTRAINT fk_cot_ins_cotizacion FOREIGN KEY (id_cotizacion) REFERENCES cotizacion(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY cotizacion_instrumento ADD CONSTRAINT fk_cot_ins_instrumento FOREIGN KEY (id_instrumento) REFERENCES instrumento(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE cotizacion_instrumento IS 'Entidad que contiene los instrumentos asignados a una cotizacion';
    --Campos basicos
    COMMENT ON COLUMN cotizacion_instrumento.id IS 'Identificador del registro';
    COMMENT ON COLUMN cotizacion_instrumento.id_ec IS 'Identicador encriptado del registro';
    COMMENT ON COLUMN cotizacion_instrumento.codigo IS 'Codigo del registro visible al usuario';
    COMMENT ON COLUMN cotizacion_instrumento.reg_cre_u IS 'Identificador del usuario que crea el registro';
    COMMENT ON COLUMN cotizacion_instrumento.reg_cre_f IS 'Fecha de creacion del registro';
    COMMENT ON COLUMN cotizacion_instrumento.reg_mod_u IS 'Identificador del usuario que modifica el registro';
    COMMENT ON COLUMN cotizacion_instrumento.reg_mod_f IS 'Fecha de modificacion del registro';
    COMMENT ON COLUMN cotizacion_instrumento.reg_eli_u IS 'Identificador del usuario que elimina el registro';
    COMMENT ON COLUMN cotizacion_instrumento.reg_eli_f IS 'Fecha de eliminacion del registro';
    COMMENT ON COLUMN cotizacion_instrumento.reg_eli IS  'Identifica si el registro esta o no eliminado: 1 - Eliminado, 0 - No eliminado';
    --Campos propios
    COMMENT ON COLUMN cotizacion_instrumento.fecha IS 'Fecha de la asignacion';
    --Campos FK
    COMMENT ON COLUMN cotizacion_instrumento.id_cotizacion IS 'Identificador de la cotizacion';
    COMMENT ON COLUMN cotizacion_instrumento.id_instrumento IS 'Identificador del instrumento';