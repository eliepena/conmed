SET search_path = business,pg_catalog;
CREATE TABLE  orden_despacho(
--Campos Basicos
    id bigint NOT NULL,
--Campos propios
    fecha_solicitud timestamp NOT NULL,
    fecha_despacho timestamp DEFAULT NULL,    
--Campos FK
    id_despachador integer DEFAULT NULL,
    id_cirugia integer DEFAULT NULL,
    id_cotizacion integer DEFAULT NULL,
    id_consignacion integer DEFAULT NULL,
    cat_estatus_orden_despacho integer NOT NULL
)INHERITS(super_table);
--CONSTRAINTS
    --PK
    ALTER TABLE orden_despacho ADD CONSTRAINT pk_orden_despacho PRIMARY KEY(id);
    --FK
    ALTER TABLE ONLY orden_despacho ADD CONSTRAINT fk_ord_des_despachador FOREIGN KEY (id_despachador) REFERENCES empleado(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY orden_despacho ADD CONSTRAINT fk_ord_des_cirugia FOREIGN KEY (id_cirugia) REFERENCES cirugia(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY orden_despacho ADD CONSTRAINT fk_ord_des_cotizacion FOREIGN KEY (id_cotizacion) REFERENCES cotizacion(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY orden_despacho ADD CONSTRAINT fk_ord_des_consignacion FOREIGN KEY (id_consignacion) REFERENCES consignacion(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY orden_despacho ADD CONSTRAINT fk_ord_des_cat_estatus FOREIGN KEY (cat_estatus_orden_despacho) REFERENCES dynamic.category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE orden_despacho IS 'Entidad que contiene las ordenes de despacho';
    --Campos basicos
    COMMENT ON COLUMN orden_despacho.id IS 'Identificador del registro';
    --Campos propios
    COMMENT ON COLUMN orden_despacho.fecha_solicitud IS 'Fecha de solicitud de la orden de despacho';
    COMMENT ON COLUMN orden_despacho.fecha_despacho IS 'Fecha de despacho de la orden de despacho';
    --Campos FK
    COMMENT ON COLUMN orden_despacho.id_despachador IS 'Identificador del empleado de almacen que despacha la orden';
    COMMENT ON COLUMN orden_despacho.id_cirugia IS 'Identificador de la ciruguia que requiere el despacho, en caso de que aplique';
    COMMENT ON COLUMN orden_despacho.id_cotizacion IS 'Identificador de la cotizacion que requiere el despacho, en caso de que aplique';
    COMMENT ON COLUMN orden_despacho.id_consignacion IS 'Identificador de la consignacion que requiere el despacho, en caso de que aplique';
    COMMENT ON COLUMN orden_despacho.cat_estatus_orden_despacho IS 'Identificador de la categoria del estatus de la orden de despacho';
