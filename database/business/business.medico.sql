SET search_path = business,pg_catalog;
CREATE TABLE medico(
--Campos Basicos
    id bigint NOT NULL,
--Campos propios

--Campos FK
    id_persona integer NOT NULL,
    cat_especialidad_medica integer NOT NULL
)INHERITS(persona);
--CONSTRAINTS
    --PK
    ALTER TABLE medico ADD CONSTRAINT pk_medico PRIMARY KEY(id);
    --FK
    ALTER TABLE ONLY medico ADD CONSTRAINT fk_med_persona FOREIGN KEY (id_persona) REFERENCES persona(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY medico ADD CONSTRAINT fk_med_cat_especialidad_medica FOREIGN KEY (cat_especialidad_medica) REFERENCES dynamic.category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE medico IS 'Entidad que contiene los medicos';
    --Campos basicos
    COMMENT ON COLUMN medico.id IS 'Identificador del registro';
    --Campos propios

    --Campos FK
    COMMENT ON COLUMN medico.id_persona IS  'Identificador del registro de superclase persona asociado al medico';
    COMMENT ON COLUMN medico.cat_especialidad_medica IS  'Identificador de la categoria de especialidad medica';