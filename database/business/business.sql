CREATE SCHEMA business AUTHORIZATION postgres;

GRANT ALL ON SCHEMA business TO postgres;

COMMENT ON SCHEMA business IS 'Esquema que contiene las tablas de regla de negocio';
