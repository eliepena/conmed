SET search_path = business,pg_catalog;
CREATE TABLE  consignacion(
--Campos Basicos
    id bigint NOT NULL,
--Campos propios
    fecha timestamp NOT NULL,
--Campos FK
    id_medico integer NOT NULL
)INHERITS(super_table);
--CONSTRAINTS
    --PK
    ALTER TABLE consignacion ADD CONSTRAINT pk_consignacion PRIMARY KEY(id);
    --FK
    ALTER TABLE ONLY consignacion ADD CONSTRAINT fk_con_medico FOREIGN KEY (id_medico) REFERENCES medico(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE consignacion IS 'Entidad que contiene las consignaciones';
    --Campos basicos
    COMMENT ON COLUMN consignacion.id IS 'Identificador del registro';
    --Campos propios
    COMMENT ON COLUMN consignacion.fecha IS 'Fecha de entrega de la consignacion';
    --Campos FK
    COMMENT ON COLUMN consignacion.id_medico IS 'Identificador del medico que recibe la consignacion';