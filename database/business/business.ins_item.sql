SET search_path = business,pg_catalog;
CREATE TABLE  ins_item(
--Campos Basicos
     id bigserial NOT NULL,
    id_ec character varying(50) NOT NULL,
    codigo character varying(10) NOT NULL,  
    reg_cre_u integer NOT NULL,
    reg_cre_f timestamp DEFAULT now() NOT NULL,
    reg_mod_u integer,
    reg_mod_f timestamp,
    reg_eli_u integer,
    reg_eli_f timestamp,
    reg_eli bit(1) DEFAULT B'0'::"bit" NOT NULL,
--Campos propios
    numero_serie character varying(25) NOT NULL,
--Campos FK
    id_instrumento integer NOT NULL
);
--CONSTRAINTS
    --PK
    ALTER TABLE ins_item ADD CONSTRAINT pk_ins_item PRIMARY KEY(id);
    --FK
    ALTER TABLE ONLY ins_item ADD CONSTRAINT fk_ins_ite_instrumento FOREIGN KEY (id_instrumento) REFERENCES instrumento(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE ins_item IS 'Entidad que contiene los registros de items de instrumentos';
    --Campos basicos
    COMMENT ON COLUMN ins_item.id IS 'Identificador del registro';
    COMMENT ON COLUMN ins_item.id_ec IS 'Identicador encriptado del registro';
    COMMENT ON COLUMN ins_item.codigo IS 'Codigo del registro visible al usuario';
    COMMENT ON COLUMN ins_item.reg_cre_u IS 'Identificador del usuario que crea el registro';
    COMMENT ON COLUMN ins_item.reg_cre_f IS 'Fecha de creacion del registro';
    COMMENT ON COLUMN ins_item.reg_mod_u IS 'Identificador del usuario que modifica el registro';
    COMMENT ON COLUMN ins_item.reg_mod_f IS 'Fecha de modificacion del registro';
    COMMENT ON COLUMN ins_item.reg_eli_u IS 'Identificador del usuario que elimina el registro';
    COMMENT ON COLUMN ins_item.reg_eli_f IS 'Fecha de eliminacion del registro';
    COMMENT ON COLUMN ins_item.reg_eli IS  'Identifica si el registro esta o no eliminado: 1 - Eliminado, 0 - No eliminado';
    --Campos propios
    COMMENT ON COLUMN ins_item.codigo IS 'Numero de serie unico del instrumento';
    --Campos FK
    COMMENT ON COLUMN ins_item.id_instrumento IS 'Identificador del instrumento que define todos los items de instrumento';