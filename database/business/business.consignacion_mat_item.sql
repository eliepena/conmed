SET search_path = business,pg_catalog;
CREATE TABLE  consignacion_mat_item(
--Campos Basicos
    id bigserial NOT NULL,
    id_ec character varying(50) NOT NULL,
    codigo character varying(10) NOT NULL,  
    reg_cre_u integer NOT NULL,
    reg_cre_f timestamp DEFAULT now() NOT NULL,
    reg_mod_u integer,
    reg_mod_f timestamp,
    reg_eli_u integer,
    reg_eli_f timestamp,
    reg_eli bit(1) DEFAULT B'0'::"bit" NOT NULL,
--Campos propios
    fecha date NOT NULL,
--Campos FK
    id_consignacion integer NOT NULL,
    id_item_material integer NOT NULL
);
--CONSTRAINTS
    --PK
    ALTER TABLE consignacion_mat_item ADD CONSTRAINT pk_consignacion_mat_item PRIMARY KEY(id);
    --FK
    ALTER TABLE ONLY consignacion_mat_item ADD CONSTRAINT fk_con_mat_ite_consignacion FOREIGN KEY (id_consignacion) REFERENCES consignacion(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY consignacion_mat_item ADD CONSTRAINT fk_con_mat_ite_item_material FOREIGN KEY (id_item_material) REFERENCES mat_item(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE consignacion_mat_item IS 'Entidad que contiene los items de materiales consignados';
    --Campos basicos
    COMMENT ON COLUMN consignacion_mat_item.id IS 'Identificador del registro';
    COMMENT ON COLUMN consignacion_mat_item.id_ec IS 'Identicador encriptado del registro';
    COMMENT ON COLUMN consignacion_mat_item.codigo IS 'Codigo del registro visible al usuario';
    COMMENT ON COLUMN consignacion_mat_item.reg_cre_u IS 'Identificador del usuario que crea el registro';
    COMMENT ON COLUMN consignacion_mat_item.reg_cre_f IS 'Fecha de creacion del registro';
    COMMENT ON COLUMN consignacion_mat_item.reg_mod_u IS 'Identificador del usuario que modifica el registro';
    COMMENT ON COLUMN consignacion_mat_item.reg_mod_f IS 'Fecha de modificacion del registro';
    COMMENT ON COLUMN consignacion_mat_item.reg_eli_u IS 'Identificador del usuario que elimina el registro';
    COMMENT ON COLUMN consignacion_mat_item.reg_eli_f IS 'Fecha de eliminacion del registro';
    COMMENT ON COLUMN consignacion_mat_item.reg_eli IS  'Identifica si el registro esta o no eliminado: 1 - Eliminado, 0 - No eliminado';
    --Campos propios
    COMMENT ON COLUMN consignacion_mat_item.fecha IS 'Fecha de la asignacion del material a la consignacion';
    --Campos FK
    COMMENT ON COLUMN consignacion_mat_item.id_consignacion IS 'Identificador de la consignacion';
    COMMENT ON COLUMN consignacion_mat_item.id_item_material IS 'Identificador del item de material consignado';