SET search_path = business,pg_catalog;
CREATE TABLE centro_salud(
--Campos Basicos
    id bigint NOT NULL,
--Campos propios
    nombre character varying(150) NOT NULL,
    rif character varying(50) NOT NULL,
    nit character varying(50) DEFAULT NULL,
    direccion text DEFAULT NULL,
--Campos FK
    cat_tipo_centro_salud integer NOT NULL
)INHERITS(super_table);
--CONSTRAINTS
    --PK
    ALTER TABLE centro_salud ADD CONSTRAINT pk_centro_salud PRIMARY KEY(id);
    --FK
    ALTER TABLE ONLY centro_salud ADD CONSTRAINT fk_cen_sal_cat_tipo_centro_salud FOREIGN KEY (cat_tipo_centro_salud) REFERENCES dynamic.category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE centro_salud IS 'Entidad que contiene los centros de salud';
    --Campos basicos
    COMMENT ON COLUMN centro_salud.id IS 'Identificador del registro';
    --Campos propios
    COMMENT ON COLUMN centro_salud.nombre IS 'Nombre del centro de salud';
    COMMENT ON COLUMN centro_salud.rif IS 'RIF del centro de salud';
    COMMENT ON COLUMN centro_salud.nit IS 'NIT del centro de salud';
    COMMENT ON COLUMN centro_salud.direccion IS 'Direccion del centro de salud';
    --Campos FK
    COMMENT ON COLUMN centro_salud.cat_tipo_centro_salud IS  'Identificador de la categoria del tipo de centro de salud. Ejemplo: clinica, hospital';