SET search_path = business,pg_catalog;
CREATE OR REPLACE TABLE centro_salud_medico(
--Campos Basicos
    id bigserial NOT NULL,
    id_ec character varying(50) NOT NULL,
    codigo character varying(10) NOT NULL,  
    reg_cre_u integer NOT NULL,
    reg_cre_f timestamp DEFAULT now() NOT NULL,
    reg_mod_u integer,
    reg_mod_f timestamp,
    reg_eli_u integer,
    reg_eli_f timestamp,
    reg_eli bit(1) DEFAULT B'0'::"bit" NOT NULL,
--Campos propios
    
--Campos FK
    id_centro_salud integer NOT NULL,
    id_medico integer NOT NULL
);
--CONSTRAINTS
    --PK
    ALTER TABLE centro_salud_medico ADD CONSTRAINT pk_centro_salud_medico PRIMARY KEY(id);
    --FK
    ALTER TABLE ONLY centro_salud_medico ADD CONSTRAINT fk_cen_sal_med_centro_salud FOREIGN KEY (id_centro_salud) REFERENCES centro_salud(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY centro_salud_medico ADD CONSTRAINT fk_cen_sal_med_medico FOREIGN KEY (id_medico) REFERENCES medico(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE centro_salud_medico IS 'Entidad que contiene los centros de salud';
    --Campos basicos
    COMMENT ON COLUMN centro_salud_medico.id IS 'Identificador del registro';
    COMMENT ON COLUMN centro_salud_medico.id_ec IS 'Identicador encriptado del registro';
    COMMENT ON COLUMN centro_salud_medico.codigo IS 'Codigo del registro visible al usuario';
    COMMENT ON COLUMN centro_salud_medico.reg_cre_u IS 'Identificador del usuario que crea el registro';
    COMMENT ON COLUMN centro_salud_medico.reg_cre_f IS 'Fecha de creacion del registro';
    COMMENT ON COLUMN centro_salud_medico.reg_mod_u IS 'Identificador del usuario que modifica el registro';
    COMMENT ON COLUMN centro_salud_medico.reg_mod_f IS 'Fecha de modificacion del registro';
    COMMENT ON COLUMN centro_salud_medico.reg_eli_u IS 'Identificador del usuario que elimina el registro';
    COMMENT ON COLUMN centro_salud_medico.reg_eli_f IS 'Fecha de eliminacion del registro';
    COMMENT ON COLUMN centro_salud_medico.reg_eli IS  'Identifica si el registro esta o no eliminado: 1 - Eliminado, 0 - No eliminado';
    --Campos propios

    --Campos FK
    COMMENT ON COLUMN centro_salud_medico.id_centro_salud IS  'Identificador del centro de salud';
    COMMENT ON COLUMN centro_salud_medico.id_medico IS  'Identificador del medico';