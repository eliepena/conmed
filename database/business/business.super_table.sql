SET search_path = business,pg_catalog;
CREATE TABLE  super_table(
    id bigserial NOT NULL,
    id_ec character varying(50) NOT NULL,
    codigo character varying(10) NOT NULL,  
    reg_cre_u integer NOT NULL,
    reg_cre_f timestamp DEFAULT now() NOT NULL,
    reg_mod_u integer,
    reg_mod_f timestamp,
    reg_eli_u integer,
    reg_eli_f timestamp,
    reg_eli bit(1) DEFAULT B'0'::"bit" NOT NULL
);
--CONSTRAINTS
    --PK
    ALTER TABLE super_table ADD CONSTRAINT pk_id_super_table PRIMARY KEY(id);
    --FK
    ALTER TABLE ONLY super_table ADD CONSTRAINT fk_sup_tab_cre_u FOREIGN KEY (reg_cre_u) REFERENCES acl.users(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY super_table ADD CONSTRAINT fk_sup_tab_mod_u FOREIGN KEY (reg_mod_u) REFERENCES acl.users(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY super_table ADD CONSTRAINT fk_sup_tab_eli_u FOREIGN KEY (reg_eli_u) REFERENCES acl.users(id) ON UPDATE CASCADE ON DELETE RESTRICT;

--COMMENTS
    --Tabla
    COMMENT ON TABLE super_table IS 'Entidad super tabla que se utiliza para aplicar campos comunes de registro a todas las tablas hijos';
    --Campos basicos
    COMMENT ON COLUMN super_table.id IS 'Identificador del registro';
    COMMENT ON COLUMN super_table.id_ec IS 'Identicador encriptado del registro';
    COMMENT ON COLUMN super_table.codigo IS 'Codigo del registro visible al usuario';
    COMMENT ON COLUMN super_table.reg_cre_u IS 'Identificador del usuario que crea el registro';
    COMMENT ON COLUMN super_table.reg_cre_f IS 'Fecha de creacion del registro';
    COMMENT ON COLUMN super_table.reg_mod_u IS 'Identificador del usuario que modifica el registro';
    COMMENT ON COLUMN super_table.reg_mod_f IS 'Fecha de modificacion del registro';
    COMMENT ON COLUMN super_table.reg_eli_u IS 'Identificador del usuario que elimina el registro';
    COMMENT ON COLUMN super_table.reg_eli_f IS 'Fecha de eliminacion del registro';
    COMMENT ON COLUMN super_table.reg_eli IS  'Identifica si el registro esta o no eliminado: 1 - Eliminado, 0 - No eliminado';