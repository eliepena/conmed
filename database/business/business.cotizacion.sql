SET search_path = business,pg_catalog;
CREATE TABLE cotizacion(
--Campos basicos
    id bigint NOT NULL,
--Campos propios
    fecha_cotizacion timestamp DEFAULT now(),
    fecha_validacion timestamp DEFAULT NULL,
    fecha_cirugia timestamp DEFAULT NULL,
    descuento float DEFAULT 0.0,
    recargo float DEFAULT 0.0,
--Campos FK
    id_medico  integer DEFAULT NULL,
    id_paciente integer DEFAULT NULL,
    id_vendedor integer NOT NULL,
    id_validador integer DEFAULT NULL,
    id_cotizacion integer DEFAULT NULL, 
    id_seguro integer DEFAULT NULL,     
    id_centro_salud integer DEFAULT NULL     
)INHERITS(super_table);
--CONSTRAINTS
    --PK
    ALTER TABLE cotizacion ADD CONSTRAINT pk_cotizacion PRIMARY KEY(id);
    --FK
    ALTER TABLE ONLY cotizacion ADD CONSTRAINT fk_cot_medico FOREIGN KEY (id_medico) REFERENCES medico(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY cotizacion ADD CONSTRAINT fk_cot_paciente FOREIGN KEY (id_paciente) REFERENCES paciente(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY cotizacion ADD CONSTRAINT fk_cot_vendedor FOREIGN KEY (id_vendedor) REFERENCES empleado(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY cotizacion ADD CONSTRAINT fk_cot_validador FOREIGN KEY (id_validador) REFERENCES empleado(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY cotizacion ADD CONSTRAINT fk_cot_cotizacion FOREIGN KEY (id_cotizacion) REFERENCES cotizacion(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY cotizacion ADD CONSTRAINT fk_cot_seguro FOREIGN KEY (id_seguro) REFERENCES seguro(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY cotizacion ADD CONSTRAINT fk_cot_clinica FOREIGN KEY (id_centro_salud) REFERENCES centro_salud(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE cotizacion IS 'Entidad que contiene las cotizaciones';
    --Campos basicos
    COMMENT ON COLUMN cotizacion.id IS 'Identificador del registro';
    --Campos propios
    COMMENT ON COLUMN cotizacion.fecha_cotizacion IS 'Fecha de cotizacion';
    COMMENT ON COLUMN cotizacion.fecha_validacion IS 'Fecha en que se valido los datos';
    COMMENT ON COLUMN cotizacion.fecha_cirugia IS 'Fecha de la cirugia';
    COMMENT ON COLUMN cotizacion.descuento IS 'Porcentaje de descuento sobre el subtotal';
    COMMENT ON COLUMN cotizacion.recargo IS 'Porcentaje de descuento sobre el subtotal';
    --Campos FK
    COMMENT ON COLUMN cotizacion.id_medico IS 'Identificador del medico';
    COMMENT ON COLUMN cotizacion.id_paciente IS 'Identificador del paciente';
    COMMENT ON COLUMN cotizacion.id_vendedor IS 'Identificador del vendedor';
    COMMENT ON COLUMN cotizacion.id_validador IS 'Identificador del usuario que valida la cotizacion';
    COMMENT ON COLUMN cotizacion.id_cotizacion IS 'Identificador de la cotizacion original(padre)';
    COMMENT ON COLUMN cotizacion.id_centro_salud IS 'Identificador de la clinica';