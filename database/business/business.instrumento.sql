SET search_path = business,pg_catalog;
CREATE TABLE  instrumento(
--Campos Basicos
    id bigint NOT NULL,
--Campos propios
    modelo character varying(50) NOT NULL,
    descripcion text DEFAULT NULL,
--Campos FK
    cat_fabricante_instrumento integer NOT NULL
)INHERITS(super_table);
--CONSTRAINTS
    --PK
    ALTER TABLE instrumento ADD CONSTRAINT pk_instrumento PRIMARY KEY(id);
    --FK
    ALTER TABLE ONLY instrumento ADD CONSTRAINT fk_ins_cat_fabricante_instrumento FOREIGN KEY (cat_fabricante_instrumento) REFERENCES dynamic.category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE instrumento IS 'Entidad que contiene los instrumentos';
    --Campos basicos
    COMMENT ON COLUMN instrumento.id IS 'Identificador del registro';
    --Campos propios
    COMMENT ON COLUMN instrumento.modelo IS 'Modelo del instrumento';
    COMMENT ON COLUMN instrumento.descripcion IS 'Descripcion del instrumento';
    --Campos FK
    COMMENT ON COLUMN instrumento.cat_fabricante_instrumento IS 'Identificador de la categoria del fabricante del instrumento';