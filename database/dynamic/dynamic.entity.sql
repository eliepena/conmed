SET search_path = dynamic,pg_catalog;
CREATE TABLE  entity(
--Campos Basicos
    id serial NOT NULL,
--Campos propios
    _name character varying(50) NOT NULL,
    _schema character varying(25) NOT NULL
--Campos FK
    
);
--CONSTRAINTS
    --PK
    ALTER TABLE entity ADD CONSTRAINT pk_entity PRIMARY KEY(id);
    --FK
    
--COMMENTS
    --Tabla
    COMMENT ON TABLE entity IS 'Entidad que contiene las entidades virtualizadas';
    --Campos basicos
    COMMENT ON COLUMN entity.id IS 'Identificador del registro';
    --Campos propios
    COMMENT ON COLUMN entity._name IS 'Nombre de la entidad';
    COMMENT ON COLUMN entity._schema IS 'Nombre del esquema al cual pertenece la entidad';
    --Campos FK