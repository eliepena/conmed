CREATE SCHEMA dynamic AUTHORIZATION postgres;

GRANT ALL ON SCHEMA dynamic TO postgres;

COMMENT ON SCHEMA dynamic IS 'Esquema que contiene la virtualizacion de tablas y campos del esquema business. Adicionalmente tiene las categorias';

SET search_path = dynamic,pg_catalog;