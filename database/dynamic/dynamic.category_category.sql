SET search_path = dynamic,pg_catalog;
CREATE TABLE  category_category(
--Campos Basicos
    
--Campos propios
    
--Campos FK
    id_parent integer NOT NULL,
    id_child integer NOT NULL
);
--CONSTRAINTS
    --PK
    ALTER TABLE category_category ADD CONSTRAINT pk_category_category PRIMARY KEY(id_parent,id_child);
    --FK
    ALTER TABLE ONLY category_category ADD CONSTRAINT fk_cat_cat_parent FOREIGN KEY (id_parent) REFERENCES category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY category_category ADD CONSTRAINT fk_cat_cat_child FOREIGN KEY (id_child) REFERENCES category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE category_category IS 'Entidad que permite la jerarquia y herencia multiple entre categorias';
    --Campos basicos
    
    --Campos propios

    --Campos FK
    COMMENT ON COLUMN category_category.id_parent IS 'Identificador de la categoria padre';
    COMMENT ON COLUMN category_category.id_child IS 'Identificador de la categoria hijo';