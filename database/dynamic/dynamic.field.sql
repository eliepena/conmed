SET search_path = dynamic,pg_catalog;
CREATE TABLE  field(
--Campos Basicos
    id serial NOT NULL,
--Campos propios
    _name character varying(50) NOT NULL,
    _label character varying(50) DEFAULT NULL,
    length integer DEFAULT NULL,
    validations text DEFAULT NULL,
    help text DEFAULT NULL,    
    tooltip text DEFAULT NULL,    
    example text DEFAULT NULL,
--Campos FK
    id_entity integer NOT NULL,
    cat_data_type integer NOT NULL
);
--CONSTRAINTS
    --PK
    ALTER TABLE field ADD CONSTRAINT pk_field PRIMARY KEY(id);
    --FK
    ALTER TABLE ONLY field ADD CONSTRAINT fk_fie_entity FOREIGN KEY (id_entity) REFERENCES entity(id) ON UPDATE CASCADE ON DELETE RESTRICT;
    ALTER TABLE ONLY field ADD CONSTRAINT fk_fie_cat_data_type FOREIGN KEY (cat_data_type) REFERENCES category(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--COMMENTS
    --Tabla
    COMMENT ON TABLE field IS 'Entidad que contiene los campos virtualizados';
    --Campos basicos
    COMMENT ON COLUMN field.id IS 'Identificador del registro';
    --Campos propios
    COMMENT ON COLUMN field._name IS 'Nombre del campo';
    COMMENT ON COLUMN field._label IS 'Etiqueta del campo';
    COMMENT ON COLUMN field.length IS 'Longitud del campo en caso de que aplique';
    COMMENT ON COLUMN field.validations IS 'Validaciones estandar para el campo se pueden extender en operaciones campos';
    COMMENT ON COLUMN field.help IS 'Ayuda detallada del campo, es usada en la documentacion';
    COMMENT ON COLUMN field.tooltip IS 'Ayuda del campo, es usada en el tooltip';
    COMMENT ON COLUMN field.example IS 'Data de ejemplo para el campo';
    --Campos FK
    COMMENT ON COLUMN field.id_entity IS 'Identificador de la entidad a la cual pertenece el campo';
    COMMENT ON COLUMN field.cat_data_type IS 'Identificador de la categoria de tipo de dato';