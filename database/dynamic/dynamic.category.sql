SET search_path = dynamic,pg_catalog;
CREATE TABLE  category(
--Campos Basicos
    id serial NOT NULL,
--Campos propios
    _table character varying(50) NOT NULL,
    _label character varying(150) NOT NULL,
    alt_value character varying(150) NOT NULL,
    _order integer DEFAULT NULL,
    is_business bit(1) DEFAULT B'1'::"bit"
--Campos FK
    
);
--CONSTRAINTS
    --PK
    ALTER TABLE category ADD CONSTRAINT pk_category PRIMARY KEY(id);
    --FK
    
--COMMENTS
    --Tabla
    COMMENT ON TABLE category IS 'Entidad que contiene las categorias utilizadas por otras entidades';
    --Campos basicos
    COMMENT ON COLUMN category.id IS 'Identificador del registro, tambien usado como valor de la categoria';
    --Campos propios
    COMMENT ON COLUMN category._table IS 'Nombre de la pseudo tabla que forma la categoria';
    COMMENT ON COLUMN category._label IS 'Nombre del elemento de la categoria';
    COMMENT ON COLUMN category.alt_value IS 'Valor alternativo del elemento de la categoria';
    COMMENT ON COLUMN category._order IS 'Valor utilizado para ordernar los elementos de la categoria';
    COMMENT ON COLUMN category.is_business IS 'Define si la categoria es parte de las reglas de negocio';
    --Campos FK